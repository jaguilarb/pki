/*##############################################################################
  ###  PROYECTO:              Librer�as generales                            ###
  ###  MODULO:                Sgi_BD : clase que maneja operaciones con BD   ###
  ###                                                                        ###
  ###  DESARROLLADORES:                                                      ###
  ###                         Roc�o Alejandra Mart�nez Olayo  RAMO           ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       diciembre 2005                                 ###
  ###                                                                        ###
  ##############################################################################
         1         2         3         4         5         6         7         8
12345678901234567890123456789012345678901234567890123456789012345678901234567890
*/

/*#################################################################################
   VERSION:
      V.1.00      (20051220 - 20051231) RAMO: Versi�n original
      V.2.00      (20141014 -         ) JAB: Cambios por migraci�n a Oracle
   CAMBIOS:
#################################################################################*/

#ifndef _SGI_BD_H_
#define _SGI_BD_H_

static const char* _SGI_BD_H_VERSION_ ATR_USED = "Sgi_BD @(#)"\
"DSIC10153AR_ 2010-04-14 Sgi_BD.h 1.1.1/4";

#include <Sgi_Bitacora.h>

#include <string>
#include <string.h>
#include <occi.h>
#include <stdio.h>
#include <map>
#include <vector>
#include <stdexcept>

#define BD_MAX_SQL              2048*2   // MAML 100413: por un Insert tabla ACUSE de la AR
#define BD_MODO_APLICACION      Environment::DEFAULT
#define BD_CADENA_CONEXION      "(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = %s)(PORT = %s))(CONNECT_DATA = (SERVER = DEDICATED)(SID = %s)))"
#define BD_MAX_SIZE_STRING_SQL  1024

using namespace oracle::occi;
using namespace std;


class CBD
{
    private:

        CBitacora*    bitacora;

        Environment*  entorno;
        Connection*   conexion;
        Statement*    sentencia;
        ResultSet*    resultado_consulta;

        vector<string> resultado_ejecucion;

        string        usuario;
        string        contrasenia;
        string        host;
        string        puerto;
        string        sid;
        string        error;

        bool          hay_conexion;
        bool          hay_registro;
        long          columnas;

        string armaCadenaConexion();

        void manejaError(SQLException* p_excepcion);
        void manejaError(exception* p_excepcion);
        void manejaError(const char* p_mensaje, ...);

        bool nuevaSentencia(string p_sentencia);
        bool liberaResultado();

        bool divideCadenaTexto(string* p_cadena, char p_separador, vector<string>* p_params);

        bool getParametrosOUT(string* p_formato, map<int, char>* p_parametros_out);
        char getTipoParametro(string* parametro);
        bool registraParametrosOUT(map<int, char>* p_parametros_out);
        bool getResultadoProcedimiento(map<int, char>* p_parametros_out);
        
  
    public:

        CBD(CBitacora* p_bitacora = NULL);
        virtual ~CBD();

        bool setConfig(const char* p_usuario, const char* p_contrasenia, const char* p_host, const char* p_puerto, const char* p_sid); 
        bool conecta();
        void desconecta();
        
        bool  consultaReg(const char* p_consulta_sql, ...);
        bool  consulta(const char* p_consulta_sql, ...);
        bool  sigReg();
   
        bool  ejecutaOper(const char* p_sentencia_sql, ...);
        bool  ejecutaSP(const char* p_nombre, const char* p_formato, ...);
        bool  ejecutaFuncion(const char* p_nombre, const char* p_formato_salida, const char* p_formato_entrada, ...);
        
        bool  getValores(const char* p_tipos, ...); // tipos "sicb", 4 vars : 1 std::string, 2 entero, 3 un caracter, 4 buffer 

        inline long getNumColumnas() { return columnas; };
        bool getColumna(int p_num_columna, string& p_valor);

        static int  prepCadDelim(int iDel, const char* cCad, char* cCadTrat); // iDel ->0 si "   -> 1 si '
};

#endif // _SGI_BD_H_
