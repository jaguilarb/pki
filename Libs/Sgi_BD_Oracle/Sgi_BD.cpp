static const char* _SGI_BD_CPP_VERSION_ ATR_USED = "Sgi_BD @(#)"\
"DSIC09212ARA 2007-12-13 Sgi_BD.cpp 1.1.1/3";

//#VERSION: 1.1.1
/*##############################################################################
  ###  PROYECTO:              Librer�as generales                            ###
  ###  MODULO:                Sgi_BD : clase que maneja operaciones con BD   ###
  ###                                                                        ###
  ###  DESARROLLADORES:                                                      ###
  ###                         Roc�o Alejandra Mart�nez Olayo  RAMO           ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       octubre 2005                                   ###
  ###                                                                        ###
  ##############################################################################
  1         2         3         4         5         6         7         8
  12345678901234567890123456789012345678901234567890123456789012345678901234567890
*/

/*#################################################################################
  VERSION:
     V.1.00      (20051001 - 20051231) RAMO: Versi�n original
     V.2.00      (20141014 -         ) JAB: Cambios por migraci�n a Oracle
 
  CAMBIOS:
     V.1.1       (20080806 - 20080806) MAML: incluye la funcion getColumna() : FALTA ETIQUETA

#################################################################################*/

#include <Sgi_BD.h>


/**
 * Constructor de la clase
 *
 * @param p_bitacora
 */
CBD::CBD(CBitacora* p_bitacora)
{
   entorno              = NULL;
   conexion             = NULL;
   sentencia            = NULL;
   resultado_consulta   = NULL;
   hay_conexion         = false;
   hay_registro         = false;
   columnas             = 0L;
   bitacora             = p_bitacora;
}

/**
 * Destructor de la clase
 */
CBD::~CBD()
{
   desconecta();
}

/**
 * Setea los p�rametros utilizados para la conexi�n a la base de datos
 *
 * @param p_usuario
 * @param p_contrasenia
 * @param p_host
 * @param p_puerto
 * @param p_sid
 *
 * @return bool   Indica si los par�metros fueron seteados
 */
bool CBD::setConfig(const char* p_usuario, const char* p_contrasenia, const char* p_host, const char* p_puerto, const char* p_sid)
{
   usuario     = string(p_usuario);
   contrasenia = string(p_contrasenia);
   host        = string(p_host);
   puerto      = string(p_puerto);
   sid         = string(p_sid);

   char u_esp = usuario.empty() ? 'N' : 'S';
   char c_esp = usuario.empty() ? 'N' : 'S';
   char h_esp = usuario.empty() ? 'N' : 'S';
   char p_esp = usuario.empty() ? 'N' : 'S';
   char s_esp = usuario.empty() ? 'N' : 'S';

   if (u_esp == 'N' || c_esp == 'N' || h_esp == 'N' || p_esp == 'N' || s_esp == 'N')
   {
      manejaError("Ha ocurrido un error al configurar los par�metros de conexi�n de la base de datos");
      manejaError("Existen par�metros de conexi�n no especificados: [usuario(%c), contrase�a(%c), host(%c), puerto(%c), sid(%c)]", u_esp, c_esp, h_esp, p_esp, s_esp);

      return false;
   }
   
   return true;
}

/**
 * Realiza la conexi�n a la base de datos, utilizando los par�metros seteados previamente
 *
 * @return bool   Indica si la conexi�n a la base de datos fue establecida
 */
bool CBD::conecta()
{
   string cadena_conexion;

   if (hay_conexion)
   {
      return true;
   }

   try
   {
      cadena_conexion = armaCadenaConexion();
      entorno         = Environment::createEnvironment(BD_MODO_APLICACION);
      conexion        = entorno->createConnection(usuario, contrasenia, cadena_conexion);
      hay_conexion    = true;

      return true;
   }
   catch (SQLException excepcion)
   {
      manejaError("Ha ocurrido un error al realizar la conexi�n a la base de datos");
      manejaError("Par�metros utilizados [usuario=%s, host=%s, puerto=%s, sid=%s]", usuario.c_str(), host.c_str(), puerto.c_str(), sid.c_str());
      manejaError(&excepcion);

      return false;
   }
}

/**
 * Realiza la desconexi�n de la base de datos
 */
void CBD::desconecta()
{
   if (hay_conexion)
   {
      try
      {
         if (sentencia)
         {
            if (resultado_consulta)
            {
               sentencia->closeResultSet(resultado_consulta);
            }
            
            conexion->terminateStatement(sentencia);
         }     
         
         entorno->terminateConnection(conexion);
         Environment::terminateEnvironment(entorno);

         resultado_ejecucion.clear();

         usuario.clear();
         contrasenia.clear();
         host.clear();
         puerto.clear();
         sid.clear();
         error.clear();

         hay_conexion = false;
         hay_registro = false;
         columnas     = 0L;
      }
      catch (SQLException excepcion)
      {
         manejaError("Ha ocurrido un error al realizar la desconexi�n de la base de datos");
         manejaError(&excepcion);
      }
   }
}


/**
 * Ejecuta una consulta a la base de datos, el resultado se mantiene en el objeto ResultSet y 
 * s�lo contiene 1 registro, por lo cual posterior a la consulta se ejecuta el m�todo sigReg()
 * para inmediatamente poder obtener el registro
 *
 * @param p_consulta_sql   La cadena que representa la consulta SQL
 * @param ...  Argumentos adicionales
 *
 * @return bool   Indica si la consulta se ejecut�
 */
bool CBD::consultaReg(const char* p_consulta_sql, ...)
{
   char buffer[BD_MAX_SQL];
   va_list argumentos;

   va_start(argumentos, p_consulta_sql);
   vsprintf(buffer, p_consulta_sql, argumentos);
   va_end(argumentos);

   bool ok = consulta(buffer);

   if (ok)
   {
      sigReg();
   }

   return ok;
}


/**
 * Ejecuta una consulta a la base de datos, el resultado se mantiene en el objeto ResultSet y 
 * puede contener 1 o m�s registros, por lo cual posteriormente se tiene que iterar utilizando
 * el m�todo sigReg()
 *
 * @param p_consulta_sql   La cadena que representa la consulta SQL
 * @param ...  Argumentos adicionales
 *
 * @return bool   Indica si la consulta se ejecut�
 */
bool CBD::consulta(const char* p_consulta_sql, ...)
{
   char buffer[BD_MAX_SQL];
   string consulta_sql;
   va_list argumentos;

   va_start(argumentos, p_consulta_sql);
   vsprintf(buffer, p_consulta_sql, argumentos);
   va_end(argumentos);

   consulta_sql = string(buffer);

   if (!nuevaSentencia(consulta_sql))
   {
      return false;
   }

   try
   {
      resultado_consulta = sentencia->executeQuery();
   }
   catch (SQLException excepcion)
   {
      manejaError("Ha ocurrido un error al ejecutar la consulta '%s' a la base de datos", consulta_sql.c_str());
      manejaError(&excepcion);

      return false;
   }

   return true;
}


/**
 * Obtiene el siguiente registro que mantiene el objeto ResultSet y actualiza el n�mero de columnas que contiene
 *
 * @return bool   Indica si hay m�s registros
 */
bool CBD::sigReg()
{
   if (!resultado_consulta)
   {
      hay_registro = false;
      columnas = 0L;
   }
   else
   {
      hay_registro = resultado_consulta->next() == ResultSet::DATA_AVAILABLE ? true : false;

      // Si hay registro obtiene el n�mero de columnas del mismo
      if (hay_registro)
      {
         vector<MetaData> lista_columnas = resultado_consulta->getColumnListMetaData();
         columnas = lista_columnas.size();
      }
   }

   return hay_registro;
}


/**
 * Obtiene los valores de las columnas de un registro devuelto por una consulta a la base de datos
 * o por la ejecuci�n de un procedimiento o funci�n
 *
 * @param tipos   Cadena que indica el n�mero y tipo de las columnas a obtener
 * @param ...     Argumentos donde guardar el valor de las columnas del registro
 *
 * @return  bool  Indica si se realiz� la obtenci�n de los datos
 */
bool CBD::getValores(const char* p_tipos, ...)
{
   va_list argumentos;

   if (!hay_registro)
   {
      manejaError("No existe registro del cual se pueda obtener datos");
      
      return false;
   }

   va_start(argumentos, p_tipos);

   for (int i = 0; p_tipos[i]; i++)
   {
      switch(p_tipos[i])
      {
         case 's':
            {
               string* str_arg = va_arg(argumentos, string*);
               string str = "";

               if (!getColumna(i, str))
               {
                  return false;
               }

               *str_arg = str;

               break;
            }
         case 'i':
            {
               int* num = va_arg(argumentos, int*);
               string str = "";

               if (!getColumna(i, str))
               {
                  return false;
               }

               *num = str.empty() ? MIN_INT : atoi(str.c_str());

               break;
            }
         case 'c':
            {
               char* car = va_arg(argumentos, char*);
               string str = "";

               if (!getColumna(i, str))
               {
                  return false;
               }

               *car = str.empty() ? 0 : str[0];

               break;
            }
         case 'b':
            {
               char* buffer = va_arg(argumentos, char*);
               string str = "";

               if (!getColumna(i, str))
               {
                  return false;
               }

               if (str.empty())
               {
                  buffer[0] = 0;
               }
               else
               {
                  strcpy(buffer, str.c_str());

                  for (int j = strlen(buffer) - 1; j >= 0 && buffer[j] == ' '; j--)
                  {
                     buffer[j] = 0;
                  }
               }

               break;
            }
      }
   }   
   
   va_end(argumentos);
   
   return true;
}


/**
 * Obtiene un campo/columna de un registro devuelto por una consulta o por la ejecuci�n de un
 * procedimiento o una funci�n
 *
 * @param p_num_columna El n�mero de columna que se quiere obtener, el rango permitido es [1 - columnas]
 * @param p_valor La cadena donde se escribe el resultado
 *
 * @return bool   Indica si se obtuvo el campo solicitado
 */
bool CBD::getColumna(int p_num_columna, string& p_valor)
{
   p_valor.clear();

   if (!hay_registro)
   {
      manejaError("No existe registro del cual se pueda obtener el campo %i", p_num_columna);
      
      return false;
   }

   // El �ndice para obtener columnas en Oracle comienza en 1, a diferencia de Informix que comienza en 0
   // Pero el c�digo de la PKI que hace uso de este m�todo solicita las columnas empezando por �ndice 0
   // Por lo cual se mantiene la forma de usarse, pero al utilizar los m�todos getXXX() de un objeto
   // Resultset o Statement se le suma 1

   try
   {
      if (resultado_consulta)
      {
         p_valor = resultado_consulta->getString(p_num_columna + 1);

         // Si al obtener el valor de la columna i del ResultSet este es NULO o TRUNCADO
         // Se retorna una cadena vac�a
         if (resultado_consulta->isNull(p_num_columna + 1) || resultado_consulta->isTruncated(p_num_columna + 1))
         {
            p_valor = "";
         }
      }
      else if (!resultado_ejecucion.empty())
      {
         p_valor = resultado_ejecucion.at(p_num_columna);
      }
      

      if (!p_valor.empty())
      {
         // Trim: para eliminar los espacios al final
         size_t found = p_valor.find_last_not_of(" \t\f\v\n\r");
         
         if (found != string::npos)
            p_valor.erase(found + 1);
         else
            p_valor.clear();
      }
   }
   catch (SQLException excepcion)
   {
      manejaError("Ha ocurrido un error al obtener el campo %i del registro actual", p_num_columna);
      manejaError(&excepcion);

      return false;
   }
   catch (out_of_range excepcion)
   {
      manejaError("Ha ocurrido un error al obtener el campo %i del registro actual", p_num_columna);
      manejaError(&excepcion);

      return false;
   }

   return true;
}


/**
 * Ejecuta operaciones DML y DDL sobre la base de datos
 *
 * @param p_sentencia_sql  Cadena que representa la sentencia SQL a ejecutar
 * @param ...  Argumentos
 *
 * @return bool   Indica si se ejecut� la sentencia SQL
 */
bool CBD::ejecutaOper(const char* p_sentencia_sql, ...)
{
   char buffer[BD_MAX_SQL];
   string sentencia_sql;
   va_list argumentos;

   va_start(argumentos, p_sentencia_sql);
   vsprintf(buffer, p_sentencia_sql, argumentos);
   va_end(argumentos);

   sentencia_sql = string(buffer);

   if (!nuevaSentencia(sentencia_sql))
   {
      return false;
   }

   try
   {
      sentencia->executeUpdate();
   }
   catch (SQLException excepcion)
   {
      manejaError("Ha ocurrido un error al ejecutar la sentencia '%s' en la base de datos", sentencia_sql.c_str());
      manejaError(&excepcion);

      return false;
   }

   return true;
}


/**
 * Ejecuta un procedimiento almacenado
 *
 * @param p_nombre El nombre del procedimiento almacenado
 * @param p_formato Cadena a formatear con los par�metros IN/OUT del procedimiento
 * @param ...  Argumentos que representan los par�metros IN/OUT (los primeros son IN y los �ltimos OUT)
 *
 * @return bool   Indica si se ejecut� el procedimiento
 */
bool  CBD::ejecutaSP(const char* p_nombre, const char* p_formato, ...)
{
   char buffer[BD_MAX_SQL];
   va_list argumentos;

   va_start(argumentos, p_formato);
   vsprintf(buffer, p_formato, argumentos);
   va_end(argumentos);

   string sentencia_sql = "BEGIN " + string(p_nombre) + "(" + string(buffer) + "); END;";    

   if (!nuevaSentencia(sentencia_sql))
   {
      manejaError("Ha ocurrido un error al ejecutar el procedimiento %s, [Error al crear el objeto Statement]", p_nombre);

      return false;
   }

   map<int, char> parametros_out;
   string formato = string(p_formato);

   if (!getParametrosOUT(&formato, &parametros_out))
   {
      manejaError("Ha ocurrido un error al ejecutar el procedimiento %s, [Error al obtener los par�metros OUT]", p_nombre);

      return false;
   }

   if (!registraParametrosOUT(&parametros_out))
   {
      manejaError("Ha ocurrido un error al ejecutar el procedimiento %s, [Error al registrar los par�metros OUT en el objeto Statement]", p_nombre);

      return false;
   }

   try
   {
        sentencia->executeUpdate();
        getResultadoProcedimiento(&parametros_out);
   }
   catch (SQLException excepcion) {
      manejaError("Ha ocurrido un error al ejecutar el procedimiento %s", p_nombre);
      manejaError(&excepcion);
      
      return false;
   }

   return true;
}


/**
 * Ejecuta una funci�n de la base de datos
 *
 * @param p_nombre El nombre de la funcion
 * @param p_formato_salida Cadena que contiene el formato de los par�metros OUT de la funci�n
 * @param p_formato_entrada Cadena que contiene el formato de los par�metros IN de la funci�n
 * @param ...  Argumentos que representan los par�metros IN
 *
 * @return bool   Indica si se ejecut� la funci�n
 */
bool  CBD::ejecutaFuncion(const char* p_nombre, const char* p_formato_salida, const char* p_formato_entrada, ...)
{
   char buffer[BD_MAX_SQL];
   va_list argumentos;

   va_start(argumentos, p_formato_entrada);
   vsprintf(buffer, p_formato_entrada, argumentos);
   va_end(argumentos);

   string sentencia_sql = "BEGIN " + string(p_formato_salida) + " := " + string(p_nombre) + "(" + string(buffer) + "); END;";    

   if (!nuevaSentencia(sentencia_sql))
   {
      manejaError("Ha ocurrido un error al ejecutar la funci�n %s, [Error al crear el objeto Statement]", p_nombre);

      return false;
   }

   map<int, char> parametros_out;
   string formato = string(p_formato_salida);

   if (!getParametrosOUT(&formato, &parametros_out))
   {
      manejaError("Ha ocurrido un error al ejecutar la funci�n %s, [Error al obtener los par�metros OUT]", p_nombre);

      return false;
   }

   if (!registraParametrosOUT(&parametros_out))
   {
      manejaError("Ha ocurrido un error al ejecutar la funci�n %s, [Error al registrar los par�metros OUT en el objeto Statement]", p_nombre);

      return false;
   }

   try
   {
        sentencia->executeUpdate();
        getResultadoProcedimiento(&parametros_out);
   }
   catch (SQLException excepcion) {
      manejaError("Ha ocurrido un error al ejecutar la funci�n %s", p_nombre);
      manejaError(&excepcion);
      
      return false;
   }

   return true;
}


/**
 * Arma la cadena de conexi�n a la base de datos Oracle, utilizando el host, puerto y sid
 *
 * @return string    "(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = host)(PORT = puerto))(CONNECT_DATA = (SERVER = DEDICATED)(SID = sid)))"
 */
string CBD::armaCadenaConexion()
{
   char buffer[512];

   sprintf(buffer, BD_CADENA_CONEXION, host.c_str(), puerto.c_str(), sid.c_str());

   return string(buffer);
}


/**
 * Escribe en la bit�cora el c�digo y mensaje de error
 *
 * @param excepcion  La excepci�n sql ocurrida
 */
void CBD::manejaError(SQLException* p_excepcion)
{
   error = "Error " + string(p_excepcion->getMessage());

   if (bitacora)
   {
      bitacora->escribePV(BIT_ERROR, "CBD: %s", error.c_str());
   }
}


/**
 * Escribe en la bit�cora el mensaje de error
 *
 * @param excepcion  La excepci�n ocurrida
 */
void CBD::manejaError(exception* p_excepcion)
{
   error = "Error " + string(p_excepcion->what());

   if (bitacora)
   {
      bitacora->escribePV(BIT_ERROR, "CBD: %s", error.c_str());
   }
}


/**
 * Escribe en la bit�cora el mensaje de error
 *
 * @param mensaje  El mensaje del error ocurrido
 */
void CBD::manejaError(const char* p_mensaje, ...)
{
   char buffer[2048];
   va_list argumentos;

   va_start(argumentos, p_mensaje);
   vsprintf(buffer, p_mensaje, argumentos);
   va_end(argumentos);

   error = string(buffer);

   if (bitacora)
   {
      bitacora->escribePV(BIT_ERROR, "CBD: %s", error.c_str());
   }
}


/**
 * Crea o resetea una sentencia sql
 *
 * @param sentencia Cadena que representa la sentencia SQL
 *
 * @return bool   Indica si se pudo crear/resetar la sentencia
 */
bool CBD::nuevaSentencia(string p_sentencia)
{
   if (sentencia)
   {
      liberaResultado();
   }
   else
   {
      try
      {
         sentencia = conexion->createStatement();
      }
      catch (SQLException excepcion)
      {
         manejaError("Ha ocurrido un error al crear el objeto Statement");
         manejaError(&excepcion);

         return false;
      }
   }
   
   try
   {
      sentencia->setSQL(p_sentencia);
   }
   catch (SQLException excepcion)
   {
      manejaError("Ha ocurrido un error al setear la sentencia '%s'", p_sentencia.c_str());
      manejaError(&excepcion);

      return false;
   }

   return true;
}


/**
 * Libera el ResultSet
 *
 * @return bool   Indica si el resultset se ha liberado
 */
bool CBD::liberaResultado()
{
   try
   {
      if (resultado_consulta)
      {
         sentencia->closeResultSet(resultado_consulta);
         resultado_consulta    = NULL;
      }

      resultado_ejecucion.clear();
      
      hay_registro = false;
      columnas     = 0L;

      return true;
   }
   catch (SQLException excepcion)
   {
      manejaError("Ha ocurrido un error al liberar el objeto ResultSet");
      manejaError(&excepcion);

      return false;
   }
}


/**
 * Obtiene los par�metros IN/OUT de un procedimiento almacenado y los agrega a un mapa (indice, tipo)
 *
 * @param p_formato La cadena de la cual se obtendr�n los par�metros IN/OUT
 * @param p_parametros_in_out El mapa donde se guardan los pares (indice, tipo) de cada par�metros (IN indices negativos, OUT indices positivos)
 *
 * @return bool   Indica si se obtuvieron los par�metros
 */
bool CBD::getParametrosOUT(string* p_formato, map<int, char>* p_parametros_out)
{
   vector<string> parametros;

   if (!divideCadenaTexto(p_formato, ',', &parametros))
   {
      return false;
   }

   int indice = 1; 

   for (vector<string>::iterator it = parametros.begin() ; it != parametros.end(); ++it)
   {
      string param = string(*it);

      // Los par�metros OUT vienen especificados como :tipo donde tipo=[i|s|c|b]
      if (param.find(":") != string::npos)
      {
         char tipo = getTipoParametro(&param); // Los tipos invalidos se especifican con el caracter 'x'

         if (tipo != 'x')
         {
            p_parametros_out->insert(pair<int, char>(indice, tipo));
         }

         indice++;
      }
   }

   return true;
}


/**
 * Obtiene el tipo de un par�metro
 *
 * @param parametros Cadena que representa el par�metro
 *
 * @return char   El tipo del par�metro: 'i', 's', 'c', 'b'
 */
char CBD::getTipoParametro(string* parametro)
{
   if (parametro->find("i") != string::npos)
   {
      return 'i';
   }
   else if (parametro->find("s") != string::npos)
   {
      return 's';
   }
   else if (parametro->find("c") != string::npos)
   {
      return 'c';
   }
   else if (parametro->find("b") != string::npos)
   {
      return 'b';
   }

   return 'x'; // Utiliza 'x' como caracter invalido
}


/**
 * Registra los par�metros OUT del procedimiento a ejecutar
 *
 * @param p_parametros_in_out El mapa de los par�metros IN/OUT pasados al ejecutar el procedimiento
 *
 * @return bool   Indica si se registraron los parametros OUT
 */
bool CBD::registraParametrosOUT(map<int, char>* p_parametros_out)
{
   for (map<int, char>::iterator it = p_parametros_out->begin(); it != p_parametros_out->end(); ++it)
   {
      if (it->first > 0)
      {
         switch (it->second)
         {
            case 'i':
            {
               try
               {
                  sentencia->registerOutParam(it->first, OCCIINT);
               }
               catch (SQLException excepcion)
               {
                  manejaError("Ha ocurrido un error al registrar el par�metro OUT (indice=%d, tipo=%s) en el objeto Statement", it->first, "OCCIINT");
                  manejaError(&excepcion);

                  return false;
               }

               break;
            }
            case 's':
            {
               try
               {
                  sentencia->registerOutParam(it->first, OCCISTRING, BD_MAX_SIZE_STRING_SQL);
               }
               catch (SQLException excepcion)
               {
                  manejaError("Ha ocurrido un error al registrar el par�metro OUT (indice=%d, tipo=%s) en el objeto Statement", it->first, "OCCISTRING");
                  manejaError(&excepcion);

                  return false;
               }

               break;
            }
            case 'c':
            {
               try
               {
                  sentencia->registerOutParam(it->first, OCCISTRING, BD_MAX_SIZE_STRING_SQL);
               }
               catch (SQLException excepcion)
               {
                  manejaError("Ha ocurrido un error al registrar el par�metro OUT (indice=%d, tipo=%s) en el objeto Statement", it->first, "OCCISTRING");
                  manejaError(&excepcion);

                  return false;
               }

               break;
            }
            case 'b':
            {
               try
               {
                  sentencia->registerOutParam(it->first, OCCISTRING, BD_MAX_SIZE_STRING_SQL);
               }
               catch (SQLException excepcion)
               {
                  manejaError("Ha ocurrido un error al registrar el par�metro OUT (indice=%d, tipo=%s) en el objeto Statement", it->first, "OCCISTRING");
                  manejaError(&excepcion);

                  return false;
               }

               break;
            }
         }
      }
   }
   
   return true;
}


/**
 * Obtiene el resultado de la ejecuci�n de un procedimiento
 *
 * @param p_parametros_out Mapa con los par�metros OUT registrados en el objeto Statement
 *
 * @return bool   Indica si se obtuvo el resultado
 */
bool CBD::getResultadoProcedimiento(map<int, char>* p_parametros_out)
{
   for (map<int, char>::iterator it = p_parametros_out->begin() ; it != p_parametros_out->end(); ++it)
   {
      switch (it->second)
      {
            case 'i':
            {
               char buffer[32];
               int numero;
               string resultado;

               try
               {
                  numero = sentencia->getInt(it->first);

                  // Si al obtener el par�metro del Statement este es NULO o TRUNCADO
                  // Se agrega una cadena vac�a
                  if (sentencia->isNull(it->first) || sentencia->isTruncated(it->first))
                  {
                     resultado = "";
                  }
                  else
                  {
                     sprintf(buffer, "%i", numero);
                     resultado = string(buffer);
                  }
                  
                  resultado_ejecucion.push_back(resultado);
               }
               catch(SQLException excepcion)
               {
                  manejaError("Ha ocurrido un error al obtener el contenido del par�metro OUT (indice=%d, tipo=%s) del objeto Statement", it->first, "INT");
                  manejaError(&excepcion);

                  return false;
               } 

               break;
            }
            case 's':
            {
               string resultado;

               try
               {
                  resultado = sentencia->getString(it->first);

                  // Si al obtener el par�metro del Statement este es NULO o TRUNCADO
                  // Se agrega una cadena vac�a
                  if (sentencia->isNull(it->first) || sentencia->isTruncated(it->first))
                  {
                     resultado = "";
                  }

                  resultado_ejecucion.push_back(resultado);
               }
               catch(SQLException excepcion)
               {
                  manejaError("Ha ocurrido un error al obtener el contenido del par�metro OUT (indice=%d, tipo=%s) del objeto Statement", it->first, "STRING");
                  manejaError(&excepcion);

                  return false;
               }

               break;
            }
            case 'c':
            {
               char caracter;
               string resultado;

               try
               {
                  resultado = sentencia->getString(it->first);

                  // Si al obtener el par�metro del Statement este es NULO o TRUNCADO
                  // Se agrega una cadena vac�a
                  if (sentencia->isNull(it->first) || sentencia->isTruncated(it->first))
                  {
                     resultado = "";
                  }
                  else
                  {
                     caracter = resultado[0];
                     resultado = string(1, caracter);
                  }

                  resultado_ejecucion.push_back(resultado);
               }
               catch(SQLException excepcion)
               {
                  manejaError("Ha ocurrido un error al obtener el contenido del par�metro OUT (indice=%d, tipo=%s) del objeto Statement", it->first, "STRING");
                  manejaError(&excepcion);

                  return false;
               }

               break;
            }
            case 'b':
            {
               string resultado;

               try
               {
                  resultado = sentencia->getString(it->first);

                  // Si al obtener el par�metro del Statement este es NULO o TRUNCADO
                  // Se agrega una cadena vac�a
                  if (sentencia->isNull(it->first) || sentencia->isTruncated(it->first))
                  {
                     resultado = "";
                  }

                  resultado_ejecucion.push_back(resultado);
               }
               catch(SQLException excepcion)
               {
                  manejaError("Ha ocurrido un error al obtener el contenido del par�metro OUT (indice=%d, tipo=%s) del objeto Statement", it->first, "STRING");
                  manejaError(&excepcion);

                  return false;
               }

               break;
            }
      }
   }

   if (!resultado_ejecucion.empty())
   {
      hay_registro = true;
      columnas = resultado_ejecucion.size();
   }

   return true;
}


/**
 * Divide una cadena de texto en tokens
 *
 * @param p_cadena La cadena de texto a dividir
 * @param p_separador El caracter a utilizar como separador para dividir la cadena
 * @param p_params Vector que contendr� los tokens
 *
 * @return bool   Indica si la divisi�n se realiz�
 */
bool CBD::divideCadenaTexto(string* p_cadena, char p_separador, vector<string>* p_params)
{
   string param;

   unsigned int inicio = 0;
   unsigned int fin = 0;

   while ((fin = p_cadena->find(p_separador, inicio)) != string::npos) {
      param = p_cadena->substr(inicio, fin - inicio);
      p_params->push_back(param);
      inicio = fin + 1;
   }

   param = p_cadena->substr(inicio);
   p_params->push_back(param);

   return true;
}


int CBD::prepCadDelim(int iDel, const char* cCad, char* cCadTrat)
{
 // MAML 070904: cCad y cCadTrat deben ser diferentes de NULL y si *cCad == "" (cadena vacia) de todos modos va a procesarla i.e. retornara 0    
 //if( cCad == NULL  || !cCad[0] || cCadTrat == NULL  || !cCadTrat[0] || iDel > 1 )
   if( cCad == NULL  || cCadTrat == NULL  || iDel > 1 ) 
      return -1;

   int iCad = strlen(cCad);
   int i  = 0 , k = 0;
   char cCadA[1024 * 2 ];
   char cCadAux[1024 * 2 ];

   memset(cCadA ,0, sizeof(cCadA));
   memset(cCadAux , 0 ,sizeof(cCadAux));

   strcpy(cCadA , cCad );

   while (i!=iCad )
   {
      if(  cCadA[i] == '\'' )
      {
         if(iDel == 1)
         {
            cCadAux[k] = cCadA[i];
            cCadAux[k+1] = cCadA[i];
            k = k + 2;
            i++;
            continue;
         }
      }
      if( cCadA[i] == '\"')
      {
         if(iDel == 0)
         {
            cCadAux[k] = cCadA[i];
            cCadAux[k+1] = cCadA[i];
            k = k + 2;
            i++;
            continue;
         }
      }
      cCadAux[k] = cCadA[i];
      k++;

      i++;
   }
   strcpy(cCadTrat , cCadAux);
   return 0;
}
