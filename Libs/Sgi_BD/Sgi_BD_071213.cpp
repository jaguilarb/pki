static const char* _SGI_BD_CPP_VERSION_ ATR_USED = "Sgi_BD @(#)"\
"DSI_07405BAS 2007-12-13 Sgi_BD.cpp 1.1.0/0";

//#VERSION: 1.1.0
/*##############################################################################
  ###  PROYECTO:              Librer�as generales                            ###
  ###  MODULO:                Sgi_BD : clase que maneja operaciones con BD   ###
  ###                                                                        ###
  ###  DESARROLLADORES:                                                      ###
  ###                         Roc�o Alejandra Mart�nez Olayo  RAMO           ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       octubre 2005                                   ###
  ###                                                                        ###
  ##############################################################################
  1         2         3         4         5         6         7         8
  12345678901234567890123456789012345678901234567890123456789012345678901234567890
*/

/*#################################################################################
  VERSION:
     V.1.00      (20051001 - 20051231) RAMO: Versi�n original
 
  CAMBIOS:
#################################################################################*/
#include <string>
#include <assert.h>

#include <Sgi_BD.h>
//#################################################################################
using std::string;
//#################################################################################
CBD::CBD(CBitacora* bit)
{
   m_query = NULL;
   m_row   = NULL;   

   m_bitacora = bit;
}
//#################################################################################
CBD::~CBD()
{
   desconecta();
}
//#################################################################################
bool CBD::setConfig(const char* ins, const char* bd, const char* usr, const char* pwd, const char* rol)
{
   if ((usr && !m_dbinfo.SetUser(ITString(usr)))     ||
       (bd  && !m_dbinfo.SetDatabase(ITString(bd)))  ||
       (ins && !m_dbinfo.SetSystem(ITString(ins)))   ||
       (pwd && !m_dbinfo.SetPassword(ITString(pwd))))
   {
      manejaError(&m_dbinfo);
      return false;
   }
      
   if (rol)
      m_rol = rol;
   
   return true;
}
//#################################################################################
bool CBD::conecta()
{
   if (m_Conexion.IsOpen())
      return true;
   
   if (!m_Conexion.Open(m_dbinfo))
   {
      manejaError(&m_Conexion);      
      return false;
   }
   
   if (!setBDRol())
      return false;
   
   return true;
}
//#################################################################################
bool CBD::conectaGral()
{
   if(m_Conexion.IsOpen())
      return true;
   if(!m_Conexion.Open(m_dbinfo))
   {
      manejaError(&m_Conexion);
      return false;
   }
   return true;
}
//#################################################################################
bool CBD::setRol(const char* rol)
{
   m_rol = rol;
   return setBDRol();
}
//#################################################################################
bool CBD::setBDRol()
{
   if (m_rol.empty())
      return true;
   
   return ejecutaOper("SET ROLE %s", m_rol.c_str());
}
//#################################################################################
void CBD::desconecta()
{
   liberaQuery();
   
   if (!m_Conexion.Close())
      manejaError(&m_Conexion);
}
//#################################################################################
bool CBD::consulta(const char* sqlFto,...)
{
   bool ok = false;
   
   va_list args;
   va_start(args, sqlFto);
   vsprintf(m_sentencia, sqlFto, args);
   va_end(args);
   
   if (nuevoQuery())
   {
      if (m_query->ExecForIteration(m_sentencia)) 
         ok = true;
      else
      {   
         if(m_query->Error())
            manejaError(m_query, true);
      }
   }
   
   return ok;
}
//#################################################################################
bool CBD::sigReg()
{
   liberaRow();
   m_row = m_query->NextRow();   
   return m_row != NULL;
}
//#################################################################################
bool CBD::ejecutaSP(const char* nombre, const char* paramFto , ...)
{
   va_list args;
   va_start(args, paramFto);
   vsprintf(m_sentencia, paramFto, args);
   va_end(args);
   
   string sql("EXECUTE PROCEDURE ");
   sql += string(nombre) + "(" + m_sentencia + ")";
   
   //>+ ERGL (070118)
   return consultaReg("%s", sql.c_str());
   /*>- ERGL (070118) Provoca error con el uso de % en los datos del contribuyente.
   return consultaReg(sql.c_str());
   -<*/
}
//#################################################################################
bool CBD::consultaReg(const char* sqlFto, ...)
{
   bool ok = false;
   
   va_list args;
   va_start(args, sqlFto);
   vsprintf(m_sentencia, sqlFto, args);
   va_end(args);

   if (nuevoQuery())
   {
      m_row = m_query->ExecOneRow(m_sentencia);
      
      if (!m_row && m_query->Error())
      {
         manejaError(m_query, true);
         ok = false;
      }
      else
         ok = true;
   }
   
   return ok;
}
//#################################################################################
bool CBD::ejecutaOper(const char* sqlFto, ...)
{
   bool ok = false;
   
   va_list args;
   va_start(args, sqlFto);
   vsprintf(m_sentencia, sqlFto, args);
   va_end(args);

   if (nuevoQuery())
   {
      ok = m_query->ExecForStatus(m_sentencia);
      if (!ok)
         manejaError(m_query, true);
   }
   
   return ok;
}
//#################################################################################
bool CBD::manejaError(ITErrorManager* em, bool query)
{
   if (!m_bitacora)
      return true;
   
   m_Error = string(em->ErrorText().Data()) + " [SQLSTATE=" + em->SqlState().Data() + "]";

   if (query)
      m_Error += string(" SQL=\"") + m_sentencia + "\"";

   m_bitacora->escribe(BIT_ERROR,m_Error.c_str());
       
   return true;
}
//#################################################################################
bool CBD::getValores(const char* tipos, ...)
{
   if (!m_row)
   {
      m_bitacora->escribe(BIT_DEBUG, "Se mand� llamar CBD::getValores cuando no existe registro");
      return false;
   }
   
   va_list args;
   va_start(args, tipos);

   for (int i = 0; tipos[i]; i++)
   {
      ITValue *col = m_row->Column(i);
      assert(col != NULL);
      
      if (!col)
      {
         m_bitacora->escribe(BIT_DEBUG, "Se mand� llamar CBD::getValores con m�s parametros de los correspondientes");
         return false;
      }
      switch(tipos[i])
      {
         case 's':
            {
               string* str = va_arg(args, string*);
               if (col->IsNull())
                  *str = "";
               else
               {
                  *str = col->Printable().Data();
                  string::size_type lsb = str->find_last_not_of(' ') + 1;
                  if (lsb < str->length())
                     str->resize(lsb);
               }
               break;
            }
         case 'i':
            {
               int* num = va_arg(args, int*);
               if (col->IsNull())
                  *num = MIN_INT;
               else
                  *num = atoi(col->Printable());
               break;
            }
         case 'c':
            {
               char* car = va_arg(args, char*);
               if (col->IsNull())
                  *car = 0;
               else
                  *car = col->Printable().Data()[0];
               break;
            }
         case 'b':
            {
               char* buffer = va_arg(args, char*);
               if (col->IsNull())
                  buffer[0] = 0;
               else
               {
                  strcpy(buffer, col->Printable().Data());
                  for (int j = strlen(buffer) - 1; j >= 0 && buffer[j] == ' '; j--)
                     buffer[j] = 0;
               }
               break;
            }
      }
      col->Release();
   }   
   
   va_end(args);
   
   return true;
}
//#################################################################################
bool CBD::liberaQuery()
{
   liberaRow();
   
   if (!m_query)
        return true;
  
   if(!m_query->Finish() && m_query->Error())
   {
      manejaError(m_query);
      return false;
   }   

   delete m_query; 
   m_query = NULL;
   return true;
}
//#################################################################################
bool CBD::liberaRow()
{
   if (m_row)
   {
      m_row->Release();
      m_row = NULL;
   }
   return true;
}
//#################################################################################
bool CBD::nuevoQuery()
{
   liberaQuery();
   m_query = new ITQuery(m_Conexion);
   if (!m_query)
   {
      m_bitacora->escribe(BIT_ERROR, "Error al crear el objeto ITQuery");
      manejaError(&m_Conexion);
      return false;
   }
   return true;
}

//#################################################################################

int CBD::prepCadDelim(int iDel ,const char * cCad , char *cCadTrat)
{
 // MAML 070904: cCad y cCadTrat deben ser diferentes de NULL y si *cCad == "" (cadena vacia) de todos modos va a procesarla i.e. retornara 0    
 //if( cCad == NULL  || !cCad[0] || cCadTrat == NULL  || !cCadTrat[0] || iDel > 1 )
   if( cCad == NULL  || cCadTrat == NULL  || iDel > 1 ) 
      return -1;

   int iCad = strlen(cCad);
   int i  = 0 , k = 0;
   char cCadA[1024 * 2 ];
   char cCadAux[1024 * 2 ];

   memset(cCadA ,0, sizeof(cCadA));
   memset(cCadAux , 0 ,sizeof(cCadAux));

   strcpy(cCadA , cCad );

   while (i!=iCad )
   {
      if(  cCadA[i] == '\'' )
      {
         if(iDel == 1)
         {
            cCadAux[k] = cCadA[i];
            cCadAux[k+1] = cCadA[i];
            k = k + 2;
            i++;
            continue;
         }
      }
      if( cCadA[i] == '\"')
      {
         if(iDel == 0)
         {
            cCadAux[k] = cCadA[i];
            cCadAux[k+1] = cCadA[i];
            k = k + 2;
            i++;
            continue;
         }
      }
      cCadAux[k] = cCadA[i];
      k++;

      i++;
   }
   strcpy(cCadTrat , cCadAux);
   return 0;
}

