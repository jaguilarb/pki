/*##############################################################################
  ###  PROYECTO:              Librer�as generales                            ###
  ###  MODULO:                Sgi_BD : clase que maneja operaciones con BD   ###
  ###                                                                        ###
  ###  DESARROLLADORES:                                                      ###
  ###                         Roc�o Alejandra Mart�nez Olayo  RAMO           ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       diciembre 2005                                 ###
  ###                                                                        ###
  ##############################################################################
         1         2         3         4         5         6         7         8
12345678901234567890123456789012345678901234567890123456789012345678901234567890
*/

/*#################################################################################
   VERSION:
      V.1.00      (20051220 - 20051231) RAMO: Versi�n original

   CAMBIOS:
#################################################################################*/
#ifndef _SGI_BD_H_
#define _SGI_BD_H_
static const char* _SGI_BD_H_VERSION_ ATR_USED = "Sgi_BD @(#)"\
"DSIC10153AR_ 2010-04-14 Sgi_BD.h 1.1.1/4";

//#VERSION: 1.1.1
// V.1.1.1     (20100413 - 20100414) MAML: Se amplia el buffer para lanzar un Query
/*###############################################################################*/
#include <string>

#include <Sgi_Bitacora.h>

#include <it.h>
/*###############################################################################*/
#define BD_MAX_SQL 2048*2   // MAML 100413: por un Insert tabla ACUSE de la AR
/*###############################################################################*/
class CBD
{
      ITConnection      m_Conexion;
      std::string       m_Error;
      ITDBInfo          m_dbinfo;
      std::string       m_rol;
      ITQuery*          m_query;
      ITRow*            m_row;
      CBitacora*        m_bitacora;
      char              m_sentencia[BD_MAX_SQL];
      long              m_numColumnas;
      
      bool manejaError(ITErrorManager* em, bool query = false);
      bool setBDRol();
      bool liberaQuery();
      bool liberaRow();
      bool nuevoQuery();
  
   public:
   
   CBD(CBitacora* bit = NULL);
   virtual ~CBD();
   bool setConfig(const char* ins, const char* bd, const char* usr, const char* pwd, const char* rol); 
   bool conecta();
   bool conectaGral();
   bool setRol(const char* rol);
   void desconecta();

   bool  consulta(const char* sqlFto,...);
   bool  sigReg();
   
   bool  ejecutaSP(const char* nombre,const char* paramFto, ...);
   
   bool  consultaReg(const char* sqlFto,...);
   bool  ejecutaOper(const char* sqlFto,...);

   bool  getValores(const char* tipos, ...); // tipos "sicb", 4 vars : 1 std::string, 2 entero, 3 un caracter, 4 buffer 

   static int  prepCadDelim(int iDel ,const char * cCad , char *cCadTrat); // iDel ->0 si "   -> 1 si '

   inline long getNumColumnas() { return m_numColumnas; };

   bool getColumna (int num_columna, std::string& valor);  
};
#endif // _SGI_BD_H_

