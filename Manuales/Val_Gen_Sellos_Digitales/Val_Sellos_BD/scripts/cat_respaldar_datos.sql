-- Respalda los datos de la tabla cat_regimen
unload to 'respaldos/cat_regimen_respaldo_datos.sql' delimiter '|' select * from cat_regimen;

-- Respalda los datos de la tabla cat_regimen
unload to 'respaldos/cat_rol_respaldo_datos.sql' delimiter '|' select * from cat_rol;

-- Respalda los datos de la tabla cat_regimen
unload to 'respaldos/cat_obligacion_respaldo_datos.sql' delimiter '|' select * from cat_obligacion;

-- Respalda los datos de la tabla cat_regimen
unload to 'respaldos/cat_actividad_respaldo_datos.sql' delimiter '|' select * from cat_actividad;