-- Elimina el campo operador de la tabla cat_regimen
alter table cat_regimen drop operador;

-- Agrega el campo regimen_act a la tabla cat_regimen
alter table cat_regimen add (regimen_act smallint not null);