-- Elimina el procedimiento almacenado que validar si un regimen permite generar sellos digitales

DROP PROCEDURE "informix".sp_valida_gen_sd(INTEGER, SET(INTEGER NOT NULL), SET(INTEGER NOT NULL), SET(INTEGER NOT NULL));

