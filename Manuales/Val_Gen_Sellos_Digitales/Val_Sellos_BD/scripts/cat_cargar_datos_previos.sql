-- Carga los datos de la tabla cat_regimen
load from 'respaldos/cat_regimen_respaldo_datos.sql' delimiter '|' insert into cat_regimen;

-- Carga los datos de la tabla cat_regimen
load from 'respaldos/cat_rol_respaldo_datos.sql' delimiter '|' insert into cat_rol;

-- Carga los datos de la tabla cat_regimen
load from 'respaldos/cat_obligacion_respaldo_datos.sql' delimiter '|' insert into cat_obligacion;

-- Carga los datos de la tabla cat_regimen
load from 'respaldos/cat_actividad_respaldo_datos.sql' delimiter '|' insert into cat_actividad;