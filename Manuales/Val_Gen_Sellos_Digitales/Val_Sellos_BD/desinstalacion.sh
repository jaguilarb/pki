#!/bin/bash 

#*********************************************************************************************************************
#   Script para deshacer los cambios implementados al instalar el procedimiento almacenado que valida las obligaciones
#   de un RFC al solicitar la generación de sellos digitales
#
#   Objetivos:
#               - Deshacer los cambios implementados por la instalación del procedimiento almacenado sp_valida_gen_sd
#
#   Requisitos: Ninguno
#
#**********************************************************************************************************************

# Directorios
DIRECTORIO_RESPALDOS="respaldos"
DIRECTORIO_SCRIPTS="scripts"


# Scripts
ARCHIVO_CAT_REGIMEN_RESPALDO_ESTRUCTURA="cat_regimen_respaldo_estructura.sql"
ARCHIVO_CAT_REGIMEN_ELIMINAR="cat_regimen_eliminar.sql"
ARCHIVO_CAT_BORRAR_DATOS="cat_borrar_datos.sql"
ARCHIVO_CAT_CARGAR_DATOS_PREVIOS="cat_cargar_datos_previos.sql"
ARCHIVO_SP_ELIMINAR="sp_eliminar.sql"


# Parámetros de la base de datos
BD_NOMBRE="ar_sat"


# Valida la existencia del directorio de respaldos
echo "Validación del directorio de respaldos......................[ INICIADO   ]"

if [ ! -d $DIRECTORIO_RESPALDOS ]; then
    echo  "Validación del directorio de respaldos......................[ FALLIDO    ]"

    exit 1
fi

echo  "Validación del directorio de respaldos......................[ FINALIZADO ]"


# Valida que se pueda utilizar el comando dbaccess
echo "Validación del acceso al comando dbaccess...................[ INICIADO   ]"

if ! type dbaccess ; then
    echo  "Validación del acceso al comando dbaccess...................[ FALLIDO    ]"

    exit 1
fi

echo  "Validación del acceso al comando dbaccess...................[ FINALIZADO ]"


# Elimina el procedimiento almacenado
echo "Desinstalación del procedimiento almacenado.................[ INICIADO   ]"

if ! dbaccess $BD_NOMBRE "$DIRECTORIO_SCRIPTS/$ARCHIVO_SP_ELIMINAR"; then
    echo  "Desinstalación del procedimiento almacenado.................[ FALLIDO    ]"

    exit 1
else
    echo  "Desinstalación del procedimiento almacenado.................[ FINALIZADO ]"
fi


# Borra los datos actuales de los catalogos (cat_regimen, cat_rol, cat_obligacion y cat_actividad)
echo "Borrado de los datos de los catálogos.......................[ INICIADO   ]"

if ! dbaccess $BD_NOMBRE "$DIRECTORIO_SCRIPTS/$ARCHIVO_CAT_BORRAR_DATOS"; then
    echo  "Borrado de los datos de los catálogos.......................[ FALLIDO    ]"

    exit 1
else
    echo  "Borrado de los datos de los catálogos.......................[ FINALIZADO ]"
fi


# Regresa la estructura inicial de la tabla cat_regimen, borra la tabla regimen
echo "Borrado de la tabla cat_regimen.............................[ INICIADO   ]"

if ! dbaccess $BD_NOMBRE "$DIRECTORIO_SCRIPTS/$ARCHIVO_CAT_REGIMEN_ELIMINAR" ; then
    echo "Borrado de la tabla cat_regimen.............................[ FALLIDO    ]"

    exit 1
else
    echo "Borrado de la tabla cat_regimen.............................[ FINALIZADO ]"
fi


# Regresa la estructura inicial de la tabla cat_regimen, crea la tabla regimen utilizando el respaldo creado
echo "Creación de la tabla cat_regimen............................[ INICIADO   ]"

if ! dbaccess $BD_NOMBRE "$DIRECTORIO_RESPALDOS/$ARCHIVO_CAT_REGIMEN_RESPALDO_ESTRUCTURA" ; then
    echo "Creación de la tabla cat_regimen............................[ FALLIDO    ]"

    exit 1
else
    echo "Creación de la tabla cat_regimen............................[ FINALIZADO ]"
fi


# Carga los datos de los catalogos (cat_regimen, cat_rol, cat_obligacion y cat_actividad) utilizando los respaldos creados
echo "Carga de los datos de los catálogos.........................[ INICIADO   ]"

if ! dbaccess $BD_NOMBRE "$DIRECTORIO_SCRIPTS/$ARCHIVO_CAT_CARGAR_DATOS_PREVIOS"; then
    echo  "Carga de los datos de los catálogos.........................[ FALLIDO    ]"

    exit 1
else
    echo  "Carga de los datos de los catálogos.........................[ FINALIZADO ]"
fi