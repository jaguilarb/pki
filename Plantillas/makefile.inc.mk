#/* vim: set filetype=make : */
################################################################################
###    PROYECTO:              SAT-SGI                                        ###
###    MODULO:                Archivo make de inclusion general              ###
###                                                                          ###
###    DESARROLLADORES:                                                      ###
###                           H�ctor Ornelas Arciga     HOA                  ###
###                                                                          ###
###    FECHA DE INICIO:       Miercoles 7, diciembre 2005                    ###
###                                                                          ###
###    Este makefile recibe las directivas:                                  ###
###         make [DBG=1]            Genera el ejecutable (release o debuger) ###
###         make [DBG=1] limpia     Limpia los objetos y los ejecutables     ###
###         make [DBG=1] copia      Copia los ejecutables al repositorio     ###
###                                                                          ###
###         make all                                                         ###
###         make limpia_all                                                  ###
###         make copia_all                                                   ###
###                                                                          ###
##.#############################################################################
#        1         2         3         4         5         6         7         8
#2345678901234567890123456789012345678901234567890123456789012345678901234567890
################################################################################
#   VERSION:
#      V.01.00    (20051207)          HOA : Versi�n original
#      V.02.00    (20070208)          HOA : Cambios para integrar a repositorio de harvest,
#                                           Integraci�n de Componentes requiere recompilar
#          .01    (20070228)          HOA : Integraci�n lib 3os 'zlib'
#          .02    (20070913)          HOA : ajustes para poder compilar en HP-UX con GCC o con aCC
################################################################################
PLANTILLA=$(shell grep "VER_PLANTILLA=" $(REP_DS)/Plantillas/makefile | cut -c 15-)
ifneq ($(PLANTILLA),$(VER_PLANTILLA))
   $(warning "ADVERTENCIA: el makefile fue generado a partir de una plantilla diferente a la actual")
endif
################################################################################
################################################################################
# Requisitos:
#    PLTFRM_DESA : variable de ambiente que indica la Plataforma de desarrollo
#                  rh72, cos41, hpux11, sles10, win
ifndef PLTFRM_DESA
   $(error "Error: no est� definida la plataforma de desarrollo PLTFRM_DESA")
endif  
################################################################################
# Verificaci�n de valores de las variables para el proyecto
ifneq    ($(ES_LIB),S)
   ifneq ($(ES_LIB),N)
      $(error "Error: variable mal definida ES_LIB(S/N)=$(ES_LIB)")
   endif
endif

ifneq    ($(OPC_THREAD), S)
   ifneq ($(OPC_THREAD), M)
      $(error "Error: variable mal definida OPC_THREAD(S/M)=$(OPC_THREAD)")
   endif
endif

ifneq          ($(INFORMIX),N)
   ifneq       ($(INFORMIX),H)
      ifneq    ($(INFORMIX),E)
         ifneq ($(INFORMIX),D)
            $(error "Error: variable mal definida INFORMIX(N/H/E/D)=$(INFORMIX)")
         endif
      endif
   endif
endif

ifneq          ($(ORACLE),N)
   ifneq       ($(ORACLE),H)
      ifneq    ($(ORACLE),E)
         ifneq ($(ORACLE),D)
            $(error "Error: variable mal definida ORACLE(N/H/E/D)=$(ORACLE)")
         endif
      endif
   endif
endif

ifneq       ($(OPENSSL),N)
   ifneq    ($(OPENSSL),D)
      ifneq ($(OPENSSL),E)
         $(error "Error: variable mal definida OPENSSL(N/D/E)=$(OPENSSL)")
      endif
   endif
endif

ifneq ($(OPENSSL),N)
   ifndef OPENSSL_VER
      $(error "Error: debe indicar la versi�n de OpenSSL a utilizar (OPENSSL_VER)")
   endif
endif

ifndef ZLIB
   ZLIB=N
endif
ifneq       ($(ZLIB),N)
   ifneq    ($(ZLIB),D)
      ifneq ($(ZLIB),E)
         $(error "Error: variable mal definida ZLIB(N/D/E)=$(ZLIB)")
      endif
   endif
endif

ifneq ($(ZLIB),N)
   ifndef ZLIB_VER
      $(error "Error: debe indicar la versi�n de ZLIB a utilizar (ZLIB_VER)")
   endif
endif

ifndef MINIZIP
   MINIZIP=N
endif
ifneq       ($(MINIZIP),N)
   ifneq    ($(MINIZIP),D)
      ifneq ($(MINIZIP),E)
         $(error "Error: variable mal definida MINIZIP(N/D/E)=$(MINIZIP)")
      endif
   endif
endif

ifneq ($(MINIZIP),N)
   ifndef MINIZIP_VER
      $(error "Error: debe indicar la versi�n de MINIZIP a utilizar (MINIZIP_VER)")
   endif
endif

################################################################################
ifeq ($(PLTFRM_DESA),win)
   CPARM=/
   GENOBJ=Fo
   LIB=lib
   LOUT=/OUT:
   EXT_OBJ=obj
   EXT_STA=lib
   EXT_DIN=dll
else
   CPARM=-
   GENOBJ=o
   LIB=ar 
   LOUT=-r 
   EXT_OBJ=o
   EXT_STA=a
   ifeq ($(PLTFRM_DESA),hpux11)
      EXT_DIN=sl
   else
      EXT_DIN=so
   endif
endif
################################################################################
ifdef DBG_NO_SRV
   DBG=1
   SUFNS=_ns
   DEF_DBGNS=$(CPARM)DDBG_NO_SRV
endif
################################################################################
# SAT #
SAT_BPATH    = $(REP_DS)/bin
SAT_LPATH    = $(REP_DS)/lib
SAT_IPATH    = $(REP_DS)/include
################################################################################
ifeq ($(ES_ORACLE), 0)
   # INFORMIX #
   ifneq ($(INFORMIX),N)
      IFX_LPATH  = $(INFORMIXDIR)/lib
      IFX_DEFS   = $(CPARM)DIT_DO_NOT_SIMULATE_BOOL $(CPARM)DIT_HAS_DISTINCT_LONG_DOUBLE \
                   $(CPARM)DIT_COMPILER_HAS_LONG_LONG $(CPARM)DIT_DLLIB $(CPARM)DMITRACE_OFF
      IFX_INC    = $(CPARM)I$(INFORMIXDIR)/incl/c++ $(CPARM)I$(INFORMIXDIR)/incl/dmi \
                   $(CPARM)I$(INFORMIXDIR)/incl $(CPARM)I$(INFORMIXDIR)/incl/esql
      ifeq ($(INFORMIX),D)
      IFX_LIBD   = $(CPARM)L$(IFX_LPATH)/c++/ $(CPARM)lifc++  \
                   $(CPARM)L$(IFX_LPATH)/esql/ \
                   $(CPARM)L$(IFX_LPATH)/dmi/ $(CPARM)lifdmi $(CPARM)L$(IFX_LPATH) $(CPARM)lifsql $(CPARM)lifasf $(CPARM)lifgen $(CPARM)lifos $(CPARM)lifgls $(CPARM)lifglx \
                  $(INFORMIXDIR)/lib/esql/checkapi.o
      else
         ifeq ($(INFORMIX),E)
            IFX_LIBS   = $(IFX_LPATH)/c++/libifc++.a \
                         $(IFX_LPATH)/esql/libifgls.a \
                         $(IFX_LPATH)/esql/libifglx.a \
                         $(IFX_LPATH)/dmi/libifdmi.a \
                         $(IFX_LPATH)/esql/libifgen.a \
                         $(IFX_LPATH)/esql/libifsql.a \
                         $(IFX_LPATH)/libifasf.a \
                         $(IFX_LPATH)/esql/libifos.a
   #                      $(IFX_LPATH)/esql/checkapi.o 
    
         endif
      endif
   endif # INFORMIX
else
# ORACLE #
   ifneq ($(ORACLE),N)
      ORA_LPATH  = $(OCCI_HOME)
      ORA_DEFS   = $(CPARM)D_REENTRANT
      ORA_INC    = $(CPARM)I$(OCCI_HOME)/sdk/include
      ifeq ($(ORACLE),D)
      ORA_LIBD   = $(CPARM)L$(ORA_LPATH) $(CPARM)locci $(CPARM)lclntsh
      else
         ifeq ($(ORACLE),E)
            ORA_LIBS   = $(ORA_LPATH)
         endif
      endif
   endif # ORACLE
endif
################################################################################
# OpenSSL #
ifneq ($(OPENSSL),N)
   SSL_PATH = $(REP_DS)/Libs3os/openssl
   SSL_LPATH = $(SSL_PATH)/lib/$(PLTFRM_DESA)
   SSL_IPATH = $(CPARM)I$(SSL_PATH)/include/$(OPENSSL_VER)
   ifeq ($(OPENSSL),D)
      LIBD_SSL=$(SSL_LPATH)/libssl_$(OPENSSL)$(OPC_THREAD)$(SUFDBG).$(OPENSSL_VER).$(EXT_DIN) \
               $(SSL_LPATH)/libcrypto_$(OPENSSL)$(OPC_THREAD)$(SUFDBG).$(OPENSSL_VER).$(EXT_DIN)
   else
      LIBS_SSL=$(SSL_LPATH)/libssl_$(OPENSSL)$(OPC_THREAD)$(SUFDBG).$(OPENSSL_VER).a \
               $(SSL_LPATH)/libcrypto_$(OPENSSL)$(OPC_THREAD)$(SUFDBG).$(OPENSSL_VER).a
   endif
endif
################################################################################
# ZLIB #
ifneq ($(ZLIB),N)
   ZLIB_PATH = $(REP_DS)/Libs3os/zlib
   ZLIB_LPATH = $(ZLIB_PATH)/lib/$(PLTFRM_DESA)
   ZLIB_IPATH = $(CPARM)I$(ZLIB_PATH)/include/$(ZLIB_VER)
   ifeq ($(ZLIB),D)
      LIBD_ZLIB=$(ZLIB_LPATH)/zlib_$(ZLIB)$(OPC_THREAD)$(SUFDBG).$(ZLIB_VER).$(EXT_DIN) 
   else
      LIBS_ZLIB=$(ZLIB_LPATH)/zlib_$(ZLIB)$(OPC_THREAD)$(SUFDBG).$(ZLIB_VER).a 
   endif
endif
################################################################################
# MINIZIP #
ifneq ($(MINIZIP),N)
   MINIZIP_PATH = $(REP_DS)/Libs3os/minizip
   MINIZIP_LPATH = $(MINIZIP_PATH)/lib/$(PLTFRM_DESA)
   MINIZIP_IPATH = $(CPARM)I$(MINIZIP_PATH)/include/$(MINIZIP_VER)
   ifeq ($(MINIZIP),D)
      LIBD_MINIZIP=$(MINIZIP_LPATH)/minizip_$(MINIZIP)$(OPC_THREAD)$(SUFDBG).$(MINIZIP_VER).$(EXT_DIN) \
                   $(MINIZIP_LPATH)/miniunz_$(MINIZIP)$(OPC_THREAD)$(SUFDBG).$(MINIZIP_VER).$(EXT_DIN) 
   else
      LIBS_MINIZIP=$(MINIZIP_LPATH)/minizip_$(MINIZIP)$(OPC_THREAD)$(SUFDBG).$(MINIZIP_VER).a \
                   $(MINIZIP_LPATH)/miniunz_$(MINIZIP)$(OPC_THREAD)$(SUFDBG).$(MINIZIP_VER).a 
   endif
endif
################################################################################
# GENERAL #

INCL=$(CPARM)I. $(CPARM)I$(SAT_IPATH) $(SSL_IPATH) $(IFX_INC) $(ORA_INC) $(ZLIB_IPATH) $(MINIZIP_IPATH) $(INCL_ADIC)
LCRYPT=$(CPARM)lcrypt

ifeq ($(PLTFRM_DESA),hpux11)
   ifeq (${CC}.,.)
      CC=aCC
   else
      ifneq ($(CC),aCC)
         ifneq ($(CC),gcc)
            $(error "Error: s�lo se soportan los compiladores 'gcc' y 'aCC'")
         endif
      endif
   endif

   PLTFRM_DESA:=${PLTFRM_DESA}_${CC}
   SAT_BPATH:=$(SAT_BPATH)_$(CC)
   SAT_LPATH:=$(SAT_LPATH)_$(CC)

   ifeq ($(CC),aCC)
      # Warnings suprimidos 392 (conversi�n de tipo (cast) no necesaria)
      #NO_WARNINGS=+W392

      CC_OPTS_REL=-AA +DD64 -z +w +O2 -DATR_USED="" $(NO_WARNINGS)
      CC_OPTS_DBG=-AA +DD64 -z +w +O0 -DATR_USED="" -g1 +d -DDBG $(DEF_DBGNS) $(NO_WARNINGS)
      LNK_OPTS_PLT_EXE=
      LNK_OPTS_PLT_LIB=-b
      LCRYPT=
   else  
#     CC_OPTS_ESP=-munix=98 -march=2.0 -D_XOPEN_UNIX -D_XOPEN_SOURCE -D_XOPEN_SOURCE_EXTENDED -D_INCLUDE__STDC_A1_SOURCE -D_INCLUDE_XOPEN_SOURCE_500 _INCLUDE__STDC__
      CC_OPTS_ESP=-DATR_USED=__attribute__\(\(used\)\)
      LCRYPT=-lstdc++
   endif
else
   ifeq ($(PLTFRM_DESA),win)
      CC=cl
      SAT_IPATH:=$(shell cygpath -m $(SAT_IPATH))
      LNK_OPTS_PLT_LIB=/MD
      # para VC2005 se agrega la opcion /EHsc
      CC_OPTS_REL=/W4 /O2 /MD /EHsc /D "_WIN32" /D "NDEBUG" /D "_MBCS" /D "ATR_USED= " /c
      CC_OPTS_DBG=/W4 /Od /MDd /EHsc /D "_WIN32" /D "_DEBUG" /D "_MBCS" /D "ATR_USED= " /D "_GSI_WINDOWS" /GZ
   else
      CC=g++
      #CC_OPTS_REL=-Wall -O2
      CC_OPTS_REL=$(CC_OPTS_ESP) -Wall -Wformat=2 -Wextra -Wunused -Wfloat-equal -Wcast-align -DATR_USED=__attribute__\(\(used\)\) -D__STDC_FORMAT_MACROS -D__STDC_CONSTANT_MACROS
      CC_OPTS_DBG=$(CC_OPTS_ESP) -Wall -Wformat=2 -Wextra -Wunused -Wfloat-equal -Wcast-align -DATR_USED=__attribute__\(\(used\)\) -g3 -fno-inline -O0 -DDBG -D__STDC_FORMAT_MACROS -D__STDC_CONSTANT_MACROS $(DEF_DBGNS)
      LNK_OPTS_PLT_EXE=-rdynamic
      LNK_OPTS_PLT_LIB=-shared
   endif
endif


ifdef DBG 
   OBJS=./$(PLTFRM_DESA)/obj_dbg$(SUFNS)
   CC_OPTS_BAS=$(CC_OPTS_DBG)
   LNK_OPTS_EXE=$(CC_OPTS_DBG) $(LNK_OPTS_PLT_EXE)
   LNK_OPTS_LIB=$(CC_OPTS_DBG) $(LNK_OPTS_PLT_LIB)
   SUFDBG=_dbg
else
   OBJS=./$(PLTFRM_DESA)/obj
   CC_OPTS_BAS=$(CC_OPTS_REL)
   LNK_OPTS_EXE=$(CC_OPTS_REL) $(LNK_OPTS_PLT_EXE)
   LNK_OPTS_LIB=$(CC_OPTS_REL) $(LNK_OPTS_PLT_LIB)
endif

CC_OPTS=$(CPARM)c $(CC_OPTS_BAS) $(CPARM)fPIC $(CPARM)D$(PLTFRM_DESA) $(CPARM)DORACLE=$(ES_ORACLE) $(INCL) $(IFX_DEFS) $(ORA_DEFS) $(CPARM)$(GENOBJ)$@ $<


DEP_OBJS_TMP=$(addsuffix .$(EXT_OBJ), $(MODULOS))
DEP_OBJS=$(addprefix $(OBJS)/, $(DEP_OBJS_TMP))

ifeq ($(ES_LIB),S)
   NOM_LIB_STA=$(APL)_E$(OPC_THREAD)$(SUFDBG)$(SUFNS).$(EXT_STA)
   LIB_STA=./$(PLTFRM_DESA)/bin/$(NOM_LIB_STA)
   NOM_LIB_DIN=$(APL)_D$(OPC_THREAD)$(SUFDBG)$(SUFNS).$(EXT_DIN)
   ifneq ($(PLTFRM_DESA),win)
      LIB_DIN=./$(PLTFRM_DESA)/bin/$(NOM_LIB_DIN)
   endif
else
   APLICACION=$(APL)$(SUFDBG)$(SUFNS)
endif

DLIBS_STA_TMP=$(addsuffix _E$(OPC_THREAD)$(SUFDBG).$(EXT_STA), $(LIBS_STA))
DLIBS_STA=$(addprefix $(SAT_LPATH)/, $(DLIBS_STA_TMP))

DLIBS_DIN_TMP=$(addsuffix _E$(OPC_THREAD)$(SUFDBG).$(EXT_DIN), $(LIBS_DIN))
DLIBS_DIN=$(addprefix $(SAT_LPATH)/, $(DLIBS_DIN_TMP))

################################################################################
################################################################################
# EJECUTABLE #
ifeq ($(ES_LIB),N)

./$(PLTFRM_DESA)/bin/$(APLICACION): ./$(PLTFRM_DESA) $(DEP_OBJS) makefile
	@echo " "; echo "****** Creando aplicacion: $@ ******" 
	$(CC) $(LNK_OPTS_EXE) $(CPARM)o $@ $(DEP_OBJS) $(OBJS_3OS) $(DLIBS_DIN) $(LIBS_FDIN) $(LIBD_SSL) $(LIBD_ZLIB) $(LIBD_MINIZIP) $(IFX_LIBD) $(ORA_LIBD) $(DLIBS_STA) $(LIBS_FSTA) $(IFX_LIBS) $(ORA_LIBS) $(LIBS_SSL) $(LIBS_MINIZIP) $(LIBS_ZLIB) $(CPARM)lm $(CPARM)ldl $(LCRYPT) $(CPARM)lnsl

limpia:
	rm -f $(DEP_OBJS) ./$(PLTFRM_DESA)/bin/$(APLICACION)

copia:
	cp ./$(PLTFRM_DESA)/bin/$(APLICACION) $(SAT_BPATH)
	@echo `chmod 770 $(SAT_BPATH)/$(APLICACION) 2> /dev/null`
	@if [ -n "$(LIB_HEADERS)" ]; then cp $(LIB_HEADERS) $(SAT_IPATH)/;echo `chmod 660 $(SAT_IPATH)/$(LIB_HEADERS)) 2> /dev/null`; fi

else 
# LIBRERIAS #

libreria: ./$(PLTFRM_DESA) $(LIB_STA) $(LIB_DIN) makefile
	@echo " "; echo "***** Bibliotecas  creadas *****"

$(LIB_STA): $(DEP_OBJS) makefile
	@echo " "; echo "****** Creando biblioteca: $@ ******";
	$(LIB) $(LOUT)$@ $(DEP_OBJS) $(DLIBS_STA) $(LIBS_FSTA)

$(LIB_DIN): $(DEP_OBJS) makefile
	@echo " "; echo "****** Creando biblioteca: $@ ******" 
	$(CC) $(LNK_OPTS_LIB) $(CPARM)o $@ $(DEP_OBJS) $(DLIBS_DIN) $(LIBS_FDIN) $(DLIBS_STA) $(LIBS_FSTA)

limpia:
	@echo 
	rm -f $(DEP_OBJS) $(LIB_STA) $(LIB_DIN)
	@echo	

copia:
	cp $(LIB_STA)  $(SAT_LPATH)/
	@echo `chmod 660 $(SAT_LPATH)/$(NOM_LIB_STA) 2> /dev/null` 
	cp $(LIB_DIN)  $(SAT_LPATH)/
	@echo `chmod 770 $(SAT_LPATH)/$(NOM_LIB_DIN) 2> /dev/null` 
	@if [ -n "$(LIB_HEADERS)" ]; then cp $(LIB_HEADERS) $(SAT_IPATH)/; echo `chmod 660 $(SAT_IPATH)/$(LIB_HEADERS) 2> /dev/null`; fi

endif

limpia_all:
	make limpia
	make DBG=1 limpia

all:
	make
	make DBG=1

copia_all:
	make copia
	make DBG=1 copia

$(DEP_OBJS): $(OBJS)/%.$(EXT_OBJ): %.cpp makefile
	@echo " "; echo "****** Creando $@" 
	$(CC) $(CC_OPTS) 

./$(PLTFRM_DESA):
	@$(REP_DS)/Plantillas/mkdirs.sh $(PLTFRM_DESA)

