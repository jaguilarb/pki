package  mx.gob.sat.excepciones;


public  class ValidacionEdoException extends Exception
{

   /**
   *  Esta excepci�n es lanzada cuando el estado proporcionado no es v�lido, valores permitidos  A: Activo R: Revocado C: Caduco X: Cualquier estado..
   */
 
   public ValidacionEdoException()
   {
      super("El estado proporcionado no es v�lido, valores permitidos  A: Activo R: Revocado C: Caduco X: Cualquier estado.");
   }

} 
