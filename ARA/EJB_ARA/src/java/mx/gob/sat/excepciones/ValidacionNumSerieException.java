package  mx.gob.sat.excepciones;


public  class ValidacionNumSerieException extends Exception
{

   /**
   *  Esta excepci�n es lanzada cuando el n�mero de serie proporcionado no es n�mero o no es un n�mero de serie v�lido. 
   **/
   
   public ValidacionNumSerieException()
   {
      super("El n�mero de serie proporcionado no es n�mero o no es un n�mero de serie v�lido.");
   }

} 
