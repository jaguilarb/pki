package  mx.gob.sat.excepciones;


public  class ValidacionNumMaxException extends Exception
{
   /**
   *  Esta excepci�n es lanzada cuando el dato proporcionado para el n�mero m�ximo de certificados  no es un n�mero o excede  de numero m�ximo permitido [1-99].
   **/
   public ValidacionNumMaxException()
   {
      super("El dato proporcionado no es un n�mero o excede  de numero m�ximo permitido [1-99].");
   }

} 
