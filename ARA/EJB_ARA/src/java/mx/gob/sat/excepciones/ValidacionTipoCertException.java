package  mx.gob.sat.excepciones;


public  class ValidacionTipoCertException extends Exception
{
   /**
   *Esta excepci�n es lanzada cuando el tipo de certificado no es v�lido, valores permitidos  0:FIEL y Sello Digital, 1:FIEL, 2:Sello Digital.
   */
   public ValidacionTipoCertException()
   {
      super("El tipo proporcionado no es v�lido, valores permitidos  0:FIEL y Sello Digital, 1:FIEL, 2:Sello Digital.");
   }

} 
