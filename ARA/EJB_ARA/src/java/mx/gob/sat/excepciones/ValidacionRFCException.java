package  mx.gob.sat.excepciones;


public  class ValidacionRFCException extends Exception
{
  
   /**
     *Esta excepci�n es lanzada cuando el RFC no tiene la estructura correcta. 
   */ 
   public ValidacionRFCException()
   {
      super("El RFC poporcionado no tiene la estructura correcta.");
   }

} 
