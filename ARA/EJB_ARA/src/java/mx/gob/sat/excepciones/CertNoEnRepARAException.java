package mx.gob.sat.excepciones;

public class CertNoEnRepARAException extends Exception
{

   /**
   *  Esta excepci�n es lanzada cuando el certificado no se encuentra en repositorio de certificados de la ARA.
   */
 
   public CertNoEnRepARAException()
   {
      super("El certificado no se encuentra en el repositorio.");
   }
}
