package mx.gob.sat.excepciones;

public class CertNoDispARAException extends Exception
{

   /**
   *  Esta excepci�n es lanzada cuando el certificado no est� disponible en la ARA.
   */
   public CertNoDispARAException()
   {
      super("Certificado no disponible");
   }


}
