package mx.gob.sat.excepciones;

public class CertNoRegARAException extends Exception
{

   /**
   *  Esta excepci�n es lanzada cuando el certificado no se encuentra registrado en la BD de la ARA.
   */
 

   public CertNoRegARAException()
   {
      super("Certificado no registrado en la base de datos");
   }
}
