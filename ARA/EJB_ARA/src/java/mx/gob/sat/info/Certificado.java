/*

* Todos los Derechos Reservados 2013 SAT.

* Servicio de Administracion Tributaria (SAT).

*

* Este software contiene informacion propiedad exclusiva del SAT considerada

* Confidencial. Queda totalmente prohibido su uso o divulgacion en forma

* parcial o total.

*/

package  mx.gob.sat.info;


import java.io.ByteArrayOutputStream;

/**
 * Clase que contiene los datos del certificado solicitado.
*/



public class Certificado
{

   private String estado;
   private int    tipo;
   private String vigenciaIni;
   private String vigenciaFin;
   private byte[] certificado;


   public Certificado()
   {
      estado = "SE";
      tipo   = 0;
   
   }

   /**
      * Este m�todo asigna el estado del certitifado A (Activo), R (Revocado)  o C (Caduco).
   */
 

   public void  setEstado(String estado)
   {
      this.estado = estado;
   }
   
    /**
      * Este m�todo asigna el Tipo del certitifado 1(FIEL), 2(Sello Digital).
    */
   
   public void setTipo(int tipo)
   {
      this.tipo = tipo;
   }
   
   /**
      * Este m�todo asigna la vigencia inicial del certificado, en formato AAAA-MM-DD HH:MM:SS
   */

   public void setVigenciaIni(String vigenciaIni)
   {
      this.vigenciaIni =  vigenciaIni; 
   }
   /**
      * Este m�todo asgina la vigencia final del certificado, en formato AAAA-MM-DD HH:MM:SS
   */

   public void setVigenciaFin(String vigenciaFin)
   {
      this.vigenciaFin = vigenciaFin;
   }
   

    /**
      * Este m�todo obtiene el estado del certitifado A (Activo), R (Revocado)  o C (Caduco).
    */
   public String getEstado()
   {
      return estado;
   }
    /**
      * Este m�todo obtiene el Tipo del certitifado 1(FIEL), 2(Sello Digital).
    */
   public int getTipo()
   {
      return tipo;
   }
   /**
      * Este m�todo obtiene la vigencia inicial del certificado, en formato AAAA-MM-DD HH:MM:SS 
   */
 
   public String getVigenciaIni()
   {
      return vigenciaIni;
   }

   /**
     * Este m�todo obtiene la vigencia final del certificado, en formato AAAA-MM-DD HH:MM:SS
   */
   public String  getVigenciaFin()
   {
      return vigenciaFin;
   }

   /**
   *  Este m�todo obtiene los bytes del certificado.
   */
   public byte[] getCertificado()
   {
      return certificado;
   }

   /**
      * Este m�todo asinga un arreglo de bytes.
   */


   public void setCertificado(byte[] certificado)
   {
      this.certificado = certificado;
   }
}
