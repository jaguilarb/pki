/*

*Todos los Derechos Reservados2013 SAT.

*Servicio de Administracion Tributaria (SAT).

*

*Este software contiene informacion propiedad exclusiva del SAT considerada

*Confidencial. Queda totalmente prohibido su uso o divulgacion en forma

*parcial o total.

*/

package mx.gob.sat;

import javax.ejb.Local;

/**
 *
 * @author Euridice
 */
@Local
public interface EJB_ARALocal extends EJB_ARARemLoc
{
    
}
