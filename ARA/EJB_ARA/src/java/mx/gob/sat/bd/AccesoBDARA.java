/*

* Todos los Derechos Reservados 2013 SAT.

* Servicio de Administracion Tributaria (SAT).

*

* Este software contiene informacion propiedad exclusiva del SAT considerada

* Confidencial. Queda totalmente prohibido su uso o divulgacion en forma

* parcial o total.

*/

package  mx.gob.sat.bd;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;
import javax.sql.DataSource;
import java.util.Properties;
import java.util.Date;
import java.text.SimpleDateFormat;
import mx.gob.sat.info.Certificado;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.MissingResourceException;
import java.sql.DriverManager;

import gob.sat.sgi.util.Sgi_ProtegePwd;

public class AccesoBDARA 
{

   //static   org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(AccesoBDARA.class.getName());
   private  Connection conn = null;
   private  String cadena   = null;
   private  String usuario  = null;
   private  String pwd 	    = null;
   private  String role     = null;
    

    public AccesoBDARA() {

        try {
            System.out.println("*** Buscando EJB_ARA.properties ");
            ResourceBundle conf = ResourceBundle.getBundle("EJB_ARA");
            cadena = conf.getString("cadena");
            usuario = conf.getString("usuario");
            pwd = conf.getString("password");
            role = conf.getString("role");

            Sgi_ProtegePwd sgiPPwd = new Sgi_ProtegePwd();
			sgiPPwd.desencripta(conf.getString("password"));
            pwd = sgiPPwd.msgout;

            System.out.println("cadena que se va a usar " + cadena);
        } catch (MissingResourceException e) {
            System.out.println("*** No se encontró EJB_ARA.properties");
        }
    }


   public void desconexionBD()
   {
      try 
      {
         if (conn != null)
         {

           conn.close();
			
            //log.info("Desconexion BD  ARA "+conn.isClosed());
			System.out.println("Desconexion BD  ARA "+conn.isClosed());
         }
         else  
		 {
			//log.info("No se encuentra conectada ");
			System.out.println("No se encuentra conectada ");
		 }
      } catch (SQLException ex){
		  try{
            if (ex.getMessage().compareTo("You cannot commit during a managed transaction!") == 0)
            {
               conn.close();
               //log.info("Desconexion BD ARA después del commit "+conn.isClosed());
			   System.out.println("Desconexion BD ARA después del commit "+conn.isClosed());
            }
            //log.debug(ex.getMessage());
			System.out.println(ex.getMessage());
			}catch(SQLException ex1){
			   ex.getMessage();
			}
      }
    }

    public  boolean  conexionBD() 
    {
		System.out.println("Entro a AccesoBDARA.conexionBD() ");
        boolean conectado  = false;
		
        try
        {
          // log.info("Conexion BD ");
		  System.out.println("Conexion BD ARA");
          Class.forName("com.informix.jdbc.IfxDriver");
		  
		  /*
      
           InitialContext initialContext = new InitialContext();
		   System.out.println("Despues de obtener initial context; se usará el DS:" + datasource);	   	   
           DataSource  ds = (DataSource)initialContext.lookup(datasource); 
		   System.out.println("Despues de obtener DS en AccesoBDARA.conexionBD()");*/
           if (conn == null) 
           {
				  System.out.println("Entro al IF donde se conecta");
				  //conn = ds.getConnection();
				  conn = DriverManager.getConnection(cadena, usuario,pwd);
				  PreparedStatement prepareStatement = conn.prepareStatement("set role "+ role +";");
				  boolean setRole = prepareStatement.execute();
				  prepareStatement.close();
				   
				 System.out.println("Se conexto exitosamente a la base de datos ");
           }
           else {
		   //log.info( "Ya estaba abierta la conexion.");
		   System.out.println("Ya estaba abierta la conexion.");
			}		   
           //log.info( "Conexion con el datasource");
		   System.out.println("Conexion con el datasource");
           conectado = true;
		   
            
        } /*catch (NamingException ex) {
           //log.error("Conexion BD " + ex.getMessage());
		   System.out.println("Conexion BD " + ex.getMessage());
		   ex.printStackTrace();
        } */catch (SQLException ex) {
           //log.error("Conexion BD " + ex.getMessage());
		   System.out.println("Conexion BD " + ex.getMessage());
		   ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
           //log.error("Conexion BD " + ex.getMessage());
		   System.out.println("Conexion BD " + ex.getMessage());
		   ex.printStackTrace();
        }
        return conectado; 
    }


   public  Certificado  consultaNumserie(String numSerie)   
   {
        PreparedStatement sReg =  null;
        ResultSet resultado =  null;
        String sConsulta = null; 
        Certificado cert = null;
        
        try 
        {
           
            //sConsulta = "select \"EDO_CER\",\"TIPCER_CVE\",\"VIG_INI\",\"VIG_FIN\" from \"CERTIFICADO\" where \"NO_SERIE\"=?";
			sConsulta = "select EDO_CER,TIPCER_CVE,VIG_INI,VIG_FIN from CERTIFICADO where NO_SERIE=?";
                                

            sReg =  conn.prepareStatement(sConsulta);
            sReg.setString(1, numSerie);
            resultado = sReg.executeQuery();
            if ( resultado.next())
            {
                int reg =resultado.getRow();
                if( reg != 0)
                {
                    cert =  new Certificado();
                    cert.setEstado(resultado.getString("EDO_CER"));
                    cert.setTipo(resultado.getInt("TIPCER_CVE"));
                    cert.setVigenciaIni(fecha(resultado.getDate("VIG_INI")));
                    cert.setVigenciaFin(fecha(resultado.getDate("VIG_FIN")));
                }
                else
                   // log.info("consultaNumserie: No se encontro ningun registro" + reg );
					 System.out.println("consultaNumserie: No se encontro ningun registro" + reg );
             }
			 conn.commit();
             sReg.close();
             resultado.close();
             sReg = null;
             resultado = null;
            
        } catch (SQLException ex) {
           // log.info("consultaNumserie: " +ex.getMessage());
			System.out.println("consultaNumserie: " +ex.getMessage());
        }
       return cert;
   }


   public  String consultaRFC(String rfc, boolean certActivo,boolean fiel)
   {
        PreparedStatement sReg =  null;
        ResultSet resultado =  null;
        String sConsulta = null;
        String sNumSerie =  null;
		System.out.println("Antes de hacer consulta");
        try
        {
            if (certActivo)
            {
               if (fiel)
               {
                  //sConsulta = "select \"NO_SERIE\" from \"CERTIFICADO\" where \"RFC\"=? and \"TIPCER_CVE\"=1 and \"EDO_CER\" ='A' ";
				  sConsulta = "select NO_SERIE from CERTIFICADO where RFC=? and TIPCER_CVE=1 and EDO_CER ='A' ";
               }
               else
               {
                  //sConsulta = "select \"NO_SERIE\" from \"CERTIFICADO\" where \"RFC\"=? and \"TIPCER_CVE\"=2 and \"EDO_CER\" ='A' ";
				  sConsulta = "select NO_SERIE from CERTIFICADO where RFC=? and TIPCER_CVE=2 and EDO_CER ='A' ";	  
               }
            }
            else
            {
			    sConsulta = "SELECT FIRST 1 NO_SERIE from CERTIFICADO where  RFC =?";
			    System.out.println("Consulta: "+ sConsulta);
            }
	
            sReg =  conn.prepareStatement(sConsulta);          
			sReg.setString(1, rfc);
            resultado = sReg.executeQuery();
			
            if ( resultado.next())
            {
                int reg =resultado.getRow();
                if( reg != 0)
                {
                     sNumSerie =resultado.getString("NO_SERIE");
                }
                else
                    //log.info("consultaRFC: No se encontró ningún registro " + reg );
					System.out.println("consultaRFC: No se encontró ningún registro " + reg );
             }
             sReg.close();
             resultado.close();
             sReg = null;
             resultado = null;

        } catch (SQLException ex) {
			
		 
            //log.info("consultaRFC : "+ex.getMessage());
			System.out.println("consultaRFC : "+ex.getMessage());
			ex.printStackTrace();
        }

       return sNumSerie;
 

   }
   
   public List<String>  consultaLista(String rfc, int tipo, String estado, int max )
   {
      
        PreparedStatement sReg =  null;
        ResultSet resultado =  null;
        String sConsulta = null;
        String sNumSerie =  "";
        List<String>  numSeries = null;

        try
        {

            //sConsulta = "select \"NO_SERIE\" from \"CERTIFICADO\" where \"RFC\"=? ";
			sConsulta = "select FIRST "+" "+ max +" NO_SERIE from CERTIFICADO where RFC=? ";
      
            if(tipo!=0){
               sConsulta+=" AND TIPCER_CVE=?" ;
			 }  
            else{
               sConsulta+=" AND TIPCER_CVE IN(1,2) ";
			}  

            if(estado.compareTo("X") != 0)
               sConsulta+=" AND EDO_CER ='"+estado+"' ORDER BY VIG_INI DESC";
            //sConsulta+=" AND ROWNUM <= "+max+ " ORDER BY VIG_INI DESC ";

            //log.info(sConsulta);
			System.out.println(sConsulta);
            sReg =  conn.prepareStatement(sConsulta);
   
            sReg.setString(1, rfc);
            if(tipo!=0)
               sReg.setInt(2,tipo );
             
            numSeries =  new ArrayList<String>();
            resultado = sReg.executeQuery();
            while ( resultado.next())
            {
                int reg =resultado.getRow();
                if( reg != 0)
                {
                     sNumSerie =resultado.getString("NO_SERIE");
                     numSeries.add(sNumSerie);
                     //log.info("+" + sNumSerie);
					 System.out.println("+" + sNumSerie);
                }
                else
                   // log.info("consultaRFC: No se encontro ningun registro " + reg );
					System.out.println("consultaRFC: No se encontro ningun registro " + reg );
             }
             sReg.close();
             resultado.close();
             sReg = null;
             resultado = null;

        } catch (SQLException ex) {
            //log.info("consultaRFC : "+ex.getMessage());
			System.out.println("consultaRFC : "+ex.getMessage());
        }

       return numSeries;



   }
   private String fecha(Date dfecha)
   {
      SimpleDateFormat  sDffecha = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
      String fecha  = sDffecha.format(dfecha);
      return fecha;

   }




}
