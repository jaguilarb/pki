/*

*Todos los Derechos Reservados2013 SAT.

*Servicio de Administracion Tributaria (SAT).

*

*Este software contiene informacion propiedad exclusiva del SAT considerada

*Confidencial. Queda totalmente prohibido su uso o divulgacion en forma

*parcial o total.

*/


package mx.gob.sat;

import javax.ejb.Remote;

/**
 *
 * @author Euridice
 */
@Remote

public interface EJB_ARARem extends EJB_ARARemLoc
{
    
}
