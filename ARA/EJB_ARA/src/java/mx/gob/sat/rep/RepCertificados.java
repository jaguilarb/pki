/*

* Todos los Derechos Reservados 2013 SAT.

* Servicio de Administracion Tributaria (SAT).

*

* Este software contiene informacion propiedad exclusiva del SAT considerada

* Confidencial. Queda totalmente prohibido su uso o divulgacion en forma

* parcial o total.

*/

package  mx.gob.sat.rec;

import java.util.Properties;
import java.util.ResourceBundle;
import java.util.MissingResourceException;

import java.io.*;
import java.net.*;


public class RepCertificados
{
   private  String ruta = "";
   private  String nombreCert = "";
   static   org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(RepCertificados.class.getName());
   private  Properties props = null;
   private  String serverFTP = "";


   private String rutaBase()
   { 
       String rutaBase = null;
       String configuracion = "EJB_ARA.properties";
   
       try {
            System.out.println("*** Buscando EJB_ARA.properties ");
            ResourceBundle conf = ResourceBundle.getBundle("EJB_ARA");
			
			rutaBase = conf.getString("rutaBase");
			System.out.println("rutaBase que se va a usar "+ rutaBase);
        } catch (MissingResourceException e) {
            System.out.println("*** No se encontr� EJB_ARA.properties");
        }
      return rutaBase;
   }

   public RepCertificados( String numSerie) 
   {
      String path1 = numSerie.substring(0,6);
      String path2 = numSerie.substring(6,12);
      String path3 = numSerie.substring(12,14);
      String path4 = numSerie.substring(14,16);
      String path5 = numSerie.substring(16,18);

      String rutaBase = rutaBase();

      this.ruta = rutaBase  +path1 + "/" + path2 + "/" + path3 + "/" + path4 + "/" + path5 +"/"+numSerie + ".cer";
	  System.out.println("Ruta del certificado "+ this.ruta);
   }

   public String getuRuta()
   {
      return this.ruta + "/" + this.nombreCert;
   }

   public  byte[] getCertificado()
   {
      byte []bCert = null;
      try
      {
         log.info("Ruta de certificado " + this.ruta);
         FileInputStream fCert =  new FileInputStream(this.ruta);

         int tam = fCert.available();
         log.info("Tama�o del certificado: " + tam);


         bCert =  new byte[tam];

         fCert.read(bCert);
         fCert.close();

         log.info("Fin de recuperacion ");

      }
      catch (Exception ex) {
         log.error("No se pudo cargar el certificado.");
      }
      return bCert;

   }
   
   	public byte[] getCertificadoFTP(){
	
	
	System.out.println("getCertificadoFTP");
	byte []bCert = null;
	
	
	try {
            URL certificadoUrl = new URL(this.ruta);
			System.out.println("URL para certificado: " + certificadoUrl);
            InputStreamReader isr = new InputStreamReader(certificadoUrl.openStream());
            BufferedReader in = new BufferedReader(isr);

            StringBuffer sb = new StringBuffer();
            String inputLine;
            boolean isFirst = true;
            
            while ((inputLine = in.readLine()) != null){
                sb.append(inputLine+"\r\n");
            }

			bCert = String.valueOf(sb).getBytes();
        }
        catch (MalformedURLException mue) {
            mue.printStackTrace();
        }
        catch (IOException ioe) {
            ioe.printStackTrace();
        }

	
	return bCert;
	}
   
/*
   public  ByteArrayOutputStream getCertificado()
   {
      ByteArrayOutputStream aCert = null;
      try
      {
         log.info("Ruta de certificado " + this.ruta);
         FileInputStream fCert =  new FileInputStream(this.ruta);
         
         int tam = fCert.available();
         log.info("Tama�o del certificado: " + tam);

         aCert  = new ByteArrayOutputStream(tam);

         byte []bCert =  new byte[tam];
         
         fCert.read(bCert);
         aCert.write(bCert,0,tam);
         log.info("Tama�o del buffer " + aCert.size());
         
         fCert.close();
         aCert.close();

         log.info("Fin de recuperacion ");
         
      }
      catch (Exception ex) {
         log.error("No se pudo cargar el certificado.");
      }
      return aCert;

   }*/




}
