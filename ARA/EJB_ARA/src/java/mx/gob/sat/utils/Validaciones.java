/*

*Todos los Derechos Reservados2013 SAT.

*Servicio de Administracion Tributaria (SAT).

*

*Este software contiene informacion propiedad exclusiva del SAT considerada

*Confidencial. Queda totalmente prohibido su uso o divulgacion en forma

*parcial o total.

*/


package  mx.gob.sat.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validaciones
{

   public static boolean isRFC(String sRFC) 
   {

        String rfcMoral = "(([A-Za-z]|[�-�]|&){3})(([0-9]{1})([0-9]{1})((0+[1-9]{1})|(1+[0-2]{1}))([0-9]{1})([0-9]{1}))([A-Za-z]|[0-9]){2}([0-9]{1}|(A|a){1})";
        String rfcFisica = "(([A-Za-z]|[�-�]|&){4})(([0-9]{1})([0-9]{1})((0+[1-9]{1})|(1+[0-2]{1}))([0-9]{1})([0-9]{1}))([A-Za-z]|[0-9]){2}([0-9]{1}|(A|a){1})";

        Pattern pat = null;
        Matcher mat = null;

        if (sRFC == null) {
            return false;
        }
        if (sRFC.length() == 12) {
            pat = Pattern.compile(rfcMoral);
        } else if (sRFC.length() == 13) {
            pat = Pattern.compile(rfcFisica);
        }
        else
           return false;

        mat = pat.matcher(sRFC);
        if (mat.matches()) {
            return true;
        }

        return false;
    }
    
    public static boolean isNumero(int num)
    {
         boolean esNum =false;
         String numero = Integer.toString(num);

         if ( isNum(numero,false) )
         {
            if(num>0 &&num<100)
               esNum= true; 
         }
         return esNum;
    }
    private static  boolean isNum(String numero,boolean numSerie)
    {
        Pattern pat = null;
        Matcher mat = null;

       if (numSerie)
         pat = Pattern.compile("^[0-9]*$");
       else
         pat = Pattern.compile("^[1-9][0-9]*$");
       
      mat = pat.matcher(numero);
      if (mat.matches())
          return true;
      else
         return false;
    }


    public static boolean isTipCertValido(int tipo)
    {
         boolean esTipo = false;
         if ((tipo == 1) || (tipo == 2) || (tipo == 0) ) 
            esTipo =true;
         return esTipo; 
    }

    public static boolean isEstado(String estado)
    {
        boolean esEstado = false;

        if ((estado.compareTo("A") == 0) || (estado.compareTo("R") == 0) || (estado.compareTo("C") == 0) || (estado.compareTo("X") == 0))
            esEstado= true;

        return esEstado;
    }
   
    public static boolean isNumSerie(String numSerie)
    {
        return  ((numSerie.length() == 20) && isNum(numSerie,true))?true:false;

    } 
    
}
