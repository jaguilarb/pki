/* 

* Todos los Derechos Reservados 2013 SAT. 

* Servicio de Administracion Tributaria (SAT). 

* 

* Este software contiene informacion propiedad exclusiva del SAT considerada 

* Confidencial. Queda totalmente prohibido su uso o divulgacion en forma 

* parcial o total. 

*/ 
package mx.gob.sat;


import java.io.ByteArrayOutputStream;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.jws.WebService;
import javax.jws.WebMethod;
import org.jboss.wsf.spi.annotation.WebContext;   
import mx.gob.sat.utils.Validaciones;
import mx.gob.sat.info.Certificado;
import mx.gob.sat.excepciones.*;
import mx.gob.sat.rec.RepCertificados;
import mx.gob.sat.bd.AccesoBDARA;
import java.util.List;




/**
 *
 * @author Euridice
 */
@Stateless
@WebService(serviceName = "ARAService")
@Remote(EJB_ARARem.class)
public class EJB_ARA implements EJB_ARALocal,EJB_ARARem
{


    /**
      * Este m�todo sirve para consultar el n�mero de serie del �ltimo 
      * certificado activo que tenga el RFC consultado, puede ser de Sello Digital o de FIEL.
      * @param  rfc  RFC del que se quiere obtener el n�mero de serie.
      * @return      El n�mero de serie del certificado.
    */

    @WebMethod
    public String  consultaRFC( String rfc) throws ValidacionRFCException,CertNoRegARAException {
      
         System.out.println("ConsultaRFC ["+rfc+"]");
         if (!Validaciones.isRFC(rfc.trim())) 
            throw new ValidacionRFCException();
         AccesoBDARA ara =  new  AccesoBDARA();
         String numSerie = null;
		
		System.out.println("ConsultaRFC ["+rfc+"]");
    
try{
	  if ( ara.conexionBD())
         {
			
            numSerie = ara.consultaRFC( rfc,false ,true);
			System.out.println("termino antes de desconexion en EJB_ARA");
			System.out.println("Numero de serie: " + numSerie);
            ara.desconexionBD();
            if (numSerie == null)
               throw new CertNoRegARAException();  
         }
		 
		 } catch (Exception ex) {
		   System.out.println("!**!**!--- PRUEBA prueba consulta RFC exception --- " + ex.getMessage());
        }
		 
         return numSerie;
    }

    /**
      * Este m�todo sirve para obtener una lista de n�meros de serie separados 
      * por pipes(|) seg�n los paramatetros dados.
      * @param  rfc     RFC a consultar.
      * @param  estado  Estado del certificado a consultar, A(Activo),R(Revocado),C(C�duco).
      * @param  tipo    Tipo de certificado a consultar, 1(FIEL),2(Sello Digital).
      * @param  numMax  N�mero m�ximo de numeros de serie a regresar, [1-99].
      * @return         Los n�meros de serie de los certificados separados por pipes(|) .
      * @exception CertNoRegARAException,ValidacionEdoException,ValidacionNumMaxException,ValidacionNumSerieException,ValidacionRFCException,ValidacionTipoCertException,CertNoDispARAException
    */


    @WebMethod
    public List<String> consultaRFCLista(String rfc, 
                                 String estado, 
                                 int tipo, 
                                 int numMax) 
                        throws CertNoRegARAException,ValidacionEdoException,ValidacionNumMaxException,ValidacionNumSerieException,ValidacionRFCException,ValidacionTipoCertException,CertNoDispARAException {
         
         System.out.println("ConsultaLista RFC["+ rfc +"] Estado["+ estado + "] Tipo["+ tipo +"] NumMax["+ numMax+"]");

         List<String> listaNumSerie = null;
        
         if (!Validaciones.isRFC(rfc.trim()))
            throw new ValidacionRFCException(); 

         if (!Validaciones.isEstado(estado.trim().toUpperCase()))
            throw new ValidacionEdoException();
         
         if (!Validaciones.isTipCertValido(tipo))
            throw new ValidacionTipoCertException();

         if (!Validaciones.isNumero(numMax))
            throw new ValidacionNumMaxException();

         AccesoBDARA ara =  new  AccesoBDARA();

         if ( ara.conexionBD())
         {
            listaNumSerie = ara.consultaLista( rfc,tipo,estado.toUpperCase(),numMax );
            System.out.println("lista numSerie [" +listaNumSerie+"]" );
            ara.desconexionBD();
         }
         return listaNumSerie; 

    }

    /**
      * Este m�todo sirve para consultar si un RFC cuenta con un certificado de FIEL activo
      * @param  rfc     RFC a consultar.
      * @return         True si cuenta con certificado activo de FIEL  o  False si no.
      * @exception   ValidacionRFCException
    */

    @WebMethod
    public boolean consultaTieneFIEL(String rfc) throws ValidacionRFCException,CertNoDispARAException {
   
         System.out.println("ConsultaTieneFIEL [" + rfc +"]");

         if (!Validaciones.isRFC(rfc.trim()))
            throw new ValidacionRFCException();


         AccesoBDARA ara =  new  AccesoBDARA();
         boolean tieneCert = false;

         if ( ara.conexionBD())
         {
            String numSerie = ara.consultaRFC( rfc,true,true );
            ara.desconexionBD();
            System.out.println("numSerie [" +numSerie+"]" );

            if (numSerie == null)
               throw new  CertNoDispARAException();
            if ( numSerie.compareTo("") !=0 ) 
               tieneCert = true;
         }
         return tieneCert;
    }

    /**
      * Este m�todo sirve para consultar si un RFC cuenta con por lo menos un  certificado de Sello Digital activo
      * @param  rfc     RFC a consultar.
      * @return         True si cuenta con por lo menos un certificado activo de Sello Digital  o  False si no.
      * @exception   ValidacionRFCException
    */

    @WebMethod
    public boolean consultaTieneSelloDigital(String rfc) throws ValidacionRFCException,CertNoDispARAException {

         System.out.println("ConsultaTieneSelloDigital [" + rfc +"]");

         if (!Validaciones.isRFC(rfc.trim()))
            throw new ValidacionRFCException();


         AccesoBDARA ara =  new  AccesoBDARA();
         boolean tieneCert = false;

         if ( ara.conexionBD())
         {
            String numSerie = ara.consultaRFC( rfc,true,false );
            ara.desconexionBD();
            System.out.println("numSerie [" +numSerie+"]" );
            if (numSerie == null)
               throw new CertNoDispARAException();   
            if (numSerie.compareTo("") !=0)
               tieneCert = true;
         }
         return tieneCert;
    }
 
  /**
      * Este m�todo sirve para consultar los datos de un certificado seg�n el n�mero de serie proporcionado.
      * @param  numeroSerie     N�mero de Serie del certificado a consultar.
      * @return      Certificado
      * @exception   ValidacionNumSerieException
      * @see Certificado
    */

    @WebMethod
    public Certificado solEdoCDxNS(String numeroSerie)  throws ValidacionNumSerieException,CertNoDispARAException{

         Certificado cert = new Certificado();
         System.out.println("solEdoCD [" + numeroSerie+"]");
         if ( !Validaciones.isNumSerie(numeroSerie.trim()))
            throw new ValidacionNumSerieException();

         AccesoBDARA ara =  new  AccesoBDARA();
         
         if ( ara.conexionBD())
         {
            cert = ara.consultaNumserie(numeroSerie);
            ara.desconexionBD();
            if (cert == null)
               throw new CertNoDispARAException();
         }
         return cert;
    }

   /**
      * Este m�todo sirve para consultar los datos de un certificado seg�n el n�mero de serie proporcionado
      * incluyendo el certificado.
      * @param  numeroSerie     N�mero de Serie del certificado a consultar.
      * @return      Certificado
      * @exception   ValidacionNumSerieException
      * @see Certificado
   */ 
    @WebMethod
    public Certificado solCDxNS( String numeroSerie) throws ValidacionNumSerieException,CertNoDispARAException,CertNoEnRepARAException,CertNoRegARAException 
    {

         Certificado cert = new Certificado();
         System.out.println("solEdoCD [" + numeroSerie+"]");

         if ( !Validaciones.isNumSerie(numeroSerie.trim()))
            throw new ValidacionNumSerieException();

         AccesoBDARA ara =  new  AccesoBDARA();

         if ( ara.conexionBD())
         {
            cert = ara.consultaNumserie(numeroSerie);
            ara.desconexionBD();
            if (cert != null)
            {
               System.out.println("Certificado en bd." );
               RepCertificados rep = new RepCertificados(numeroSerie);
			   System.out.println("**** Antes de crear el array de bytes*****");
               //byte[] cer = rep.getCertificado();
			   byte[] cer = rep.getCertificadoFTP();
               if( cer != null)
               {
                  cert.setCertificado(cer);
                  System.out.println("Certificado en repositorio." );
               }
               else  throw new CertNoEnRepARAException(); 
            }
            else throw new CertNoRegARAException();
         }
         return cert;
    }

}
