/*

*Todos los Derechos Reservados2013 SAT.

*Servicio de Administracion Tributaria (SAT).

*

*Este software contiene informacion propiedad exclusiva del SAT considerada

*Confidencial. Queda totalmente prohibido su uso o divulgacion en forma

*parcial o total.

*/

package mx.gob.sat;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import mx.gob.sat.info.Certificado;
import mx.gob.sat.excepciones.*;
import java.util.List;


/**
 *
 * @author Euridice
 */


@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface EJB_ARARemLoc 
{
    public  String consultaRFC(String rfc) throws ValidacionRFCException,CertNoRegARAException;
    public  List<String> consultaRFCLista(String rfc, String estado, int tipo, int numMax) throws CertNoRegARAException,ValidacionEdoException,ValidacionNumMaxException,ValidacionNumSerieException,ValidacionRFCException,ValidacionTipoCertException,CertNoDispARAException; 
    public  boolean consultaTieneFIEL(String rfc)  throws ValidacionRFCException,CertNoDispARAException;
    public  boolean consultaTieneSelloDigital(String rfc) throws ValidacionRFCException,CertNoDispARAException;
    public  Certificado solEdoCDxNS(String numeroSerie) throws ValidacionNumSerieException,CertNoDispARAException;
    public  Certificado solCDxNS(String numeroSerie) throws ValidacionNumSerieException,CertNoDispARAException,CertNoEnRepARAException,CertNoRegARAException;
     
    
}
