#include <iostream>

#include <string>
//- #include <SgiOpenSSL.h>   //- GHM (070430)
#include <Sgi_OpenSSL.h>
//- #include <MensajesSAT.h>  //- GHM (070430)
#include <Sgi_MsgPKI.h>
#include <Sgi_ARA.h>

using std::cout;
using std::endl;

//*******************************************************************************************
int procesaParametros(int argc, char* argv[])
{
   int oper = argc > 1 ? atoi(argv[1]) : 0;

   if (oper < 1 || oper > 6 || (oper == 6 && argc != 3) || (oper != 6 && argc != 3))
   {
      cout << endl << endl ;
      cout << "Uso: " << endl << "\t" << argv[0] << " 1..4 no_serie" << endl;
      cout <<                    "\t" << argv[0] << " 5    rfc" << endl << endl;
      cout <<                    "\t" << argv[0] << " 6    rfc #tipoCert Estado #cuantos" << endl << endl;
      cout << "\t\t" << "1  Solicita CD" << endl;
      cout << "\t\t" << "2  Solicita CD con fecha inicial de vigencia" << endl;
      cout << "\t\t" << "3  Solicita estado de CD" << endl;
      cout << "\t\t" << "4  Solicita estado de CD con fecha inicial de vigencia" << endl;
      cout << "\t\t" << "5  Solicita datos de CD de FEA por RFC" << endl;
      cout << "\t\t" << "6  Solicita lista de CDs por RFC" << endl << endl;
      return 0;
   }

   return oper;
}
//*******************************************************************************************
int main (int argc, char* argv[])
{
   static uint8 buffer[8 * 1024];
   int lbuffer = sizeof(buffer);
   CSgiARA ara;
   int oper =0 ;

   if ( (oper = procesaParametros(argc, argv)) == 0 )
      return 0;

   if (argc == 3)
      ara.Bitacora.escribePV(BIT_INFO, "Los argumentos le�dos son: (%s, %s, %s)",
                             argv[0], argv[1], argv[2]); 
   else
      ara.Bitacora.escribePV(BIT_INFO, "Los argumentos le�dos son: (%s, %s, %s, %s, %s, %s)",
                             argv[0], argv[1], argv[2], argv[3], argv[4], argv[5]);
   iniciaLibOpenSSL();

   char edo[2], caducidad[20], ultimoCert[21];
   int  tipo, error = 0;
   int lultimoCert = sizeof(ultimoCert);
   
   int res = ara._conecta();
   if (res)
   {
      cout << "Error al conectarse al servicio de la ARA: " << res << endl;
      terminaLibOpenSSL();
      return 0;
   }
   int longDato = strlen(argv[2]);
   switch (atoi(argv[1]))
   {
      case 1:
      {
         error = ara.solCertificado(false, argv[2], longDato, buffer, &lbuffer, &tipo);
         if (!error)
            ara.Bitacora.escribePV(BIT_INFO, "Solicitud de certificado: %s, (tam = %i)", argv[2], lbuffer);
            //cout << "Solicitud de certificado: " << argv[2] << " (tam = " << lbuffer << ")" << endl;
         else
         {
            ara.Bitacora.escribePV(BIT_ERROR, "Solicitud de certificado: %s,  (error = %x)",argv[2], error );
            cout << "ERROR: En la solicitud del certificado: " << argv[2] << " (error = " << std::hex << error << ")" << endl;
         }
         break;
      }
      case 2:
      {
         error = ara.solCertificado(true, argv[2], longDato, buffer, &lbuffer, &tipo);
         if (!error)
            ara.Bitacora.escribePV(BIT_INFO, "Solicitud de certificado con VigInic: %s, (tam = %i)", argv[2], lbuffer);
         else
         {
            ara.Bitacora.escribePV(BIT_ERROR, "Solicitud de certificado con VigInic: %s,  (error = %x)",argv[2], error );
            cout << "ERROR: En la solicitud del certificado con VigInic: " << argv[2] << " (error = " << std::hex << error << ")" << endl;
         }
         break;
      }
      case 3:
      {
         error = ara.verEDOCERT(false, argv[2], edo, caducidad, &tipo);
         if (!error)
            //cout << "Solicitud de estado de certificado: " << argv[1] << " (edo = " << edo << ", cad = " 
            //<< caducidad << ", tipo = " << tipo << ")" << endl;
            ara.Bitacora.escribePV(BIT_INFO, "Solicitud de estado de certificado: %s (edo = %s) ( cad = %s) (tipo=%d)",
                                   argv[2], edo, caducidad, tipo  );
         else
         {
            ara.Bitacora.escribePV(BIT_ERROR, "Solicitud de certificado: %s (error = %x)", argv[2], error);
            cout << "ERROR: En la solicitud del estado del certificado: " << argv[2] << " (error = " << std::hex << error << ")" << endl;
         }
         break;
      }
      case 4:
      {
         error = ara.verEDOCERT(true, argv[2], edo, caducidad, &tipo);
         if (!error)
            ara.Bitacora.escribePV(BIT_INFO, "Solicitud de estado de certificado con vigencia: %s (edo = %s)"
                                   " ( cad = %s) (tipo=%d)", argv[2], edo, caducidad, tipo  );
         else
         {
            ara.Bitacora.escribePV(BIT_ERROR, "Solicitud de certificado con VigInic: %s (error = %x)", argv[2], error);
            cout << "ERROR: En la solicitud del estado del certificado con VigInic: " << argv[2] << " (error = " << std::hex << error << ")" << endl;
         }
         break;
      }
      case 5:
      {
         error = ara.solCertRFC(argv[2], longDato, ultimoCert, &lultimoCert);
         if (!error)
             ara.Bitacora.escribePV(BIT_INFO, "Solicitud de certificado por RFC: %s, ultimo n�mero de Serie %s)",
                                    argv[2], ultimoCert);
         else
         {
            ara.Bitacora.escribePV(BIT_ERROR, "Solicitud de certificado por RFC: %s, �ltimo n�mero de Serie (error = %x)", argv[2], error);
            cout << "ERROR: En la solicitud del �ltimo n�mero de serie del certificado con RFC: " << argv[2] << "(error = " << std::hex << error << ")" << endl;
         }
         break;
      }
      case 6: //rfc #tipoCert Estado #cuantos
      {
         std::string listaCerts;
         error = ara.solListaCertRFC(argv[2], argv[3], argv[4], argv[5], &listaCerts);
         if (!error)
             ara.Bitacora.escribePV(BIT_INFO, "Solicitud de lista de certificados por RFC: %s, lista de no_Serie %s)",
                                    argv[2], listaCerts.c_str() );
         else
         {
            ara.Bitacora.escribePV(BIT_ERROR, "Solicitud de lista de certificados por RFC: %s (error = %x)", argv[2], error);
            cout << "ERROR: En la solicitud de lista de certificados por RFC: " << argv[2] << " (error = " << std::hex << error << ")" << endl;
         }
         break;
      }
      default:
      {
         ara.Bitacora.escribePV(BIT_ERROR, "Se recibi� como par�metro el dato (%s), el cual no es correcto", argv[1]);
         break;
      }
   }
   ara._desconecta();
   terminaLibOpenSSL();

   return 0;
}


