#include <iostream>
#include <string>

#include <Sgi_OpenSSL.h>
#include <Sgi_MsgPKI.h>
#include <Sgi_ARAv2.h>
using std::cout;
using std::endl;

//*******************************************************************************************
bool procesaParametros(int argc, char* argv[])
{
   int oper = argc > 1 ? atoi(argv[1]) : 0;

   if ( oper < 1 || oper > 7 || (oper == 6 && argc != 6) || ((oper == 1||oper == 2||oper == 3||oper == 4) && argc != 3) ||
        ( oper == 7 && argc != 2) )
   {
      cout << endl << endl ;
      cout << "Uso: " << endl << "\t" << argv[0] << " 1..4 no_serie" << endl;
      cout <<                    "\t" << argv[0] << " 5    rfc" << endl << endl;
      cout <<                    "\t" << argv[0] << " 6    rfc Estado(A,R) #tipoCert(1,2) #cuantos(n)" << endl << endl;
      cout << "\t\t" << "1  Solicita CD" << endl;
      cout << "\t\t" << "2  Solicita CD con fecha inicial de vigencia" << endl;
      cout << "\t\t" << "3  Solicita estado de CD" << endl;
      cout << "\t\t" << "4  Solicita estado de CD con fecha inicial de vigencia" << endl;
      cout << "\t\t" << "5  Solicita datos de CD de FEA por RFC" << endl;
      cout << "\t\t" << "6  Solicita lista de CDs por RFC" << endl << endl;
      cout << "\t\t" << "7  Env�a KeepAlive" << endl << endl; 
      cout << "Solicit� el operador: " << oper << " Verifique que los par�metros sean los correctos" << endl;
      return false;
   }
   
   return true ;
}
//*******************************************************************************************
int main (int argc, char* argv[])
{

   int          oper = 0, lcertif = (8 * 1024),  error = 0;
   char         edoResult ='R'; 
   uint8        itipo=0, certificado[lcertif];
   std::string  vig_ini = "                   ", vig_fin = "                   ", lista = "";
   std::string  numSerie = "             ";
   
   CBitacora Bitacora("/usr/local/SAT/Desarrollo/Proyectos/PKI/ARA/PbaSrvARA/bit_PbaSrvARA.log");

   if ( (oper = procesaParametros(argc, argv)) == 0 )
      return 0;

   if (argc == 3)
      Bitacora.escribePV(BIT_INFO, "Argumentos recibidos (%s, %s, %s)",
                             argv[0], argv[1], argv[2]); 
   else
      Bitacora.escribePV(BIT_INFO, "Argumentos recibidos (%s, %s, %s, %s, %s, %s)",
                             argv[0], argv[1], argv[2], argv[3], argv[4], argv[5]);
   iniciaLibOpenSSL();
   
   CSgiARA ara("Sgi_ARA_prueba.cfg");

   int res = ara.Inicia();
   if ( (res = ara.Conecta()) != 0 )
   {
      cout << "Error al conectarse al servicio de la ARA: " << res << endl;
      ara.Desconecta();
      terminaLibOpenSSL();
      return 1;
   }
   switch (atoi(argv[1]))
   {
      case 1:
      case 2:
         if ( atoi(argv[1]) == 1 )
            error = ara.SolCDxNS( (const char*) argv[2], edoResult, itipo, NULL , &vig_fin, 
                                  (uint8*)certificado, lcertif);
         else
            error = ara.SolCDxNS( (const char*) argv[2], edoResult, itipo, &vig_ini, &vig_fin,
                                  (uint8*)certificado, lcertif);
         if ( error==0 )
         {
            Bitacora.escribePV(BIT_INFO, "Se recibi� el certificado con num_serie: %s, tam = %d, estado (%c), tipo (%d)"
                                         " y vigencias: (%s - %s)",
                                         argv[2], lcertif, edoResult, itipo, vig_ini.c_str(), vig_fin.c_str());
            cout << "Se recibi� el certificado con num_serie: " << argv[2] << " y (tam = " << lcertif << "), con estado (";
            cout << edoResult << "), de tipo (" << itipo << "), con vigencia (" << vig_ini.c_str() << "-" << vig_fin.c_str() << ")" << endl;
         }
         else
         {
            Bitacora.escribePV(BIT_ERROR, "Solicitud de certificado: %s,  (error = %x)",argv[2], error );
            cout << "ERROR: En la solicitud del certificado por num_serie: " << argv[2] << " (error = " << error << ")" << endl; 
            cout << "TxtError" << ara.getErrorTxt().c_str() << endl;
         }
         break;
      case 3:
      case 4:
         if ( atoi(argv[1]) == 3 )
            error = ara.SolEdoCDxNS((const char*)argv[2], edoResult, itipo, NULL, &vig_fin);
         else
            error = ara.SolEdoCDxNS((const char*)argv[2], edoResult, itipo, &vig_ini, &vig_fin);
         if ( error == 0 )
         {
            Bitacora.escribePV(BIT_INFO, "Se recibi� el estado del certificado con num_serie: %s, estado (%c),"
                                         " tipo (%d) y vigencias: (%s - %s)",
                                         argv[2], edoResult, itipo, vig_ini.c_str(), vig_fin.c_str());
            cout << "Se recibi� el certificado con num_serie: " << argv[2] << "Con estado (" << edoResult;
            cout << "), de tipo (" << itipo << ") y con vigencia (" << vig_ini.c_str() << "-" << vig_fin.c_str() << ")" << endl;
         }
         else
         {
            Bitacora.escribePV(BIT_ERROR, "Solicitud del estado de certificado: %s,  (error = %x)",argv[2], error );
            cout << "ERROR: En la solicitud del certificado con VigInic: " << argv[2] << " (error = " << error << ")" << endl;
            cout << "TxtError" << ara.getErrorTxt().c_str() << endl;
         }
         break;
      case 5:
         if ( (error = ara.SolNSCDFEAxRFC((const char*)argv[2], (std::string&) numSerie)) == 0)
         {
             Bitacora.escribePV(BIT_INFO, "Solicitud de certificado por RFC: %s, ultimo n�mero de Serie %s)",
                                    argv[2], numSerie.c_str());
             cout << "Se recibi� el num_serie: " << numSerie.c_str() << " del RFC consultado: " << argv[2] << endl;
         }
         else
         {
            Bitacora.escribePV(BIT_ERROR, "Solicitud de certificado por RFC: %s, se present� el error (%d)", argv[2], error);
            cout << "ERROR: En la solicitud del �ltimo n�mero de serie del certificado con RFC: " << argv[2] << "(error = " ;
            cout << error << ")" << endl;
            cout << "TxtError" << ara.getErrorTxt().c_str() << endl;
         }
         break;
      case 6: //rfc #tipoCert Estado #cuantos
      {
         if ( (error = ara.SolListaCDxRFC((const char*) argv[2], (char) *argv[3], (uint8) atoi(argv[4]), (uint16) atoi(argv[5]),
               &lista)) == 0 )
         {
             Bitacora.escribePV(BIT_INFO, "Solicitud de lista de certificados por RFC: %s, lista de no_Serie %s)",
                                          argv[2], lista.c_str() );
             cout << "Se recibi� del rfc(" << argv[2] << ") la lista de certificados: " << lista.c_str() << endl;
         }
         else
         {
            Bitacora.escribePV(BIT_ERROR, "B�squeda de los certificados del RFC: %s (error = %x)", argv[2], error);
            cout << "ERROR: En la solicitud de lista de certificados por RFC: " << argv[2] << ", error = " <<  error;
            cout << ")" << endl;
            cout << "TxtError" << ara.getErrorTxt().c_str() << endl;
         }
         break;
      }
      case 7:
         if ( (error = ara.KeepAlive()) == 0 )
         {
            Bitacora.escribe(BIT_INFO, "Se env�o correctamente un KEEPALIVE");
            cout << "Se env�o correctamente un KEEPALIVE" << endl;
         }else
         {
            Bitacora.escribePV(BIT_ERROR, "Se recibi� el error: %d al enviar un KEEPALIVE ", error);
            cout << "Se encontr� el error: " << error << " al enviar un KEEPALIVE" << endl;
            cout << "TxtError" << ara.getErrorTxt().c_str() << endl;
         }
      default:
      {
         Bitacora.escribePV(BIT_ERROR, "Se recibi� como par�metro el dato (%s), el cual no es correcto", argv[1]);
         break;
      }
   }
}


