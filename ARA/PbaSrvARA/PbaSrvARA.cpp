#include <iostream>
#include <string>

#include <Sgi_OpenSSL.h>
#include <Sgi_MsgPKI.h>
#include <Sgi_ARA.h>
using std::cout;
using std::endl;

//------------------------------------------------------------------------------------------
int procesaParametros(int argc, char* argv[], CBitacora Bitacora)
{
   int oper = argc > 1 ? atoi(argv[1]) : 0;

   if ( oper < 1 || oper > 7 || (oper == 6 && argc != 6) || ((oper == 1||oper == 2||oper == 3||oper == 4) && argc != 3) ||
        ( oper == 7 && argc != 2) )
   {
      cout << endl << endl ;
      cout << "Uso: " << endl << "\t" << argv[0] << " 1..4 no_serie" << endl;
      cout <<                    "\t" << argv[0] << " 5    rfc" << endl << endl;
      cout <<                    "\t" << argv[0] << " 6    rfc Estado(A,R) #tipoCert(1,2) #cuantos(n)" << endl << endl;
      cout << "\t\t" << "1  Solicita CD" << endl;
      cout << "\t\t" << "2  Solicita CD con fecha inicial de vigencia" << endl;
      cout << "\t\t" << "3  Solicita estado de CD" << endl;
      cout << "\t\t" << "4  Solicita estado de CD con fecha inicial de vigencia" << endl;
      cout << "\t\t" << "5  Solicita datos de CD de FEA por RFC" << endl;
      cout << "\t\t" << "6  Solicita lista de CDs por RFC" << endl << endl;
      cout << "\t\t" << "7  Env�a KeepAlive" << endl << endl; 
      cout << "Solicit� el operador: " << oper << " Verifique que los par�metros sean los correctos" << endl;
      return 0;
   }
   switch (oper)
   {
      case 1:
      case 2:
      case 3:
      case 4:
      case 5:
         Bitacora.escribePV(BIT_INFO, "Argumentos recibidos (%s, %s, %s)", argv[0], argv[1], argv[2]);
         break;
      case 6:
         Bitacora.escribePV(BIT_INFO, "Argumentos recibidos (%s, %s, %s, %s, %s, %s)",
                                      argv[0], argv[1], argv[2], argv[3], argv[4], argv[5]);
         break;
      case 7:
         Bitacora.escribePV(BIT_INFO, "Argumentos recibidos (%s, %s)",
                                      argv[0], argv[1]);
         break;
   }
   return oper ;
}

//------------------------------------------------------------------------------------------
void SolCertificado( int operador, const char* no_serie, CBitacora Bitacora, CSgiARA* Obj_ARA)
{
   int          lcertif = (2 * 1024),  error = 0;
   char         edoResult =' ';
   uint8        itipo=0, certificado[lcertif];
   std::string  vig_ini = " ", vig_fin = " ";


   if ( operador == 1 )
      error = Obj_ARA->SolCDxNS( no_serie, edoResult, itipo, NULL , &vig_fin, (uint8*)certificado, lcertif);
   else
      error = Obj_ARA->SolCDxNS( no_serie, edoResult, itipo, &vig_ini, &vig_fin, (uint8*)certificado, lcertif);
   if ( error==0 )
   {
      Bitacora.escribePV(BIT_INFO, "Se recibi� el certificado con num_serie: %s, tam = %d, estado (%c), tipo (%d) y "
                                   "vigencias: (%s - %s)",
                                   no_serie, lcertif, edoResult, itipo, vig_ini.c_str(), vig_fin.c_str());
      cout << "Se recibi� el certificado con num_serie: " << no_serie << " y (tam = " << lcertif << "), con estado (";
      cout << edoResult << "), de tipo (" << (int) itipo << "), con vigencia (" << vig_ini.c_str() << "-" << vig_fin.c_str() << ")" << endl;
   }
   else
   {
      Bitacora.escribePV(BIT_ERROR, "Solicitud de certificado: %s,  (error = %x)", no_serie, error );
      cout << "ERROR: En la solicitud del certificado por num_serie: " << no_serie  << " (error = " << error << ")" << endl;
      cout << "TxtError: " << Obj_ARA->getErrorTxt().c_str() << endl;
   }
}

//------------------------------------------------------------------------------------------
void SolEdoCertificado(int operador, const char* no_serie, CBitacora Bitacora, CSgiARA* Obj_ARA)
{
   int          error = 0;
   char         edoResult =' ';
   uint8        itipo=0;
   std::string  vig_ini = " ", vig_fin = " ";
   
   if ( operador == 3 )
      error = Obj_ARA->SolEdoCDxNS(no_serie, edoResult, itipo, NULL, &vig_fin);
   else
      error = Obj_ARA->SolEdoCDxNS(no_serie, edoResult, itipo, &vig_ini, &vig_fin);
   if ( error == 0 )
   {
      Bitacora.escribePV(BIT_INFO, "Se recibi� el estado del certificado con num_serie: %s, estado (%c),"
                                   " tipo (%d) y vigencias: (%s - %s)",
                                   no_serie, edoResult, itipo, vig_ini.c_str(), vig_fin.c_str());
      cout << "Se recibi� el certificado con num_serie: " << no_serie << "Con estado (" << edoResult;
      cout << "), de tipo (" << (int) itipo << ") y con vigencia (" << vig_ini.c_str() << "-" << vig_fin.c_str() << ")" << endl;
   }
   else
   {
      Bitacora.escribePV(BIT_ERROR, "Solicitud del estado de certificado: %s,  (error = %x)", no_serie, error );
      cout << "ERROR: En la solicitud del certificado con VigInic: " << no_serie << " (error = " << error << ")" << endl;
      cout << "TxtError: " << Obj_ARA->getErrorTxt().c_str() << endl;
   }
}
//------------------------------------------------------------------------------------------
void SolCertificadoRFC(const char* rfc, CBitacora Bitacora, CSgiARA* Obj_ARA)
{
   int error = 0;
   std::string  numSerie = " ";

   if ( (error = Obj_ARA->SolNSCDFEAxRFC(rfc, &numSerie)) == 0)
   {
      Bitacora.escribePV(BIT_INFO, "Solicitud de certificado por RFC: %s, ultimo n�mero de Serie %s)", rfc, numSerie.c_str());
      cout << "Se recibi� el num_serie: " << numSerie.c_str() << " del RFC consultado: " << rfc << endl;
   }
   else
   {
        Bitacora.escribePV(BIT_ERROR, "Solicitud de certificado por RFC: %s, se present� el error (%d)", rfc, error);
        cout << "ERROR: En la solicitud del �ltimo n�mero de serie del certificado con RFC: " << rfc << "(error = " ;
        cout << error << ")" << endl;
        cout << "TxtError: " << Obj_ARA->getErrorTxt().c_str() << endl;
   }
}

//------------------------------------------------------------------------------------------
void SolListaCertif(const char* rfc, char edo, uint8 itipo, uint16 numCerts, CBitacora Bitacora, CSgiARA* Obj_ARA)
{
   int error=0;
   std::string lista=" ";
   char edoSol[2];
   edoSol[0]=edo;

   if ( (error = Obj_ARA->SolListaCDxRFC(rfc, edoSol[0], itipo, numCerts, &lista)) == 0 )
   {
      Bitacora.escribePV(BIT_INFO, "Solicitud de lista de certificados por RFC: %s, lista de no_Serie (%s)",
                                   rfc, lista.c_str() );
      cout << "Se recibi� del rfc(" << rfc << ") la lista de certificados: " << lista.c_str() << endl;
   }
   else
   {
      Bitacora.escribePV(BIT_ERROR, "B�squeda de los certificados del RFC: %s (error = %x)", rfc, error);
      cout << "ERROR: En la solicitud de lista de certificados por RFC: " << rfc << ", error = " <<  error;
      cout << ")" << endl;
      cout << "TxtError: " << Obj_ARA->getErrorTxt().c_str() << endl;
   }
}
//------------------------------------------------------------------------------------------
void EnviaKeepAlive(CBitacora Bitacora, CSgiARA* Obj_ARA)
{
   int error = 0;
   if ( (error = Obj_ARA->KeepAlive()) == 0 )
   {
      Bitacora.escribe(BIT_INFO, "Se env�o correctamente un KEEPALIVE");
      cout << "Se env�o correctamente un KEEPALIVE" << endl;
   }else
   {
      Bitacora.escribePV(BIT_ERROR, "Se recibi� el error: %d al enviar un KEEPALIVE ", error);
      cout << "Se encontr� el error: " << error << " al enviar un KEEPALIVE" << endl;
      cout << "TxtError: " << Obj_ARA->getErrorTxt().c_str() << endl;
   }
}

//------------------------------------------------------------------------------------------
int main (int argc, char* argv[])
{
   int   oper = 0, res = 0;
   
   CBitacora Bitacora("/usr/local/SAT/PKI/ARA/Sgi_ARA_ghm_0711.log");

   if ( (oper = procesaParametros(argc, argv, Bitacora)) == 0 )
      return 0;
   iniciaLibOpenSSL();
   CSgiARA ara("Sgi_ARA_prueba.cfg");
   res = ara.Inicia();
   if ( (res = ara.Conecta()) != 0 )
   {
      cout << "Error al conectarse al servicio de la ARA: " << res << endl;
      ara.Desconecta();
      terminaLibOpenSSL();
      return 1;
   }
   switch (oper)
   {
      case 1:
      case 2:
         SolCertificado( oper, (const char*) argv[2], Bitacora, &ara); 
         break;
      case 3:
      case 4:
         SolEdoCertificado( oper, (const char*) argv[2], Bitacora, &ara);
         break;
      case 5:
         SolCertificadoRFC((const char*) argv[2], Bitacora, &ara);
         break;
      case 6: //rfc #tipoCert Estado #cuantos
         SolListaCertif((const char*) argv[2], (char) *argv[3], (uint8) atoi(argv[4]), (uint16) atoi(argv[5]), Bitacora, &ara);
         break;
      case 7:
         EnviaKeepAlive(Bitacora, &ara);
         break;
      default:
      {
         Bitacora.escribePV(BIT_ERROR, "Se recibi� como par�metro el dato (%s), el cual no es correcto", argv[1]);
         break;
      }
   }
   terminaLibOpenSSL();
}

