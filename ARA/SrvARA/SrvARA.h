#ifndef _SRVARA_H_
#define _SRVARA_H_
static const char* _SRVARA_H_VERSION_ ATR_USED = "@(#) SrvARA (L : DSIC10155ARA : SrvARA.h : 1.1.1 : 1 : 10/04/16 )";

//#VERSION: 1.1.1
//###############################################################################
#include <string>
#include <vector>

#include <SgiTipos.h>
#include <Sgi_SrvUnx.h>
#include <Sgi_MsgPKI.h>      
#include <Sgi_ConfigFile.h> 
#include <Sgi_Semaforo.h>
#include <Sgi_BD.h>     
#include <Sgi_Archivo.h>
#include <Sgi_Fecha.h>

#ifdef rh72
   #include <stdio.h>
#endif
//###############################################################################
// MACROS 

#define PATH_CONF            "/usr/local/SAT/PKI/etc/"
#define PATH_BITA            "/var/local/SAT/PKI/"

#ifdef DBG
   #define ARCH_CONF            PATH_CONF "SrvARA_dbg.cfg"
   #define ARCH_BITA            PATH_BITA "SrvARA_dbg.log"
#else
   #define ARCH_CONF            PATH_CONF "SrvARA.cfg"
   #define ARCH_BITA            PATH_BITA "SrvARA.log"
#endif

// ERRORES
#define ERR_ARA_COMAR             1
#define ERR_ARA_OPENOVAL          2
#define ERR_ARA_NUMSER           10
#define ERR_ARA_CERNODIS         11
// Error al consultar si el RFC ya cuenta con certificado Digital
#define ERR_ARA_RFCNOCER         12
#define ERR_ARA_NO_CONS_BD     1002
#define ERR_ARA_NO_EXISTE_REG  1005


//VARIABLES GLOBALES
extern std::string strArchLog; //>+ V.2.0.03 

//###############################################################################
// 

class CSrvARA : public CSrvUnx
{

   private :
      unsigned int nons;
      std::vector<std::string> v_ns;
      std::string   rutaArchConfig;
      bool esSAT;
      bool esExtendida;

   public:
      
      typedef enum ARA_Error 
      {
//>         ARAE_ComunicacionAR  =    1,
         ARAE_OpeNoVal        =    2,
         ARAE_NumSer          =   10,
//>         ARAE_CerNoDisp       =   11,
//>         ARAE_RFCNoCer        =   12,
         ARAE_RFC             =   13,
         ARAE_MsgMalForCli    =  120,
         ARAE_MsgMalForMed    =  121,
         ARAE_InfoNoDisp      =  122,
         
         ARAE_Default         = 9999
      };
 

      CSrvARA();
      virtual ~CSrvARA();

   protected:

      MensajesARA  MsgCli;
      uint8*       SktBufCli;
      const int    clSktBufCli;        
      int          lSktBufCli;        
      
      MensajesARA  MsgMed;
      uint8*       SktBufMed;
      const int    clSktBufMed;        
      int          lSktBufMed;        

      pid_t        pid;                //>+ ERGL 071109
  
      CConfigFile  *Configuracion; 
      CBD*         BDARA;    

      string       araCert;
      string       araPKey;
      string       araPwd;
      string       Repositorio;
      string       pathFifo;
      string       pathFifoLec;  //>+ ERGL (070530)

      CSemaforo    *Semaforo;

      virtual bool Proceso();
      virtual bool Inicia();
      virtual bool SetParmsSktProc(CSSL_parms& parms);

      virtual bool ProcesaParametros(int /*argc*/, char* /*argv*/[], char* /*env*/[]); //>>+ V.2.0.01

      virtual bool LeeCfg();
      virtual bool CnxBDARA();
      virtual const char* getMsgError(ARA_Error error);
      virtual bool Operaciones();

      bool configuraBDARA();

      bool IniciaSemaforo();
      bool ReservaSemaforo();
      bool LiberaSemaforo();
      bool envRespuesta();
      bool envOperError(BIT_NIVEL nivel, const char* texto, ARA_Error error);
      bool getDatoMsg(bool cli, int no_msg, char* dato, int* ldato);
      bool pideCertMed(const char* no_serie, char opcion); // opci�n 1 archivo, 2 BD, 3 ambos
      bool envMsgMed();
      bool recMsgMed();
      bool CargaArchCertificado(const string& rutaCert, CArchivo& carch);
      bool InicioDeSesion();
      char certDisponible(const string* rutaCert, const char* no_serie);
                         // res: -1 error, 0 disponible, '1' falta archivo, 2 falta BD, 3 faltan ambos'
      bool obtenDatosEdoBD(const char* no_serie, char *edo, char *tipo, string *vig_ini, string *vig_fin);
      bool obtenNSFEA(const char* rfc, string* no_serie);
      bool valNoSerie(const char* no_serie, int lno_serie);
      bool valRFC    (const char* rfc, int lrfc);
      bool revExistenDatos(const char* no_serie, bool repos, string* ruta);
      bool pideListaNSMed(const char* rfc, int tipo, char edo_cer, int nregs, string* lista);
      
      bool solCDxNS();
      bool solEdoCDxNS();
      bool solNSFEAxRFC();
      bool solListaCDxRFC();
      bool keepalive();

      bool IniciaFifoLectura();   //>+ ERGL (070530)
      bool EnviaIdentificador(int pfd);     //>+ ERGL (070530)
      bool BorraFifoResp();                 //>+ ERGL (071108)
      bool extensionCaducos(const char* no_serie, string rfc, char *tipo, char *edo, string *vig_fin);// (130410) 

};

#endif //_SRVARA_H_
