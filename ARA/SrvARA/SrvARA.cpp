static const char* _SRVARA_CPP_VERSION_ ATR_USED = "@(#) SrvARA (L : DSIC10155ARA : SrvARA.cpp : 1.1.1 : 1 : 10/04/16 )";

//#VERSION: 1.1.1
/**********************************************************************************/
const char *SRV_ARA_VERSION = "SRV_ARA_VERSION V.2.0.0";
/**********************************************************************************/
//#include <unistd.h>
//#include <sys/types.h>
//#include <sys/stat.h>
#include <fcntl.h>
//#include <sys/ipc.h>
#include <assert.h>

#include <SrvARA.h>
#include <Sgi_PKI.h>
#include <Sgi_Archivo.h>
//################################################################################
//GNC Se agregan definiciones de consultas

#include <Sgi_ProtegePwd.h>

#if ORACLE
   #include <CosltSrvARA_ORA.h>
#else
   #include <CosltSrvARA.h> 

#endif

//################################################################################
//   Variables globales
//################################################################################
#define LNG_NOSERIE 20

#define MSG_NUMSERINV "No. de serie inv�lido"
#define MSG_CERNODIS  "Certificado no disponible"
#define MSG_RFCNOCER  "RFC no cuenta con certificado"
#define MSG_CERTNOEX  "Certificado no existe"			   
//********************************************************************************
void FechaBD2Cad(string& fecha)
{
   fecha.erase( 0, 2);
   fecha.erase( 2, 1);
   fecha.erase( 4, 1);
   fecha.erase( 6, 1);
   fecha.erase( 8, 1);
   fecha.erase(10, 1);
} 
//################################################################################
CSrvARA::CSrvARA() :
     MsgCli(), clSktBufCli(sizeof(MsgCli.buffer)), 
     MsgMed(), clSktBufMed(sizeof(MsgMed.buffer)),
     Configuracion( NULL )
{
   MsgMed.esNueva = true;
   pid = 0;
   nons = 0;
   esSAT = false;
   esExtendida = false;
	
   SktBufCli     = MsgCli.buffer;
   SktBufMed     = MsgMed.buffer;
   
   Semaforo   = NULL;
   BDARA   = NULL; 
}
//********************************************************************************
CSrvARA::~CSrvARA()
{
   if (Semaforo)
      delete Semaforo;

   if (BDARA)
      delete BDARA;

   if( Configuracion )
   {
      delete Configuracion;
      Configuracion = NULL;
   }

}
//********************************************************************************
bool CSrvARA::SetParmsSktProc(CSSL_parms& parms)
{
   #ifdef DBG
      #define ESPERA  600
   #else
      #define ESPERA   60
   #endif 
   struct timeval espera = {ESPERA, 0};
   parms.SetServicio(EPlano, &espera);
   return true;
}
//********************************************************************************
bool CSrvARA::LeeCfg()
{
   Bitacora->escribePV(BIT_INFO, "CSrvARA: Archivo de Configuraci�n %s", rutaArchConfig.c_str());
   if( !Configuracion )
   {
      Configuracion = new CConfigFile( rutaArchConfig.c_str() );
   }
   int error = Configuracion->cargaCfgVars();
   if (error)
      Bitacora->escribePV(BIT_ERROR, "En CSrvARA.LeeCfg(): Problemas al leer el archivo de configuraci�n (%d): %s", error, rutaArchConfig.c_str());
   else
   {
      int r1, r2, r3, r4, r5, r6, r7;

      // Lectura de par�metros globales
      r1 = Configuracion->getValorVar("[SRVARA]", "certificado", &araCert);
      r2 = Configuracion->getValorVar("[SRVARA]", "llave_priv" , &araPKey);
      r3 = Configuracion->getValorVar("[SRVARA]", "pwd_priv"   , &araPwd);
      r4 = Configuracion->getValorVar("[SRVARA]", "repositorio", &Repositorio);
      r5 = Configuracion->getValorVar("[SRVARA]", "path_fifo"  , &pathFifo);
      r6 = Configuracion->getValorVar("[SRVARA]", "path_fifo_lec", &pathFifoLec); //>+ERGL (070530)

      std::string no_ns_sat;
      Configuracion->getValorVar("[SERIES-SAT]", "total", &no_ns_sat); 
      nons = atoi( no_ns_sat.c_str() ); 

      char *ns_tag = (char *)malloc( sizeof(char) * 64 );
      memset( ns_tag, 0, sizeof( char ) * 64 );

      for( unsigned int i = 0; i < nons; i++ )
      {
         sprintf( ns_tag, "ns%d", i+1 );
         std::string valor;
         Configuracion->getValorVar( "[SERIES-SAT]", ns_tag, &valor );
         v_ns.insert( v_ns.end(), valor );
         Bitacora->escribePV( BIT_INFO, "CSrvARA: Se asocio el numero de serie %s con el SAT", valor.c_str() );
      }

      std::string extendida;
      r7 = Configuracion->getValorVar("[VIG_EXTENDIDA]", "extendida", &extendida);
      if (!r7)
      {
         if(!extendida.compare("1") )
         {
            esExtendida = true;
            Bitacora->escribePV(BIT_INFO, "CSrvARA: Activada la opcion de Vigencia Final Extendida para los certificados con estado C (caduco)");
         }
         else
            esExtendida = false;
      }
      else
         Bitacora->escribePV(BIT_INFO, "CSrvARA: No se encontro la etiqueta [VIG_EXTENDIDA] ni el parametro extendida en el archivo de configuracion. Se regresaran las vigencias finales reales y no las extendidas para los certificados con estado C (caduco)");      

      if (r1 || r2 || r3 || r4 || r5 || r6)	//>+ERGL (070530) Se agrego r6
      {
         Bitacora->escribePV(BIT_ERROR, "En CSrvARA.LeeCfg(): Error en par�metros de configuraci�n SRVARA (%d,%d,%d,%d,%d)",
            r1, r2, r3, r4, r5);
         return false;
      }
   }
   return !error;   
}
//********************************************************************************
bool CSrvARA::CnxBDARA()
{   
   BDARA = new CBD(Bitacora);

   if (!BDARA)
   {
      Bitacora->escribe(BIT_ERROR, "En CSrvARA.CnxBDARA(): Error al crear el objeto para conexi�n a la base de datos");

      return false;
   }

   if(!configuraBDARA())
   {
      Bitacora->escribe(BIT_ERROR, "En CSrvARA.CnxBDARA(): Error al configurar los par�metros de conexi�n a la base de datos");

      return false;
   }

   if ( !BDARA->conecta() )
   {
      Bitacora->escribe(BIT_ERROR, "En CSrvARA.CnxBDARA(): Error al realizar la conexi�n a la BD con los par�metros configurados");

      return false;
   }
  
   return true;
}
//********************************************************************************
bool CSrvARA::configuraBDARA()
{
   string bdInstancia, bdNombre, bdRol; // Para Informix
   string bdHost, bdSid, bdPuerto;  // Para Oracle
   string bdUsr, bdPwd; // Para Oracle e Informix

   int    r1, r2, r3, r4, r5;

   uint8 password[128];
   int l_password = sizeof(password);
   
   //+ GHM(070909): Se agreg� esta l�nea porque al no estar limpio el arreglo y despues de desencriptar el password
   //               se manten�a los caracteres en el password provocando un error al accesar a la BD 
   memset(password, 0, l_password);

   #if ORACLE
      r1 = Configuracion->getValorVar("[BD-ARA-ORACLE]", "host" , &bdHost);
      r2 = Configuracion->getValorVar("[BD-ARA-ORACLE]", "port" , &bdPuerto);
      r3 = Configuracion->getValorVar("[BD-ARA-ORACLE]", "sid"  , &bdSid);
      r4 = Configuracion->getValorVar("[BD-ARA-ORACLE]", "usr"  , &bdUsr);
      r5 = Configuracion->getValorVar("[BD-ARA-ORACLE]", "pwd"  , &bdPwd);
   #else
      r1 = Configuracion->getValorVar("[BD-ARA]", "instancia", &bdInstancia);
      r2 = Configuracion->getValorVar("[BD-ARA]", "nombre"   , &bdNombre);
      r3 = Configuracion->getValorVar("[BD-ARA]", "usr"      , &bdUsr);
      r4 = Configuracion->getValorVar("[BD-ARA]", "pwd"      , &bdPwd);
      r5 = Configuracion->getValorVar("[BD-ARA]", "role"     , &bdRol);
   #endif
 
   if (r1 || r2 || r3 || r4 || r5 )
   {
      Bitacora->escribePV(BIT_ERROR, "En CSrvARA.configuraBDARA(): Error en los par�metros de configuraci�n BD_ARA (%d,%d,%d,%d,%d)", r1, r2, r3, r4, r5);
      
      return false;
   }

   if(!desencripta((uint8 *)bdPwd.c_str(), bdPwd.size(), password, &l_password))
   {
      Bitacora->escribe(BIT_ERROR, "En CSrvARA.configuraBDARA(): En no se ha podido desencriptar el password para la base de datos.");

      return false;
   }

   bool ok_configura = false;

   #if ORACLE
      Bitacora->escribePV(BIT_DEBUG, "CSrvARA: Datos Config BD: host(%s), puerto(%s), sid(%s), usuario(%s)", bdHost.c_str(), bdPuerto.c_str(), bdSid.c_str(), bdUsr.c_str());

      ok_configura = BDARA->setConfig(bdUsr.c_str(), (char *)password, bdHost.c_str(), bdPuerto.c_str(), bdSid.c_str());
   #else
      Bitacora->escribePV(BIT_DEBUG, "CSrvARA: Datos Config BD: instancia(%s), nombre(%s), usr(%s), rol(%s)", bdInstancia.c_str(), bdNombre.c_str(), bdUsr.c_str(), bdRol.c_str());

      ok_configura = BDARA->setConfig(bdInstancia.c_str(), bdNombre.c_str(), bdUsr.c_str(), (char *)password, bdRol.c_str());
   #endif

   if (!ok_configura)
   {
      Bitacora->escribe(BIT_ERROR, "En CSrvARA.configuraBDARA(): No se han podido configurar los par�metros de conexi�n para la base de datos");

      return false;
   }

   return true;
}
//********************************************************************************
bool CSrvARA::IniciaSemaforo()
{
   if (!Semaforo)
   {
      Semaforo = new CSemaforo(pathFifo.c_str(), 123);
      if (!Semaforo)
         Bitacora->escribe(BIT_ERROR, errno, "En CSrvARA.IniciaSemaforo(): Error al asignar memoria al semaforo para la consulta al Mediador");
      else if (!Semaforo->getError())
         Bitacora->escribePV(BIT_DEBUG, "CSrvARA: Semaforo creado: 0x%X - %d", Semaforo->getLlave(), Semaforo->getId());
      else
      {
         Bitacora->escribePV(BIT_ERROR, "En CSrvARA.IniciaSemaforo(): Error al crear el semaforo para la consulta al Mediador 0x%X (0x%08X)",
            Semaforo->getLlave(), Semaforo->getError());
         delete Semaforo;
         Semaforo = NULL;
      }
   }
   return Semaforo != NULL;
}
//********************************************************************************
bool CSrvARA::ReservaSemaforo()
{
   if (!IniciaSemaforo())
      return false;

   intE error = Semaforo->bloquea();
   if (error)
      Bitacora->escribe(BIT_ERROR, error, "En CSrvARA.ReservaSemaforo(): Error al reservar el semaforo para la consulta al Mediador");

   return !error; 
}
//********************************************************************************
bool CSrvARA::LiberaSemaforo()
{
   intE error = -1;

   if (!Semaforo)
      Bitacora->escribe(BIT_ERROR, "En CSrvARA.LiberaSemaforo(): Error al liberar el semaforo (nulo)");
   else
   {
      error = Semaforo->desBloquea();
      if (error)
         Bitacora->escribe(BIT_ERROR, error, "En CSrvARA.LiberaSemaforo(): Error al liberar el semaforo");
   }

   return !error;
}
//********************************************************************************
/* Crea un fifo para comunicacion desde ARA_Mediador utilizando el pid del proceso actual. */
//>+ ERGL (070528)
bool CSrvARA::IniciaFifoLectura()
{
   pid = getpid();
   char nombre_fifo[255];
   if( access(pathFifoLec.c_str(), F_OK) != 0 )
   {
      sprintf(nombre_fifo, "%s.%d", pathFifoLec.c_str(), pid);
      if( (mkfifo(nombre_fifo, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)) != -1 || errno == EEXIST )
      {
        pathFifoLec = nombre_fifo;
      }
      else
        return false;
   }
   return true;
}
//********************************************************************************
/* Envia el identificador del proceso ARA_Mediador para que este abra la tuberia. */
//>+ ERGL (070528)
bool CSrvARA::EnviaIdentificador(int pfd)
{
   //>- pid_t pid;                      //>- ERGL 071109
   //>- if( IniciaFifoLectura(pid) )    //>- ERGL 071109
   if( IniciaFifoLectura() )
   {
      //int fifo = open( pathFifo.c_str(), O_WRONLY );
      if( pfd != -1 )
      {
         char pid_cad[32];
         sprintf(pid_cad+1, "ID%d", pid);
         uint8 pid_len = (strlen(pid_cad+1)+1);
         pid_cad[0] = pid_len;
         int escritos = write( pfd, pid_cad, pid_len + 1 );
         if( escritos == (pid_len + 1) )
         {
            return true;
         }
         else
            Bitacora->escribe(BIT_ERROR, "En CSrvARA.EnviaIdentificador(): Error al escribir el pid en el fifo para ARA_Mediador.");
      }
      else
         Bitacora->escribe(BIT_ERROR, "En CSrvARA.EnviaIdentificador(): No se pudo abrir el fifo de escritura para ARA_Mediador.");
   }
   else
      Bitacora->escribe( BIT_ERROR, "En CSrvARA.EnviaIdentificador(): No se ha podido crear el fifo de lectura." );
   return false;
}
//********************************************************************************
bool CSrvARA::Inicia()
{
   return LeeCfg() && CnxBDARA();
}
//********************************************************************************
const char* CSrvARA::getMsgError(ARA_Error error)
{
   switch (error)
   {
      case ARAE_OpeNoVal    : return "Error, solicitud de operaci�n inv�lida";
      case ARAE_NumSer      : return "Error, n�mero de serie inv�lido";
      case ARAE_RFC         : return "Error, RFC inv�lido";
      case ARAE_MsgMalForCli: return "Error, solicitud con mensaje mal formado";
      case ARAE_MsgMalForMed: return "Error, respuesta mediador mal formada";
      case ARAE_InfoNoDisp  : return "Error, por el momento la informaci�n no est� disponible";
      case ARAE_Default     : assert(false);
   }
   return "Error interno";
}
//********************************************************************************
bool CSrvARA::getDatoMsg(bool cli, int no_msg, char* dato, int* ldato)
{
   MensajesARA *Msg = cli ? &MsgCli : &MsgMed;
   intE error = Msg->getMensaje(no_msg, dato, ldato); 

   if (error)
      envOperError(BIT_INFO, NULL, cli ? ARAE_MsgMalForCli : ARAE_MsgMalForMed);

   return !error;
}
//********************************************************************************
bool CSrvARA::envRespuesta()
{
   intE error = SktProc->Envia(SktBufCli, MsgCli.tamMensaje());
   if (error)
      Bitacora->escribe(BIT_ERROR, error, "En CSrvARA.envRespuesta(): No se pudo enviar mensaje al cliente");
   else
      Bitacora->escribe(BIT_INFO, "CSrvARA: Mensaje enviado al cliente");
   return !error;
}
//********************************************************************************
bool CSrvARA::envOperError(BIT_NIVEL nivel, const char* texto, ARA_Error error)
{
   if (texto)
      Bitacora->escribe(nivel, texto);
 
   const char* msg = getMsgError(error);
   Bitacora->escribePV(BIT_INFO, "CSrvARA: Enviando error -> %s (ARA %d)", msg, error);  

   char strNError[10];
   sprintf(strNError, "%d", error);

   intE emsg = MsgCli.setMensaje(ERROR_ARA, strNError, strlen(strNError), MSG_CERNODIS, strlen(MSG_CERNODIS)); 
   if (emsg)
      Bitacora->escribe(BIT_ERROR, emsg, "En CSrvARA.envOperError(): No se pudo armar mensaje de error para enviar al cliente");
   else 
      return envRespuesta();
   
   return !emsg;
}
//******************************************************************************** 
//>+ ERGL 071108
bool CSrvARA::envMsgMed()
{
   bool ok = false;
   if ( (access(pathFifo.c_str(), F_OK) == 0) || (mkfifo(pathFifo.c_str(), 0660) == 0) )
   {
      int fifo = open( pathFifo.c_str(), O_WRONLY | O_NONBLOCK);
      if ( fifo != -1 ) 
      {
         if ( EnviaIdentificador( fifo ) )
         {
            int lSktBufMed = MsgMed.tamMensaje();
            int escritos = write(fifo, SktBufMed, lSktBufMed);
            ok = (escritos == lSktBufMed);
            if ( !ok )
            {
               Bitacora->escribe(BIT_ERROR, sgiError(0, eERRNO, errno),
                                 "Error al escribir en  el archivo de intercambio con el mediador");
            }
         }
         else
            Bitacora->escribe( BIT_ERROR, "En CSrvARA.envMsgMed(): No se ha podido enviar el identificador al ARA_Mediador" );
         close(fifo);
      } 
      else
      {
         Bitacora->escribe( BIT_ERROR, sgiError( 0, eERRNO, errno ), "Error al abrir el archivo "
                           "de intercambio con el mediador -escritura-" );
      }
   }
   else
      Bitacora->escribe(BIT_ERROR, sgiError(0, eERRNO, errno), "Error al crear el archivo de intercambio con el mediador");  

   return ok;
}

/*>- ERGL 071108
bool CSrvARA::envMsgMed()
{
   if (access(pathFifo.c_str(), F_OK) && mkfifo(pathFifo.c_str(), 0660) == -1)
   {
     Bitacora->escribe(BIT_ERROR, sgiError(0, eERRNO, errno), "Error al crear el archivo de intercambio con el mediador");
     return false;
   }

   int fifo = open(pathFifo.c_str(), O_WRONLY);
   if (fifo == -1)
   {
      Bitacora->escribe(BIT_ERROR, sgiError(0, eERRNO, errno), 
                        "Error al abrir el archivo de intercambio con el mediador -escritura-");
      return false;
   }
 
   if ( !EnviaIdentificador(fifo) )
   {
      return false;
   }
   
   int lSktBufMed = MsgMed.tamMensaje();
   int escritos = write(fifo, SktBufMed, lSktBufMed);
   bool ok = escritos == lSktBufMed;
   if (!ok)
   {
      Bitacora->escribe(BIT_ERROR, sgiError(0, eERRNO, errno),
                        "Error al escribir en  el archivo de intercambio con el mediador");
   }
   close(fifo);
   return ok;
}
-<*/
//********************************************************************************
bool CSrvARA::recMsgMed()
{
   //int fifo = open(pathFifo.c_str(), O_RDONLY);   //>- ERGL (070531)
   int fifo = open(pathFifoLec.c_str(), O_RDONLY | O_NONBLOCK);
   if (fifo == -1)
   { 
      Bitacora->escribe(BIT_ERROR, sgiError(0, eERRNO, errno) ,
                        "En CSrvARA.recMsgMed(): Error al abrir el archivo de intercambio con el mediador -lectura-");
      return false;
   }

   bool ok = false;

   int lSktBufMed = read(fifo, SktBufMed, clSktBufMed);
   if (lSktBufMed < 0)
      Bitacora->escribe(BIT_ERROR, sgiError(0, eERRNO, errno),
                        "Error al leer del archivo de intercambio con el mediador");
   else if (lSktBufMed < 4 || lSktBufMed != MsgMed.tamMensaje())
      Bitacora->escribe(BIT_ERROR, "En CSrvARA.recMsgMed(): Error en recepci�n, mensaje recibido del mediador mal formado");
   else
      ok = true;

   close(fifo);
   //>- unlink(pathFifoLec.c_str()); //ERGL jue nov  8 10:36:28 CST 2007
   return ok;
}
//********************************************************************************
bool CSrvARA::pideCertMed(const char* no_serie, char opcion)
     // opci�n '1' archivo, '2' BD, '3' ambos
{
   bool ok = false;
   Bitacora->escribePV(BIT_INFO, "CSrvARA: Solicitando al mediador el certificado: %s (opci�n %c)", no_serie, opcion);

   if (!ReservaSemaforo())
      return false;

   intE error = MsgMed.setMensaje(SOLCERTMEDARA, no_serie, strlen(no_serie), &opcion, 1);
   if (error)
   {
      Bitacora->escribe(BIT_ERROR, error, "En CSrvARA.pideCertMed(): Error al armar mensaje de solicitud al mediador");
      LiberaSemaforo(); //>+ ERGL 071108 
   }
   else if (envMsgMed() && recMsgMed()) 
   {
      if (MsgMed.tipOperacion() == CERTREGARA)           
         ok = true;                 
      else if (MsgMed.tipOperacion() == CERTNOEXISTE)        
         Bitacora->escribePV(BIT_ERROR, "En CSrvARA.pideCertMed(): El Certificado no existe %s", no_serie);
      else
         Bitacora->escribePV(BIT_ERROR, "En CSrvARA.pideCertMed(): Error al obtener la actualizaci�n de los datos del certificado %s", no_serie);
   }
   LiberaSemaforo();

   return ok;
}
//********************************************************************************
bool CSrvARA::CargaArchCertificado(const string& rutaCert, CArchivo& carch)
{
   intE error = carch.Lee(rutaCert.c_str());
   if (error)
   {
      string msg = "En CSrvARA.CargaArchCertificado(): No se pudo leer el Certificado del repositorio: ";
      msg += rutaCert;
      Bitacora->escribe(BIT_ERROR, error, msg.c_str());
      return false;
   }
   return true;
}
//********************************************************************************
bool CSrvARA::obtenDatosEdoBD(const char* no_serie, char *edo, char *tipo, string *vig_ini, string *vig_fin)
{
   string rfc;

   if ( !BDARA->consultaReg(QS_RFC_EDO_TIP_VIVF_CERT, no_serie) )
   {
      Bitacora->escribePV(BIT_ERROR, "En CSrvARA.obtenDatosEdoBD(): Error al extraer los datos del certificado con n�mero de serie (%s)",no_serie);
      return false;
   }
   if ( !BDARA->getValores("sccss",&rfc ,edo, tipo, vig_ini, vig_fin) )
   {
      Bitacora->escribePV(BIT_ERROR, "En CSrvARA.obtenDatosEdoBD(): Error al obtener los datos del certificado con n�mero de serie (%s)", no_serie);
      return false;
   }
   
   if (esExtendida)
      extensionCaducos(no_serie, rfc, tipo, edo, vig_fin);

   FechaBD2Cad(*vig_ini);
   FechaBD2Cad(*vig_fin);
   
   return true;
}
//********************************************************************************
bool CSrvARA::obtenNSFEA(const char* rfc, string* no_serie)
{
   if (BDARA->consultaReg(QS_NOSERIE_CERTIFICADO, rfc) &&
       BDARA->getValores("s", no_serie))
      return true;
   
   if (pideListaNSMed(rfc, 1, 'X', 1, no_serie))
   {
      string::size_type loc = no_serie->find('|', 0);
      if (loc != string::npos)
         no_serie->erase(loc);
      return true;
   } 

   return false;
}
//********************************************************************************
// res: -1 error, 0 disponible, '1' falta archivo, 2 falta BD, 3 faltan ambos'
char CSrvARA::certDisponible(const string* rutaCert, const char* no_serie)
{
   char respuesta = 0;
   int  renglones = 0;
   char l_edo = 0;

   if (rutaCert && access(rutaCert->c_str(), F_OK))
   {
      Bitacora->escribePV(BIT_INFO, "CSrvARA: No est� disponible el archivo: %s (errno %d)", rutaCert->c_str(), errno); 
      respuesta = 1;
   }
   //if ( !BDARA->consultaReg("SELECT COUNT(*) FROM certificado WHERE no_serie = '%s'", no_serie) )

   if ( !BDARA->consultaReg(QS_CUANTOS_EDO_CERT, no_serie) )
   {
      Bitacora->escribePV(BIT_ERROR, "En CSrvARA.certDisponible(): Error al extraer los certificados existentes por n�mero de serie (%s)",no_serie);
      //return -1;
      //respuesta+=2;
   }
   //if ( !BDARA->getValores("i", &renglones) )
   if ( !BDARA->getValores("ic", &renglones, &l_edo) )
   {
      Bitacora->escribePV(BIT_ERROR, "En CSrvARA.certDisponible(): Error al obtener el n�mero de certificados del n�mero de serie (%s)",
                          no_serie);
   }
   if( renglones && (!esSAT && l_edo == 'A') )
   {
      Bitacora->escribePV( BIT_INFO, "CSrvARA: El certificado solicitado no es del SAT y tiene estado Activo, se solicitara actualizacion (%s)", no_serie );
      respuesta += 2;
   }
   else if( !renglones )
   {
      Bitacora->escribePV(BIT_INFO, "CSrvARA: No est� disponible el registro en BD del n�mero de serie: %s", no_serie); 
      respuesta += 2;
   }

   if (respuesta)
      respuesta+= 0x30;

   return respuesta;
}
//********************************************************************************
bool CSrvARA::InicioDeSesion()
{
   uint8 password[128];
   int l_password = sizeof(password);

   if(!desencripta((uint8 *)araPwd.c_str(), araPwd.size(), password, &l_password))
   {
      Bitacora->escribe(BIT_ERROR, "En CSrvARA.InicioDeSesion(): No se ha podido desencriptar el password para la ARA.");
      return FALSE;
   }
   //>+- HOA (090511): corrige potenciales problemas si el password tiene longitud diferente a 9
   //>- password[9] = '\0';
   password[l_password] = '\0';
   //<+- HOA (090511)
   intE error = MsgCli.Mensajes::Inicia(ARA, (char*) araCert.c_str(), (char*) araPKey.c_str(), 
                                        (char*) password, l_password, NULL);  
   if (error) 
      Bitacora->escribe(BIT_ERROR, error , "En CSrvARA.InicioDeSesion(): Error al iniciar la librer�a para el intercambio de mensajes con el cliente");
   else
   {
      error = MsgMed.Mensajes::Inicia(ARA, (char*) araCert.c_str(), (char*) araPKey.c_str(), 
                                      (char*) password, l_password, NULL);
      if (error) 
         Bitacora->escribe(BIT_ERROR, error, "En CSrvARA.InicioDeSesion(): Error al iniciar la librer�a para el intercambio de mensajes con el Mediador");
      else
      {
         MsgMed.setVersion(0); 
         MsgCli.setVersion(0); 
         error = MsgCli.ConectaServidor(SktProc);
         //Verificar 
         if (error) 
            Bitacora->escribe(BIT_ERROR, error, "En CSrvARA.InicioDeSesion(): Error en protocolo de inicio de conexi�n");
      } 
   }

   return !error;
}
//********************************************************************************
bool CSrvARA::Proceso()
{
   bool ok;
   
   ok = InicioDeSesion() &&
        Operaciones();
   BorraFifoResp (); 

   return ok;
}
//********************************************************************************
bool CSrvARA::valNoSerie(const char* no_serie, int lno_serie)
{
   bool ok = false;
   int oper = MsgCli.tipOperacion();

   if (!lno_serie || !no_serie[0])
   {
      Bitacora->escribePV(BIT_ALERTA, "CSrvARA: No se recibi� n�mero de serie (oper %d)", oper);
      envOperError(BIT_INFO, NULL, ARAE_NumSer);
   }
   else
   {
      Bitacora->escribePV(BIT_INFO, "CSrvARA: Recibe solicitud (oper %d) por n�mero de serie: '%s'", oper, no_serie);
      size_t tamNS = strlen(no_serie);
      if (lno_serie != LNG_NOSERIE || tamNS != LNG_NOSERIE)
      {
         Bitacora->escribePV(BIT_ALERTA, "CSrvARA: Longitud del n�mero de serie incorrecta (oper %d): %d, %d", oper, lno_serie, tamNS);
         envOperError(BIT_INFO, NULL, ARAE_NumSer);
      }
      else if (strspn(no_serie, "0123456789") != tamNS)
         envOperError(BIT_ALERTA, "CSrvARA: Error, el n�mero de serie contiene caracteres inv�lidos", ARAE_NumSer);
      else
      {
         std::string s_noserie( no_serie );
         for( unsigned int i = 0; i < nons; i++ )
         {
            std::string tmp_s = v_ns.at( i );
            Bitacora->escribePV( BIT_DEBUG, "CSrvARA: Comparando %s (%d) con %s", tmp_s.c_str(), tmp_s.size(), s_noserie.c_str() );
            //if( !tmp_s.compare( 0, tmp_s.size(), s_noserie ) )
            if( s_noserie.compare( 0, 12, tmp_s ) == 0)
            {
               esSAT = true;
               break;
            }
         }
         ok = true;
      }
   }
   return ok;
}
//********************************************************************************
bool CSrvARA::valRFC (const char* rfc, int lrfc)
{
   bool ok = false;
   
   if (!lrfc || !rfc[0])
      envOperError(BIT_ALERTA, "CSrvARA: No se recibi� RFC", ARAE_RFC);
   else
   {
      Bitacora->escribePV(BIT_INFO, "CSrvARA: Recibe solicitud por RFC: '%s'", rfc);
      size_t tamRFC = strlen(rfc);
      if ((lrfc < 12 || lrfc > 13) || (size_t) lrfc != tamRFC)
      {
         Bitacora->escribePV(BIT_ALERTA, "CSrvARA: Longitud del RFC incorrecta: %d, %d", lrfc, tamRFC);
         envOperError(BIT_INFO, NULL, ARAE_RFC);
      }
      else if (strspn(rfc, "ABCDEFGHIJKLMN�OPQRSTUVWXYZ&0123456789") != tamRFC)
         envOperError(BIT_ALERTA, "CSrvARA: Error, el RFC contiene caracteres inv�lidos", ARAE_RFC);
      else
         ok = true;
   }
   return ok;
}
//********************************************************************************
bool CSrvARA::revExistenDatos(const char* no_serie, bool repos, string* ruta)
{
   assert((repos && ruta != NULL) || (!repos && ruta == NULL));

   bool ok = true;
   if (repos) 
   {
      intE error = pkiRutaCert(Repositorio, string(no_serie), *ruta);
      if (error)
      {
         Bitacora->escribe(BIT_ERROR, error, "En CSrvARA::revExistenDatos(): Error al armar la ruta del certificado en el repositorio");
         envOperError(BIT_INFO, NULL, ARAE_InfoNoDisp);
         return false;         
      }
   }

   char pedirMed = certDisponible(ruta, no_serie);
   if (pedirMed)
   {
      ok = pideCertMed(no_serie, pedirMed);
      if (!ok)
         envOperError(BIT_INFO, NULL, ARAE_InfoNoDisp);
   }

   return ok;
}
//********************************************************************************
bool CSrvARA::pideListaNSMed(const char* rfc, int tipo, char edo_cer, int nregs, string* lista)
{
   bool ok = false;

   Bitacora->escribePV(BIT_INFO, "CSrvARA: Solicitando al mediador lista de CDs: RFC(%s), tipo(%d), edo(%c), max_regs(%d)",
                       rfc, tipo, edo_cer, nregs);   

   #ifdef SLES10
      lista->clear();
   #else
      lista->erase(0, lista->length());
   #endif

   if (!ReservaSemaforo())
      return false;

   char cTipo[5], cNRegs[10];
   sprintf(cTipo , "%d", tipo);
   sprintf(cNRegs, "%d", nregs);

   intE error = MsgMed.setMensaje(SOLLISTACERTRFC, rfc, strlen(rfc), cTipo, strlen(cTipo), 
                                  &edo_cer, 1, cNRegs, strlen(cNRegs));
   if (error)
   {
      Bitacora->escribe(BIT_ERROR, error, "En CSrvARA.pideListaNSMed(): Error al armar mensaje de solicitud al mediador");
      LiberaSemaforo(); //>+ ERGL 071108
   }
   else if (envMsgMed() && recMsgMed())
   {
      if (MsgMed.tipOperacion() == LISTACERT)
      {
         static char bufLista[32 * 1024];
         int lbufLista = sizeof(bufLista);
         error = MsgMed.getMensaje(LISTACERT, bufLista, &lbufLista); 
         if (error)
         {
            Bitacora->escribe(BIT_ERROR, error, "En CSrvARA.pideListaNSMed(): Error al decodificar el mensaje de respuesta del mediador");
            envOperError(BIT_INFO, NULL, ARAE_InfoNoDisp);
         }
         else
         {
            *lista = bufLista;
            ok = true;
         }
      }
      else if (MsgMed.tipOperacion() == CERTNOEXISTE)
         Bitacora->escribePV(BIT_INFO, "CSrvARA: El RFC %s no cuenta con CDs con los par�metros especificados", rfc);
      else
         Bitacora->escribePV(BIT_ERROR, "En CSrvARA.pideListaNSMed(): Error al obtener la lista de CDs del rfc %s", rfc);
   }
   LiberaSemaforo();

   return ok;
}
//********************************************************************************
//********************************************************************************
bool CSrvARA::Operaciones()
{
   intE error;

   for (;;)
   {
      error = MsgCli.Recibe(SktProc); 

//>>+ GHM (071116): Se agrega funci�n para que en el DEBUG se pueda observar que se obtiene del mensaje
#ifdef DBG
      std::string DatoRecibido = dumpBuffer( "Mensaje recibido: ", MsgCli.buffer, MsgCli.getTamDatos() );
      Bitacora->escribe(BIT_DEBUG, DatoRecibido.c_str());
#endif
//<<+ GHM (071116)

      if (!error)
      {
         int oper = MsgCli.tipOperacion(); 
         switch (oper)
         {
            case SOLCERTARA        : 
            case SOLCERTARAF       : solCDxNS      (); break;
 
            case SOLEDOCERTARA     :
            case SOLEDOCERTARAF    : solEdoCDxNS   (); break;

            case SOLCERTRFC        : solNSFEAxRFC  (); break;
            case SOLLISTACERTRFC   : solListaCDxRFC(); break;
            case PKI_KEEPALIVE     : keepalive     (); break;
            case SOLDESCONEXIONARA : return true;
            default                : envOperError(BIT_ALERTA, NULL, ARAE_OpeNoVal); 
         }
      }
      else
      { 
         if (sgiErrorBase(error) != ERR_SKTTIMEOUT)
         {  
            Bitacora->escribe(BIT_ERROR, error , "Error al recibir solicitud de operaci�n");
            break;
         }
         struct timespec ts = { 0, 1000000};
         nanosleep(&ts, NULL);
      }
   }

   return !error;
}
//######################################################################################################################
//### OPERACIONES
//######################################################################################################################
bool CSrvARA::solCDxNS()
{
   bool ok = false;
   CArchivo archCert;
   char edo = 0, tipo = 0;
   string vigIni, vigFin;

   int  no_mens = MsgCli.tipOperacion();

   Bitacora->escribePV(BIT_INFO, "---> Inicio solCDxNS (%d)", no_mens);
   
   char no_serie[64];
   int  lno_serie = sizeof(no_serie);
   string rutaCert;

   if (getDatoMsg(true, no_mens, no_serie, &lno_serie) && 
       valNoSerie(no_serie, lno_serie) && 
       revExistenDatos(no_serie, true, &rutaCert)) 
   {
      ok = CargaArchCertificado(rutaCert, archCert) && obtenDatosEdoBD(no_serie, &edo, &tipo, &vigIni, &vigFin);
      if (!ok)
         envOperError(BIT_INFO, NULL, ARAE_InfoNoDisp);
      else
      {
         Bitacora->escribePV(BIT_INFO, "Datos NS %s: Estado(%c), Tipo(%c), Vigencia(%s-%s)", 
                                  no_serie, edo, tipo, vigIni.c_str(), vigFin.c_str());
         intE error;
         if (no_mens == SOLCERTARAF)
            error = MsgCli.setMensaje(CERTARAF, &edo, 1, &tipo, 1, vigIni.c_str(), vigIni.length(), 
                                      vigFin.c_str(), vigFin.length(), archCert.getBuf(), archCert.getLng()); 
         else
            error = MsgCli.setMensaje(CERTARA , &edo, 1, &tipo, 1, vigFin.c_str(), vigFin.length(), 
                                      archCert.getBuf(), archCert.getLng());
         if (error)
         {
            Bitacora->escribe(BIT_ERROR, error, "Error al armar mensaje de contestaci�n");
            envOperError(BIT_INFO, NULL, ARAE_InfoNoDisp);
         }
         else
            ok = envRespuesta(); 
      }
   }
   Bitacora->escribePV(BIT_INFO, "<--- Fin solCDxNS (%d)", no_mens);
   return ok;
}
//######################################################################################################################
bool CSrvARA::solEdoCDxNS()  
{
   bool ok = false;
   char edo = 0, tipo = 0;
   string vigIni, vigFin;

   int no_mens = MsgCli.tipOperacion();
   
   Bitacora->escribePV(BIT_INFO, "---> Inicio solEdoCDxNS (%d)", no_mens);

   char  no_serie[64];
   int   lno_serie = sizeof(no_serie);
   
   if (getDatoMsg(true, no_mens, no_serie, &lno_serie) && 
       valNoSerie(no_serie, lno_serie) &&
       revExistenDatos(no_serie, false, NULL))
   {
      ok = obtenDatosEdoBD(no_serie, &edo, &tipo, &vigIni, &vigFin);
      if (!ok)
         envOperError(BIT_INFO, NULL, ARAE_InfoNoDisp);
      else
      {      
         Bitacora->escribePV(BIT_INFO, "Datos NS %s: Estado(%c), Tipo(%c), Vigencia(%s-%s)",
                                  no_serie, edo, tipo, vigIni.c_str(), vigFin.c_str());
         intE error;
         if (no_mens == SOLEDOCERTARAF)
            error = MsgCli.setMensaje(EDOCERTARAF, &edo, 1, vigIni.c_str(), vigIni.length(),
                                      vigFin.c_str(), vigFin.length(), &tipo, 1);
         else
            error = MsgCli.setMensaje(EDOCERTARA , &edo, 1, vigFin.c_str(), vigFin.length(), &tipo, 1);
         if (error)
         {
            Bitacora->escribe(BIT_ERROR, error, "Error al armar mensaje de contestaci�n");
            envOperError(BIT_INFO, NULL, ARAE_InfoNoDisp);
         }
         else
            ok = envRespuesta();
      }
   }
   Bitacora->escribePV(BIT_INFO, "<--- Fin solEdoCDxNS (%d)", no_mens);
   return ok;
}
//######################################################################################################################
bool CSrvARA::solNSFEAxRFC()
{
   bool ok = false;

   Bitacora->escribe(BIT_INFO, "---> Inicio solNSFEAxRFC");

   char Rfc[20];
   int  lRfc = sizeof(Rfc);

   if (getDatoMsg(true, MsgCli.tipOperacion(), Rfc, &lRfc) &&
       valRFC(Rfc, lRfc))
   {
      Bitacora->escribePV(BIT_INFO, "Recibe solicitud del �ltimo n�mero de serie de CD de FIEL por RFC: %s", Rfc);

      string nSerie;
      ok = obtenNSFEA(Rfc, &nSerie);
      if (!ok)
         envOperError(BIT_INFO, NULL, ARAE_InfoNoDisp);
      else
      {
         Bitacora->escribePV(BIT_INFO, "Dato CD de FIEL: %s -> %s", Rfc, nSerie.c_str());
         intE error = MsgCli.setMensaje(CERTRFC, nSerie.c_str(), nSerie.length());
         if (error)
         {
            Bitacora->escribe(BIT_ERROR, error, "Error al armar mensaje de contestaci�n");
            envOperError(BIT_INFO, NULL, ARAE_InfoNoDisp);
         }
         else
            ok = envRespuesta();
      }
   }   
   Bitacora->escribe(BIT_INFO, "<--- Fin solNSFEAxRFC");
   return ok;
}
//######################################################################################################################
bool CSrvARA::solListaCDxRFC()
{
   char rfc[20], tipo[2], edo[2], nregs[10];
   int  lrfc = sizeof(rfc), ltipo = sizeof(tipo), ledo = sizeof(edo), lnregs = sizeof(nregs);
   bool ok = false;

   Bitacora->escribe(BIT_INFO, "---> Inicio solListaCDxRFC");

   intE error = MsgCli.getMensaje(MsgCli.tipOperacion(), rfc, &lrfc, tipo, &ltipo, edo, &ledo, nregs, &lnregs); 
   if (error)
   {
      Bitacora->escribe(BIT_ERROR, error, "solListaCDxRFC, error al decodificar el mensaje del cliente");
      envOperError(BIT_INFO, NULL, ARAE_MsgMalForCli);
   }
   else if (valRFC(rfc, lrfc))
   {
      Bitacora->escribePV(BIT_INFO, "Solicitud lista de CDs: RFC(%s), tipo(%s), edo(%s), max_regs(%s)", 
                          rfc, tipo, edo, nregs);

      string lista;
      if (!pideListaNSMed(rfc, atoi(tipo), edo[0], atoi(nregs), &lista))
      {
         //>+ ERGL (070613)
         Bitacora->escribePV(BIT_ERROR, "Ocurrio un error al solicitar la lista de n�mero de serie");
         envOperError(BIT_INFO, NULL, ARAE_InfoNoDisp);
      }
      else
      {
         Bitacora->escribePV(BIT_INFO, "Lista NS de %s: '%s'", rfc, lista.c_str()); 
         error = MsgCli.setMensaje(LISTACERT, lista.c_str(), lista.length());
         if (error)
         {
            Bitacora->escribe(BIT_ERROR, error, "Error al armar mensaje de contestaci�n");
            envOperError(BIT_INFO, NULL, ARAE_InfoNoDisp);
         }
         else
            ok = envRespuesta();
      }
   }
   Bitacora->escribe(BIT_INFO, "<--- Fin solListaCDxRFC");
   return ok;
}
//######################################################################################################################
bool CSrvARA::keepalive()
{
   return envRespuesta();
}
//######################################################################################################################
//>+ ERGL 071108
bool CSrvARA::BorraFifoResp()
{
   int verror = 0;
   if ( (access(pathFifoLec.c_str(), F_OK)) == 0 )
   {
      unlink(pathFifoLec.c_str());
      verror = errno;
      if ( verror != 0 )
      {
         Bitacora->escribePV(BIT_ERROR, "Ocurrio un error al intentar borrar el fifo de lectura. (%d)", verror );
         return false;
      }
      Bitacora->escribePV(BIT_DEBUG, "Se ha eliminado el archivo %s", pathFifoLec.c_str());  
   }
   else
   {
      verror = errno;
      Bitacora->escribePV(BIT_DEBUG, "No es posible accesar al fifo ERRNO(%d)", verror);  
   }
   return true;
}
//######################################################################################################################

bool CSrvARA::
extensionCaducos(const char* no_serie, string rfc, char *tipo, char *edo, string *vig_fin)
{
   
   //>>> HOA: 090505 PENDIENTE: Falta revisar si tiene un nuevo certificado
   // Adici�n para agregar 90 d�as naturales a la vigencia final de los Certificados de Fiel
   // en el caso de que est� vencido y el RFC no tenga CD de Fiel posterior
   // en el caso de que est� vencido y el RFC no tenga CD de Fiel posterior
   if (*tipo == '1' && (*edo == 'C' || *edo == 'A'))
   {
      CFecha fecha;
      int error = 0;

      time_t act = time(NULL);
      if (act == ((time_t)-1))
      {
         Bitacora->escribePV(BIT_ERROR, "obtenDatosEdoBD: Al obtener la fecha actual - time (%i)", errno);
         return false;
      }

      time_t tVigFin = fecha.ConvFechFtoBDTOtime_t(*vig_fin, HORA_UTC, &error);
      if (*edo == 'C' || (difftime(tVigFin, act) <= 0))
      {
         tVigFin += (86400 * 90); // Segs por dia * 90 dias

         if (difftime(tVigFin, act) > 0)
         {
            string ultNSerie;
            if (!obtenNSFEA(rfc.c_str(), &ultNSerie))
               Bitacora->escribe(BIT_ERROR, "obtenDatosEdoBD: Al obtener �ltimo n�mero de serie");
            else if (no_serie >= ultNSerie)
            {
               struct tm* stVigFin = fecha.FechaCal(&tVigFin, &error);
               if (!stVigFin)
               {
                  Bitacora->escribePV(BIT_ERROR, "obtenDatosEdoBD: Al convertir fecha vig_fin - FechaCal (%i)", error);
                  return false;
               }
               char bufVigFin[TAM_FECBD + 1];
               if (fecha.strTimeTOfechBD(stVigFin, bufVigFin))
               {
                  Bitacora->escribe(BIT_ERROR, "obtenDatosEdoBD: Al convertir fecha vig_fin a fto BD - strTimeTOfechBD");
                  return false;
               }
               *vig_fin = bufVigFin;
               *edo     = 'A';
            }
         }
      }
   }
   //<<< HOA: 090505

   return true;
}
