#ifndef _COSLTSRVARA_H_
#define _COSLTSRVARA_H_

static const char* _COSLTSRVARA_H_VERSION_ ATR_USED = "@(#) SrvAC ( L : DSIC10392AR_ : ConsltSrvARA.h : 1.0.0 : 10/10/14)";

//#VERSION: 1.1.0
/*##############################################################################
  ###  PROYECTO:              PKI-SAT                                        ###
  ###  MODULO:                Servicio ARA                                    ###
  ###                                                                        ###
  ###                                                                        ###
  ###  DESARROLLADORES:       Guadalupe Negrete Castro         GNC           ###
  ###                                                                        ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       Viernes 10, octubre del 2014                   ###
  ###                                                                        ###
  ##############################################################################
         1         2         3         4         5         6         7         8
12345678901234567890123456789012345678901234567890123456789012345678901234567890

################################################################################
  VERSION:
     V.1.0.0  (20141010- )    GNC: Version Original
                              Consutas para el Servicio de Autoridad Registradora de Aplicaciones PKI-SAT
  

#################################################################################*/



//#VERSION: 1.1.0



			 
#define QS_RFC_EDO_TIP_VIVF_CERT             "SELECT rfc,edo_cer, tipcer_cve, vig_ini, vig_fin FROM certificado WHERE no_serie = '%s'"
#define QS_NOSERIE_CERTIFICADO               "SELECT FIRST 1 no_serie FROM certificado WHERE rfc = '%s' and tipcer_cve = 1 ORDER BY no_serie DESC;"
#define QS_CUANTOS_EDO_CERT                  "SELECT COUNT(*), edo_cer FROM certificado WHERE no_serie = '%s' GROUP BY edo_cer;"










#endif















