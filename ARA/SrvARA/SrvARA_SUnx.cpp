static const char* _SRVARA_SUNX_CPP_VERSION_ ATR_USED = "@(#) SrvARA (L : DSIC10155ARA : SrvARA_SUnx.cpp : 1.1.1 : 1 : 10/04/16 )";

//#VERSION: 1.1.1
//######################################################################################################################
//######################################################################################################################
//
// Complemento de la funcionalidad requerida por la librer�a Sgi_SrvUnx para crear el servicio SrvARA
//
//######################################################################################################################
//######################################################################################################################
#include <SrvARA.h>

//VARIABLES GLOBALES
std::string strArchLog; //>+ V.2.0.03

//######################################################################################################################
//######################################################################################################################
//>+ V.2.0.03

bool CSrvARA::
ProcesaParametros( int argc, char *argv[], char *env[] )
{
   int opt = 0;
   size_t l_path = 0;
   char *ruta_cfg = NULL;
   char *ruta_bit = NULL;

   #if defined(DBG) && !defined(DBG_NO_SRV)
      sleep(5);
   #endif

   strArchLog = std::string(ARCH_BITA);
   rutaArchConfig = std::string( ARCH_CONF );
   while( (opt = getopt( argc, argv, "c:b:" )) != -1 )
   {
      switch( opt )
      {
         case 'c' :
            l_path = strlen( optarg );
            ruta_cfg = (char *)malloc( sizeof(char) * l_path+1 );
            ruta_cfg[l_path] = 0x00;
            strncpy( ruta_cfg, optarg, l_path );
            rutaArchConfig = ruta_cfg;
            break;
         case 'b' :
            l_path = strlen( optarg );
            ruta_bit = (char *)malloc( sizeof(char) * l_path+1 );
            ruta_bit[l_path] = 0x00;
            strncpy( ruta_bit, optarg, l_path );
            strArchLog = ruta_bit;
            break;
      }
   }
   return true;
}

/*bool CSrvARA::ProcesaParametros(int argc, char* argv[], char* env[])
{
   #if defined(DBG) && !defined(DBG_NO_SRV)
      sleep(5);
   #endif
   
   int   c = -1;     // Inicializa el car�cter fuera del rango esperado
   opterr = 0;       // Inicializa el indicador de error de getopt
   
   strArchLog = std::string(ARCH_BITA); //Inicializa la variable del archivo de bit�cora
   if ( (argc > 1))  //En caso de recibir argumentos
   {
      for (;;)  //Rastreo de todas los argumentos
      {
         c = getopt(argc, argv, "l"); //Argumentos a evaluar
         if (c == -1)
            break;
         switch (c)
         {
            case 'l':
               if (argv[optind] != NULL)
               {
                  std::string strPaso(argv[optind]);
                  if ( strPaso.length() > 0 )
                     strArchLog = strPaso ; 
                  break;
               }
            //Si se requieren procesar mas argumentos se deben colocar aqu� el c�digo correspondiente
            //case valorArgumento
         } 
      }
   }
   return true; 
}*/
//<+ V.2.0.03
//######################################################################################################################
CBitacora* Crea_CBitacora()
{
   #ifdef DBG_NO_SRV
      #ifdef PUERTO
         DBG_NS_Puerto = PUERTO;
      #else
         DBG_NS_Puerto = 4006;
      #endif
   #endif
   //CBitacora* bit = new CBitacora(ARCH_BITA); //<- V.2.0.03
   CBitacora* bit = new CBitacora(strArchLog);  //>+ V.2.0.03
   #ifdef DBG
      #ifndef DBG_NO_SRV
         sleep(5);
      #endif
      if (bit)
         bit->setNivel(BIT_DEBUG);
   #endif
   return bit;
}
//######################################################################################################################
CSrvUnx* Crea_CSrvUnx()
{
   return new CSrvARA;
}
//######################################################################################################################
bool libSrvUnx_Ini()
{
   #if defined(DBG) && !defined(DBG_NO_SRV)
      sleep(5);
   #endif
   iniciaLibOpenSSL();
   return true;
}
//######################################################################################################################
bool libSrvUnx_Fin()
{
   terminaLibOpenSSL();
   return true;
}
//######################################################################################################################
//######################################################################################################################
