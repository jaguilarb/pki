/***********************************************************************************************************************
*   PROYECTO:                 PKI SAT
*   MODULO:                   ARA_Mediador: Servicio intermediario entre ARA y la AR
*
*   DESARROLLADO POR:         Silvia Eur�dice Rocha Reyes   SERR
*                             H�ctor Ornelas Arciga         HOA
*
*   FECHA DE INICIO:          Lunes 22, marzo del 2004
*   LENGUAJE:                 C++
*   DESCRIPCI�N:              Atiende solicitudes de informaci�n del SrvARA, cuando este no tiene informaci�n disponible
*                             de forma local, solicita dicha informaci�n a la AR.
*
***********************************************************************************************************************/
VERSIONES

   + V.2.0.0 (2007/03/09) - HOA: Reingenieria del c�digo, se crea librer�a Sgi_Demonio y se utiliza como base del servicio
          .1 (2007-11-21) - GHM: Se incorpor� la funci�n 
                                    void dumpBuffer(const char* texto, const uint8* buffer, int lbuf)
                                 A la librer�a Sgi_MsgPKI


