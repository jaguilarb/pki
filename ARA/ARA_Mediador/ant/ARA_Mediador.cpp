static const char* _ARA_MEDIADOR_CPP_VERSION_ ATR_USED = "ant @(#)"\
"DSIC07412ARA 2007-12-13 ARA_Mediador.cpp 1.1.0/0";

//#VERSION: 1.1.0

//********************************************************************************************************************** 
//********************************************************************************************************************** 
#include<Sgi_Mediador_ARA.h>

//********************************************************************************************************************** 
//********************************************************************************************************************** 
const char* VER_ARA_Mediador = "ARA_Mediador ver.2.00.00.00"; 

CMediador_ARA medARA;
//********************************************************************************************************************** 
//********************************************************************************************************************** 
//********************************************************************************************************************** 
//********************************************************************************************************************** 
//********************************************************************************************************************** 
int main (int argc, char* argv[], char* env[])
{
   iniciaLibOpenSSL();
   int8 res = medARA.procesaParametros(argc, argv, env);
   if (res <= 0)
      return res ? 0 : -1;

   ok = medARA.Inicia() &&
        medARA.Proceso();


   terminaLibOpenSSL();
   return ok ? 0 : 1;
}
//********************************************************************************************************************** 
//********************************************************************************************************************** 




*********;


#include<sys/types.h>
#include<dirent.h>
#include<stdio.h>
#include<signal.h>
#include<unistd.h>
#include<stdlib.h>


int PID;
Mediador_ARA medARA;

int main()
{
   iniciaLibOpenSSL();
   
   char pid[6];
   FILE *Archivo;
   void manejo_Sen(int sig);
   int error;

   if(signal(SIGINT,manejo_Sen)==SIG_ERR)
   {
      perror("signal");
      exit(-1);
   }

   if (signal(SIGPIPE,manejo_Sen) == SIG_ERR)
   {
      perror("signal");
      exit(-2);
   }

   PID = getpid();
   sprintf(pid,"%d",PID);
#ifndef DBG
      Archivo = fopen("/tmp/ARA_Mediador.pID","w+");
#else
      Archivo = fopen("/tmp/ARA_Mediador_dbg.pID","w+");
#endif
   if (Archivo == 0)
       printf("Error al crear el archivo pID (errno %d)\n", errno);
   else
       fputs(pid,Archivo);
   fclose(Archivo);

   error =    medARA.LeeArchivoConf();
   if(error)
   {
      medARA.escribeBit("Error en  LeeArchivoConf() %d .", error);
      return error;
   }


   error   =  medARA.Inicia();
   if(error)
   {
      medARA.escribeBit("Error en  Inicia() %d .", error);
      return error;
   }
   for(;;)
   {
      error   =  medARA.ServicioMediadorARA();
      if(error)
      {
        medARA.escribeBit("Error en ServicioMediadorARA()) %d .", error);
        return error;
      }
   }

   

}
void manejo_Sen(int sig)
{
  medARA.escribeBit("*************************************************************************************************");
  medARA.escribeBit("***************************** FINALIZANDO ARA_MEDIADOR ******************************************");
  medARA.escribeBit("*****************************        SE�AL  %d     **********************************************",sig);
  
  if(sig == SIGPIPE)
  medARA.Desconexion(); 
  terminaLibOpenSSL();

  medARA.escribeBit("*********************** ARA_Mediador FINALIZADO  %d  **********************************************",PID);
  medARA.escribeBit("*************************************************************************************************");
  exit(0);
}

