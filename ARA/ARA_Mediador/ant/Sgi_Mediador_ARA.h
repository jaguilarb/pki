#ifndef _SGI_MEDIADOR_ARA_H_
#define _SGI_MEDIADOR_ARA_H_
static const char* _SGI_MEDIADOR_ARA_H_VERSION_ ATR_USED = "ant @(#)"\
"DSIC07412ARA 2007-12-13 Sgi_Mediador_ARA.h 1.1.0/0";

//#VERSION: 1.1.0
#include <MensajesSAT.h>
#include <SgiOpenSSL.h>
//>>> V.01.01 GHM (070316)
//- #include<ArchivosConfg.h>
//- #include<ArchivosLog.h>
#include <CConfigFile.h>
#include <Sgi_Bitacora.h>
//<<< V.01.01 GHM
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>

#define ERROR_FIFO_NOCREADO   1   
#define ERROR_FIFO_NOABIERTO  2
#define ERROR_FIFO_LECTURA    3	
#define ERROR_FIFO_ESCRITURA  4
#define NO_HAY_PEND           5
#define ERROR_LECT_ARCH_CONF  6
#define ERROR_INICIA          7

//>>> V.01.01.02 GHM (070320): Se omite para que se lea del archivo de configuración
/* >-  V.01.01.02 GHM (070320)
#ifdef DBG
   #define fifo "/tmp/fifoMedARA_DBG"
#else
   #define fifo "/tmp/fifoMedARA" 
#endif
<-  V.01.01.02 GHM (070320) */

class Mediador_ARA
{
   int   error;
   int   puerto;
   int   desc_fifo;
   int   lbuffEscrFifo; 
   int   lnumSerie;
   uint8 iNumARA;
   char  buffEscrFifo[TAM_DATOS];
   //- char  IP[15];              //- V.01.01.02 GHM (070320) 
   //- char  certARA[TAM_DATOS];  //- V.01.01.02 GHM (070320)
   char  numSerie[21];
   char  num_ara[100];
   std::string fifo;          //+ V.01.01.03 GHM (070320)   
   std::string certARA, IP;   //+ V.01.01.02 GHM (070320
   MensajesARA mensARA;
   //- ArchivosLog log;       //- V.01.01.01 GHM (070316)
   CBitacora log;             //+ V.01.01.01 GHM (070316)
   CConfigFile Configuracion; //+ V.01.01.02 GHM (070316)
   
   int         iCiclosPet;    //+ V.01.01.04 GHM (070320)
   CSSL_parms  param;
   CSSL_Skt   *skt;
   Sgi_SSL     ssl;

   void LimpiaVar();

   protected:
      char  buffLectFifo[TAM_DATOS];
      int   lbuffLectFifo;
      
   private:
  
   int CreaFifo();
   int LeeFifo();
   int EscribeFifo();
   int LeePendiente();
   int Conexion();
   int CreaSocket(); 
   int SolCert();
   int Cliente(char* num);
   int Errores(int error,char* Merror);
   int Envia();
   int Recibe();
   int ObtieneDatos();
   int Envia_a();
   int Recibe_a();
   int SolListaCert();

   
   public:

   Mediador_ARA();

   int Inicia();
   int LeeArchivoConf();
   void  escribeBit(char *cDesc , int iError  = -1 );
   int ServicioMediadorARA();   
   int Desconexion();




};
#endif
