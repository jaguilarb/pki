static const char* _SGI_MEDIADOR_ARA_CPP_VERSION_ ATR_USED = "ant @(#)"\
"DSIC07412ARA 2007-12-13 Sgi_Mediador_ARA.cpp 1.1.0/0";

//#VERSION: 1.1.0
#include<Sgi_Mediador_ARA.h>

//>>> V.01.01 GHM (070316): Se definen los path de los archivos de bit�cora y configuraci�n
//#################################################################################
#define PATH_CONF            "/usr/local/SAT/PKI/etc/"
#define PATH_BITA            "/var/local/SAT/PKI/"

#ifdef DBG
   #define ARCH_CONF            PATH_CONF "ARA_Mediador_dbg.cfg"
   #define ARCH_BITA            PATH_BITA "ARA_Mediador_dbg.log"
#else
   #define ARCH_CONF            PATH_CONF "ARA_Mediador.cfg"
   #define ARCH_BITA            PATH_BITA "ARA_Mediador.log"
#endif
//################################################################################
//<<< V.01.01 GHM (070316)

   Mediador_ARA::
   Mediador_ARA():
       log(ARCH_BITA), Configuracion(ARCH_CONF)
   {
      error     = -1;
      desc_fifo =  0;
      lbuffLectFifo = sizeof(buffLectFifo);
      lbuffEscrFifo = sizeof(buffEscrFifo);
      memset(buffEscrFifo,0,sizeof(buffEscrFifo));
      memset(buffLectFifo,0,sizeof(buffLectFifo));
      //>>> V.01.01.01 GHM (070316): Se inicia la bit�cora
      //log = new CBitacora(PATH_BITA);
      /*- #ifndef DBG
         log.Inicia("/var/local/SAT/PKI/ARA_Mediador.log");
      #else
	 log.Inicia("/var/local/SAT/PKI/ARA_Mediador_dbg.log"); 
      #endif*/
      //<<< V.01.01.01 GHM (070316)
   }
//********************************************************************************************************************
int8 MediadorARA::procesaParametros(int argc, char* argv[], char* /* env */ [])
{
   if (argc == 1)
      return 1;

   for (int i = 1; i < argc; i++)
   {
      if (argv[i][0] == '-')
      {
         if (toupper(argv[i][1]) == 'V')
         {
            cout << VER_ARA_Mediador << endl;
            return 0;
         }
      }
   }   
   cout << "Error, parametros inv�lidos" << endl;

   return -1;
}
//********************************************************************************************************************
  int Mediador_ARA::
  Errores(int error,char* Merror)
  {
     char e[TAM_DATOS];
     char err[30];
     memset(e,0,TAM_DATOS);
     
     sprintf(err,"%d",error);
     sprintf(e,"%s:%d",Merror,error);
     mensARA.setMensaje(ERROR_ARA,err,strlen(err),e,strlen(e));
     //log.escribe(0,e); //- V.01.01.01 GHM (070316)
     log.escribe(BIT_ERROR, e); //+ V.01.01.01 GHM (070316)
     return error; 
  }


//********************************************************************************************************************
   void  Mediador_ARA::
   LimpiaVar()
   {
      error     = -1;
      memset(buffLectFifo,0,sizeof(buffLectFifo));
      lbuffLectFifo = sizeof(buffLectFifo);
   }
//********************************************************************************************************************
   int Mediador_ARA::
   CreaFifo()
   {
      //- log.escribe(0,"FIFO a crear  : %s ",fifo); //- V.01.01.03 GHM (070320)
      log.escribePV(BIT_INFO, "FIFO a crear  : %s ", fifo.c_str() ); //+ V.01.01.03 GHM (070316)
      //error = mkfifo(fifo, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH); //- V.01.01.03 GHM (070320)
      error = mkfifo(fifo.c_str(), S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH); //+ V.01.01.03 GHM (070320)
      if((error < 0) && (errno != EEXIST))
         return Errores(errno,"CreaFifo");  
      return 0;

   }
//******************************************************************************************************************
   int Mediador_ARA::
   LeeFifo()
   {
      //desc_fifo = open(fifo,O_RDONLY); //- V.01.01.03 GHM (070320)
      desc_fifo = open(fifo.c_str(),O_RDONLY); //+ V.01.01.03 GHM (070320)
      if (desc_fifo < 0)
         return  Errores(ERROR_FIFO_NOABIERTO,"LeeFifo");
      lbuffLectFifo = sizeof(buffLectFifo);   
      memset(buffLectFifo,0,lbuffLectFifo);
      return  read(desc_fifo,buffLectFifo,lbuffLectFifo) ? 0 : Errores(errno,"LeeFifo");

   }
//******************************************************************************************************************
   int Mediador_ARA::
   EscribeFifo()
   {

      //desc_fifo = open(fifo,O_WRONLY); //- V.01.01.03 GHM (070320)
      desc_fifo = open(fifo.c_str(), O_WRONLY); //+ V.01.01.03 GHM (070320)
      //- log.escribe(0,"FIFO a Escribir  : %s", fifo); //- V.01.01.02 GHM (070316)
      log.escribePV(BIT_INFO, "FIFO a Escribir  : %s ", fifo.c_str()); //+ V.01.01.01 GHM (070316)
      if(desc_fifo < 0)
         return  Errores(ERROR_FIFO_NOABIERTO,"EscribeFifo");

      error =  write(desc_fifo,buffLectFifo,lbuffLectFifo);
      if(error != lbuffLectFifo)
        return  Errores(errno,"EscribeFifo:Error");
      else 
      { 
         //- log.escribe(0,"Bytes Escritos %d  Operacion Enviada %d ",error, mensARA.tipOperacion()); //- V.01.01.01 GHM 
         log.escribePV(BIT_INFO, "Bytes Escritos %d  Operacion Enviada %d ",error,
                       mensARA.tipOperacion()); //+ V.01.01.01 GHM (070316)
        close(desc_fifo);
         return 0; 
      }

   }

//******************************************************************************************************************
   int Mediador_ARA::
   LeePendiente()
   {
      struct timeval t_espera;
      fd_set fifos;
       
      FD_ZERO(&fifos);
      FD_SET(desc_fifo,&fifos);

      t_espera.tv_sec = 30;
      t_espera.tv_usec = 0;

      error = select(desc_fifo +1 ,&fifos,NULL,NULL,&t_espera);
      if( error > 0 )
      {
         if( FD_ISSET(desc_fifo,&fifos) )
            printf("Hay algo ....");
      } 
 
     return 0; 

   }

//******************************************************************************************************************
   int Mediador_ARA::
   Inicia()
   {
      //iNumARA = atoi(num_ara); //- V.01.01.02 GHM (070320): Se hace la conversi�n en donde se lee el dato
      error = CreaFifo();
      if(error)
         return Errores(error,"ServicioMediadorARA:CreaFifo");
      // - error = mensARA.Inicia(certARA); //- V.01.01.02 GHM (070320):
      error = mensARA.Inicia((char*) certARA.c_str());
      if(error)
         return Errores(ERROR_INICIA,"IniciaMensARA");
      mensARA.setVersion(1);
      mensARA.setDestino(iNumARA);
      //return error;
      return Errores(Conexion(),"Conexion ARA");
          
   }	 


//******************************************************************************************************************
   int Mediador_ARA::
   Conexion()
   { 
      error = CreaSocket();	   
      if(error)
         return Errores(error,"Conexion:CreaSocket");	      
      error = mensARA.ConectaCliente(skt);
      if(error)
         return  Errores(error,"Conexion");
      return error;
   }

//******************************************************************************************************************
   int Mediador_ARA::
   Desconexion()
   {
       error = mensARA.setMensaje(SOLDESCONEXIONARA);
       if(error)
          Errores(error,"Desconexion");	        
       error = Envia();
       if(error)
	  Errores(error,"Desconexion:Envia");     
       ssl.Termina();
       delete skt;
       Errores(error,"Desconexion");
       return error;        
   
   }	

//******************************************************************************************************************
   int Mediador_ARA::
   CreaSocket()
   {
      char prueba[100];
      sprintf(prueba,"%s","Esta es una prueba");
      //- param.SetCliente( EPlano ,NULL,IP,puerto ,prueba,"dos","tres","cuatro",NULL,NULL); //- V.01.01.02 GHM (070320)
      param.SetCliente( EPlano ,NULL, IP.c_str(), puerto, prueba, "dos", "tres", "cuatro",
                        NULL, NULL); //+ V.01.01.02 GHM (070320)
      ssl.Inicia();
      return  Errores(ssl.Cliente(param,skt),"CreaSocket");
 
   }

//>>> V.01.01.02 GHM (070316): Se redefinio la forma de leer la configuraci�n al agregar el uso de la librer�a CConfigFile
//******************************************************************************************************************
int Mediador_ARA::
LeeArchivoConf()
{
/* >-  V.01.01.02 GHM
     ArchivosConfg aConfigura;
#ifndef DBG
      char archConf[]="/usr/local/SAT/PKI/etc/ARA_Mediador.cfg";
#else
      char archConf[]="/usr/local/SAT/PKI/etc/ARA_Mediador_dbg.cfg";
#endif 
   char* variables[]={"CERTARA","IP","PUERTO","ARA"}; 
   char puerto_c[10];
<-  V.01.01.02 GHM */

   std::string puerto_c, num_ara;
   std::string CiclosPet;
/* >-  V.01.01.02 GHM
   memset(puerto_c ,0 ,sizeof(puerto_c));
   memset(certARA  ,0 ,sizeof(certARA));
   memset(num_ara  ,0 ,sizeof(num_ara));
   memset(IP       ,0 ,sizeof(IP));
<-  V.01.01.02 GHM */
   int error = Configuracion.cargaCfgVars(); //+ V.01.01.02 GHM (070320)
   if (error) //+ V.01.01.02 GHM (070320)
   {
      log.escribePV(BIT_ERROR, "Problemas al leer el archivo de configuraci�n (%d): %s",
      error, ARCH_CONF); //+ V.01.01.02 GHM (070320)
      return error;
   } 
   /* >-  V.01.01.02 GHM
   error = aConfigura.VarConfg(4,variables,archConf,certARA,IP,puerto_c,num_ara); 
   if(error)
      return Errores(error,"LeeArchivoConf:VarConfg"); <- V.01.01.02 GHM */
   int r1, r2, r3, r4;  //+ V.01.01.02 GHM (070320)
   int r5;              //+ V.01.01.03 GHM (070320)
   int r6;

   r1 = Configuracion.getValorVar("[ARA]" , "CERTARA", &certARA); //+ V.01.01.02
   r2 = Configuracion.getValorVar("[HOST]", "IP"     , &IP);      //+ V.01.01.02
   r3 = Configuracion.getValorVar("[HOST]", "PTO"    , &puerto_c);//+ V.01.01.02
   r4 = Configuracion.getValorVar("[ARA]" , "NUMARA" , &num_ara); //+ V.01.01.02
   r5 = Configuracion.getValorVar("[ARA]" , "FIFO"   , &fifo);    //+ V.01.01.03
   r6 = Configuracion.getValorVar("[ARA]" , "TAMCICLO",&CiclosPet);//+ V.01.01.04

   if (r1 || r2 || r3 || r4 || r5 || r6 ) //+ V.01.01.02 GHM (070320)
   {
      log.escribePV(BIT_ERROR, "Error al leer los par�metros de configuraci�n CERTARA(%d), IP(%d), PTO(%d), NUMARA(%d) "
                    "FIFO(%d) y TAMCICLO (%d)", r1, r2, r3, r4, r5, r6); //+ V.01.01.02 GHM (070320)
      return ERROR_LECT_ARCH_CONF; //+ V.01.01.02 GHM (070320)
   }
   log.escribePV(BIT_DEBUG, "Los par�metros de configuraci�n le�dos son: "
                            "CertARA(%s), Host(%s), Puerto(%s), numARA(%s), fifo(%s) y tamciclo(%s)",
                 certARA.c_str(), IP.c_str(), puerto_c.c_str(), num_ara.c_str(), fifo.c_str(),
                 CiclosPet.c_str() ); //+ V.01.01.02 GHM (070320)
   puerto = atoi(puerto_c.c_str());
   iNumARA = atoi(num_ara.c_str()); 
   iCiclosPet = atoi(CiclosPet.c_str()); //+ V.01.01.04 GHM (070320)
   return EXITO;
}

//<<< V.01.01.02 GHM (070316)

//*****************************************************************************************************************

   int Mediador_ARA::
   SolListaCert()
   {
      char bufferARA[TAM_DATOS];
      char rfc[14];
      char tipCert[2];    
      char edo[2];
      char limite[10]; 
       
 
      int lbufferARA = sizeof(bufferARA);
      int lrfc       = sizeof(rfc);
      int ltipCert   = sizeof(ltipCert); 
      int ledo       = sizeof(edo);  
      int llimite    = sizeof(limite);  

      memset(bufferARA,0,lbufferARA);
      memset(rfc      ,0,lrfc);          
      memset(tipCert  ,0,ltipCert);
      memset(edo      ,0,ledo);
      memset(limite   ,0,llimite);
      
      mensARA.setDestino(iNumARA);

      error =  mensARA.getMensaje(SOLLISTACERTRFC,rfc,&lrfc,tipCert,&ltipCert,edo,&ledo,limite,&llimite);
      if(error)
         return sgiErrorBase(error);
      error = mensARA.setMensaje(SOLLISTACERTRFC,rfc,lrfc,tipCert,ltipCert,edo,ledo,limite,llimite); 
      if(error)
         return sgiErrorBase(error);

      //- log.escribe(0,"Solicitud de Listas  de Certificados %s",rfc); //- V.01.01.01 GHM (070316)
      log.escribePV(BIT_INFO, "Solicitud de Listas  de Certificados %s",rfc); //+ V.01.01.01 GHM (070316)
      error = Envia();
      if(error)
      {
          //- log.escribe(0,"Error en Envia"); //- V.01.01.01 GHM (070316)
          log.escribe(BIT_ERROR, "Error en Env�a"); //+ V.01.01.01 GHM (070316)
          error = CreaSocket();
          if(error)
             return Errores(error,"SolCert:CreaSocket");
          //- log.escribe(0,"Conectandose..."); //- V.01.01.01 GHM (070316)
          log.escribe(BIT_INFO, "Conect�ndose.."); //+ V.01.01.01 GHM (070316)
          error = Conexion();
          if(error)
              return Errores(error,"SolCert:Conexion");
          //- log.escribe(0,"Re-envio de peticion"); //- V.01.01.01 GHM (070316)
          log.escribe(BIT_INFO, "Re-env�o de petici�n"); //+ V.01.01.01 GHM (070316)
          error = Envia();
          if(error)
            return Errores(error,"SolListaCert:Envia:Envia");

      }
      error = Recibe();
      if(error)
      {   /* >>> - V.01.01.01 GHM (070316)
          log.escribe(0,"Error en Recibe");
          log.escribe(0,"Conectandose..."); <<< - V.01.01.01 GHM (070316) */
          log.escribe(BIT_ERROR, "Error en Recibe");//+ V.01.01.01 GHM (070316)
          log.escribe(BIT_INFO, "Conect�ndose..."); //+ V.01.01.01 GHM (070316)
          error = Conexion();
          if(error)
             return Errores(error,"SolCert:Conexion");
          //- log.escribe(0,"Re-envio de peticion"); //- V.01.01.01 GHM (070316)
          log.escribe(BIT_INFO, "Re-env�o de petici�n"); //+ V.01.01.01 GHM (070316)
          error = Envia_a();
          if(error)
             return Errores(error,"SolCert:Envia:Envia");
          //- log.escribe(0,"Recepcion  de peticion"); //- V.01.01.01 GHM (070316)
          log.escribe(BIT_INFO, "Recepci�n de petici�n");//+ V.01.01.01 GHM (070316)
          error = Recibe();
          if(error)
            return Errores(error,"SolListaCert:CreaSocket");
      } 
      switch(mensARA.tipOperacion())
      {
         case LISTACERT:{
                           memset(buffLectFifo,0,sizeof(buffLectFifo));
                           lbuffLectFifo = sizeof(buffLectFifo);
                           //- log.escribe(0,"LISTACERT"); // V.01.01.01 GHM (070316)
                           log.escribe(BIT_INFO, "LISTACERT"); //+ V.01.01.01 GHM (070316)
                           error = mensARA.getMensaje(LISTACERT,bufferARA,&lbufferARA);
                           if(error)  
                              return Errores(sgiErrorBase(error),"SolCert:ObtieneDatos");
                           error = mensARA.setMensaje(LISTACERT,bufferARA,lbufferARA);
                           if(error)
                              return Errores(sgiErrorBase(error),"SolCert:ObtieneDatos");
                           error = mensARA.setDatos(buffLectFifo,&lbuffLectFifo);
                           if(error)
                              return Errores(sgiErrorBase(error),"SolCert:ObtieneDatos");
                           error = EscribeFifo();

                           break;
                        } 
         case CERTREGARA:{
                          //-log.escribe(0,"CERTREGARA"); //- V.01.01.01 GHM (070316)
                          log.escribe(BIT_INFO, "CERTREGARA"); //+ V.01.01.01 GHM (070316)
                          error = ObtieneDatos();
                          if(error)
                             return Errores(sgiErrorBase(error),"SolCert:ObtieneDatos");
                          //- V.01.01.01 GHM (070316)
                          //-log.escribe(0,"SolCert:Recibe:Peticion %d NumSerie %s",mensARA.tipOperacion(),numSerie);
                          log.escribePV(BIT_INFO,"SolCert:Recibe:Petici�n %d NumSerie %s",mensARA.tipOperacion(),
                                        numSerie); //+ V.01.01.01 GHM (070316)
                          error = EscribeFifo();
                          break;
                       }
         case CERTNOEXISTE:
         case ERRBDREGARA:
         case ERRFTPARA:
	      case ERROR_ARA:			 
			 
                         {
                            //- log.escribe(0,"Mensaje Recibido %d",mensARA.tipOperacion());; //- V.01.01.01 GHM (070316)
                            log.escribePV(BIT_ERROR, "Mensaje Recibido %d",
                                        mensARA.tipOperacion()); //+ V.01.01.01 GHM (070316)
                            error = EscribeFifo();
                            break;
                         }

     }//Switch



      return error;


   }
//*****************************************************************************************************************
   int Mediador_ARA::
   SolCert()
   {
      mensARA.setDestino(iNumARA);
      lnumSerie = sizeof(numSerie);
      error =  mensARA.getMensaje(SOLCERTARA,numSerie,&lnumSerie);
      if(error)
         return sgiErrorBase(error);
      //log.escribe(0,"Solicitud de Certificado %s",numSerie); //- V.01.01.01 GHM (070316)
      log.escribePV(BIT_INFO,"Solicitud de Certificado %s", numSerie); //+ V.01.01.01 GHM (070316)
      error = Envia();   
      if(error)
      {   
         //- log.escribe(0,"Error en Envia"); //- V.01.01.01 GHM (070316) 
         log.escribe(BIT_INFO, "Error en Env�a"); //+ V.01.01.01 GHM (070316)
         error = CreaSocket();  
         if(error)
             return Errores(error,"SolCert:CreaSocket");
         //- log.escribe(0,"Conectandose..."); //- V.01.01.01 GHM (070316)
         log.escribe(BIT_INFO, "Conect�ndose"); //+ V.01.01.01 GHM (070316)
         error = Conexion();
         if(error)
             return Errores(error,"SolCert:Conexion");
         //- log.escribe(0,"Re-envio de peticion"); //- V.01.01.01 GHM (070316)
         log.escribe(BIT_INFO, "Re-env�o de petici�n"); //+ V.01.01.01 GHM (070316)
         error = Envia();
         if(error)
             return Errores(error,"SolCert:Envia:Envia"); 
       
      }
      error = Recibe();
      if(error)
      {
         //- log.escribe(0,"Error en Recibe"); //- V.01.01.01 GHM (070316)
         log.escribe(BIT_ERROR, "Error en Recibe"); //+ V.01.01.01 GHM (070316)
          error = CreaSocket();
         if(error)
             return Errores(error,"SolCert:CreaSocket");
         //log.escribe(0,"Conectandose..."); //- V.01.01.01 GHM (070316)
         log.escribe(BIT_INFO,"Conect�ndose..");    //+ V.01.01.01 GHM (070316)
         error = Conexion();
         if(error)
             return Errores(error,"SolCert:Conexion");
         //- log.escribe(0,"Re-envio de peticion"); //- V.01.01.01 GHM (070316)
         log.escribe(BIT_INFO, "Re-env�o de petici�n");//+ V.01.01.01 GHM (070316)
         error = Envia_a();
         if(error)
             return Errores(error,"SolCert:Envia:Envia");
         //- log.escribe(0,"Recepcion  de peticion"); //- V.01.01.01 GHM (070316)
         log.escribe(BIT_INFO, "Recepci�n de petici�n"); //+ V.01.01.01 GHM (070316)
         error = Recibe();
         if(error)
             return Errores(error,"SolCert:Envia:Envia");
    } 
      
    switch(mensARA.tipOperacion())
    {  
       case CERTREGARA:{  
                          //- log.escribe(0,"CERTREGARA"); //- V.01.01.01 GHM (070316) 
                          log.escribe(BIT_INFO,"CERTREGARA");     //+ V.01.01.01 GHM (070316)
                          error = ObtieneDatos();
                          if(error)
                             return Errores(sgiErrorBase(error),"SolCert:ObtieneDatos");
                          //- log.escribe(0,"SolCert:Recibe:Peticion %d NumSerie %s",
                          //-            mensARA.tipOperacion(),numSerie); //- V.01.01.01 GHM (070316)
                          log.escribePV(BIT_INFO,"SolCert:Recibe:Petici�n %d NumSerie %s",
                                      mensARA.tipOperacion(),numSerie);  //+ V.01.01.01 GHM (070316)
                          error = EscribeFifo();
                          break; 
                       }
       case CERTNOEXISTE:
       case ERRBDREGARA:
       case ERRFTPARA:     
       case ERROR_ARA:
                         {
                            //- log.escribe(0,"Mensaje Recibido %d",mensARA.tipOperacion()); //- V.01.01.01 GHM (070316)
                            log.escribePV(BIT_ERROR,"Mensaje Recibido %d",
                                          mensARA.tipOperacion()); //+ V.02.01.01 GHM (070316)
                            error = EscribeFifo();
                            break;
                         }
   }
         
      return error;
   }


//*****************************************************************************************************************
   int Mediador_ARA::
   ServicioMediadorARA()
   {
       	 
      char bufferAuxiliar[TAM_DATOS];
      int  lbufferAuxiliar = sizeof(bufferAuxiliar);
      //- log.escribe(0,"ARA_Mediador Iniciado"); //- V.01.01.01 GHM (070316)
      log.escribe(BIT_INFO, "ARA_Mediador Iniciado"); //+ V.01.01.01 GHM (070316)
      for(;;)
      {
         //desc_fifo = open(fifo,O_RDONLY); //- V.01.01.03 GHM (070320)
         desc_fifo = open(fifo.c_str(), O_RDONLY); //- V.01.01.03 GHM (070320)
         if(desc_fifo < 0)
            return  Errores(ERROR_FIFO_NOABIERTO,"ServicioMediadorARA");

         lbuffLectFifo = sizeof(buffLectFifo);
         lbufferAuxiliar = sizeof(bufferAuxiliar);
         memset(buffLectFifo,0,lbuffLectFifo);
         memset(bufferAuxiliar,0,sizeof(bufferAuxiliar)); 
           
         error = read(desc_fifo,buffLectFifo,lbuffLectFifo);
         if(error < 0)
             Errores(errno,"AlLeerFifo");
         close(desc_fifo);  
	 
	      memcpy(bufferAuxiliar,buffLectFifo,lbuffLectFifo);
	      lbufferAuxiliar = lbuffLectFifo;
	 
         error = mensARA.getDatos(buffLectFifo,lbuffLectFifo);
         if(error)
            return Errores(sgiErrorBase(error),"ServicioMediadorARA:getDatos");

	 
         switch(mensARA.tipOperacion())
         {
            
            case SOLCERTARA:{ //-  log.escribe(0,"--->Solicitud de Certificado"); //- V.01.01.01 GHM (070316)
                              log.escribe(BIT_INFO, "--->Solicitud de Certificado"); //+ V.01.01.01 GHM (070316)
			                      //error =  Conexion();	    
			                      //if(error)
			                      //   Errores(errno,"ServicioMediadorARA:Conexion");	        
			                      error = mensARA.getDatos(bufferAuxiliar,lbufferAuxiliar);
            		             if(error)
				                      return Errores(sgiErrorBase(error),"ServicioMediadorARA:getDatos");
                               error = SolCert();
			                      if(error)
			 	                      EscribeFifo();
			                      //Desconexion(); 

                               //- log.escribe(0,"--->Fin de Solicitud"); //- V.01.01.01 GHM (070316)
                               log.escribe(BIT_INFO, "--->Fin de Solicitud"); //+ V.01.01.01 GHM (070316)
                               break;
                            }

            case SOLLISTACERTRFC:{
                               //- log.escribe(0,"--->Solicitud Lista de Certificados"); //- V.01.01.01 GHM (070316)
                               log.escribe(BIT_INFO, "--->Solicitud Lists de Certificados"); //+ V.01.01.01 GHM (070316)
                               //error =  Conexion();
                               //if(error)
                               //   return  Errores(errno,"ServicioMediadorARA:Conexion");
			        
                                error = mensARA.getDatos(bufferAuxiliar,lbufferAuxiliar);
                                if(error)
                                  return Errores(sgiErrorBase(error),"ServicioMediadorARA:getDatos");

                                error = SolListaCert();
			                       if(error)
                                   EscribeFifo();	
                                //Desconexion();
                                //- log.escribe(0,"--->Fin de Solicitud"); //- V.01.01.01 GHM (070316)
                                log.escribe(BIT_INFO,"--->Fin de Solicitud"); //+ V.01.01.01 GHM (070316)
                                break;
				 
                               }
            default:{ Errores(0,"Operacion no esperada"); EscribeFifo();break;}  

         }
      }
   }
//*****************************************************************************************************************
  int Mediador_ARA::
  Cliente(char* num) 
  {
     MensajesARA mensARA_aux;
     char Dato[TAM_DATOS];
     int  lDato = sizeof(Dato);
     
     error = LeeArchivoConf();
      if(error)
         return ERROR_LECT_ARCH_CONF;
     //- error = mensARA.Inicia(certARA); //- V.01.01.02 GHM (070320)
     error = mensARA.Inicia((char*) certARA.c_str()); //+ V.01.01.02 GHM (070320)
     if(error)
         return error;
     error = mensARA_aux.Mensajes::Inicia(1,"/usr/local/SAT/PKI/Certificados/AR_SAT1024.cer",
                                  "/usr/local/SAT/PKI/Certificados/AR_SAT1024.key",
                                  "arsat1024",strlen("arsat1024"),
                                  "/usr/local/SAT/PKI/Certificados/AR_SAT1024.cer");
    if(error)
         return error;
     //mensARA.setMensaje(SOLCERTARA,num,strlen(num));
     error = mensARA_aux.setMensaje(SOLLISTACERTRFC,num,strlen(num), "1",1,"A",1,"6",1);
     if(error)
        return error;
     error = mensARA_aux.setDatos(buffLectFifo,&lbuffLectFifo);

     EscribeFifo();
     //- log.escribe(0,"Cliente:Escribe %s",num); //- V.01.01.01 GHM (070316)
     log.escribePV(BIT_INFO, "Cliente:Escribe %s",num); //+ V.01.01.01 GHM (070316)
     error = LeeFifo();
     error =  mensARA_aux.getDatos(buffLectFifo,lbuffLectFifo);
     int ope = mensARA_aux.tipOperacion();
     error = mensARA_aux.getMensaje(ope,buffEscrFifo,&lbuffEscrFifo,Dato,&lDato );
     //- log.escribe(0,"Cliente:Lee:Operacion %d NumSerie %s",ope,buffEscrFifo); //- V.01.01.01 GHM (070316)
     log.escribePV(BIT_INFO, "Cliente:Lee:Operacion %d NumSerie %s",ope,buffEscrFifo); //+ V.01.01.01 GHM (070316)
     printf("Mensaje Recibido %d  Num_serie %s",ope,buffEscrFifo);
     return 0;
  }

   int Mediador_ARA::
   ObtieneDatos()
   {
      error =  mensARA.getMensaje(SOLCERTARA,numSerie,&lnumSerie);
      if(error)
         return sgiErrorBase(error);
      memset(buffLectFifo,0,strlen(buffLectFifo));
      error = mensARA.setDatos(buffLectFifo,&lbuffLectFifo);
      if(error)
         return Errores(sgiErrorBase(error),"SolCert:setDatos");
      //- log.escribe(0,"ObtieneDatos %d",error); //- V.01.01.01 GHM (070316)
      log.escribePV(BIT_INFO, "ObtieneDatos %d", error); //+ V.01.01.01 GHM (070316)
      return error;
   }

   int Mediador_ARA::
   Envia()
   {
      memset(buffLectFifo,0,sizeof(buffLectFifo));
      lbuffLectFifo = sizeof(buffLectFifo);
      error = mensARA.setDatos(buffLectFifo,&lbuffLectFifo);
      if(error)
         return Errores(sgiErrorBase(error),"SolCert:setDatos");
      memcpy(buffEscrFifo,buffLectFifo,lbuffLectFifo);
      lbuffEscrFifo = lbuffLectFifo;
      return   skt->Envia((uint8 *)buffLectFifo,lbuffLectFifo);
       
   }
   int Mediador_ARA::
   Recibe()
   {
      lbuffLectFifo = sizeof(buffLectFifo);
      memset(buffLectFifo,0,strlen(buffLectFifo));
      error =  skt->Recibe((uint8 *)buffLectFifo,lbuffLectFifo);
      if(error)
         return Errores(error,"SolCert:Recibe");

      error = mensARA.getDatos(buffLectFifo,lbuffLectFifo);
      if(error)
          return Errores(sgiErrorBase(error),"SolCert:getDatos");
      //- log.escribe(0,"Recibe %d",error); //- V.01.01.01 GHM (070316)
      log.escribePV(BIT_INFO, "Recibe %d",error);//+ V.01.01.01 GHM (070316)
      return error;
   }
 
  int Mediador_ARA::
  Envia_a()
   {
      return   skt->Envia((uint8 *)buffEscrFifo,lbuffEscrFifo);

   }

   int Mediador_ARA::
   Recibe_a()
   {
      lbuffEscrFifo = sizeof(buffEscrFifo);
      memset(buffEscrFifo,0,sizeof(buffEscrFifo));
      error =  skt->Recibe((uint8 *)buffEscrFifo,lbuffEscrFifo);
      if(error)
         return Errores(error,"SolCert:Recibe");

      error = mensARA.getDatos(buffLectFifo,lbuffLectFifo);
      if(error)
          return Errores(sgiErrorBase(error),"SolCert:getDatos");
      //- log.escribe(0, "Recibe_a %d",error); //- V.01.01.01 GHM (070316)
      log.escribePV(BIT_INFO, "Recibe_a %d", error); //+ V.01.01.01 GHM (070316)
      return error;
   }

void  Mediador_ARA::
escribeBit(char *cDesc , int iError)
{
  if(iError == -1)
     //- log.escribe(0, cDesc ); //- V.01.01.01 GHM (070316)
     log.escribe(BIT_INFO, cDesc); //+ V.01.01.01 GHM (070316)
  else
     //- log.escribe(0, cDesc ,iError); //- V.01.01.01 GHM (070316)
     log.escribePV(BIT_INFO, cDesc ,iError); //+ V.01.01.01 GHM (070316)
}



