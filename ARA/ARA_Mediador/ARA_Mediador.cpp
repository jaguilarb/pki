static const char* _ARA_MEDIADOR_CPP_VERSION_ ATR_USED = "ARA_Mediador @(#)"\
"DSIC07412ARA 2007-12-13 ARA_Mediador.cpp 1.1.0/0";

//#VERSION: 1.1.0
//#####################################################################################################################
#include <signal.h>
#include <sys/select.h>
#include <fcntl.h>

#include <ARA_Mediador.h>
#include <Sgi_OpenSSL.h>
#include <Sgi_ConfigFile.h>
//#####################################################################################################################
const char* VER_ARA_Mediador = "ARA_Mediador ver.2.00.00.00"; 
//#####################################################################################################################
bool CARA_Mediador::Procesar = true;
//#####################################################################################################################

/* //>>- V.2.0.1 GHM (20071121): Se deshabilita para utilizar la funci�n de la librer�a de Sgi_MsgPKI
#ifdef DBG
void dumpBuffer(const char* texto, const uint8* buffer, int lbuf)
{
   std::string msg(texto), hex, asc;
   char tmp[64];
   int i, j;

   sprintf(tmp, " (%d bytes):\n\t", lbuf);
   msg += tmp; 

   for (i = j = 0; i < lbuf; i++, j++)
   {
      if (j == 16)
      {
         (((msg += hex) += "   ") += asc) += "\n\t";  
         //>+ GHM (071106): Se agreg� la directiva porque para el compilador utilizado en rh72
         //                 no es soportada la funci�n clear.
#ifdef rh72
         hex.erase(0, hex.length());
         asc.erase(0, asc.length());
#else
         hex.clear();
         asc.clear();
#endif
         j = 0;
      }
      sprintf(tmp, "%02X ", buffer[i]);
      hex += tmp;

      sprintf(tmp, "%c"  , buffer[i] < 32 ? '.' :  buffer[i]);
      asc += tmp;
   }

   for (; j < 16; j++)
      hex += "   ";

   ((msg += hex) += "   ") += asc;      
   Bitacora->escribe(BIT_DEBUG, msg.c_str());
}
#endif*/
//<<- V.2.0.1 GHM (20071121)

//#####################################################################################################################
CARA_Mediador::CARA_Mediador()
   : CDemonio(), FdFifoSol(-1), FdFifoResp(-1), ID(0), ARA_CD(), pathFifo(), MaxInactividad(0), AR_IP(), AR_Puerto(0), MensARA(), Skt(NULL) 
{
}
//#####################################################################################################################
CARA_Mediador::~CARA_Mediador()
{
   CierraFifo(true);
   CierraFifo(false);
}
//#####################################################################################################################
bool CARA_Mediador::CargaCfg()
{
   std::string id, inactivo, pto;
   CConfigFile cfg(ARCH_CONF);
   
   int error = cfg.cargaCfgVars(); 
   if (error) 
   {
      Bitacora->escribePV(BIT_ERROR, "Problemas al leer el archivo de configuraci�n (%d): %s", error, ARCH_CONF); 
      return false;
   } 

   int r1 = cfg.getValorVar("[ARA]" , "ID"      , &id);
   int r2 = cfg.getValorVar("[ARA]" , "CERTARA" , &ARA_CD); 
   int r3 = cfg.getValorVar("[ARA]" , "FIFO"    , &pathFifo);  
   int r4 = cfg.getValorVar("[ARA]" , "INACTIVO", &inactivo);
   int r5 = cfg.getValorVar("[AR]"  , "IP"      , &AR_IP);      
   int r6 = cfg.getValorVar("[AR]"  , "PUERTO"  , &pto);
   int r7 = cfg.getValorVar("[ARA]" , "EFIFO"   , &pathFifo_escritura);	//>+ ERGL (070528)

   if (r1 || r2 || r3 || r4 || r5 || r6 || r7)	//>> ERGL (070528) 
   {
      Bitacora->escribePV(BIT_ERROR, "Error al leer los par�metros de configuraci�n ID(%d), CERTARA(%d), FIFO(%d), INACTIVO(%d) "
                    "AR IP(%d), PUERTO (%d) y FIFO de escritura (%d)", r1, r2, r3, r4, r5, r6, r7); 
      return false;
   }

   Bitacora->escribePV(BIT_DEBUG, "Los par�metros de configuraci�n le�dos son: "
                            "ID(%s), CertARA(%s), fifo(%s), fifo escritura(%s), inactivo(%s), AR ip(%s), AR puerto(%s)",
                 id.c_str(), ARA_CD.c_str(), pathFifo.c_str(), pathFifo_escritura.c_str(), inactivo.c_str(), AR_IP.c_str(), pto.c_str()); 
   AR_Puerto = atoi(pto.c_str()); 
   ID = atoi(id.c_str()); 
   MaxInactividad = atoi(inactivo.c_str()); 

   return true;
}
//#####################################################################################################################
bool CARA_Mediador::CreaFifo()
{
   bool res = true;

   if (access(pathFifo.c_str(), F_OK) && mkfifo(pathFifo.c_str(), 0660))
   {
      Bitacora->escribePV(BIT_ERROR, "No se pudo crear el FIFO de comunicaci�n con SrvARA (%d)", errno);
      res = false;  
   }

   return res;
}
//#####################################################################################################################
bool CARA_Mediador::ConectaAR()
{
   intE error = MensARA.Inicia(ARA_CD.c_str());
   if (error)
   {
      Bitacora->escribe(BIT_ERROR, error, "Problemas al iniciar el objeto de mensajes ARA");
      return false;
   }
   
   MensARA.setVersion(1);
   MensARA.setDestino(ID);

   #ifdef DBG
      struct timeval espera = {300, 0 };
   #else
      struct timeval espera = { 60, 0 };
   #endif

   CSSL_parms  param;
   param.SetCliente(EPlano, &espera, AR_IP.c_str(), AR_Puerto);

   error = Sgi_SSL::Cliente(param, Skt);
   if (error)
   {
      Bitacora->escribe(BIT_ERROR, error, "Problemas de conexi�n a la AR (socket)");
      return false;
   }

   Skt->setKEEPALIVE(1800, 2,60);

   error = MensARA.ConectaCliente(Skt);   
   if (error)
   {
      Bitacora->escribe(BIT_ERROR, error, "Problemas de conexi�n a la AR (socket)");
      return false;
   }
   
   return true;
}
//#####################################################################################################################
bool CARA_Mediador::Inicia ()
{
   iniciaLibOpenSSL();
   Sgi_SSL::Inicia();
   bool ok = CargaCfg() &&
             CreaFifo() &&
             ConectaAR(); 
   return ok;
}
//#####################################################################################################################
bool CARA_Mediador::Termina()
{
   Sgi_SSL::Termina();
   terminaLibOpenSSL();
   return true;
}
//#####################################################################################################################
void CARA_Mediador::ManejadorSignals(int senal)
{
   Bitacora->escribePV(BIT_INFO, "Se recibi� la se�al '%d'", senal);
   switch (senal)
   {
      case SIGINT: Procesar = false; break;
   }
}
//#####################################################################################################################
bool CARA_Mediador::setManejadorSignals()
{
   int senales[] = { SIGINT };
   struct sigaction accion;

   memset(&accion, 0, sizeof accion);
   accion.sa_handler = ManejadorSignals;
   accion.sa_flags   =  SA_SIGINFO;
   
   for (uint i = 0; i < (sizeof(senales) / sizeof(int)); i++)
   {
     if (sigaction(senales[i], &accion, NULL))
     {
        Bitacora->escribePV(BIT_ERROR, "Problemas al establecer el manejador de se�ales (se�al=%d, errno=%d)", senales[i], errno);
        return false;
     } 
   }   
   return true;
}
//#####################################################################################################################
bool CARA_Mediador::AbreFifo(bool solicitud)
{
   int *fifo, flag = 0;
   std::string pathArch;

   if (solicitud)
   {
      fifo = &FdFifoSol;
      flag = O_RDONLY | O_NONBLOCK;
      pathArch = pathFifo;
   }
   else
   {
      fifo = &FdFifoResp;
      flag = O_WRONLY;
      pathArch = pathFifo_escritura + "." + (ProcCli + 2);
   }

   if (*fifo == -1)
   {
      *fifo = open(pathArch.c_str(), flag);
      if (*fifo == -1)
         Bitacora->escribePV(BIT_ERROR, "Error al abrir el archivo FIFO (%d): %s", errno, pathArch.c_str());
   }
   return *fifo != -1;
}
//#####################################################################################################################
void CARA_Mediador::CierraFifo(bool solicitud)
{
   if (solicitud)
   {
      if (FdFifoSol != -1)
      {
         close(FdFifoSol);
         FdFifoSol = -1;
      }
   }
   else
   {
      if ( FdFifoResp != -1 )
      {
         close(FdFifoResp);
         FdFifoResp = -1;
      }
   }
}
//#####################################################################################################################
int CARA_Mediador::HaySolicitud()
{
   if (!AbreFifo(true))
      return false;

   fd_set fdALeer;
   struct timeval tv;

   FD_ZERO(&fdALeer);
   FD_SET(FdFifoSol, &fdALeer);
   tv.tv_sec = 1;
   tv.tv_usec = 0;
   
   int disp = select(FdFifoSol + 1, &fdALeer, NULL, NULL, &tv);
   if (disp == -1)
      Bitacora->escribePV(BIT_ERROR, "Error al consultar si el FIFO tiene peticiones pendientes (%d)", errno);
   else if (disp > 0)
      Bitacora->escribePV(BIT_DEBUG, "El select() indica que hay lectura pendiente en el fifo (%d, %d)", disp, FD_ISSET(FdFifoSol, &fdALeer));

   return disp;
}
//#####################################################################################################################
bool CARA_Mediador::LeeIdProceso()
{
   bool ok = false;

   ssize_t res = read(FdFifoSol, ProcCli, 1);
   if (!res || (res == -1 && errno == EAGAIN))
      return false;

   if (res == 1)
   {
      uint8 lEsperado = ProcCli[0];
      if (lEsperado > sizeof(ProcCli) - 1)
         Bitacora->escribePV(BIT_ERROR, "Error al recibir id del proceso, lng mayor de la esperada (%d)", lEsperado);
      else
      {
         ssize_t lMensaje = res = read(FdFifoSol, ProcCli, lEsperado);
         ok = lMensaje == lEsperado;
         if (!ok)
            Bitacora->escribePV(BIT_ERROR, "Error en recepci�n, no se ley� completo el id de proceso (rec=%d <-> esp=%d)", 
                                     lMensaje, lEsperado); 
         else
         { 
            #ifdef DBG
               Bitacora->escribePV(BIT_DEBUG, "ID PROC: %*.*s", lMensaje, lMensaje, ProcCli);
            #endif // DBG
            if (lMensaje < 3 || ProcCli[0] != 'I' || ProcCli[1] != 'D')
            {
               Bitacora->escribePV(BIT_ERROR, "Error en recepci�n, id de proceso recibido del archivo de "
                                              "intercambio inv�lido (lng %d)", lMensaje); 
               ok = false;
            }
         }
      }
   }
   else
      Bitacora->escribePV(BIT_ERROR, "Error al leer el id del proceso del archivo de intercambio (%d)", errno);
      
   return ok;
}
//#####################################################################################################################
bool CARA_Mediador::LeeSolicitud()
{
   if (!LeeIdProceso())
   {
      CierraFifo(true);
      return false;
   }

   ssize_t lMensaje;
   for (int i = 0; i < 100; i++)
   {
      lMensaje = read(FdFifoSol, MensARA.buffer, sizeof(MensARA.buffer));
      if (lMensaje > 0 || (lMensaje == -1 && errno != EAGAIN) || errno != EAGAIN)
         break;
      #ifdef DBG
      else if( errno == EAGAIN )
         Bitacora->escribePV(BIT_DEBUG, "Ocurrio el error EAGAIN, se intentara leer nuevamente.");
      #endif
      struct timespec ts = { 0, 1000000};
      nanosleep(&ts, NULL);      
   }
   CierraFifo(true);
 
   bool ok = (lMensaje >= 4) && (lMensaje == MensARA.tamMensaje());
   if (!ok)
   {
      if (lMensaje < 0)
         Bitacora->escribePV(BIT_ERROR, "Error al leer del archivo de intercambio (%d)", errno);
      else
      {
         Bitacora->escribePV(BIT_ERROR, "Error en recepci�n, mensaje recibido del archivo de intercambio mal formado "
                                        "(Rec %d <-> TMsg %d)", lMensaje, MensARA.tamMensaje());
         #ifdef DBG
         std::string letbuff = dumpBuffer("Solicitud recibida:", MensARA.buffer, lMensaje);
         Bitacora->escribe(BIT_DEBUG, letbuff.c_str());
         #endif
      }
      RespuestaError(MalFormada);
      EnviaRespuesta();
   }
   #ifdef DBG
   if (lMensaje > 0)
   //V.2.0.1 GHM (20071121): Se modifica por el cambio del tipo de funci�n
   //dumpBuffer("Solicitud recibida:", MensARA.buffer, lMensaje); //>- V.2.0.1 GHM (20071121)
   //>>+ V.2.0.1 GHM (20071121)
   {
      std::string letbuff = dumpBuffer("Solicitud recibida:", MensARA.buffer, lMensaje);
      Bitacora->escribe(BIT_DEBUG, letbuff.c_str());
   }
   //<<+ V.2.0.1 GHM 
   #endif

   return ok;   
}
//#####################################################################################################################
void CARA_Mediador::RespuestaError(TIPERROR tipError)
{
   char cError[10];
   static const char* desc[] = 
   {
      "Sin descripci�n",
      "Error interno",
      "Mensaje mal formado",
      "Operaci�n inv�lida"
   };

   sprintf(cError, "%d", tipError);

   intE error = MensARA.setMensaje(ERROR_ARA, cError, strlen(cError), desc[tipError], strlen(desc[tipError]));
   if (error)
   {
      memset(MensARA.buffer, 0, sizeof(MensARA.buffer));
      Bitacora->escribe(BIT_ERROR, error, "Error al armar el mensaje ERROR_ARA para el SrvARA");
   }
}
//#####################################################################################################################
bool CARA_Mediador::EnviaRespuesta()
{
   bool ok = AbreFifo(false);

   if (ok)
   {
      int lBuf = MensARA.tamMensaje();
      int escritos = write(FdFifoResp, MensARA.buffer, lBuf);
      ok = escritos == lBuf;
      if (!ok)
         Bitacora->escribePV(BIT_ERROR, "Error al escribir en el archivo de intercambio con el SrvARA (%d)", errno);
      #ifdef DBG
      if (ok)
      //V.2.0.1 GHM (20071121): Se modifica por el cambio del tipo de funci�n
      //dumpBuffer("Respuesta enviada:", MensARA.buffer, lBuf); //>- V.2.0.1 GHM (20071121)
      //>>+ V.2.0.1 GHM (20071121)
      {
         std::string letbuff = dumpBuffer("Respuesta enviada:", MensARA.buffer, lBuf);
         Bitacora->escribe(BIT_DEBUG, letbuff.c_str());
      }
      //<<+ V.2.0.1 GHM 
      #endif
      
      CierraFifo(false);
   }
   memset(MensARA.buffer, 0, 100); //<

   return ok;
}
//#####################################################################################################################
bool CARA_Mediador::EnviaAR()
{
   MensARA.setDestino(ID);
   intE error = Skt->Envia(MensARA.buffer, MensARA.tamMensaje());
   if (error)
      Bitacora->escribe(BIT_ERROR, error, "No se pudo enviar mensaje a la AR");
   else
      Bitacora->escribe(BIT_INFO, "Mensaje enviado a la AR");
   return !error;
}
//#####################################################################################################################
bool CARA_Mediador::RecibeAR()
{
   /*>- ERGL mi� sep 12 17:01:06 CDT 2007
   int  lBuf  = sizeof(MensARA.buffer);
   intE error = Skt->Recibe(MensARA.buffer, lBuf);
   */
   intE error = MensARA.Recibe(Skt);
   if (error)
      Bitacora->escribe(BIT_ERROR, error, "No se pudo recibir respuesta de la AR");
   /* else if (lBuf != MensARA.tamMensaje()) //>- ERGL mi� sep 12 17:00:36 CDT 2007
   {
      Bitacora->escribePV(BIT_ERROR, "Error, se recibi� mensaje con longitud incorrecta (recibidos %d <-> mensaje %d)",
         lBuf, MensARA.tamMensaje());
      error = -1; 
   }
   else */
   Bitacora->escribe(BIT_INFO, "Mensaje recibido de la AR");

   return !error;
}
//#####################################################################################################################
bool CARA_Mediador::EnviaKeepAlive()
{
   Bitacora->escribe(BIT_INFO, "Inicio KEEPALIVE");
   intE error = MensARA.setMensaje(PKI_KEEPALIVE);
   if (error)
      Bitacora->escribe(BIT_ERROR, error, "Error al armar el mensaje");
   else if (!EnviaAR() || !RecibeAR())
      error = -1;
   
   Bitacora->escribe(BIT_INFO, "Fin KEEPALIVE");
   return !error;
}
//#####################################################################################################################
bool CARA_Mediador::EnviaDesconexion()
{
   Bitacora->escribe(BIT_INFO, "Inicio Desconexi�n");
   intE error = MensARA.setMensaje(SOLDESCONEXIONARA);
   if (error)
      Bitacora->escribe(BIT_ERROR, error, "Error al armar el mensaje");
   else if (!EnviaAR())
      error = -1;
   
   Bitacora->escribe(BIT_INFO, "Fin Desconexi�n");
   return !error;
}
//#####################################################################################################################
bool CARA_Mediador::SolCert()
{
   Bitacora->escribe(BIT_INFO, "---> Solicitud de Certificado");
 
   char no_serie[30], opcion[2];
   int lno_serie = 30;
   int lopcion = 2;
   //int lno_serie = sizeof(no_serie), lopcion = sizeof(opcion);

   int error = MensARA.getMensaje(SOLCERTMEDARA, no_serie, &lno_serie, opcion, &lopcion);
   if (error)
   {
      Bitacora->escribe(BIT_ERROR, error, "Error al decodificar mensaje recibido del cliente");
      RespuestaError(MalFormada);
   }
   else
   {
      Bitacora->escribePV(BIT_INFO, "Solicita Certificado: %s (opcion %s)", no_serie, opcion);
      if (!EnviaAR() || !RecibeAR())
         RespuestaError(ErrorInt);
      else
      {  
         if (MensARA.tipOperacion() == CERTREGARA)
         {
            lno_serie = sizeof(no_serie);
            error = MensARA.getMensaje(CERTREGARA, no_serie, &lno_serie);
            if (error)
            {
                Bitacora->escribe(BIT_ERROR, error, "Error al decodificar mensaje recibido de la AR");
                RespuestaError(ErrorInt);
            }
            else
               Bitacora->escribePV(BIT_INFO, "Certificado registrado an la ARA: %s", no_serie);
         }
         else
            Bitacora->escribePV(BIT_INFO, "Recibe respuesta %d", MensARA.tipOperacion()); 
      }
   }

   bool ok = EnviaRespuesta();
   Bitacora->escribe(BIT_INFO, "<--- Fin de Solicitud"); 
   return ok;
}
//#####################################################################################################################
bool CARA_Mediador::SolListaCerts()
{
   Bitacora->escribe(BIT_INFO, "---> Solicitud de Listas de Certificados"); 
   
   char rfc[14], tipCert[2], edo[2], limite[10]; 
   int  lrfc = sizeof(rfc), ltipCert = sizeof(tipCert),
        ledo = sizeof(edo), llimite  = sizeof(limite);

   int error = MensARA.getMensaje(SOLLISTACERTRFC, rfc, &lrfc, tipCert, &ltipCert, edo, &ledo, limite, &llimite);
   if (error)
   {
      Bitacora->escribe(BIT_ERROR, error, "Error al decodificar mensaje recibido del mediador");
      RespuestaError(MalFormada);
   }
   else
   {
      Bitacora->escribePV(BIT_INFO, "Solicita lista de CDs: RFC(%s), Tipo(%s), Edo(%s), Cuantos(%s)", 
         rfc, tipCert, edo, limite);
      if (!EnviaAR() || !RecibeAR())
         RespuestaError(ErrorInt);
      else
      {  
         if (MensARA.tipOperacion() == LISTACERT)
            Bitacora->escribe(BIT_INFO, "Recepci�n de lista de Cds de la AR");
         else
            Bitacora->escribePV(BIT_INFO, "Recibe respuesta %d", MensARA.tipOperacion()); 
      }
   }

   bool ok = EnviaRespuesta();
   Bitacora->escribe(BIT_INFO, "<--- Fin de Solicitud"); 
   return ok;
}
//#####################################################################################################################
bool CARA_Mediador::ProcesaSolicitud()
{
   bool ok = true;

   if ( LeeSolicitud() )
   {
      switch (MensARA.tipOperacion())
      {
         case SOLCERTMEDARA  : ok = SolCert(); break;
         case SOLLISTACERTRFC: ok = SolListaCerts(); break;
         default:
            Bitacora->escribePV(BIT_ERROR, "Error, operaci�n solicitada no v�lida (%d)", MensARA.tipOperacion());
            RespuestaError(OperInv);
            ok = EnviaRespuesta();
      }
   }
   
   return ok;
}
//#####################################################################################################################
bool CARA_Mediador::Proceso()
{
   Bitacora->escribe(BIT_INFO, "Servicio iniciado");

   int inactivo = 0;
   for (;Procesar;)
   {
      int disp = HaySolicitud();   
      if (disp == -1)
         break;

      if (!disp)
      {
         inactivo++;
         if (inactivo >= MaxInactividad)
         {
            if (!EnviaKeepAlive())
               break;
            inactivo = 0;
         }
         continue;
      }

      inactivo = 0;
      ProcesaSolicitud();
   }

   EnviaDesconexion();
   Bitacora->escribe(BIT_INFO, "Servicio detenido");
   return true;
}
//#####################################################################################################################
//#####################################################################################################################
