#ifndef _ARA_MEDIADOR_H_
#define _ARA_MEDIADOR_H_
static const char* _ARA_MEDIADOR_H_VERSION_ ATR_USED = "ARA_Mediador @(#)"\
"DSIC07412ARA 2007-12-13 ARA_Mediador.h 1.1.0/0";

//#VERSION: 1.1.0
//###############################################################################
#include <string>

#include <Sgi_Demonio.h>
#include <Sgi_MsgPKI.h>
//###############################################################################
#define PATH_CONF            "/usr/local/SAT/PKI/etc/"
#define PATH_BITA            "/var/local/SAT/PKI/"

#ifdef DBG
   #define ARCH_CONF            PATH_CONF "ARA_Mediador_dbg.cfg"
   #define ARCH_BITA            PATH_BITA "ARA_Mediador_dbg.log"
#else
   #define ARCH_CONF            PATH_CONF "ARA_Mediador.cfg"
   #define ARCH_BITA            PATH_BITA "ARA_Mediador.log"
#endif
//###############################################################################
enum TIPERROR { ErrorInt = 1, MalFormada, OperInv };
//###############################################################################
class CARA_Mediador: public CDemonio
{
      int          FdFifoSol;
      int          FdFifoResp;
      char         ProcCli[16];
      
   protected:
      int          ID;
      std::string  ARA_CD;
      std::string  pathFifo;
      std::string  pathFifo_escritura;	//>+ ERGL (070528)
      int          MaxInactividad;
      std::string  AR_IP;
      int          AR_Puerto;
      MensajesARA  MensARA;
      CSSL_Skt    *Skt;
      
      static bool Procesar;

      static void ManejadorSignals(int senal);

      bool CreaFifo();
      bool ConectaAR();
      bool AbreFifo  (bool solicitud);
      void CierraFifo(bool solicitud);
      int  HaySolicitud();
      bool ProcesaSolicitud();
      bool EnviaKeepAlive();
      bool EnviaDesconexion();
      bool LeeSolicitud();
      bool LeeIdProceso();
      bool SolCert();
      bool SolListaCerts();
      void RespuestaError(TIPERROR tipError);
      bool EnviaRespuesta();
      bool EnviaAR();
      bool RecibeAR();

      virtual bool Inicia ();
      virtual bool Termina();
      virtual bool setManejadorSignals();
      virtual bool CargaCfg();

      virtual bool Proceso();
   public:
      CARA_Mediador();
      virtual ~CARA_Mediador();
};
//###############################################################################
#endif // _ARA_MEDIADOR_H_
