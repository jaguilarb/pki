static const char* _SRVARA_SUNX_CPP_VERSION_ ATR_USED = "SrvARA_90 @(#)"\
"DSIC09212ARA 2009-05-19 SrvARA_SUnx.cpp 1.1.0/0";

//#VERSION: 1.1.0
//######################################################################################################################
//######################################################################################################################
//
// Complemento de la funcionalidad requerida por la librer�a Sgi_SrvUnx para crear el servicio SrvARA
//
//######################################################################################################################
//######################################################################################################################
#include <SrvARA.h>

//VARIABLES GLOBALES
std::string strArchLog; //>+ V.2.0.03

//######################################################################################################################
//######################################################################################################################
//>+ V.2.0.03
bool CSrvARA::ProcesaParametros(int argc, char* argv[], char* /*env*/[])
{
   #if defined(DBG) && !defined(DBG_NO_SRV)
      sleep(15);
   #endif
   
   int   c = -1;     // Inicializa el car�cter fuera del rango esperado
   opterr = 0;       // Inicializa el indicador de error de getopt
   
   strArchLog = std::string(ARCH_BITA); //Inicializa la variable del archivo de bit�cora
   if ( (argc > 1))  //En caso de recibir argumentos
   {
      for (;;)  //Rastreo de todas los argumentos
      {
         c = getopt(argc, argv, "l"); //Argumentos a evaluar
         if (c == -1)
            break;
         switch (c)
         {
            case 'l':
               if (argv[optind] != NULL)
               {
                  std::string strPaso(argv[optind]);
                  if ( strPaso.length() > 0 )
                     strArchLog = strPaso ; 
                  break;
               }
            //Si se requieren procesar mas argumentos se deben colocar aqu� el c�digo correspondiente
            //case valorArgumento
         } 
      }
   }
   return true; 
}
//<+ V.2.0.03
//######################################################################################################################
CBitacora* Crea_CBitacora()
{
   #ifdef DBG_NO_SRV
      #ifdef PUERTO
         DBG_NS_Puerto = PUERTO;
      #else
         DBG_NS_Puerto = 4006;
      #endif
   #endif
   //CBitacora* bit = new CBitacora(ARCH_BITA); //<- V.2.0.03
   CBitacora* bit = new CBitacora(strArchLog);  //>+ V.2.0.03
   #ifdef DBG
      #ifndef DBG_NO_SRV
         sleep(15);
      #endif
      if (bit)
         bit->setNivel(BIT_DEBUG);
   #endif
   return bit;
}
//######################################################################################################################
CSrvUnx* Crea_CSrvUnx()
{
   return new CSrvARA;
}
//######################################################################################################################
bool libSrvUnx_Ini()
{
   #if defined(DBG) && !defined(DBG_NO_SRV)
      sleep(15);
   #endif
   iniciaLibOpenSSL();
   return true;
}
//######################################################################################################################
bool libSrvUnx_Fin()
{
   terminaLibOpenSSL();
   return true;
}
//######################################################################################################################
//######################################################################################################################
