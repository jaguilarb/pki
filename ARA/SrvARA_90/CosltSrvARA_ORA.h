#ifndef _COSLTSRVARA_H_
#define _COSLTSRVARA_H_

static const char* _COSLTSRVARA_H_VERSION_ ATR_USED = "@(#) SrvAC ( L : DSIC10392AR_ : ConsltSrvARA.h : 1.0.0 : 10/10/14)";

//#VERSION: 1.1.0
/*##############################################################################
  ###  PROYECTO:              PKI-SAT                                        ###
  ###  MODULO:                Servicio ARA                                    ###
  ###                                                                        ###
  ###                                                                        ###
  ###  DESARROLLADORES:       Guadalupe Negrete Castro         GNC           ###
  ###                                                                        ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       Viernes 10, octubre del 2014                   ###
  ###                                                                        ###
  ##############################################################################
         1         2         3         4         5         6         7         8
12345678901234567890123456789012345678901234567890123456789012345678901234567890

################################################################################
  VERSION:
     V.1.0.0  (20141010- )    GNC: Version Original
                              Consutas para el Servicio de Autoridad Registradora de Aplicaciones PKI-SAT
  

#################################################################################*/



//#VERSION: 1.1.0



			 
#define QS_RFC_EDO_TIP_VIVF_CERT             "SELECT RFC,EDOCER, TIPCERCVE, VIGINI, VIGFIN FROM SEG_ARA.SEGT_CERTIFICADO WHERE NOSERIE = '%s'"
#define QS_NOSERIE_CERTIFICADO               "SELECT * FROM (SELECT NOSERIE FROM SEG_ARA.SEGT_CERTIFICADO WHERE RFC = '%s' and TIPCERCVE = 1 ORDER BY NOSERIE DESC) WHERE ROWNUM = 1"
#define QS_CUANTOS_EDO_CERT                  "SELECT COUNT(*), EDOCER FROM SEG_ARA.SEGT_CERTIFICADO WHERE NOSERIE = '%s' GROUP BY EDOCER;"










#endif















