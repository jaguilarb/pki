#include<certificador.h>

using namespace SgiCripto;

certificador::
certificador()
{
   ERR_load_crypto_strings();
   OpenSSL_add_all_algorithms();

   bGenLocal = false;
   cTipoCer  = NULL;
   cExtensiones = NULL;
   cCertAC = NULL;
   cLlavePriv = NULL;
   cPassPriv= NULL;
   cRequerimiento = NULL;
   iReq = sizeof(cReq);
   llavePrivada = NULL;
   extensiones = NULL;


}


certificador::
~certificador()
{
   if ( cTipoCer != NULL )
   {
      //delete cTipoCer;
      cTipoCer = NULL;
   }
   if (cExtensiones != NULL)
   {
      //delete cExtensiones;
      cExtensiones = NULL;
   }
   if (cCertAC != NULL) 
   {
      //delete cCertAC;
      cCertAC = NULL;
   }
   if (cLlavePriv != NULL)
   {
      //delete cLlavePriv;
      cLlavePriv  = NULL;
   }
   if (cPassPriv != NULL)
   {
      //delete cPassPriv;
      cPassPriv = NULL;
   }
   if ( cRequerimiento != NULL)
   {
      //delete cRequerimiento;
      cRequerimiento = NULL;
   }
   if ( cCadCert != NULL)
   {
      //delete cCadCert;
      cCadCert = NULL;
   }
   if (llavePrivada != NULL)
   {
      RSA_free(llavePrivada);
      llavePrivada = NULL;
   } 

}

bool certificador::
uso()
{

   printf("Aplicación certificador.\nUso :\n%27s %28s %83s %76s %85s %104s %94s %83s\n",
                  "-l Generación Local\n","-x Generación Externa\n","-t Tipo de Certificado a generar( ocsp,crl,selladora,clienteSSL,servidorSSL)\n","-e Archivo donde se encuentran las extensiones para los certificados.\n","-c Ruta y nombre del certificado de la AC que se utilizará para la generación.\n","-k Ruta y nombre de la llave privada correspondiente a la AC que se utilizará para la generación.\n","-r Ruta y nombre del requerimiento que se utilizará para la generación del certificado.\n","-w Ruta y Nombre del archivo de la cadena de certificación.");

   return false;

}


bool certificador::
lectArgumentos(int nArgs, char* cArgs[])
{
   int  iOpcion = -1;
   int  iTam = -1;
   bool bCompletos = false;

   if ( nArgs ==  1)
      return uso();

   while( (iOpcion = getopt( nArgs,cArgs, "lxt:e:c:k:r:w:" )) != -1 )
   {
      iTam = 0;
      if (optarg != NULL)
         iTam = strlen(optarg);
      switch( iOpcion )
      {
         case 'l' : // AC local.(llave PKCS8)
            printf("Local\n");
            bGenLocal = true;
            break;
         case 'x' : // AC externa.(Conexion AC)
            bGenLocal = false;
            printf("Externa\n");
            break;
         case 't' :  //  Tipo de Certificado a generar.
            cTipoCer =  (char *)malloc( sizeof(char) * iTam+1 );
            cTipoCer[0] = '\0';
            strncpy(cTipoCer,optarg,iTam);
            printf("Tipo %s\n",cTipoCer);
            break;
         case 'e' :  // Archivo donde se encontraran las extensiones para la generacion del certificado.
            cExtensiones = (char *)malloc( sizeof(char) * iTam+1 ); 
            cExtensiones[0] = '\0';
            strncpy(cExtensiones,optarg,iTam);
            printf("Archivo de extensiones %s\n",cExtensiones);
            break;
         case 'c' :  // Certificado de la AC local.
            cCertAC  =  (char *)malloc( sizeof(char) * iTam+1 );
            cCertAC[0] = '\0';
            strncpy(cCertAC,optarg,iTam);
            printf("Certificado de la AC local %s\n",cCertAC);
            break;
         case 'k' :
            cLlavePriv  = (char *)malloc( sizeof(char) * iTam+1 ); 
            cLlavePriv[0] = '\0';
            strncpy(cLlavePriv,optarg,iTam);
            printf("Llave Privada %s\n",cLlavePriv);
            break;
         case 'r' :
            cRequerimiento  = (char *)malloc( sizeof(char) * iTam+1 );
            cRequerimiento[0] = '\0';
            strncpy(cRequerimiento,optarg,iTam);
            printf("Requerimiento %s\n",cRequerimiento);
            break;
         case 'w' :
            cCadCert  = (char *)malloc( sizeof(char) * iTam+1 ); 
            cCadCert[0] = '\0';
            strncpy(cCadCert,optarg,iTam);
            printf("Cadena de certificacion %s\n",cCadCert);
            break;
         default:
            printf("Argumento inválido.\n");
            return false;
            break;
      }
   }

   if ( bGenLocal && (cExtensiones!=NULL) && (cCertAC!=NULL) && (cTipoCer!=NULL) && (cRequerimiento!=NULL)&& (cLlavePriv!=NULL) && (cCadCert!=NULL) )
   {
      bCompletos = true;
      printf("==== >>  Generacion de Certificado Local.\n");
   }
   else if ( !bGenLocal && (cExtensiones!=NULL) && (cTipoCer!=NULL) && (cRequerimiento!=NULL))
   {
      bCompletos = true;
      printf("==== >>  Generacion de Certificado Externa.\n");
   }
   else
      return uso();

  return bCompletos; 
    
}

bool certificador::
leerPassword()
{
   char *cPass = NULL;
   char *cPassVer = NULL;
   bool leerPassword = false;
 
   cPass    = getpass("Escribe password de la llave privada : ");
   cPassVer = getpass("Verificación de password de la llave privada : ");

   if(!cPass || !cPassVer)
   {
      printf("No se obtuvo el password correctamente");
      return leerPassword;
   }

   if ( strcmp(cPass,cPassVer) == 0 )
   {
      cPassPriv =  new char(strlen(cPassVer)+1);
      strncpy( cPassPriv,cPassVer,strlen(cPassVer));
      cPassPriv[strlen(cPassVer)] = 0;
      leerPassword = true;
   }
   else 
      printf("Password de llave privada incorrecta.");

   if (cPass !=  NULL)
   {
      delete cPass;
      cPass = NULL;
   }
   if (cPassVer != NULL)
   {
      //delete cPassVer;
      cPassVer = NULL;
   }
   return leerPassword;
}



bool certificador::
validacionDeLlavesPrivadayPublica()
{
   CSgi_IO          *certIO      = NULL;
   CSgi_IO          *llavePrivIO = NULL;
   CSgi_Certificado *cert        = NULL;
   CSgi_LlavePriv   *llavePriv   = NULL;
   CSgi_CadCert     *cadCert     = NULL;

   bool bCorresponden = false;
   int iError[5];
   char cAC[150];
   int  iAC = sizeof(cAC);

   
   certIO      = new CSgi_IO();
   llavePrivIO = new CSgi_IO();
   llavePriv   = new CSgi_LlavePriv();
   cert        = new CSgi_Certificado();
   cadCert     = new CSgi_CadCert();

   if ( !certIO || !cert || !llavePrivIO || !llavePriv ||!cadCert)
      return false;
//     throw ExceptionCert();

   if ( !leerPassword())
      return false;
   //     throw ExceptionCert();

   iError[0] = certIO->inicia(IO_in,cCertAC);
   iError[1] = llavePrivIO->inicia(IO_in,cLlavePriv);
   iError[2] = cert->inicia(TC_UTF8, certIO);
   iError[3] = llavePriv->inicia(llavePrivIO, (uchar*)cPassPriv,strlen(cPassPriv));   
   iError[4] = cadCert->inicia(cCadCert);

   
   if ( iError[0] || iError[1] || iError[2] || iError[3] ||iError[4])
      return false;
             //     throw ExceptionCert();
   
   if ( !llavePriv->verificaLlaves( cert ) )
   {
      if ( !cadCert->verifica(CTipoDesconocido, cert,NULL))
      {
         if (!cert->getExtension(Ext_basic_constraints,cAC,&iAC))
         {
            if ( strcmp(cAC,"0") != 0 )
            {
               EVP_PKEY* evpLlavePrivada = llavePriv->getEvpKey();
               llavePrivada  = EVP_PKEY_get1_RSA(evpLlavePrivada);
               if (llavePrivada != NULL)
                  bCorresponden =  true;
            }
         }
      }
   }

   delete certIO;      certIO=NULL;
   delete cert;        cert=NULL;
   delete llavePrivIO; llavePrivIO =NULL; 
   delete llavePriv;   llavePriv =NULL;

   return bCorresponden;
}



bool certificador::
validaRequerimiento()
{

   FILE *fReq = NULL;
   bool bValidado = false;
   int iError;
   iReq = -1;

   SGIX509 req;
   int   iNombres[6] = {NID_organizationName,NID_organizationalUnitName,NID_countryName,NID_stateOrProvinceName,NID_localityName,NID_commonName};
   SNames atributos[1];
   int    iAtributos[1] = {NID_pkcs9_challengePassword};
   int    iNoNULL =0 ;


   fReq = fopen(cRequerimiento, "r");
   if (fReq != NULL)
   { 
      iReq = fread(cReq,1,sizeof(cReq),fReq);
      if (iReq > 0) 
      {
         iError = req.ProcREQ(cReq,iReq, nombres, atributos,NULL, iNombres,6, iAtributos, 1);
         if (!iError)
         {
               for(int i=0;i<6;i++)
               {
                  if(  nombres[i].dato == NULL)
                     break;
                  else iNoNULL++;
               }    
               if (iNoNULL == 6)
                  bValidado = true;
         }
      }
   }

   if (fReq != NULL)
   {
      fclose(fReq);
      fReq = NULL;
   }
   if ( bValidado )
      printf("Requerimiento con datos completos.");

   return bValidado;

}

bool certificador::
leerExtensiones()
{
   CArchCfgSC ext;
   int iNumDatosExt;
   bool bExtensiones = false;

   if ( ext.cargaArchivo(cExtensiones) )
   {
      iNumDatosExt = ext.length(cTipoCer);
      if (iNumDatosExt != 0) 
      {
         extensiones = new SNames[iNumDatosExt];
         if (extensiones != NULL)
         {
            for (int i =0; i<iNumDatosExt; i++)
            {
               std::string sNombreExt;
               std::string sValorExt;

               ext.get(cTipoCer, i, sNombreExt, sValorExt);

               extensiones[i].Inicia();

               strncpy(extensiones[i].id,(char*)sNombreExt.c_str(),sNombreExt.size());
               strncpy(extensiones[i].dato,(char*)sValorExt.c_str(),sValorExt.size());
            }
            iExtensiones = iNumDatosExt;
            bExtensiones = true;
         }
      }
   }
   return bExtensiones;
}

bool certificador::
numeroSerie(bool bAumentar)
{

   FILE *fNumSerie = NULL;
   bool bNumSerie= false;
   char cNumSerieAux[21];
   char cSerial[9];

   int iNumSerie = sizeof(cNumSerie);

   
   fNumSerie = fopen("serial", "r+");
   if (fNumSerie != NULL)
   {
      if (bAumentar)
      {
         iNumSerie = fwrite(cNumSerie,1,20,fNumSerie); 
         if (iNumSerie == 20)
            bNumSerie = true;
             
      }
      else
      {
         memset(cNumSerieAux,0,sizeof(cNumSerieAux));
         memset(cSerial,0,sizeof(cSerial));
         memset(cNumSerie,0,sizeof(cNumSerie));

         iNumSerie = fread(cNumSerieAux,1,20,fNumSerie);
         if (iNumSerie == 20)
         {
            cNumSerie[20] = 0;
            strncpy(cNumSerie,cNumSerieAux,12);
            strncpy(cSerial,cNumSerieAux+12,8);
            cSerial[8] =0;
            int serial =  atoi(cSerial);
            serial++;
            
            int iCeros = 0;
            if (serial < 10)
               iCeros = 7;
            else if (serial < 100 )
               iCeros = 6;
            else if (serial < 1000)
               iCeros = 5;
            else if (serial < 10000)
               iCeros = 4;
            else if (serial < 100000)
               iCeros = 3;
            else if (serial < 1000000)
               iCeros = 2;
            else if (serial < 10000000)
               iCeros = 1;
            else
               iCeros = 0;

            for(int i=0;i<iCeros;i++)
               cSerial[i]= '0';

            sprintf(cSerial+iCeros,"%d",serial); 
            strncpy(cNumSerie+12,cSerial,8); 
            bNumSerie = true; 
            printf("Numero de serie %s\n",cNumSerie); 
         }
      }
   }
   if (fNumSerie !=  NULL)
   {
      fclose(fNumSerie);
      fNumSerie = NULL;
   }   

   return bNumSerie;

}

bool certificador::
generaCertificado()
{
   bool bGenerado = false;
   SGIX509 cert;
   uchar bCert[6000];
   char cNombre[25];
   int iCert = sizeof(bCert);
   int iError;


   if ( numeroSerie(false) )
   {
      iError = cert.GenCERTAC(cReq,iReq,cCertAC,(uchar*)cNumSerie,strlen(cNumSerie),70848000,llavePrivada,extensiones,iExtensiones,bCert,&iCert,nombres,6,1);
//   iError = cert.GenCERTAC(cReq,iReq,cCertAC,(uchar*)"99001000000100000006",20,70848000,llavePrivada,extensiones,iExtensiones,bCert,&iCert,NULL,0,1);
      if(!iError)
      {
         FILE *fCert = NULL;
         sprintf(cNombre,"%s.cer\0",cNumSerie);
   
         fCert = fopen(cNombre,"w");
         if (fCert != NULL)
         {
            fwrite(bCert,1,iCert,fCert);
            fclose(fCert);
            numeroSerie(true);
            bGenerado = true;
         }

      }
   }

   return bGenerado;
}

void certificador::
principal(int iArgs, char *cArgs[])
{

   if ( lectArgumentos(iArgs, cArgs) )
   {
      if ( bGenLocal )
      {
         if ( validacionDeLlavesPrivadayPublica())
         {
            if( validaRequerimiento() )
            {
               if (leerExtensiones())
               {
                  if ( generaCertificado() )
                  {
                     // Guarda datos en archivo
                     printf("Certificado generado.\n");
                  }
               }
            }
         }
      }
      else 
      {
         // Conectarse al servicio de la AC para generacion.
         // Armar PKCS7 para envio de generacion.
         // Enviar 
         // Recibir  
         // Guardar.
      }

   }   
}
