EVP_PKEY* ProcLlavePrivCert::
llavePrivada(const char* cLlavePrivada, const char* cPassword, int iPassword)
{

   EVP_PKEY    *llaveprivada = NULL;
   PKCS8_PRIV_KEY_INFO *info = NULL;
   FILE           *llavepriv = NULL;
   X509_SIG           *pkcs8 = NULL;

   llavepriv = fopen(cLlavePrivada,"r");
   if (llavepriv != NULL)
   {
      pkcs8 = d2i_PKCS8_fp(llavepriv,NULL);
      if (pkcs8 != NULL)
      {
         info = PKCS8_decrypt( pkcs8, cPassword, iPassword);
         if (info != NULL)
            llaveprivada  = EVP_PKCS82PKEY(info);
         else iError = ERR_OBT_LLAVE_PRIV;

      }else iError = ERR_OBT_LLAVE_PRIV_F;

   }else iError = ERR_ABR_LLAVE_PRIV;

   if (info != NULL)
      PKCS8_PRIV_KEY_INFO_free(info);
   if (llavepriv !=  NULL)
   {
      fclose(llavepriv);
      llavepriv = NULL;
   }
   if (pkcs8 != NULL)
      X509_SIG_free(pkcs8);

   return llaveprivada;
}

X509* ProcLlavePrivCert::
certificado(const char *rutaCertAC)
{
   X509        *cert = NULL;
   X509_NAME *sujeto = NULL;
   FILE      *cert_f = NULL;

   cert_f = fopen(rutaCertAC,"r");
   if ( cert_f )
   {
      cert = d2i_X509_fp(cert_f,NULL);
      if (cert == NULL)
      {
         rewind(cert_f);
         cert = PEM_read_X509(cert_f,NULL,NULL,NULL);
      }
      if (cert != NULL)
      {
         sujeto = X509_get_subject_name(cert);
         if (sujeto != NULL)
         {
            X509_CRL_set_issuer_name(lista, sujeto);
         } else iError = ERR_OBT_SUJ;

      }else iError = ERR_OBT_CERT;

   }else iError = ERR_ABR_CERT;

   if (cert_f != NULL)
   {
      fclose(cert_f);
      cert_f = NULL;
   }
   if (sujeto != NULL)
      X509_NAME_free(sujeto);

   return cert;
}

