#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <openssl/err.h>
#include <Sgi_Cripto.h>
#include <Sgi_LlavePriv.h>
#include <Sgi_Certificado.h>
#include <Sgi_CadCert.h>
#include <Sgi_IO.h>
#include <Sgi_OpenSSL.h>
#include <Sgi_ConfOpen.h>




class certificador
{


   public:

         certificador();
        ~certificador();

         void principal(int nArgs, char* cArgs[]); 

   private:

         bool bGenLocal; 
         char *cTipoCer;
         char *cExtensiones;
         char *cCertAC;
         char *cLlavePriv;
         char *cPassPriv;
         char *cRequerimiento;
         char *cCadCert;

         RSA *llavePrivada;
         SNames nombres[6];
         SNames *extensiones;
         int  iExtensiones;
         
         uchar cReq[6000];
         char  cNumSerie[21];
         int  iReq;

                           
         bool uso();
         bool validacionDeLlavesPrivadayPublica();
         bool lectArgumentos(int nArgs, char* cArgs[]);
         bool leerPassword();
         bool validaRequerimiento();
         bool generaCertificado();
         bool leerExtensiones();
         bool numeroSerie(bool bAumentar); 


};
