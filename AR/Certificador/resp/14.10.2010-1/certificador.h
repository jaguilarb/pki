#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <openssl/err.h>
#include <Sgi_Cripto.h>
#include <Sgi_LlavePriv.h>
#include <Sgi_Certificado.h>
#include <Sgi_CadCert.h>
#include <Sgi_Sobre.h>
#include <Sgi_LlavePriv.h>
#include <Sgi_IO.h>
#include <Sgi_OpenSSL.h>
#include <Sgi_ConfOpen.h>
#include <Sgi_SSL.h>
#include <Sgi_ProtegePwd.h>
#include <Sgi_PKI.h>



class certificador
{


   public:

         certificador();
        ~certificador();

         void principal(int nArgs, char* cArgs[]); 

   private:

         bool bGenLocal; 
         char *cTipoCer;
         char *cExtensiones;
         char *cCertAC;
         char *cLlavePriv;
         char *cPassPriv;
         char *cRequerimiento;
         char *cCadCert;
         char *cConfiguracion;
         char  cNumSerie[21];
         char  cExtensionesEn[3000];
         uchar cReq[6000];
         std::string sVigencia;
         std::string sRepositorio;

         int  iExtensiones;
         int  iTamExtensiones;
         int  iReq;

         CSSL_parms  paramAC;
         CSSL_Skt   *sktAC;
         Sgi_SSL     sslAC;

         RSA *llavePrivada;
         SNames nombres[1];
         SNames *extensiones;

                           
         bool uso();
         bool validacionDeLlavesPrivadayPublica();
         bool lectArgumentos(int nArgs, char* cArgs[]);
         bool leerPassword();
         bool validaRequerimiento();
         bool generaCertificado();
         bool leerExtensiones();
         bool numeroSerie(bool bAumentar); 
         bool conectaServACGen();
         bool solicitudCert();
         bool armaPKCS7(char* cSolicitud, int iSolicitud,char *bSolicCert, int *ibSolicCert);
         bool guardaCertificado(uchar* cCert, int iCert );


};
