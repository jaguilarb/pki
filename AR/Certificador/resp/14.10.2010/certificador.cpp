#include<certificador.h>

using namespace SgiCripto;

//
//  Funciones para el socket.
//
char  PASSLLAVCLISKT[50];

static int password_cb(char *buf, int size, int rwflag, void *password)
{
   rwflag = 0;
   password =PASSLLAVCLISKT;
   strncpy(buf, (char *)(password), size);
   buf[size - 1] = '\0';
   return(strlen(buf));
}


int verify_callback(int ok, X509_STORE_CTX *store)
{
   char data[256];
   if (!ok)
   {
      X509 *cert = X509_STORE_CTX_get_current_cert(store);
      int depth = X509_STORE_CTX_get_error_depth(store);
      int err = X509_STORE_CTX_get_error(store);

      fprintf(stderr,"Error con el certificado %i\n",depth);
      X509_NAME_oneline(X509_get_issuer_name(cert),data,256);
      fprintf(stderr,"issuer = %s\n",data);
      X509_NAME_oneline(X509_get_subject_name(cert),data,256);
      fprintf(stderr,"subject=%s\n",data);
      fprintf(stderr,"err%i:%s\n",err,X509_verify_cert_error_string(err));
   }
   return (ok);
}


certificador::
certificador()
{
   ERR_load_crypto_strings();
   OpenSSL_add_all_algorithms();

   bGenLocal = false;
   cTipoCer  = NULL;
   cExtensiones = NULL;
   cCertAC = NULL;
   cLlavePriv = NULL;
   cPassPriv= NULL;
   cRequerimiento = NULL;
   iReq = sizeof(cReq);
   llavePrivada = NULL;
   extensiones = NULL;


}


certificador::
~certificador()
{
   if ( cTipoCer != NULL )
   {
      //delete cTipoCer;
      cTipoCer = NULL;
   }
   if (cExtensiones != NULL)
   {
      //delete cExtensiones;
      cExtensiones = NULL;
   }
   if (cCertAC != NULL) 
   {
      //delete cCertAC;
      cCertAC = NULL;
   }
   if (cLlavePriv != NULL)
   {
      //delete cLlavePriv;
      cLlavePriv  = NULL;
   }
   if (cPassPriv != NULL)
   {
      //delete cPassPriv;
      cPassPriv = NULL;
   }
   if ( cRequerimiento != NULL)
   {
      //delete cRequerimiento;
      cRequerimiento = NULL;
   }
   if ( cCadCert != NULL)
   {
      //delete cCadCert;
      cCadCert = NULL;
   }
   if (llavePrivada != NULL)
   {
      RSA_free(llavePrivada);
      llavePrivada = NULL;
   } 

}

bool certificador::
uso()
{
   printf("Aplicación certificador.\nUso :\n %s %27s %84s %77s %86s %105s %95s %67s %10s %29s %84s %95s %77s %76s \n",
         "Generación Local.\n",
         "-l Generación Local\n",
         "-t Tipo de Certificado a generar( ocsp,crl,selladora,clienteSSL,servidorSSL)\n",
         "-e Archivo donde se encuentran las extensiones para los certificados.\n",
         "-c Ruta y nombre del certificado de la AC que se utilizará para la generación.\n",
         "-k Ruta y nombre de la llave privada correspondiente a la AC que se utilizará para la generación.\n",
         "-r Ruta y nombre del requerimiento que se utilizará para la generación del certificado.\n",
         "-w Ruta y Nombre del archivo de la cadena de certificación.\n",
         "Generación Externa\n",
         "-x Generación Externa\n",
         "-t Tipo de Certificado a generar( ocsp,crl,selladora,clienteSSL,servidorSSL)\n",
         "-r Ruta y nombre del requerimiento que se utilizará para la generación del certificado.\n",
         "-e Archivo donde se encuentran las extensiones para los certificados.\n", 
         "-f Ruta y nombre del archivo de configuración para servicio de la AC.");

   return false;

}


bool certificador::
lectArgumentos(int nArgs, char* cArgs[])
{
   int  iOpcion = -1;
   int  iTam = -1;
   bool bCompletos = false;

   if ( nArgs ==  1)
      return uso();

   while( (iOpcion = getopt( nArgs,cArgs, "lxt:e:c:k:r:w:f:" )) != -1 )
   {
      iTam = 0;
      if (optarg != NULL)
         iTam = strlen(optarg);
      switch( iOpcion )
      {
         case 'l' : // AC local.(llave PKCS8)
            bGenLocal = true;
            break;
         case 'x' : // AC externa.(Conexion AC)
            bGenLocal = false;
            break;
         case 't' :  //  Tipo de Certificado a generar.
            cTipoCer =  (char *)malloc( sizeof(char) * iTam+1 );
            cTipoCer[0] = '\0';
            strncpy(cTipoCer,optarg,iTam);
            break;
         case 'e' :  // Archivo donde se encontraran las extensiones para la generacion del certificado.
            cExtensiones = (char *)malloc( sizeof(char) * iTam+1 ); 
            cExtensiones[0] = '\0';
            strncpy(cExtensiones,optarg,iTam);
            break;
         case 'c' :  // Certificado de la AC local.
            cCertAC  =  (char *)malloc( sizeof(char) * iTam+1 );
            cCertAC[0] = '\0';
            strncpy(cCertAC,optarg,iTam);
            break;
         case 'k' :
            cLlavePriv  = (char *)malloc( sizeof(char) * iTam+1 ); 
            cLlavePriv[0] = '\0';
            strncpy(cLlavePriv,optarg,iTam);
            break;
         case 'r' :
            cRequerimiento  = (char *)malloc( sizeof(char) * iTam+1 );
            cRequerimiento[0] = '\0';
            strncpy(cRequerimiento,optarg,iTam);
            break;
         case 'w' :
            cCadCert  = (char *)malloc( sizeof(char) * iTam+1 ); 
            cCadCert[0] = '\0';
            strncpy(cCadCert,optarg,iTam);
            break;
         case 'f':
            cConfiguracion = (char *)malloc( sizeof(char) * iTam+1 );
            cConfiguracion[0] = '\0';
            strncpy(cConfiguracion,optarg,iTam);
            break;
         default:
            printf("Argumento inválido.\n");
            return false;
            break;
      }
   }

   if ( bGenLocal && (cExtensiones!=NULL) && (cCertAC!=NULL) && (cTipoCer!=NULL) && (cRequerimiento!=NULL)&& (cLlavePriv!=NULL) && (cCadCert!=NULL) )
   {
      bCompletos = true;
      printf("==== >>  Generacion de Certificado Local.\n");
   }
   else if ( !bGenLocal && (cExtensiones!=NULL) && (cTipoCer!=NULL) && (cRequerimiento!=NULL) && (cConfiguracion!=NULL))
   {
      bCompletos = true;
      printf("==== >>  Generacion de Certificado Externa.\n");
   }
   else
      return uso();

  return bCompletos; 
    
}

bool certificador::
leerPassword()
{
   char *cPass = NULL;
   char *cPassVer = NULL;
   bool leerPassword = false;
 
   cPass    = getpass("Escribe password de la llave privada : ");
   cPassVer = getpass("Verificación de password de la llave privada : ");

   if(!cPass || !cPassVer)
   {
      printf("No se obtuvo el password correctamente");
      return leerPassword;
   }

   if ( strcmp(cPass,cPassVer) == 0 )
   {
      cPassPriv =  new char(strlen(cPassVer)+1);
      strncpy( cPassPriv,cPassVer,strlen(cPassVer));
      cPassPriv[strlen(cPassVer)] = 0;
      leerPassword = true;
   }
   else 
      printf("Password de llave privada incorrecta.");

   if (cPass !=  NULL)
   {
      delete cPass;
      cPass = NULL;
   }
   if (cPassVer != NULL)
   {
      //delete cPassVer;
      cPassVer = NULL;
   }
   return leerPassword;
}



bool certificador::
validacionDeLlavesPrivadayPublica()
{
   CSgi_IO          *certIO      = NULL;
   CSgi_IO          *llavePrivIO = NULL;
   CSgi_Certificado *cert        = NULL;
   CSgi_LlavePriv   *llavePriv   = NULL;
   CSgi_CadCert     *cadCert     = NULL;

   bool bCorresponden = false;
   int iError[5];
   char cAC[150];
   int  iAC = sizeof(cAC);

   
   certIO      = new CSgi_IO();
   llavePrivIO = new CSgi_IO();
   llavePriv   = new CSgi_LlavePriv();
   cert        = new CSgi_Certificado();
   cadCert     = new CSgi_CadCert();

   if ( !certIO || !cert || !llavePrivIO || !llavePriv ||!cadCert)
      return false;
//     throw ExceptionCert();

   if ( !leerPassword())
      return false;
   //     throw ExceptionCert();

   iError[0] = certIO->inicia(IO_in,cCertAC);
   iError[1] = llavePrivIO->inicia(IO_in,cLlavePriv);
   iError[2] = cert->inicia(TC_UTF8, certIO);
   iError[3] = llavePriv->inicia(llavePrivIO, (uchar*)cPassPriv,strlen(cPassPriv));   
   iError[4] = cadCert->inicia(cCadCert);

   
   if ( iError[0] || iError[1] || iError[2] || iError[3] ||iError[4])
      return false;
   //     throw ExceptionCert();
   
   if ( !llavePriv->verificaLlaves( cert ) )
   {
      if ( !cadCert->verifica(CTipoDesconocido, cert,NULL))
      {
         if (!cert->getExtension(Ext_basic_constraints,cAC,&iAC))
         {
            if ( strcmp(cAC,"0") != 0 )
            {
               EVP_PKEY* evpLlavePrivada = llavePriv->getEvpKey();
               llavePrivada  = EVP_PKEY_get1_RSA(evpLlavePrivada);
               if (llavePrivada != NULL)
                  bCorresponden =  true;
            }
         }
      }
   }

   delete certIO;      certIO=NULL;
   delete cert;        cert=NULL;
   delete llavePrivIO; llavePrivIO =NULL; 
   delete llavePriv;   llavePriv =NULL;

   printf("Verificacion de llave privada y certificado correcta.");
   return bCorresponden;
}



bool certificador::
validaRequerimiento()
{

   FILE *fReq = NULL;
   bool bValidado = false;
   int iError;
   iReq = -1;

   SGIX509 req;
   SNames nombresVal[6]; 
   SNames atributos[1];
   int    iNombres[6]   = {NID_organizationName,NID_organizationalUnitName,NID_countryName,NID_stateOrProvinceName,NID_localityName,NID_commonName};
   int    iAtributos[1] = {NID_pkcs9_challengePassword};
   int    iNoNULL =0 ;


   fReq = fopen(cRequerimiento, "r");
   if (fReq != NULL)
   { 
      iReq = fread(cReq,1,sizeof(cReq),fReq);
      if (iReq > 0) 
      {
         iError = req.ProcREQ(cReq,iReq, nombresVal, atributos,NULL, iNombres,6, iAtributos, 1);
         if (!iError)
         {
               for(int i=0;i<6;i++)
               {
                  if(  nombresVal[i].dato == NULL)
                     break;
                  else iNoNULL++;
               }    
               if (iNoNULL == 6)
               {
                  nombres[0].Inicia();
                  strcpy(nombres[0].dato,nombresVal[5].dato);
                  strcpy(nombres[0].id,nombresVal[5].id);
                  bValidado = true;
               }
         }
      }
   }

   if (fReq != NULL)
   {
      fclose(fReq);
      fReq = NULL;
   }
   if ( bValidado )
      printf("Requerimiento con datos completos.\n");


   return bValidado;

}

bool certificador::
leerExtensiones()
{
   CArchCfgSC ext;
   int iNumDatosExt;
   bool bExtensiones = false;

   iTamExtensiones = 0;
   if ( ext.cargaArchivo(cExtensiones) )
   {
      iNumDatosExt = ext.length(cTipoCer);
      if (iNumDatosExt != 0) 
      {
         for (int i =0; i<iNumDatosExt; i++)
         {
            std::string sNombreExt;
            std::string sValorExt;

            ext.get(cTipoCer, i, sNombreExt, sValorExt);

            if (sNombreExt.compare("vigencia") == 0)
               continue;

            if(bGenLocal)
            {
               if (i==0)
                  extensiones = new SNames[iNumDatosExt];
               if (extensiones != NULL)
               {

                  extensiones[i].Inicia();
                  strncpy(extensiones[i].id,(char*)sNombreExt.c_str(),sNombreExt.size());
                  strncpy(extensiones[i].dato,(char*)sValorExt.c_str(),sValorExt.size());
               }
            }
            else
            {
               sprintf(cExtensionesEn+iTamExtensiones,"%s=%s^", sNombreExt.c_str(),sValorExt.c_str());
               iTamExtensiones = strlen(cExtensionesEn);
            }
         }
         ext.get(cTipoCer, (const char*)"vigencia", sVigencia);
         iExtensiones = iNumDatosExt-1;
                     
         bExtensiones = true;
      }
   }
   return bExtensiones;
}

bool certificador::
numeroSerie(bool bAumentar)
{

   FILE *fNumSerie = NULL;
   bool bNumSerie= false;
   char cNumSerieAux[21];
   char cSerial[9];

   int iNumSerie = sizeof(cNumSerie);

   
   fNumSerie = fopen("serial", "r+");
   if (fNumSerie != NULL)
   {
      if (bAumentar)
      {
         iNumSerie = fwrite(cNumSerie,1,20,fNumSerie); 
         if (iNumSerie == 20)
            bNumSerie = true;
             
      }
      else
      {
         memset(cNumSerieAux,0,sizeof(cNumSerieAux));
         memset(cSerial,0,sizeof(cSerial));
         memset(cNumSerie,0,sizeof(cNumSerie));

         iNumSerie = fread(cNumSerieAux,1,20,fNumSerie);
         if (iNumSerie == 20)
         {
            cNumSerie[20] = 0;
            strncpy(cNumSerie,cNumSerieAux,12);
            strncpy(cSerial,cNumSerieAux+12,8);
            cSerial[8] =0;
            int serial =  atoi(cSerial);
            serial++;
            
            int iCeros = 0;
            if (serial < 10)
               iCeros = 7;
            else if (serial < 100 )
               iCeros = 6;
            else if (serial < 1000)
               iCeros = 5;
            else if (serial < 10000)
               iCeros = 4;
            else if (serial < 100000)
               iCeros = 3;
            else if (serial < 1000000)
               iCeros = 2;
            else if (serial < 10000000)
               iCeros = 1;
            else
               iCeros = 0;

            for(int i=0;i<iCeros;i++)
               cSerial[i]= '0';

            sprintf(cSerial+iCeros,"%d",serial); 
            strncpy(cNumSerie+12,cSerial,8); 
            bNumSerie = true; 
            printf("Numero de serie %s\n",cNumSerie); 
         }
      }
   }
   if (fNumSerie !=  NULL)
   {
      fclose(fNumSerie);
      fNumSerie = NULL;
   }   

   return bNumSerie;

}

bool certificador::
generaCertificado()
{
   bool bGenerado = false;
   SGIX509 cert;
   uchar bCert[6000];
   char cNombre[25];
   int iCert = sizeof(bCert);
   int iError;


   if ( numeroSerie(false) )
   {
      iError = cert.GenCERTAC(cReq,iReq,cCertAC,(uchar*)cNumSerie,strlen(cNumSerie),atoi(sVigencia.c_str()),
                              llavePrivada,extensiones,iExtensiones,bCert,&iCert,nombres,1,1);
      if(!iError)
      {
         FILE *fCert = NULL;
         sprintf(cNombre,"%s.cer",cNumSerie);
   
         fCert = fopen(cNombre,"w");
         if (fCert != NULL)
         {
            fwrite(bCert,1,iCert,fCert);
            fclose(fCert);
            numeroSerie(true);
            bGenerado = true;
         }

      }
   }

   return bGenerado;
}

////
/// Externo
////
bool certificador::
conectaServACGen()
{

   std::string sIP;
   std::string sPuerto;
   std::string sCertAC;
   std::string sCertSKT;
   std::string sRutaCerts;
   std::string sCertCliente;
   std::string sLlaveCliente;
   std::string sPwdCliCif;

   CArchCfgSC configAC;
   char cPwdDes[50];
   int  iPwdDes;
   int  iNumDatosAC;

   bool conectaServACGen = false;

 
   if ( configAC.cargaArchivo(cConfiguracion) )
   {
      iNumDatosAC = configAC.length("conexionAC");
      if (iNumDatosAC != 0)
      {
         configAC.get("conexionAC", "ip", sIP);
         configAC.get("conexionAC", "puerto", sPuerto);
         configAC.get("conexionAC", "certAC", sCertAC);
         configAC.get("conexionAC", "certsSKT", sCertSKT);
         configAC.get("conexionAC", "rutaCerts", sRutaCerts);
         configAC.get("conexionAC", "certCliente", sCertCliente);
         configAC.get("conexionAC", "llaveCliente", sLlaveCliente);
         configAC.get("conexionAC", "passwordCliente", sPwdCliCif);
   
         if( !desencripta((uint8*)sPwdCliCif.c_str(),sPwdCliCif.length(),(uint8*)cPwdDes,&iPwdDes))
            //throw ExcepcionRevAR(102,"Error al descifrar el pwd de la llave del cliente socket.");

         cPwdDes[iPwdDes]=0;
         strcpy(PASSLLAVCLISKT, cPwdDes);


         paramAC.SetCliente( ESSL_entero ,NULL, (char *)sIP.c_str(), atoi(sPuerto.c_str()), (char *)sCertSKT.c_str(),
                                                (char *)sRutaCerts.c_str(), (char *) sCertCliente.c_str(),
                                                (char *)sLlaveCliente.c_str(), verify_callback, password_cb);
         sslAC.Inicia();

         if (!sslAC.Cliente(paramAC,sktAC))
         {
            if (!sktAC->setKEEPALIVE(900, 2,60))
               conectaServACGen = true;
         }
      }
   }
   return conectaServACGen;


}

bool certificador::
armaPKCS7(char* cSolicitud, int iSolicitud)
{
   uchar bSolicCert[10000];
   uchar cPassword[100];
   std::string sLlavePriv;
   std::string sCertRemitente; 
   std::string sPwdCliCif;
   int  iPassword;
   int  ibSolicCert;
   bool bArmaPKCS7 = false;

   PtrSgiIO        cRemitenteIO = NULL;
   PtrSgiIO        cLlavePrivIO = NULL;
   PtrSgiIO        datosGenIO   = NULL;
   PtrSgiIO        solIO        = NULL;
   PtrSgiCert      cRemitente   = NULL;
   PtrSgiLlavePriv cLlavePriv   = NULL;
   PtrSgiSobre     sobre        = NULL;
   PtrSgiCert      cdFirmantes[1];
   PtrSgiCert      cdDestinatarios[1];
   PtrSgiLlavePriv pkFirmantes[1];
   int iError[6];
   CArchCfgSC configPKCS7;

   memset(cPassword,0,sizeof(cPassword));

   if ( !configPKCS7.cargaArchivo(cConfiguracion) )
      return false;

   int iNumDatosAC = configPKCS7.length("pkcs7");
   if (iNumDatosAC != 0)
   {
      configPKCS7.get("pkcs7", "certificado", sCertRemitente);
      configPKCS7.get("pkcs7", "llavePrivada", sLlavePriv);
      configPKCS7.get("pkcs7", "passwLlavePriv", sPwdCliCif);

      if( !desencripta((uint8*)sPwdCliCif.c_str(),sPwdCliCif.length(),(uint8*)cPassword,&iPassword))
         //throw ExcepcionRevAR(102,"Error al descifrar el pwd de la llave del cliente socket.");
         return false;
   }else return false;


   solIO        = new_SgiIO();
   datosGenIO   = new_SgiIO();
   cRemitenteIO = new_SgiIO();
   cLlavePrivIO = new_SgiIO();
   cLlavePriv   = new_SgiLlavePriv();
   cRemitente   = new_SgiCertificado();
   sobre        = new_SgiSobre();


   if (!solIO || !datosGenIO || !cRemitenteIO || !cLlavePrivIO || !cLlavePriv ||!cRemitente || !sobre )
      return false;


   iError[0] = SgiIO::inicia(solIO, IO_out, bSolicCert,&ibSolicCert);
   iError[1] = SgiIO::inicia(datosGenIO, IO_in, (uchar*)cSolicitud,&iSolicitud);
   iError[2] = SgiIO::inicia(cRemitenteIO, IO_in, (char *)sCertRemitente.c_str());
   iError[3] = SgiIO::inicia(cLlavePrivIO, IO_in, (char *)sLlavePriv.c_str() );
   iError[4] = SgiLlavePriv::inicia(cLlavePriv, cLlavePrivIO, cPassword,iPassword);
   iError[5] = SgiCertificado::inicia(cRemitente, TC_ASCII, cRemitenteIO);

   cdFirmantes[0] = cRemitente;
   pkFirmantes[0] = cLlavePriv;

   if (iError[0] || iError[1] ||iError[2] ||iError[3] ||iError[4] || iError[5])
      return false;

   iError[0] = SgiSobre::genera(sobre, cdFirmantes, pkFirmantes, 1, datosGenIO, TPKCS7_Firmado, solIO, AEnc_DES_EDE3_CBC, cdDestinatarios, 0);
   if(!iError[0])
      bArmaPKCS7 = true;

   return  bArmaPKCS7;
}
/*
bool certificador::
armaPKCS7(char* cSolicitud)
{
   // Temporales
   std::string sCertRemitente;
   std::string sLlavePriv;
   std::string sCertDest;
   uchar bSolicCert[10000];
   uchar cPassword[100];
   int  iPassword;
   int  ibSolicCert;

   CSgi_Sobre        *sSol         = NULL;
   CSgi_Certificado  *cDest        = NULL;
   CSgi_Certificado  *cRemitente   = NULL;
   CSgi_LlavePriv    *cLlavePriv   = NULL;
   CSgi_IO           *datosGenIO   = NULL;
   CSgi_IO           *solIO        = NULL;
   CSgi_IO           *cDestIO      = NULL;
   CSgi_IO           *cLlavePrivIO = NULL;
   CSgi_IO           *cRemitenteIO = NULL;
   std::vector<CSgi_Certificado> vRemitente;
   CSgi_Certificado  remitentes[1];
   CSgi_LlavePriv    llaves[1];
   

   sSol         =  new CSgi_Sobre();
   cDest        =  new CSgi_Certificado();
   cRemitente   =  new CSgi_Certificado();
   cLlavePriv   =  new CSgi_LlavePriv();
   datosGenIO   =  new CSgi_IO();
   solIO        =  new CSgi_IO();
   cDestIO      =  new CSgi_IO();
   cLlavePrivIO =  new CSgi_IO();
   cRemitenteIO =  new CSgi_IO();

   
   int iError[9];

   if (!sSol || !cDest || !cRemitente || !cLlavePriv || !datosGenIO || !solIO || !cDestIO ||!cLlavePrivIO ||!cRemitenteIO)
      return false;

   iError[0] = cRemitenteIO->inicia(IO_in,sCertRemitente.c_str());
   iError[1] = cLlavePrivIO->inicia(IO_in, sLlavePriv.c_str());
   iError[2] = cDestIO->inicia(IO_in,sCertDest.c_str());
   iError[3] = solIO->inicia(IO_out,bSolicCert,&ibSolicCert);
   //iError[4] = datosGenIO->inicia(IO_in,No hay con buffer);  
   iError[5] = cLlavePriv->inicia(cLlavePrivIO,cPassword,iPassword); // Verificar el password 
   iError[6] = cRemitente->inicia(TC_UTF8,cRemitenteIO);
   iError[7] = cDest->inicia(TC_UTF8,cDestIO); 
   iError[8] = sSol->inicia(solIO); 

//   remitentes[0] = cRemitente;
 //  llaves[0]=cLlavePriv;
//   vRemitente.add( cRemitente); 
   //sSol->genera( vRemitente,cLlavePriv,datosGenIO,TPKCS7_Firmado,solIO,AEnc_AES_128_CBC,cDest);
   sSol->genera( remitentes,llaves,datosGenIO,TPKCS7_Firmado,solIO,AEnc_AES_128_CBC,  NULL);

}
*/
bool certificador::
solicitudCert()
{
   bool bSolicitudCert = false;
   char cSolicitud[8000];
   char cReqB64[3000];
   int iReqB64 = sizeof(cReqB64);

   if ( B64_Codifica((uint8*)cReq, iReq,cReqB64,(uint16*)&iReqB64)) 
   {
      if (numeroSerie(false))
      {
         strncpy(cSolicitud,cNumSerie,20);
         strncpy(cSolicitud+20,"|",1);
         strncpy(cSolicitud+20+1, cExtensionesEn, iTamExtensiones);
         strncpy(cSolicitud+20+iTamExtensiones+1,"|",1);
         strncpy(cSolicitud+20+2+iTamExtensiones, (const char*)cReqB64,iReqB64);
   
         armaPKCS7(cSolicitud,20+iTamExtensiones+iReqB64 +3);
      }
   }


   return bSolicitudCert;
}

void certificador::
principal(int iArgs, char *cArgs[])
{

   if ( lectArgumentos(iArgs, cArgs) )
   {
      if ( bGenLocal )
      {
         if ( validacionDeLlavesPrivadayPublica())
         {
            if( validaRequerimiento() )
            {
               if (leerExtensiones())
               {
                  if ( generaCertificado() )
                  {
                     // Guarda datos en archivo
                     printf("Certificado generado.\n");
                  }
               }
            }
         }
      }
      else 
      {
         leerExtensiones();
         validaRequerimiento();
         solicitudCert();// Armar PKCS7 para envio de generacion.
         conectaServACGen();
         // Armar PKCS7 para envio de generacion.
         // Enviar 
         // Recibir  
         // Guardar.
         // Adelantar el numero de serie
      }

   }   
}
