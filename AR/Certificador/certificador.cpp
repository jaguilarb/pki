static const char* _CERTIFICADOR_CPP_VERSION ATR_USED ="@(#) certificador ( L : DSIC10482AR_ : certificador.cpp : 1.0.1 : 1 : 08/12/10)";
#include <certificador.h>


using namespace SgiCripto;

//
//  Funciones para el socket.
//
char  PASSLLAVCLISKT[50];

static int password_cb(char *buf, int size, int rwflag, void *password)
{
   rwflag = 0;
   password =PASSLLAVCLISKT;
   strncpy(buf, (char *)(password), size);
   buf[size - 1] = '\0';
   return(strlen(buf));
}


int verify_callback(int ok, X509_STORE_CTX *store)
{
   char data[256];
   if (!ok)
   {
      X509 *cert = X509_STORE_CTX_get_current_cert(store);
      int depth = X509_STORE_CTX_get_error_depth(store);
      int err = X509_STORE_CTX_get_error(store);

      fprintf(stderr,"Error con el certificado %i\n",depth);
      X509_NAME_oneline(X509_get_issuer_name(cert),data,256);
      fprintf(stderr,"issuer = %s\n",data);
      X509_NAME_oneline(X509_get_subject_name(cert),data,256);
      fprintf(stderr,"subject=%s\n",data);
      fprintf(stderr,"err%i:%s\n",err,X509_verify_cert_error_string(err));
   }
   return (ok);
}


certificador::
certificador()
{
   ERR_load_crypto_strings();
   OpenSSL_add_all_algorithms();

   bGenLocal      = false;
   cTipoCer       = NULL;
   cExtensiones   = NULL;
   cCertAC        = NULL;
   cLlavePriv     = NULL;
   cPassPriv      = NULL;
   cRequerimiento = NULL;
   cCadCert       = NULL;
   cConfiguracion = NULL;
   cNumAC         = NULL;
   iReq           = sizeof(cReq);
   sktAC          = NULL;
   llavePrivada   = NULL;
   extensiones    = NULL;
}


certificador::
~certificador()
{
   if ( cTipoCer != NULL )
   {
      free( cTipoCer);
      cTipoCer = NULL;
   }
   if (cExtensiones != NULL)
   {
      free(cExtensiones);
      cExtensiones = NULL;
   }
   if (cCertAC != NULL) 
   {
      free(cCertAC);
      cCertAC = NULL;
   }
   if (cLlavePriv != NULL)
   {
      free(cLlavePriv);
      cLlavePriv  = NULL;
   }
   if (cPassPriv != NULL)
   {
      free(cPassPriv);
      cPassPriv = NULL;
   }
   if ( cRequerimiento != NULL)
   {
      free(cRequerimiento);
      cRequerimiento = NULL;
   }
   if ( cCadCert != NULL)
   {
      free(cCadCert);
      cCadCert = NULL;
   }
   if (cConfiguracion != NULL)
   {
      free(cConfiguracion);
      cConfiguracion = NULL;
   }
   if (cNumAC != NULL)
   {
      free(cNumAC);
      cNumAC = NULL;
   }
   if (sktAC != NULL)
   {
      delete sktAC;
      sktAC = NULL;
   }
   if (llavePrivada != NULL)
   {
      RSA_free(llavePrivada);
      llavePrivada = NULL;
   } 
   if (extensiones != NULL)
   {
      delete extensiones;  
      extensiones = NULL;
   }

}

bool certificador::
uso()
{
   printf("Aplicaci�n certificador.\nUso :\n %s %27s %84s %77s %86s %105s %95s %67s %10s %29s %84s %92s %95s %77s %77s \n %49s\n",
         "Generaci�n Local.\n",
         "-l Generaci�n Local\n",
         "-t Tipo de Certificado a generar( ocsp,crl,selladora,clienteSSL,servidorSSL)\n",
         "-e Archivo donde se encuentran las extensiones para los certificados.\n",
         "-c Ruta y nombre del certificado de la AC que se utilizar� para la generaci�n.\n",
         "-k Ruta y nombre de la llave privada correspondiente a la AC que se utilizar� para la generaci�n.\n",
         "-r Ruta y nombre del requerimiento que se utilizar� para la generaci�n del certificado.\n",
         "-w Ruta y Nombre del archivo de la cadena de certificaci�n.\n",
         "Generaci�n Externa\n",
         "-x Generaci�n Externa\n",
         "-t Tipo de Certificado a generar( ocsp,crl,selladora,clienteSSL,servidorSSL)\n",
         "-a N�mero de AC que se utilizar� para firmar el requerimiento (generar certificado).\n",
         "-r Ruta y nombre del requerimiento que se utilizar� para la generaci�n del certificado.\n",
         "-e Archivo donde se encuentran las extensiones para los certificados.\n",
         "-f Ruta y nombre del archivo de configuraci�n para servicio de la AC.\n",
         "-v Despliega la versi�n de la aplicaci�n.\n"
         );

   return false;

}

bool certificador::
esNumero()
{
   bool bnumero = false;
   int iLetra   = -1;
   int tamNumAC = strlen(cNumAC);
   for(int i=0 ;i< tamNumAC;i++)
   {
      iLetra = isalpha(cNumAC[i]);
      if (iLetra != 0)
        break;
      else bnumero = true;
   }
}


bool certificador::
lectArgumentos(int nArgs, char* cArgs[])
{
   int  iOpcion = -1;
   int  iTam = -1;
   bool bCompletos = false;

   if ( nArgs ==  1)
      return uso();

   while( (iOpcion = getopt( nArgs,cArgs, "vlxt:e:c:k:r:w:f:a:" )) != -1 )
   {
      iTam = 0;
      if (optarg != NULL)
         iTam = strlen(optarg);
      switch( iOpcion )
      {
         case 'l' : // AC local.(llave PKCS8)
            bGenLocal = true;
            break;
         case 'x' : // AC externa.(Conexion AC)
            bGenLocal = false;
            break;
         case 't' :  //  Tipo de Certificado a generar.
            cTipoCer =  (char *)malloc( sizeof(char) * iTam+1 );
            cTipoCer[0] = '\0';
            strncpy(cTipoCer,optarg,iTam);
            break;
         case 'e' :  // Archivo donde se encontraran las extensiones para la generacion del certificado.
            cExtensiones = (char *)malloc( sizeof(char) * iTam+1 ); 
            cExtensiones[0] = '\0';
            strncpy(cExtensiones,optarg,iTam);
            break;
         case 'c' :  // Certificado de la AC local.
            cCertAC  =  (char *)malloc( sizeof(char) * iTam+1 );
            cCertAC[0] = '\0';
            strncpy(cCertAC,optarg,iTam);
            break;
         case 'k' :
            cLlavePriv  = (char *)malloc( sizeof(char) * iTam+1 ); 
            cLlavePriv[0] = '\0';
            strncpy(cLlavePriv,optarg,iTam);
            break;
         case 'r' :
            cRequerimiento  = (char *)malloc( sizeof(char) * iTam+1 );
            cRequerimiento[0] = '\0';
            strncpy(cRequerimiento,optarg,iTam);
            break;
         case 'w' :
            cCadCert  = (char *)malloc( sizeof(char) * iTam+1 ); 
            cCadCert[0] = '\0';
            strncpy(cCadCert,optarg,iTam);
            break;
         case 'f':
            cConfiguracion = (char *)malloc( sizeof(char) * iTam+1 );
            cConfiguracion[0] = '\0';
            strncpy(cConfiguracion,optarg,iTam);
            break;
         case 'a':
            cNumAC = (char*)malloc( sizeof(char) * iTam+1 );
            memset(cNumAC, 0,iTam+1);
            strncpy(cNumAC,optarg,iTam);
            if (!esNumero())
            {
               Bitacora->escribePV(BIT_ERROR, "Error en certificador en lectArgumentos: Debe de colocarse un n�mero, no una letra");
               printf("En la opcion -a debe colocarse un numero no letras.\n");
               return false;
            }
            break;
         case 'v':
            printf("certificador versi�n %s\n",VERSION);
            return false;
            break;
         default:
            Bitacora->escribePV( BIT_ERROR, "Error en certificador en lectArgumentos: Argumento inv�lido %s ",iOpcion);
            printf("Argumento inv�lido.\n");
            return false;
            break;
      }
   }

   if ( bGenLocal && (cExtensiones!=NULL) && (cCertAC!=NULL) && (cTipoCer!=NULL) && (cRequerimiento!=NULL)&& (cLlavePriv!=NULL) && (cCadCert!=NULL) )
   {
      bCompletos = true;
      Bitacora->escribePV( BIT_INFO, "Generaci�n de Certificado Local");
      printf("\n==== >>  Generacion de Certificado Local.\n\n");
   }
   else if ( !bGenLocal && (cExtensiones!=NULL) && (cTipoCer!=NULL) && (cRequerimiento!=NULL) && (cConfiguracion!=NULL) && (cNumAC!=NULL))
   {
      bCompletos = true;
      Bitacora->escribePV( BIT_INFO, "Generaci�n de Certificado Externo");
      printf("\n==== >>  Generacion de Certificado Externa.\n\n");
   }
   else
      return uso();

  return bCompletos; 
    
}

bool certificador::
leerPassword() throw (ExcepcionCertificador)
{
   char *cLecPass = NULL;
   char cPass [50];
   bool leerPassword = false;

   memset(cPass,0,sizeof(cPass));
 
   cLecPass  = getpass("- Escribir password de la llave privada : ");
   if(!cLecPass)
   {
      Bitacora->escribePV( BIT_ERROR, "Error en certificador en leerPassword: No se escribi� el password correctamente", cLecPass); 
      throw ExcepcionCertificador(200,"Escribir el password correctamente.");
   }

   strncpy(cPass,cLecPass,strlen(cLecPass));
   delete cLecPass;
   cLecPass = NULL;

   cPassPriv =  new char(strlen(cPass)+1);
   
   strncpy( cPassPriv,cPass,strlen(cPass));
   cPassPriv[strlen(cPass)] = 0;
   leerPassword = true;

   if (cLecPass != NULL)
      cLecPass = NULL;

   return leerPassword;
}



bool certificador::
validacionDeLlavesPrivadayPublica() throw (ExcepcionCertificador)
{
   CSgi_IO          *certIO      = NULL;
   CSgi_IO          *llavePrivIO = NULL;
   CSgi_Certificado *cert        = NULL;
   CSgi_LlavePriv   *llavePriv   = NULL;
   CSgi_CadCert     *cadCert     = NULL;

   bool bCorresponden = false;
   int iError[6];
   char cAC[150];
   int  iAC = sizeof(cAC);

   
   certIO      = new CSgi_IO();
   llavePrivIO = new CSgi_IO();
   llavePriv   = new CSgi_LlavePriv();
   cert        = new CSgi_Certificado();
   cadCert     = new CSgi_CadCert();

   if ( !certIO || !cert || !llavePrivIO || !llavePriv ||!cadCert)
   {
      Bitacora->escribePV(BIT_ERROR, "Error en certificador en validacionDeLlavesPrivadayPublica: No se asign� memoria para los objetos",
      certIO, llavePrivIO, llavePriv, cert, cadCert);
      throw ExcepcionCertificador(100,"No se le asign� memoria a los objetos.");
   }

   leerPassword();

   iError[0] = certIO->inicia(IO_in,cCertAC);
   iError[1] = llavePrivIO->inicia(IO_in,cLlavePriv);
   iError[2] = cert->inicia(TC_UTF8, certIO);
   iError[3] = llavePriv->inicia(llavePrivIO, (uchar*)cPassPriv,strlen(cPassPriv));   
   iError[4] = cadCert->inicia(cCadCert);

   if ( iError[0] )
      throw ExcepcionCertificador(iError[0], "No se pudo abrir el flujo IO para el certificado especificado.");
   if ( iError[1] )
      throw ExcepcionCertificador(iError[1], "No se pudo abrir el flujo IO para la llave privada especificada.");
   if ( iError[2] )
      throw ExcepcionCertificador(iError[2], "No se pudieron obtener los datos del certificado.");
   if ( iError[3] )
      throw ExcepcionCertificador(iError[3], "No se pudo obtener la llave privada.");
   if ( iError[4] )
      throw ExcepcionCertificador(iError[4], "No se pudieron cargar los certificados de la cadena de certificaci�n.");
   
   if ( !(iError[5] = llavePriv->verificaLlaves( cert )) )
   {
      if ( !(iError[5] = cadCert->verifica(CTipoDesconocido, cert, NULL)) )
      {
         if ( !(iError[5] = cert->getExtension(Ext_basic_constraints, cAC, &iAC)) )
         {
            if ( strcmp(cAC, "0") != 0 )
            {
               EVP_PKEY* evpLlavePrivada = llavePriv->getEvpKey();
               llavePrivada  = EVP_PKEY_get1_RSA(evpLlavePrivada);
               if (llavePrivada != NULL)
                  bCorresponden =  true;
            }else{ 
                  Bitacora->escribePV(BIT_ERROR, "Error en certificador en validacionDeLlavesPrivadayPublica: La llave Privada y certificado no son de AC.");
                  throw ExcepcionCertificador(102,"La llave Privada y certificado no son de AC.");
                  }
            
         }else{ 
               Bitacora->escribePV(BIT_ERROR, "Error en certificador en validacionDeLlavesPrivadayPublica: Error al obtener el basic constraint del certificado.");
               throw ExcepcionCertificador(103,"Error al obtener el basic constraint del certificado.");
               }
         
      }else{ 
            Bitacora->escribePV(BIT_ERROR, "Error en certificador en validacionDeLlavesPrivadayPublica: Error al verificar la cadena de certificacion.");
            throw ExcepcionCertificador(104,"Error al verificar la cadena de certificacion.");
            }
            
   }else{ 
         Bitacora->escribePV(BIT_ERROR, "Error en certificador en validacionDeLlavesPrivadayPublica:"
         " No corresponden la llave privada y la llave publica.");         
         throw ExcepcionCertificador(105,"No corresponden la llave privada y la llave publica.");
         }

   delete certIO;      certIO=NULL;
   delete cert;        cert=NULL;
   delete llavePrivIO; llavePrivIO =NULL; 
   delete llavePriv;   llavePriv =NULL;

   Bitacora->escribePV(BIT_INFO, "certificador.validacionDeLlavesPrivadayPublica: La llave privada y el certificado son correctos");
   printf("- Verificacion de llave privada y certificado correcta.\n");

   return bCorresponden;
}



bool certificador::
validaRequerimiento() throw (ExcepcionCertificador)
{ 

   FILE *fReq = NULL;
   bool bValidado = false;
   int iError;
   iReq = -1;

   SGIX509 req;
   SNames nombresVal[6]; 
   SNames atributos[1];
   int    iNombres[6]   = {NID_organizationName,NID_organizationalUnitName,NID_countryName,NID_stateOrProvinceName,NID_localityName,NID_commonName};
   int    iAtributos[1] = {NID_pkcs9_challengePassword};
   int    iNoNULL =0 ;


   fReq = fopen(cRequerimiento, "r");
   if (fReq != NULL)
   { 
      iReq = fread(cReq, 1, sizeof(cReq), fReq);
      if (iReq > 0) 
      {
         iError = req.ProcREQ(cReq, iReq, nombresVal, atributos, NULL, iNombres, 6, iAtributos, 1);
         if (!iError)
         {
            for(int i=0;i<6;i++)
            {
               if(  nombresVal[i].dato == NULL)
                  break;
               else iNoNULL++;
            }    
            if (iNoNULL == 6)
            {
               nombres[0].Inicia();
               strcpy(nombres[0].dato,nombresVal[5].dato);
               strcpy(nombres[0].id,nombresVal[5].id);
               bValidado = true;
            }else throw ExcepcionCertificador(300,"El requerimiento no contiene los datos minimos para poder generar un certificado.");
         }else  throw ExcepcionCertificador(301,"Error al obtener los datos del requerimiento.");
      }else  throw ExcepcionCertificador(302,"Error al leer el requerimiento.");
   }else 
        { 
         Bitacora->escribePV(BIT_ERROR, "Error en certificador en validaRequerimiento: Error al abrir el archivo del requerimiento");
         throw ExcepcionCertificador(errno,"Error al abrir el requerimiento ,verificar.");
        }

   if (fReq != NULL)
   {
      fclose(fReq);
      fReq = NULL;
   }

   Bitacora->escribePV(BIT_INFO, "Error en certificador en validaRequerimiento: Requerimiento con datos completos");
   printf("- Verificacion correcta del requerimiento (datos completos).\n");

   return bValidado;
}

bool certificador::
leerExtensiones() throw (ExcepcionCertificador)
{
   CArchCfgSC ext;
   int iNumDatosExt;
   bool bExtensiones = false;

   iTamExtensiones = 0;
   if ( ext.cargaArchivo(cExtensiones) )
   {
      ext.get("repositorio", "repositorioCerts", sRepositorio);

      iNumDatosExt = ext.length(cTipoCer);
      if (iNumDatosExt != 0) 
      {
         for (int i =0; i<iNumDatosExt; i++)
         {
            std::string sNombreExt;
            std::string sValorExt;

            ext.get(cTipoCer, i, sNombreExt, sValorExt);

            if (sNombreExt.compare("vigencia") == 0)
               continue;

            if(bGenLocal)
            {
               if (i==0)
                  extensiones = new SNames[iNumDatosExt];
               if (extensiones != NULL)
               {

                  extensiones[i].Inicia();
                  strncpy(extensiones[i].id,(char*)sNombreExt.c_str(),sNombreExt.size());
                  strncpy(extensiones[i].dato,(char*)sValorExt.c_str(),sValorExt.size());
               }else{
                     Bitacora->escribePV(BIT_ERROR, "Error en certificador en leerExtensiones: No se cre� el arreglo para las ectensiones ",
                     sNombreExt.c_str(),sValorExt.c_str());
                     throw ExcepcionCertificador(400,"No creo el arreglo de los SNames para las extensiones.");
                    }
            }
            else
            {
               sprintf(cExtensionesEn+iTamExtensiones,"%s=%s^", sNombreExt.c_str(),sValorExt.c_str());
               iTamExtensiones = strlen(cExtensionesEn);
            }
         }
         ext.get(cTipoCer, (const char*)"vigencia", sVigencia);
         iExtensiones = iNumDatosExt-1;
         bExtensiones = true;
      }else{
            Bitacora->escribePV(BIT_ERROR, "Error en certificador en leerExtensiones:"
            " No existe en el archivo de configuracion los datos para este tipo de certificado.");
            throw ExcepcionCertificador(401,"No existe en el archivo de configuracion los datos para este tipo de certificado.");
            } 
      
      
   }else{
         Bitacora->escribePV(BIT_ERROR, "Error en certificador en leerExtensiones:"
         "Verificar si el archivo de las extensiones existe o tiene bien la estructura.");      
         throw ExcepcionCertificador(errno,"Verificar si el archivo de las extensiones existe o tiene bien la estructura.");
        }

   
   return bExtensiones;
}

bool certificador::
numeroSerie(bool bAumentar)
{

   FILE *fNumSerie = NULL;
   bool bNumSerie= false;
   char cNumSerieAux[21];
   char cSerial[9];

   int iNumSerie = sizeof(cNumSerie);

   
   fNumSerie = fopen("serial", "r+");
   if (fNumSerie != NULL)
   {
      if (bAumentar)
      {
         iNumSerie = fwrite(cNumSerie,1,20,fNumSerie); 
         if (iNumSerie == 20)
            bNumSerie = true;
         
         else{
              Bitacora->escribePV(BIT_ERROR, "Error en certificador en numeroSerie: Error al escribir el numero de serie.");
              throw ExcepcionCertificador(500, "Error al escribir el numero de serie.");
             } 
      }
      else
      {
         memset(cNumSerieAux,0,sizeof(cNumSerieAux));
         memset(cSerial,0,sizeof(cSerial));
         memset(cNumSerie,0,sizeof(cNumSerie));

         iNumSerie = fread(cNumSerieAux,1,20,fNumSerie);
         if (iNumSerie == 20)
         {
            cNumSerie[20] = 0;
            strncpy(cNumSerie,cNumSerieAux,12);
            strncpy(cSerial,cNumSerieAux+12,8);
            cSerial[8] =0;
            int serial =  atoi(cSerial);
            serial++;
            
            int iCeros = 0;
            if (serial < 10)
               iCeros = 7;
            else if (serial < 100 )
               iCeros = 6;
            else if (serial < 1000)
               iCeros = 5;
            else if (serial < 10000)
               iCeros = 4;
            else if (serial < 100000)
               iCeros = 3;
            else if (serial < 1000000)
               iCeros = 2;
            else if (serial < 10000000)
               iCeros = 1;
            else
               iCeros = 0;

            for(int i=0;i<iCeros;i++)
               cSerial[i]= '0';

            sprintf(cSerial+iCeros,"%d",serial); 
            strncpy(cNumSerie+12,cSerial,8); 
            bNumSerie = true; 

            Bitacora->escribePV(BIT_INFO, "certificador.numeroSerie: Se utiliza el n�mero de serie",cNumSerie);
            printf("- N�mero de serie a utilizar para la generaci�n [ %s ]\n",cNumSerie); 
         }
      } 
   } else{
          Bitacora->escribePV(BIT_ERROR, "Error en certificador en numeroSerie: Error al abrir el archivo de numero de serie.");
          throw ExcepcionCertificador(500, "Error al abrir el archivo de numero de serie.");
         }

   if (fNumSerie !=  NULL)
   {
      fclose(fNumSerie);
      fNumSerie = NULL;
   }   

   return bNumSerie;
}

bool certificador::
generaCertificado() throw (ExcepcionCertificador)
{
   bool bGenerado = false;
   SGIX509 cert;
   uchar bCert[6000];
   std::string sNombre;
   int iCert = sizeof(bCert);
   int iError;
   
   numeroSerie(false);

   iError = cert.GenCERT(cReq, iReq, cCertAC, (unsigned char*)cNumSerie, atoi(sVigencia.c_str()), llavePrivada, extensiones, iExtensiones,
                         NULL, ID_DER, bCert , &iCert );
   if(!iError)
   {
      guardaCertificado(bCert, iCert,sNombre);
      if (guardaRegistro(sNombre ))
      {
        numeroSerie(true);
        bGenerado = true;
      }
      else
      {
         unlink(sNombre.c_str());
         Bitacora->escribePV(BIT_ERROR, "Error en certificador en generaCertificado: Error al guardar el registro del certificado.");
         throw ExcepcionCertificador(500, "Error al guardar el registro del certificado.");
      }
   } else{
          Bitacora->escribePV(BIT_ERROR, "Error en certificador en generaCertificado: Error al generar el certificado."); 
          throw ExcepcionCertificador(iError, "Error al generar el certificado.");
         } 

   Bitacora->escribePV(BIT_INFO, "certificador.generaCertificado:",sNombre.c_str()); 
   printf("- Certificado generado y guardado en [ %s ]\n\n",sNombre.c_str());

   return bGenerado;
}

bool certificador::
guardaRegistro(std::string sNombre) 
{
   X509 *cert = NULL;
   X509_NAME *sujeto = NULL;
   X509_NAME *emisor = NULL;
   FILE  *fCert = NULL;
   FILE  *fReg  = NULL;
   bool guardaRegistro = false;

   fCert = fopen(sNombre.c_str(),"r");
   if ( fCert !=  NULL)
   {
      cert = d2i_X509_fp(fCert, NULL);
      if(cert!= NULL) 
      {
         sujeto = X509_get_subject_name(cert);
         emisor = X509_get_issuer_name(cert);         

         if (sujeto != NULL &&  emisor != NULL)
         {
            fReg = fopen("RegistroCertGenerados.reg","a"); 
            if (fReg != NULL)
            {
               fprintf(fReg,"\n%s|%s|%s|%s|",
                              cNumSerie,cTipoCer, 
                             (const char*)cert->cert_info->validity->notBefore->data,
                             (const char*)cert->cert_info->validity->notAfter->data);
               X509_NAME_print_ex_fp(fReg, sujeto, 1, XN_FLAG_ONELINE&~XN_FLAG_SPC_EQ  );
               fwrite("|",1,1,fReg);
               X509_NAME_print_ex_fp(fReg, emisor, 1, XN_FLAG_ONELINE&~XN_FLAG_SPC_EQ   );
               guardaRegistro = true;
            }else{
                  Bitacora->escribePV(BIT_ERROR, "Error en certificador en guardaRegistro: Error al abrir el archivo para hacer el registro",errno);
                  printf("- Error al abrir el archivo para hacer el registro [%d].\n",errno);
                  } 
            
         }else{
               Bitacora->escribePV(BIT_ERROR, "Error en certificador en guardaRegistro: No se obtuvieron el sujeto y el emisor");
               printf("- No se obtuvieron el sujeto y el emisor.\n");
               }
     
      }else{
            Bitacora->escribePV(BIT_ERROR, "Error en certificador en guardaRegistro: No se obtuvo el certificado");
            printf("No se obtuvo el certificado.\n");
            }
              
   }else{ 
         Bitacora->escribePV(BIT_ERROR, "Error en certificador en guardaRegistro: Error al abrir el certificado",errno);
         printf("- Error al abrir el certificado [%d].\n",errno);
         }

   if(fReg != NULL)
   {
      fclose(fReg);
      fReg = NULL;
   }
   if(fCert != NULL)
   {
      fclose(fCert);
      fCert = NULL;   
   }

   return  guardaRegistro;
}

bool certificador::
guardaCertificado(uchar* cCert, int iCert, std::string &sNombre) throw (ExcepcionCertificador)
{
   bool bGuardaCertificado = false;
   FILE *fCert = NULL;
   std::string sRuta;

   if (!pkiRutaCert(sRepositorio, cNumSerie, sRuta,true))
   {
      fCert = fopen( sRuta.c_str(),"w");
      if (fCert != NULL)
      {
         fwrite(cCert,1,iCert,fCert);
         fclose(fCert);
         sNombre = sRuta;
         bGuardaCertificado = true;
      }else{ 
            Bitacora->escribePV(BIT_ERROR, "Error en certificador en guardaCertificado: Error al crear el archivo");
            throw ExcepcionCertificador(errno,"Error al crear el archivo.");
            }
   
   }else{ 
         Bitacora->escribePV(BIT_ERROR, "Error en certificador en guardaCertificado: Error al crear la ruta del certificado.");
         throw ExcepcionCertificador(errno,"Error al crear la ruta del certificado.");
         }

   return bGuardaCertificado;
}

////
/// Externo
////
bool certificador::
conectaServACGen() throw (ExcepcionCertificador)
{

   std::string sIP;
   std::string sPuerto;
   std::string sCertSKT;
   std::string sRutaCerts;
   std::string sCertCliente;
   std::string sLlaveCliente;
   std::string sPwdCliCif;

   CArchCfgSC configAC;
   char cPwdDes[50];
   int  iPwdDes;
   int  iNumDatosAC;
   int  iError;

   bool conectaServACGen = false;

 
   if ( configAC.cargaArchivo(cConfiguracion) )
   {
      iNumDatosAC = configAC.length("conexionAC");
      if (iNumDatosAC != 0)
      {
         configAC.get("conexionAC", "ip", sIP);
         configAC.get("conexionAC", "puerto", sPuerto);
         configAC.get("conexionAC", "certsSKT", sCertSKT);
         configAC.get("conexionAC", "rutaCerts", sRutaCerts);
         configAC.get("conexionAC", "certCliente", sCertCliente);
         configAC.get("conexionAC", "llaveCliente", sLlaveCliente);
         configAC.get("conexionAC", "passwordCliente", sPwdCliCif);
   
         if( !desencripta((uint8*)sPwdCliCif.c_str(),sPwdCliCif.length(),(uint8*)cPwdDes,&iPwdDes))
           {
            Bitacora->escribePV(BIT_ERROR, "Error en certificador en conectaServACGen: Error al descifrar el pwd de la llave del cliente socket.");
            throw ExcepcionCertificador(700,"Error al descifrar el pwd de la llave del cliente socket.");
            }

         cPwdDes[iPwdDes]=0;
         strncpy(PASSLLAVCLISKT, cPwdDes,iPwdDes);
         PASSLLAVCLISKT[iPwdDes]=0;

         paramAC.SetCliente( ESSL_entero ,NULL, (char *)sIP.c_str(), atoi(sPuerto.c_str()), (char *)sCertSKT.c_str(),
                                                (char *)sRutaCerts.c_str(), (char *) sCertCliente.c_str(),
                                                (char *)sLlaveCliente.c_str(), verify_callback, password_cb);
         sslAC.Inicia();

         iError = sslAC.Cliente(paramAC, sktAC);
         if(!iError)
         {
            if (!sktAC->setKEEPALIVE(900, 2,60))
               conectaServACGen = true;
         }else{ 
               Bitacora->escribePV(BIT_ERROR, "Error en certificador en conectaServACGen: Error al conectarse al servicio de la AC.");
               throw ExcepcionCertificador(iError,"Error al conectarse al servicio de la AC.");
               }
      
      } else{ 
             Bitacora->escribePV(BIT_ERROR, "Error en certificador en conectaServACGen: "
             "No se encontraron los parametros para la conexion con la AC en el archivo de configuracion.");
             throw ExcepcionCertificador(1000,"No se encontraron los parametros para la conexion con la AC en el archivo de configuracion.");
             }
   
   } else{
          Bitacora->escribePV(BIT_ERROR, "Error en certificador en conectaServACGen: Verificar que el archivo de configuraci�n exista.");
          throw ExcepcionCertificador(errno,"Verificar que el archivo de configuraci�n exista.");
          }
   
   return conectaServACGen;
}


bool certificador::
armaPKCS7(char* cSolicitud, int iSolicitud,char *bSolicCert, int *ibSolicCert) throw (ExcepcionCertificador)
{
   uchar cPassword[100];
   std::string sLlavePriv;
   std::string sCertRemitente; 
   std::string sPwdCliCif;
   int  iPassword;
   bool bArmaPKCS7 = false;

   PtrSgiIO        cRemitenteIO = NULL;
   PtrSgiIO        cLlavePrivIO = NULL;
   PtrSgiIO        datosGenIO   = NULL;
   PtrSgiIO        solIO        = NULL;
   PtrSgiCert      cRemitente   = NULL;
   PtrSgiLlavePriv cLlavePriv   = NULL;
   PtrSgiSobre     sobre        = NULL;
   PtrSgiCert      cdFirmantes[1];
   PtrSgiCert      cdDestinatarios[1];
   PtrSgiLlavePriv pkFirmantes[1];
   int iError[6];
   CArchCfgSC configPKCS7;

   memset(cPassword,0,sizeof(cPassword));

   if ( !configPKCS7.cargaArchivo(cConfiguracion) )
      {
       Bitacora->escribePV(BIT_ERROR, "Error en certificador en armaPKCS7: Verificar que el archivo de configuraci�n exista.");
       throw ExcepcionCertificador(800,"Error al cargar el archivo de configuracion");
       }

   int iNumDatosAC = configPKCS7.length("pkcs7");
   if (iNumDatosAC != 0)
   {
      configPKCS7.get("pkcs7", "certificado", sCertRemitente);
      configPKCS7.get("pkcs7", "llavePrivada", sLlavePriv);
      configPKCS7.get("pkcs7", "passwLlavePriv", sPwdCliCif);

      if( !desencripta((uint8*)sPwdCliCif.c_str(),sPwdCliCif.length(),(uint8*)cPassword,&iPassword))
      {
         Bitacora->escribePV(BIT_ERROR, "Error en certificador en armaPKCS7: Error al descifrar el pwd de la llave del cliente socket.");
         throw ExcepcionCertificador(801,"Error al descifrar el pwd de la llave del cliente socket.");
         }

   }else{ 
         Bitacora->escribePV(BIT_ERROR, "Error en certificador en armaPKCS7: Parametros para el pkcs7 incompletos en el archivo de configuracion.");
         throw ExcepcionCertificador(802,"Parametros para el pkcs7 incompletos en el archivo de configuracion.");
         }


   solIO        = new_SgiIO();
   datosGenIO   = new_SgiIO();
   cRemitenteIO = new_SgiIO();
   cLlavePrivIO = new_SgiIO();
   cLlavePriv   = new_SgiLlavePriv();
   cRemitente   = new_SgiCertificado();
   sobre        = new_SgiSobre();


   if (!solIO || !datosGenIO || !cRemitenteIO || !cLlavePrivIO || !cLlavePriv ||!cRemitente || !sobre )
   {
      Bitacora->escribePV(BIT_ERROR, "Error en certificador en armaPKCS7: No se crearon los objetos para PKCS7",
      solIO,datosGenIO,cRemitenteIO,cLlavePrivIO,cLlavePriv,cRemitente,sobre );
      throw ExcepcionCertificador(803,"No se crearon los objetos para PKCS7");
      }


   iError[0] = SgiIO::inicia(solIO, IO_out, (uchar*)bSolicCert, ibSolicCert);
   iError[1] = SgiIO::inicia(datosGenIO, IO_in, (uchar*)cSolicitud,&iSolicitud);
   iError[2] = SgiIO::inicia(cRemitenteIO, IO_in, (char *)sCertRemitente.c_str());
   iError[3] = SgiIO::inicia(cLlavePrivIO, IO_in, (char *)sLlavePriv.c_str() );
   iError[4] = SgiLlavePriv::inicia(cLlavePriv, cLlavePrivIO, cPassword,iPassword);
   iError[5] = SgiCertificado::inicia(cRemitente, TC_ASCII, cRemitenteIO);

   cdFirmantes[0] = cRemitente;
   pkFirmantes[0] = cLlavePriv;

   if (iError[0])
      throw ExcepcionCertificador(804,"Error al inicializar el objeto solIO");
   if (iError[1])
      throw ExcepcionCertificador(804,"Error al inicializar el objeto datosGenIO");
   if (iError[2])
      throw ExcepcionCertificador(804,"Error al inicializar el objeto cRemitenteIO");
   if (iError[3])
      throw ExcepcionCertificador(804,"Error al inicializar el objeto cLlavePrivIO");
   if (iError[4])
      throw ExcepcionCertificador(804,"Error al inicializar el objeto cLlavePriv");
   if (iError[5])
      throw ExcepcionCertificador(804,"Error al inicializar el objeto cRemitente");

   iError[0] = SgiSobre::genera(sobre, cdFirmantes, pkFirmantes, 1, datosGenIO, TPKCS7_Firmado, solIO, AEnc_DES_EDE3_CBC, cdDestinatarios, 0);
   if(!iError[0])
      bArmaPKCS7 = true;
   else
   {
      Bitacora->escribePV(BIT_ERROR, "Error en certificador en armaPKCS7: No se genero el sobre");
      throw ExcepcionCertificador(iError[0],"No se genero el sobre");
      }

   if (solIO != NULL)
      delete_SgiIO(solIO);
   if (datosGenIO != NULL)
      delete_SgiIO(datosGenIO);
   if (cRemitenteIO != NULL)
      delete_SgiIO(cRemitenteIO);
   if (cLlavePrivIO != NULL)
      delete_SgiIO(cLlavePrivIO);
   if (cLlavePriv != NULL)
      delete_SgiLlavePriv(cLlavePriv);
   if (cRemitente != NULL)
      delete_SgiCertificado(cRemitente);
   if (sobre != NULL)
      delete_SgiSobre(sobre);

   return  bArmaPKCS7;
}

bool certificador::
solicitudCert() throw (ExcepcionCertificador)
{
   bool  bSolicitudCert = false;
   char  cSolicitud[8000];
   char  cCertificado[8000];
   char  bSolicCert[10000];
   char  cReqB64[3000];
   std::string sNombre;
   int   iCertificado  = sizeof(cCertificado);
   int   ibSolicCert   = sizeof(bSolicCert);
   int   iReqB64       = sizeof(cReqB64);
   int  iSolicitud     = sizeof(cSolicitud);

   if ( B64_Codifica((uint8*)cReq, iReq,cReqB64,(uint16*)&iReqB64)) 
   {

      numeroSerie(false);
      int iTamVig    = sVigencia.size();
      int iTamTipCer = strlen(cTipoCer);
      int iTamAC     = strlen(cNumAC);
      int iResultado = -1;

      strncpy(cSolicitud,cNumSerie,20);
      strncpy(cSolicitud+20,"|",1);
      strncpy(cSolicitud+20+1,cTipoCer,iTamTipCer);
      strncpy(cSolicitud+20+1+iTamTipCer,"|",1);
      strncpy(cSolicitud+20+2+iTamTipCer,cNumAC,iTamAC);
      strncpy(cSolicitud+20+2+iTamTipCer+iTamAC,"|",1);
      strncpy(cSolicitud+20+3+iTamTipCer+iTamAC,sVigencia.c_str(),iTamVig);
      strncpy(cSolicitud+20+3+iTamTipCer+iTamAC+iTamVig,"|",1);
      strncpy(cSolicitud+20+4+iTamTipCer+iTamAC+iTamVig, cExtensionesEn, iTamExtensiones);
      strncpy(cSolicitud+20+4+iTamTipCer+iTamAC+iTamVig+iTamExtensiones,"|",1);
      strncpy(cSolicitud+20+5+iTamTipCer+iTamAC+iTamVig+iTamExtensiones, (const char*)cReqB64,iReqB64);

      iSolicitud = 20+5+iTamTipCer+iTamAC+iTamVig+iTamExtensiones+iReqB64;

      armaPKCS7(cSolicitud,iSolicitud,bSolicCert+2,&ibSolicCert); 

      uint16  tamPKCS7_1 = (uint16) htons(ibSolicCert);
        
      memcpy(bSolicCert,&tamPKCS7_1,sizeof(uint16));

      if ( sktAC != NULL)
      {
         if ( !sktAC->Envia((uint8*)bSolicCert,ibSolicCert+2) )
         {
            int c_len = 2;
            iResultado = sktAC->Recibe((uint8*)cCertificado,(int &)c_len); 
            if ( !iResultado )
            {
               memcpy( &c_len, cCertificado, 2 ); 
               iCertificado = ntohs( c_len );
               if (cCertificado[0] != -1)
               {
                  iResultado = sktAC->Recibe((uint8*)cCertificado,iCertificado);
                  guardaCertificado((uchar*)cCertificado,iCertificado,sNombre);
                  if(guardaRegistro(sNombre))
                  {
                     numeroSerie(true);
                     Bitacora->escribePV(BIT_ERROR, "Error en certificador en solicitudCert: Certificado generado y guardado en %s",sNombre.c_str());
                     printf("- Certificado generado y guardado en [ %s ].\n\n",sNombre.c_str());
                  }
                  else{
                       Bitacora->escribePV(BIT_ERROR, "Error en certificador en solicitudCert: Error al Registrar el certificado en el registro "); 
                       printf("- Error al Registrar el certificado en el registro [%d].\n",errno);
                       }
               
               }else{ 
                     Bitacora->escribePV(BIT_ERROR, "Error en certificador en solicitudCert: "
                     "Certificado no generado en la AC revisar el servicio CertificadorAC."); 
                     throw ExcepcionCertificador(901,"- Certificado no generado en la AC revisar el servicio CertificadorAC.");
                     }
               
            }else{
                  Bitacora->escribePV(BIT_ERROR, "Error en certificador en solicitudCert: Error al recibir del servicio.",iResultado);
                  throw ExcepcionCertificador(iResultado,"- Error al recibir del servicio.");
                  }
                        
         }else{ 
               Bitacora->escribePV(BIT_ERROR, "Error en certificador en solicitudCert: Error al enviar al servicio.");
               throw ExcepcionCertificador(errno,"- Error al enviar al servicio.");
               }
         
      }else{ 
            Bitacora->escribePV(BIT_ERROR, "Error en certificador en solicitudCert: No se ha conectado al CertificadorAC.");
            throw ExcepcionCertificador(902,"- No se ha conectado al CertificadorAC.");
            }
   
   }else{ 
         Bitacora->escribePV(BIT_ERROR, "Error en certificador en solicitudCert: Error al codificar en B64.");
         throw ExcepcionCertificador(900,"- Error al codificar en B64.");
         }    

   return bSolicitudCert;
}


void certificador::
principal(int iArgs, char *cArgs[])
{
   try
   {
      if ( lectArgumentos(iArgs, cArgs) )
      {
         if ( bGenLocal )
         {
            leerExtensiones();
            validaRequerimiento();
            validacionDeLlavesPrivadayPublica();
            generaCertificado();
         }
         else 
         {
            leerExtensiones();
            validaRequerimiento();
            conectaServACGen();
            solicitudCert();
         }

      }   
   }
   catch (ExcepcionCertificador ex)
   {
         printf("\n- Resultado : %s [%d]\n\n",ex.descripcion(),ex.numError());
   }
}
