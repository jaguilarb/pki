static const char *_EXCEPCIONCERTIFICADOR_H_VERSION_ ATR_USED ="@(#) certificador ( L : DSIC10482AR_ : ExcepcionCertificador.h : 1.0.1 : 1 : 08/12/10)";

#ifndef _EXCEPCIONCERTIFICADOR_H_
#define _EXCEPCIONCERTIFICADOR_H_

#include <ExcepcionPKI.h>

typedef struct
{
   int   iError;
   char* cDesc;
} Excepcion;


class ExcepcionCertificador: public ExcepcionPKI
{
   public:
      ExcepcionCertificador(int iErr,const char* cDes) : ExcepcionPKI(iErr), iError(iErr) ,cDesc(cDes) {}
      int numError(){return iError;}
      virtual const char* descripcion() const throw() {return cDesc;};
   private:
      int iError;
      const char* cDesc;
};
#endif
