static const char * _CERTIFICADOR_H_VERSION ATR_USED ="@(#) certificador ( L : DSIC10482AR_ : certificador.h : 1.0.1 : 1 : 30/11/10)";

#include <stdio.h>
#include <ctype.h>
#include <unistd.h>
#include <string.h>
#include <openssl/err.h>
#include <Sgi_Cripto.h>
#include <Sgi_LlavePriv.h>
#include <Sgi_Certificado.h>
#include <Sgi_CadCert.h>
#include <Sgi_Sobre.h>
#include <Sgi_LlavePriv.h>
#include <Sgi_IO.h>
#include <Sgi_OpenSSL.h>
#include <Sgi_ConfOpen.h>
#include <Sgi_SSL.h>
#include <Sgi_ProtegePwd.h>
#include <arpa/inet.h>
#include <ExcepcionCertificador.h>




#include <Sgi_PKI.h>
#include <Sgi_Bitacora.h>
#include <Sgi_MsgPKI.h> 
#include <Sgi_ConfigFile.h> 




#define VERSION   "1.0.1"

class certificador
{


   public:
         certificador();
        ~certificador();

         void principal(int nArgs, char* cArgs[]); 

   private:

         bool bGenLocal; 
         char *cTipoCer;
         char *cExtensiones;
         char *cCertAC;
         char *cLlavePriv;
         char *cPassPriv;
         char *cRequerimiento;
         char *cCadCert;
         char *cConfiguracion;
         char *cNumAC;
         char  cNumSerie[21];
         char  cExtensionesEn[3000];
         uchar cReq[6000];
         std::string sVigencia;
         std::string sRepositorio;

         int  iExtensiones;
         int  iTamExtensiones;
         int  iReq;

         CSSL_parms  paramAC;
         CSSL_Skt   *sktAC;
         Sgi_SSL     sslAC;
         
         CBitacora *Bitacora;

         RSA *llavePrivada;
         SNames nombres[1];
         SNames *extensiones;

                           
         bool uso();
         bool validacionDeLlavesPrivadayPublica() throw (ExcepcionCertificador);
         bool lectArgumentos(int nArgs, char* cArgs[]);
         bool leerPassword() throw (ExcepcionCertificador);
         bool validaRequerimiento() throw (ExcepcionCertificador);
         bool generaCertificado() throw (ExcepcionCertificador);
         bool leerExtensiones()throw (ExcepcionCertificador);
         bool numeroSerie(bool bAumentar); 
         bool conectaServACGen() throw (ExcepcionCertificador);
         bool solicitudCert() throw (ExcepcionCertificador);
         bool armaPKCS7(char* cSolicitud, int iSolicitud,char *bSolicCert, int *ibSolicCert) throw (ExcepcionCertificador);
         bool guardaCertificado(uchar* cCert, int iCert,  std::string &sNombre ) throw (ExcepcionCertificador);
         bool guardaRegistro( std::string sNombre );
         bool esNumero(); 


};
