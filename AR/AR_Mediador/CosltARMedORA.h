#ifndef _COSLTARMEDORA_H_
#define _COSLTARMEDORA_H_

static const char* _COSLTARMEDORA_H_VERSION_ ATR_USED = "@(#) SrvAC ( L : DSIC10392AR_ : ConsltARMedORA.h : 1.0.0 : 10/10/14)";

//#VERSION: 1.1.0
/*##############################################################################
  ###  PROYECTO:              PKI-SAT                                        ###
  ###  MODULO:                Servicio AR                                    ###
  ###                                                                        ###
  ###                                                                        ###
  ###  DESARROLLADORES:       Guadalupe Negrete Castro         GNC           ###
  ###                                                                        ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       Viernes 10, octubre del 2014                   ###
  ###                                                                        ###
  ##############################################################################
         1         2         3         4         5         6         7         8
12345678901234567890123456789012345678901234567890123456789012345678901234567890

################################################################################
  VERSION:
     V.1.0.0  (20141010- )    GNC: Version Original
                              Consutas para el Servicio de Autoridad Registradora PKI-SAT
  

#################################################################################*/



//#VERSION: 1.1.0


#define QS_EDO_CERT               "SELECT  EDOCER FROM SEG_PKI.SEGT_CERTIFICADO WHERE NOSERIE = '%s';"
#define QU_FREGIES_CERT           "UPDATE SEG_PKI.SEGT_CERTIFICADO SET FECREGIES = '%s' WHERE NOSERIE = '%s';"
#define QU_EDO_VF_CERT            "UPDATE SEG_PKI.SEGT_CERTIFICADO SET  EDOCER = 'R', VIGFIN ='%s' WHERE NOSERIE = '%s';"
#define QU_EDO_VF_FIES_CERT       "UPDATE SEG_PKI.SEGT_CERTIFICADO SET  EDOCER = 'R', VIGFIN ='%s' , FECREGIES ='%s' WHERE NOSERIE = '%s';"
#define QU_CERTIFICADO    		  "UPDATE SEG_PKI.SEGT_CERTIFICADO SET FECREGIES = '%s', DIGPUBKMD5 = '%s',DIGPUBKSHA1 = '%s',PUBK= '%s',ESSAT = '%s',TRAMITECVE = '%s' WHERE  RFC = '%s' AND  NOSERIE = '%s';"
#define QS_TRAMITE_CVE            "SELECT TRAMITECVE FROM SEG_PKI.SEGT_TRAMITE WHERE SOLOPERCVE = '%s'"
#define QS_SERIE_CERT             "SELECT * FROM (SELECT NOSERIE FROM SEG_PKI.SEGT_CERTIFICADO WHERE RFC= '%s' AND EDOCER ='A' and TIPCERCVE IN ('1', '2') ORDER BY NOSERIE DESC) WHERE ROWNUM = 1"
#define QU_EDO_CERT			      "UPDATE SEG_PKI.SEGT_CERTIFICADO SET EDOCER='R' WHERE NOSERIE = '%s'"
#define QS_EDO_VF_FIES_CERT       "SELECT EDOCER ,VIGFIN , FECREGIES FROM SEG_PKI.SEGT_CERTIFICADO WHERE NOSERIE = '%s';"
#define QS_RFC_TIP_CERTIFICADO    "SELECT RFC, TIPCERCVE from SEG_PKI.SEGT_CERTIFICADO where NOSERIE = '%s'"
			 
#define SP_GEN_NUMOPER		        "SEG_PKI.segsf_gennumoper" 
#define SP_GEN_NUMTRAM              "SEG_PKI.segsf_gennumtram"
#define SP_REG_OPERDET              "SEG_PKI.segsp_regoperdet"









#endif




