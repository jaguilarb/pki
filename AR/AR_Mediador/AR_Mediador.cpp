static const char* _AR_MEDIADOR_CPP_VERSION_ ATR_USED = "AR_Mediador @(#)"\
"DSIC09041AR_ 2007-12-13 AR_Mediador.cpp 1.1.0/1";

//#VERSION: 1.1.0
#include<Sgi_Mediador.h>
#include<sys/types.h>
#include<dirent.h>
#include<stdio.h>
#include<signal.h>
#include<unistd.h>
#include<stdlib.h>

int PID;
Mediador med;

int main()
{
   iniciaLibOpenSSL();
   
   /*>- char pid[6]; //ERGL Fri Feb  1 17:12:54 CST 2008
   FILE *Archivo;
   -<*/
   void manejo_Sen(int sig);
   int error;

   if(signal(SIGINT,manejo_Sen)==SIG_ERR)
   {
      perror("signal");
      exit(-1);
   }

   if (signal(SIGPIPE,manejo_Sen) == SIG_ERR)
   {
      perror("signal");
      exit(-1);
   }

   PID = getpid();
/*>- ERGL Fri Feb  1 17:11:59 CST 2008
   sprintf(pid,"%d",PID);
#ifndef DBG
   Archivo = fopen("/tmp/AR_Mediador.pID","w+");
#else
   Archivo = fopen("/tmp/AR_Mediador_dbg.pID","w+");
#endif
       
   if(Archivo == 0)
       printf("Error al crear el archivo\n");
   else
       fputs(pid,Archivo);
   fclose(Archivo);
-<*/
   error =  med.Inicia();
   if(error)
   {
      printf("Error Inicia %d\n",error);
      if(errno != 2)
         med.Termina();
      terminaLibOpenSSL();
      return -1;
   }
   
   med.Servicio();

}
void manejo_Sen(int sig)
{
  med.bit->escribe(BIT_INFO,"*************************************************************************************************");
  med.bit->escribe(BIT_INFO,"***************************** FINALIZANDO MEDIADOR **********************************************");
  med.bit->escribePV(BIT_INFO,"*****************************        SE�AL  %d     **********************************************",sig);
  
  if(sig == SIGPIPE)
     med.conectAC = false; 
  med.Termina(); 
  terminaLibOpenSSL();

  med.bit->escribePV(BIT_INFO,"*********************** AR_Mediador FINALIZADO  %d  **********************************************",PID);
  med.bit->escribe(BIT_INFO,"*************************************************************************************************");
  exit(0);
}

