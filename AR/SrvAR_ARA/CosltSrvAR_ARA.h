#ifndef _COSLTSRVAR_ARA_H_
#define _COSLTSRVAR_ARA_H_

static const char* _COSLTREVAR_H_VERSION_ ATR_USED = "@(#) SrvAC ( L : DSIC10392AR_ : ConsltSrvAR_ARA.h : 1.0.0 : 10/10/14)";

//#VERSION: 1.1.0
/*##############################################################################
  ###  PROYECTO:              PKI-SAT                                        ###
  ###  MODULO:                Servicio AR                                    ###
  ###                                                                        ###
  ###                                                                        ###
  ###  DESARROLLADORES:       Guadalupe Negrete Castro         GNC           ###
  ###                                                                        ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       Viernes 10, octubre del 2014                   ###
  ###                                                                        ###
  ##############################################################################
         1         2         3         4         5         6         7         8
12345678901234567890123456789012345678901234567890123456789012345678901234567890

################################################################################
  VERSION:
     V.1.0.0  (20141010- )    GNC: Version Original
                              Consutas para el Servicio de Autoridad Registradora PKI-SAT
  

#################################################################################*/



//#VERSION: 1.1.0


#define QS_TIPO_EDO_RFC_CURP_VIVF_CERT      "SELECT tipcer_cve, edo_cer, rfc, curp, vig_ini, vig_fin FROM certificado WHERE no_serie = '%s'"          
#define QS_NOSERIE_CERTIFICADO              "SELECT FIRST %d no_serie FROM certificado WHERE rfc = '%s' AND tipcer_cve = %d ORDER BY vig_ini DESC"
#define QS_NOSERIE_CERT_RFC_TIPO_EDO        "SELECT FIRST %d no_serie FROM certificado WHERE rfc = '%s' AND tipcer_cve = %d AND edo_cer = '%s' ORDER BY vig_ini DESC"
#define QS_NOSERIE_CERT_RFC_TIPCER          "SELECT FIRST %d no_serie FROM certificado WHERE rfc = '%s' AND tipcer_cve IN (1, 2) ORDER BY vig_ini DESC"
#define QS_NOSERIE_CERT_TIPO1Y2             "SELECT FIRST %d no_serie FROM certificado WHERE rfc = '%s' AND tipcer_cve IN (1, 2) AND edo_cer = '%s' ORDER BY vig_ini DESC"
#define QS_EDO_CERTIFICADO                  "SELECT COUNT(*), edo_cer FROM certificado WHERE no_serie = '%s' GROUP BY edo_cer;"
#define QU_EDO_CERTIFICADO  			    "UPDATE certificado set edo_cer='%s' where no_serie ='%s'"
#define QI_CERTIFICADO_C                    "INSERT INTO certificado (no_serie, tipcer_cve, edo_cer, rfc, curp,nombre,correo,rfc_rl,curp_rl, vig_ini, vig_fin, fec_reg_ies, dig_pubk_md5, dig_pubk_sha1, pubk, es_sat, tramite_cve) VALUES ('%s', %s, '%s', '%s', '%s', '%s','%s', '%s', '%s', '%s', '%s',NULL, '%s', '%s', '%s','N', NULL)"
#define QS_EDO_VF_CERTIFICADO               "SELECT edo_cer, vig_fin FROM certificado WHERE no_serie = '%s'"
#define QU_EDO_VF_CERTIFICADO	            "UPDATE certificado SET edo_cer = '%s', vig_fin = '%s' WHERE no_serie = '%s'"
#define QI_CERTIFICADO_M                    "INSERT INTO certificado (no_serie, tipcer_cve, edo_cer, rfc, curp, vig_ini, vig_fin) VALUES ('%s', %s, '%s', '%s', '%s', '%s', '%s')"
#define QS_TNOSERIE_CERT_RFC_TIPO_EDO		"SELECT no_serie FROM certificado WHERE rfc = '%s' AND tipcer_cve = %d AND edo_cer = '%s' ORDER BY vig_ini DESC"
#define QS_TNOSERIE_CERTIFICADO             "SELECT no_serie FROM certificado WHERE rfc = '%s' AND tipcer_cve = %d ORDER BY vig_ini DESC"
#define QS_TNOSERIE_CERT_TIPO1Y2	        "SELECT  no_serie FROM certificado WHERE rfc = '%s' AND tipcer_cve IN (1, 2) AND edo_cer = '%s' ORDER BY vig_ini DESC"
#define QS_TNOSERIE_CERT_RFC_TIPCER   		"SELECT no_serie FROM certificado WHERE rfc = '%s' AND tipcer_cve IN (1, 2) ORDER BY vig_ini DESC"





#endif




