#ifndef _SRVAR_ARA_H_
#define _SRVAR_ARA_H_
static const char* _SRVAR_ARA_H_VERSION_ ATR_USED = "@(#) SrvAR_ARA (L : DSIC10155AR_ : SrvAR_ARA.h : 1.1.1 : 1 : 10/04/16 )";

//#VERSION: 1.1.1
//###############################################################################
#include <string>

#include <SgiTipos.h>
#include <Sgi_SrvUnx.h>
#include <Sgi_MsgPKI.h>      
#include <Sgi_ConfigFile.h> 
#include <Sgi_BD.h>
#include <Sgi_OCSP.h>
#include <Map.h>
#include <ModRecuperacion.h>
#include <ExcepcionOCSP.h>
#include <dlfcn.h>
#include <Sgi_Digest.h>
#include <Sgi_Fecha.h>

//###############################################################################
// MACROS

#define PATH_CONF            "/usr/local/SAT/PKI/etc/"
#define PATH_BITA            "/var/local/SAT/PKI/"

#ifdef DBG
   #define ARCH_CONF            PATH_CONF "SrvAR_ARA_dbg.cfg"
   #define ARCH_BITA            PATH_BITA "SrvAR_ARA_dbg.log"
#else
   #define ARCH_CONF            PATH_CONF "SrvAR_ARA.cfg"
   #define ARCH_BITA            PATH_BITA "SrvAR_ARA.log"
#endif

//###############################################################################
class CSrvAR_ARA : public CSrvUnx
{
   public:

      typedef enum ARA_Error
      {
//>         ARAE_ComunicacionAR  =    1,
         ARAE_OpeNoVal        =    2,
         ARAE_NumSer          =   10,
//>         ARAE_CerNoDisp       =   11,
         ARAE_RFCNoCer        =   12,
         ARAE_RFC             =   13,
         ARAE_MsgMalForCli    =  120,
         ARAE_MsgMalForMed    =  121,
         ARAE_InfoNoDisp      =  122,

         ARAE_Default         = 9999
      };


      CSrvAR_ARA();
      ~CSrvAR_ARA();

   protected:
      uint8         AraID;
      MensajesARA   Msg;
      CConfigFile   Configuracion;
      CBD          *BDAR;
      CBD          *BDARA;
      std::string   ArRepPath;
      std::string   AraRepHost;
      std::string   AraRepUsr;
      std::string   AraRepPath;
    
      void         *cargaLibreria;
      
      map<char*,ModRecuperacion*> modulos; 
      map<char*,entidad*> entidades;
      entidad *ent;

      bool cargaValores( char *num_serie);
      bool obtieneCert(char* num_Serie);
      bool cargaModulo(char* modulo);
      bool cargaModulos();
      ModRecuperacion *obtModulo( char* modulo);
      bool obtieneDatosCert( CSgi_Certificado *cert);
      bool regCertAR(CSgi_Certificado* cert, uchar* cEdo); 
      bool digestion(SgiCripto::tAlgDig iAlgDig, uchar *cDato, int iDato , char *cDig, uint16 *iDig); 
      int  ObtieneLlave( RSA * rLlave ,char *cLlavePub , int *iLlavePub );
      bool guardaCert(char *sNumSerie ,CSgi_Certificado *cert);

      bool InicioDeSesion();
      bool IniBD(bool bdAR);
      bool IniRep();
      bool RegBDARA(const char* no_serie);
      bool RegRepARA(const char* no_serie);
      bool envRespuesta();
      bool envOperError(BIT_NIVEL nivel, const char* texto, ARA_Error error);
      bool ArmaLista(std::string &lista, const char* rfc, char tipo, char edo, int cuantos);
      bool Desencripta(const std::string &enc, std::string &desenc);
      
      bool SolCert_v1();
      bool SolCert();
      bool SolListaCerts();
      bool KeepAlive();

      virtual bool Proceso();
      virtual bool Inicia();
      virtual bool SetParmsSktProc(CSSL_parms& parms);
      
      virtual bool LeeCfg();
      virtual const char* getMsgError(ARA_Error error);
      virtual bool Operaciones();
};
//###############################################################################
#endif //_SRVAR_ARA_H_
