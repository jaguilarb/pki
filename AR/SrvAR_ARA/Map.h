#ifndef MAP_H
#define MAP_H

#include <Sgi_ConfigFile.h>
#include <Sgi_OCSP.h>
#include <iostream>
#include <map>


static const char* _MAP_H_VERSION_ ATR_USED = "@(#) SrvAR_ARA (L : DSIC10155AR_ : Map.h : 1.1.1 : 0 : 10/04/16 )";

using namespace std;
using namespace SgiOCSP;




typedef  struct
{
  char cNumAR_AC[100];
  char cURL[100];
  char cPuerto[20];
  char cRuta[100];
  char cCertDelegado[100];
  char cCadenaCert[200];
  char cEmisor[100];
  char cModulo[100];
  char cArgModulo[100];
  char cNombreEnt[100];
  SolicitanteOCSP *solOCSP;

}entidad;

map<char*,entidad*> cargaDatosConfig(char* archcert);
entidad *deDonde(map<char*,entidad*> entidades , char* numSerie);

#endif
