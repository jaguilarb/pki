static const char* _SRVAR_ARA_CPP_VERSION_ ATR_USED = "@(#) SrvAR_ARA (L : DSIC10155AR_ : SrvAR_ARA.cpp : 1.1.1 : 1 : 10/04/16 )";


//#VERSION: 1.1.0
/**********************************************************************************/
const char *SRV_AR_ARA_VERSION = "V.2.0.0";
/**********************************************************************************/
#include <assert.h>

#include <SrvAR_ARA.h>
#include <Sgi_ProtegePwd.h>
#include <Sgi_PKI.h>
#include <SockSeg.h>

#if ORACLE
   #include <CosltSrvAR_ARAORA.h>//GNC Se agregan definiciones de consultas
#else
   #include <CosltSrvAR_ARA.h>//GNC Se agregan definiciones de consultas
#endif

//######################################################################################################################
#define MSG_CERNODIS  "Certificado no disponible"
//######################################################################################################################
CSrvAR_ARA::CSrvAR_ARA() :
   AraID(0),
   Configuracion(ARCH_CONF),
   BDAR(NULL),
   BDARA(NULL),
   cargaLibreria(NULL)
{
}
//######################################################################################################################
CSrvAR_ARA::~CSrvAR_ARA()
{
   if (BDAR)
      delete BDAR;
   if (BDARA)
      delete BDARA;
   if (cargaLibreria)
      dlclose(cargaLibreria);  
}
//######################################################################################################################
bool CSrvAR_ARA::SetParmsSktProc(CSSL_parms& parms)
{
   parms.SetServicio(EPlano, NULL);
   return true;
}

//######################################################################################################################
bool CSrvAR_ARA::LeeCfg()
{
   int error = Configuracion.cargaCfgVars();
   if (error)
      Bitacora->escribePV(BIT_ERROR, "Error en CSrvAR_ARA en LeeCfg: Problemas al leer el archivo de configuraci�n (%d): %s", error, ARCH_CONF);

   return !error;
}
//######################################################################################################################
bool CSrvAR_ARA::Desencripta(const std::string &enc, std::string &desenc)
{
   uint8 pwd[512];
   int  lpwd = sizeof(pwd);

   memset(pwd, 0, lpwd);
   if (!desencripta((const uint8*) enc.c_str(), enc.size(), pwd , &lpwd))
      return false;

   pwd[lpwd] = 0;
   desenc = (const char*) pwd;
   return true;
}

//######################################################################################################################
bool CSrvAR_ARA::InicioDeSesion()
{
   std::string arCert, arPvK, arPwd;
   std::string arNvlBit;
   int         r1, r2, r3, r4; 
   int         r5; //+ V.02.00.01 GHM (071022) 

   r1 = Configuracion.getValorVar("[SRV]", "certificado", &arCert);
   r2 = Configuracion.getValorVar("[SRV]", "llave_priv" , &arPvK);
   r3 = Configuracion.getValorVar("[SRV]", "pwd_priv"   , &arPwd);
   r4 = Configuracion.getValorVar("[SRV]", "repositorio", &ArRepPath);
   r5 = Configuracion.getValorVar("[BITACORA]", "nivel_Bit", &arNvlBit); //+ V.02.00.01 GHM (071022)
   if (r1 || r2 || r3 || r4 /*+ V.02.00.01*/ || r5)
   {
      Bitacora->escribePV(BIT_ERROR, "Error en CSrvAR_ARA en InicioDeSesion: Error en los par�metros de configuraci�n SRV (%d,%d,%d,%d,%d)",
            r1, r2, r3, r4 /*+ V.02.00.01*/, r5);
      return false;
   }
   //>>> + V.02.00.01 GHM (071022)
   Bitacora->escribePV(BIT_DEBUG, "CSrvAR_ARA.InicioDeSesion: De archivo de configuraci�n se obtuvo: ARCert(%s), ARPvK (%s), RutaRep(%s) y nivelBit(%s)",
                                  arCert.c_str(), arPvK.c_str(), ArRepPath.c_str(), arNvlBit.c_str());
   BIT_NIVEL iNvlBitac = (BIT_NIVEL) atoi(arNvlBit.c_str()); 
   if (Bitacora)
      Bitacora->setNivel( iNvlBitac);
   //<<< + V.02.00.01 GHM (071022)
   if (!Desencripta(arPwd, arPwd))
   {
      Bitacora->escribe(BIT_ERROR, "Error en CSrvAR_ARA en InicioDeSesion: Error al desencriptar la contrase�a de la llave privada");
      return false;
   }

   intE error = Msg.Mensajes::Inicia(ARA, (char*) arCert.c_str(), (char*) arPvK.c_str(),
                                        (char*) arPwd.c_str(), arPwd.size(), NULL);
   if (error)
      Bitacora->escribe(BIT_ERROR, error , "Error en CSrvAR_ARA en InicioDeSesion: Error al iniciar la librer�a para el intercambio de mensajes con el cliente");
   else
   {
      Msg.setVersion(0);
      error = Msg.ConectaServidor(SktProc);
      if (error)
         Bitacora->escribe(BIT_ERROR, error, "Error en CSrvAR_ARA en InicioDeSesion: Error en protocolo de inicio de conexi�n");
      else
      {
         AraID = Msg.getDestino();
         Bitacora->escribePV(BIT_INFO, "CSrvAR_ARA en InicioDeSesion: Servicio atendiendo ARA: %d", AraID);
      }
   }

   entidades =  cargaDatosConfig(ARCH_CONF);
   //if (entidades != NULL) 
      cargaModulos();

   return !error;
   
}
//######################################################################################################################
bool CSrvAR_ARA::IniBD(bool bdAR)
{
   CBD* &BD = bdAR ? BDAR      : BDARA;

   BD = new CBD(Bitacora);

   char seccion[20];

   if (bdAR)
   {
      #if ORACLE
         strcpy(seccion, "[BD-AR-ORACLE]");
      #else
         strcpy(seccion, "[BD-AR]");
      #endif
   }
   else
   {
      #if ORACLE
         sprintf(seccion, "[BD-ARA-ORACLE-%d]", AraID);
      #else
         sprintf(seccion, "[BD-ARA-%d]", AraID);
      #endif
   }

   if (!BD)
   {
      Bitacora->escribePV(BIT_ERROR, "Error en CSrvAR_ARA en IniBD: Error al crear el objeto de la base de datos %s", seccion);

      return false;
   }

   std::string bdInstancia, bdNombre, bdRol; // Par�metros de Informix
   std::string bdHost, bdPuerto, bdSid;      // Par�metros de Oracle
   std::string bdUsr, bdPwd;                 // Par�metros de Oracle e Informix

   int r1, r2, r3, r4, r5;

   #if ORACLE
      r1 = Configuracion.getValorVar(seccion, "host", &bdHost);
      r2 = Configuracion.getValorVar(seccion, "port", &bdPuerto);
      r3 = Configuracion.getValorVar(seccion, "sid" , &bdSid);
      r4 = Configuracion.getValorVar(seccion, "usr" , &bdUsr);
      r5 = Configuracion.getValorVar(seccion, "pwd" , &bdPwd);
   #else
      r1 = Configuracion.getValorVar(seccion, "instancia", &bdInstancia);
      r2 = Configuracion.getValorVar(seccion, "nombre"   , &bdNombre);
      r3 = Configuracion.getValorVar(seccion, "usr"      , &bdUsr);
      r4 = Configuracion.getValorVar(seccion, "pwd"      , &bdPwd);
      r5 = Configuracion.getValorVar(seccion, "rol"      , &bdRol);
   #endif

   if (r1 || r2 || r3 || r4 || r5)
   {
      Bitacora->escribePV(BIT_ERROR, "Error en CSrvAR_ARA en IniBD: Error en los par�metros de configuraci�n %s (%d,%d,%d,%d,%d)", seccion, r1, r2, r3, r4, r5);
      
      return false;
   }

   if (!Desencripta(bdPwd, bdPwd))
   {
      Bitacora->escribePV(BIT_ERROR, "Error en CSrvAR_ARA en IniBD: Error al desencriptar la contrase�a de la base de datos %s", seccion);

      return false;
   }

   bool ok_configura = false;

   #if ORACLE
      //>>> + JAB (20141017)
      Bitacora->escribePV(BIT_DEBUG, "CSrvAR_ARA.IniBD: De archivo de configuraci�n se obtuvo: host(%s), puerto(%s), sid(%s) y usuario(%s)", bdHost.c_str(), 
      bdPuerto.c_str(), bdSid.c_str(), bdUsr.c_str());
      //<<< + JAB (20141027)

      ok_configura = BD->setConfig(bdUsr.c_str(), bdPwd.c_str(), bdHost.c_str(), bdPuerto.c_str(), bdSid.c_str());
   #else
      //>>> + V.02.00.01 GHM (071022)
      Bitacora->escribePV(BIT_DEBUG, "CSrvAR_ARA.IniBD: De archivo de configuraci�n se obtuvo: Instancia(%s), NombreBD(%s), UsrBD(%s) y rol(%s)", bdInstancia.c_str(), bdNombre.c_str(), bdUsr.c_str(), bdRol.c_str());
      //<<< + V.02.00.01 GHM (071022)

      ok_configura = BD->setConfig(bdInstancia.c_str(), bdNombre.c_str(), bdUsr.c_str(), bdPwd.c_str(), bdRol.c_str());
   #endif

   if (!ok_configura)
   {
      Bitacora->escribePV(BIT_ERROR, "Error en CSrvAR_ARA en IniBD: No se han podido configurar los par�metros de conexi�n para la base de datos %s", seccion);

      return false;
   }

   return true;
}
//######################################################################################################################
bool CSrvAR_ARA::IniRep()
{
   char seccion[15];
   sprintf(seccion, "[REP-ARA-%d]", AraID);

   int    r1, r2, r3;

   r1 = Configuracion.getValorVar(seccion, "host"       , &AraRepHost);
   r2 = Configuracion.getValorVar(seccion, "usr"        , &AraRepUsr);
   r3 = Configuracion.getValorVar(seccion, "repositorio", &AraRepPath);

   if (r1 || r2 || r3)
   {
      Bitacora->escribePV(BIT_ERROR, "Error en CSrvAR_ARA en IniRep: Error en los par�metros de configuraci�n %s (%d,%d,%d)",
            seccion, r1, r2, r3);
      return false;
   }
   //>>> + V.02.00.01 GHM (071022)
   Bitacora->escribePV(BIT_DEBUG, "CSrvAR_ARA.IniRep: De archivo de configuraci�n: host(%s), Usr(%s) y repositorio(%s)",
                                  AraRepHost.c_str(), AraRepUsr.c_str(), AraRepUsr.c_str());
   //<<< + V.02.00.01 GHM (071022)
   return true;
}
//######################################################################################################################
bool CSrvAR_ARA::envRespuesta()
{
   intE error = SktProc->Envia(Msg.buffer, Msg.tamMensaje());
   if (error)
      Bitacora->escribe(BIT_ERROR, error, "Error en CSrvAR_ARA en envRespuesta: No se pudo enviar mensaje al cliente");
   else
      Bitacora->escribe(BIT_INFO, "CSrvAR_ARA.envRespuesta: Mensaje enviado al cliente");
   return !error;
}
//######################################################################################################################
const char* CSrvAR_ARA::getMsgError(ARA_Error error)
{
   switch (error)
   {
      case ARAE_OpeNoVal    : return "Error, solicitud de operaci�n inv�lida";
      case ARAE_NumSer      : return "Error, n�mero de serie inv�lido";
      case ARAE_RFCNoCer    : return "El RFC no cuenta con Certificados Digitales con los par�metros solicitados";
      case ARAE_RFC         : return "Error, RFC inv�lido";
      case ARAE_MsgMalForCli: return "Error, solicitud con mensaje mal formado";
      case ARAE_MsgMalForMed: return "Error, respuesta mediador mal formada";
      case ARAE_InfoNoDisp  : return "Error, por el momento la informaci�n no est� disponible";
      case ARAE_Default     : assert(false);
   }
   return "Error interno";
}
//######################################################################################################################
bool CSrvAR_ARA::envOperError(BIT_NIVEL nivel, const char* texto, ARA_Error error)
{
   if (texto)
      Bitacora->escribe(nivel, texto);

   const char* msg = getMsgError(error);
   Bitacora->escribePV(BIT_INFO, "CSrvAR_ARA.envOperError: Enviando error -> %s (ARA %d)", msg, error);

   char strNError[10];
   sprintf(strNError, "%d", error);

   intE emsg = Msg.setMensaje(ERROR_ARA, strNError, strlen(strNError), MSG_CERNODIS, strlen(MSG_CERNODIS));
   if (emsg)
      Bitacora->escribe(BIT_ERROR, emsg, "Error en CSrvAR_ARA en envOperError: No se pudo armar mensaje de error para enviar al cliente");
   else
      return envRespuesta();

   return !emsg;
}
//######################################################################################################################
bool CSrvAR_ARA::RegBDARA(const char* no_serie)
{
   std::string tipo, edo, rfc, curp, vig_ini, vig_fin;

   if (!BDAR->conecta())
   {
      Bitacora->escribe(BIT_ERROR, "Error en CSrvAR_ARA en RegBDARA: No se pudo conectar a la BD de la AR");
      return false;
   }

   if (!BDAR->consultaReg(QS_TIPO_EDO_RFC_CURP_VIVF_CERT, no_serie) ||
       !BDAR->getValores("ssssss", &tipo, &edo, &rfc, &curp, &vig_ini, &vig_fin))
      return false;
   
   bool ok = BDARA->conecta();
   if (ok)
   { 
      //if (!BDARA->consultaReg("SELECT edo_cer, vig_fin FROM certificado WHERE no_serie = '%s'", no_serie)) //>- ERGL (072205)
      if (BDARA->consultaReg(QS_EDO_VF_CERTIFICADO, no_serie))
      {
         std::string edo2, vig_fin2;
         if (BDARA->getValores("ss", &edo2, &vig_fin2))
         {
            if (edo2 != edo || vig_fin2 != vig_fin)
            {
               ok = BDARA->ejecutaOper(QU_EDO_CERTIFICADO,edo.c_str(), vig_fin.c_str(), no_serie);
               if (ok)
                  Bitacora->escribe(BIT_INFO, "CSrvAR_ARA.RegBDARA: Registro del CD actualizado en la BD");
            }
         }
         else
         {
            ok = BDARA->ejecutaOper(QI_CERTIFICADO_M, no_serie, tipo.c_str(),
                                    edo.c_str(), rfc.c_str(), curp.c_str(), vig_ini.c_str(), vig_fin.c_str());
            if (ok)
               Bitacora->escribe(BIT_INFO, "CSrvAR_ARA.RegBDARA: Registro del CD insertado en la BD");
         }
      } 
      BDARA->desconecta();
   }

   return ok;
}
//######################################################################################################################
bool CSrvAR_ARA::RegRepARA(const char* no_serie)
{
   std::string rutaCertLocal, dirCertRemoto;
   intE error = pkiRutaCert(ArRepPath, std::string(no_serie), rutaCertLocal, false);
   Bitacora->escribePV(BIT_DEBUG, "CSrvAR_ARA.RegRepARA: Ruta del CD local: ArRepPath(%s), no_serie(%s)", 
                                   ArRepPath.c_str(), std::string(no_serie).c_str(), rutaCertLocal.c_str() );
   if (error)
      Bitacora->escribe(BIT_ERROR, error, "Error en CSrvAR_ARA en RegRepARA: Error al armar la ruta del CD local");
   else if (access(rutaCertLocal.c_str(), R_OK))
   {
      if (errno == ENOENT)
         Bitacora->escribe(BIT_INFO, "CSrvAR_ARA.RegRepARA: El CD solicitado no existe en el repositorio");
      else
         Bitacora->escribePV(BIT_ERROR, "Error en CSrvAR_ARA en RegRepARA: No se tiene acceso al CD en el repositorio (%d)", errno);
   }
   else
   {
      error = pkiDirCert(AraRepPath, std::string(no_serie), dirCertRemoto, false);
      Bitacora->escribePV(BIT_DEBUG, "CSrvAR_ARA.RegRepARA: Ruta del CD remoto: AraRepPath(%s), no_serie(%s)",
                                     AraRepPath.c_str(), std::string(no_serie).c_str(), dirCertRemoto.c_str() );
      if (error)
         Bitacora->escribe(BIT_ERROR, error, "Error en CSrvAR_ARA en RegRepARA: Error al armar la ruta del CD remoto");
      else
      {
         //>>> HOA (080425): se cambia para revisar que no exista el directorio para intentar crearlo 
         //                  y que se generen los subdirectorios con los permisos correctos
         /* ->
         std::string comando("umask 022; mkdir -p -m 755 ");
         comando += dirCertRemoto;
         <- */
         std::string comando("if test ! -d ");
         comando += dirCertRemoto;
         comando += "; then umask 022; mkdir -p -m 755 ";
         comando += dirCertRemoto;
         comando += "; fi";
         //<<< HOA (080425)
         char cErrorSkt[1024];
         int res = EjecutaCmdoRem((char*) AraRepHost.c_str(), (char*) AraRepUsr.c_str(), 
                                  (char*) comando.c_str(), cErrorSkt);
         if (res)
         {
            if (res != -41)
            {
               Bitacora->escribePV(BIT_ERROR, "Error en CSrvAR_ARA en RegRepARA: No se pudo crear el directorio de deposito en la ARA (%d): %s", 
                                   res, cErrorSkt);
               return false;
            }
            else
               Bitacora->escribePV(BIT_INFO, "CSrvAR_ARA.RegRepARA: Al crear el directorio de deposito en la ARA (%d): %s", res, cErrorSkt);
         }
         dirCertRemoto += no_serie + std::string(".cer");
         Bitacora->escribePV(BIT_DEBUG, "CSrvAR_ARA.RegRepARA: Parms socket: rutaLocal(%s), rutaRem(%s)", rutaCertLocal.c_str(), 
                                       dirCertRemoto.c_str());
         res = TransmiteArchivo((char*) AraRepHost.c_str(), (char*) AraRepUsr.c_str(), (char*) rutaCertLocal.c_str(), 
                                (char*) dirCertRemoto.c_str(), cErrorSkt);
         if (res)
         {
               Bitacora->escribePV(BIT_ERROR, "Error en CSrvAR_ARA en RegRepARA: No se pudo depositar el CD en el repositorio de la ARA (%d): %s", 
                                   res, cErrorSkt);
               return false;
         }
         Bitacora->escribe(BIT_INFO, "CSrvAR_ARA.RegRepARA: CD depositado en el repositorio de la ARA");
         //>>> HOA (080425): Se agrega para cambiar los permisos del archivo de certificado de 660 a 644
         comando = "chmod 644 " + dirCertRemoto;
         res = EjecutaCmdoRem((char*) AraRepHost.c_str(), (char*) AraRepUsr.c_str(),
                              (char*) comando.c_str(), cErrorSkt);
         if (res)
            Bitacora->escribePV(BIT_ERROR, "Error en CSrvAR_ARA en RegRepARA: No se pudo cambiar los permisos al certificado en la ARA (%d): %s",
                                res, cErrorSkt); 
         //<<< HOA (080425)
         return true;        
      }
   }

   return false;
}
//######################################################################################################################
bool CSrvAR_ARA::ArmaLista(std::string &lista, const char* rfc, char tipo, char edo, int cuantos)
{
   lista = "";
   if (tipo < '0' || tipo > '2') 
   {
      Bitacora->escribe(BIT_ERROR, "Error en CSrvAR_ARA en ArmaLista: El tipo de CD solicitado es inv�lido");
      return false;
   }

   if (!BDAR->conecta())
   {
      Bitacora->escribe(BIT_ERROR, "Error en CSrvAR_ARA en ArmaLista: No se pudo conectar a la BD de la AR");
      return false;
   }

   std::string query("");
   
   bool ok=false;
   
   
  if (cuantos)
   {
     if (tipo != '0')
     {
	    if (edo != 'X')
       {
         #if ORACLE
            ok = BDAR->consulta(QS_NOSERIE_CERT_RFC_TIPO_EDO,rfc,tipo,edo,cuantos);
         #else
	         ok = BDAR->consulta(QS_NOSERIE_CERT_RFC_TIPO_EDO,cuantos,rfc,tipo,edo);
         #endif
       }
	    else
       {
         #if ORACLE
            ok = BDAR->consulta(QS_NOSERIE_CERTIFICADO,rfc,tipo,cuantos);
         #else
	         ok = BDAR->consulta(QS_NOSERIE_CERTIFICADO,cuantos,rfc,tipo);
         #endif
       }
     }
	  else
     {
       if (edo != 'X')
       {
         #if ORACLE
            ok = BDAR->consulta(QS_NOSERIE_CERT_TIPO1Y2,rfc,edo,cuantos);
         #else
	         ok = BDAR->consulta(QS_NOSERIE_CERT_TIPO1Y2,cuantos,rfc,edo);
         #endif
		 }
		 else
       {
         #if ORACLE
            ok = BDAR->consulta(QS_NOSERIE_CERT_RFC_TIPCER,rfc,cuantos);
         #else
            ok = BDAR->consulta(QS_NOSERIE_CERT_RFC_TIPCER,cuantos,rfc);
         #endif
       }
     }
   }
   else
   {
     if (tipo != '0')
     {
	   if (edo != 'X')
      {
         ok = BDAR->consulta(QS_TNOSERIE_CERT_RFC_TIPO_EDO,rfc,tipo,edo);
      }
	   else
      {
         ok = BDAR->consulta(QS_TNOSERIE_CERTIFICADO,rfc,tipo);
      }
     }
	  else
     {
       if (edo != 'X')
       {
         ok = BDAR->consulta(QS_TNOSERIE_CERT_TIPO1Y2,rfc,edo);
       }
		 else
       {
         ok = BDAR->consulta(QS_TNOSERIE_CERT_RFC_TIPCER,rfc);
       }
     }
   }


  // bool ok = BDAR->consulta(query.c_str());
   if (ok)
   {
      std::string numSerie;
      int i = 0;
      while (ok && BDAR->sigReg())
      {
         if (i)
            lista += "|";
         ok = BDAR->getValores("s", &numSerie);
         if (!ok)
            Bitacora->escribe(BIT_ERROR, "Error en CSrvAR_ARA en ArmaLista: Error al obtener el n�mero de serie para armar lista de CDs");
         else
            lista += numSerie;
         i++;
      }
   }

   return ok;
}
//######################################################################################################################
bool CSrvAR_ARA::Inicia()
{
   return LeeCfg();
}
//######################################################################################################################
bool CSrvAR_ARA::Proceso()
{
   bool ok;

   #ifdef DBG
   //sleep(20);
   #endif

   ok = InicioDeSesion() &&
        IniBD(true)  &&
        IniBD(false) &&
        IniRep()     && 
        Operaciones();

   return ok;
}
//######################################################################################################################
//######################################################################################################################
bool CSrvAR_ARA::Operaciones()
{
   intE error;

   for (;;)
   {
      /*>- ERGL mi� sep 12 16:22:00 CDT 2007
      int lBuf = sizeof(Msg.buffer);
      error = SktProc->Recibe(Msg.buffer, lBuf);
      */
      error = Msg.Recibe(SktProc);
      if (!error)
      {
         int oper = Msg.tipOperacion();
         switch (oper)
         {
            case SOLCERTARA        : SolCert_v1   (); break;
            case SOLCERTMEDARA     : SolCert      (); break;
            case SOLLISTACERTRFC   : SolListaCerts(); break;
            case PKI_KEEPALIVE     : KeepAlive    (); break;
            case SOLDESCONEXIONARA : return true;
            default                : envOperError(BIT_ALERTA, NULL, ARAE_OpeNoVal);
         }
      }
      else
      {
         if (sgiErrorBase(error) != ERR_SKTTIMEOUT)
         {
            Bitacora->escribe(BIT_ERROR, error , "Error en CSrvAR_ARA en Operaciones: Error al recibir solicitud de operaci�n");
            break;
         }
         struct timespec ts = { 0, 1000000};
         nanosleep(&ts, NULL);
      }
   }

   return !error;
}
//######################################################################################################################
//### OPERACIONES
//######################################################################################################################
bool CSrvAR_ARA::SolCert_v1()
{
   Bitacora->escribe(BIT_INFO, "CSrvAR_ARA.SolCert_v1: ---> Inicio solCD_v1");

   bool ok = false;
   char numSerie[30];
   int  lnumSerie = sizeof(numSerie);

   intE error = Msg.getMensaje(SOLCERTARA, numSerie, &lnumSerie);
   if (error)
   {
      Bitacora->escribe(BIT_ERROR, error, "Error en CSrvAR_ARA en SolCert_v1: solCert_v1, error al decodificar el mensaje del cliente");
      envOperError(BIT_INFO, NULL, ARAE_MsgMalForCli);
   }
   else
   {
      Bitacora->escribePV(BIT_INFO, "CSrvAR_ARA.SolCert_v1: Solicitud de actualizaci�n CD: %s", numSerie);
      if ( !obtieneCert(numSerie) )
         envOperError(BIT_INFO, NULL, ARAE_InfoNoDisp);         
      else
      {
         ok = RegBDARA(numSerie) && RegRepARA(numSerie);
         if (ok)
         {
            error = Msg.setMensaje(CERTREGARA, numSerie, lnumSerie);
            if (error)
            {  
               Bitacora->escribe(BIT_ERROR, error, "Error en CSrvAR_ARA en SolCert_v1: Error al armar mensaje de respuesta");
               ok = false;
            }
            else
               ok = envRespuesta();
         }
         else
            envOperError(BIT_INFO, NULL, ARAE_InfoNoDisp);
      }
   }

   Bitacora->escribe(BIT_INFO, "CSrvAR_ARA.SolCert_v1: <--- Fin    solCD_v1");
   return ok;
}
//######################################################################################################################
bool CSrvAR_ARA::SolCert()
{
   Bitacora->escribe(BIT_INFO, "CSrvAR_ARA.SolCert: ---> Inicio solCD");

   bool ok = false;
   char numSerie[30], opcion[2];
   int  lnumSerie = sizeof(numSerie), lopcion = sizeof(opcion);

   intE error = Msg.getMensaje(SOLCERTMEDARA, numSerie, &lnumSerie, opcion, &lopcion);
   if (error)
   {
      Bitacora->escribe(BIT_ERROR, error, "Error en CSrvAR_ARA en SolCert: solCert, error al decodificar el mensaje del cliente");
      envOperError(BIT_INFO, NULL, ARAE_MsgMalForCli);
   }
   else
   {
      Bitacora->escribePV(BIT_INFO, "CSrvAR_ARA en SolCert: Solicitud de actualizaci�n CD: %s (opcion %s)", numSerie, opcion);

      ok = true;
      if ( !obtieneCert(numSerie) )
         envOperError(BIT_INFO, NULL, ARAE_InfoNoDisp);   
      else
      {
         if (opcion[0] == '1' || opcion[0] == '3') // repositorio
            ok = RegRepARA(numSerie);
      
         if (ok && (opcion[0] == '2' || opcion[0] == '3')) // BD
            ok = RegBDARA(numSerie);

         if (ok)
         {
            error = Msg.setMensaje(CERTREGARA, numSerie, lnumSerie);
            if (error)
            {
               Bitacora->escribe(BIT_ERROR, error, "Error en CSrvAR_ARA en SolCert: Error al armar mensaje de respuesta");
               ok = false;
            }
            else
               ok = envRespuesta();
         }
         else
            envOperError(BIT_INFO, NULL, ARAE_InfoNoDisp);
      }
   }

   Bitacora->escribe(BIT_INFO, "CSrvAR_ARA.SolCert: <--- Fin    solCD");
   return ok;
}
//######################################################################################################################
bool CSrvAR_ARA::SolListaCerts()
{
   Bitacora->escribe(BIT_INFO, "---> Inicio solListaCerts");

   bool ok = false;
   char rfc[14], tipCert[2], edo[2], ctos[15];
   int  lrfc = sizeof(rfc), ltipCer = sizeof(tipCert), 
        ledo = sizeof(edo), lctos   = sizeof(ctos);

   intE error = Msg.getMensaje(SOLLISTACERTRFC, rfc, &lrfc, tipCert, &ltipCer, edo, &ledo, ctos, &lctos);
   if (error)
   {
      Bitacora->escribe(BIT_ERROR, error, "Error en CSrvAR_ARA en SolListaCerts: solListaCerts, error al decodificar el mensaje del cliente");
      envOperError(BIT_INFO, NULL, ARAE_MsgMalForCli);
   }
   else
   {
      Bitacora->escribePV(BIT_INFO, "CSrvAR_ARA.SolListaCerts: Solicitud de lista de CDs: rfc(%s), tipo(%s), edo(%s), cuantos(%s)", 
         rfc, tipCert, edo, ctos);
      
      std::string lista;

      ok = ArmaLista(lista, rfc, tipCert[0], edo[0], atoi(ctos));

      if (!ok)
         envOperError(BIT_INFO, NULL, ARAE_InfoNoDisp);
      else if (!lista.length())
         envOperError(BIT_INFO, "CSrvAR_ARA.SolListaCerts: RFC sin CDs con los datos proporcionados.", ARAE_RFCNoCer);
      else 
      {
         error = Msg.setMensaje(LISTACERT, lista.c_str(), lista.length());
         if (error)
         {
            Bitacora->escribe(BIT_ERROR, error, "Error en CSrvAR_ARA en SolListaCerts: Error al armar el mensaje de respuesta con la lista");
            envOperError(BIT_ERROR, NULL, ARAE_InfoNoDisp);
         }
         else
            ok = envRespuesta();
      }
   }
 
   Bitacora->escribe(BIT_INFO, "CSrvAR_ARA.SolListaCerts: <--- Fin    solListaCerts");
   return ok;
}
//######################################################################################################################
bool CSrvAR_ARA::KeepAlive()
{
   Bitacora->escribe(BIT_INFO, "CSrvAR_ARA.KeepAlive: ---> KeepAlive");
   bool ok = envRespuesta();
   Bitacora->escribe(BIT_INFO, "CSrvAR_ARA.KeepAlive: <--- KeepAlive");
   return ok;
}
//######################################################################################################################

bool CSrvAR_ARA::
cargaValores( char *num_serie)
{
   ent = deDonde(entidades , num_serie);
   if(ent!= NULL)
      return true;
   return false;
}

//######################################################################################################################


bool CSrvAR_ARA::
cargaModulos()
{
   map<char*,entidad*>::iterator it;
   entidad *ent = NULL;

   for ( it = entidades.begin() ; it != entidades.end(); it++ )
   {
      ent = (*it).second;
      cargaModulo(ent->cModulo);

   }

   return true;
}


bool CSrvAR_ARA::
cargaModulo( char* nombreModulo)
{

   if( (strcmp(nombreModulo,"") == 0) || (nombreModulo == NULL))
     return false;
   
   if ( obtModulo( nombreModulo) != NULL)
      return true; 

   ModRecuperacion *(*fnc)();

   char *nombre = (char *)malloc( sizeof(char) * strlen(nombreModulo)+4 );
   sprintf( nombre,"%s.so",nombreModulo);

   cargaLibreria = dlopen(nombre, RTLD_LAZY | RTLD_LOCAL );
   if(cargaLibreria )
   {
      fnc = (ModRecuperacion *(*)())dlsym(cargaLibreria, "getModulo");
      if( fnc )
      {
          ModRecuperacion *modulo = (fnc)();
          modulos.insert(pair<char*,ModRecuperacion*>( nombreModulo, modulo));
          return true;
      }
      else
         Bitacora->escribePV(BIT_ERROR, "Error en CSrvAR_ARA en cargaModulo: No se cargo el modulo de recuperacion de certificados [ %s ]",dlerror());
   }
   else
     Bitacora->escribePV(BIT_ERROR, "Error en CSrvAR_ARA en cargaModulo: No se obtuvo la libreria del modulo.  [ %s ]",dlerror());
  // liberar nombre y ptr 

   return false;

}

ModRecuperacion* CSrvAR_ARA::
obtModulo( char* modulo)
{
    map<char*,ModRecuperacion* >::iterator it;
    it =  modulos.find(modulo);
    return it->second;

}


bool CSrvAR_ARA::
obtieneCert(char* num_Serie)
{
   SolicitanteOCSP *solOCSP = NULL;

   if( cargaValores( num_Serie) ) 
   {
       if(strcmp(ent->cNombreEnt,"SAT") == 0)
          return true;
       else
       {
          if( solOCSP == NULL)
             solOCSP = new SolicitanteOCSP(ent->cURL, ent->cPuerto, ent->cRuta, ent->cCertDelegado, ent->cCadenaCert);
          
          //if( ent->solOCSP == NULL )
          ent->solOCSP = solOCSP;
    
          CSgi_IO *c_emisor = new CSgi_IO();
          c_emisor->inicia( SgiCripto::IO_in, ent->cEmisor );           

          try
          {
             st_respuesta resp;
             memset(&resp,0,sizeof(st_respuesta));

             bool firma = solOCSP->consultaEstado( num_Serie, c_emisor, resp);
             if( firma )
             {
                if ( strcmp((const char*)resp.estado, "D") != 0)
                {
                  ModRecuperacion *moduloRec = obtModulo(ent->cModulo);
                  if(moduloRec!= NULL)
                  {
                     moduloRec->setDatos(resp.data);
                     CSgi_Certificado *certificado = moduloRec->getCertificado(num_Serie);
                     if( !certificado )
                        Bitacora->escribePV(BIT_ERROR, "Error en CSrvAR_ARA en obtieneCert: El certificado con ns %s tiene estado (%s) pero no se pudo obtener de la respuesta OCSP.",
                                                     num_Serie,resp.estado );
                     else
                     {
                        Bitacora->escribePV(BIT_INFO,"CSrvAR_ARA.obtieneCert: Se obtuvo el certificado con Numero de serie %s y Estado %s de %s",
                                                         num_Serie,resp.estado,ent->cNombreEnt); 
                        return regCertAR(certificado, resp.estado);
                     }
                  }
                  else  Bitacora->escribePV(BIT_ERROR,"Error en CSrvAR_ARA en obtieneCert: No se pudo obtener el modulo [ %s ].",ent->cModulo);
                }
                else Bitacora->escribePV(BIT_INFO,"CSrvAR_ARA.obtieneCert: El OCSP  de %s no reconoce Numero de serie %s.",ent->cNombreEnt,num_Serie);
            }else Bitacora->escribePV(BIT_ERROR,"Error en CSrvAR_ARA en obtieneCert: Firma de respuesta OCSP %s  no verificada.",ent->cNombreEnt); 
          }catch( ExcepcionOCSP execp )
          {
              Bitacora->escribePV(BIT_ERROR,  "Error en CSrvAR_ARA en obtieneCert: %s",execp.descripcion()); 
          }

       }
   }
   else
      Bitacora->escribePV( BIT_ERROR, "Error en CSrvAR_ARA en obtieneCert: No se encontro una configuracion para la entidad con numero de serie %s", num_Serie );
   return false;
}


bool CSrvAR_ARA::
regCertAR(CSgi_Certificado *cert, uchar* cEdo)
{

    std::string sNumSerie,sEdoCer,sRFC,sCURP, sNombre, sCorreo,sRFC_rl,sCURP_rl,sVigIni,sVigFin;
    EVP_PKEY *llavePublica = NULL;
    uchar moduloPub[2048]; 

    char cLlavePubB64[2048];
    char digMD5B64[300];
    char digSHA1B64[300];
    int iLlavePubB64   = sizeof(cLlavePubB64);
    uint16 idigMD5B64  = sizeof(digMD5B64);
    uint16 idigSHA1B64 = sizeof(digSHA1B64);
    int imoduloPub = -1;
    int tiene_reg = 0;

    bool  resultado = false;

    CFecha fecha;
    char cVigIni[100];
    char cVigFin[100];
    char cEdoBD[4];
 

    cert->getNumSerie( &sNumSerie );

    if (!BDAR->conecta())
    {
      Bitacora->escribe(BIT_ERROR, "Error en CSrvAR_ARA en regCertAR: No se pudo conectar a la BD de la AR");
      return false;
    }
    char cNumSerie[21];

    strncpy(cNumSerie,sNumSerie.c_str(),20);
    cNumSerie[20] ='\0';


    //if(!BDAR->consultaReg("SELECT edo_cer FROM  certificado where no_serie = '%s'", cNumSerie))
    if(!BDAR->consultaReg(QS_EDO_CERTIFICADO, cNumSerie))
    {
        Bitacora->escribePV(BIT_ERROR, "Error en CSrvAR_ARA en regCertAR: No se pudo recuperar el estado del certificado %s",  cNumSerie);
        return false;
    }
    if(BDAR->getValores("ib", &tiene_reg,cEdoBD))
    {
       resultado = guardaCert(cNumSerie ,cert);
       if( (strcmp((char*)cEdoBD,"A" ) == 0) && (strcmp((char*)cEdo,"A" ) == 0))
          return true;
       if( (strcmp((char*)cEdoBD,"R" ) == 0 )&& (strcmp((char*)cEdo,"R" ) == 0)) 
          return true;
       if( (strcmp((char*)cEdoBD,"A" ) == 0) && (strcmp((char*)cEdo,"R" ) == 0 ))
       {
          resultado = BDAR->ejecutaOper(QU_EDO_CERTIFICADO,cEdo,cNumSerie);
          if(resultado)
             return true;
       }
        return false;   
    }   
    if ( tiene_reg == 0 )
    {
       llavePublica   = cert->getLlavePublica(); 
       rsa_st *stRSA  = llavePublica->pkey.rsa; 


       cert->getTitular( SgiCripto::DN_RFC, &sRFC );
       cert->getTitular( SgiCripto::DN_CURP, &sCURP );
       cert->getTitular( SgiCripto::DN_Nombre, &sNombre );
       cert->getTitular( SgiCripto::DN_Email, &sCorreo );
       cert->getTitular( SgiCripto::DN_Nombre, &sRFC_rl );
       cert->getTitular( SgiCripto::DN_Nombre, &sCURP_rl );
       cert->getVigInicial( &sVigIni );
       cert->getVigFinal( &sVigFin );

       imoduloPub = BN_bn2bin(stRSA->n, moduloPub ); 

       digestion(SgiCripto::ADig_MD5,  moduloPub, imoduloPub, digMD5B64, &idigMD5B64);
       digestion(SgiCripto::ADig_SHA1, moduloPub, imoduloPub, digSHA1B64, &idigSHA1B64);  

       ObtieneLlave(  cert->getLlavePublicaRSA() , cLlavePubB64 , &iLlavePubB64 );

       fecha.ConvCadFtoCertTOBD( sVigIni, cVigIni);
       fecha.ConvCadFtoCertTOBD( sVigFin, cVigFin);
        
       resultado = BDAR->ejecutaOper(QI_CERTIFICADO_C,  cNumSerie, "1",cEdo, sRFC.c_str(), sCURP.c_str(),sNombre.c_str(),sCorreo.c_str(),sRFC_rl.c_str(),sCURP_rl.c_str(),cVigIni, cVigFin,digMD5B64,digSHA1B64, cLlavePubB64);
     
        if (resultado)
           resultado = guardaCert(cNumSerie ,cert);
     }

     BDAR->desconecta();
     return resultado;      

}
 
bool CSrvAR_ARA::
digestion(SgiCripto::tAlgDig iAlgDig, uchar *cDato, int iDato , char *cDig, uint16 *iDig)
{
    uint8 dig[100];
    int idig  = sizeof(dig);
    
    SgiCripto::CSgi_Digest digestiones;

    SgiCripto::tError  res =  digestiones.genDigestIni(iAlgDig);
    res =  digestiones.genDigestProceso(cDato, iDato);
    res =  digestiones.genDigestFin(dig, &idig);

    return B64_Codifica(dig, idig, cDig, iDig);

}

int CSrvAR_ARA ::
ObtieneLlave( RSA * rLlave ,char *cLlavePub , int *iLlavePub )
{

   BIO   *bio = NULL;
   uchar uLlave[1024];
   int   iLlave = sizeof(uLlave);
   int   error;
   uint16 iLlavePubint;

   memset(uLlave, 0, sizeof(uLlave));

   bio = BIO_new(BIO_s_mem());
   if(bio == NULL)
      return -1;

   i2d_RSAPublicKey_bio(bio,rLlave);

   iLlave = BIO_read(bio, uLlave, bio->num_write);
   if(iLlave != (int)bio->num_write)
      return -1;

   error =B64_Codifica((uint8*)uLlave, (uint16) iLlave , cLlavePub,&iLlavePubint);
   if(!error)
      return -1;

   BIO_free_all(bio);
   *iLlavePub = iLlavePubint;
   return 0;

}


bool CSrvAR_ARA::
guardaCert(char *sNumSerie ,CSgi_Certificado *cert)
{
   string sRepos, sRutaCert;
   int resp ;
   
   resp = Configuracion.getValorVar("[SRV]", "repositorio", &sRepos); 
   if(!resp)
   {
      intE error = pkiRutaCert(sRepos, std::string(sNumSerie), sRutaCert, true);  
      if( !error )
      {
         if (access(sRutaCert.c_str(), R_OK))
         {
            if (errno == ENOENT)
            {
               FILE *arch = NULL;
               arch =  fopen(sRutaCert.c_str(),"w+");
               if(arch != NULL)
               {
                  int r = i2d_X509_fp( arch, cert->getX509() );
                  fclose(arch);
                  if( r )
                     return true;
               }
            } 
         }
         else return true;
      }
   }
  return false;
}
