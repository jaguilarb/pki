#ifndef _COSLTSRVAR_ARAORA_H_
#define _COSLTSRVAR_ARAORA_H_

static const char* _COSLTREVAR_ARAORA_H_VERSION_ ATR_USED = "@(#) SrvAC ( L : DSIC10392AR_ : ConsltSrvAR_ARAORA.h : 1.0.0 : 10/10/14)";

//#VERSION: 1.1.0
/*##############################################################################
  ###  PROYECTO:              PKI-SAT                                        ###
  ###  MODULO:                Servicio AR                                    ###
  ###                                                                        ###
  ###                                                                        ###
  ###  DESARROLLADORES:       Guadalupe Negrete Castro         GNC           ###
  ###                                                                        ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       Viernes 10, octubre del 2014                   ###
  ###                                                                        ###
  ##############################################################################
         1         2         3         4         5         6         7         8
12345678901234567890123456789012345678901234567890123456789012345678901234567890

################################################################################
  VERSION:
     V.1.0.0  (20141010- )    GNC: Version Original
                              Consutas para el Servicio de Autoridad Registradora PKI-SAT
  

#################################################################################*/



//#VERSION: 1.1.0



#define QS_TIPO_EDO_RFC_CURP_VIVF_CERT      "SELECT TIPCERCVE, EDOCER, RFC, CURP, VIGINI, VIGFIN FROM SEG_PKI.SEGT_CERTIFICADO WHERE NOSERIE = '%s'"          
#define QS_NOSERIE_CERTIFICADO              "SELECT * FROM (SELECT NOSERIE FROM SEG_PKI.SEGT_CERTIFICADO WHERE RFC = '%s' AND TIPCERCVE = %d ORDER BY VIGINI DESC) WHERE ROWNUM <= %d"
#define QS_NOSERIE_CERT_RFC_TIPO_EDO        "SELECT * FROM (SELECT NOSERIE FROM SEG_PKI.SEGT_CERTIFICADO WHERE RFC = '%s' AND TIPCERCVE = %d AND EDOCER = '%s' ORDER BY VIGINI DESC) WHERE AND ROWNUM <= %d"
#define QS_NOSERIE_CERT_RFC_TIPCER          "SELECT * FROM (SELECT NOSERIE FROM SEG_PKI.SEGT_CERTIFICADO WHERE RFC = '%s' AND TIPCERCVE IN (1, 2) ORDER BY VIGINI DESC) WHERE ROWNUM <= %d"
#define QS_NOSERIE_CERT_TIPO1Y2             "SELECT * FROM (SELECT NOSERIE FROM SEG_PKI.SEGT_CERTIFICADO WHERE RFC = '%s' AND TIPCERCVE IN (1, 2) AND EDOCER = '%s' ORDER BY VIGINI DESC) ROWNUM <= %d"
#define QS_EDO_CERTIFICADO                  "SELECT COUNT(*), EDOCER FROM SEG_PKI.SEGT_CERTIFICADO WHERE NOSERIE = '%s' GROUP BY EDOCER;"
#define QU_EDO_CERTIFICADO  			    "UPDATE SEG_PKI.SEGT_CERTIFICADO set EDOCER='%s' where NOSERIE ='%s'"
#define QI_CERTIFICADO_C                    "INSERT INTO SEG_PKI.SEGT_CERTIFICADO (NOSERIE, TIPCERCVE, EDOCER, RFC, CURP,NOM,CORREO,RFCRL,CURPRL, VIGINI, VIGFIN, FECREGIES, DIGPUBKMD5, DIGPUBKSHA1, PUBK, ESSAT, TRAMITECVE) VALUES ('%s', %s, '%s', '%s', '%s', '%s','%s', '%s', '%s', '%s', '%s',NULL, '%s', '%s', '%s','N', NULL)"
#define QS_EDO_VF_CERTIFICADO               "SELECT EDOCER, VIGFIN FROM SEG_PKI.SEGT_CERTIFICADO WHERE NOSERIE = '%s'"
#define QU_EDO_VF_CERTIFICADO	            "UPDATE SEG_PKI.SEGT_CERTIFICADO SET EDOCER = '%s', VIGFIN = '%s' WHERE NOSERIE = '%s'"
#define QI_CERTIFICADO_M                    "INSERT INTO SEG_PKI.SEGT_CERTIFICADO (NOSERIE, TIPCERCVE, EDOCER, RFC, CURP, VIGINI, VIGFIN) VALUES ('%s', %s, '%s', '%s', '%s', '%s', '%s')"
#define QS_TNOSERIE_CERT_RFC_TIPO_EDO		"SELECT NOSERIE FROM SEG_PKI.SEGT_CERTIFICADO WHERE RFC = '%s' AND TIPCERCVE = %d AND EDOCER = '%s' ORDER BY VIGINI DESC"
#define QS_TNOSERIE_CERTIFICADO             "SELECT NOSERIE FROM SEG_PKI.SEGT_CERTIFICADO WHERE RFC = '%s' AND TIPCERCVE = %d ORDER BY VIGINI DESC"
#define QS_TNOSERIE_CERT_TIPO1Y2	        "SELECT  NOSERIE FROM SEG_PKI.SEGT_CERTIFICADO WHERE RFC = '%s' AND TIPCERCVE IN (1, 2) AND EDOCER = '%s' ORDER BY VIGINI DESC"
#define QS_TNOSERIE_CERT_RFC_TIPCER   		"SELECT NOSERIE FROM SEG_PKI.SEGT_CERTIFICADO WHERE RFC = '%s' AND TIPCERCVE IN (1, 2) ORDER BY VIGINI DESC"



#endif




