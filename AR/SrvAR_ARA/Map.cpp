#include <Map.h>
static const char* _MAP_CPP_VERSION_ ATR_USED = "@(#) SrvAR_ARA (L : DSIC10155AR_ : Map.cpp : 1.1.1 : 0 : 10/04/16 )";

entidad* setDatosPtr(const char *numAR_AC, const char *cURL, const char *cPuerto, const char *cRuta, const char *cCertDelegado,
                 const char *cCadenaCert,const  char *cEmisor, const char *cModulo, const char *cArgModulo, const char *cNombreEnt)
{
   
   entidad *ent = new entidad;

   sprintf(ent->cNumAR_AC,"%s",numAR_AC);
   sprintf(ent->cURL,"%s", cURL);
   sprintf(ent->cPuerto,"%s",cPuerto);
   sprintf(ent->cRuta ,"%s",cRuta);
   sprintf(ent->cCertDelegado ,"%s",cCertDelegado);
   sprintf(ent->cCadenaCert ,"%s",cCadenaCert);
   sprintf(ent->cEmisor ,"%s",cEmisor);
   sprintf(ent->cModulo ,"%s",cModulo);
   sprintf(ent->cArgModulo ,"%s",cArgModulo);
   sprintf(ent->cNombreEnt ,"%s",cNombreEnt);

   return ent;

}

entidad setDatos(const char *numAR_AC, const char *cURL, const char *cPuerto, const char *cRuta, const char *cCertDelegado,
                 const char *cCadenaCert,const  char *cEmisor, const char *cModulo, const char *cArgModulo, const char *cNombreEnt)
{
   
   entidad ent;

   sprintf(ent.cNumAR_AC,"%s",numAR_AC);
   sprintf(ent.cURL,"%s", cURL);
   sprintf(ent.cPuerto,"%s",cPuerto);
   sprintf(ent.cRuta ,"%s",cRuta);
   sprintf(ent.cCertDelegado ,"%s",cCertDelegado);
   sprintf(ent.cCadenaCert ,"%s",cCadenaCert);
   sprintf(ent.cEmisor ,"%s",cEmisor);
   sprintf(ent.cModulo ,"%s",cModulo);
   sprintf(ent.cArgModulo ,"%s",cArgModulo);
   sprintf(ent.cNombreEnt ,"%s",cNombreEnt);

   return ent;

}


entidad *deDonde(map<char*,entidad*> entidades , char* numSerie)
{
   entidad *ent = NULL;
   map<char*,entidad*>::iterator it;

   for ( it=  entidades.begin() ; it != entidades.end(); it++ )
   {
     ent = it->second;
     if(strncmp(numSerie, ent->cNumAR_AC, 12) == 0 )
        return ent;
   }
   return NULL;
}

entidad* obtieneDatos(CConfigFile config,const char* cNombreEnt,char*etiqueta,int j )
{
   int resp;

   entidad *ent = NULL;
   entidad etiquetas;
   char puerto[30];

   string sNumARAC,sURL,sPuerto,sRuta,sCertDelegado,sCadenaCert,sEmisor,sModulo,sArgModulo;

   sprintf(etiquetas.cNumAR_AC,"ARAC_%d", j);
   sprintf(etiquetas.cURL,"URL_%d", j);
   sprintf(puerto,"PUERTO_%d",j);
   sprintf(etiquetas.cRuta ,"RUTA_%d",j);
   sprintf(etiquetas.cCertDelegado ,"CERT_DELEGADO_%d",j);
   sprintf(etiquetas.cCadenaCert ,"CADENA_CERT_%d",j);
   sprintf(etiquetas.cEmisor ,"EMISOR_%d",j);
   sprintf(etiquetas.cModulo ,"MODULO_%d",j);
   sprintf(etiquetas.cArgModulo ,"ARGUMENTOS_MOD_%d",j);


   resp = config.getValorVar(etiqueta, etiquetas.cNumAR_AC, &sNumARAC );
   resp = config.getValorVar(etiqueta, etiquetas.cURL, &sURL);
   resp = config.getValorVar(etiqueta, puerto, &sPuerto );
   if(resp)
      sPuerto = "80";
   resp = config.getValorVar(etiqueta, etiquetas.cRuta, &sRuta);
   resp = config.getValorVar(etiqueta, etiquetas.cCertDelegado, &sCertDelegado);
   resp = config.getValorVar(etiqueta, etiquetas.cCadenaCert, &sCadenaCert);
   resp = config.getValorVar(etiqueta, etiquetas.cEmisor, &sEmisor);
   resp = config.getValorVar(etiqueta, etiquetas.cModulo, &sModulo);
   resp = config.getValorVar(etiqueta, etiquetas.cArgModulo, &sArgModulo);


   ent  = setDatosPtr(sNumARAC.c_str(),sURL.c_str(),sPuerto.c_str(), sRuta.c_str(),sCertDelegado.c_str(),
                   sCadenaCert.c_str(), sEmisor.c_str(),sModulo.c_str(),sArgModulo.c_str(),cNombreEnt);

   return ent;

}

map<char*,entidad*> cargaDatosConfig(char* archcert)
{
   entidad *gral = NULL;
   map<char*,entidad*> mapaEntidad;

   CConfigFile  conf(archcert);

   if( !conf.cargaCfgVars())
   {   
      string numEnt,numARACs,sNombreEnt;
      char etiqueta[130]; 

      conf.getValorVar("[ENT_GENERAL]","NUM_EN",&numEnt); 
      
      int iNumEnt = atoi( numEnt.c_str());
      int resp  =-1;

      map<char*,entidad*>::iterator iter;

      for( int  i=0 ;i<iNumEnt;i++)
      {
         sprintf(etiqueta,"[ENTIDAD_%d]",i);
         resp = conf.getValorVar(etiqueta, "NUM_ARAC", &numARACs);
         resp = conf.getValorVar(etiqueta, "NOMBRE", &sNombreEnt);

         int iNumARACs =  atoi(numARACs.c_str());
         pair<map<char*,entidad*>::iterator,bool> respb;
         for(int j=1;j<iNumARACs+1;j++)
         {
            char *llave = new char[20];
            memset(llave,0,20);
            gral =obtieneDatos(conf,sNombreEnt.c_str() ,etiqueta, j );

            sprintf(llave,"%s",gral->cNumAR_AC);
  
            iter  = mapaEntidad.begin();
            mapaEntidad.insert(iter, pair<char*,entidad*>( llave, gral));
         }
         memset(etiqueta,0,sizeof(etiqueta));

      }
   } // Carga Variables del archivo
   return mapaEntidad;
}


