
#include <CSolQR.h>
#include <Sgi_ConfigFile.h>
#include <Sgi_Bitacora.h>
#include <CSolDatosWS_QR.h>
#include "ObtDatosWS.h"
#include <string.h> 

//CBitacora       *Bitacora      = NULL;
//CSolDatosWS_QR  ObjWS_QR;

std::string m_URLWS_QR;
CSolDatosWS_QR  objWS_QR;


#define PATH_CONF            "/usr/local/SAT/PKI/etc/"
#define PATH_DBIT            "/var/local/SAT/PKI/"

#ifdef DBG
   #define ARCH_CONF            PATH_CONF "ObtDatosWS_QR_dbg.cfg"
   #define PATH_BITA            PATH_DBIT "Obtiene_QR_dbg.log"
#else
   #define ARCH_CONF            PATH_CONF "ObtDatosWS_QR.cfg"
   #define PATH_BITA            PATH_DBIT "Obtiene_QR.log"
#endif

//######################################################################################################################
//Declaración de los CONSTRUCTORES
CSolQR :: CSolQR (CConfigFile& config, MensajesSAT& msgcli, CSSL_Skt* skt) : COperacion(config, msgcli, skt)
{
   iError = 0;
   lcadOriginal = sizeof(cadOriginal);
}

CSolQR :: ~CSolQR(){}

bool CSolQR :: preProceso()
{
    
    Bitacora       =  new CBitacora(PATH_BITA);
    Bitacora->escribePV(BIT_INFO, "CSolQR.preProceso: Inicia Pre-Proceso");
    assert(m_MsgCli.tipOperacion() == SOLQR);
    bool exito = false;
    //CConfigFile Configuracion(ARCH_CONF);
    // if ( (iError = Configuracion.cargaCfgVars()) )
    //{
    //  Bitacora->escribePV(BIT_INFO, "Error al leer el archivo de configuracion %d que se encuentra en '%s'",iError,ARCH_CONF);
    //  std::cout << "Problemas al leer el archivo de configuración (" << iError << "): " << ARCH_CONF << std::endl;
    //  return false;
    //}

    m_Configuracion.getValorVar("[WEB_SERVICE_ACUSES]" , "URLWS"      , &m_URLWS_QR );
     if ( m_URLWS_QR.length() == 0 )
    {
      Bitacora->escribePV(BIT_INFO, "CSolQR.preProceso: Error al recuperar la URL del WEB service");
      std::cout << "No se recupero la URL correctamente" << std::endl;

      return false;
    }

    iError = m_MsgCli.getMensaje(SOLQR, cadOriginal,&lcadOriginal );

    if (iError)
   {
      Bitacora->escribePV(BIT_INFO, "CSolQR.preProceso: Error al extraer los datos del mensaje");

      msgCliError('I', "Error en CSolQR en preProceso: Error al extraer los datos del mensaje", iError, eMSGSAT);
   }
    else{
      exito = true;
   }
   Bitacora->escribePV(BIT_INFO,"CSolQR.preProceso: Termina pre-proceso");
   return exito;
}

bool CSolQR :: Proceso()
{
    bool exito = false;
    std::string  qr ="";
    std::string  cadOrig=cadOriginal;
    char cadQR[3000+4];
    Bitacora->escribePV(BIT_INFO, "CSolQR.Proceso: Se obtuvo  la cadena '%s'***",cadOriginal);
    exito = objWS_QR.obtieneCodigoQR(cadOrig, &qr);
    Bitacora->escribePV(BIT_INFO, "CSolQR.Proceso: Se recuperÃ el QR '%s'***",qr.c_str());
    sprintf(cadQR,"%s",qr.c_str());
    if(strlen(qr.c_str())==0){
      sprintf(cadQR,"EUHD921024");
    }
    cadQR[strlen(cadQR)] = 0;
    Bitacora->escribePV(BIT_INFO,"CSolQR.Proceso: Se tiene el qr '%s' con longitud '%d'",cadQR,strlen(cadQR));
    if(m_MsgCli.setMensaje(CADENAQR,cadQR,strlen(cadQR))){
         Bitacora->escribePV(BIT_ERROR,"Error en CSolQR en Proceso: Error al guardar los datos en el mensaje");
    }
    else{
         Bitacora->escribePV(BIT_INFO,"CSolQR.Proceso: Se guardo el qr '%s' con longitud '%d'",cadQR,strlen(cadQR));
    }
    if(envia(CERTISAT)){
         Bitacora->escribePV(BIT_ERROR,"Error en CSolQR en Proceso: Error al enviar el mensaje");
   }
   else{
         Bitacora->escribePV(BIT_INFO,"CSolQR.Proceso: El mensaje fue enviado");
   }
    

    return exito;
}

/*
bool CSolQR::mensajeCodigoQR()
{
    bool exito = false;
    std::string  cadOriginal, qr;
    int iError;  
    CConfigFile Configuracion(ARCH_CONF);
    Bitacora       =  new CBitacora(PATH_BITA);

    Bitacora->escribe(BIT_ERROR, "***Inicia función para obtener codigo QR***");
     if ( (iError = Configuracion.cargaCfgVars()) )
    {
      std::cout << "Problemas al leer el archivo de configuración (" << iError << "): " << ARCH_CONF << std::endl;
      return false;
    }
    
    Configuracion.getValorVar("[WEB_SERVICE_IDC]" , "URLWS"      , &m_URLWS_QR );
    
    if ( m_URLWS_QR.length() == 0 )
    {
      std::cout << "No se recupero la URL correctamente" << std::endl;
      return false;
    }

    m_MsgCli.getMensaje(SOLQR,&cadOriginal);       

    bool resp = objWS_QR.obtieneCodigoQR(cadOriginal.c_str(), &qr);

    m_MsgCli.setMensaje(CADENAQR,qr);

   return exito;
}*/
