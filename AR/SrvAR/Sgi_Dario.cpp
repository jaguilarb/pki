static const char* _SGI_DARIO_CPP_VERSION_ ATR_USED = "SrvAR @(#)"\
"DSIC09041AR_ 2007-12-13 Sgi_Dario.cpp 1.1.0/3";

//#VERSION: 1.1.0
#include <Sgi_Dario.h>

Sgi_Dario::
Sgi_Dario():
bd(NULL)
{

}

Sgi_Dario::
~Sgi_Dario()
{
  if ( bd )
  {
     delete bd;
     bd = NULL;
  }
}

bool Sgi_Dario::
Conecta(const char* cInstancia, const char* cBD ,const char* cUsuario, const char* cPassword, const char* cRol )
{
   bd = new CBD(Bitacora);
   if (bd != NULL)
   {
      if ( bd->setConfig(cInstancia,cBD ,cUsuario, cPassword, cRol) )
      {
          if ( bd->conecta())
             return true;
      }
   }else  Bitacora->escribe(BIT_ERROR, "Error en Sgi_Dario en Conecta: No se creó el objeto CBD");
   return false;
}

bool Sgi_Dario::
Consulta(const char* cDato, char* cResultado)
{
    int  iStatus;
    char cCert[2];
    char cBiom[2];
    char cRFC[14];
 
    memset(cRFC,0,sizeof(cRFC));

    if ( strlen(cDato) == 12)
      sprintf(cRFC, " %s",cDato); 
    else
      sprintf(cRFC, "%s",cDato); 

    if ( bd->ejecutaSP(SGI_DARIO_SP_DATOS_ADIC_CRM_NOMBRE, SGI_DARIO_SP_DATOS_ADIC_CRM_PARAMS, cRFC) )
    { 
       if ( bd->getValores("ibb",&iStatus, cCert, cBiom) )
       {
          if ( strcmp(cBiom ,"S" ) == 0)
             strcpy( cResultado, "Y"); 
          else
             strcpy( cResultado, cBiom);   
          return true;
       }
    }  
    return false;
} 


