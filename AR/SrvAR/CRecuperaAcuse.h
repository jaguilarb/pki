/* ##############################################################################
 **###  PROYECTO:              PKI-SAT                                        ###
 **###  MODULO:                SrvAR	Servidor de la AR                     ###
 **###                         Lee los Acuses de la DB.                       ###
 **###                                                                        ###
 **###  DESARROLLADORES:       MELM					      ###
 **###                                                                        ###
 **###  FECHA DE INICIO:       Jueves 16, febrero del 2006		      ###
 **###                                                                        ###
 **##############################################################################
 **  1         2         3         4         5         6         7         8
 **12345678901234567890123456789012345678901234567890123456789012345678901234567890
 **
 **/

#ifndef _CRECUPERAACUSE_H_
#define _CRECUPERAACUSE_H_
static const char* _CRECUPERAACUSE_H_VERSION_ ATR_USED = "SrvAR @(#)"\
"DSIC09041AR_ 2007-12-13 CRecuperaAcuse.h 1.1.1/3";

//#VERSION: 1.1.1

class CRecuperaAcuse : public COperacion
{
   char	m_tipoRec;		// Tipo de la Recuperacion: 'T', 'G', 'R'
   char	m_Tram_Serie[20+1];	// Numero de Tramite o de Serie

   public:
      CRecuperaAcuse(CConfigFile&, MensajesSAT&, CSSL_Skt* );

   protected:
      //*** Clases nuevas para redefinir en cada operacion
      virtual bool preProceso();
      virtual bool Proceso();
      virtual const char* getNombreOperacion(){ return "RECACUSE"; };
      virtual int getTipOper(){ return RECACUSE; };
      //***
};

#endif // _CRECUPERAACUSE_H_

