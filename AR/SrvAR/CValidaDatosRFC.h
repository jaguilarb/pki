#ifndef _CVALIDADATOSRFC_H_
#define _CVALIDADATOSRFC_H_
static const char* _CVALIDADATOSRFC_H_VERSION_ ATR_USED = "SrvAR @(#)"\
"DSIC09041AR_ 2009-01-29 CValidaDatosRFC.h 1.1.0/2";

//#VERSION: 1.1.0

#include "CUtilBDAR.h"
#include "CDatosRFC.h"
#include "string"

#define SIN_BIOMETRICO  5

using namespace std;
class CValidaDatosRFC
{
   protected:
      CDatosRFC *m_datosRFC;
      CUtilBDAR *m_UtilBDAR;
      int       m_iTipoCer;
      bool      m_inicializado;  

   private:
      CConfigFile &m_Config;

      //char m_rfc[100];
      //char m_rfcrl[100];

      std::string sFuenteBiom;
      bool ObtieneConfigBiometricos();

   public:

      char m_rfc[100];

      CValidaDatosRFC(CConfigFile& config, CUtilBDAR *UtilBDAR, int iTipoCert);
      virtual ~CValidaDatosRFC();
      std::string getError();
      string    m_MsgDesc;
       
      //int  ConectaWS();
      bool ObtieneDatosWS(char* rfc);
      bool ValidaSFyD();
      //bool CerObl();
      bool TieneBiom();
      bool getRFCOrig(char* rfcOrig);
      int  getStatusRFC();
      bool getNombreRFC(std::string &nombre);
};

#endif // _CVALIDADATOSRFC_H_
