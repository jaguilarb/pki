/*##############################################################################
  ###  PROYECTO:              PKI-SAT                                        ###
  ###  MODULO:                Solicitud de datos de certificado              ###
  ###                                                                        ###
  ###                                                                        ###
  ###  DESARROLLADORES:       H�ctor Ornelas Arciga           HOA            ###
  ###                                                                        ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       Lunes 23, enero de 2006                        ###
  ###                                                                        ###
  ##############################################################################
         1         2         3         4         5         6         7         8
12345678901234567890123456789012345678901234567890123456789012345678901234567890

################################################################################
  VERSION:
     V.1.0.0  (20060123- )    HOA: Version Original
     V.1.0.1  (20141014 -         ) JAB: Se agrupan las sentencias SQL utilizadas en el SrvAR en un archivo de
                                              cabecera

#################################################################################*/
#ifndef _CSOLDATOSCERT_H_
#define _CSOLDATOSCERT_H_
static const char* _CSOLDATOSCERT_H_VERSION_ ATR_USED = "SrvAR @(#)"\
"DSIC09041AR_ 2007-12-13 CSolDatosCert.h 1.1.1/3";

//#VERSION: 1.1.1
//################################################################################
#include <COperacion.h>
//################################################################################
class CSolDatosCert : public COperacion
{
      char                m_nSerie[22];
   protected:
      virtual bool        preProceso();
      virtual bool        Proceso();
      virtual const char* getNombreOperacion() {return "SOLDATOSCERT";}
      virtual int         getTipOper() {return 1;} // PENDIENTE
      
   public:
      CSolDatosCert(CConfigFile& config, MensajesSAT& msgcli, CSSL_Skt* skt);
      virtual ~CSolDatosCert();
};
//################################################################################
#endif // _CSOLDATOSCERT_H_
