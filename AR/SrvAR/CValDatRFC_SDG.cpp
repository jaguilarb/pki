static const char* _CVALDATRFC_SDG_CPP_VERSION_ ATR_USED = "SrvAR @(#)"\
"DSIC09342AR_ 2009-08-17 CValDatRFC_SDG.cpp 1.1.1/3";

//#VERSION: 1.1.1
/*######################################################################################################################
  ###  PROYECTO:              PKI-SAT                                                                                ###
  ###  MODULO:                Clase que realiza las validaciones del RFC para generaci�n de FIEL                     ###
  ###                                                                                                                ###
  ###  DESARROLLADORES:       Gudelia Hern�ndez Molina        HEMG70AL                                               ###
  ###  FECHA DE INICIO:       25 de noviembre de 2008                                                                ###
  ###                                                                                                                ###
  ######################################################################################################################
         1         2         3         4         5         6         7         8         9        10        11        12
1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*#####################################################################################################################
   VERSION:
      V.1.1.1    (20090817 - 20090817) GHM: por cambio en el criterio de generaci�n de Sello Digital
      V.1.1.2    (20141001 -         ) JAB: Modificacionespara hacer uso del procedimiento almacenado que realiza la 
                                            validaci�n en la base de datos
####################################################################################################################*/

#include <CValidaDatosRFC.h>
#include <CValDatRFC_SDG.h>

CValDatRFC_SDG::CValDatRFC_SDG(CConfigFile& config, CUtilBDAR *UtilBDAR, int iTipoCert):
   CValidaDatosRFC(config, UtilBDAR, iTipoCert)
{
   liberaListas();
}

CValDatRFC_SDG::~CValDatRFC_SDG()
{
   liberaListas(); 
}

void CValDatRFC_SDG::liberaListas()
{
   pListaReg.clear();
   pListaRol.clear();
   pListaOblig.clear();
   pListaActiv.clear();

   return;
}

bool CValDatRFC_SDG::generaListaReg()
{
   std::string cve_regimen_ws;
   bool regimen_activo = false;

   // Obtiene el n�mero de reg�menes del WS IDC asociado al RFC
   int num_regimen = m_datosRFC->getsNumRegs();

   for (int indice=0; indice < num_regimen; indice++)
   {
      if(m_datosRFC->getReg(indice, &cve_regimen_ws, &regimen_activo))
      {
         // Si el r�gimen est� activo se agrega a la lista
         if (regimen_activo)
         {
            pListaReg.push_back(atoi(cve_regimen_ws.c_str()));
         }
      }
   }

   return true;
}

bool CValDatRFC_SDG::generaListaRol()
{
   std::string cve_rol_ws;
   bool rol_activo = false;

   // Obtiene el n�mero de roles del WS IDC asociado al RFC
   int num_rol = m_datosRFC->getsNumRoles();
   Bitacora->escribe(BIT_INFO, "CValDatRFC_SDG en generaListaRol: obteniendo el n�mero de Roles");
   for (int indice=0; indice < num_rol; indice++)
   {
      if (m_datosRFC->getRole(indice, &cve_rol_ws, &rol_activo))
      {
         // Si el rol est� activo lo agrega a la cadena
         if (rol_activo)
         {
            pListaRol += (cve_rol_ws + ",");
         }
      }
   }

   pListaRol = pListaRol.substr(0, pListaRol.length() - 1);

   return true;
}

bool CValDatRFC_SDG::generaListaObl()
{
   std::string cve_oblig_ws;
   bool oblig_activa = false;

   // Obtiene el n�mero de obligaciones del WS IDC asociadas al RFC
   int num_oblig = m_datosRFC->getsNumObligs();
   Bitacora->escribe(BIT_INFO, "CValDatRFC_SDG en generaListaObl: obteniendo el n�mero de Obligaciones");
   for (int indice=0; indice < num_oblig; indice++)
   {
      if (m_datosRFC->getObl(indice, &cve_oblig_ws, &oblig_activa))
      {
         // Si la obligaci�n est� activa la agrega a la cadena
         if (oblig_activa)
         {
            pListaOblig += (cve_oblig_ws + ",");
         }
      }
   }

   pListaOblig = pListaOblig.substr(0, pListaOblig.length() - 1);

   return true;
}

bool CValDatRFC_SDG::generaListaAct()
{
   std::string cve_activ_ws;
   bool activ_activa = false;

   // Obtiene el n�mero de actividades del WS IDC asociado al RFC
   int num_activ = m_datosRFC->getsNumActivs();
   Bitacora->escribe(BIT_INFO, "CValDatRFC_SDG en generaListaAct: obteniendo el n�mero de Actividades");
   for (int indice=0; indice < num_activ; indice++)
   {
      if (m_datosRFC->getAct(indice, &cve_activ_ws, &activ_activa))
      {
         // Si la actividad est� activa la agrega a la cadena
         if (activ_activa)
         {
            pListaActiv += (cve_activ_ws + ",");
         }
      }
   }

   pListaActiv = pListaActiv.substr(0, pListaActiv.length() - 1);

   return true;
}

bool CValDatRFC_SDG::CerObl()
{
   std::string rfc = m_datosRFC->getRFCOriginal();

   Bitacora->escribePV(BIT_INFO, "CValDatRFC_SDG en CerObl: Inicia validaci�n de obligaciones fiscales del RFC %s", rfc.c_str());

   bool cumple_validacion = false;
   std::string mensaje_validacion = "";

   generaListaReg(); // Genera la lista de regimenes a partir de la respuesta del WS IDC

   if(pListaReg.empty())
   {
      Bitacora->escribe(BIT_ERROR, "Error en CValDatRFC_SDG en CerObl: La lista de reg�menes est� vac�a");
   }
   else 
   {
      generaListaRol(); // Genera la cadena de roles a partir de la respuesta del WS IDC
      generaListaObl(); // Genera la cadena de obligaciones a partir de la respuesta del WS IDC
      generaListaAct(); // Genera la cadena de actividades a partir de la respuesta del WS IDC

      std::list<int>::iterator cve_regimen;
      int resultado;

      // Itera la lista de regimenes
      for (cve_regimen = pListaReg.begin(); cve_regimen != pListaReg.end(); ++cve_regimen) 
      {
         Bitacora->escribePV(BIT_INFO, "CValDatRFC_SDG en CerObl: Valida regimen=[%d], roles=[%s], obligaciones=[%s], actividades=[%s]", *cve_regimen, 
         pListaRol.c_str(), pListaOblig.c_str(), pListaActiv.c_str());

         // Ejecuta el procedimiento almacenado para validar si el regimen puede generar sellos digitales
         resultado = m_UtilBDAR->valGenSellos(*cve_regimen, pListaRol.c_str(), pListaOblig.c_str(), pListaActiv.c_str(), &mensaje_validacion);

         Bitacora->escribePV(BIT_INFO, "CValDatRFC_SDG en CerObl: %s", mensaje_validacion.c_str());

         // El resultado puede ser -1 (error interno), 0 (no puede generar sellos) y 1 (puede generar sellos)
         cumple_validacion = cumple_validacion || (resultado != -1 ? resultado : 0);

         // Si puede generar sellos digitales, termina y no realiza la validaci�n para los dem�s regimenes en caso de haberlos
         if (cumple_validacion) 
         {
            break;
         }
      }
   }

   Bitacora->escribePV(BIT_INFO, "CValDatRFC_SDG en CerObl: Termina la validaci�n de obligaciones fiscales del RFC %s", rfc.c_str());

   return cumple_validacion;
}
