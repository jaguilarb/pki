static const char* _CSOLDATOSRFC_CPP_VERSION_ ATR_USED = "SrvAR @(#)"\
"DSIC10294AR_ 2010-08-02 CSolDatosRFC.cpp 1.1.1/2";

//#VERSION: 1.1.1
// MAML 20100802: se implementa la funci�n msgCliError
#include <CSolDatosRFC.h>



CSolDatosRFC::
CSolDatosRFC(CConfigFile& configura, MensajesSAT& msgcli, CSSL_Skt* skt):
COperacion(configura, msgcli, skt),Config(configura),datosRFC(NULL),datosRFCRL(NULL),conRL(false),sTieneCert("N")
{
   iError = ERROR;
   iMensajeRecibido = ERROR;
   cRFC[0]  = 0;
   cRFCRL[0]= 0;
  

}


CSolDatosRFC::
~CSolDatosRFC()
{
   if (datosRFC)
   {
      delete datosRFC;
      datosRFC = NULL;
   }
   if (datosRFCRL)
   {
      delete datosRFCRL;
      datosRFCRL = NULL;
   }
 
}

///////////////////////////
//
/////////////////////////////

bool CSolDatosRFC::
preProceso()
{
   bool ok = true;
   iMensajeRecibido = m_MsgCli.tipOperacion();

   if ( (iMensajeRecibido != SOLDATOSRFC) && (iMensajeRecibido != SOLDATOSRFCRL)  )
   {
      Bitacora->escribePV(BIT_ERROR, "Error en CSolDatosRFC en preProceso: mensaje inesperado [ %d ].", iMensajeRecibido);
      ok = false;
   }
   else
   {
      iError = ObtieneMensaje(iMensajeRecibido);
      if(iError) 
      {
          Bitacora->escribePV(BIT_ERROR, "Error en CSolDatosRFC en preProceso: Error al obtener el mensaje  [ %d ].",iError);
          ok = false;
      }
   }
   
   if ( ok )
   {
      iError = ObtieneConfigBiometricos();
      if(iError)
      {
         Bitacora->escribePV(BIT_ERROR, "Error en CSolDatosRFC en preProceso: Error al Obtener la fuente de los biometricos del archivo de configuracion [ %d ] ", 
         iError);
         ok = false;
      }
   }
   
   if ( ok )
   {
      iError = ConectaWSLDAP();   
      if (iError)
      {
         Bitacora->escribePV(BIT_ERROR, "Error en CSolDatosRFC en preProceso: Error al Conectar a los WS o LDAP  [ %d ] ", iError);
         ok = false;
      }
   }
   
   //-->> MAML 20100730: siempre en todas las funciones de preProceso() debe terminar con esta funci�n cuando hay error 
   if (!ok)
      msgCliError('I', "Error en CSolDatosRFC en preProceso: Error en Preproceso" , (iError)?iError:iMensajeRecibido, eAPL); // si no lo encuentra en 
      //cat_error_oper => manda 90 (error interno)  
   //--<<
   return ok;
   
}

bool CSolDatosRFC::
Proceso()
{
   bool bExito = false;
   std::string sMensaje;

   if (  !ObtieneDatosRFC() )
   {
      if ( ArmaCadena(sMensaje))
      {
        ArmaRespuesta(DATOSRFC);
        Bitacora->escribePV(BIT_INFO, "CSolDatosRFC en Proceso: Se obtiene informaci�n de CSolDatosRFC :  Datos Obtenidos. %s", cDatosRFC);
        //DesconectaLDAP(); // MAML Para q sirve ?? 02/Abril/2009
        bExito = true;
      }
      else
      { 
         char *cMensaje = (char*)sMensaje.c_str();
         ArmaError(iError, cMensaje);   
         cMensaje = NULL;
      }
   }

   Envia();

   return bExito;
}


int CSolDatosRFC::
ObtieneMensaje(int iMensaje)
{
   char cFirma[TAM_FIRMA];
   int  iFirma =  sizeof(cFirma);
   
   iRFC   = sizeof(cRFC);
   iRFCRL = sizeof(cRFCRL);

   conRL = (iMensaje == SOLDATOSRFCRL) ? true:false; 

   iError = m_MsgCli.getMensaje(iMensaje, cFirma, &iFirma, cRFC, &iRFC, cRFCRL, &iRFCRL ); 
   if (iError) {
       Bitacora->escribePV(BIT_INFO, "CSolDatosRFC en ObtieneMensaje: Error al Obtener el mensaje recibido. C�digo Error: [%d]", iError);
       return ArmaError( iError, "En ObtieneMensaje: Error al Obtener el mensaje recibido.");
   }
   return EXITO;
}

 
int CSolDatosRFC::
ConectaWSLDAP()
{
   iError = ERROR;
    
   if (conRL)
   {
      iError =  ConectaWSLDAPRL();
      if(iError)
         return iError;
   } 

   iError = ConectaWSLDAPContrib();
   if (iError)
      return iError;

   return EXITO;
}

int CSolDatosRFC::
ConectaWSLDAPContrib()
{
   iError = ERROR;
  
   datosRFC = new CDatosRFC(cRFC, Config, 1);
   if (!datosRFC)
      return ERROR_INI_CDATOSRFC;

   iError  = datosRFC->ConectaWS();
   if (iError)
      return ERROR_CONECTA_WS;
   
   if ( strcmp( sFuenteBiom.c_str(),"L") == 0)
   {  
      iError  = datosRFC->ConectaLDAP();
      if(!iError)
         return ERROR_CONECTA_LDAP;
   }
   else if ( strcmp( sFuenteBiom.c_str(),"D") == 0)
   {
      iError =datosRFC->ConectaDARIO();
      if(iError)
         return ERROR_CONECTA_DARIO;
   }
   return EXITO;
}

int CSolDatosRFC::
ConectaWSLDAPRL()
{
   iError = ERROR;

   datosRFCRL = new CDatosRFC(cRFCRL, Config, 1);
   if (!datosRFCRL)
      return ERROR_INI_CDATOSRFC;

   iError  = datosRFCRL->ConectaWS();
   if (iError)
      return ERROR_CONECTA_WS;

   if ( strcmp( sFuenteBiom.c_str(),"L") == 0)
   {
      iError  = datosRFCRL->ConectaLDAP();
      if(!iError)
         return ERROR_CONECTA_LDAP;
   }
   else if ( strcmp( sFuenteBiom.c_str(),"D") == 0)
   {
      iError = datosRFCRL->ConectaDARIO();
      if(iError)
         return ERROR_CONECTA_DARIO;
   }


   return EXITO;
}


int CSolDatosRFC::
ArmaError( int iError, char *cDescError)
{
   char codError[5];
   int iDescError = strlen(cDescError);    // TIPO, codigo error,desc error 
   
   sprintf(codError, "%d",iError);
   iError = m_MsgCli.setMensaje(MSGERROR,"I" ,1,codError, strlen(codError),cDescError, iDescError );




/*
 *Se inicializa un apuntador de tipo char con el mensaje "Error en CSolDatosRFC: "
 *Despu�s se concatena con el mensaje de error de cDescError, y se env�a a la Bitacora.
 *
 */
  
//String con el mensaje a concatenar
     char nombreClaseCSolDatosRFC[200]="Error en CSolDatosRFC ";
     char *errorCSolDatosRFC=nombreClaseCSolDatosRFC;
     int i=0, j=0;

    while(errorCSolDatosRFC[i]!='\0'){
        i++;
    }

//Al string se le concatena cDescError
    while(cDescError[j]!='\0'){
        errorCSolDatosRFC[i++]=cDescError[j++];
    }

    errorCSolDatosRFC[i]='\0';
    Bitacora->escribePV(BIT_ERROR, errorCSolDatosRFC, iError);
   
   
  //Bitacora->escribePV(BIT_ERROR, cDescError, iError);  
   return iError;
}


int CSolDatosRFC::
ObtieneDatosRFC()
{
   std::string sError;
   std::string sRFC   = cRFC;
   std::string sRFCRL = cRFCRL; 

   if ( datosRFC->ObtieneDatosWS())
   {
      sError = "en ObtieneDatosRFC: No se pudo obtener la informacion completa del contribuyente [" + sRFC+"]";
      ArmaError(10,(char*)sError.c_str());
      return  ERROR_OBT_DATOS_WS;
   }
 
   if (conRL)
   {
     if ( datosRFCRL->ObtieneDatosWS())
     {
        //sError = "ObtieneDatosWS Del Representante legal " + sRFCRL;
        sError = "No se obtieneDatosWS Del Representante legal " + sRFCRL;
        ArmaError(10,(char*)sError.c_str());
        return  ERROR_OBT_DATOS_WS;
     }
     //AGZ: Vamos a obtener la CURP del RL de la Base de datos para que sea la 
     //ultima generada con su FIEL
     if (m_utilBDAR != NULL){
        if (!m_utilBDAR->getCURP(cRFCRL, &sCurpFiel)){
           Bitacora->escribe(BIT_ERROR, "Error en CsolDatosRFC en ObtieneDatosRFC: No se obtuvo la CURP del Certificado de FIEL activo");
        }
     } 
     //AGZ: Fin.
     sTieneCertRL = m_utilBDAR->tieneFEAact(cRFCRL)? "Y":"N";

   }
 
   if ( strcmp( sFuenteBiom.c_str(),"L") == 0)
      iError = datosRFC->ObtieneDatosLDAP();
   else if ( strcmp( sFuenteBiom.c_str(),"D") == 0)
      iError = datosRFC->ObtieneDatosDARIO();
   if (iError != EXITO )
   {
       if (iError == ERROR_OBT_DATOS)
          Bitacora->escribe(BIT_ERROR, "Error en CsolDatosRFC en ObtieneDatosRFC: No se obtuvieron los datos de los biometricos del contribuyente.");
       if (iError == ERROR_DATOS_NULL)
          Bitacora->escribe(BIT_ERROR, "Error en CsolDatosRFC en ObtieneDatosRFC: El RFC que pretende buscar esta en blanco.");
       if (iError == ERROR_OBT_DN)
          Bitacora->escribe(BIT_ERROR, "Error en CsolDatosRFC en ObtieneDatosRFC: No se obtuvo el DN del  RFC." );

       sError = "en ObtieneDatosRFC: No se pudo obtener la informacion completa del contribuyente [" + sRFC+"]";
       ArmaError(iError,(char*)sError.c_str());
       return ERROR_OBT_DATOS_LDAP;
   }

   sTieneCert = m_utilBDAR->tieneFEAact(cRFC)? "Y":"N";


   return EXITO; 

}


bool CSolDatosRFC::
ArmaCadena(std::string &sMensaje)
{
   bool bResp = false;
   std::string sDatosRL; 
   std::string sTieneBio;
   std::string sDomicilio;
   std::string sTipContrib;
   std::string sNombre;
   std::string sDescSitDom;
   std::string sNombreRL;
   std::string sSitFiscal;
   std::string sDescSitFiscal;
   std::string sCURPRFC; 
   std::string sCURPRFCRL;
   int iSitDom = 0;


   sDomicilio     = datosRFC->getDomicilio();
   sSitFiscal     = datosRFC->getSitFiscal();
   sTipContrib    = datosRFC->getTipContrib();
   sNombre        = datosRFC->getNombre();
   sDescSitDom    = datosRFC->getDescSitDom();
   sTieneBio      = datosRFC->sTieneBiom;
   sCURPRFC       = datosRFC->getCURP();
   iSitDom        = datosRFC->getSitDomicilio();
   sDescSitFiscal = datosRFC->getDescSitFis();

   //GHM(090326): Se inhabilito este registro en la bit�cora para no desplegar informaci�n sensible
   //>- Bitacora->escribePV(BIT_INFO, "Domicilio                 [%s].",sDomicilio.c_str());
   Bitacora->escribePV(BIT_INFO, "CSolDatosRFC.ArmaCadena: Situaci�n Fiscal          [%s].",sSitFiscal.c_str());
   Bitacora->escribePV(BIT_INFO, "CSolDatosRFC.ArmaCadena: Tipo de Contribuyente     [%s].",sTipContrib.c_str());
   Bitacora->escribePV(BIT_INFO, "CSolDatosRFC.ArmaCadena: Nombre                    [%s].",sNombre.c_str());
   Bitacora->escribePV(BIT_INFO, "CSolDatosRFC.ArmaCadena: Descripcion Sit.Domicilio [%s].",sDescSitDom.c_str());
   Bitacora->escribePV(BIT_INFO, "CSolDatosRFC.ArmaCadena: Biom�tricos               [%s].",sTieneBio.c_str());
   Bitacora->escribePV(BIT_INFO, "CSolDatosRFC.ArmaCadena: CURP                      [%s].",sCURPRFC.c_str());
   Bitacora->escribePV(BIT_INFO, "CSolDatosRFC.ArmaCadena: Situaci�n de Domicilio    [%d].",iSitDom);
   Bitacora->escribePV(BIT_INFO, "CSolDatosRFC.ArmaCadena: Descripcion Sit. Fiscal   [%s].",sDescSitFiscal.c_str());
   

   if ( sDomicilio.compare(" ") != 0 )
   {
      if ( sSitFiscal.compare(" ") != 0 )
      {
         if ( sTipContrib.compare(" ") != 0)
         {
            if ( sNombre.compare(" ") != 0)
            {
               if ( sDescSitDom.compare(" ") != 0)
               {
                  if ( sTieneBio.compare(" ") != 0)
                  {
                     if (sDescSitFiscal.compare(" ") != 0)
                     {
                        sMensaje = "Datos Completos encontrados.";
                        bResp = true;
                     }
                     else
                        sMensaje = "No se obtuvo la Descripci�n de la Situaci�n Fiscal."; 
                  }
                  else sMensaje = "No se obtuvieron los biom�tricos.";
               }
               else sMensaje = "No se obtuvo la  Descripci�n de Situaci�n de domicilio.";
            }
            else sMensaje =  "No se obtuvo el nombre.";
         }
         else sMensaje =  "No se obtuvo el tipo de contribuyente.";
      }
      else sMensaje = "No se obtuvo la Situaci�n fiscal.";
   }
   else sMensaje =  "No se obtuvo la situaci�n de domicilio.";




  if(!bResp)
    return bResp;


   if (conRL)
   {
      std::string sCurp_idc = datosRFCRL->getCURP();
      std::string sRFCRL = cRFCRL;
      sNombreRL  = datosRFCRL->getNombre();
      //sCURPRFCRL = datosRFCRL->getCURP();
      if (sCurpFiel.compare(sCurp_idc) == 0){
         sCURPRFCRL = sCurp_idc;
      }
      else sCURPRFCRL = sCurpFiel;
      sDatosRL = sRFCRL  + "|" + sTieneCertRL + "|" + sNombreRL + "|" + sCURPRFCRL + "|"; 
   }
   else
      sDatosRL = "||||"; 
 
   sprintf( cDatosRFC,"%s|%s|%s|%s|%s|%s|%s|%d|%s|%s|%s|%s",
                      cRFC,
                      sTipContrib.c_str(),
                      sNombre.c_str(),
                      sCURPRFC.c_str(),
                      sDomicilio.c_str(),
                      sSitFiscal.c_str(),
                      sDescSitDom.c_str(),
                      iSitDom, 
                      sDescSitFiscal.c_str(),
                      sTieneCert.c_str(),
                      sTieneBio.c_str(),
                      sDatosRL.c_str()
          );  
  
   return bResp; 

}

int CSolDatosRFC::
Envia()
{
   return envia(CERTISAT);
}


int CSolDatosRFC::
ArmaRespuesta(int iOperacion)
{
   int iDatosRFC = strlen(cDatosRFC);
   iError = m_MsgCli.setMensaje(iOperacion, cDatosRFC, iDatosRFC );  
   if (!iError)
   { 
      if (m_utilBDAR->RegOperDetalleBDAR(iOperacion, m_numOp))
         Bitacora->escribePV(BIT_INFO,"CSolDatosRFC.ArmaRespuesta: Mensaje armado %d." ,iOperacion);
      else
         Bitacora->escribePV(BIT_INFO,"CSolDatosRFC.ArmaRespuesta: Error al insertar la operacion %d." ,iOperacion);
   }
   else
      ArmaError(iError, "ArmaRespuesta: Error al  armar el mensaje");
   return iError;

}
/*void CSolDatosRFC::
DesconectaLDAP()
{
   datosRFC->DesconectaLDAP();
  
}*/

bool CSolDatosRFC::
ObtieneDatosBD(char *cRFCb)
{
   std::string sRFCaux = "N";
   sTieneCert = m_utilBDAR->tieneFEAact(cRFCb)? "Y":"N";
   return true;

}


bool CSolDatosRFC::
ObtieneConfigBiometricos()
{
    Config.cargaCfgVars();
    return Config.getValorVar("[BIOMETRICOS]", "FUENTE",&sFuenteBiom);

}

