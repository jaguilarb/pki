#ifndef _CSELLOSDIGS_H_
#define _CSELLOSDIGS_H_
static const char* _CSELLOSDIGS_H_VERSION_ ATR_USED = "SrvAR @(#)"\
"DSIC09041AR_ 2007-12-13 CSellosDigs.h 1.1.1/3";

//#VERSION: 1.1.1


#define	ERR_UPDATE_REQS_SELLOS	333	// Error al intentar actualizar la tabla de Reqs_Sellos

///////////////////////////////////////////////////////////////////////////////
// COMO NO SER USAR VECTORES O PATRONES (O TEMPLATES)
// hare una lista ligada de los nombres de los archivos certificado

typedef struct lista_cert {
   char		cNomCer[MAX_RUTA_ARCH+1]; // +1 del pipe q se le va a concatenar a cada name de archivo
   lista_cert	*pNext;

   lista_cert () { memset(&cNomCer, 0, MAX_RUTA_ARCH+1); pNext = NULL; }; 
} LISTA_CERT;

extern LISTA_CERT *pListaNodo;

// Inserta un nuevo nodo en la lista ligada
bool insertaNodo(char *);

// libera todos los nodos de la actualización masiva
void liberaNodos();

///////////////////////////////////////////////////////////////////////////////
class CSellosDigs : public CArchivoEnsob
{
   public:
      CSellosDigs(CConfigFile&, MensajesSAT&, CSSL_Skt* );
      virtual ~CSellosDigs();	// Es virtual para no tener problemas con la compilación en suse-linux
      int borraDir(const char *ruta);
      int borraArchivos(const char *ruta);
 
   protected:
      //*** Clases nuevas para redefinir en cada operacion
      virtual bool preProceso();
      virtual bool Proceso();
      virtual const char* getNombreOperacion() { return "INICIOTRANSSDG"; };
      virtual int getTipOper() { return INICIOTRANSSDG; } ;
      //***

   private:
      // Para obtener el nombe Original del certificado: SIN RUTA, porque asi llega
      desCompresion	*pdescComp;	// Necesitamos este puntero vivo p/q no destruya lista ligada
      struct  nomzip	stMiNodo;
      SHA_CTX           m_ctx;
      int               iNumPartes;
      string	        spath_Procesados;
      // Globales Por que se dividio EnviaIES en DescomprimeReqsSellos y GeneraSellosDigs() 
      char	        cArchRen[MAX_RUTA_ARCH], cDirArchsReq[MAX_RUTA_ARCH], cDirArchsCer[MAX_RUTA_ARCH];

      bool acuseExito();      
      bool msgInicial();
      bool msgRecArch();
      bool msgFinArch();
      void sustituyeAmp(char *cCadSrc, char *cCadDes);

      bool DescomprimeReqsSellos();
      int  numero_reqs();
      bool enviaMsgSello(int cReq, int n_reqs );
      bool GeneraSellosDigs();
      bool ComprimeSellosDigs();
      char *getListaCer();
      bool guardaCertSelloDig(char *cArchCer);
      bool obtenFechaTramite(const char *fecha);
      bool genAcuseSellos(CAcuses *pacuse);
      int  regFileSellos_BDAR(const char *NameFileREQ, int *oper_secSellos);
      int  updateSellos_BDAR(int oper_secSellos);
      bool MarcasSellos();
      bool NombreRuta(int iNumFTP,char* cRutaIni,char* cRutaFin);
};

#endif // _CSELLOSDIGS_H_

