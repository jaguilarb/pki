#ifndef _CSOLCERT_H_
#define _CSOLCERT_H_
/*######################################################################################################################
  ###  PROYECTO:              PKI-SAT                                                                                ###
  ###  MODULO:                Clase que realiza la Conexi�n al SrvAR                                                 ###
  ###                                                                                                                ###
  ###  DESARROLLADORES:       Gudelia Hern�ndez Molina        GHM                                                    ###
  ###                         H�ctor Ornelas Arciga           HOA                                                    ###
  ###  FECHA DE INICIO:       Martes 06, diciembre del 2005                                                          ###
  ###                                                                                                                ###
  ######################################################################################################################
         1         2         3         4         5         6         7         8         9        10        11        12
123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
 #######################################################################################################################
   VERSION:
      V.1.00      (20051206 - 20060101) GHM: Primera Versi�n
   CAMBIOS:
      V.1.1.4     (20141014 -         ) JAB: Se agrupan las sentencias SQL utilizadas en el SrvAR en un archivo de
                                              cabecera
######################################################################################################################*/

static const char* _CSOLCERT_H_VERSION_ ATR_USED = "SrvAR @(#)"\
"DSIC09041AR_ 2007-12-13 CSolCert.h 1.1.1/3";

//#VERSION: 1.1.1

// *************************************************************** //
#include <CGeneraCert.h>
#include <CValidaDatosRFC.h>
#include <CValDatRFC_SDG.h>

// *************************************************************** //
// *************************************************************** //
// *************************************************************** //
using namespace std;
class CSolCert: public CGeneraCert
{
   public:
      CSolCert(CConfigFile& config, MensajesSAT& msgcli, CSSL_Skt* skt);
      virtual ~CSolCert();

   protected:
      CValidaDatosRFC *m_valDat_FIEL;
      CValDatRFC_SDG  *m_valDat_SDG;

      virtual       bool  preProceso();
      virtual       bool  Proceso();
      virtual const char* getNombreOperacion() { return "Solicita Certificado"; }
      virtual       int   getTipOper        () { return SOLCERT; }

      virtual       int   ValidaSujeto(); // MAML 16/Enero/2007: de bool a int. P/hacer eficiente manejo de errores      
       
      bool IniValidaWS_IDC();   //Para la sustituci�n de validaciones en DARIO
      bool IniValidaSDWS_IDC(); //Para la sustituci�n de validaciones en DARIO
      bool Registra_RFC();     

      int  ValidaFIELWS();
      int  ValidaSDGWS();
      int  ValidaAGC();
      int  AlmacenaReq(int idx = 0, bool enviaError = true); // MAML 16/Enero/2007: p/hacer eficiente manejo de errores
      bool ValidaSFyD();
      void Error(bool enviaError, char tipo, const char* msg, int error, eIdLib eId, int errorBD);
      void ArmaError( char tipo, const char* msg, int error, int errorBD);
      bool getAcuseDatos(string *sNo_Serie,string *sNombre,string *sPubK,string *sModulo,string *sNombreRL,string *sRFCRL);
      bool genAcuseGenCert(CAcuses *pacuse);
      
};


#endif
