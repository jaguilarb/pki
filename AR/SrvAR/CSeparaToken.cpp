static const char* _CSEPARATOKEN_CPP_VERSION_ ATR_USED = "SrvAR @(#)"\
"DSIC10294AR_ 2010-07-22 CSeparaToken.cpp 1.1.1/2";

//#VERSION: 1.1.0
//#VERSION: 1.1.1: se agrega lencadena y acepta cadenas como: "valor0", "valor0||valor2" ie ya no importa si termina o no en el carater separador
#include <stdio.h>
#include <string.h>
#include <Sgi_Bitacora.h>
#include "CSeparaToken.h"

// Suponemos en cadena cadena el sig. formato: y siempre valor0 != null
// "valor0|valor1||valor4|valor5|||valor8|"
CSeparaToken::CSeparaToken(char *cadena, char caracter) 
{
   char *pch  = NULL; 
   char *pant = NULL;
   int  laux  = 0;
   int  len   = 0;
   i= 0;
   
   inicia(cadena);
   //printf ("Looking for the 's' character in \"%s\"...\n",str);
   pant = cadena;
   pch  = strchr(cadena, caracter);
   if (pch == NULL)
      pch  = strchr(cadena, '\0');
   for(i = laux = len =  0; pch != NULL && i < MAX_SEPARADORES && lencadena > laux; i++, laux += len + 1)
	{
		len = pch-pant; // Longitud de la cadena != NULL
		if ( cadena[laux] == caracter ) // => Se repite "caractercaracter"
			items[i] = NULL;
		else 
		{
			items[i] = new char[len+1]; // mas el nulo
         if ( items[i] )
			   sprintf(items[i], "%*.*s", len, len, pant); 
         else
         {
            Bitacora->escribe(BIT_ERROR, "Error en CSeparaToken en CSeparaToken: Falta de memoria para procesar la cadena en CSeparaToken" );
            i = -1;
         }
		}
		pant = (cadena+laux+len+1); // mas el caracter
		pch = strchr(pch+1, caracter);
      if (pch == NULL && lencadena > (laux+len+1) )
         pch=strchr(pant, '\0');
	}
}

int CSeparaToken::getCounter()
{
	return i;
}

CSeparaToken::~CSeparaToken()
{
   for (int j = 0; j < i ; j++)
   {
      if ( items[j] != NULL )
      {
         delete[] items[j];
         items[j] = NULL;
      }
   }
}

void CSeparaToken::inicia(char *cadena )
{
   for (i = 0; i < MAX_SEPARADORES ; i++)
      items[i] = NULL;

   lencadena = (cadena != NULL)? strlen(cadena):0;
}
 
