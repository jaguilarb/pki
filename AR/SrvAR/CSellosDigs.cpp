static const char* _CSELLOSDIGS_CPP_VERSION_ ATR_USED = "SrvAR @(#)"\
"DSIC09041AR_ 2007-12-13 CSellosDigs.cpp 1.1.1/3";

//#VERSION: 1.1.1
// Esta en la ruta de Informix: /usr/informix/incl/c++/  

#include <assert.h>
#include <unzip.h>
//#include <unistd.h>
#include <dirent.h>

#include <Sgi_BD.h>
//- #include <CConfigFile.h> //- GHM (070430)
#include <Sgi_ConfigFile.h>  //+ GHM (070430)
#include <CAcuses.h>
#include <CGeneraCert.h>
#include <CSolCert.h>

//- #include <ARunZip.h>  //- GHM (070430)
//- #include <ARinZip.h>  //- GHM (070430)
#include <Sgi_Zip.h>      //+ GHM (070430)
#include <CUtilBDAR.h>		// Funciones de Utileria de la AR
#include <CArchivoEnsob.h>
#include <CSellosDigs.h>

#include <Sgi_PKI.h>       //+ GHM (070321): Para utilizar la funci�n CreaDir

#if ORACLE
   #include <CosltSrvAR_ORA.h>   // + JAB (20141014)
#else
   #include <CosltSrvAR.h>   // + JAB (20141014)
#endif

// POR FAVOR, NO USAR VARIABLES GLOBALES CON LAS CLASES DE INFORMIX: IT*
// Donde esta el delete de las clases se les hace new en PbaBaseSrvPKI.cpp (ejemplo: *ptrSrvArPKI)
// Si estan la funcion abreBD_AR(), falta la cierraBD_AR() ??

/* **************************************************************************************** */

LISTA_CERT *pListaNodo; // OjO: depurar esta variable global, Q tal si le pasa lo mismo q a las de Informix

// Inserta un nuevo nodo en la lista ligada
bool insertaNodo(char *cNameCer) // Reg
{
   LISTA_CERT		*pNew = NULL, *pAux = NULL;

   if ( (pNew = new LISTA_CERT()) != NULL) 
   {
      memcpy(&(pNew->cNomCer), cNameCer, strlen(cNameCer)); // cNameCer debe terminar en nulo 
      if (pListaNodo == NULL) // es el primero	
         pListaNodo = pNew;
      else 
      { // hay q buscar el ultimo y asignarlo 
         pAux = pListaNodo;
         while (pAux->pNext != NULL)
            pAux = pAux->pNext;
         // segun yo llego al ultimo y lo Inserta al final de la lista
         pAux->pNext = pNew;
      }
   }
   else 
   {
      Bitacora->escribe(BIT_ERROR, "(EM01) Error de memoria en new LISTA_CERT");
      liberaNodos();
      return (true);
   }
   return (false);
}

// libera todos los nodos de la actualizaci�n masiva
void liberaNodos() 
{
   LISTA_CERT		*pAux = NULL, *pSig = NULL;

   if (pListaNodo == NULL) 
      return; 
   pAux = pListaNodo; // apunta al 1ero. al menos existe 1 nodo
   do 
   {
      pSig = pAux->pNext;
      delete pAux;
      pAux = NULL; 
      if (pSig != NULL)
         pAux = pSig;
   } while (pSig != NULL);
   pListaNodo = NULL; // segun yo aqui TODOS pAux y pSig APUNTAN a NULL
}

/* *********************************************************************** */
/* *********************************************************************** */
CSellosDigs::CSellosDigs(CConfigFile& config, MensajesSAT& msgcli, CSSL_Skt* skt):
   CArchivoEnsob(config, msgcli, skt)
{
   // SIEMPE VA A SER DEL TIPO 2 DE SELLOS DIGITALES y Esta en la clase CGeneraCert
   m_tipo = TIPCER_SD;
   //GeneroCERT = false;		// Al menos trata de generar un certificado Digital
   memset(cArchRen, 0, sizeof(cArchRen));
   memset(cDirArchsReq, 0, sizeof(cDirArchsReq));
   memset(cDirArchsCer, 0, sizeof(cDirArchsCer));
   pdescComp = NULL;
}

/* *********************************************************************** */
CSellosDigs::~CSellosDigs()
{
   //char cComando[2*MAX_RUTA_ARCH];

   if (pdescComp)	// Objeto global para ver siempre la lista ligada de la descompresion
   {
      delete pdescComp;
      pdescComp = NULL;
   }
   // SE crearon los directorios .REQ y .CER => los borramos y too los archivos Generados o NO
   if (cDirArchsCer[0]) 
   {
      borraArchivos(cDirArchsCer);
      borraDir(cDirArchsCer);
   }
   if (cDirArchsReq[0]) 
   {
      borraArchivos(cDirArchsReq);
      borraDir(cDirArchsReq);
   }
   if (cArchRen[0] && m_numTram[0])
      // MAML 080902: ahora respalda el archivo ensobretado. // unlink(cArchRen);  
      AlmacenaEns( cArchRen, ".sdg" ); 
}

/* *********************************************************************** */

// FUNCION INICIAL
// llama a los 3 primeros mensajes iniciales para recibir el archivo ensobretado
// Si no llega bien el archivo Ensobretado => PELAS, regresa false
// Recibe el mensaje INICIOTRANSSDG y debiera hacer validaciones y dejar listo para Generar el certificado
bool CSellosDigs::preProceso()
{
   // Obtiene los datos de la Solicitud de Sellos, nombre/archivo	
   assert(m_MsgCli.tipOperacion() == INICIOTRANSSDG);

   if ( !error &&  msgInicial() && msgRecArch() ) 
   {
      msgFinArch();
   }

   // Iniciamos las varibles de una tramite : m_cadOriOp, m_refArchOp
   if ( !error && DescomprimeReqsSellos() ) 
      IniciaFin_preProceso();

   if (error)
      msgCliError(m_errTipo, m_errDesc, error, eAPL);

   return( !error );
   // despues de aqui se inicializa m_numOp y m_numTram
}

// FUNCION PRINCIPAL de la clase Derivada
bool CSellosDigs::Proceso()
{
   // OJO MELM : tal vez la funcion envTramite(NUMTRAMITE) deberia ser virtual por que el mensaje 
   //            q Sellos necesita mandar es TRANSCOMPLETA
   
   int oper_sec = m_utilBDAR->RegOperDetalleBDAR(TRANSCOMPLETA, m_numOp );

   Bitacora->escribe(BIT_INFO, "CSellosDigs: Registro de la operaci�n TRANSCOMPLETA en la BD");

   // YA TENGO LA FECHA DE OPERACION d TRANSCOMPLETA => PUEDO GENERAR EL ACUSE
   //CAcuses m_acuse(m_BDAR, m_Configuracion, m_numOp, m_numTram );
   //if ( !(error = (oper_sec == -1)) && genAcuseSellos(&m_acuse) )
   //{	
      //AGZ: Ya no por que se responde antes.
      //m_MsgCli.setMensaje(TRANSCOMPLETA, m_acuse.cad_orig, strlen(m_acuse.cad_orig),
      //                    m_acuse.firma,    strlen(m_acuse.firma) );  
      //envia(CERTISAT); // manda el Mensaje al CERTISAT
     // genAcuseRecepcion();
   //}
   // Trata de Generar todos los certs. digitales, y Verifica si al menos uno se genero
   //AGZ: vamos a tratar de responder aqu�... 
   genAcuseRecepcion();

   if ( !error )
      GeneraSellosDigs();

   ComprimeSellosDigs(); 
	
   if (error)
      Bitacora->escribePV(BIT_ERROR, "Error (%i) al generar los certificados de sellos digitales", error); //AGZ: Ya no debe regresar errores al CERTISAT 
   //msgCliError(m_errTipo, m_errDesc, error, eAPL); // Para q llamarla en tanto lugar
   
   return( !error );
}

/* *********************************************************************** 
 Metodo para recueperar el nombre del Contribuyente necesario para regresar
 el mensaje de recepcion de solicitud.
 RETURN: Regresa intE con 0 si exito != 0 Error.
*********************************************************************** */
int CSellosDigs::getNombreContribuyente(char* nombre) //Si solicita sellos ya tiene FIEL
{
   string tmp;
   if (m_BDAR != NULL)
   {
      if (m_BDAR->consultaReg(SELLOS_OBTEN_PRIMER_NOMBRE_CERTIFICADO_POR_RFC, m_rfcOp))
      {
         if (m_BDAR->getValores("s", &tmp))
         {
            strcpy(nombre, tmp.c_str());

            return 0;
         }
         else
         {
            Bitacora->escribe(BIT_ERROR, "CSellosDigs.getNombreContribuyente: No se pudo obtener el resultado de la consulta a la BD");

            return ERR_GET_VAL_NOMBRE;
         }
      }
      else
      {
         Bitacora->escribe(BIT_ERROR, "CSellosDigs.getNombreContribuyente: No se pudo ejecutar la consulta a la BD");

         return ERR_GET_NOMBRE; 
      }
   }
   else
   {
      Bitacora->escribe(BIT_ERROR, "CSellosDigs.getNombreContribuyente: No hay conexi�n abierta a la BD, el objeto m_BDAR es nulo");

      return ERR_GET_NOMBRE;
   }
}

int CSellosDigs::getFechaOp(char* fecha)
{
   string tmp;

   if (m_BDAR != NULL)
   {
      if (m_BDAR->consultaReg(SELLOS_OBTEN_FECHA_OPERACION_POR_SOLICITUD, m_numOp))
      {
         if (m_BDAR->getValores("s", &tmp))
         {
            strcpy(fecha, tmp.c_str());

            return 0;
         }
         else
         {
            Bitacora->escribe(BIT_ERROR, "CSellosDigs.getFechaOp: No se pudo obtener la fecha de la operaci�n");

            return ERR_GET_VAL_FECHAOP;
         }
      }
      else
      {
         Bitacora->escribe(BIT_ERROR, "CSellosDigs.getFechaOp: No pudo ejecutar la consulta que obtiene la fecha de la operaci�n de la BD");

         return ERR_GET_FECHAOP;
      }
   }
   else
   {
      Bitacora->escribe(BIT_ERROR, "CSellosDigs.getFechaOp: No hay conexi�n abierta con la BD, el objeto m_BDAR es nulo");

      return ERR_GET_FECHAOP;
   }
}

void CSellosDigs::genAcuseRecepcion()
{
   //AGZ: Aqu� vamos a regresar el mensaje de recepcion.
   char sNombre[512], FechaHora[100];
   int errNum = -1;
   
   Bitacora->escribe(BIT_INFO, "CSellosDigs: Generaci�n y envio del acuse de recepci�n del requerimiento de sellos digitales");

   FechaHora[0]=0; 
   errNum = getFechaOp((char*)FechaHora);
   if (errNum == 0)
   {
      errNum = m_MsgCli.setMensaje(ACUSERECSELLOS, m_rfcOp, strlen(m_rfcOp), cNombreContrib, strlen(cNombreContrib),
                                   m_numTram, strlen(m_numTram), FechaHora, strlen(FechaHora));
      //errNum = m_MsgCli.setMensaje(ACUSERECSELLOS, m_rfcOp, strlen(m_rfcOp), cNombreContrib, strlen(cNombreContrib), m_numOp, strlen(m_numOp), 
      //            FechaHora, strlen(FechaHora));
      if (errNum == 0)
      {
         errNum = envia(CERTISAT); //Aqui enviamos el mensaje RECSELLOS... se debe detener el tramite si hay estos errores?
         if (errNum == 0)
         {
            m_utilBDAR->RegOperDetalleBDAR(ACUSERECSELLOS, m_numOp ); //Hay que validar que se hace aqui si hay error...

            return;
         }
         else
         {
            Bitacora->escribePV(BIT_ERROR, "CSellosDigs.genAcuseRecepcion: No se pudo enviar el mensaje ACUSERECSELLOS al CERTISAT (error=%i)", errNum);

            return;  
         }
      }
      else
      {
         Bitacora->escribePV(BIT_ERROR, "CSellosDigs.genAcuseRecepcion: No se pudo crear el mensaje ACUSERECSELLOS (error=%i)", errNum);
      }
   }
   else
   {
      Bitacora->escribePV(BIT_ERROR, "CSellosDigs.genAcuseRecepcion: No se pudo obtener la fecha de operacion (error=%i)", error);
   }
   //}
   //else Bitacora->escribePV(BIT_ERROR, "Error al obtener el nombre del Contribuyente. (%i)", error);
   
   char serror[5];
   sprintf(serror, "%i", errNum);
   char sDesError[256];
   sprintf(sDesError, "Error durante la generaci�n del acuse. Su operaci�n sigue en proceso. (%s)", m_numTram);
   int i_DesError = strlen(sDesError);

   errNum = m_MsgCli.setMensaje(MSGERROR, "O", strlen("O"), serror, strlen(serror), sDesError, i_DesError);
   if (errNum)
   {
      Bitacora->escribePV(BIT_ERROR, "CSellosDigs.genAcuseRecepcion: No se pudo crear el mensaje MSGERROR (error=%i)", errNum);
   }

   errNum = envia(CERTISAT);
   if (errNum)
   {
      Bitacora->escribePV(BIT_ERROR, "CSellosDigs.genAcuseRecepcion: No se pudo enviar el mensaje MSGERROR al CERTISAT (error=%i)", errNum);
   }
   //AGZ: Fin
}

/* *********************************************************************** */
/* *********************************************************************** */
/* *********************************************************************** */
/* *********************************************************************** */
bool CSellosDigs::msgInicial()
{
   char   cNumPartes[4];
   string claves[] = { "PATH_TMPSDIG" }, spath_TmpSDig;	

   iNumPartes = sizeof(cNumPartes);
   m_ifirmaOp = MAX_TAM_FIRMA_B64; 
   m_irfcOp   = MAX_TAM_RFC;

   Bitacora->escribe(BIT_ERROR, "CSellosDigs: Recepci�n del mensaje INICIOTRANSSDG");

   error = m_MsgCli.getMensaje(INICIOTRANSSDG, m_firmaOp, &m_ifirmaOp, cNumPartes, &iNumPartes, m_rfcOp, &m_irfcOp);

   if (error)
   {
      Bitacora->escribe(BIT_ERROR, "CSellosDigs.msgInicial: No se pudieron obtener los datos del mensaje INICIOTRANSSDG");
      
      setVarsErr('I', error, "No se pudieron obtener los datos del mensaje INICIOTRANSSDG");
   }
   else
      error = m_Configuracion.getValoresConf("[AR_ALMACENA_SELLOS]", 1, claves, &spath_TmpSDig);

   if (!error)
   {
     // Verificaci�n de RFC bloqueado
     if ( GetRFCBloqueado(m_rfcOp) )
     {
         Bitacora->escribePV(BIT_ERROR, "CSellosDigs.msgInicial: El RFC %s esta bloqueado", m_rfcOp);
         Bitacora->escribePV(BIT_ERROR, "CSellosDigs.msgInicial: Sin efectos, de acuerdo a la regla I.2.2.4 %s:%s", m_rfcOp);
         
         setVarsErr('N', 1, "Sin efectos, de acuerdo a la regla I.2.2.4");
         msgCliError(m_errTipo, m_errDesc, m_errTipo, eAPL);
            
         return false;
     }
 
      iNumPartes = atoi(cNumPartes);
      SHA1_Init(&m_ctx);

      //- CreaDir ( (char *)spath_TmpSDig.c_str() ); //- GHM (070321)
      pkiCreaDir ( (char *)spath_TmpSDig.c_str() );   //+ GHM (070321): Para utilizar la funci�n definida en Sgi_PKI
      
      sprintf (cArchENS, "%s%s_XXXXXX", spath_TmpSDig.c_str(), m_rfcOp); // m_Configuracion.get("PATH_TMPSDIG")
      mkstemp(cArchENS);

      iFd = open(cArchENS, O_WRONLY | O_CREAT | O_TRUNC | O_APPEND, S_IRUSR | S_IWUSR);
      if (iFd == -1) 
      {
         Bitacora->escribePV(BIT_ERROR, "CSellosDigs.msgInicial: No se pudo crear el archivo temporal de recepci�n del requerimiento de sellos digitales: %s", cArchENS);

         error = sgiError(0,eERRNO,errno);

         return setVarsErr('I', error, "No se pudo crear el archivo temporal de recepci�n del requerimiento de sellos digitales");
      }

      Bitacora->escribePV(BIT_INFO, "CSellosDigs: Se cre� el archivo temporal de recepci�n del requerimiento de sellos digitales: %s", cArchENS);

      error = m_MsgCli.setMensaje(OPE_EXITO); //TODO: Aqu� al parecer es en donde se va a poner el mensaje de recepci�n de solicitud
      if (!error)
         error = envia(CERTISAT);

   }
   else
   {
      Bitacora->escribe(BIT_ERROR, "CSellosDigs.msgInicial: No se pudo obtener la variable de configuraci�n PATH_TMPSDIG");

      setVarsErr('I', error, "No se pudo obtener la variable de configuraci�n PATH_TMPSDIG");
   }

   return !error;
}
/* *********************************************************************** */
bool CSellosDigs::msgRecArch()
{
   char  cNumParte[4];
   int   iNumParte = sizeof(cNumParte);

   Bitacora->escribe(BIT_INFO, "CSellosDigs: Recepci�n de los mensajes PARTTRANSSDG");
   
   for (int i = 1, j=0; i <= iNumPartes; i++)
   {
      uint8 iCoderror = 0;
      static char cBufArch[sizeof(m_MsgCli.buffer)];
      int iBufArch = sizeof(cBufArch);
      //int iTamBufer = sizeof(m_MsgCli.buffer);
      iNumParte = sizeof(cNumParte);

      //intE error = m_SktProc->Recibe(m_MsgCli.buffer, iTamBufer); //>- ERGL mi� sep 12 16:35:38 CDT 2007
      intE error = m_MsgCli.Recibe(m_SktProc);
      if (!error) 
      {
         m_ifirmaOp = MAX_TAM_FIRMA_B64;
         error = m_MsgCli.getMensaje(PARTTRANSSDG,  m_firmaOp,  &m_ifirmaOp,
                                                    cNumParte , &iNumParte,
                                                    cBufArch  , &iBufArch);
      }
      if (error || (i != atoi(cNumParte)))
      {
         uint8 eFte  = sgiErrorFuente(error);
         int16 eBase = sgiErrorBase  (error);

         if (!error || (eFte == eSGISSL && eBase == ERR_SKTTIMEOUT)) 
            iCoderror = 9;
         else 
            iCoderror = 1;
      }
      else
      {
         if (write(iFd, cBufArch, iBufArch) != iBufArch)
         {
            Bitacora->escribe(BIT_ERROR, sgiError(0, eERRNO, errno), "CSellosDigs.msgRecArch: No se pudo escribir en el archivo temporal de recepci�n del requerimiento de sellos digitales");

            iCoderror = 9;
         }
      }

      sprintf(cNumParte, "%d", i);
      sprintf(m_firmaOp, "%d", iCoderror);

      error = m_MsgCli.setMensaje(EDOTRANSSDG, cNumParte, strlen(cNumParte), m_firmaOp, strlen(m_firmaOp));

      error = m_SktProc->Envia(m_MsgCli.buffer, m_MsgCli.tamMensaje()); 
         
      if ( iCoderror || error ) 
      { 
         if (iCoderror == 1 && j++ < 3) 
         { // Hasta 3 veces se puede mandar la parte i
            i--;
            continue;
         }

         Bitacora->escribePV(BIT_ERROR, "CSellosDigs.msgRecArch: No se pudo recibir la parte %d del requerimiento de sellos digitales", i);

         cerrarYeliminarArch();

         error = (!error) ? iCoderror : error;

         setVarsErr('I', error, "No se pudieron recibir las partes del requerimiento de sellos digitales");

         return error;
      }

      j = 0; // Si no hay problema la parte i  se envio bien
      SHA1_Update(&m_ctx, cBufArch, iBufArch);
   }
   
   close(iFd);
   iFd = -1;

   Bitacora->escribe(BIT_INFO, "CSellosDigs: Se recibiron las partes del requerimiento de sellos digitales");

   return !error;
}

/* *********************************************************************** */
bool CSellosDigs::msgFinArch()
{
   /*>- ERGL mi� sep 12 16:39:41 CDT 2007
   int iTamBufer = sizeof(m_MsgCli.buffer);
   error = m_SktProc->Recibe(m_MsgCli.buffer, iTamBufer);
   */
   Bitacora->escribe(BIT_INFO, "CSellosDigs: Recepci�n del mensaje FINTRANSSDG");

   error = m_MsgCli.Recibe(m_SktProc); 
   if (error || m_MsgCli.tipOperacion() != FINTRANSSDG)
   {  
      Bitacora->escribe(BIT_ERROR, "CSellosDigs.msgFinArch: No se pudo recibir el mensaje de fin de transmisi�n del archivo");

      cerrarYeliminarArch();

      return setVarsErr('I', error, "No se pudo recibir el mensaje de fin de transmisi�n del archivo");
   }

   //char cDigFte[30]; 
   int	idig_archivo = sizeof(dig_archivo), // iDigFte
      	iref_arch=sizeof(ref_arch);

   m_ifirmaOp = MAX_TAM_FIRMA_B64;
   error = m_MsgCli.getMensaje(FINTRANSSDG, m_firmaOp,   &m_ifirmaOp,	
                                            dig_archivo, &idig_archivo, 
                                            ref_arch,	 &iref_arch );
   if (error)
   {  
      Bitacora->escribe(BIT_ERROR, "CSellosDigs.msgFinArch: No se pudieron obtener los datos del mensaje FINTRANSSDG");

      cerrarYeliminarArch();

      return setVarsErr('I', error, "No se pudieron obtener los datos del mensaje FINTRANSSDG");
   }

   uchar uDigArch[SHA_DIGEST_LENGTH];
   SHA1_Final(uDigArch, &m_ctx);

   uint8	ui8DigFte[21];
   uint16	ui16DigFte = sizeof(ui8DigFte);
   B64_Decodifica(dig_archivo, idig_archivo, ui8DigFte, &ui16DigFte);

   if ( (error = memcmp(uDigArch, ui8DigFte, SHA_DIGEST_LENGTH)) != 0)
   {
      Bitacora->escribe(BIT_ERROR, "CSellosDigs.msgFinArch: La digesti�n recibida no coincide con la del archivo a procesar");

      cerrarYeliminarArch();

      setVarsErr('I', error, "La digesti�n recibida no coincide con la del archivo a procesar");
   }

   // 08/Jul/2005: Tenemos q implementar una validacion para verificar si el archivo .sdg esta correctamente ensobretado
   if ( !error && !valArchEns() ) //Se hace esta validacion p/NO mandar un No. d Operacion y por tanto un ACUSE en el CW
      cerrarYeliminarArch();

   /*
   // MELM Enero 30: Esto ya se hara en las 2 funciones genOperacion() y genTramite()
   // OjO: se guarda el Numero de Operacion PADRE q trae la operacion inicial y el tipo de Mensaje
   regBitacora(nSerie_, m_tipo, m_rfcOp, nombreReq_, alacCve_," "," ","0",""); // nombreReq_: no lleva es el proceso PADRE
   */
   Bitacora->escribe(BIT_INFO, "CSellosDigs: Se valid� el archivo ensobretado");

   return !error;	
}


// Busca los caracteres & y los escapa i.e les agrega el caracter de escape '\\' NO FUNCIONA, NO SE PORQUE
// suponemos cCadDes con el suficiente espacio
void CSellosDigs::sustituyeAmp(char *cCadSrc, char *cCadDes)
{
   memset(cCadDes, 0, sizeof(cCadDes)); // Solo limpia los 1eros 4 caracteres
   int i, iLen = strlen( cCadSrc );

   for (i = 0;i < iLen; i++) 
   {
      if (cCadSrc[i] == '&')  // Agrega los 2 caracteres
         strcat(cCadDes, "3"); //strcat(cCadDes, "\\&"); //strcat(cCadDes, "\x26");
      else // Agrega solo 1 caracter
         sprintf(cCadDes, "%s%c", cCadDes, cCadSrc[i]);
   }
   cCadDes[i+1]=0;
}


// Funcion para llamarse desde la funcion de Preproceso y descomprime los archivos .req
bool CSellosDigs::DescomprimeReqsSellos()
{  
   char      cArchZip[MAX_RUTA_ARCH], cRfcAux[30]; //, cInstruccion[2][2*MAX_RUTA_ARCH];
   string    claves[] = { "PATH_PROCESANDO", "PATH_PROCESADOS" }, spath_Procesando;
   string proceso;
   memset(&stMiNodo, 0, sizeof(nomzip));	
   memset(&cDirArchsCer, 0, sizeof(cDirArchsCer));

   Bitacora->escribe(BIT_INFO, "CSellosDigs: Descompresi�n del requerimiento de sellos digitales");

   // if ( m_Configuracion.open("arInicia.ini","AR_ALMACENA_SELLOS") ) { // Existe el arch. de configuraci�n
   if ( !( error = m_Configuracion.getValoresConf("[AR_ALMACENA_SELLOS]", 2, claves, &spath_Procesando, &spath_Procesados )) )
   {
      //- CreaDir ( (char *)spath_Procesando.c_str() );  //- GHM (070321)
      pkiCreaDir ( (char *)spath_Procesando.c_str() ); //+ GHM (070321): Para utilizar la funci�n definida en Sgi_PKI

     //- CreaDir ( (char *)spath_Procesados.c_str() ); //- GHM (070321)
      pkiCreaDir ( (char *)spath_Procesados.c_str() ); //+ GHM (070321): Para utilizar la funci�n definida en Sgi_PKI

      sprintf(cArchRen, "%s%s.sdg", spath_Procesando.c_str(), m_rfcOp ); // Las rutas traen al final el /  
      //sprintf(cArchRen, "%s%s_%s", spath_Procesando.c_str(),  m_rfcOp, m_numTram); En esta parte todavia m_numTram == '\0'
      sprintf(cArchZip, "%s%s.zip", spath_Procesando.c_str(), m_rfcOp );

      if ( !(error = rename(cArchENS, cArchRen)) ) 
      {  // renombro el archivo temporal al lugar q quiero
         // 1.- se renombra en FinARCH   
         // 2.- Amilkar le paso el archivo renombrado 	 RETORNA 1 vector de 1 s�lo elemento
         strcpy(cArchENS, cArchRen);	// Solo se movio de lugar, pero sigue siendo el archivo ensobretado
         proceso="SELLOS";

         if ( valArchEnsAutentico(cArchZip, proceso) ) 
         {
            // Se crean los directorios donde se dejan los archivos .req y los .cer
            sustituyeAmp(m_rfcOp, cRfcAux); // se hace esto por los RFCs con '&' Se cambia por el '3'
            sprintf(cDirArchsReq, "%s%s/", spath_Procesando.c_str(), cRfcAux); 
            sprintf(cDirArchsCer, "%s%s/", spath_Procesados.c_str(), cRfcAux); 
            //sprintf(cInstruccion[0],"mkdir -p %s", cDirArchsReq);
            //sprintf(cInstruccion[1],"mkdir -p %s", cDirArchsCer);
            Bitacora->escribePV(BIT_INFO, "CSellosDigs: cRfcAux=%s",      cRfcAux);
            Bitacora->escribePV(BIT_INFO, "CSellosDigs: cDirArchsReq=%s", cDirArchsReq);
            // CAMBIAR: nombre del directorio en lugar de guion bajo, guion normal ya q coincide con nombre del archivo

            //if (system(cInstruccion[0]) != -1 && system(cInstruccion[1]) != -1) // MAML errno == 17: Aun si el directorio existe, descomprime 
            if ( (!pkiCreaDir(cDirArchsReq) || errno == 17) && (!pkiCreaDir(cDirArchsCer) || errno == 17) )
            {
               //   "ABA900101ML2.pso" == Archivo de PRUEBAS
               pdescComp = new desCompresion(cArchZip, cDirArchsReq, &stMiNodo, error); // esta en ARunZip.h
               if ( pdescComp == NULL || error )
               {
                  //OjO: Que pasa si aqui ocurre un error => manda el acuse
                  //     pero cual error va a marcar en el CW (OjO: Probar)
                  Bitacora->escribe(BIT_ERROR, "CSellosDigs.DescomprimeReqsSellos: Error en la funci�n de desCompresion()");
                  setVarsErr('I', error, "Error en la funci�n de desCompresion()");
               }
            }	
            else 
            {
               error = errno; // OjO MELM
               Bitacora->escribe(BIT_ERROR, "CSellosDigs.DescomprimeReqsSellos: Error al crear los directorios para la descompresi�n de archivos del requerimiento");
               setVarsErr('I', error, "Error al crear los directorios para la descompresi�n de archivos del requerimiento");
            }
            if (!error) 
               unlink(cArchZip); // borra EL ARCh. COMPRIMIDO d REQUERIMIENTOS Q SE PROCESO BIEN
            // else con error DEJA TODOS los archivos COMPROMIDOS donde hubo ERROR en PATH_PROCESANDO

         } // end de valArchEnsAutentico
      }
      else 
      {
         Bitacora->escribePV(BIT_ERROR, "CSellosDigs.DescomprimeReqsSellos: No se puede renombrar el archivo .sdg :%s, errno %d", cArchENS,errno);
         sprintf (m_errDesc, "No se puede renombrar el archivo .sdg :%s, errno %d", cArchENS,errno);	
         setVarsErr('I', error, m_errDesc);
         unlink(cArchENS);  // BORRA EL ARCHIVO QUE NO SE PROCESO, por que no se pudo mover
      }	
   }
   else
   {
      Bitacora->escribe(BIT_ERROR, "CSellosDigs.DescomprimeReqsSellos: No se pudieron obtener los par�metros de configuraci�n PATH_PROCESANDO y PATH_PROCESADOS");

      setVarsErr('I', error, "No se pudieron obtener los par�metros de configuraci�n PATH_PROCESANDO y PATH_PROCESADOS");
   }

   Bitacora->escribePV(BIT_INFO, "CSellosDigs: Se descomprimi� el archivo del requerimiento de sellos digitales %s", cArchENS);

   return !error;
}


// OJO MELM : la class CGeneraCert necesita iniciado m_rfc
// --- empieza la generacion de requerimento�s
bool CSellosDigs::GeneraSellosDigs()
{
   char	   cArchivo[MAX_RUTA_ARCH], cInstruccion[2*MAX_RUTA_ARCH], cAuxil[MAX_RUTA_ARCH], *pPos;
   struct  nomzip  *stSigNodo;
   int     oper_secSellos, cReq = 0; // Contador d archs. reqs. q necesita la funcion AlmacenaReq()

   Bitacora->escribe(BIT_INFO, "CSellosDigs: Generaci�n de sellos digitales");

   // regBitacoraDetalle(ENSPROCESANDO);  // DEBE traer el n�mero de operacion Original
   if (( error = ValidaSujeto() ) != 0)
   {
      Bitacora->escribe(BIT_ERROR, "CSellosDigs.GeneraSellosDigs: La validaci�n del sujeto no permite generar sellos digitales");

      return !error; 
   }

   pListaNodo = NULL;	
   for( stSigNodo = &stMiNodo; stSigNodo != NULL; stSigNodo = stMiNodo.sig ) 
   { // !error && aunque haya error q intente mandar otros .req
      memcpy( &stMiNodo, stSigNodo, sizeof(nomzip));

      Bitacora->escribePV(BIT_INFO, "CSellosDigs: Nombre del archivo de requerimiento=%s", stMiNodo.nom); // para ver la depuraci�n

      // OjO MELM : Se graban los intentos de generar cada Sello Digital en la DB
      if ( (error = (!(regFileSellos_BDAR(stMiNodo.nom, &oper_secSellos)) != 0 )) != 0 )
      {
         sprintf (m_errDesc, "No se pudo registrar el archivo de sellos %s en la DB de la AR", stMiNodo.nom);
         Bitacora->escribePV(BIT_ERROR, "CSellosDigs.GeneraSellosDigs: %s", m_errDesc);
         setVarsErr('I', error, m_errDesc);
      }
		
      // Obtiene el nombre del archivo .req en una variable q no se usa
      memset(cArchivo, 0, sizeof(cArchivo));
      sprintf(cArchivo, "%s%s", cDirArchsReq, stMiNodo.nom);
      
      if (!error && !(error = leeReq(cArchivo)) && !(error = AlmacenaReq(cReq++, true)) &&
          !(error = ValidaReq()) && !(error = ApartaLlave()) && !(error = Genera())) 
      {
         // separamos el nombre y abrimos un nuevo archivo cer para escribirlo
         memset(cAuxil, 0, sizeof(cAuxil));
         memcpy(cAuxil, stMiNodo.nom, strlen(stMiNodo.nom)-strlen(pPos = strstr(stMiNodo.nom,".req")));
         sprintf(cInstruccion,"%s%s.cer", cDirArchsCer, cAuxil); //ponerle la extensi�n

         if ( guardaCertSelloDig(cInstruccion) )	
         {
            // va metiendo el nombre de este certificado en una variable con memoria 
            strcat(cInstruccion, "|"); // le agregamos el pipe
            if ( !(error = insertaNodo(cInstruccion)) ) // para despues hacer la compresion
               error = updateSellos_BDAR(oper_secSellos);
         }   
      }

      if (!error)
         m_utilBDAR->RegOperDetalleBDAR( CERTGEN, m_numOp);
      else 
      {   
         error = (error != DUPLICADOCERT && error != REQSELLOS_DATOSDIF_CERTENS) ? CERTNOGEN : error;
         m_utilBDAR->RegOperDetalleBDAR( MSGERROR, m_numOp, error); // MELM 071105: el SP sp_SegTramite necesita est� regto. p/detalles de sellos. 
      }   
      memset( stMiNodo.nom, 0, 50 ); // limpiamos solo la estructura donde se llena el nombre, p/siguiente archivo .req
   } // End del for

   return !error;
}


//Comprimir todo cDirArchsCer 
bool CSellosDigs::ComprimeSellosDigs()
{
   char	cInstruccion[2*MAX_RUTA_ARCH];

   Bitacora->escribe(BIT_INFO, "CSellosDigs: Compresi�n de sellos digitales");

   if ( pListaNodo ) 
   { 
      // Al menos se genero 1 certificado
      sprintf(cInstruccion,"%s%s.zip", spath_Procesados.c_str(), m_numTram);  // m_rfcOp, m_Configurac.get("PATH_PROCESADOS")
      
      if ( !(error = comprime( getListaCer(), cInstruccion, false) ) ) 
      { 
         if(!MarcasSellos())
         {
            Bitacora->escribePV(BIT_ERROR, "CSellosDigs.ComprimeSellosDigs: Error al crear la marca del archivo %s.zip", m_numTram);

            setVarsErr('I', error, "Error al crear la marca del archivo zip");
         }
         else
            Bitacora->escribe(BIT_INFO, "CSellosDigs: OPE_EXITO al menos se gener� un certificado de sello digital");
             // Solo si al menos se genero un certf. de Sello => se manda generar el Acuse de Sellos
             // YA TENGO LA FECHA DE OPERACION d TRANSCOMPLETA => PUEDO GENERAR EL ACUSE
         CAcuses m_acuse(m_BDAR, m_Configuracion, m_numOp, m_numTram );
         genAcuseSellos(&m_acuse);
             //AGZ: Quitamos el comentario...
      }
      else
      {
         Bitacora->escribe(BIT_ERROR, "CSellosDigs.ComprimeSellosDigs: Error en la funci�n que comprime los certificados de sellos digitales");
         setVarsErr('I', error, "Error en la funci�n que comprime los certificados de sellos digitales");
      }
      
      liberaNodos();
   }
   else 
   {  // No se GENERARON SELLOS 
      Bitacora->escribePV(BIT_ERROR, "CSellosDigs.ComprimeSellosDigs: No hay certificados de sellos digitales, error = %d, desc_error = %s", error, m_errDesc);
      setVarsErr('N', error, m_errDesc ); // Creo q ya la mando CGeneraCert.cpp => se puede repetir
      //setVarsErr('N', error, "No se genero ningun certificado de Sello Digital");
      //>>> MAML (070531): No es necesario reasignar el error puesto q las 3 etiquetas ya estan en la tabla 'cat_error_oper'  
      //if (error == DUPLICADOCERT || error == REQSELLOS_DATOSDIF_CERTENS)
      //   error = SDGYAPROCESADO;	
      //<<<
      //m_utilBDAR->RegOperDetalleBDAR( MSGERROR, m_numOp, SDGYAPROCESADO); // MELM 16/Enero/07: YA lo hace msgCliError()
   }

   // MAML: borra todos los archivos .REQ y .CER Generados o NO, en el destructor 
   //sprintf(cInstruccion, "rm -f %s/*.*;rm -f %s/*.*", cDirArchsReq, cDirArchsCer); 
   //system( cInstruccion );
   return !error;
}

// De todos los nombres de certificados guardados en una struc genera una sola cadena concatenada
char *CSellosDigs::getListaCer()
{
   LISTA_CERT	*pAux = NULL;
   //char		cListaCer[TAM_CERT]; // VOY A USAR UN STACK GRANDE // NO ACEPTA vars. LOCALES 

   memset(m_cert, 0, TAM_CERT); // OjO MELM : HABER SI NO TIENES PROBLEMAS POR USAR VARS. DE
   for (pAux = pListaNodo; pAux != NULL ; pAux = pAux->pNext)	// LA CLASE PADRE
      strcat( (char *)m_cert, pAux->cNomCer);

   m_cert[ strlen((const char *)m_cert)-1] = 0;   // Para quitarle el ultimo PIPE

   Bitacora->escribePV(BIT_DEBUG, "CSellosDigs: Lista de certificados de sellos digitales %s", (char *)m_cert);

   return ( (char *)m_cert );
}

// Guardamos cada archivo certificado en el directorio cDirArchsCer
bool CSellosDigs::guardaCertSelloDig(char *cArchCer)
{
   FILE	*pFile = NULL;	//	para guardar el certificado

   Bitacora->escribePV(BIT_INFO, "CSellosDigs: Guardado del certificado de sello digital %s", cArchCer);

   if ((pFile = fopen (cArchCer,"wb")) != NULL && (int)(fwrite (m_cert, 1, m_lcert, pFile)) == m_lcert ) 
   { 
      fclose (pFile); // El certificado temporal ha sido creado y cerrado
      pFile = NULL;
   }
   else 
   {
      error  = ERR_SAFE_CERT_SELLO;
      sprintf (m_errDesc, "No se pudo guardar el certificado temporal de sello digital %s", cArchCer);
      Bitacora->escribePV(BIT_ERROR, "CSellosDigs.guardaCertSelloDig: %s", m_errDesc);
      setVarsErr('I', error, m_errDesc);
   }

   return (!error );
}


// -----------------------------------------------------------------------------------------------------------
//	LA SIGUIENTE FUNCION ME AYUDA A GENERAR EL ACUSE DE SELLOS DIGS 
bool CSellosDigs::genAcuseSellos(CAcuses *pacuse)	
{
   char nameArch[MAX_RUTA_ARCH];

   if ( (error = pacuse->generaAcuse(TRANSCOMPLETA, SOLIC_CERT_SELLO_DIG, m_rfcOp, cNombreContrib, "", "", "",
		 "", MODULO_CERTISAT_WEB, m_tipo,	-1, quitaRutaArch(ref_arch, nameArch), dig_archivo) ) != 0 )
   {
      sprintf (m_errDesc, "No pudo genersarse el acuse de sellos digitales en la BD, numTramite=%s", m_numTram);
      Bitacora->escribePV(BIT_ERROR, "CSellosDigs.genAcuseSellos: %s", m_errDesc);
      setVarsErr('I', error, m_errDesc); // error = ERR_INS_ACUSE_DB;
   }
   else 
      Bitacora->escribePV(BIT_INFO, "CSellosDigs: OK se gener� acuse de sellos digitales en la AR, numTramite=%s, cad_orig=%s, firma=%s", 
                           m_numTram, pacuse->cad_orig, pacuse->firma);
   
   return (!error);
}
// -----------------------------------------------------------------------------------------------------------



/* *********************************************************************** */
// Registra nombre del file .req en la DB de la AR
int CSellosDigs::regFileSellos_BDAR(const char *NameFileREQ, int *oper_secSellos)
{	
   // Mandamos el store procedure de ROCIO para REQUERIMIENTOS de SELLOS DIGITALES
   bool ok_ejecuta = false;

   #if ORACLE
      ok_ejecuta = m_BDAR->ejecutaFuncion(SELLOS_SP_AGREGA_REQ_SELLO_NOMBRE, SELLOS_SP_AGREGA_REQ_SELLO_RETURN, SELLOS_SP_AGREGA_REQ_SELLO_PARAMS, m_numOp, NameFileREQ);
   #else
      ok_ejecuta = m_BDAR->ejecutaSP(SELLOS_SP_AGREGA_REQ_SELLO_NOMBRE, SELLOS_SP_AGREGA_REQ_SELLO_PARAMS, m_numOp, NameFileREQ);
   #endif

   return (ok_ejecuta && m_BDAR->getValores("i", oper_secSellos));
   // Segun ROCIO regresa 0 si hay Error o > 0 es el numero consecutivo
}


//m_numSerRenSellos OjO  stMiNodo.nom
int CSellosDigs::updateSellos_BDAR(int oper_secSellos)
{	
   if (!m_BDAR->ejecutaOper(SELLOS_ACTUALIZA_REQ_SELLO, m_numSerRenSellos, m_numOp, oper_secSellos) )
   {
      error =	ERR_UPDATE_REQS_SELLOS;
      sprintf (m_errDesc, "No se actualiz� el n�mero de serie='%s' en la tabla reqs_sellos con nombre_req='%s'", m_numSerRenSellos, stMiNodo.nom);
      Bitacora->escribePV(BIT_ERROR, "CSellosDigs.updateSellos_BDAR: %s", m_errDesc);
      setVarsErr('I', error, m_errDesc);
   }
   return (error);
}




/* **************************************************************************************** */
/* **************************************************************************************** */
/* **************************************************************************************** */
bool CSellosDigs::MarcasSellos()
{
   error = -1;
   char        archivo[300];
   char        completa[256];
   static char cRutaFTP[265];
   int         desc;
   string      sRuta;
   string      sNumFTP;

   memset(completa,0,sizeof(completa));
   memset(cRutaFTP ,0,sizeof(cRutaFTP));

   sprintf(archivo,"%s.zip", m_numTram);

   Bitacora->escribePV(BIT_INFO, "CSellosDigs: MarcasSellos %s", m_numTram);

   error = m_Configuracion.getValorVar("[AR_ALMACENA_SELLOS]","MARCAS"  ,&sRuta);
   if(error)
   {
      Bitacora->escribe(BIT_ERROR, "CSellosDigs.MarcasSellos: No se pudo obtener la variable de configuraci�n MARCAS");

      return false;
   }

   error = m_Configuracion.getValorVar("[AR_FTP]","NUMFTP"  ,&sNumFTP);
   if(error)
   {
      Bitacora->escribe(BIT_ERROR, "CSellosDigs.MarcasSellos: No se pudo obtener la variable de configuraci�n NUMFTP");
      
      return false;
   }
      

   for(int i = 1 ; i< atoi(sNumFTP.c_str())+1 ;i++)
   {
        memset(cRutaFTP,0,sizeof(cRutaFTP));
        NombreRuta(i,(char*)sRuta.c_str(),cRutaFTP);
        mkdir(cRutaFTP , 0774);
        sprintf(completa,"%s%s",cRutaFTP,archivo);

        desc = open(completa,S_IRWXU);
        if(!desc)
        {
           Bitacora->escribePV(BIT_ERROR, "CSellosDigs.MarcasSellos: No se pudo crear la marca de sellos %s, error %d", m_numTram, errno);
           
           return false;
        }

        close(desc);
   }

   return true;
}

bool CSellosDigs:: NombreRuta(int iNumFTP,char* cRutaIni,char* cRutaFin)
{

   strcpy(cRutaFin,cRutaIni);

   if(iNumFTP< 10)
   {
      strcat(cRutaFin,"FTPPUB0");
      sprintf(cRutaFin + strlen(cRutaFin),"%d/",iNumFTP);
   }
   else
   {
      strcat(cRutaFin,"FTPPUB");
      sprintf(cRutaFin + strlen(cRutaFin),"%d/",iNumFTP);
   }
   return 0;

}
//############################################################################################################
// 080827: Borra un directorio tomando solo la ruta. retorna 0 si OK else errno
int CSellosDigs::borraDir(const char *ruta)
{
   return ( rmdir(ruta) ); // <<-- errno   NOTA: solo borra los directorios vacios
}

//############################################################################################################
// 080827: Borra todos los archivos de un directorio dado
int CSellosDigs::borraArchivos(const char *ruta)
{
   DIR    *direc;
   struct dirent *lista_nom;
   char   borra[2*MAX_RUTA_ARCH];

   direc = opendir( ruta );
   if(direc == NULL)
   {
      Bitacora->escribePV(BIT_ERROR, "CSellosDigs.borraArchivos: Error al abrir el directorio=%s, errno=%d", ruta, errno);
      return -1;
   }

   while((lista_nom = readdir(direc))!= NULL )
   {
      if( !strcmp(lista_nom[0].d_name,".") )
        continue;
      if( !strcmp(lista_nom[0].d_name,".."))
        continue;
      sprintf(borra,"%s%s", ruta, lista_nom[0].d_name );
      unlink(borra);
   } // END del WHILE

   closedir(direc);
   direc = NULL;
   lista_nom = NULL;
   //Bitacora->escribePV(BIT_DEBUG, "OjO: valor final errno=%d en borraArchivos()", errno);
   
   return 0; // (errno != 0) ?; 
}
