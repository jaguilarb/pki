static const char* _CCONSULTACERT_CPP_VERSION_ ATR_USED = "SrvAR @(#)"\
"DSIC10275AR_ 2010-07-07 CConsultaCert.cpp 1.1.4/5";

//#VERSION: 1.1.2
// V.1.1.3 (100412) GHM: se modifica la forma de llamar al query ya que no se ejecutaba con un nombre de 255 caracteres

// V.1.1.1.1 (080424) SERR: Se agrego informacion a la bitacora
#include <CConsultaCert.h>
//- #include <Sgi_Pki.h> //- GHM (070321): Se modific� el nombre de la librer�a
#include <Sgi_PKI.h> //+ GHM (070321)

int CConsultaCert::m_MsgConsulta = -1;

CConsultaCert::CConsultaCert(CConfigFile& config, MensajesSAT& msgcli, CSSL_Skt* skt):
   COperacion(config, msgcli, skt)
{
   m_byCert = NULL;
   m_ibyCert = 0;
   iniciaVarsCons();
}

CConsultaCert::~CConsultaCert()
{
   if(m_byCert)
      delete[] m_byCert;
   m_byCert = NULL;
   m_ibyCert = 0;
   terminaVarsCons();
   m_MsgConsulta = -1;
}

bool CConsultaCert::preProceso()
{
   int error = 0; 
   bool ok = false;
         
   Bitacora->escribePV(BIT_INFO, "CConsultaCert en preProceso: Se inicializan las variables de consulta %s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",m_rfcCon, 
   m_firmaCon, m_numSer, m_fec_ini, m_fec_fin,m_bandera,m_Dato, m_agc,m_irfcCon,m_ifirmaCon,m_inumSer,m_ifec_ini,m_ifec_fin,m_ibandera,m_iDato,m_iagc);
   iniciaVarsCons();
   switch(getTipOper())
   {
      case VERCERTNOSER:
      case VERCERTRFC:
      {
         error = sgiErrorBase(m_MsgCli.getMensaje(VERCERTNOSER, m_firmaCon, &m_ifirmaCon, m_rfcCon, &m_irfcCon, 
                              m_Dato, &m_iDato));
         if(m_MsgConsulta == VERCERTNOSER)
            sprintf(m_numSer, "%s", m_Dato);
         // V.1.1.1.1 (080424) SERR: Se agrego informacion a la bitacora
         Bitacora->escribePV(BIT_INFO, "CConsultaCert en preProceso: Datos ( %s , %s )",m_rfcCon,m_Dato);
         break;
      }
      default:
      {
         msgCliError('I', "Error en CConsultaCert en preProceso: Error solicitud no reconocida.", ERR_OP_NO_RECONOCIDA, eAPL);
         break;
      }
   }
   if(!error)
      ok = true;
   else
       msgCliError('I', "Error en CConsultaCert en preProceso: Error al obtener el Mensaje del CERTISAT.", error, eMSGSAT);
   return ok;   
}

bool CConsultaCert::Proceso()
{
   int error = 0;
   bool ok = false;
   if(!m_BDAR->ejecutaSP(CONS_CERT_SP_REGISTRA_OPER_NOMBRE, CONS_CERT_SP_REGISTRA_OPER_PARAMS, m_numOp, m_MsgConsulta, 0))
      escribeBitacora(eAPL, ERR_BD_REGOPERDET, "CConsultaCert en Proceso: Error al registrar la operacion en la BD.");
   error = msgProceso(m_MsgConsulta);
   if(!error)
      ok = true;
   else if(error == ERR_NO_EXISTE_REG)
      msgCliError('N', "Error en CConsultaCert en Proceso: Error no existen datos del Certificado Digital.", error, eAPL);
   else if(error == ERR_RFC_NO_COIN)
      msgCliError('N', "Error en CConsultaCert en Proceso: Error el n�mero de serie no corresponde con el rfc.", error, eAPL);
   else if (error == ERR_NO_EXISTE_NSERIE)
      msgCliError('N', "Error en CConsultaCert en Proceso: El n�mero de serie no es v�lido.", error, eAPL);
   // >>> ERGL ( 070105 )
   else if (error == ERR_NO_EXISTE_RFC)
      msgCliError('N', "Error en CConsultaCert en Proceso: El RFC no est� registrado en la base de datos.", error, eAPL);
   // <<<
   //>+ GHM (080417): Se env�a un mensaje espec�fico para el caso de no encontrar el certificado en el repositorio
   else if (error == ERR_NO_OBT_CERT)
      msgCliError('N', "Error en CConsultaCert en Proceso: El Certificado no esta disponible, solicite apoyo a su �rea central.", error, eAPL);
   //<+ GHM (080417)
   else
      msgCliError('I', "Error en CConsultaCert en Proceso: Error al procesar el mensaje.", error, eAPL);
   return ok;   
}

const char* CConsultaCert::getNombreOperacion()
{
   getTipOper();
   switch(m_MsgConsulta)
   {
      case VERCERTNOSER:{return "Consulta de Certificado Digital por numero de serie.";}
      case VERCERTRFC:{return "Consulta de Certificado Digital por RFC.";}
      default:{return NULL;}
   }
}

int CConsultaCert::getTipOper()
{
   if (m_MsgConsulta == -1)
      m_MsgConsulta = m_MsgCli.tipOperacion();
   return m_MsgConsulta;
}

int CConsultaCert::msgProceso(int i_op)
{
   int error = 0;
   bool b_primero = false;
   string s_resQueryTmp, s_rfcTmp;
   char psz_Query[1024*30];
   psz_Query[0] = 0;
   
   switch(i_op)
   {
      case VERCERTNOSER:
      {
         Bitacora->escribePV(BIT_INFO, "CConsultaCert en MsgProceso: Verificaci�n del certificado por n�mero de serie");
         if (m_rfcCon[0] == '.')
         {
            if (m_BDAR->consultaReg(CONS_CERT_OBTEN_CADENA_DATOS_CERTIFICADO_POR_NUM_SERIE_1, m_Dato))
            {
               if (!m_BDAR->getValores("s", &s_resQueryTmp))
               {  
                  escribeBitacora(eAPL, ERR_NO_EXISTE_NSERIE, "Error en CConsultaCert en MsgProceso: Error al consultar datos del Certificado Digital.(edo_cer|"
                  "tipcer_cve|rfc)"); 
                  return ERR_NO_EXISTE_NSERIE;
               }               
            } else Bitacora->escribe(BIT_ERROR, "Error en CConsultaCert en MsgProceso: Error al consultar la cadena de datos por el n�mero de serie 1");
         }
         else 
         {
            if (m_BDAR->consultaReg(CONS_CERT_OBTEN_CADENA_DATOS_CERTIFICADO_POR_NUM_SERIE_2, m_Dato, m_rfcCon))
            {
               if (!m_BDAR->getValores("ss", &s_resQueryTmp, &s_rfcTmp))
               {
                  escribeBitacora(eAPL, ERR_NO_EXISTE_NSERIE, "Error en CConsultaCert en MsgProceso: Error al consultar datos del Certificado Digital.(edo_cer|"
                  "tipcer_cve)");
                  return ERR_NO_EXISTE_NSERIE;
               }
               if (strncmp(s_rfcTmp.c_str(), m_rfcCon, s_rfcTmp.length()) != 0)
               {
                   escribeBitacora(eAPL, ERR_RFC_NO_COIN, "Error en CConsultaCert en MsgProceso: Error el n�mero de serie no corresponde con el rfc.");
                   return ERR_RFC_NO_COIN;
               }
            }else Bitacora->escribe(BIT_ERROR, "Error en CConsultaCert en MsgProceso: Error al consultar la cadena de datos por el n�mero de serie 2");
         }
         error = getCertificado();
         if (!error)
         {
            error = sgiErrorBase(m_MsgCli.setMensaje(DATCERTNUMSERIE, (char*)s_resQueryTmp.c_str(), s_resQueryTmp.length(), 
                                 m_byCert, m_ibyCert));
            if(!error)
               break;
            else
            {
               escribeBitacora(eMSGSAT, error, "Error en CConsultaCert en MsgProceso: Error al armar el mensaje.(DATCERTNUMSERIE)");
               return ERR_SET_MSG;
            }
         }
         else
         {
            string sError = string("Error en CConsultaCert en MsgProceso: Error al obtener el Certificado Digital del repositorio, num_serie=") + m_Dato;
            escribeBitacora(eAPL, error, sError.c_str() ); 
            return ERR_NO_OBT_CERT;
         }
         break;
      }
      //GHM(100412): Se detecto que la operaci�n de concatenaci�n (||) en el query marca error cuando la cadena final es mayor a 255 caracteres.
      //             Se divide la extracci�n de datos con respecto al nombre.
      case VERCERTRFC:
      {
         Bitacora->escribePV(BIT_INFO, "CConsultaCert en MsgProceso: Verificaci�n del certificado por RFC");
         int i_tam = 0;
         string s_numSerRev;
         //+ GHM(100412)
         string s_resQueryTmp1, s_resQueryTmp2;
         if (m_Dato[0] == '0')
         {
            if(m_BDAR->consultaReg(CONS_CERT_OBTEN_ULT_NUM_SERIE_DE_FIEL_POR_RFC, m_rfcCon))
            {
               if (m_BDAR->getValores("s", &s_numSerRev))
               {
                  sprintf(m_numSer, "%s", s_numSerRev.c_str());
                  error = getCertificado();
                  if (!error)
                  {
                     /* >- GHM(100412)
                     if (m_BDAR->consultaReg("select no_serie||'|'||edo_cer||'|'||vig_ini||'|'||vig_fin||'|'||tramite_cve"
                         "||'|'||fec_reg_ies||'|'||nombre||'|'||tipcer_cve from certificado where no_serie = '%s'"
                         " and rfc = '%s';", m_numSer, m_rfcCon))
                     {
                        if (m_BDAR->getValores("s", &s_resQueryTmp)) 
                        {
                     <- GHM(100412)*/
                     // >+ GHM(100412)
                     if (m_BDAR->consultaReg(CONS_CERT_OBTEN_CADENA_DATOS_CERTIFICADO_POR_NUM_SERIE_Y_RFC, m_numSer, m_rfcCon))
                     {
                        if (m_BDAR->getValores("sss", &s_resQueryTmp, &s_resQueryTmp1, &s_resQueryTmp2))
                        {
                           s_resQueryTmp = s_resQueryTmp + "|"+ s_resQueryTmp1 +"|"+s_resQueryTmp2;
                     // >+ GHM(100412)
                           error = sgiErrorBase(m_MsgCli.setMensaje(DATCERTRFC, m_numOp, strlen(m_numOp),
                           (char*)s_resQueryTmp.c_str(), s_resQueryTmp.length(), m_byCert, m_ibyCert));
                           if(!error)
                              break;
                           else
                           {
                              escribeBitacora(eMSGSAT, error, "Error en CConsultaCert en MsgProceso: Error al armar el mensaje.(DATCERTNUMSERIE)");
                              return ERR_SET_MSG;
                           }
                        }
                        else 
                        {
                           escribeBitacora(eAPL, ERR_NO_CONS_BD, "Error en CConsultaCert en MsgProceso: Error al consultar datos del Certificado Digital.");
                           return ERR_NO_CONS_BD;
                        }
                     }
                     else 
                     {
                        Bitacora->escribePV(BIT_ERROR, "Error en CConsultaCert en MsgProceso: Error al consultar los valores %s, %s en la BD.", m_numSer,m_rfcCon);
                        return ERR_NO_CONS_BD;
                     }
                  }
                  else
                  {
                     string sError = string("Error en CConsultaCert en MsgProceso: Error al obtener el Certificado Digital del repositorio, num_serie=") + m_numSer;
                     escribeBitacora(eAPL, error, sError.c_str() );
                     return (error==2)?ERR_NO_OBT_CERT:error;
                  }
               }
               else 
               {
               /*>- ERGL (070108)
                  escribeBitacora(eAPL, ERR_NO_CONS_BD, "Error al consultar numero de serie del Certificado Digital.");
                  return ERR_NO_CONS_BD;
               -<*/
               //>>> ERGL (070108)
	          escribeBitacora(eAPL, ERR_NO_EXISTE_RFC, "Error en CConsultaCert en MsgProceso: Error al consultar numero de serie del Certificado Digital.");
                  return ERR_NO_EXISTE_RFC;
               //<<<
               }
            }
            else
            {
               escribeBitacora(eAPL, ERR_NO_CONS_BD, "Error en CConsultaCert en MsgProceso: Error al consultar la BD.)");
               return ERR_NO_CONS_BD;
            }
         }
         else 
         {
            /* >- GHM(100412)
            if (m_BDAR->consulta("select no_serie||'|'||edo_cer||'|'||vig_ini||'|'||vig_fin||'|'||tramite_cve||'|'||"
                "fec_reg_ies||'|'||nombre||'|'||tipcer_cve||'|' from certificado where rfc = '%s' order by no_serie desc;", m_rfcCon))
            <- */
            // >+ MAML(100707): falto la cadena "and tipcer_cve < 3 " en este query.  GHM(100412)
            if (m_BDAR->consulta(CONS_CERT_OBTEN_CADENA_DATOS_FIEL_POR_RFC, m_rfcCon))
            // <+ GHM(100412)
            {
               for(int i = 0; i < 20; i++)
               {
                  if (m_BDAR->sigReg())
                  {
                     /* >- GHM(100412)
                     if (m_BDAR->getValores("s", &s_resQueryTmp))
                     {
                     <- GHM(100412)*/
                     // >+ GHM(100412)
                     if (m_BDAR->getValores("sss", &s_resQueryTmp, &s_resQueryTmp1, &s_resQueryTmp2))
                     {
                        s_resQueryTmp = s_resQueryTmp + "|" + s_resQueryTmp1 + "|" + s_resQueryTmp2;
                     // <+ GHM(100412) 
                        if(!b_primero)
                        {
                           strcpy(psz_Query, s_resQueryTmp.c_str());
                           b_primero = true;
                        }
                        else
                            strcat(psz_Query, s_resQueryTmp.c_str());
                     }
                     else
                     {
                        /*>- ERGL (070108)
                        escribeBitacora(eAPL, ERR_NO_CONS_BD, "Error al consultar datos del Certificado Digital.");
                        return ERR_NO_CONS_BD;
                        -<*/
                        //>>> ERGL (070108)
	                escribeBitacora(eAPL, ERR_NO_EXISTE_RFC, "Error en CConsultaCert en MsgProceso: Error al consultar los datos del Certificado Digital.");
                        return ERR_NO_EXISTE_RFC;
                        //<<<
                     }
                  }
                  else if (psz_Query[0] == 0)
                  {
                     escribeBitacora(eAPL, ERR_NO_CONS_BD, "Error en CConsultaCert en MsgProceso: Error no hay datos del Certificado Digital.");
                     return ERR_NO_EXISTE_REG;
                  }
                  else break;
               }
               i_tam = strlen(psz_Query);
               if(psz_Query[i_tam] == '|')
                  psz_Query[i_tam] = 0;
               error = sgiErrorBase(m_MsgCli.setMensaje(DATCERTRFC, m_numOp, strlen(m_numOp), psz_Query,
                        strlen(psz_Query), "NULL", 4));
               if(!error)
                  break;
               else 
               {
                  escribeBitacora(eMSGSAT, error, "Error en CConsultaCert en MsgProceso: Error al armar el mensaje.(DATCERTRFC)");
                  return ERR_SET_MSG;
               }
            }
            else 
            {
               escribeBitacora(eAPL, ERR_NO_CONS_BD, "Error en CConsultaCert en MsgProceso: Error al consultar la BD.");
               return ERR_NO_CONS_BD;
            }
         }
         break;
      }
   }
   error = envia(CERTISAT);
   if(!error)
      return 0;
   else 
   {
      escribeBitacora(eSGISSL, error, "Error en CConsultaCert en MsgProceso: Error al enviar el Mensaje al CERTISAT.");
      return error;
   }
}

int CConsultaCert::getCertificado(void)
{
   int error = 0;
   string s_repositorio;
   string s_nSerie = m_numSer;
   ManArchivo fd;
   
   Bitacora->escribePV(BIT_INFO, "CConsultaCert en getCertificado: Se ejecutan el m�todo getValores para obtener los valores %s, %s, %s", s_repositorio, s_nSerie, 
   m_pathCerts);
   
   error = m_Configuracion.getValorVar(TAG_PATH_CERTS, PATH_REP_CERTS, &s_repositorio);
   if (error != 0)
    {
      Bitacora->escribePV(BIT_ERROR, "Error en CConsultaCert en getCertificado: Error al obtener los valores %s, %s, %s", s_repositorio, s_nSerie, m_pathCerts);
      return error;
    }
   error = sgiErrorBase(pkiRutaCert(s_repositorio, s_nSerie, m_pathCerts, false));
   if (!error)
   {
       if (fd.TamArchivo((char*)m_pathCerts.c_str(), &m_ibyCert))
       {
          m_byCert = new uchar[m_ibyCert];
          if (m_byCert)
          { 
             if (fd.ProcesaArchivo((char*)m_pathCerts.c_str(), m_byCert, &m_ibyCert))
                return error;
          }else Bitacora->escribe(BIT_ERROR, "Error en CConsultaCert en getCertificado: No se cre� el objeto m_byCert"); 
          error = errno;
          return error;
       }
       error = errno;
       return error;
   }
   return error;
}

void CConsultaCert::iniciaVarsCons(void)
{
   m_rfcCon[0]   = 0; 
   m_firmaCon[0] = 0;
   m_numSer[0]   = 0;
   m_fec_ini[0]  = 0;
   m_fec_fin[0]  = 0;
   m_bandera[0]  = 0;
   m_Dato[0]     = 0;
   m_agc[0]      = 0;
   m_irfcCon     = sizeof(m_rfcCon);
   m_ifirmaCon   = sizeof(m_firmaCon);
   m_inumSer     = sizeof(m_numSer);
   m_ifec_ini    = sizeof(m_fec_ini);
   m_ifec_fin    = sizeof(m_fec_fin);
   m_ibandera    = sizeof(m_bandera);
   m_iDato       = sizeof(m_Dato);
   m_iagc        = sizeof(m_agc);
}


void CConsultaCert::terminaVarsCons(void)
{
   m_firmaCon[0] = 0;
   m_numSer[0]   = 0;
   m_fec_ini[0]  = 0;
   m_fec_fin[0]  = 0;
   m_bandera[0]  = 0;
   m_Dato[0]     = 0;
   m_agc[0]      = 0;
   m_irfcCon     = 0;
   m_ifirmaCon      = 0;
   m_inumSer     = 0;
   m_ifec_ini    = 0;
   m_ifec_fin    = 0;
   m_ibandera    = 0;
   m_iDato       = 0;
   m_iagc        = 0;
}

