/*##############################################################################
  ###  PROYECTO:              PKI-SAT                                        ###
  ###  MODULO:                Servicio AR                                    ###
  ###                                                                        ###
  ###                                                                        ###
  ###  DESARROLLADORES:       Silvia Euridice Rocha Reyes     RORS           ###
  ###                                                                        ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       Martes 11, noviembre del 2008                  ###
  ###                                                                        ###
  ##############################################################################
         1         2         3         4         5         6         7         8
12345678901234567890123456789012345678901234567890123456789012345678901234567890

################################################################################
  VERSION:
     V.1.0.0  (20081111- )    RORS: Version Original
                              Servicio de Autoridad Registradora para la PKI-SAT

#################################################################################*/
#ifndef _CDATOSRFC_H_
#define _CDATOSRFC_H_
static const char* _CDATOSRFC_H_VERSION_ ATR_USED = "SrvAR @(#)"\
"DSIC09041AR_ 2009-01-29 CDatosRFC.h 1.1.0/2";

//#VERSION: 1.1.0

#include <Sgi_LDAP.h>
#include <Sgi_Dario.h>
#include <Sgi_Bitacora.h>
#include <Sgi_ConfigFile.h>
#include <Sgi_ProtegePwd.h>
#include <string>
#include <CSolDatosWS_IDC.h>

#define ERROR   1
#define ERROR_OBT_DATOS 2
#define ERROR_OBT_DN    3
#define ERROR_DATOS_NULL 4



class CDatosRFC
{
   private:
   
      CSolDatosWS_IDC * valWS;
      Sgi_LDAP        * ldap;
      Sgi_Dario       * dario;
      CConfigFile&      archConf;
  
      std::string sIPLDAP;
      std::string sPuertoLDAP;
      std::string sUsuarioLDAP;
      std::string sPasswordLDAP;
      std::string sRutaBaseLDAP;
      std::string sRutaBaseUsuLDAP;
  
      std::string sUsuarioWS;
      std::string sPasswordEncWS;
      std::string sPasswordWS;
      std::string sTipoMensWS;
      std::string sConHTTPSWS;
      std::string sCertWS;

      std::string sServDARIO;
      std::string sBDDARIO;
      std::string sUsuarioDARIO;
      std::string sPasswordDARIO;
      std::string sRoleDARIO;
    
      std::string sRFC;

      int iTipCert;
      int iError;

      int   ObtieneConfiguracionLDAP();
      int   ObtieneConfiguracionWS();
      int   ObtieneConfiguracionDARIO();

   public:
      std::string sTieneBiom;

      CDatosRFC(const std::string &sRFCc, CConfigFile& config, int iTipoCert);
     ~CDatosRFC();   

      int  ConectaWS();
      int  ConectaLDAP();
      int  ConectaDARIO();
      int  ObtieneDatosWS();
      int  ObtieneDatosLDAP();
      int  ObtieneDatosDARIO();
      //void DesconectaLDAP();


      std::string getTipContrib();
      std::string getNombre();
      int  getSitDomicilio();
      std::string getDomicilio();
      std::string getSitFiscal();
      std::string getTieneBiom();
      std::string getDescSitDom();
      std::string getRFCOriginal();
      std::string getCURP();
      std::string getDescSitFis();


      bool getReg(int indice, std::string *Regimen, bool *Activo );
      bool getObl(int indice, std::string *Obligacion, bool *Activo );
      bool getRole(int indice, std::string *Rol, bool *Activo );
      bool getAct (int indice, std::string *Activ, bool *Activo );

      int getsNumRegs();
      int getsNumObligs();
      int getsNumRoles();
      int getsNumActivs();
      int getsStatusRFC();
};

extern CBitacora* Bitacora;

#endif //_CDATOSRFC_H_
