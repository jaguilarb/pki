static const char* _COPERACION_CPP_VERSION_ ATR_USED = "SrvAR @(#)"\
"DSIC10294AR_ 2010-08-02 COperacion.cpp 1.1.2/3";

//#VERSION: 1.1.1
/*##############################################################################
  ###  PROYECTO:              PKI-SAT                                        ###
  ###  MODULO:                Servicio AR                                    ###
  ###                                                                        ###
  ###                                                                        ###
  ###  DESARROLLADORES:       Amilcar Guerrero Zuñiga         AGZ            ###
  ###                                                                        ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       Lunes 5, diciembre del 2005                    ###
  ###                                                                        ###
  ##############################################################################
         1         2         3         4         5         6         7         8
12345678901234567890123456789012345678901234567890123456789012345678901234567890

################################################################################
  VERSION:
     V.1.0.0  (20051130- )    AGZ: Version Original
                              Servicio de Autoridad Registradora para la PKI-SAT
     V.1.1.1  (20080827- )    GHM: Se modifico la generación del número de trámite
                                   y de operación para no aceptar VALIDO
     V.1.1.2  (20100802- )    MAML: Se quita una llamada de msgCliError() de la 
                                   funcion general Operacion()
     V.1.1.3  (20141014 -         ) JAB: Se agrupan las sentencias SQL utilizadas en el SrvAR en un archivo de
                                              cabecera

#################################################################################*/

#include <COperacion.h>
#include <SrvAR.h>

//##################################################################################
char  COperacion:: m_agcCve[9];
//##################################################################################
COperacion::COperacion(CConfigFile& config, MensajesSAT& msgcli, CSSL_Skt* skt, bool tramite) :
   m_Configuracion(config), m_MsgCli(msgcli), m_SktProc(skt)
{
   m_BDAR      = NULL;
   m_msgMed    = NULL;
   m_fifoMed   = NULL;
  
   m_firmaOp   = NULL;
   m_rfcOp     = NULL;
   m_cadOriOp  = NULL;
   m_refArchOp = NULL;
   m_secOp     = NULL;
   m_utilBDAR  = NULL;

   m_numOp  [0] = 0;
   m_numTram[0] = 0;

   m_tramite = tramite;
}
//##################################################################################
COperacion::~COperacion()
{
   if (m_utilBDAR)
      delete m_utilBDAR;
   if (m_BDAR)
      delete m_BDAR;
   if (m_msgMed)
      delete m_msgMed;
   if (m_fifoMed)
      delete m_fifoMed;
   m_BDAR     = NULL;
   m_msgMed   = NULL;
   m_fifoMed  = NULL;
   m_utilBDAR = NULL;
   if (m_tramite)
      terminaVarsTram();
}
//##################################################################################
bool COperacion::Operacion()
{
   bool ok = false;
   char edo_agc[2], edo_agcCert[2];

   memset(edo_agc,0,2);
   memset(edo_agcCert,0,2);
   
   Bitacora->escribePV(BIT_INFO, "COperacion: Inicio de la Operación: %s", getNombreOperacion());

   int base_datos = AR_BD;

   #if ORACLE
      base_datos = AR_BD_ORACLE;
   #endif

   if (conectaBD(base_datos))
   {
      m_utilBDAR = new CUtilBDAR;
      if (m_utilBDAR->Inicializa(m_BDAR))
      {
         if ( m_tramite && iniciaVarsTram() )
            msgCliError('I', "COperacion.Operacion: No se pudieron iniciar las variables de la operación", ERR_ASG_MEM_VAR, eAPL);
         else if (preProceso())
         {
            if (m_utilBDAR->getEdoAgCyCert(m_agcCve, edo_agc, edo_agcCert))
            {
               if (edo_agc[0] != 'A')
                  msgCliError('N', "COperacion.Operacion: El agente certificador no cuenta con privilegios para accesar al sistema", ERR_EDO_AGC, eAPL); 
               else if (edo_agcCert[0] != 'A')
                   msgCliError('N', "COperacion.Operacion: El certificado del agente certificador no está activo", ERR_EDO_CERT_AGC, eAPL);
               else
               {
                  if (genOperacion())
                  {
                     if (m_tramite)
                        ok = genTramite() && envTramite();
                     else 
                        ok = true;
                     if (ok)
                        ok = Proceso();
                     
                     if (m_tramite)
                        if (!m_utilBDAR->RegOperDetalleBDAR(FINDETRAMITE, m_numOp))
                           Bitacora->escribe(BIT_ERROR, "COperacion.Operacion: No se pudo registrar el fin de trámite en la BD");
                  }
               }
            }
            else
               msgCliError('I', "COperacion.Operacion: No se tienen los datos del agente certificador" , ERR_EDO_AGC, eAPL);
         }
         //-->> MAML 20100730: siempre en todas las funciones de preProceso() debe terminar con esta función cuando hay error
         //                    y no mandarlo aqui por que se puede dar el caso de mandar mensajes de mas al Certisat                      
         /* else 
             msgCliError('I', "Error en Preproceso" , ERR_EDO_AGC, eAPL); */
         //--<<
      } // Inicializa
      desconectaBD(base_datos);
   }
   Bitacora->escribePV(BIT_INFO, "COperacion: Fin de Operación: %s", getNombreOperacion());

   return ok;
}
/*##############################################################################
   PROPOSITO: Esta funcion establece la conexion con la BD.

   PREMISA:   Pasar la constante AR_DB.
   EFECTO:    No tiene efecto sobre el ambiente.
   ENTRADAS:  id_db: Identificador de la base de datos a la que se va a conectar
   SALIDAS:   Regresa: 0 -> Exito o un codigo de error.
#################################################################################*/

bool COperacion::conectaBD(int id_db)
{
   string s_srv, s_db, s_role;    // Informix
   string s_host, s_pto, s_sid;   // Oracle
   string s_usr, s_paswd, sError; // Oracle e Informix

   static char* etiq[][2] =
   {
      {"AR"   , "[AR_BASE_DATOS]"},
      {"AR"   , "[AR_BASE_DATOS_ORACLE]"}
   };

   CBD** bd = &m_BDAR; 
   
   if (*bd)
   {
      sError = string("COperacion.conectaBD: Ya existe una conexión abierta a la BD de ") + etiq[id_db][0];
      escribeBitacora(eAPL, ERR_NOT_DESENCRYPT_PWD, sError.c_str());
      return false; 
   }
   
   int ok_vars = 1;

   #if ORACLE
      ok_vars = getVarConBDOracle(etiq[id_db][1], &s_host, &s_pto, &s_sid, &s_usr, &s_paswd);
   #else
      ok_vars = getVarConBD(etiq[id_db][1], &s_srv, &s_db, &s_usr, &s_paswd, &s_role);
   #endif

   if (ok_vars != 0)
   {
      sError = string("COperacion.conectaBD: No se pudieron obtener los parámetros de conexión a la BD de ") + etiq[id_db][0];
      escribeBitacora(eAPL, ERR_NOT_DESENCRYPT_PWD, sError.c_str());
      return false;
   }
   if (!procesaPassword(s_paswd, &s_paswd))
   {
      sError = string("COperacion.conectaBD: No se pudo descifrar la contraseña de conexión a la BD de ") + etiq[id_db][0];
      escribeBitacora(eAPL, ERR_NOT_DESENCRYPT_PWD, sError.c_str());
      return false; 
   }
   
   *bd = new CBD(Bitacora);
/*
   if (!bd ||
       !bd->setConfig(s_srv.c_str(), s_db.c_str(), s_usr.c_str(), s_paswd.c_str(), s_role.c_str()) ||
       !bd->conecta())
   {
      sError = string("Error al conectarse a la BD de ") + etiq[id_db][0];
      escribeBitacora(eAPL, ERR_NOT_DESENCRYPT_PWD, sError.c_str());
      return false; 
   }
*/
   bool ok  = *bd != NULL;

   bool ok_configura = false;

   #if ORACLE
      ok_configura = (*bd)->setConfig(s_usr.c_str(), s_paswd.c_str(), s_host.c_str(), s_pto.c_str(), s_sid.c_str());
   #else
      ok_configura = (*bd)->setConfig(s_srv.c_str(), s_db.c_str(), s_usr.c_str(), s_paswd.c_str(), s_role.c_str());
   #endif

   ok = ok && ok_configura;
   ok = ok && (*bd)->conecta();
   if (!ok)
   {
      sError = string("COperacion.conectaBD: No se pudo abrir la conexión a la BD de ") + etiq[id_db][0];
      escribeBitacora(eAPL, ERR_NOT_DESENCRYPT_PWD, sError.c_str());
      return false; 
   }

   Bitacora->escribePV(BIT_INFO, "COperacion: Conexión abierta a la BD de la %s", etiq[id_db][0]);
   
   return true;
}
/*##############################################################################
   PROPOSITO: Esta funcion obtiene los valores de las variables para la 
              conexion con la BD, las variables que solicita son: NOMBRE
              USUARIO, PASSWORD, SERVIDOR, ROL. 
              
   PREMISA:   El apuntador global a la clase SrvAR debe tener las variables
              del archivo de configuracion en memoria.
   EFECTO:    No tiene efecto sobre el ambiente.
   ENTRADAS:  s_id: Identificador de la base de datos a la que se va a conectar
   SALIDAS:   No tiene valores de retorno.
#################################################################################*/

int COperacion::getVarConBD(const char* sz_id, string* s_srv, string* s_db, string* s_usr, string* s_pswd, string* s_role)
{
   int errVars = 0;

   errVars = m_Configuracion.getValorVar(sz_id, "SERVIDOR", s_srv);
   if(errVars != 0)
      return errVars;
   errVars = m_Configuracion.getValorVar(sz_id, "BD"  , s_db);
   if(errVars != 0)
      return errVars;
   errVars = m_Configuracion.getValorVar(sz_id, "USUARIO" , s_usr);
   if(errVars != 0)
      return errVars;
   errVars = m_Configuracion.getValorVar(sz_id, "PASSWORD", s_pswd);
   if(errVars != 0)
      return errVars;
   errVars = m_Configuracion.getValorVar(sz_id, "ROL"     , s_role);
   //>- if(errVars != 0) // ERGL mar jul 24 11:05:38 CDT 2007
   if(errVars < 0)
      return errVars;

   return 0;
}

int COperacion::getVarConBDOracle(const char* sz_id, string* s_host, string* s_pto, string* s_sid, string* s_usr, string* s_pswd)
{
   int errVars = 0;

   errVars = m_Configuracion.getValorVar(sz_id, "HOST", s_host);
   if(errVars != 0)
      return errVars;

   errVars = m_Configuracion.getValorVar(sz_id, "PORT", s_pto);
   if(errVars != 0)
      return errVars;

   errVars = m_Configuracion.getValorVar(sz_id, "SID" , s_sid);
   if(errVars != 0)
      return errVars;

   errVars = m_Configuracion.getValorVar(sz_id, "USR" , s_usr);
   if(errVars != 0)
      return errVars;

   errVars = m_Configuracion.getValorVar(sz_id, "PWD" , s_pswd);
   if(errVars < 0)
      return errVars;

   return 0;
}

/*##############################################################################
   PROPOSITO: Esta funcion desencripta el password de la BD.

   PREMISA:   Pasar el password encriptado.
   EFECTO:    No tiene efecto sobre el ambiente.
   ENTRADAS:  s_pwdEnc: Password encriptado
   SALIDAS:   Regresa: true -> Exito o false error.
#################################################################################*/

bool COperacion::procesaPassword(const string& s_pwdEnc, string *s_pwdDes)
{
   bool b_resOperacion = true;
   int i_pwdSalida = s_pwdEnc.size();
   char *psz_pwdSalida = new char[i_pwdSalida];
   if (!desencripta((uint8*) s_pwdEnc.c_str(), i_pwdSalida, (uint8*) psz_pwdSalida, &i_pwdSalida))
      b_resOperacion = false;
   else
      s_pwdDes->assign(psz_pwdSalida, i_pwdSalida);
   delete[] psz_pwdSalida;
   psz_pwdSalida = NULL;
   return b_resOperacion;
}

/*##############################################################################
   PROPOSITO: Este metodo se utiliza para escribir a la bitacora

   PREMISA:   
   EFECTO:    
   ENTRADAS:  
   SALIDAS:   Regresa: 0 -> Exito o un codigo de error.
#################################################################################*/
intE COperacion::escribeBitacora(int i_apl, int i_cdgErr, const char *psz_descripcion)
{
   intE errorOp = 0;    
   errorOp = sgiError(i_apl, i_cdgErr);
   Bitacora->escribe(BIT_ERROR, errorOp, psz_descripcion);

   return errorOp;
}




/*##############################################################################
   PROPOSITO: Este metodo se utiliza para concatenar el nombre de la clase 
              que ejecuta el mensaje de error msgCliError().
              sólo si es requerido en la Bitacora, de no ser requerido
              envía el mensaje de error sin el nombre de la clase.
              
   PREMISA:   El formato de error en la bit�cora se espera recibir con el nombre 
              de la clase y el m�todo donde se ejecut�, seguido del caracter  ':'
              ejemplo: COperacion Metodo1: "Error "
   EFECTO:    
   ENTRADAS:  Si el entero es 1 coloca el nombre de la clase, si es diferente
              elimina el nombre de la clase.
   SALIDAS:   Regresa: un Char con o sin el nombre de la clase.
#################################################################################*/
const char* COperacion::SeparaNombreClase(const char *QuitarClase, int NumeroQuitarClase)
{
     
     char DosPuntos=':';
     char *apuntador=NULL;

     apuntador=strchr(QuitarClase,DosPuntos);
     if(apuntador==NULL)
        return(QuitarClase);
        
     apuntador++;
     
     //Coloca el nombre de la clase
     if(NumeroQuitarClase==1)
     {
       QuitarClase=QuitarClase;
       return(QuitarClase);
       
     //Elimina el nombre de la clase      
       }else{
             //apuntador++;
             QuitarClase=apuntador;
             return(QuitarClase);
             }
}


/*##############################################################################
   PROPOSITO: Este metodo se encarga de cerrar la conexion con la BD.

   PREMISA:   
   EFECTO:    
   ENTRADAS:  
   SALIDAS:   Regresa: 0 -> Exito o un codigo de error.
#################################################################################*/
void COperacion::desconectaBD(int i_db)
{
   if (m_BDAR)
   {
      m_BDAR->desconecta();

      delete m_BDAR;
      m_BDAR = NULL;
   }

   Bitacora->escribe(BIT_INFO, "COperacion: Desconexión de la base de datos");
}
/*##############################################################################
   PROPOSITO: Esta funcion envia informacion por medio de un socket o fifo.

   PREMISA:   Pasar la constante enum 
   EFECTO:    No tiene efecto sobre el ambiente.
   ENTRADAS:  i_destino: Identificador del destino del envio.
   SALIDAS:   Regresa: 0 -> Exito o un codigo de error.
#################################################################################*/
int COperacion::envia(DST_MSG i_destino)
{
   int error = 0;
   switch(i_destino)
   {
      case AR_MEDIADOR:
      {
         Bitacora->escribe(BIT_INFO, "COperacion: Envio de mensaje al AR_Mediador");

         if(!m_fifoMed->getIniciado())
         {
            if(m_fifoMed->inicia(m_msgMed) != 0)
            {
               error = sgiErrorBase(escribeBitacora(eAPL, ERR_NO_CREO_FIFO, "COperacion.envia: No se pudo crear el FIFO de comunicación"));
               break;
            }
         }
         if(m_fifoMed->escribe() != 0)
         {
            error = sgiErrorBase(escribeBitacora(eAPL, ERR_NO_ESCRIBE_FIFO, "COperacion.envia: No se pudo escribir en el FIFO de comunicación"));
            break;
         }
         break;
      }
      case CERTISAT:
      {
         Bitacora->escribe(BIT_INFO, "COperacion: Envio de mensaje al CERTISAT");

         error = m_SktProc->Envia(m_MsgCli.buffer, m_MsgCli.tamMensaje());
         if (error)
         {
            error = sgiErrorBase(escribeBitacora(eAPL, ERR_NO_ENVIA_SKT, "COperacion.envia: No se pudo enviar el mensaje a través del socket de comunicación")); 
            break;
         }
         break; 
      }
      default:
      {
         error = sgiErrorBase(escribeBitacora(eAPL, ERR_OPC_DEST_NO_RECON, "COperacion.envia: Destino de mensaje no reconocido"));
         break;
      }
   }

   return error;
}

/*##############################################################################
   PROPOSITO: Esta funcion recibe informacion de un socket o un fifo.

   PREMISA:   Pasar la constante AR_MEDIADOR o CERTISAT para que haga el envio.
   EFECTO:    No tiene efecto sobre el ambiente.
   ENTRADAS:  id_origen: Identificador del origen del que se recibe
   SALIDAS:   Regresa: 0 -> Exito o un codigo de error.
#################################################################################*/
int COperacion::recibe(DST_MSG i_origen)
{
   int error = 0; 
   char *buffMen = new char[TAM_DATOS];
   //int i_buffMen = TAM_DATOS; mié sep 12 16:33:33 CDT 2007

   switch(i_origen)
   {
      case AR_MEDIADOR:
      {
         Bitacora->escribe(BIT_INFO, "COperacion: Recepción de mensaje del AR_Mediador");

         if(!m_fifoMed->getIniciado())
         {
            if(m_fifoMed->inicia(m_msgMed) != 0)
            {
               escribeBitacora(eAPL, ERR_NO_CREO_FIFO, "COperacion.recibe: No se pudo crear el FIFO de comunicación");
               break;
            }
         }
         if(m_fifoMed->lee() != 0)
         {
            escribeBitacora(eAPL, ERR_NO_LEE_FIFO, "COperacion.recibe: No se pudo leer del FIFO de comunicación");
            break;
         }
         break;
      }
      case CERTISAT:
      {
         Bitacora->escribe(BIT_INFO, "COperacion: Recepción de mensaje del CERTISAT");

         //error = m_SktProc->Recibe(m_MsgCli.buffer, i_buffMen); //>- ERGL mié sep 12 16:31:58 CDT 2007
         error = m_MsgCli.Recibe(m_SktProc);
         if(error != 0)
         {
            escribeBitacora(eAPL, sgiErrorBase(error), "COperacion.recibe: No se pudo recibir el mensaje del socket de comunicación");
            break;
         }
         break;
      }
      default:
      {
         error = sgiErrorBase(escribeBitacora(eAPL, ERR_OPC_ORIGEN_NO_RECON, "COperacion.recibe: Origen de mensaje no reconocido"));
         break;
      }
   }
   delete []buffMen;

   return error;
}

/*##############################################################################
   PROPOSITO: Esta funcion procesa un mensaje de error contenido en el 
              objeto de MensajesSAT.

   PREMISA:   Que ya se haya recibido el mensaje con el objeto m_mensajesMed.
   EFECTO:    No tiene efecto sobre el ambiente.
   ENTRADAS:  
   SALIDAS:   Regresa: 0 -> Exito o un codigo de error.
              Escribe en cada variable el contenido del mensaje desglosado.
#################################################################################*/

int COperacion::getMsgErr(DST_MSG origen, char& psz_tipError, char *psz_cdgErr, int i_cdgErr, char *psz_desc, int i_desc)
{
   intE errOp = 0;
   char by_firma[684];
   int  i_firma = sizeof(by_firma);
   int  i_tipError = 3;
   
   if(origen == AR_MEDIADOR)
   {
      Bitacora->escribe(BIT_INFO, "COperacion: Procesa mensaje de error del AR_Mediador");

      if(!m_msgMed)
      {
         escribeBitacora(eAPL, ERR_FIFO_MED_INI, "COperacion.getMsgErr: El objeto de mensajes mediador no ha sido iniciado");
         
         return ERR_FIFO_MED_INI;
      }
      
      errOp = m_msgMed->getMensaje(MSGERROR, by_firma, &i_firma, &psz_tipError, &i_tipError, 
                                   psz_cdgErr, &i_cdgErr, psz_desc, &i_desc);
   }
   else
   {
      Bitacora->escribe(BIT_INFO, "COperacion: Procesa mensaje de error del CERTISAT");

      errOp = m_MsgCli.getMensaje(MSGERROR, by_firma, &i_firma, &psz_tipError, &i_tipError,
                                  psz_cdgErr, &i_cdgErr, psz_desc, &i_desc);
   }

   if(errOp != 0)
      return sgiErrorBase(escribeBitacora(eAPL, sgiErrorBase(errOp), "COperacion.getMsgErr: No se pudieron obtener los datos del mensaje de error"));
   else
      return 0;
}

/*##############################################################################
   PROPOSITO: Crea el mensaje de error y lo coloca en el objeto de 
              MensajesSAT.

   PREMISA:   
   EFECTO:    No tiene efecto sobre el ambiente.
   ENTRADAS:  Todos los parametros son de entrada y representan el contenido
              de lo que se va a insertar al mensaje y el tamanio del contenido.
   SALIDAS:   Regresa: 0 -> Exito o un codigo de error.
#################################################################################*/

int COperacion::setMsgErr(DST_MSG destino, char psz_tipError, const char *psz_cdgErr, const char *psz_desc)
{
   intE errOp = 0;
   if(destino == AR_MEDIADOR)
   {
      Bitacora->escribe(BIT_INFO, "COperacion: Creación de mensaje de error para el AR_Mediador");

      if(!m_msgMed)
      {
         escribeBitacora(eAPL, ERR_FIFO_MED_INI, "COperacion.setMsgErr: El objeto de mensajes mediador no ha sido iniciado");
         return ERR_FIFO_MED_INI;
      }
      else errOp = m_msgMed->setMensaje(MSGERROR, &psz_tipError, 1, 
                                        psz_cdgErr, strlen(psz_cdgErr), psz_desc, strlen(psz_desc));
   }
   else
   {
      Bitacora->escribe(BIT_INFO, "COperacion: Creación de mensaje de error para el CERTISAT");

      errOp = m_MsgCli.setMensaje(MSGERROR, &psz_tipError, 1,
                                  psz_cdgErr, strlen(psz_cdgErr), psz_desc, strlen(psz_desc));
   }

   if(errOp != 0)
      return sgiErrorBase(escribeBitacora(eAPL, sgiErrorBase(errOp), "COperacion.setMsgErr: No se pudo crear el mensaje de error"));
   
   return 0;
}

/*##############################################################################
   PROPOSITO: Esta funcion asigna memoria a los apuntadores miembro de la clase.
              para iniciar comunicacion con el AR_Mediador

   PREMISA:   
   EFECTO:    No tiene efecto sobre el ambiente.
   ENTRADAS:  Todos los parametros son entradas y son los datos necesarios para 
              iniciar el objeto de MensajesSAT. 
   SALIDAS:   Regresa: 0 -> Exito o un codigo de error.
#################################################################################*/

int COperacion::iniciaMed(char *psz_cerDestino, char *psz_cerOrigen, char *psz_keyOrigen, char *psz_passOrigen)
{
   intE errorOp = 0;
   int errFifoIni = 0;

   Bitacora->escribe(BIT_INFO, "COperacion: Inicializa los objetos de comunicación con el AR_Mediador");

   m_msgMed = new MensajesSAT;
   if(!m_msgMed)
   {
      Bitacora->escribe(BIT_ERROR, "COperacion.iniciaMed: No se pudo crear el objeto m_msgMed");

      return ERR_NO_ASG_MEM_OBJ_MEN;
   }

   errorOp = m_msgMed->Inicia(AR, psz_cerOrigen, psz_keyOrigen, psz_passOrigen, (int)strlen(psz_passOrigen), psz_cerDestino);
   if(errorOp != 0)
   {
      Bitacora->escribe(BIT_ERROR, "COperacion.iniciaMed: No se pudo iniciar el objeto m_msgMed");
      delete m_msgMed;

      return sgiErrorBase(errorOp);
   }

   m_fifoMed = new CFifoArMed;
   if(!m_fifoMed)
   {
      Bitacora->escribe(BIT_ERROR, "COperacion.iniciaMed: No se pudo crear el objeto m_fifoMed");

      return ERR_NO_ASG_MEM_OBJ_FIFO;
   }

   errFifoIni = m_fifoMed->inicia(m_msgMed);
   if(errFifoIni != 0)
   {
      Bitacora->escribe(BIT_ERROR, "COperacion.iniciaMed: No se pudo iniciar el objeto m_fifoMed");

      return ERR_FIFO_MED_INI;
   }

   return 0;
}

/*##############################################################################
   PROPOSITO: Esta funcion libera la memoria asignada a los apuntadores en 
              la funcion de iniciaMed.

   PREMISA:   
   EFECTO:    Libera la memoria asignada a los apuntradores.
   ENTRADAS:  id_db: Identificador de la base de datos a la que se va a conectar
   SALIDAS:   Regresa: 0 -> Exito o un codigo de error.
#################################################################################*/
void COperacion::terminaMed(void)
{
   Bitacora->escribe(BIT_INFO, "COperacion: Finaliza los objetos de comunicación con el AR_Mediador");

   if(m_msgMed)
      delete m_msgMed;
   if(m_fifoMed)
      delete m_fifoMed;

   m_fifoMed = NULL;
   m_msgMed = NULL;
}

/*##################################################################################
 
    PROPOSITO: Generar una operación.
    PREMISA  : Ya estar conectado a la BD.
    EFECTO   :
    ENTRADAS : cTip     :  Tipo de Opeación a generar.
               iTipOper :  El Número de Mensaje ejemplo : SOLCERT.
               cAgcCve  :  Clave del Agente , RFC , Recortado.
    SALIDAS  : cOper    :  Número de operación generado.
###################################################################################*/

bool COperacion::genOperacion()
{  
   // NOTA MAML: El store sp_GenNumOper() crea un insert para la tabla SOL_OPERACION y 
   //            otro insert (el 1ero.) para la tabla OPERACION(m_numOp, 1, getTipOper(), CURRENT)

   bool ok_ejecuta = false;

   #if ORACLE
      ok_ejecuta = m_BDAR->ejecutaFuncion(OPE_SP_GENERA_NUM_OPERACION_NOMBRE, OPE_SP_GENERA_NUM_OPERACION_RETURN, OPE_SP_GENERA_NUM_OPERACION_PARAMS, getTipOper(), m_agcCve);
   #else
      ok_ejecuta = m_BDAR->ejecutaSP(OPE_SP_GENERA_NUM_OPERACION_NOMBRE, OPE_SP_GENERA_NUM_OPERACION_PARAMS, getTipOper(), m_agcCve);
   #endif

   bool ok = ok_ejecuta && m_BDAR->getValores("b", m_numOp) && ( strncmp("INVALIDO", m_numOp, 8) !=  0);
 
   if (ok)
      Bitacora->escribePV(BIT_INFO, "COperacion: Número de operación generado = %s", m_numOp);
   else
   {
      //>+  V.1.1.1  GHM (080606): Se modificó para tener más datos del error que se presentó.
      //>- escribeBitacora(eAPL, 0, "Error al generar el número de operacion.(Ver error BD)");
      //>- msgCliError('I', "Error al generar el número de operacion.(Ver error BD)", ERR_GEN_NUM_OPER, eAPL);
      Bitacora->escribePV(BIT_ERROR, "COperacion.genOperacion: No se pudo generar el número de operación con TipOper(%d), CveAgC(%s), NumOp(%s)", getTipOper(), m_agcCve, m_numOp); 
      msgCliError('I', "No se pudo generar el número de operacion (Ver error de BD)", ERR_GEN_NUM_OPER, eAPL);
      //<+  V.1.1.1  GHM
   }
       
   return ok;
}

/*##################################################################################

    PROPOSITO: Generar un tramite
    PREMISA  : Ya estar conectado a la BD. Haber generado una operacion
    EFECTO   :
    ENTRADAS :  
    SALIDAS  :  Regresa el numero de tramite generado.
###################################################################################*/

bool COperacion::genTramite()
{

   m_BDAR->prepCadDelim(1, (const char*)m_cadOriOp, m_cadOriOp);

   bool ok_ejecuta = false;

   #if ORACLE
      ok_ejecuta = m_BDAR->ejecutaFuncion(OPE_SP_GENERA_NUM_TRAMITE_NOMBRE, OPE_SP_GENERA_NUM_TRAMITE_RETURN, OPE_SP_GENERA_NUM_TRAMITE_PARAMS,
                               getTipOper(), m_numOp, m_rfcOp, m_agcCve, m_cadOriOp, m_firmaOp, m_refArchOp);
   #else
      ok_ejecuta = m_BDAR->ejecutaSP(OPE_SP_GENERA_NUM_TRAMITE_NOMBRE, OPE_SP_GENERA_NUM_TRAMITE_PARAMS,
                               getTipOper(), m_numOp, m_rfcOp, m_agcCve, m_cadOriOp, m_firmaOp, m_refArchOp);
   #endif

   bool ok = ok_ejecuta && m_BDAR->getValores("b", m_numTram) && ( strncmp("INVALIDO", m_numTram, 8) != 0);
   
   if (ok)
      Bitacora->escribePV(BIT_INFO, "COperacion: Número de trámite generado = %s", m_numTram);
   else
   {
      //>> V.1.1.1  GHM (080606): Se modificó para tener más datos del error que se presentó.
      //>- escribeBitacora(eAPL, 0, "Error al generar el número de trámite.(Ver error BD)");
      //>- msgCliError('I', "Error al generar el número de trámite.(Ver error BD)", ERR_GEN_NUM_OPER, eAPL);
      Bitacora->escribePV(BIT_ERROR, "COperacion.genTramite: No se pudo generar el trámite con TipOper(%d), numOper(%s), RFC(%s), CveAgC(%s), cadOrig(%s), firmaOp(%s), refArch(%s)",
                          getTipOper(), m_numOp, m_rfcOp, m_agcCve, m_cadOriOp, m_firmaOp, m_refArchOp);

      msgCliError('I', "Error al generar el número de trámite (Ver error de BD)", ERR_GEN_NUM_OPER, eAPL); // RORS Faltaba ya que se quedaba 
      //esperandeo el CV.
      //<+  V.1.1.1  GHM 
   }
   
   return ok;
}

/*##################################################################################

    PROPOSITO: Envia el numero de tramite obtenido al CERTISAT. 
    PREMISA  : 
    EFECTO   :
    ENTRADAS :  
    SALIDAS  :  
###################################################################################*/

 bool COperacion::envTramite()
{
   bool ok = false;
   int err = 0;
   m_numTram[strlen(m_numTram)] = 0;

   err = sgiErrorBase(m_MsgCli.setMensaje (NUMTRAMITE, m_numTram, strlen (m_numTram)));
   if (!err)
   {
      if ((err = envia(CERTISAT) == 0))
         ok = true;
      else 
         escribeBitacora(eSGISSL, err, "COperacion.envTramite: No se pudo enviar el mensaje de trámite al CERTISAT");
   }
   else 
   {
      escribeBitacora(eMSGSAT, err, "COperacion.envTramite: No se pudo preparar el mensaje para el CERTISAT");
      msgCliError('I', "No se pudo preparar el mensaje NUMTRAMITE para el CERTISAT", err, eMSGSAT); 
   }

   return ok;
}

/*##################################################################################

    PROPOSITO: Inicia las variables de operacion.
    PREMISA  :
    EFECTO   :
    ENTRADAS :
    SALIDAS  :
###################################################################################*/

int COperacion::iniciaVarsTram(void)
{
   m_ifirmaOp   = MAX_TAM_FIRMA_B64;
   m_irfcOp     = MAX_TAM_RFC;
   m_icadOriOp  = MAX_TAM_CADORI;
   m_irefArchOp = MAX_TAM_PATH;
   m_isecOp     = MAX_TAM_SEC_AR;
   m_firmaOp    = new char[MAX_TAM_FIRMA_B64];
   m_rfcOp      = new char[MAX_TAM_RFC];
   m_cadOriOp   = new char[MAX_TAM_CADORI];
   m_refArchOp  = new char[MAX_TAM_PATH];
   m_secOp      = new char[MAX_TAM_SEC_AR];
   if (!m_firmaOp || !m_rfcOp || !m_cadOriOp || !m_refArchOp || !m_secOp)
      return ERR_ASG_MEM_VAR;
   return 0;
}

/*##################################################################################

    PROPOSITO: Termina las variables de tramite.
    PREMISA  :
    EFECTO   :
    ENTRADAS :
    SALIDAS  :
###################################################################################*/

void COperacion::terminaVarsTram(void)
{
   if (m_firmaOp)
      delete[] m_firmaOp;
   if (m_rfcOp)
      delete[] m_rfcOp;
   if (m_cadOriOp)
      delete[] m_cadOriOp;
   if (m_refArchOp)
      delete[] m_refArchOp;
   if(m_secOp)
      delete[] m_secOp;
   m_ifirmaOp   = 0;
   m_irfcOp     = 0;
   m_icadOriOp  = 0;
   m_irefArchOp = 0;
   m_isecOp     = 0;
   m_firmaOp    = NULL;
   m_rfcOp      = NULL;
   m_cadOriOp   = NULL;
   m_refArchOp  = NULL;
   m_secOp      = NULL;
}

/*##################################################################################

    PROPOSITO: Envia mensaje de Error al CERTISAT, hace el registro del error en la
       base de datos y escribe en la bitacora.
    PREMISA  : Los elementos de BD y de Mensajes deben estar iniciados.
    EFECTO   :
    ENTRADAS : Entero de referencia a la aplicacion en donde sucedio el error, error
       local de la aplicacion, mensaje, medio 'P', 'O'.
    SALIDAS  : regresa false.
###################################################################################*/

bool COperacion::msgCliError(char c_tipErr, const char* psz_msg, int i_error, eIdLib i_apl)
{
   int error = 0;
   int NumeroClase=0;
   //Para sobreescribir el Nombre de la Clase 
   const char *psz_msgDos=psz_msg;
   
   bool ok = false;
   char psz_error[20];
   sprintf(psz_error, "%d", i_error);

   switch(c_tipErr)
   {
      case 'I':
      default:
      {
         // Mensaje de error genérico utilizado para especificar los errores internos de la aplicación
         const char msgCliTxt[] = "Ha ocurrido un error al procesar la solicitud, por favor vuelva a intentarlo";
         error = setMsgErr(CERTISAT, c_tipErr, psz_error, msgCliTxt);

         break;
      }
      case 'N':
      {
         // Mensaje de error de negocio, puede venir con el formato "Nombre_clase:nombre_método:mensaje_error"
         // por lo cual es necesario separar en tokens para obtener solo el mensaje de error
         NumeroClase=2;
         psz_msg=SeparaNombreClase(psz_msgDos,NumeroClase);
         error = setMsgErr(CERTISAT, c_tipErr, psz_error, psz_msg);

         break;
      }
   }

   if (!error)
   {
      error = envia(CERTISAT);  // Envía el mensaje de error al CERTISAT
      if (!error)
      {
         if (m_numOp[0])
         {
            if (m_utilBDAR->RegOperDetalleBDAR(MSGERROR, m_numOp, i_error)) // Registra el mensaje de error en la BD
               ok = true;
            else 
               escribeBitacora(eAPL, ERR_BD_REG_OPER_SEG, "COperacion.msgCliError: No se registró el mensaje de error en la BD");
         }
      }
      else 
         escribeBitacora(eSGISSL, error, "COperacion.msgCliError: No se envió el mensaje de error al CERTISAT");
   }
   else 
      escribeBitacora(eMSGSAT, error, "COperacion.msgCliError: No se pudo crear el mensaje de error");
      
   //Con nombre de la clase   
   NumeroClase=1;
   psz_msg=SeparaNombreClase(psz_msgDos,NumeroClase);
   escribeBitacora(i_apl, i_error, psz_msg);  // Escribe el mensaje de error en la bitácora
   
   return ok;
}

/*##################################################################################

    PROPOSITO: Envia mensaje de Error al CERTISAT, hace el registro del error en la
       base de datos y escribe en la bitacora.
    PREMISA  : Los elementos de BD y de Mensajes deben estar iniciados.
    EFECTO   :
    ENTRADAS : Entero de referencia a la aplicacion en donde sucedio el error, error
       local de la aplicacion, mensaje, medio 'P', 'O'.
    SALIDAS  : regresa false.
###################################################################################*/

int COperacion::iniMed()
{
   string s_cert, s_key, s_psw, s_certAd, s_pswD;
   int errVars = 0;

   Bitacora->escribe(BIT_INFO, "COperacion: Carga parámetros de configuración para la comunicación con el AR_Mediador");

   errVars = m_Configuracion.getValorVar(TAG_MSG_MED, "CERT", &s_cert);
   if (errVars != 0)
   {
      Bitacora->escribe(BIT_ERROR, "COperacion.iniMed: No se pudo obtener el parámetro CERT");

      return errVars;
   }

   errVars = m_Configuracion.getValorVar(TAG_MSG_MED, "PRIVK"  , &s_key);
   if (errVars != 0)
   {
      Bitacora->escribe(BIT_ERROR, "COperacion.iniMed: No se pudo obtener el parámetro PRIVK");

      return errVars;
   }

   errVars = m_Configuracion.getValorVar(TAG_MSG_MED, "PWDPK" , &s_psw);
   if (errVars != 0)
   {
      Bitacora->escribe(BIT_ERROR, "COperacion.iniMed: No se pudo obtener el parámetro PWDPK");

      return errVars;
   }

   errVars = m_Configuracion.getValorVar(TAG_MSG_MED, "RCERAGCS", &s_certAd);
   if (errVars != 0)
   {
      Bitacora->escribe(BIT_ERROR, "COperacion.iniMed: No se pudo obtener el parámetro RCERAGCS");

      return errVars;
   }

   if (!procesaPassword(s_psw, &s_pswD))
   {
      Bitacora->escribe(BIT_ERROR, "COperacion.iniMed: No se pudo descifrar el parámetro PWDPK");

      return ERR_NOT_DESENCRYPT_PWD;
   }

   errVars = iniciaMed((char*)s_cert.c_str(), (char*)s_cert.c_str(), (char*)s_key.c_str(), (char*)s_pswD.c_str());
   if(errVars != 0)
   {
      return ERR_NO_INICIA_VARS_MED;
   }

   return errVars;
}
