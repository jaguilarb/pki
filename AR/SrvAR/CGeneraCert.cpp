static const char* _CGENERACERT_CPP_VERSION_ ATR_USED = "@(#) SrvAR ( L : DSIC10392AR_ : CGeneraCert.cpp : 1.1.3 : 6 : 28/09/10)";

//#VERSION: 1.1.1
/*######################################################################################################################
  ###  PROYECTO:              PKI-SAT                                                                                ###
  ###  MODULO:                Clase que realiza la Generaci�n de CDs                                                 ###
  ###                                                                                                                ###
  ###  DESARROLLADORES:       H�ctor Ornelas Arciga           HOA                                                    ###
  ###  FECHA DE INICIO:       Jueves 26, enero del 2006                                                              ###
  ###                                                                                                                ###
  ######################################################################################################################
         1         2         3         4         5         6         7         8         9        10        11        12
123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
 #######################################################################################################################
   VERSION:
      V.1.00      (20060126 - 2006    ) HOA: Primera Versi�n
   CAMBIOS:
      V.1.1.1     (100413) MAML: se incorpora la clase CSeparaToken y se amplia el buffer en el mensaje al Mediador 
      V.1.1.2     (100722) MAML: correcciones a la llamada de la clase CSeparaToken 
           .3     (20141014 -         ) JAB: Se agrupan las sentencias SQL utilizadas en el SrvAR en un archivo de
                                              cabecera
######################################################################################################################*/
#include <assert.h>setVarsErr
#include <CGeneraCert.h>
#include <Sgi_SrvUnx.h>
#include <CUtilBDAR.h>
//#include <CSeparaToken.h>
#include <Sgi_PKI.h>  // Se cambia CSeparaToken.h por Sgi_PKI
//######################################################################################################
CGeneraCert :: CGeneraCert (CConfigFile& config, MensajesSAT& msgcli, CSSL_Skt* skt) :
   COperacion(config, msgcli, skt, true)
{
   //>>- GHM (090402): Se env�o a CSolCert
      //>>+ GHM (081216): Para la sustituci�n de validaciones en DARIO
      //m_valDat_FIEL = NULL;
      //m_valDat_SDG  = NULL;
      //+<< GHM (081215) 
   //<<- GHM (090402)
   m_rfc[0]   = 0;
   m_tipo     = 0;
   m_req      = NULL;
   bNvoReq    = NULL;
   m_lreq     = 0;
   m_cert     = NULL;
   m_lcert    = 0;
   m_operSec  = 0;
   reqNvo     = false;

   //m_agcRfcRec[0] = 0;
   //m_agcModulo[0] = 0;
   
   m_pubKey = NULL;

   limpiaVarsErr(); 
   LimpiaVarsReq();
}
//######################################################################################################
CGeneraCert::~CGeneraCert()
{
   if (m_req)
   {
      delete[] m_req;
      m_req = NULL;
   }
   if (bNvoReq)
   {
      delete[] bNvoReq;
      bNvoReq = NULL;
   }  

   if (m_cert)
   {
      delete[] m_cert;
      m_cert = NULL;
   }
   
   LimpiaVarsReq(true);
   terminaMed();
}
//######################################################################################################
void CGeneraCert::limpiaVarsErr()
{
   m_errTipo = ' ';
   m_errCod  = 0;
   m_errDesc[0] = 0;
}
//######################################################################################################
int CGeneraCert::setVarsErr(char tipo, int codigo, const char* desc)
{
   m_errTipo = tipo;
   m_errCod  = codigo;
   strcpy(m_errDesc, desc);
   //>+ ERGL (070129)

   Bitacora->escribePV(BIT_INFO, "CGeneraCert: Se prepara para enviar, ERROR = %c, CODIGO = %d, MENSAJE = %s", tipo, codigo, desc);

   return codigo;
}
//######################################################################################################
void CGeneraCert :: LimpiaVarsReq(bool soloMem)
{
   if (m_pubKey)
   {
      RSA_free(m_pubKey);
      m_pubKey = NULL;
   }
    
   if (!soloMem)
   {
      m_reqRFC        [0] = 0;
      m_reqRFCRL      [0] = 0;
      m_reqCURP       [0] = 0;
      m_reqCURPRL     [0] = 0;
      m_reqEMail          = "";
      m_reqNombreComun    = "";
      m_reqOrganizacion   = "";
      m_reqUnidad         = "";
      m_reqNombre         = ""; 
      m_reqPwdRev     [0] = 0;
   }  
}
//######################################################################################################
int CGeneraCert :: CargaReq()
{
   static int snids [] = { ID_RFC, ID_CN, ID_ORG, ID_ORGU, ID_EMAIL, ID_NAME, ID_CURP};
   static int satrib[] = { ID_CHPWD };
   static int lsnids   = sizeof(snids )/sizeof(int);
   static int lsatrib  = sizeof(satrib)/sizeof(int);
   
   SGIX509  x509;
   SNames   sujeto[sizeof(snids )/sizeof(int)], 
            atrib [sizeof(satrib)/sizeof(int)];
   int i;
   int tamLlave;
   
   limpiaVarsErr();
   LimpiaVarsReq();
   
   m_pubKey = RSA_new();
   if (!m_pubKey)
   {
      Bitacora->escribe(BIT_ERROR, sgiError(1, eERRNO, errno), "CGeneraCert Error en CargaReq:  no se pudo crear el objeto RSA");
      return setVarsErr('I', ERR_CR_RSANEW, "CGeneraCert Error en CargaReq: no se pudo crear el objeto RSA");
   }
   
   memset(sujeto, 0, sizeof(sujeto));
   memset(atrib , 0, sizeof(atrib));
   
   intE error = x509.ProcREQ(m_req, m_lreq, sujeto, atrib, &m_pubKey, snids, lsnids, satrib, lsatrib);
   if (error)
   {
      Bitacora->escribe(BIT_ERROR, sgiError(1, eAPL, error), "CGeneraCert Error en CargaReq: no se pudo procesar el requerimiento");
      return setVarsErr('I', ERR_CR_PROCREQ, "CGeneraCert Error en CargaReq: no se pudo procesar el requerimiento");
   }

   // 4.11.2013 Modificaci�n el tama�o de las llaves de 1024 a 2048
/*
   tamLlave = RSA_size(m_pubKey);
   if((tamLlave == 128) && (m_tipo == TIPCER_FEA))
   {
      Bitacora->escribe(BIT_ERROR,ERR_CR_TAMLLAVE , "La llave p�blica debe ser mayor a 1024 favor de usar la versi�n de Solcedi 2.1.4 o superior.");
      return setVarsErr('N', ERR_CR_TAMLLAVE, "La llave p�blica debe ser mayor a 1024 favor de usar la versi�n de Solcedi 2.1.4 o superior.");
   }
   Bitacora->escribePV(BIT_INFO,"Tama�o de la llave recibida %d ",tamLlave);
*/
   // 9.11.2012 Esta validaci�n se cambio de la AC a la AR, por cambio en Solcedi.
   if ( !x509.verificaReq() ) 
   {
      Bitacora->escribe(BIT_ERROR, sgiError(1, eAPL, error), "CGeneraCert Error en CargaReq: Error al verificar la firma del requerimiento,requerimiento" 
      "ap�crifo.");
      return setVarsErr('I', ERR_CR_PROCREQ, "CGeneraCert Error en CargaReq: Error al verificar la firma del requerimiento, requerimiento ap�crifo.");
   } 

   for (i = 0; i < lsnids; i++)
   {
      if (!sujeto[i].dato)
         continue;
      
      switch (snids[i])
      {
         case ID_RFC  : if (!ProcesaId(xRFC , sujeto[i].dato)) return ERR_CR_RFCINV; break; 
         case ID_CURP : if (!ProcesaId(xCURP, sujeto[i].dato)) return ERR_CR_CURPINV; break;
         case ID_CN   : m_reqNombreComun  = sujeto[i].dato; break; 
         case ID_ORG  : m_reqOrganizacion = sujeto[i].dato; break;
         case ID_ORGU : m_reqUnidad       = sujeto[i].dato; break; 
         case ID_EMAIL: m_reqEMail        = sujeto[i].dato; break;
         case ID_NAME : m_reqNombre       = sujeto[i].dato; break;
         default: 
                 Bitacora->escribePV(BIT_ERROR, "CGeneraCert Error en CargaReq: identificador inv�lido (%d)", snids[i]);
                 return setVarsErr('I', ERR_CR_IDINV, "CGeneraCert Error en CargaReq: identificador inv�lido");
      }
   }
   if ( m_reqRFCRL[0] != '\0' &&  m_reqCURPRL[0]== '\0')
      generaNuevoRequerimiento(m_reqRFCRL,sujeto,lsnids,atrib,lsatrib);


   for (i = 0; i < lsatrib; i++)
   {
      if (!atrib[i].dato)
         continue;
      
      switch (satrib[i])
      {
         case ID_CHPWD:
              if (strlen(atrib[i].dato) != 28) {
                 Bitacora->escribePV(BIT_ERROR, "CGeneraCert Error en CargaReq: Clave_Revoc inv�lida (%s)", atrib[i].dato );
                 return setVarsErr('N', ERR_CR_PWDREVINV, "CGeneraCert Error en CargaReq: Contrase�a de revocaci�n vacia o no cumple con las especificaciones.");
              }   
              strcpy(m_reqPwdRev, atrib[i].dato);
              break;
         default:
              Bitacora->escribePV(BIT_ERROR, "CGeneraCert Error en CargaReq: identificador inv�lido (%d)", satrib[i]);
              return setVarsErr('I', ERR_CR_IDINV, "CGeneraCert Error en CargaReq: Error en CargaReq, identificador inv�lido");
      }
   }   
   
   return 0;
}
//######################################################################################################################
//>>- GHM (090402): Se enviaron las clases a CSolCert
//>>+ GHM (081215): Para la sustituci�n de validaciones en DARIO
/*bool CGeneraCert::IniValidaWS_IDC()
{
   limpiaVarsErr();
  
   m_valDat_FIEL = new CValDatRFC_FIEL(m_Configuracion, m_utilBDAR, TIPCER_FEA); 
   if ( m_valDat_FIEL == NULL)
   {
      Bitacora->escribe(BIT_ERROR, "No se inicializ� el objeto CValDatRFCFIEL");
      return false;
   }
   if ( !m_valDat_FIEL->ObtieneDatosWS(m_rfc) )
   {
      Bitacora->escribe(BIT_ERROR, "No se obtuvo la informaci�n del RFC consultado");
      return false;
   }
   if ( !m_valDat_FIEL->getNombreRFC(m_nombre) )
   {
      Bitacora->escribe(BIT_ERROR, "No se pudo obterner el nombre del RFC consultado");
      return false;
   } 
   char rfcOrigCons[15];
   if ( ! m_valDat_FIEL->getRFCOrig((char*)rfcOrigCons) )
   {
      Bitacora->escribe(BIT_ERROR, "No se obtuvo correctamente el RFC de consulta");
      setVarsErr('N', 1, m_valDat_FIEL->m_MsgDesc.c_str());
      return false;
   }
   if ( GetRFCBloqueado(rfcOrigCons) )
      return false;
   if (m_valDat_FIEL->getStatusRFC() == -1)
      setVarsErr('N', 1, m_valDat_FIEL->m_MsgDesc.c_str());
   Registra_RFC();
   return true;
}
//+<< GHM (081215) 

//>>+ GHM (081217): Para la sustituci�n de validaciones en DARIO
bool CGeneraCert::IniValidaSDWS_IDC()
{
   limpiaVarsErr();

   m_valDat_SDG = new CValDatRFC_SDG(m_Configuracion, m_utilBDAR, TIPCER_SD);
   if (m_valDat_SDG == NULL)
   {
      Bitacora->escribe(BIT_ERROR, "No se inicializ� el objeto CValDatRFCFIEL");
      return false;
   }
   if (!m_valDat_SDG->ObtieneDatosWS(m_rfc) )
   {
      Bitacora->escribe(BIT_ERROR, "No se obtuvo la informaci�n del RFC consultado");
      return false;
   }
   char rfcOrigCons[15];
   if ( ! m_valDat_SDG->getRFCOrig((char*)rfcOrigCons) )
   {
      Bitacora->escribe(BIT_ERROR, "No se obtuvo correctamente el RFC de consulta");
      setVarsErr('N', 1, m_valDat_SDG->m_MsgDesc.c_str());
      return false;
   }
   if ( GetRFCBloqueado(rfcOrigCons) )
      return false;

   if ( !m_valDat_SDG->getNombreRFC(m_nombre) )
   {
      Bitacora->escribe(BIT_ERROR, "No se pudo obterner el nombre del RFC consultado");
      return false;
   }
   if (m_valDat_SDG->getStatusRFC() == -1)
      setVarsErr('N', 1, m_valDat_SDG->m_MsgDesc.c_str());
      Registra_RFC();
   return true;
}*/
//+<< GHM (081217)
//<<- GHM (090402): Se env�o las clases a CSolCert

//######################################################################################################################
//>>- GHM (090402): Se env�o al destructor de CSolCert
//>>+ GHM (081215): Para la sustituci�n de validaciones en DARIO
/*bool CGeneraCert::FinValidaWS_IDC()
{
   if (m_valDat_FIEL)
   {
      //ERGL Aqui es donde se cuelga!!!
      //delete m_valDat_FIEL;
      m_valDat_FIEL = NULL;
   }
   if (m_valDat_SDG)
   {
      delete m_valDat_SDG;
      m_valDat_SDG = NULL;
   }
   return true;
}*/
//+<< GHM (081215)
//<<- GHM (090402):

//######################################################################################################################
// MAML 080826: Verifica en la AR si el RFC Original esta bloqueado: Regresa 0 si NO, != 0 si bloquedo
int CGeneraCert::GetRFCBloqueado(const char *RFC, bool esRepLegal )
{
   if ( m_utilBDAR->RFCBloqueado(RFC) == 1 )
   {
       sprintf (m_errDesc, "El RFC %s original del %s esta bloqueado", RFC, (esRepLegal) ? "rep. legal" : "contribuyente");
       
       return setVarsErr('N', ERR_RFC_BLOQ, m_errDesc);
   }

   return 0;
}
//######################################################################################################
bool CGeneraCert::ProcesaId(PROC_ID id, const char* dato)
{
   char   *d1, *d2;
   size_t ld;
   int    i;
   
   if (id == xRFC)
   {
      d1 = m_reqRFC;
      d2 = m_reqRFCRL;
      ld = 13;
   }
   else
   {
      d1 = m_reqCURP;
      d2 = m_reqCURPRL;
      ld = 18;
   }

   char* pRl = strchr(dato, '/');
   if (pRl)
   {
      for (i = 1; ((pRl - i) >= dato) && *(pRl - i) == ' '; i++)
        *(pRl -i) = 0;
      for (pRl++; *pRl == ' '; pRl++);

      if (id ==  xRFC)   
      {
         if (strlen(pRl) > ld)
            return false;
         strcpy(d2, pRl);
      }
      else  if (id ==  xCURP)
      {
          if (strlen(pRl) == 0)
            d2 = NULL;
          else
            strcpy(d2, pRl);
      }
         
   }
   
   if (strlen(dato) > ld)
         return false;
   strcpy(d1, dato);

   return true;
}
//######################################################################################################
#define VR_LET "No se puede generar el Certificado Digital:\n"

int CGeneraCert::ValidaReq()
{
   int res = CargaReq();
   if (res)
      return res;
   // RFCs iguales? tramite <-> requerimiento
   if (strcmp(m_rfc, m_reqRFC))
      return setVarsErr('N', ERR_VR_RFCDIF, VR_LET "\tCGeneraCert Error en validaReq: El RFC del requerimiento no es igual al del solicitante");
   // Verifica longitud RFC
   int lng = strlen(m_reqRFC);
   bool esPF = (lng == 13);
   if ((m_tipo == TIPCER_AGC && (!esPF || m_reqRFCRL[0])) ||  // Val para AGC: PF y no RL
       (lng != 12 && lng != 13))
      return setVarsErr('N', ERR_CR_RFCINV, VR_LET "\tCGeneraCert Error en validaReq: RFC incorrecto");
   // Verifica longitud CURP
   if ((esPF && (strlen(m_reqCURP) != 18)) ||
       ((m_tipo == TIPCER_FEA && m_reqRFCRL[0]) && ((strlen(m_reqCURPRL) != 18) &&  (strlen(m_reqCURPRL) !=0))) ||
       ((m_tipo == TIPCER_AGC) && m_reqCURPRL[0]) || // Val para AGC: noCURPRL
       (((m_tipo != TIPCER_AGC) && m_reqCURPRL[0]) && ((strlen(m_reqCURPRL) != 18) &&  (strlen(m_reqCURPRL) !=0)) ))
       //(((m_tipo != TIPCER_AGC) && m_reqCURPRL[0]) && (strlen(m_reqCURPRL) != 18)))
      return setVarsErr('N', ERR_CR_CURPINV, VR_LET "\tCGeneraCert Error en validaReq: CURP inv�lida");
   // Verifica que cuente con email
   if ((m_tipo != TIPCER_SD) && m_reqEMail.empty())
      return setVarsErr('N', ERR_VR_NOEMAIL, VR_LET "\tCGeneraCert Error en validaReq: Correo electr�nico requerido");
   // Verifica que si es CSD: tenga info en CN, Org y Unidad
   if (m_tipo == TIPCER_SD)
   {
      if (m_reqOrganizacion.empty() && m_reqNombreComun.empty())
        return setVarsErr('N', ERR_VR_NOORG, VR_LET "\tCGeneraCert Error en validaReq: Raz�n Social requerida y/o Nombre Comun Requerido");
      if (m_reqUnidad.empty())
        return setVarsErr('N', ERR_VR_NOUNIDAD, VR_LET "\tCGeneraCert Error en validaReq: Nombre de unidad requerida");
      if ( ( res = ValidaDatosREQSellos(esPF) ) != 0)
        return res;    
   }
   // Verifica que tenga pwd de revocaci�n
   if (strlen(m_reqPwdRev) != 28)
      return setVarsErr('N', ERR_CR_PWDREVINV, VR_LET "\tCGeneraCert Error en validaReq: Contrase�a de revocaci�n vacia o no cumple con los requerimientos");
   // en caso de ser de FEA y tener RL, verifica que el RL tenga CDFEA activo
   if ((m_tipo == TIPCER_FEA) && m_reqRFCRL[0])
   {
      if (!m_utilBDAR->tieneFEAact(m_reqRFCRL))
         return setVarsErr('N', ERR_VR_RLSINFEA, VR_LET "\tCGeneraCert Error en validaReq: El Representante Legal no cuenta con Certificado Digital de FIEL" 
         "activo");
      if ( (res = GetRFCBloqueado(m_reqRFCRL, true)) != 0 ) //<<-- MAML 080826
         return res;
   }
   if( getTipOper() == SOLREN && (res = ValidaDatosREQRenov()) != 0  )
      return res;   
   
   res = generaDigs();
   if (res)
      return setVarsErr('I', res, "\tCGeneraCert Error en validaReq: Error al generar la digesti�n de la llave p�blica");
   
   res = valLlaveUnica();
   if (res)
      return res;
   
   return 0;
}
//######################################################################################################
int CGeneraCert::valLlaveUnica()
{
   int res = m_utilBDAR->valLlaveUnica(m_rfc, m_reqModMd5, m_reqModSha1);
   char *ConcatenaClase;
   
   if (res == -1)
      return setVarsErr('I', res, "\tCGeneraCert Error en ValLlaveUnica: Error al consultar la unicidad de la llave p�blica");
   else if (res)
   {
      sprintf(ConcatenaClase,"%s%s","CGeneraCert Error en ValLlaveUnica: ",m_utilBDAR->m_MsgDesc);
      return setVarsErr('N', res, ConcatenaClase);
    }
   return 0;
}
//######################################################################################################
int CGeneraCert::generaDigs()
{
   uint16  lmd5 = sizeof(m_reqModMd5), lsha1 = sizeof(m_reqModSha1);
   SGIPUBLICA pubKey;
   SGIRSA rsa; 
   
   intE error = rsa.setPublica(m_pubKey, &pubKey);
   if (error)
   {
      Bitacora->escribe(BIT_ERROR, sgiError(2, eSGIOSSL, error), "CGeneraCert Error en generaDigs: ApartaLlave: error en setPublica");
      return ERR_AL_SETPUB;
   }

   error = digModuloPub(&rsa, &pubKey, MD5_ALG, m_reqModMd5, &lmd5);
   if (error)
     return error;

   error = digModuloPub(&rsa, &pubKey, SHA1_ALG, m_reqModSha1, &lsha1);
   return error;
}
//######################################################################################################
int CGeneraCert::digModuloPub(SGIRSA* rsa, SGIPUBLICA *pubKey, int alg_id, char* digestion, uint16 *lngdigestion)
{
   uint8  uDig[50];
   int    luDig = sizeof(uDig);
   
   int error = rsa->DigestAlg(pubKey->moduloP, pubKey->bits / 8, alg_id, uDig, &luDig);
   if (error)
   {
      Bitacora->escribePV(BIT_ERROR, "CGeneraCert Error en digModuloPub: error en DigestAlg(%d) = %d", alg_id, error);
      return ERR_DM_DIGMOD;
   }
   
   error = B64_Codifica(uDig, luDig, digestion, lngdigestion);
   if (!error)
   {
      Bitacora->escribePV(BIT_ERROR, "CGeneraCert Error en digModuloPub: error en B64(%d) = %d", alg_id, error);
      return ERR_DM_B64DIG;
   }
   
   return 0;
}
//######################################################################################################
int CGeneraCert::ApartaLlave()
{
   if (!m_utilBDAR->ApartaLlave(m_numOp, m_rfc, m_reqModMd5, m_reqModSha1, &m_operSec)) 
      return setVarsErr('I', ERR_AL_ARAL, "CGeneraCert Error en ApartaLlave: Aparta llave, error al registrar en la BD");
   
   return 0;
}
//######################################################################################################
int CGeneraCert::EnviaSolMed()
{
   char tipo[5], consOper[11];
   if (m_tipo != TIPCER_FEA && m_tipo != TIPCER_SD) 
      m_nombre = m_agcNombre + " " + m_agcApPat + " " + m_agcApMat;

   int error = iniMed();
   if (error)
   {
      Bitacora->escribe(BIT_ERROR, error, "CGeneraCert Error en EnviaSolMed: Error al iniciar el mediador");
      return ERR_GE_INIMED;
   }   
  
   EliminaEspacios( m_nombre );
   string datosPersona(m_rfc);
   datosPersona += "|" + m_nombre + "|" + m_agcCve;
   sprintf(tipo, "%d", m_tipo);
   sprintf(consOper, "%d", m_operSec);
    
   if (!reqNvo)
   { 
      error = m_msgMed->setMensaje(getOperCodSol(), m_numOp, strlen(m_numOp), consOper, strlen(consOper), 
                                   datosPersona.c_str(), datosPersona.length(),
                                   tipo, strlen(tipo), m_req, m_lreq);
   }
   else
   {
      error = m_msgMed->setMensaje(getOperCodSol(), m_numOp, strlen(m_numOp), consOper, strlen(consOper), 
                                   datosPersona.c_str(), datosPersona.length(),
                                   tipo, strlen(tipo), bNvoReq, iNvoReq);
   }
   if (error)
   {
      Bitacora->escribe(BIT_ERROR, error, "CGeneraCert Error en EnviaSolMed: Error en setMensaje");
      return ERR_GE_SETMSGMED;
   }   
   
   m_utilBDAR->RegOperDetalleBDAR(getOperCodSol(), m_numOp);
   
   error = envia(AR_MEDIADOR);
   if (error)
   {
      Bitacora->escribe(BIT_ERROR, error, "CGeneraCert Error en EnviaSolMed: Error al enviar petici�n el mediador");
      return ERR_GE_ENVMED;
   }   

   return 0;
}
//######################################################################################################
int CGeneraCert::RecibeRespMed()
{
   char firma[TAM_FIRMA]; //, rfc[256]; MAML 100409
   char rfc[768]; // Tama�o real de la cadena q usa la funcion es: 13+602+8+1 = TAM_RFC + TAM_NOMBRE + TAM_CLAVE_AGC + NULO = 624
   string sResp;
   int  lfirma = sizeof(firma), lrfc = sizeof(rfc);
   
   int error = recibe(AR_MEDIADOR);
   if (error)
   {
      Bitacora->escribe(BIT_ERROR, error, "CGeneraCert Error en RecibeRespMed: Error al recibir respuesta del mediador");
      return ERR_GE_RECMED;
   }
   
   if (m_msgMed->tipOperacion() == MSGERROR)
   {
      char tipo[2], cerror[11], derror[256];
      int  ltipo = sizeof(tipo), lcerror = sizeof(cerror), lderror = sizeof(derror);
      error = m_msgMed->getMensaje(MSGERROR, firma, &lfirma, tipo, &ltipo, cerror, &lcerror, derror, &lderror);
      if (error)
      {
         Bitacora->escribe(BIT_ERROR, error, "CGeneraCert Error en RecibeRespMed: Error en getMensaje de la respuesta del mediador (MSGERROR)");
         return ERR_GE_GETMSGMED;
      }
      m_utilBDAR->RegOperDetalleBDAR(MSGERROR, m_numOp);
      setVarsErr(tipo[0], atoi(cerror), derror);
      return ERR_GE_RECMEDERR;
   }
         
   assert(m_msgMed->tipOperacion() == getOperCodRes());

   m_lcert = TAM_CERT;
   lfirma = sizeof(firma);
   error = m_msgMed->getMensaje(getOperCodRes(), firma, &lfirma, rfc, &lrfc, m_cert, &m_lcert); 
   if (error)
   {
      Bitacora->escribe(BIT_ERROR, error, "CGeneraCert Error en RecibeRespMed: Error en getMensaje de la respuesta del mediador");
      Bitacora->escribePV(BIT_ERROR, "CGeneraCert Error en RecibeRespMed: Error en getMensaje de la respuesta del mediador %d", error);
      return ERR_GE_GETMSGMED;
   }
   //+++ Para el caso de Renovacion/Generacion Sello Digital, obtiene el Numero de Serie
   if ( getOperCodRes() == CERTRENAR || m_tipo == TIPCER_SD) 
   {  /* 
      char cNombre[50];
      char cAgc[20];

      if( SeparaCad(4 , rfc ,rfc, cNombre ,cAgc, m_numSerRenSellos) != 0 )
          return setVarsErr('I', ERR_CR_PWDREVINV, VR_LET "\tAl obtener los datos de la cadena recibida.");          
      */
      CSeparaToken *Tkn; // MAML 100409: Usamos esta clase para quitarnos el problema de dar el tama�o correcto de las cadenas
   
      if ( (Tkn = new CSeparaToken(rfc, '|' ) ) != NULL )
      {
         if ( Tkn->getCounter() != 4)
         {
            Bitacora->escribe(BIT_ERROR, "CGeneraCert Error en RecibeRespMed: Error al procesar el buffer 'rfc' en CSeparaToken, no hay no_serie");
            error = ERR_BUFFER_FTO;        
         }
         else
            strcpy(m_numSerRenSellos, Tkn->items[3] ); // por el momento solo se guarda esta variable
         delete Tkn;
         Tkn = NULL;
      }     
   }     
   //+++
   m_utilBDAR->RegOperDetalleBDAR(getOperCodRes(), m_numOp);

   switch(m_tipo)
   {
      
       case 3:
       case 4:
       case 5:   
       case 7:
                  {
      
                    if (!GuardaCertAgente())
                       return -1;
                    break;
                 }
   }
   
   return error;   
}
//#####################################################################################################
int CGeneraCert::Genera()
{
   int error = EnviaSolMed();
   if (error)
      return setVarsErr('I', error, "CGeneraCert Error en Genera: Error al enviar la solicitud al mediador");

   error = RecibeRespMed();
   if (error == ERR_GE_RECMEDERR)
      return error;
   else if (error)
      setVarsErr('I', error, "CGeneraCert Error en Genera: Error al recibir respuesta del mediador");
   
   m_utilBDAR->delLlaveApartada(m_numOp, m_operSec);
   
   return 0;
}
//######################################################################################################
bool CGeneraCert::setMemReqYCert()
{
   const char* msg = "CGeneraCert::setMemReqYCert -> Error al asignar memoria a los buffers de requerimiento y certificado";
   
   m_req  = new uint8[m_lreq  = TAM_REQ];
   bNvoReq= new uint8[iNvoReq  = TAM_REQ];
   m_cert = new uint8[m_lcert = TAM_CERT];

   bool ok = (m_req && m_cert ) && bNvoReq;
   if (!ok)
   {
      Bitacora->escribe(BIT_ERROR, sgiError(0, eERRNO, errno), msg);
      setVarsErr('I', 1, msg);
   }
   
   return ok;
}
//######################################################################################################
//>>- GHM (090402): Se env�o a la clase de CSolCert
/*bool CGeneraCert::Registra_RFC()
{
   char cRFCOrig[14];
   int iAvance = 0;
   bool bResp  = false;

   if ( m_tipo == TIPCER_FEA )
      bResp = m_valDat_FIEL->getRFCOrig(cRFCOrig);
   else if( m_tipo == TIPCER_SD )
      bResp = m_valDat_SDG->getRFCOrig(cRFCOrig);

   if(bResp)
   {
      if( cRFCOrig[0] == ' ')
         iAvance = 1 ;
      if(strcmp(m_rfc ,cRFCOrig + iAvance) )
        return m_utilBDAR->RegistraRFC( cRFCOrig , m_rfc );
   }
   return false;
}*/
//<<- GHM (090402)

//#########################################################################################################
/* MAML: Cambiar esta funcion por la clase CSeparaToken
int CGeneraCert ::
SeparaCad(int iPipes , char *cCad ,...)
{
   va_list lista;
   va_start(lista, cCad);

   char cCadAux[256];
   char *cRes;
   char *dato = NULL;

   int   iTamR = 0 ;

   strcpy (cCadAux ,cCad );

   for(int i = 0 ; i < iPipes ;i++ )
   {
      dato = va_arg(lista,char*);
      if(dato == NULL)
         return 10;
      cRes = strtok(cCadAux + iTamR + i,"|");
      strcpy(dato,cRes);
      iTamR = iTamR + strlen(cRes);

   }
   va_end(lista);
   return 0;
}
*/

//#########################################################################################################

bool CGeneraCert::
VerificacionDatos(char *cCadRec )
{
   string sModulo;
   string sAgcMod;
   int  error = 0;
   /*
   char cAgcNombre[50];
   char cAgcApPat[50];
   char cAgcApMat[50];

   if( SeparaCad( 6 , cCadRec , m_rfc , cAgcNombre ,cAgcApPat ,cAgcApMat ,m_agcRfcRec ,m_agcModulo) != 0 )
      return setVarsErr('I', ERR_CR_PWDREVINV, VR_LET "\tAl obtener los datos de la cadena recibida.");
  
   m_agcNombre = cAgcNombre;
   m_agcApPat  = cAgcApPat;
   m_agcApMat  = cAgcApMat;
   */

   //>>> MAML 100409: Usamos esta clase para quitarnos el problema de dar el tama�o correcto de las cadenas
   CSeparaToken *Tkn;

   if ( (Tkn = new CSeparaToken(cCadRec, '|' ) ) != NULL )
   {
      if ( Tkn->getCounter() != 6 )
         error = ERR_BUFFER_FTO;
      else
      {
         strcpy(m_rfc, Tkn->items[0] ); // por el momento solo se guarda aqui...
         m_agcNombre = Tkn->items[1];
         m_agcApPat  = Tkn->items[2];
         m_agcApMat  = Tkn->items[3];
         m_agcRfcRec = Tkn->items[4];
         m_agcModulo = Tkn->items[5];
      }
      delete Tkn;
      Tkn = NULL;
   }
   if (error)
      return setVarsErr('I', error, VR_LET "\tCGeneraCert Error en VerificacionDatos: Error al procesar el buffer 'cCadRec' en CSeparaToken.");
   //<<<
 
   if(!m_BDAR->consultaReg(GEN_CERT_OBTEN_CLAVE_MODULO_POR_AGENTE, m_agcModulo.c_str() ))
   {   
      setVarsErr('I', 1, "CGeneraCert Error en VerificacionDatos: Al consultar el Modulo en la tabla CAT_MODULO.");
      return false;
   }

   if(!m_BDAR->getValores("s",&sModulo ))
   {
     setVarsErr('N', 2, "CGeneraCert Error en VerificacionDatos: El modulo indicado no existe en la tabla CAT_MODULO.");
     return false;
   }
  
   if(!m_BDAR->consultaReg(GEN_CERT_OBTEN_MODULO_AGENTE_POR_CLAVE, m_agcRfcRec.c_str() ))
   {
      setVarsErr('N', 3, "CGeneraCert Error en VerificacionDatos: El modulo indicado no existe en la tabla AGENTE.");
      return false;
   }
   if(m_BDAR->getValores("s",&sAgcMod))   // Existe Agente
   {   
      if(!strcmp(sAgcMod.c_str(), m_agcModulo.c_str() ) )
         return true;
      else
        return setVarsErr('N', 4, "CGeneraCert Error en VerificacionDatos: Los Datos del agente no estan actualizados el modulo es distinto al que se envi� con anterioridad."); 
     
   }    
   else  // No Existe
      return true;

}

bool CGeneraCert::
GuardaCertAgente()
{
   string sAgcNumSerie;

   if(m_tipo != 1 && m_tipo != 2)
   {
      switch(m_tipo)
      {
         case  3 : strcpy(m_agcNivel,"3");break;
         case  4 : strcpy(m_agcNivel,"2");break;
         case  5 : strcpy(m_agcNivel,"1");break;
         case  7 : strcpy(m_agcNivel,"7");break;
      }
   }

   // 19/Dic/2006, GHM: se elimina del query el num_serie, "and NO_SERIE like '000010999999%'"  
   if(!m_BDAR->consultaReg(GEN_CERT_OBTEN_ULT_NUM_SERIE_DE_CERTIFICADO_AGC_POR_RFC, m_rfc ))
   {
      Bitacora->escribe(BIT_ERROR, "CGeneraCert Error en GuardaCertAgente: Error al consultar el n�mero de serie del certificado del AGC por RFC");
      return false;
    }
   
   if(!m_BDAR->getValores("b" ,m_agcNumSerie ))
   {
      Bitacora->escribe(BIT_ERROR, "CGeneraCert Error en GuardaCertAgente: Error al obtener el n�mero de serie del certificado de la BD de AR");   
      return false;
    }

   if(!m_BDAR->consultaReg(GEN_CERT_OBTEN_NUM_SERIE_AGENTE_POR_CLAVE, m_agcRfcRec.c_str() ))
   {
      Bitacora->escribe(BIT_ERROR, "CGeneraCert Error en GuardaCertAgente: Error al consultar el n�mero de serie del certificado del AGC por Clave");
      return false;
    }
   if(!m_BDAR->getValores("s" ,&sAgcNumSerie ))
   {
      Bitacora->escribe(BIT_ERROR, "CGeneraCert Error en GuardaCertAgente: Error al obtener el n�mero de serie del certificado de la BD de AR");     
      return RegistraAGC();
    }
   else
      return ActualizaAgc();
    
   return true;
}

// MELM 20/02/2007: Quita los espacios en blanco al final de un string, por error en la Generaci�n de CDs de FEA y Sellos.
void CGeneraCert::EliminaEspacios(string &str)
{
   if(str.empty())
      return;
   std::string::iterator p = str.begin();
   while(*p && *p == ' ')
   {
      str.erase(p);
   }
   p = str.end();
   p--;
   while(*p && *p == ' ')
   {
      str.erase(p--);
   }   
   /*string::size_type lsb = str->find_last_not_of(' ') + 1;
   if (lsb < str->length())
      str->resize(lsb);*/
}

// Valida q los datos del req. d Sellos, no hayan cambiado con respecto al Certif. Activo del contribuyente
int CGeneraCert::ValidaDatosREQSellos(bool esPF)
{
   char     curp_contr[19], rfc_RL[14], curp_RL[19];

   string s_Nombre_Organiz = (esPF) ? m_reqNombreComun : m_reqOrganizacion ;
   EliminaEspacios( s_Nombre_Organiz );   
   if ( !m_BDAR->consultaReg(GEN_CERT_OBTEN_DATOS_CERTIFICADO_POR_RFC_Y_TIPO, m_rfc, FEA) || !m_BDAR->getValores("bbbb", curp_contr, m_Nombre, rfc_RL, curp_RL) )
      return setVarsErr('I', ERR_GET_DAT_CERT_ACTIVO, VR_LET "\tCGeneraCert Error en validaDatosREQSellos: Los datos del certificado activo no se pueden" 
      "recuperar");
   //>>> MAML 080423: por si el nombre es >= 64 y exactamente en esta posicion tiene un espacio en blanco => insertamos un string de ayuda:
   m_Nombre[64] = 0;
   string s_Nombre_DB = m_Nombre;
   EliminaEspacios( s_Nombre_DB );
   // La validaci�n de comparar los RFCs ya se hizo antes i.e. m_rfc vs m_reqRFC
   if ( ( curp_contr[0] && strcmp(curp_contr, m_reqCURP  ) ) ||
        ( m_Nombre[0]   && (          strncmp( s_Nombre_DB.c_str(), s_Nombre_Organiz.c_str() , 64 )    
                                 /*   strncmp( m_Nombre, s_Nombre_Organiz.c_str() , 64 )
                            (esPF  && strncmp( m_Nombre, m_reqNombreComun.c_str() , 64 ) ) ||
                            (!esPF && strncmp( m_Nombre, m_reqOrganizacion.c_str(), 64 ) ) */
                           )                             ) ||
        ( rfc_RL[0]     && strcmp(rfc_RL,     m_reqRFCRL ) ) ||
        ( curp_RL[0]    && strcmp(curp_RL,    m_reqCURPRL) )  )
   {
     sprintf (m_errDesc, "\tRequerimiento de Sellos generado con datos inactivos; \tcurp=%s vs m_CURP=%s,\tnombre=%s vs m_nombre=%s,\trfc_RL=%s vs m_rfc_RL=%s,"
     "\tcurp_RL=%s vs m_curp_RL=%s",
         curp_contr, m_reqCURP, s_Nombre_DB.c_str(), s_Nombre_Organiz.c_str(), //m_Nombre, (esPF)? m_reqNombreComun.c_str():m_reqOrganizacion.c_str(),
         rfc_RL, m_reqRFCRL, curp_RL, m_reqCURPRL );
     sprintf(m_errDesc,"CGeneraCert Error en validaDatosREQSellos: ",m_errDesc); 
     return setVarsErr('N', REQSELLOS_DATOSDIF_CERTENS, m_errDesc );
   }  
   return(0);
}
 
// Valida q el RFC del repres. Legal sea Igual al del req. de RENOV, con respecto al Certif. Activo del contribuyente
int CGeneraCert::ValidaDatosREQRenov()
{
   char     rfc_RL[14];
 
   if ( !m_BDAR->consultaReg(GEN_CERT_OBTEN_RFCRL_CERTIFICADO_POR_RFC_Y_TIPO, m_rfc, FEA) || !m_BDAR->getValores("b", rfc_RL) )
      return setVarsErr('I', ERR_GET_DAT_CERT_ACTIVO, VR_LET "\tCGeneraCert Error en validaDatosREQRenov: Los datos del certificado activo no se pueden" 
      "recuperar");
   // La validaci�n de comparar los RFCs ya se hizo antes i.e. m_rfc vs m_reqRFC
   if ( strncmp(rfc_RL, m_reqRFCRL, 13) ) // Si es DIFERENTE el Rep. legal anterior con el Actual => Error
   {
     sprintf (m_errDesc, "CGeneraCert Error en validaDatosREQRenov: Requerimiento de Renovaci�n generado con diferente representante legal; antes=%s vs ahora=%s",
              rfc_RL, m_reqRFCRL );
     return setVarsErr('N',REQRENOV_DIFER_RFCRL, m_errDesc );
   }
   return(0);
}

bool  CGeneraCert::ActualizaAgc()
{
   if( m_BDAR->ejecutaOper(GEN_CERT_ACTUALIZA_AGENTE_POR_CLAVE, m_agcNombre.c_str() , m_agcApPat.c_str(), m_agcApMat.c_str(), 
                           m_agcModulo.c_str() , m_agcNumSerie, m_agcCve, m_numTram, m_agcNivel, m_agcRfcRec.c_str() )  )
   {
     if(GuardaArchivo())
     {
        Bitacora->escribePV(BIT_INFO ,"CGeneraCert ActualizaAGC  %s actualizado  en BD y guardado  en  repositorio.", m_agcRfcRec.c_str() );      
        return true; 
     }
   }
   else
   {
      Bitacora->escribePV(BIT_ERROR ,"CGeneraCert Error en ActualizaAgc: Error en la actulizaci�n del  AGC  %s. ", m_agcRfcRec.c_str() );
      return false;
  
   }
   return false;
}

bool  CGeneraCert::GuardaArchivo()
{

   ManArchivo arch;
   string sRutaAgc;
   m_Configuracion.getValorVar("[LLAVES_MENSAJES_SRV]","RCERAGCS" ,&sRutaAgc);
   sRutaAgc = sRutaAgc + "//" +m_agcNumSerie + ".cer";
   if (arch.CreaArchivo((uchar*)m_cert,m_lcert , (char*)sRutaAgc.c_str() ))
   {   
      Bitacora->escribePV(BIT_INFO ,"CGeneraCert en GuardaArchivo: Certificado del AGC  %s Guardado  en  repositorio.", m_agcRfcRec.c_str() );
      return true;
   }
   else
   {
      Bitacora->escribePV(BIT_ERROR ,"CGeneraCert Error en GuardaArchivo: Error [%d] al guardar el certificado del  AGC  %s en el  repositorio.", errno, 
      m_agcRfcRec.c_str() );
      return false;

   }  

}

bool CGeneraCert::RegistraAGC()
{
   int sResp;

   bool ok_ejecuta = false;

   #if ORACLE
      ok_ejecuta = m_BDAR->ejecutaFuncion(GEN_CERT_SP_AGREGA_AGENTE_NOMBRE, GEN_CERT_SP_AGREGA_AGENTE_RETURN, GEN_CERT_SP_AGREGA_AGENTE_PARAMS, m_agcRfcRec.c_str() , m_agcNombre.c_str(),
                                                                 m_agcApPat.c_str(), m_agcApMat.c_str() , m_agcModulo.c_str() ,
                                                                 m_agcNumSerie , m_agcNivel , m_agcCve ,m_numTram);
   #else
      ok_ejecuta = m_BDAR->ejecutaSP(GEN_CERT_SP_AGREGA_AGENTE_NOMBRE, GEN_CERT_SP_AGREGA_AGENTE_PARAMS, m_agcRfcRec.c_str() , m_agcNombre.c_str(),
                                                                 m_agcApPat.c_str(), m_agcApMat.c_str() , m_agcModulo.c_str() ,
                                                                 m_agcNumSerie , m_agcNivel , m_agcCve ,m_numTram);
   #endif

  if(ok_ejecuta)
  {
     if( m_BDAR->getValores("i",&sResp))
     {
        if(sResp == 0)
           return GuardaArchivo();
        else
        {
           Bitacora->escribePV(BIT_ERROR ,"CGeneraCert Error en RegistraAGC: Error  %d  al registrar el agente %s en la BD.",sResp, m_agcRfcRec.c_str());
           return false;
        }
     }else Bitacora->escribe(BIT_ERROR ,"CGeneraCert Error en RegistraAGC: Error  No se obtuvieron los valores al ejecutar el sp.");
  }else Bitacora->escribe(BIT_ERROR ,"CGeneraCert Error en RegistraAGC: Error  No se ejecut� el sp.");

  return false;
}

int CGeneraCert::generaNuevoRequerimiento( char* rfcRL, SNames sujeto[], int isujeto, SNames atrib[], int iatrib)
{

   SGIX509     ReqCert;
/*
   SNames      pwd[1];
   SNames      correo[3];
   //int         icorreo[3] = {48,503,105};
   int         icorreo[3] = {ID_EMAIL,ID_RFC,ID_CURP};
   //int         ipwdr[1]   = {54};
   int         ipwdr[1]   = {ID_CHPWD};

   error = ReqCert.ProcREQ( bReq,iReq ,correo, pwd, &llavepb,icorreo, 3,ipwdr,1);
   if(error != 0)
      return error;

static int snids [] = { ID_RFC, ID_CN, ID_ORG, ID_ORGU, ID_EMAIL, ID_NAME, ID_CURP};
*/
   SNames sujetoRenovado[3];
   char   bCURP[40];
   char   bRFC[40];
   string sCURP_rec; 
   string sCURP;
   int error;

   sujetoRenovado[0].dato = sujeto[4].dato;
   sujetoRenovado[0].id = "emailAddress";
   sujetoRenovado[0].idNid = ID_EMAIL;

   if ( !m_utilBDAR->getCURP(rfcRL, &sCURP ))
   {
      Bitacora->escribe(BIT_ERROR ,"CGeneraCert error en GeneraNuevoRequerimiento: No se obtuvo CURP ");
      return -1;
   }
   Bitacora->escribe(BIT_INFO ,"CGeneraCert GeneraNuevoRequerimiento: genera NuevoRequerimiento");
   // Persona Moral

   //m_reqRFC  m_reqRFCRL
   //if (m_reqRFC != null && m_reqRFCRL != null) 
   if ( strlen ( sujeto[0].dato ) == 12  )
   { 
      sprintf(bCURP," / %s", sCURP.c_str()); 
      sprintf(bRFC,"%s / %s", sujeto[0].dato ,rfcRL);
   }
   // Persona Fisica con Representante Legal
   if ( strlen ( sujeto[0].dato ) ==  13 )
   {
      sprintf(bCURP,"%s / %s", sujeto[6].dato, sCURP.c_str());
      sprintf(bRFC,"%s / %s", sujeto[0].dato ,rfcRL);
   }

   sujetoRenovado[2].dato= bCURP;
   sujetoRenovado[2].id= "serialNumber";
   sujetoRenovado[2].idNid= ID_CURP;

   sujetoRenovado[1].dato = bRFC;
   //sujetoRenovado[1].dato = sujeto[0].dato;
   sujetoRenovado[1].id = "x500UniqueIdentifier";
   sujetoRenovado[1].idNid = ID_RFC;

   error=   ReqCert.GenREQ_REQ((const uchar*)m_req,m_lreq,(uchar*)bNvoReq, &iNvoReq, sujetoRenovado,3,atrib,iatrib,4,false);
   if (error == 0)
      reqNvo =  true; 
   return error;

}

bool CGeneraCert::
ValidaBloqueado(char* rfc)
{
   Bitacora->escribePV(BIT_DEBUG, "CGeneraCert en ValidaBloqueado: ValidaBloqueado %s ",rfc);
   string edoRFC;   
   if ( !m_BDAR->consultaReg(GEN_CERT_OBTEN_ESTADO_RFC, rfc)) 
      return setVarsErr('I', ERR_GET_DAT_CERT_ACTIVO, VR_LET "\tCGeneraCert Error en ValidaBloqueado: No se pudo obtener el estado del rfc en la tabla bloqueado");
   if(!m_BDAR->getValores("s", &edoRFC))
      return false;
   if (edoRFC.c_str() == "B")
   { 
      Bitacora->escribePV(BIT_DEBUG, "CGeneraCert en ValidaBloqueado: Estado del RFC [%s]",edoRFC.c_str());
      return true;
   }else 
   return false;
  
}


