static const char* _CRENOVACION_CPP_VERSION_ ATR_USED = "SrvAR @(#)"\
"DSIC09041AR_ 2007-12-13 CRenovacion.cpp 1.1.1/3";

//#VERSION: 1.1.1
#include <assert.h>

#include <Sgi_BD.h>
//- #include <CConfigFile.h> //- GHM (070430)
#include <Sgi_ConfigFile.h>    //+ GHM (070430)
#include <CAcuses.h>
#include <CGeneraCert.h>
#include <CSolCert.h>

#include <CArchivoEnsob.h>
#include <CRenovacion.h>

#include <Sgi_PKI.h>       //+ GHM (070321): Para utilizar la funci�n CreaDir

#if ORACLE
   #include <CosltSrvAR_ORA.h>   // + JAB (20141014)
#else
   #include <CosltSrvAR.h>   // + JAB (20141014)
#endif

/* *********************************************************************** */
CRenovacion::CRenovacion(CConfigFile& config, MensajesSAT& msgcli, CSSL_Skt* skt):
   CArchivoEnsob(config, msgcli, skt)
{
   m_tipo = TIPCER_FEA; // SIEMPE VA A SER DEL TIPO 1 DE FEA
   memset(cArchRenov , 0, MAX_RUTA_ARCH);
}

CRenovacion::~CRenovacion()
{
   if (cArchRenov[0] && m_numTram[0] )
      // MAML 080902: ahora respalda el archivo ensobretado. // unlink(cArchRenov);
      AlmacenaEns( cArchRenov, ".ren" );
}

/* *********************************************************************** */
// FUNCION INICIAL
// llama a los 3 primeros mensajes iniciales para recibir el archivo ensobretado
// Recibe el mensaje SOLREN y debiera hacer validaciones y dejarlo listo para Genenar el certificado
bool CRenovacion::preProceso()
{
   assert(m_MsgCli.tipOperacion() == SOLREN);

   if ( !error && msgRecibeArch() && DescomprimeReqRenov() ) 
      IniciaFin_preProceso();

   if (error)
      msgCliError(m_errTipo, m_errDesc, error, eAPL); 
   return( !error );
}

// FUNCION PRINCIPAL de la clase Derivada
bool CRenovacion::Proceso()
{
   Bitacora->escribe(BIT_INFO, "CRenovacion en Proceso: Antes llamar a GeneraRenov() y se supone q debe mandar CERTREN");
   if ( !error )
      GeneraRenov(); // Trata de Generar la RENOV. del Certificado, y Verifica q se genere

   if (error)
      msgCliError(m_errTipo, m_errDesc, error, eAPL); // Para q llamarla en tanto lugar
   return( !error );
}

/* *********************************************************************** */
// Digestion del archivo .REN se hace aqui del lado de la AR
int CRenovacion::get_digArchivoREN(const char *cBufArch, int iBufArch)
{
   SHA_CTX	m_ctx;
   uchar	uDigArch[SHA_DIGEST_LENGTH];
   uint		i_DigArch = sizeof(uDigArch);
   uint16	idig_archivo = sizeof(dig_archivo);

   SHA1_Init(&m_ctx);
   SHA1_Update(&m_ctx, cBufArch, iBufArch);
   SHA1_Final(uDigArch, &m_ctx);
   if (!B64_Codifica( (uint8*)uDigArch, (uint16)i_DigArch, dig_archivo, &idig_archivo))
      error = ERR_NO_APL_B64;
   return(error);
}

/* *********************************************************************** */
// Recibe el archivo de renovacion en un Buffer y procedemos a VALIDARLO y luego RENOVARLO
bool CRenovacion::msgRecibeArch()
{
   int		iref_arch=sizeof(ref_arch); 	   
   static char	cBufArch[sizeof(m_MsgCli.buffer)];
   int		iBufArch = sizeof(cBufArch);
   string claves[] = { "PATH_TMPSDIG" },  spath_TmpSDig;
   //AGZ
   char m_rfcTmp[31];
   char *r = NULL;
   //Fin.

   m_ifirmaOp = MAX_TAM_FIRMA_B64; 
   m_irfcOp   = sizeof(m_rfcTmp);

   error = m_MsgCli.getMensaje(SOLREN, m_firmaOp, &m_ifirmaOp,
                                       m_rfcTmp,  &m_irfcOp, // TipoCert: siempre es FEA
                                       ref_arch,  &iref_arch, cBufArch, &iBufArch); 
   
   if (error)	
      setVarsErr('I', error, "Error en CRenovacion en msgRecibeArch: Posible mensaje mal formado, al decodificar mensaje SOLREN");
   else if ( ( error = m_Configuracion.getValoresConf("[AR_ALMACENA_SELLOS]", 1, claves, &spath_TmpSDig )) != 0 ) 
   {
      setVarsErr('I', error, "Error en CRenovacion en msgRecibeArch: Error al obtener el valor de PATH_TMPSDIG en [AR_ALMACENA_SELLOS] en getValoresConf()");
      return error;
   }


   //AGZ:
   if (strlen(m_rfcTmp) > 13)
   {
      r = strtok(m_rfcTmp, " ");
      if (r)
         strcpy(m_rfcOp, r);
   }
   else 
      strcpy(m_rfcOp, m_rfcTmp);   
   //Fin.

   if (!error && !get_digArchivoREN(cBufArch, iBufArch) )
   {
      //- CreaDir ( (char *)spath_TmpSDig.c_str() );  //- GHM (070321)
      pkiCreaDir ( (char *)spath_TmpSDig.c_str() );   //+ GHM (070321): Para utilizar la funci�n definida en Sgi_PKI
      sprintf (cArchENS, "%s%s_XXXXXX", spath_TmpSDig.c_str(), m_rfcOp); // m_Configuracion.get("PATH_TMPSDIG")
      mkstemp(cArchENS);

      iFd = open(cArchENS, O_WRONLY | O_CREAT | O_TRUNC | O_APPEND, S_IRUSR | S_IWUSR);
      if (iFd == -1)  
      {
         error = sgiError(0,eERRNO,errno);
         return setVarsErr('I', error, "Error en CRenovacion en msgRecibeArch: Error al crear archivo temporal de renovaci�n de certificado");
      } 
      Bitacora->escribePV(BIT_INFO,"CRenovacion en msgRecibeArch: Creaci�n del archivo temporal de recepci�n de solicitud de renovaci�n de certificado: %s "
                                , cArchENS);

      if (write(iFd, cBufArch, iBufArch) != iBufArch)   
      {
         cerrarYeliminarArch();
         error = sgiError(0,eERRNO,errno);
         return setVarsErr('I', error, "Error en CRenovacion en msgRecibeArch: Error al escribir en el archivo temporal de renovaci�n de certificado");
      }
      close(iFd);
      iFd = -1;

      if ( !valArchEns() )  //Se hace esta validacion p/NO mandar un No. d Operacion y por tanto un ACUSE en el CW
         cerrarYeliminarArch();
   }
   return !error;
}

// Funcion para llamarse desde la funcion de Preproceso y descomprime el archivos .req
bool CRenovacion::DescomprimeReqRenov()
{
   string  claves[] = { "PATH_PROCESANDO" }, spath_Procesando;
   string proceso;

   if ( !(error =  m_Configuracion.getValoresConf("[AR_ALMACENA_SELLOS]", 1, claves, &spath_Procesando )) )  
   {
      //- CreaDir ( (char *)spath_Procesando.c_str() ); //- GHM (070321)
      pkiCreaDir ( (char *)spath_Procesando.c_str() ); //+ GHM (070321): Para utilizar la funci�n definida en Sgi_PKI
      sprintf(cArchRenov, "%s%s.ren", spath_Procesando.c_str(), m_rfcOp); //Las rutas traen al final el /  
      sprintf(cArchREQ, "%s%s.req", spath_Procesando.c_str(), m_rfcOp); 
      if ( !(error = rename(cArchENS, cArchRenov)) ) 
      { // Aqui en RENOV. es necesario renombrar el arch. ensobretado ??
         strcpy(cArchENS, cArchRenov);	// Solo se movio de lugar, pero sigue siendo el archivo ensobretado
         //valArchEnsAutentico(cArchREQ);
         proceso="RENOVA";
         sprintf (m_errDesc, "Error en CRenovacion en DescomprimeReqRenov: no se puede renombrar el archivo .ren:%s ",proceso.c_str());
         valArchEnsAutentico(cArchREQ,proceso.c_str()); //ASU 12-12-2013 CAMBIO PARA PERMITIR RENOVAR CERTIFICADOS CADUCOS 			
      } // end d rename()
      else 
      {
         sprintf (m_errDesc, "Error en CRenovacion en DescomprimeReqRenov: no se puede renombrar el archivo .ren:%s por %s", cArchENS, cArchRenov);
         setVarsErr('I', error, m_errDesc);
         unlink(cArchENS);  // BORRA EL ARCHIVO QUE NO SE PROCESO, por que no se pudo mover
      }	
   }
   else
      setVarsErr('I', error, "Error en CRenovacion en DescomprimeReqRenov: al obtener el valor de PATH_PROCESANDO en [AR_ALMACENA_SELLOS] en getValoresConf()");
 
   return !error;
}

// Envia de la AR al Mediador (IES): Inicia el Proceso de Renovacion de Certificado
bool CRenovacion::GeneraRenov()
{
   Bitacora->escribePV(BIT_INFO, "CRenovacion en GeneraRenov: Inicia el proceso de renovaci�n de certificados");
   if ( (error = (m_utilBDAR->RegOperDetalleBDAR(SOLREN, m_numOp)== -1)) != 0 ) 
   {
      sprintf(m_errDesc, "Error en CRenovacion en GeneraRenov: no puede registrar el dato SOLREN en la DB de la AR con m_numOp=%s", m_numOp);
      setVarsErr('I', error, m_errDesc);
      return !error;
   }

   // Ya con el nombre del arch .req, lo manda procesar
   if ( !(error = ValidaSujeto()) && !(error = leeReq(cArchREQ)) && !(error = AlmacenaReq(0, true)) && 
        !(error = ValidaReq())    && !(error = ApartaLlave())    && !(error = Genera())             &&  regRevDetBD() ) 
   { 
      // YA TENGO LA FECHA DE OPERACION d CERTREN => PUEDO GENERAR su ACUSE
      m_utilBDAR->RegOperDetalleBDAR( CERTREN, m_numOp);
      CAcuses m_acuse(m_BDAR, m_Configuracion, m_numOp, m_numTram);
      Bitacora->escribePV(BIT_INFO, "CRenovacion en GeneraRenov: Inicia el proceso de generar el acuse de Renovaci�n");
      if ( genAcuseRenov( &m_acuse ) ) 
      {	
         m_MsgCli.setMensaje(CERTREN, //m_rfc, strlen(m_rfc),  Parametro YA NO usado
                              m_acuse.cad_orig, strlen(m_acuse.cad_orig),
                              m_acuse.firma,    strlen(m_acuse.firma),
                              m_cert,   m_lcert	  );
         envia(CERTISAT);
      }
   }

   if (!error) 
   {
      unlink(cArchREQ); // borra EL ARCHIVO DE REQUERIMIENTO Q SE PROCESO BIEN
   }   
   else // con error DEJA el archivo DE REQUERIMIENTO donde hubo ERROR en PATH_PROCESANDO
      // m_utilBDAR->RegOperDetalleBDAR( MSGERROR, m_numOp, CERTNOREN); 
      // MAML 16/Enero/2007: msgCliError() q se llama despes YA hace RegOperDetalleBDAR(), unicamente asignamos el error
      error = (error == 1) ? CERTNOREN : error;
         
   return !error;
}


// -----------------------------------------------------------------------------------------------------------
//	LAS SIGUIENTE FUNCION ME AYUDAN A GENERAR EL ACUSE DE RENOVACION  
bool CRenovacion::genAcuseRenov(CAcuses *pacuse)
{
   //	Y la clase CRenovacion de la AR debe obtener los parametros faltantes (!= "" � 0 )
   string sNo_Serie, sNombre, sPubK, sModulo, sNombreRL, sRFCRL;
   char   nameArch[MAX_RUTA_ARCH];

   if ( (error = !getAcuseDatos(&sNo_Serie, &sNombre, &sPubK, &sModulo, &sNombreRL, &sRFCRL)) != 0)
      return (!error);	

   quitaRutaArch(ref_arch, nameArch);
  
   if ( m_utilBDAR->getNivelAGC(m_agcCve) == 6 )
   { // Es una RENOVACION desde el Certisat-WEB
      Bitacora->escribePV(BIT_INFO,"CRenovacion en genAcuseRenov: Renovacion desde el Certisat-WEB");
      error = pacuse->generaAcuse(CERTREN, RENOVACION_CERT_DIG,  m_rfcOp, sNombre.c_str(), 
				  m_numSerRenSellos, sPubK.c_str(), "", "", 
                                  MODULO_CERTISAT_WEB, m_tipo, -1, nameArch , dig_archivo);
    }
    
   else
   {	// Es una RENOVACION desde el Certisat-Ventanilla
      Bitacora->escribePV(BIT_INFO,"CRenovacion en genAcuseRenov: Renovacion desde el Certisat-Ventanilla");
      error = pacuse->generaAcuse(CERTREN, RENOVACION_CERT_DIG,  m_rfcOp, sNombre.c_str(), 
                                  m_numSerRenSellos, sPubK.c_str(), sRFCRL.c_str(), sNombreRL.c_str(),  
                                  sModulo.c_str(), m_tipo, -1, nameArch , dig_archivo);
    }
    
   if ( error != 0 )
   {
      sprintf (m_errDesc, "Error en CRenovacion en genAcuseRenov: no pudo generarse el Acuse de Renovaci�n en la DB, numTramite=%s", m_numTram );
      setVarsErr('I', error, m_errDesc); // msgCliError('I', m_errDesc, ERR_INS_ACUSE_DB, eAPL);
   }
   else 
      Bitacora->escribePV(BIT_INFO, "CRenovacion en genAcuseRenov: OK se gener� Acuse de Renovaci�n en la AR, numTramite=%s, cad_orig=%s, firma=%s", 
                                    m_numTram, pacuse->cad_orig, pacuse->firma );
   return (!error);
}
// OjO: Si se quita la funcion EsTramiteWEB() => genAcuseRenov() la puedo poner en la clase CArchivoEnsob
//		Si se podria pero too seria un pasadero de todos los 13 parametros q necesita generaAcuse(...)
// -----------------------------------------------------------------------------------------------------------

//>>> MAML (070604) RELACIONADO CON EL ACUSE DE REVOCACION EN UN PROCESO DE RENOVACION.
int CRenovacion::regRevDetBD()
{
   int  TRAM_RENOV = 6;
   char MEDIO_REN = 'R';
   char *no_serie_orig = ( strchr( m_numSerRenSellos, '/' ) + 2 );
   char cad_almc[256];

   if( !no_serie_orig )
   {
      sprintf (m_errDesc, "Error en CRenovacion en regRevDetB: No se pudo procesar la cadena para obtener el numero de serie: %s", m_numSerRenSellos );
      setVarsErr('I', error = NSERIE_ANTERIOR_REG_REVREN, m_errDesc); 
   }
   else 
      sprintf( cad_almc, RENOV_AGREGA_REVOCACION, m_numTram, no_serie_orig, MEDIO_REN, "", TRAM_RENOV, "" );
   if( !error && !m_BDAR->ejecutaOper(cad_almc) )
   {
      sprintf (m_errDesc, "Error en CRenovacion en regRevDetB: Error al registrar revocacion a detalle. No se pudo insertar en la base de datos: %s", cad_almc );
      setVarsErr('I', error = ERR_DB_REG_REV_DET, m_errDesc);
   }
   return !error;   
}
//<<<
