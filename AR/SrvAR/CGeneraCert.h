/*######################################################################################################################
  ###  PROYECTO:              PKI-SAT                                                                                ###
  ###  MODULO:                Clase que realiza la Generaci�n de CDs                                                 ###
  ###                                                                                                                ###
  ###  DESARROLLADORES:       Gudelia Hern�ndez Molina        GHM                                                    ###
  ###                         H�ctor Ornelas Arciga           HOA                                                    ###
  ###  FECHA DE INICIO:       Jueves 26, enero del 2006                                                              ###
  ###                                                                                                                ###
  ######################################################################################################################
         1         2         3         4         5         6         7         8         9        10        11        12
123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
 #######################################################################################################################
   VERSION:
      V.1.00      (20060126 - 20060302) HOA: Primera Versi�n
   CAMBIOS:
      V.1.1.1     (100413) MAML: se incorpora la clase CSeparaToken y se elimina SeparaCad
           .3     (20141014 -         ) JAB: Se agrupan las sentencias SQL utilizadas en el SrvAR en un archivo de
                                              cabecera
######################################################################################################################*/
#ifndef _CGENERACERT_H_
#define _CGENERACERT_H_
static const char* _CGENERACERT_H_VERSION_ ATR_USED = "SrvAR @(#)"\
"DSIC10312AR_ 2010-08-04 CGeneraCert.h 1.1.2/5";

//#VERSION: 1.1.1
//######################################################################################################
#include <SgiTipos.h>
#include <COperacion.h>
#include <DefDatos.h>
//######################################################################################################
#define ERR_CR_PROCREQ   120
#define ERR_CR_RSANEW    121
#define ERR_CR_INFINC    122
#define ERR_CR_IDINV     123
#define ERR_CR_RFCINV    124
#define ERR_CR_CURPINV   125
#define ERR_CR_TAMLLAVE  126

#define ERR_VR_RFCDIF    127
#define ERR_VR_NOEMAIL   128
#define ERR_VR_NOCN      129
#define ERR_VR_NOORG     130
#define ERR_VR_NOUNIDAD  131
#define ERR_VR_NONOMBRE  132
#define ERR_VR_RLSINFEA  133

#define ERR_DM_DIGMOD    134
#define ERR_DM_B64DIG    135

#define ERR_AL_SETPUB    136
#define ERR_AL_ARINI     137	// MELM YA NO SE USA
#define ERR_AL_ARAL      138

#define ERR_GE_INIMED    139  // error al iniciar  mediador
#define ERR_GE_INTWS     140  // error al consultar en ws 
#define ERR_GE_NOREGWS   141  // error no registro en ws 
#define ERR_GE_SETMSGMED 142
#define ERR_GE_ENVMED    143
#define ERR_GE_RECMED    144
#define ERR_GE_ARMED     145
#define ERR_GE_GETMSGMED 146
#define ERR_GE_RECMEDERR 147

#define ERR_GET_DAT_CERT_ACTIVO     148

#define ERR_GC_INTERNO  149
#define ERR_RFC_BLOQ    150
#define ERR_BUFFER_FTO  151    // el buffer no cumple con el formato dise�ado 

#define TAM_NOMBRE      660    // Longitud maxima del nombre del Contribuyente en la tabla CERTIFICADO de la BD de la AR 
                               // Proximamente se ajustara al Web Services del SAT; PM: 255+1+50 = 306, PF = 254+1+200+1+200 = 654 

//######################################################################################################
typedef enum { xRFC = 0, xCURP } PROC_ID;
//######################################################################################################
using namespace std;
class CGeneraCert : public COperacion
{
   //VARIBLES
   public:
   protected:
      //>>- GHM (090402): Se env�o a la clase CSolCert
         //>>+ GHM (081215): Para la sustituci�n de validaciones en DARIO
         //CValDatRFC_FIEL *m_valDat_FIEL;
         //CValDatRFC_SDG  *m_valDat_SDG;
         //+<< GHM (081215)
      //>>+ GHM (090402)
      char         m_rfc[14]; // MAML 100409: esta variable p/cambiarla a string, se debe de cambiar aqui y en las clases derivadas 
      int          m_tipo;
      uint8       *m_req;
      uint8       *bNvoReq;
      int          iNvoReq;
      int          m_lreq;
      int          m_operSec;
      uint8       *m_cert;
      int          m_lcert;
      string       m_rfcext;
      //> Datos si sol de AGC      
      string       m_agcNombre;
      string       m_agcApPat;
      string       m_agcApMat;
      string       m_nombre;
      string       m_agcRfcRec;  // char         m_agcRfcRec[9];
      string       m_agcModulo;  // char         m_agcModulo[4]; 
      char         m_agcNumSerie[21];
      char         m_agcNivel[2];
      //< Datos si sol de AGC      
      char         m_Nombre[660]; // Cambio de 255 a 660 por nombre largo.
     
      //> Datos extraidos del requerimiento
      RSA         *m_pubKey;
      char         m_reqRFC[14];
      char         m_reqRFCRL[14];
      char         m_reqCURP[19];
      char         m_reqCURPRL[19];
      string       m_reqEMail;
      string       m_reqNombreComun;
      string       m_reqOrganizacion;
      string       m_reqUnidad;
      string       m_reqNombre;
      char         m_reqPwdRev[29];
      char         m_reqModMd5[25];
      char         m_reqModSha1[29];
      //< Datos extraidos del requerimiento
      //> Variables que contienen el error recibido en MSGERROR 
      char         m_errTipo;
      int          m_errCod;
      char         m_errDesc[512];
      //< Variables que contienen el error recibido en MSGERROR 
      
      char         m_numSerRenSellos[50];  // Var. q guarda los 2 numeros de Serie necesarios en RENovacion o de Sellos
      bool         reqNvo;
         
   private:
   //FUNCIONES
   public:
      CGeneraCert(CConfigFile& config, MensajesSAT& msgcli, CSSL_Skt* skt);
      virtual ~CGeneraCert();
   protected:
      virtual int getOperCodSol() {return SOLCERTAR;}
      virtual int getOperCodRes() {return CERTGENAR;}   
      
      bool setMemReqYCert();
      void limpiaVarsErr();
      int  setVarsErr(char tipo, int codigo, const char* desc);
      int  CargaReq(); 
      void LimpiaVarsReq(bool soloMem = false);
      bool ProcesaId(PROC_ID id, const char* dato);
      int  EnviaSolMed();
      int  RecibeRespMed();
      int  generaDigs();
      int  valLlaveUnica();
      
      //>>- GHM (090302): Se env�o a CSolCert
         //>>+ GHM (081216): Para la sustituci�n de validaciones en DARIO
         //bool IniValidaWS_IDC();
         //bool IniValidaSDWS_IDC();
         //bool FinValidaWS_IDC();
         //+<< GHM (081216)
      //<<- GHM (090302)
      int  GetRFCBloqueado(const char *RFC, bool esRepLegal = false );
      int  ValidaReq();
      int  ApartaLlave();
      int  Genera();
      //bool Registra_RFC(); //- GHM (090302): Se env�o a CSolCert
      //int  SeparaCad(int iPipes ,char *cCad ,...); // MAML 100409: se elimina su uso
      bool VerificacionDatos(char *cCadRec );
      bool GuardaCertAgente();
      bool RegistraAGC();
      bool GuardaArchivo();
      bool ActualizaAgc();
      int  generaNuevoRequerimiento( char* rfc, SNames sujeto[], int isujeto, SNames atrib[], int iatrib);
      bool ValidaBloqueado(char* rfc); 

   private:
      int   digModuloPub(SGIRSA* rsa, SGIPUBLICA *pubKey, int alg_id, char* digestion, uint16 *lngdigestion);
      void  EliminaEspacios(string &str);
      int   ValidaDatosREQSellos(bool esPF);
      int   ValidaDatosREQRenov();
};
//######################################################################################################
#endif // _CGENERACERT_H_
