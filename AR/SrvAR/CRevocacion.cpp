static const char* _CREVOCACION_CPP_VERSION_ ATR_USED = "SrvAR @(#)"\
"DSIC09041AR_ 2007-12-13 CRevocacion.cpp 1.1.2/3";

//#VERSION: 1.1.2
/*##############################################################################
  ###  PROYECTO:              PKI-SAT                                        ###
  ###  MODULO:                Servicio AR para evocacion de Certificados     ###
  ###                         Digitales por oficio y por clave de revocacion ###
  ###                                                                        ###
  ###                                                                        ###
  ###  DESARROLLADORES:       Amilcar Guerrero Zu�iga         AGZ            ###
  ###                                                                        ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       Miercoles 14, diciembre del 2005               ###
  ###                                                                        ###
  ##############################################################################
         1         2         3         4         5         6         7         8
12345678901234567890123456789012345678901234567890123456789012345678901234567890

################################################################################
  VERSION:
     V.1.0.0  (20051214- )    AGZ: Version Original
                              Servicio de Autoridad Registradora para la PKI-SAT
                              Clase para el manejo de revocacion de certificado.

     V.1.1.1.1 (080424) SERR:  Se agregro informacion a las bitacoras.

     V.1.1.1.2 (20141014 -         ) JAB: Se agrupan las sentencias SQL utilizadas en el SrvAR en un archivo de
                                              cabecera

#################################################################################*/

#include <Sgi_BD.h>
//#include <CConfigFile.h>   //- GHM (070430)
#include <Sgi_ConfigFile.h>  //+ GHM (070430)
#include <CAcuses.h>
#include <Sgi_PKI.h>
#include <CRevocacion.h>
#include <Sgi_OpenSSL.h>

int CRevocacion::m_iMsgRecib = -1;

/*##############################################################################
   PROPOSITO: Constructor.
#################################################################################*/

CRevocacion::CRevocacion(CConfigFile& config, MensajesSAT& msgcli, CSSL_Skt* skt):
   COperacion(config, msgcli, skt, true)
{
   m_icveRev = 0;
   m_cadenaOriginal[0] = 0;
   m_motivCve[0] = 0;
   m_numSerOp[0] = 0;
   m_numSerFIEL[0]=0;
   m_refMov[0] = 0;
   m_cadSolRev[0] = 0;
   m_firmaSolRev[0] = 0;
   m_icadOriOp = 0;
   m_irefArchOp = 0;
   m_icadenaOriginal = 0;
   m_imotivCve = 0;
   m_irefMov = 0;
   m_icadSolRev=0; 
   m_ifirmaSolRev =0;
   revFirma = false;
   revJuridica = false;
}

/*##############################################################################
   PROPOSITO: Destructor.
#################################################################################*/

CRevocacion::~CRevocacion()
{
   terminaMed();
   m_iMsgRecib = -1;
}

/*##############################################################################
   PROPOSITO: Este metodo es el encargado de procesar el mensaje recibido 
              del CertiSAT

   PREMISA:
   EFECTO:    Este metodo va a inicializar algunas variables de la clase
              COperacion que deberan ser liberadas en el destructor.
   ENTRADAS:
   SALIDAS:
#################################################################################*/

bool CRevocacion::preProceso()
{
   int errMsgRev = 0;
   switch(m_MsgCli.tipOperacion())
   {
      case SOLREVJUR:
      case SOLREV:
      case SOLREVF:
      case SOLREVOF:
      {
         iniciaVarsOp();
         Bitacora->escribePV(BIT_INFO, "CRevocaci�n en preProceso: Inicializando las variables de Operaci�n (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", 
         m_inumSerOp, m_ifirmaSolRev, m_icadSolRev, m_inumSerOp, m_inumSerFIEL, m_icveRev,m_icadenaOriginal,m_irefMov,m_imotivCve, m_irfcRL,m_iNomRL);
         if (m_iMsgRecib == -1)
            m_iMsgRecib = m_MsgCli.tipOperacion();

         
         if (m_iMsgRecib == SOLREV)
         {
            errMsgRev = sgiErrorBase(m_MsgCli.getMensaje(SOLREV, m_firmaOp, &m_ifirmaOp, m_rfcOp, &m_irfcOp, m_numSerOp, 
              &m_inumSerOp, m_motivCve, &m_imotivCve, m_cveRev, &m_icveRev, m_rfcRL, &m_irfcRL, m_NomRL, &m_iNomRL));
            // V.1.1.1.1 (080424) SERR:  Se agregro informacion a las bitacoras.
            Bitacora->escribePV(BIT_INFO, "CRevocaci�n en preProceso: Datos Recibidos ( %s, %s , %s, %s, %s , %s )", 
                                          m_rfcOp,  m_numSerOp,m_motivCve, m_cveRev, m_rfcRL,  m_NomRL);
         }
         else if (m_iMsgRecib == SOLREVF)
         {
            
            errMsgRev = sgiErrorBase(m_MsgCli.getMensaje(SOLREVF, m_firmaOp, &m_ifirmaOp, m_rfcOp, &m_irfcOp,  m_cadSolRev, &m_icadSolRev, 
                                                                  m_firmaSolRev, &m_ifirmaSolRev));

            if (errMsgRev ==  0)
            {
               Bitacora->escribePV(BIT_INFO, "CRevocaci�n en preProceso: Datos Recibidos ( %s, %s , %s, %s, %s )",
                                          m_rfcOp,  m_cadSolRev, m_firmaSolRev, m_numSerOp,m_numSerFIEL);
               //return procesaRevocacion();//Esta es la que va a hacer todo salvo lo de Base de Datos.
            }
            else
            {
               msgCliError('I', "Error en CRevocaci�n en preProceso: Error al obtener el mensaje.", errMsgRev, eMSGSAT);
               return false;
            }

         }
         else if (m_iMsgRecib == SOLREVOF)
         {
            errMsgRev = sgiErrorBase(m_MsgCli.getMensaje(SOLREVOF, m_firmaOp, &m_ifirmaOp, m_rfcOp, &m_irfcOp, m_numSerOp, 
              &m_inumSerOp, m_motivCve, &m_imotivCve, m_refMov, &m_irefMov, m_rfcRL, &m_irfcRL, m_NomRL, &m_iNomRL));
            //V.1.1.1.1 (080424) SERR:  Se agregro informacion a las bitacoras.
            Bitacora->escribePV(BIT_INFO, "CRevocaci�n en preProceso: Datos Recibidos ( %s,  %s , %s, %s,  %s , %s )",
                                          m_rfcOp,  m_numSerOp,m_motivCve, m_refMov, m_rfcRL,  m_NomRL);
         }
         else  if (m_iMsgRecib == SOLREVJUR)
         {
            /*errMsgRev = sgiErrorBase(m_MsgCli.getMensaje(SOLREVJUR, m_firmaOp, &m_ifirmaOp, m_rfcOp, &m_irfcOp, m_motivCve, &m_imotivCve,
                                                                   m_refMov, &m_irefMov,m_cadSolRev, &m_icadSolRev,m_firmaSolRev, &m_ifirmaSolRev));
            */
            errMsgRev = m_MsgCli.getMensaje(SOLREVJUR, m_firmaOp, &m_ifirmaOp, m_rfcOp, &m_irfcOp, m_motivCve, &m_imotivCve,
                                                                   m_refMov, &m_irefMov,m_cadSolRev, &m_icadSolRev,m_firmaSolRev, &m_ifirmaSolRev);
            if (errMsgRev ==  0)
            {
                Bitacora->escribePV(BIT_INFO, "CRevocaci�n en preProceso: Datos Recibidos SOLREVJUR ( %s, %s , %s , %s ,%s)",m_rfcOp,   
                m_motivCve,m_refMov,m_cadSolRev ,m_firmaSolRev);

            }
            else  
               Bitacora->escribePV(BIT_ERROR,"Error en CRevocaci�n en preProceso: Error %d, f %d rfc %d m %d r %d cad %d fs %d ",errMsgRev,m_ifirmaOp,m_irfcOp,m_imotivCve,m_irefMov,m_icadSolRev,m_ifirmaSolRev );


         }
         
         if(errMsgRev != 0)
         {
            msgCliError('I', "Error en CRevocaci�n en preProceso: Error al obtener el mensaje.", errMsgRev, eMSGSAT);
            return false;
         }


         if (errMsgRev == 0 && m_iMsgRecib == SOLREV)
            m_cveRev[m_icveRev] = 0;
         else m_refMov[m_irefMov] = 0;

         m_motivCve[m_imotivCve] = 0;
         m_firmaOp[m_ifirmaOp] = 0;
         m_numSerOp[m_inumSerOp] = 0;
         m_rfcRL[m_irfcRL] = 0;
         m_NomRL[m_iNomRL] = 0;

         errMsgRev = sgiErrorBase(m_MsgCli.getCadOrig(m_cadOriOp, &m_icadOriOp));
         if (errMsgRev != 0)
         {
            msgCliError('I', "Error en CRevocaci�n en preProceso: Error al obtener la cadena original del mensaje.", errMsgRev, eMSGSAT);
            return false;
         }
 
         m_cadOriOp[m_icadOriOp] = 0;
         m_refArchOp[0] = '\0';
         m_cadMed = string(m_rfcOp) + "|" + string(m_agcCve);
         return true;
      }
      default:
      {
         msgCliError('I', "Error en CRevocacion en preProceso: Error mensaje recibido no esperado.", ERR_MSG_NO_ESPERADO, eAPL);
         return false;
      }
   }
}

/*##############################################################################
   PROPOSITO: Esta funcion es la encargada de llevar a cabo todos los pasos
              para realizar un arevocacion de Certificado Digital.

   PREMISA:
   EFECTO:
   ENTRADAS:
   SALIDAS:
#################################################################################*/

bool CRevocacion::Proceso()
{
   int  error = 0;
   int  secAROp;

   error = iniMed();
   if (error)
      msgCliError('I', "Error en CRevocacion en Proceso: Error al iniciar el mediador", error, eAPL);
   else
   {
      if ( ( secAROp = m_utilBDAR->RegOperDetalleBDAR(m_MsgCli.tipOperacion(), m_numOp, 0) ) < 0)
      {
         msgCliError('I', "Error en CRevocacion en Proceso: Error al insertar el detalle de la operacion.", error = ERR_BD_REG_OPER_SEG, eAPL);
         return false;
      }

      sprintf(m_secOp, "%d", secAROp);
      
      if (m_iMsgRecib == SOLREV)
         error = sgiErrorBase(m_msgMed->setMensaje(SOLREVAR, m_numOp, strlen(m_numOp), m_secOp, m_isecOp = strlen(m_secOp), 
             m_cadMed.c_str(), m_cadMed.length(), m_numSerOp, m_inumSerOp, m_cveRev, m_icveRev));
      else if  (m_iMsgRecib == SOLREVOF )
         error = sgiErrorBase(m_msgMed->setMensaje(SOLREVOFAR, m_numOp, strlen(m_numOp), m_secOp,  m_isecOp = strlen(m_secOp),
            m_cadMed.c_str(), m_cadMed.length(), m_numSerOp, m_inumSerOp));
      else if (m_iMsgRecib == SOLREVF)
      {
         if(procesaRevocacion())//Esta es la que va a hacer todo salvo lo de Base de Datos.
         {
            error = sgiErrorBase(m_msgMed->setMensaje(SOLREVFAR, m_numOp, strlen(m_numOp), m_secOp, m_isecOp = strlen(m_secOp), 
                m_cadMed.c_str(), m_cadMed.length(), m_numSerieRev, strlen(m_numSerieRev)));//, m_cadSolRev, m_icadSolRev, m_firmaSolRev, m_ifirmaSolRev));
            revFirma = true;
         }
         else
         {  
            if(error == ERR_VALIDA_DATOS_MSG)
               msgCliError('I', "Error en CRevocacion en Proceso: Error al procesar la solicitud de revocaci�n.", error, eSGISSL);
         }
      }
      else if( m_iMsgRecib == SOLREVJUR)      
      {
           revJuridica =  true;
           RevocacionJuridica();   
           error = envia(CERTISAT);
           return true;
      }

      if (!error)
      {
         error = envia(AR_MEDIADOR);
         if (error != 0)
         {
            //>+ ERGL (070129)
            //escribeBitacora(eAPL, error, "Error al enviar al mediador.");
            msgCliError('I', "Error en CRevocacion en Proceso: Error al enviar informaci�n al mediador.", error, eSGISSL);
            return false;
         }
         
         error = recibe(AR_MEDIADOR);
         if (error != 0)
         {
            //>+ ERGL (070129)
            //escribeBitacora(eAPL, error, "Error al enviar al mediador.");
            msgCliError('I', "Error en CRevocacion en Proceso: Error al recibir informaci�n del mediador.", error, eSGISSL);
            return false;
         }
 
         switch(m_msgMed->tipOperacion())
         {
            case CERTREVAR:
            {
               if (m_utilBDAR->RegOperDetalleBDAR(CERTREVAR, m_numOp, 0) < 0 || 
                   m_utilBDAR->RegOperDetalleBDAR(CERTREV,   m_numOp, 0) < 0 )
               {
                  msgCliError('I', "Error en CRevocacion en Proceso: Error al insertar el detalle de la operacion CERTREV.", error = ERR_BD_REG_OPER_SEG, eAPL);
                  return false;
               }
               error = regRevDetBD();   
               if ( error != 0 )
               {
                  msgCliError('I', "Error en CRevocacion en Proceso: Error al registrar revocacion a detalle.", error, eAPL);
                  return false;
               }
               
               CAcuses m_acuse(m_BDAR, m_Configuracion, m_numOp, m_numTram);
               if ( !genAcuseRevoc(&m_acuse) ) 
               {
                  //>+ ERGL (070129)
                  //escribeBitacora(eAPL, ERR_GET_DATOS_ACUSE, "Error al generar la cadena de datos.");
                  msgCliError('I', "Error en CRevocacion en Proceso: Error al generar la cadena de datos.", error = ERR_GET_DATOS_ACUSE, eAPL);
                  return false;
               }
               if (m_cadSolRev[0] == 0)
               {
                  sprintf(m_cadSolRev, "%s", ".");
                  m_icadSolRev = 1;
               }
               error = sgiErrorBase(m_MsgCli.setMensaje(CERTREV, m_cadSolRev, m_icadSolRev,
                                    m_acuse.cad_orig, strlen(m_acuse.cad_orig), m_acuse.firma, strlen(m_acuse.firma)));
                                    // m_cadOrigAcuse.c_str(), m_cadOrigAcuse.length(), m_FirmaCadOrig, m_iFirmaCadOrig));
               if (!error)
               {
                  error = envia(CERTISAT);
                  if(error)
                     //>+ ERGL (070129)
                     //escribeBitacora(eAPL, error, "Error al enviar al mediador.");
                     msgCliError('I', "Error en CRevocacion en Proceso: Error al enviar informaci�n al mediador.", error, eSGISSL);
               }
               else
               {
                  //>+ ERGL (070129)
                  //escribeBitacora(eAPL, error, "Error al preparar el mensaje.(CERTREV)");
                  msgCliError('I', "Error en CRevocacion en Proceso: Error al preparar el mensaje.(CERTREV)", error, eSGISSL);
               }
               break;
            }
            case MSGERROR:
            {
               char psz_tipError, psz_cdgErr[10], psz_desc[385];
               int i_cdgErr = sizeof(psz_cdgErr), i_desc = sizeof(psz_desc);
               
               error = getMsgErr(AR_MEDIADOR, psz_tipError, psz_cdgErr, i_cdgErr, psz_desc, i_desc);
               if (!error)
               {
                  error = setMsgErr(CERTISAT, psz_tipError, psz_cdgErr, psz_desc);                  
                  if (!error)
                  {
                     if (m_utilBDAR->RegOperDetalleBDAR(m_msgMed->tipOperacion(), m_numOp, atoi(psz_cdgErr)) < 0)
                        escribeBitacora(eAPL, ERR_BD_REG_OPER_SEG, "Error en CRevocacion en Proceso: Error al ejecutar el SP sp_regOperDet");
                     error = envia(CERTISAT);
                  }
               }
               else
                  //>+ ERGL (070129)
                  //escribeBitacora(eAPL, error, "Error al preparar el mensaje.(CERTREV)");
                  msgCliError(psz_tipError, "Error en CRevocacion en Proceso: Error al preparar el mensaje.(CERTREV)", error = atoi(psz_cdgErr), eMSGSAT);
               break;
            }
            default:
               //>+ ERGL (070129)
               //escribeBitacora(eAPL, error, "Error mensaje no esperado del servidor.");
               msgCliError('I', "Error en CRevocacion en Proceso: Error mensaje no esperado del servidor.", error = ERR_MENS_MED_NO_ESP, eAPL);
         }
      }
   }
   return !error;
}

/*##############################################################################
   PROPOSITO: Esta funcion es la encargada de llevar a cabo todos los pasos
              para realizar un arevocacion de Certificado Digital.

   PREMISA:
   EFECTO:
   ENTRADAS:
   SALIDAS:
#################################################################################*/

const char* CRevocacion::getNombreOperacion()
{
   if (m_iMsgRecib == -1)
      m_iMsgRecib = m_MsgCli.tipOperacion();
   if(m_iMsgRecib == SOLREV)
      return "Revocacion por clave de revocacion.";
   else if(m_iMsgRecib == SOLREVOF)
      return "Revocacion por oficio.";
   else if(m_iMsgRecib == SOLREVF)
      return "Revocacion por firma de Solicitud.";
   else if(m_iMsgRecib == SOLREVJUR)
      return "Revocacion Juridica.";
   else return NULL;
}

/*##############################################################################
   PROPOSITO: Esta funcion es la encargada de llevar a cabo todos los pasos
              para realizar un arevocacion de Certificado Digital.

   PREMISA:
   EFECTO:
   ENTRADAS:
   SALIDAS:
#################################################################################*/

int CRevocacion::getTipOper()
{
   if (m_iMsgRecib == -1)
      m_iMsgRecib = m_MsgCli.tipOperacion();
   return m_iMsgRecib;
}

/*##############################################################################
   PROPOSITO: Esta funcion es la encargada de llevar a cabo todos los pasos
              para realizar un arevocacion de Certificado Digital.

   PREMISA:
   EFECTO:
   ENTRADAS:
   SALIDAS:
#################################################################################*/

void CRevocacion::iniciaVarsOp()
{
   m_inumSerOp       = sizeof(m_numSerOp);
   m_ifirmaSolRev    = sizeof(m_firmaSolRev); 
   m_icadSolRev      = sizeof(m_cadSolRev);
   m_inumSerOp       = sizeof(m_numSerOp);
   m_inumSerFIEL     = sizeof(m_numSerFIEL);
   m_icveRev         = sizeof(m_cveRev);
   m_icadenaOriginal = sizeof(m_cadenaOriginal);
   m_irefMov         = sizeof(m_refMov);
   m_imotivCve       = sizeof(m_motivCve);
   m_irfcRL          = sizeof(m_rfcRL);
   m_iNomRL          = sizeof(m_NomRL);
   
}
 
/*##############################################################################
   PROPOSITO: Esta funcion es la encargada de llevar a cabo todos los pasos
              para realizar un arevocacion de Certificado Digital.

   PREMISA:
   EFECTO:
   ENTRADAS:
   SALIDAS:
#################################################################################*/

int CRevocacion::regRevDetBD()
{
   char   med[2], m_refMov_DB[50*2+1];
   string s_agcCve;

   if(m_iMsgRecib == SOLREV)
      sprintf(med, "%s", "C\0");
   else
   {
      sprintf(med, "%s", "O\0");
      if (revFirma) 
      {
         sprintf(m_cveRev, "%s", "0");
         revFirma = false; 
      }
      else
         sprintf(m_cveRev, "%s", "\0"); 
      
   }
   if((m_refMov[0] == '.'))
      strcpy(m_refMov, m_numTram);
   if( m_BDAR->prepCadDelim( 1, m_refMov, m_refMov_DB ) != 0 ||
      !m_BDAR->ejecutaOper(REVOC_AGREGA_REVOCACION, m_numTram, m_numSerOp, med, m_refMov_DB, m_motivCve, m_cadSolRev)) //Vamos a meter la CO de la solicitud
   {
      //>>> ERGL (070131)
      escribeBitacora(eAPL, ERR_BD_REG_REV_DET, "Error en CRevocacion en regRevDetBD: Error al registrar revocacion a detalle."); 
      Bitacora->escribePV(BIT_ERROR, "Error en CRevocacion en regRevDetBD: No se pudo insertar en la base de datos: insert into rev_det values ('%s','%s','%s','%s','%s','%s')", m_numTram, m_numSerOp, med, m_refMov_DB, m_motivCve, m_cveRev);
      //<<<
      return ERR_NO_INSERT_REV_DET;
   }
   return 0; 
}

/*##############################################################################
   PROPOSITO: Esta funcion es la encargada de llevar a cabo todos los pasos
              para realizar un arevocacion de Certificado Digital.

   PREMISA:
   EFECTO:
   ENTRADAS:
   SALIDAS:
#################################################################################*/

bool CRevocacion::getNombreRL(char *nombre)
{
   bool ok = false;
   char nombreRL[256];
   string s_rfcRLTmp;
   
   nombreRL[0] = 0;
   
   if (m_BDAR->consultaReg(REVOC_OBTEN_RFCRL_CERTIFICADO_POR_NUM_SERIE, m_numSerOp))
   {
      if (!m_BDAR->getValores("s", &s_rfcRLTmp))
         return ok;
   }
   else Bitacora->escribe(BIT_ERROR, "Error en CRevocacion en getNombreRL: Error al consultar el RFC del RL");
      
   if (m_BDAR->consultaReg(REVOC_OBTEN_ULT_NOMBRE_DE_CERTIFICADO_POR_RFC, s_rfcRLTmp.c_str()))
   {
      if(m_BDAR->getValores("b", nombreRL))
      {
         if(nombreRL[0] != 0)
         {
            sprintf(nombre, "%s", nombreRL);
            sprintf(m_rfcRL, "%s", s_rfcRLTmp.c_str());
            ok = true;
         }
      } else Bitacora->escribe(BIT_ERROR, "Error en CRevocacion en getNombreRL: Error al obtener el nombre del RL");    
   } else Bitacora->escribe(BIT_ERROR, "Error en CRevocacion en getNombreRL: Error al consultar el nombre del certificado por RFC");
   
   return ok;
} 

bool CRevocacion::genAcuseRevoc(CAcuses *pacuse)
{
   int error = 0, tipo = -1;

   string s_nom, s_modCve;
   
   if (m_BDAR->consultaReg(REVOC_OBTEN_DATOS_CERTIFICADO_POR_NUM_SERIE, m_numSerOp))
   {
      if(!m_BDAR->getValores("si", &s_nom, &tipo))
         return false;
   }
   char s_aux[strlen(m_cadSolRev) + 1];
   sprintf(s_aux, "|%s", m_cadSolRev);
   // Y la clase CRevocacion de la AR debe obtener los parametros faltantes (!= "" � 0 )
   if ( m_utilBDAR->getNivelAGC(m_agcCve) == 6 ) // Es una REVOCACION desde el Certisat-WEB
   {
       Bitacora->escribe(BIT_INFO, "CRevocacion en genAcuseRevoc: AGC=6 es una Revocaci�n desde el certisat-WEB");
       if( m_iMsgRecib == SOLREVJUR )
       {
         Bitacora->escribePV(BIT_INFO, "CRevocacion en genAcuseRevoc: FIRMA %s", m_firmaSolRev );  
         Bitacora->escribePV(BIT_INFO, "CRevocacion en genAcuseRevoc: AUX %s", s_aux );  
         error = pacuse->generaAcuse(CERTREV, REVOCACION_JURIDICA,  m_rfcOp, s_nom.c_str(), m_numSerOp, "", "", "",
            MODULO_REVOCACIONJUR, tipo, atoi(m_motivCve), "", "",s_aux, m_firmaSolRev);
       }
       else   
         error = pacuse->generaAcuse(CERTREV, REVOCACION_CLAVE,  m_rfcOp, s_nom.c_str(), m_numSerOp, "", "", "", 
            MODULO_CERTISAT_WEB, tipo, 1, "", "",/*m_cadSolRev*/s_aux, m_firmaSolRev);
   }
   else {  // Es una REVOCACION desde el Certisat-Ventanilla, hay q diferenciar si es REV * Oficio � Clave  
      
      if(m_rfcRL[0] == '.')
      {
         if(!getNombreRL(m_NomRL))
         {
            Bitacora->escribe(BIT_INFO, "CRevocacion en genAcuseRevoc: RFC no cuenta con representante legal.");
            m_NomRL[0] = '\0';
            m_rfcRL[0] = '\0';
            if (m_BDAR->consultaReg(REVOC_OBTEN_ULT_NOMBRE_DE_CERTIFICADO_POR_RFC, m_rfcOp))
            {
               if (m_BDAR->getValores("b", m_NomRL))
                  strcpy(m_rfcRL, m_rfcOp);
            }
         }
      }

      if (m_BDAR->consultaReg(REVOC_OBTEN_MODULO_AGENTE_POR_CLAVE, m_agcCve))
      {
         if (!m_BDAR->getValores("s", &s_modCve))
            return false;
      }
      
      error = pacuse->generaAcuse(CERTREV, (m_iMsgRecib == SOLREV) ? REVOCACION_CLAVE : REVOCACION_OFICIO, m_rfcOp, 
          s_nom.c_str(), m_numSerOp, "", m_rfcRL, m_NomRL, s_modCve.c_str(), tipo, atoi(m_motivCve), "", "");      
   }   
   if ( error != 0 )
      Bitacora->escribePV(BIT_ERROR, "Error en CRevocacion en genAcuseRevoc: no pudo genersarse el Acuse de Revocaci�n en la DB, numTramite=%s", m_numTram );
   else 
      Bitacora->escribePV(BIT_INFO, "CRevocacion en genAcuseRevoc:OK se genero Acuse de Revocaci�n en la AR, numTramite=%s, cad_orig=%s, firma=%s", 
                              m_numTram, pacuse->cad_orig, pacuse->firma );
   return (!error);   
}   


bool CRevocacion::procesaRevocacion()
{
      //Separa elementos de la cadena original de la solicitud de revocacion
      CSeparaToken* Tkn = NULL;
      string s_noSerieQuery, sRuta, sRutaFin;
      int error = 0;
      bool retVal = false;
      
      Bitacora->escribePV(BIT_INFO, "Error en CRevocacion en procesaRevocacion: Se separa la cadena de solicitud de Revocaci�n %s con el caracter |" ,m_cadSolRev);
      if ( (Tkn = new CSeparaToken(m_cadSolRev, '|' ) ) != NULL )
      {
         int ctr = Tkn->getCounter();
         if ( ctr < 3 || ctr > 4)
         {
            Bitacora->escribe(BIT_ERROR, "Error en CRevocacion en procesaRevocacion: Error al procesar el buffer 'm_cadSolRev' en CSeparaToken.");
            error = ERR_GET_DATOS_CO_FIRMA;
         }
         else
         {
            strcpy(m_numSerieRev, Tkn->items[0] );
            if (ctr == 4)
            { 
               strcpy(m_numSerieFiel, Tkn->items[1] );
               strcpy(m_rfcRec, Tkn->items[2]);
            }
            else 
            {
               strcpy(m_rfcRec, Tkn->items[1]);
               strcpy(m_numSerieFiel, m_numSerieRev); 
            }
            strcpy(m_numSerOp, m_numSerieRev);
         }
         delete Tkn;
         Tkn = NULL;
      }else
           Bitacora->escribe(BIT_ERROR, "Error en CRevocacion en procesaRevocacion: No se pudo crear el objeto CSeparaToken");
      
      //Validamos que el Certificado a revocar y el FIEL sean del mismo contribuyente
      if (m_BDAR->consultaReg(REVOC_OBTEN_NUM_SERIE_CERTIFICADO_POR_RFC_Y_NUM_SERIE, m_rfcRec, m_numSerieRev, m_numSerieFiel))
      {
         if (!m_BDAR->getValores("s", &s_noSerieQuery))
         {
            Bitacora->escribePV(BIT_ERROR, "Error en CRevocacion en procesaRevocacion: Error al validar Certificados del Contribuyente. (%s, %s)", 
               m_numSerieRev, m_numSerieFiel);
            error = ERR_VALIDA_DATOS_MSG;
            msgCliError('N', "Error en CRevocacion en procesaRevocacion: El certificado con el que firm� la solicitud se encuentra revocado o caduco.", error, 
            eSGISSL);
            return false;
         }
      }
      //Buscamos el Certificado FIEL para validar firma
      m_Configuracion.getValorVar("[AR]", "REPOSITORIO", &sRuta);
      pkiRutaCert(sRuta.c_str(), s_noSerieQuery.c_str() , sRutaFin ,false, LINUX);
       
      //Verificamos la firma
      uchar tmp_firma[512];
      uint16 tmp_ifirma = sizeof(tmp_firma);
      if (!B64_Decodifica((char*)m_firmaSolRev, (uint16)m_ifirmaSolRev, (uint8*)tmp_firma, &tmp_ifirma))
      {
         Bitacora->escribe(BIT_ERROR, "Error en CRevocacion en procesaRevocacion: Error al quitar Base 64 a la firma.");
         return false;
      }
      SGIRSA* pSgiR = new SGIRSA();
      error = sgiErrorBase(pSgiR->VerificaFirmaP((uchar*)tmp_firma, tmp_ifirma, sRutaFin.c_str(), SHA1_ALG, (uchar*)m_cadSolRev, &m_icadSolRev));
      if (error)
      { 
         Bitacora->escribe(BIT_ERROR, "Error en CRevocacion en procesaRevocacion: Error al validar la firma de la solicitud.");
         delete pSgiR;
         return false;
      }
      delete pSgiR;
      sprintf(m_motivCve, "%s", "0");
      Bitacora->escribe(BIT_INFO, "CRevocacion en procesaRevocacion: Se proceso exitosamente la solicitud de revocacion por FIEL");
      return true;
}

//  L�gica  Revocacion Autoridad (Juridica)

bool CRevocacion::
RevocacionJuridica()
{

      string certsRev;
      int numCertsAct = 0;
      char *cNumTramRev =  new char[18];

      Bitacora->escribePV(BIT_INFO, "CRevocacion en RevocacionJuridica: Revocacion Juridica ( %s, %s , %s )",m_rfcOp,  m_refMov, m_firmaSolRev);
      conexionBDRev();

      if ( !revBD->consultaReg(REVOC_OBTEN_TOTAL_FIEL_POR_RFC, m_rfcOp) )
      {
         Bitacora->escribe(BIT_ERROR, "Error en CRevocaci�n en RevocacionJuridica: Revocacion Juridica sin certificados activos");
         return false;
      }
      if(!revBD->getValores("i", &numCertsAct))
         return false;

      if(numCertsAct == 0 )
      {
         msgCliError('N', "Error en CRevocacion en RevocacionJuridica: El RFC no cuenta con certificados activos.", 1, eMSGSAT);
         return false;
      }

      if ( !revBD->consulta(REVOC_OBTEN_DATOS_FIEL_POR_RFC, m_rfcOp) )
      {
         Bitacora->escribe(BIT_ERROR, "Error en CRevocacion en RevocacionJuridica: Revocacion Juridica sin certificados activos");
         return false;
      }

      while( revBD->sigReg() )
      {
         string tipo;
         
         if(!revBD->getValores("bs", m_numSerOp,&tipo))
            return false;

         Bitacora->escribePV(BIT_DEBUG, "CRevocacion en RevocacionJuridica: Tipo de Certificado [ %s ]",tipo.c_str());
         Bitacora->escribePV(BIT_DEBUG, "CRevocacion en RevocacionJuridica: Numero de serie a revocar [ %s ]",m_numSerOp);
         Bitacora->escribePV(BIT_DEBUG, "CRevocacion en RevocacionJuridica: Operacion [%s] Tramite [%s] ",m_numOp,m_numTram);
         if ( Revocacion() )
         {
               string tramite(m_numTram);
               string numero(m_numSerOp);
               string fecha  = consultaFechaRev();
               certsRev+=tramite+"^"+numero+"^"+fecha+"|";
               Bitacora->escribePV(BIT_INFO, "CRevocacion en RevocacionJuridica: CertsRevocados %s" ,certsRev.c_str());
         }    
         CreaTramite(SOLREVJUR, cNumTramRev, m_rfcOp );
         Bitacora->escribePV(BIT_DEBUG, "CRevocacion en RevocacionJuridica: CertsRevocados %s" ,certsRev.c_str());

      }
      Bitacora->escribePV(BIT_DEBUG, "CRevocacion en RevocacionJuridica: certsRev.length() %d",certsRev.length());
      if ( certsRev.length() ==  0)
         certsRev= "NO REVOCADOS";
      int error = sgiErrorBase(m_MsgCli.setMensaje(REVJURFIN, certsRev.c_str(), certsRev.length()));
      Bitacora->escribePV(BIT_DEBUG, "CRevocacion en RevocacionJuridica: Mensaje armado %d",error);
      revBD->desconecta();
      revJuridica = false;
      return true;

}

bool CRevocacion::
CreaTramite(int iOperacion , char * cNumTramRev ,char *cRfc )
{
      string sOper;
      Bitacora->escribePV(BIT_INFO, "CRevocacion en CreaTramite: Crea tramite para certificado Agente [%s] Operacion [%d]",m_agcCve,iOperacion);

      bool ok_ejecuta = false;

      #if ORACLE
        ok_ejecuta = m_BDAR->ejecutaFuncion(REVOC_SP_GENERA_NUM_OPER_NOMBRE, REVOC_SP_GENERA_NUM_OPER_RETURN, REVOC_SP_GENERA_NUM_OPER_PARAMS, iOperacion,m_agcCve );
      #else
        ok_ejecuta = m_BDAR->ejecutaSP(REVOC_SP_GENERA_NUM_OPER_NOMBRE, REVOC_SP_GENERA_NUM_OPER_PARAMS, iOperacion,m_agcCve );
      #endif

      if ( ok_ejecuta ) // Operacion
      {

         Bitacora->escribePV(BIT_INFO,"CRevocacion en CreaTramite: Se ejecuto sp generacion de operacion");
         if (m_BDAR->getValores("b",m_numOp))
         {
            Bitacora->escribePV(BIT_INFO, "CRevocacion en CreaTramite: Operacion [%s]",m_numOp);

            #if ORACLE
                ok_ejecuta = m_BDAR->ejecutaFuncion(REVOC_SP_GENERA_NUM_TRAM_NOMBRE, REVOC_SP_GENERA_NUM_TRAM_RETURN, REVOC_SP_GENERA_NUM_TRAM_PARAMS, iOperacion,m_numOp,cRfc,m_agcCve );
            #else
                ok_ejecuta = m_BDAR->ejecutaSP(REVOC_SP_GENERA_NUM_TRAM_NOMBRE, REVOC_SP_GENERA_NUM_TRAM_PARAMS, iOperacion,m_numOp,cRfc,m_agcCve );
            #endif

            if( ok_ejecuta )
            {
                Bitacora->escribePV(BIT_INFO,"CRevocacion en CreaTramite: Se ejecuto sp generacion tramite");
                if (m_BDAR->getValores("b",m_numTram))
                {
                   Bitacora->escribePV(BIT_INFO, "CRevocacion en CreaTramite: Operacion [%s] Tramite [%s] ",m_numOp,m_numTram);
                   return true;
                }
            }
         }
     }
     return false;
}

   
bool CRevocacion::
Revocacion()
{
      Bitacora->escribePV(BIT_INFO, "CRevocacion en Revocacion: Proceso de revocaci�n [%s]" ,m_numSerOp); 
      int error = sgiErrorBase(m_msgMed->setMensaje(SOLREVOFAR, m_numOp, strlen(m_numOp), m_secOp,  m_isecOp = strlen(m_secOp),
           m_cadMed.c_str(), m_cadMed.length(), m_numSerOp, strlen(m_numSerOp)));
      if (error != 0)
      {
          Bitacora->escribePV(BIT_INFO, "CRevocacion en Revocacion: Error al armar el mensaje de revocaci�n. [%d]",error);   
          msgCliError('I', "Error en CRevocacion en Revocacion: Error al armar el mensaje de revocaci�n.", error, eSGISSL);
          return false;
      } 
      error = envia(AR_MEDIADOR);
      if (error != 0)
      {
          Bitacora->escribePV(BIT_INFO, "CRevocacion en Revocacion: Error al Enviar al mendiador [%d]",error);
          msgCliError('I', "Error en CRevocacion en Revocacion: Error al enviar al mediador.", error, eSGISSL);
          return false;
      }

      error = recibe(AR_MEDIADOR);
      if (error != 0)
      {
          Bitacora->escribePV(BIT_INFO, "CRevocacion en Revocacion: Error al recibir del mendiador [%d]",error);
          msgCliError('I', "Error en CRevocacion en Revocacion: Error al recibir del mediador.", error, eSGISSL);
          return false;
      }

      Bitacora->escribePV(BIT_INFO, "CRevocacion en Revocacion: Mensaje recibido del mediador [%d]",m_msgMed->tipOperacion());


      switch(m_msgMed->tipOperacion())
      {
            case CERTREVAR:
            {
               if (m_utilBDAR->RegOperDetalleBDAR(CERTREVAR, m_numOp, 0) < 0 ||
                   m_utilBDAR->RegOperDetalleBDAR(CERTREV,   m_numOp, 0) < 0 )
               {
                  msgCliError('I', "Error en CRevocacion en Revocacion: Error al insertar el detalle de la operacion CERTREV.", error = ERR_BD_REG_OPER_SEG, eAPL);
                  return false;
               }
               Bitacora->escribePV(BIT_INFO, "CRevocacion en Revocacion: REVJUR Se registraron los dectalles del numero de operacio [%s]",m_numOp);
               error = regRevDetBD();
               if ( error != 0 )
               {
                  msgCliError('I', "Error en CRevocacion en Revocacion: Error al registrar revocacion a detalle.", error, eAPL);
                  return false;
               }
               Bitacora->escribePV(BIT_INFO, "CRevocacion en Revocacion: Generacuon Acuse. NumOperacion [%s] NumTramite[%s]",m_numOp, m_numTram);
               CAcuses m_acuse(m_BDAR, m_Configuracion, m_numOp, m_numTram);
               if ( !genAcuseRevoc(&m_acuse) )
               {
                  msgCliError('I', "Error en CRevocacion en Revocacion: Error al generar la cadena de datos.", error = ERR_GET_DATOS_ACUSE, eAPL);
                  return false;
               }
               if (m_cadSolRev[0] == 0)
               {
                  sprintf(m_cadSolRev, "%s", ".");
                  m_icadSolRev = 1;
               }
               if (!m_utilBDAR->RegOperDetalleBDAR(FINDETRAMITE, m_numOp))
                   Bitacora->escribe(BIT_ERROR, "Error en CRevocacion en Revocacion: Error al insertar el fin de tr�mite en la BD.");
               return true;
               break;
            }
            case MSGERROR:
            {
               char psz_tipError, psz_cdgErr[10], psz_desc[385];
               int i_cdgErr = sizeof(psz_cdgErr), i_desc = sizeof(psz_desc);

               error = getMsgErr(AR_MEDIADOR, psz_tipError, psz_cdgErr, i_cdgErr, psz_desc, i_desc);
               if (!error)
               {
                  error = setMsgErr(CERTISAT, psz_tipError, psz_cdgErr, psz_desc);
                  if (!error)
                  {
                     if (m_utilBDAR->RegOperDetalleBDAR(m_msgMed->tipOperacion(), m_numOp, atoi(psz_cdgErr)) < 0)
                        escribeBitacora(eAPL, ERR_BD_REG_OPER_SEG, "Error en CRevocacion en Revocacion: Error al ejecutar el SP sp_regOperDet");
                  }
               }
               else
                  msgCliError(psz_tipError, "Error en CRevocacion en Revocacion: Error al preparar el mensaje.(CERTREV)", error = atoi(psz_cdgErr), eMSGSAT);
               break;
            }
            default:
               msgCliError('I', "Error en CRevocacion en Revocacion: Error mensaje no esperado del servidor.", error = ERR_MENS_MED_NO_ESP, eAPL);
         }
         return false;
}



bool CRevocacion::
conexionBDRev()
{
      revBD = new CBD(Bitacora);

      string servidor,base,usuario,password,role;
      getVarConBD("[AR_BASE_DATOS]",&servidor,&base,&usuario,&password,&role);

   
      if (!procesaPassword(password, &password))
      {
         string sError = string("Error en CRevocaci�n en conexionBDRev: Error al desencriptar el password de conexi�n a la BD de AR") ;
         escribeBitacora(eAPL, ERR_NOT_DESENCRYPT_PWD, sError.c_str());
         return false;
      }


      revBD->setConfig(servidor.c_str(), base.c_str(), usuario.c_str(), password.c_str(), role.c_str());
      revBD->conecta();
      return true;

}

   string CRevocacion::
   consultaFechaRev()
   {
      string vigFin;
      Bitacora->escribePV(BIT_DEBUG, "CRevocacion en consultaFechaRev: consultaFechaRev [%s]",m_numSerOp);

      if ( !m_BDAR->consultaReg(REVOC_OBTEN_FECH_VIG_FIN_CERTIFICADO_POR_NUM_SERIE, m_numSerOp) )
      {
         return false;
      }

      if(!m_BDAR->getValores("s", &vigFin))
          return false;
      Bitacora->escribePV(BIT_DEBUG, "CRevocacion en consultaFechaRev: Fecha revocacion  [%s]",vigFin.c_str());
      return vigFin;

   }

