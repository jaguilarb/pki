static const char* _CACUSES_CPP_VERSION_ ATR_USED = "@(#) SrvAR ( L : DSIC10392AR_ : CAcuses.cpp : 1.1.3 : 4 : 28/09/10)";

//#VERSION: 1.1.1
/*######################################################################################################################
  ###  PROYECTO:              PKI-SAT                                                                                ###
  ###  MODULO:                Clase que realiza el manejo de los datos de los acuses                                 ###
  ###                                                                                                                ###
  ###  DESARROLLADORES:       EDSI-ADA:  Equipo d Desarrollo para Seguridad de la Informacion de HOA                 ###
  ###  FECHA DE INICIO:       Domingo 12, febrero 2006                                                               ###
  ###                                                                                                                ###
  ######################################################################################################################
         1         2         3         4         5         6         7         8         9        10        11        12
123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
 #######################################################################################################################
   VERSION:
      V.1.0.0      (20060212 - 20060212) HOA: Primera Versi�n
           .1      (20141014 -         ) JAB: Se agrupan las sentencias SQL utilizadas en el SrvAR en un archivo de
                                              cabecera
   CAMBIOS:
######################################################################################################################*/

#include <SgiTipos.h> 
//- #include <SgiOpenSSL.h>    //- GHM (070427)
#include <Sgi_OpenSSL.h>       //+ GHM (070427)
#include <Sgi_B64.h>
//- #include <PwdProtection.h> //- GHM (070427)
#include <Sgi_ProtegePwd.h>    //+ GHM (070427)
#include <errno.h>
#include <DefDatos.h>
#include <Sgi_BD.h>
//- #include <CConfigFile.h>   //- GHM (070427)
#include <Sgi_ConfigFile.h>    //+ GHM (070427	
#include <CAcuses.h> // Para los Titulos de los Acuses
#include <Sgi_MsgPKI.h> //Para las operaciones //+ AGZ (120326)

#if ORACLE
   #include <CosltSrvAR_ORA.h>   // + JAB (20141014)
#else
   #include <CosltSrvAR.h>   // + JAB (20141014)
#endif

//#####################################################################################################################

/*
//#####################################################################################################################}
// Parametros por pasar a la funcion CAcuses::generaAcuse(parametros)
TRAMITE:          CERTGEN       CERTREV         CERTREN       TRANSCOMPLETA
CAD_ORIGINAL;
tramite_cve|      PARM		PARM            PARM          PARM	
tipacu_cve|       PARM		PARM            PARM	      PARM	
fec_op|	          ok            ok              ok            ok    (CAcuses) como PARM el IdMsg o Tipo de TRAMITE
rfc|              PARM		PARM	        PARM          PARM
nombre|	          PARM		PARM	        PARM          PARM
no_serie|         PARM		PARM            PARM          ""
llave_pub|        PARM		""		PARM	      ""
rfc_sol|	  PARM          PARM-WEB:""     PARM-WEB:""   ""
nombre_sol|       PARM		PARM-WEB:""     PARM-WEB:""   ""
mod_cve|          PARM		PARM-CSW        PARM-CSW      PARM-CSW --> DESCRIPCION MODULO == mod_desc  
tipcer_cve|       PARM	        PARM	        PARM          PARM  --> tipcer_desc de la tabla: CAT_TIP_CER	
motrev_cve|       -1            motrev_cve--1	-1            -1    --> motrev_desc de la tabla: CAT_MOTIVO_REVOCA
ref_arch|         ""            ""              PARM          PARM	
dig_archivo|      PARM          ""              PARM          PARM		
NumSerie_AR|      La funcion q va a obtener la firma lo va a agregar al final y me lo va a regresar
		  getFirmaCadena(char *cadOri, uint i_cadOri, char *firma, uint16 *i_firma, char *NumSerieAR)
//#####################################################################################################################
*/


// -------------------------------------------------------------------------------------------------
/* Constructor para Generar o Buscar un Acuse en la DB
   (in) ar y m_Conf     : Vars. de COperacion::m_BDAR  y m_Configuracion
   (in) numOp y numTram : Vars. de COperacion::m_numOp y m_numTram, se inician aqui <=> generaAcuse()
*/
CAcuses::CAcuses(CBD* ar, CConfigFile& config, char *numOp, char *numTram):
   m_Configuracion(config)
{
   //*** Objetos de la clase COperacion
   m_BDAR    = ar;			// Suponemos ya conectado a la DB
   m_numOp   = numOp;		// Solo se usa junto con la funcion generaAcuse()
   if ( numTram )
      sprintf( m_numTram, "%s", numTram);
   //***
   error		= 0;
   InicializaCad();
}

/*##################################################################################
 * MELM: 2 FUNCIONES PUBLICAS PARA GENERAR y BUSCAR  LOS ACUSES DEL CERTISAT
  ################################################################################## */

/* Busca y Obtiene los datos del ACUSE por Tipo de Recuperacion  y por numero de Tramite/Serie
   (out)tipacu_cve : campo 'tipacu_cve' de la tabla de 'ACUSE'
   (out)tipcer_cve : campo 'tipcer_cve' de la tabla de 'ACUSE'
   (in) tipoRec    : tipo de Recuperacion: por la tabla  de 'T':Acuse, 'G':Certificado , 'R': Rev_Det
   (in) no_Tram_Serie: se Recupera por el campo 'no_serie' o el campo 'tramite_cve'
   RETORNA 0 si no hay error � el ID del MSG de ERROR
*/
int CAcuses::buscaAcuse(int *tipacu_cve, int *tipcer_cve, const char tipoRec, const char *no_Tram_Serie)
{
   int   hayAcuse;
   char  nombre[660];     //  campo de la tabla: ACUSE
   char  cMedio[2];
	
   switch (tipoRec) {
   case 'T': // Recuperacion por Numero de Tramite
            sprintf( m_numTram, "%s", no_Tram_Serie);
            break;
   case 'G': // Recuperacion de Cert. Generado
            error = getCampo("TRAMITECVE", "SEG_PKI.SEGT_CERTIFICADO", "NOSERIE", "'%s'", (void *)no_Tram_Serie, m_numTram);
            break;
   case 'R': // Recuperacion de Cert. Revocado
            error = obtieneDatosRev( no_Tram_Serie,cMedio,m_numTram);
            break;
   default : error = ERR_LLAMA_BUSCA_ACUSE;
   }
   if (error)
       Bitacora->escribePV(BIT_ERROR, "CAcuses Error en buscaAcuse: Error en la llamada buscaAcuse(tipoRec='T','G','R'), y tipoRec=='%c'", tipoRec);

   // Aqui ya tenemos Inicializado correctamente el num. de Tramite para Buscarlo en la tabla de Acuse
   if ( !error && 
          (!m_BDAR->consultaReg(ACU_EXISTE_ACUSE_POR_NUM_DE_TRAMITE, m_numTram) ||
           !m_BDAR->getValores("i", &hayAcuse ) || !hayAcuse) ) // NO Hay Acuses con este IdTramite
   {
      //if ( strcmp(cMotivo,'O') == 0 )
      //   error = ERR_GET_NUM_ACUSE;
      //else if ( strcmp(cMotivo,'C') == 0 )
      //   error = ERR_GET_NUM_ACUSE;
      if ( strcmp(cMedio,"R") == 0 )
         error = ERR_GET_REN_ACUSE;
      else if ( strcmp(cMedio,"A") == 0 )
         error = ERR_GET_AUT_ACUSE;
      else
         error = ERR_GET_NUM_ACUSE;

      Bitacora->escribePV(BIT_ERROR, "CAcuses Error en buscaAcuse: No exite el acuse, buscaAcuse(), m_numTram=%s", m_numTram);
   }
   // SI Hay Acuses con este IdTramite => Trae los datos del Acuse
   else if ( !error && 
         (!m_BDAR->consultaReg(ACU_OBTEN_DATOS_ACUSE_POR_NUM_DE_TRAMITE, m_numTram) || !m_BDAR->getValores("ibbbi", tipacu_cve, nombre, cad_orig, firma, tipcer_cve)) )
   {
      error = ERR_GET_DAT_ACUSE;
      Bitacora->escribePV(BIT_ERROR, "CAcuses Error en buscaAcuse: Error al obtener los datos del acuse en buscaAcuse(), m_numTram=%s", m_numTram );
   }
   else if ( !strcmp(nombre,   "VERSION ANTERIOR") ||
             !strcmp(cad_orig, "Acuse procesado con la version anterior") ||
             !strcmp(firma,    "Acuse procesado con la version anterior")    )
   {
      error = ACUSE_VERS_ANT;
      Bitacora->escribePV(BIT_ERROR, "CAcuses Error en buscaAcuse: Error datos no disponibles, acuse de la version anterior, m_numTram=%s", m_numTram );
   }
   return(error);
}


// Inicia los datos del Acuse del Certisat, funci�n usada por SELLOS, REN, REV y CERTGEN
// Funcion q genera (y guarda) los Acuses de EXITO  
int CAcuses::generaAcuse(int idMsg, int tipacu_cve, const char *rfc, const char *nombre, const char *no_serie, 
                         const char *llave_pub, const char *rfc_sol, const char *nombre_sol, const char *mod_cve,
                         int tipcer_cve, int motrev_cve, const char *ref_arch, const char *dig_archivo, 
                         const char* cadOrigRev, const char *firmCadOrigRev)
{
   char  fecha_acu[20+1];     // regresa el campo 'fec_op' de la tabla OPERACION, lo recupero con el parm: idMsg
   char  tipacu_desc[80*2+1]; // regresa el campo 'tipacu_desc' de la tabla CAT_TIPO_ACUSE
   char	 mod_desc[80+1];      // regresa el campo 'mod_desc' de CAT_MODULO. NOTA en el WEB se manda MODULO_CERTISAT_WEB
   char	 tipcer_desc[50+1];   // regresa el campo 'tipcer_desc' de la tabla CAT_TIP_CER
   char  motrev_desc[30+1];   // regresa el campo 'motrev_desc' de CAT_MOTIVO_REVOCA. NOTA en el WEB se manda ""

   // Se trae la descripcion de los Titulos de Acuse:  
   if ( !strncmp(mod_cve, MODULO_CERTISAT_WEB, 3) ) // Titulos de Acuses WEB == Descripcion de los Tipos de Acuse
      error =  getCampo("TIPACUDESC", "SEG_PKI.SEGC_TIPO_ACUSE",   "TIPACUCVE", "%d",   (void *)&tipacu_cve, tipacu_desc);
   else	// Titulos del Certisat-Normal : se los trae del archivo de Configuracion
      error = getTitulosCertisat(tipacu_cve, tipcer_cve, tipacu_desc);

   motrev_desc[0] = 0;	// Lo iniciamos a nulo y si no se usa => motrev_cve == -1 
   if ( !error &&
          !(error = obtenFechaMsgExitoso(idMsg, fecha_acu)) &&
          !(error = getCampo("MODDESC",    "SEG_PKI.SEGC_MODULO",       "MODCVE",    "'%s'", (void *)mod_cve,     mod_desc)) &&
          !(error = getCampo("TIPCERDESC", "SEG_PKI.SEGC_TIP_CER",      "TIPCERCVE", "%d",   (void *)&tipcer_cve, tipcer_desc)) &&
           ( motrev_cve == -1 ||
          !(error = getCampo("MOTREVDESC", "SEG_PKI.SEGC_MOTIVO_REVOCA", "MOTREVCVE", "%d",  (void *)&motrev_cve, motrev_desc))
           ) 
      ) 
   {
   Bitacora->escribePV(BIT_INFO, "CAcuses en generaAcuse: Construyendo la cadena original del certisat");
   // Para construir la cadena Original del certisat, necesitamos obtener la fecha de EXITO del tramite
   // Si genero el Acuse => construye la cadena original INICIAL de retorno al CERTISAT con el siguiente formato:
   //tramite_cve|tipacu_desc|fecha_acu|rfc|nombre|no_serie|llave_pub|rfc_sol|nombre_sol|mod_desc|tipcer_desc|motrev_desc|
   //ref_arch|dig_archivo|NumSerie_AR|
      if ( ((!strncmp(mod_cve, MODULO_REVOCACIONJUR, 3)) || (!strncmp(mod_cve, MODULO_CERTISAT_WEB, 3))) && (idMsg == CERTREV) )
      { 
         //sprintf(cad_orig, "%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|", m_numTram, tipacu_desc,fecha_acu,rfc,
         //     nombre,no_serie,llave_pub,rfc_sol,nombre_sol,mod_desc,tipcer_desc,motrev_desc,ref_arch,dig_archivo,firmCadOrigRev);
         sprintf(cad_orig, "%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|",m_numTram, tipacu_desc,fecha_acu,rfc,
           nombre,no_serie,llave_pub,rfc_sol,nombre_sol,mod_desc,tipcer_desc,motrev_desc,ref_arch,dig_archivo); 
         sprintf(fcad_orig, "|%s", firmCadOrigRev);
         ifcad_orig = strlen(fcad_orig);
         fcad_orig[ifcad_orig] = 0;
      }
      else 
      sprintf(cad_orig, "%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|", m_numTram, tipacu_desc,fecha_acu,rfc,
              nombre,no_serie,llave_pub,rfc_sol,nombre_sol,mod_desc,tipcer_desc,motrev_desc,ref_arch,dig_archivo);
      
      Bitacora->escribePV(BIT_INFO, "CAcuses en generaAcuse: Cadena original del sertisat con firma");
      // Ahora Generamos la firma con la cadena original ya firmada
      uint16 iFirma = sizeof(firma);
      //AGZ: hay que pasar la Cadena Original de la solicitud de revocacion, para que no la concatene mal...
      error = getFirmaCadena(cad_orig, strlen(cad_orig), firma, &iFirma, (char*) mod_cve, idMsg); // Aqui agrega el campo NumSerie_AR en cad_orig

      //if ( (!strncmp(mod_cve, MODULO_CERTISAT_WEB, 3)) && (idMsg == CERTREV) )
      //   sprintf(cad_orig + strlen(cad_orig),"%s|",cadOrigRev);

   }
   if (!error )
      guardaAcuse(tipacu_cve, mod_cve, nombre, llave_pub, rfc_sol, nombre_sol, fecha_acu, tipcer_cve);	// Graba en la DB
   Bitacora->escribePV(BIT_INFO,"CAcuses en GetFirmaCadena: Se guard� el Acuse");

   if ( error ) // Pudo No guardarse el Acuse => No regresa ni cadena original NI firma
      InicializaCad();	

   return error;
}

int CAcuses::buscaCadSolRev(char* no_Tramite, char* cadSolRev, int *icadSolRev)
{
   char cad_aux[300];
   if (*icadSolRev < sizeof(cad_aux))
      return ERR_GET_CAMPO;
   int error = -1;
   int ldato = strlen(no_Tramite);
   if (ldato < 20)
      error =  getCampo("CVEREV", "SEG_PKI.SEGT_REV_DET",   "TRAMITECVE", "%s",   (void *)no_Tramite, (char*)cad_aux);
   else error = getCampo("CVEREV", "SEG_PKI.SEGT_REV_DET", "NOSERIE", "%s", (void*)no_Tramite, (char*)cad_aux);
   if (cad_aux[0] == 0)
   {
      cad_aux[0] = '.';
      *icadSolRev = 1;
      strncpy(cadSolRev, cad_aux, *icadSolRev);
      error = 0;
   }
   else
   {
      *icadSolRev = strlen(cad_aux);
      strncpy(cadSolRev, cad_aux, *icadSolRev);
   }
   return error;
}

//-----------------

void CAcuses::InicializaCad()
{
   Bitacora->escribePV(BIT_INFO,"CAcuses en InicializaCad: No pudo generarse el Acuse, se limpian las variables",cad_orig, firma);
   memset(cad_orig, 0, sizeof(cad_orig));
   memset(firma, 0, sizeof(firma));
}


//#####################################################################################################################
//    LAS FUNCIONES SIGUIENTES SON PARA MANEJAR LOS ACUSES DEL CERTISAT EN LA DB
//######################################################################################################################
/* Obtiene la ULTIMA fecha d un mensaje exitoso Q ya existe en DB, como: CERTGEN, CERTREN, CERTREV y TRANSCOMPLETA
   (in) idMsg     : Id. del mensaje de exito
   (out) fecha_acu: regresa el campo 'fec_op' de la tabla OPERACION
   RETORNA 0 si no hay error � el ID del MSG de ERROR
*/
int CAcuses::obtenFechaMsgExitoso(int idMsg, char *fecha_acu)
{
   if ( !m_BDAR->consultaReg(ACU_OBTEN_ULT_FECHA_DE_OPERACION_EXITOSA, m_numOp, idMsg) || !m_BDAR->getValores("b", fecha_acu ) )
   {
      error = ERR_GET_FECHA_EXITO;
      Bitacora->escribePV(BIT_ERROR, "Error en CAcuses en ObtenFechaMsgExitoso: (idMensaje=%d), numOperacion=%s", idMsg, m_numOp );
   }
   return error;
}

/* Obtiene el valor de un 'campo' de la 'tabla' dependiendo de su clave y de su tipoClave
   (in)  nameCampo : nombre del 'campo' de la correspondiente 'tabla'
   (in)  tabla     : nombre de la 'tabla' de la AR
   (in)  nameClave : nombre de la 'clave' de la tabla correspondiente
   (in)  clave     : valor  de la 'clave', puede ser un 'const char *' o un 'int'
   (in)  tipoClave : tipo de la 'clave', puede ser un '%s' o un %d
   (out) campo     : valor del 'campo' de la correspondiente 'tabla'
   RETORNA 0 si no hay error y el valor del 'campo' correspondiente   �   el ID del MSG de ERROR
*/
int CAcuses::getCampo(const char *nameCampo, const char *tabla, const char *nameClave, const char *tipoClave, 
                      void *clave, char* campo)
{
   char	cad_aux[300];	// Se usa para cargar el QUERY o para mandar un mensaje de Horror
   bool	es_int;

   es_int = !strcmp(tipoClave, "%d");
   sprintf(cad_aux, ACU_OBTEN_CAMPO_X_DE_TABLA_X_POR_CAMPO_X, nameCampo, tabla, nameClave, tipoClave );	
   if ( ((es_int)? !m_BDAR->consultaReg(cad_aux, *(int *)clave ):!m_BDAR->consultaReg(cad_aux, clave) ) ||
                   !m_BDAR->getValores("b", campo ) )
   {
      error = ERR_GET_CAMPO;
      sprintf(cad_aux, "Error en CAcuses en getCampo: (%s, %s=%s) de la tabla:%s", nameCampo, nameClave, tipoClave, tabla);
      if (es_int)
         Bitacora->escribePV(BIT_ERROR, cad_aux, *(int *)clave );
      else
         Bitacora->escribePV(BIT_ERROR, cad_aux, clave );
   }
   return error;
}

/* Guarda los datos del registro en la tabla de ACUSE
   (in) Todas las variable ya inicializadas con los datos del ACUSE
   RETORNA 0 si no hay error � el ID del MSG de ERROR */
int CAcuses::guardaAcuse(int tipacu_cve, const char *mod_cve, const char *nombre, const char *llave_pub,
                         const char *rfc_sol, const char *nombre_sol, const char *fecha_acu, int tipcer_cve)
{
   char nombre_DB[256*2], cad_orig_DB[4096*2+1], nombre_sol_DB[256*2]; // Cadenas formateadas p/grabarse en la DB
   
   if (  m_BDAR->prepCadDelim( 1, nombre,     nombre_DB    ) != 0 ||
         m_BDAR->prepCadDelim( 1, nombre_sol, nombre_sol_DB) != 0 ||
         m_BDAR->prepCadDelim( 1, cad_orig,   cad_orig_DB  ) != 0 || 
        !m_BDAR->ejecutaOper(ACU_AGREGA_ACUSE, m_numTram, tipacu_cve, mod_cve, nombre_DB, llave_pub, rfc_sol, nombre_sol_DB, cad_orig_DB , firma, 
        fecha_acu,tipcer_cve) )
   {
      error = ERR_SET_REG_ACUSE;
      Bitacora->escribePV(BIT_ERROR, 
                          "Error en CAcuses Error en guardaAcuse: (tipacu_cve=%d,nombre_DB=%s, nombre_sol_DB=%s, cad_orig_DB=%s) m_numTram=%s",
                          tipacu_cve, nombre_DB, nombre_sol_DB, cad_orig_DB, m_numTram);
   }
   return (error);
}

/* Funcion q trae del archivo de Config. los Titulos de Acuses necesarios para el Certisat-normal NO para el WEB
   (in)  tipacu_cve : tipo de Acuse     ; 1 Gen, 2 Rev_Ofi, 3 Rev_Cve, 4 Ren, 5 Sellos
   (in)  tipcer_cve : tipo de Certifcado; 1 FEA, 2 Sello Digital, 3 y >s AgCs
   (out) tipacu_desc: Descripci�n del Titulo, suponemos q hay espacio suficiente
   RETORNA 0 si no hay error y la decripcion del 'Titulo' correspondiente    �   el ID del MSG de ERROR
*/
int CAcuses::getTitulosCertisat(int tipacu_cve, int tipcer_cve, char *tipacu_desc)
{
   string	Titulo;

   switch (tipacu_cve) {
   case GENERACION_CERT_DIG :	
         if (tipcer_cve == FEA)
            error = m_Configuracion.getValorVar("[TITULOIMP]", "TGENFEA" , &Titulo);
         else // Es siempre de AGCs							   	
            error = m_Configuracion.getValorVar("[TITULOIMP]", "TGENAGC" , &Titulo);
         break;
   case REVOCACION_OFICIO   :  
   case REVOCACION_JURIDICA :  
   case REVOCACION_CLAVE    :  
         // OjO Suponemos q el Certisat Aqui no hace la distincion si es * Oficio � Clave
         if (tipcer_cve == FEA)
            error = m_Configuracion.getValorVar("[TITULOIMP]", "TREVFEA" , &Titulo);
         else if (tipcer_cve == SELLOS)
            error = m_Configuracion.getValorVar("[TITULOIMP]", "TREVSD" ,  &Titulo);
         else // if (tipcer_cve == AgCs)
            error = m_Configuracion.getValorVar("[TITULOIMP]", "TREVAGC" , &Titulo);
         break;
   case RENOVACION_CERT_DIG :	
         // Siempre son de FEA
         error = m_Configuracion.getValorVar("[TITULOIMP]", "TREN" , &Titulo);
         break;
   default:Titulo = "Titulo para impresion de Acuses del Certisat-Ventanilla";
   }	
   strcpy(tipacu_desc, (char*)Titulo.c_str());
   return (error);
}


/*##################################################################################
    PROPOSITO: Obtener la firma en Base64 de una cadena dada.
    PREMISA  : Los elementos de BD y de Mensajes deben estar iniciados.
    EFECTO   :
    ENTRADAS : Entero de referencia a la aplicacion en donde sucedio el error, error
       local de la aplicacion, mensaje, medio 'P', 'O'.
    SALIDAS  : regresa false.
###################################################################################*/
int CAcuses::getFirmaCadena(char *cadOri, uint i_cadOri, char *firma, uint16 *i_firma, char* mod_cve, int idMsg)
{
   i_cadOri = i_cadOri;
   RSA    *privKey = NULL;
   SGIRSA *rsa = NULL;
   uint8  password[512];
   uchar  by_Firma[512];
   string s_llave, s_pass, s_CadenaOriginal = cadOri;
   char   psz_numSerieAR[21];
   int    i_password = sizeof(password);
   uint   i_byFirma = sizeof(by_Firma);

   Bitacora->escribePV(BIT_INFO,"CAcuses en getFirmaCadena: Obteniendo los valores de la llave privada");
   error = m_Configuracion.getValorVar("[LLAVES_FIRMAR_ACUSE]", "PRIVK" , &s_llave);
   if (error)
   { 
      Bitacora->escribePV(BIT_ERROR,"Error en CAcuses en getFirmaCadena: No se obtuvieron los valores de la llave privada");
      return error;
    }
    
    
   Bitacora->escribePV(BIT_INFO,"CAcuses en getFirmaCadena: Obteniendo los valores del password de la llave privada");
   error = m_Configuracion.getValorVar("[LLAVES_FIRMAR_ACUSE]", "PWDPK" , &s_pass);
   if (error)
   {
      Bitacora->escribePV(BIT_ERROR,"Error en CAcuses en getFirmaCadena: No se obtuvieron los valores del password de la llave privada");
      return error;
    }
   
   Bitacora->escribePV(BIT_INFO,"CAcuses en getFirmaCadena: Desencriptando el password obtenido");
   if(!desencripta((uint8*)s_pass.c_str(), s_pass.length(), password, &i_password))
   {
      Bitacora->escribePV(BIT_ERROR,"Error en CAcuses en getFirmaCadena: No se desencripta el password");
      return ERR_NOT_DESENCRYPT_PWD;
    }
    
   password[i_password] = 0;

   privKey = RSA_new();
   if(privKey)
   {
     rsa = new SGIRSA;
     if(rsa)
     {
        error = sgiErrorBase(rsa->DecodPvKey((char*)s_llave.c_str(), (char*)password, &privKey));
        if(!error)
        {
           error = getSerialAR(psz_numSerieAR);
           if (!error)
           {           
              s_CadenaOriginal += psz_numSerieAR;
              if ( (!strncmp(mod_cve, MODULO_CERTISAT_WEB, 3)) && (idMsg == CERTREV) )
               s_CadenaOriginal += fcad_orig;
              error = sgiErrorBase(rsa->RSAFirma((uchar*)s_CadenaOriginal.c_str(), s_CadenaOriginal.length(), privKey, by_Firma, &i_byFirma, SHA1_ALG));
              if (!error)
              {
                 if (!B64_Codifica(by_Firma, i_byFirma, firma, i_firma))
                    error = ERR_NO_APL_B64;
              }        
           }
        }
     }else Bitacora->escribePV(BIT_ERROR,"Error en CAcuses en getFirmaCadena: No se cre� el objeto SGIRSA");
   }
   
   if (privKey)
      RSA_free(privKey);
   if (rsa)
      delete rsa;
   rsa     = NULL;
   privKey = NULL;
   strcpy(cadOri, (char*)s_CadenaOriginal.c_str());
   return error;
}

int CAcuses::getSerialAR(char *serial)
{
   SGIX509    *x509  = NULL;
   ManArchivo *fd = NULL;
   uchar      *by_Cert = NULL;
   string     s_PathCertificadoAR;
   int        i_tamArchivo = 0;

   x509 = new SGIX509;
   fd   = new ManArchivo;

   if(fd && x509)
   {
      error = m_Configuracion.getValorVar("[LLAVES_FIRMAR_ACUSE]", "CERT", &s_PathCertificadoAR);
      if(!error)
      {
          if(fd->TamArchivo((char*)s_PathCertificadoAR.c_str(), &i_tamArchivo))
          {
             by_Cert = new uchar[i_tamArchivo];
             if(by_Cert)
             {
                i_tamArchivo = i_tamArchivo;
                if(fd->ProcesaArchivo((char*)s_PathCertificadoAR.c_str(), by_Cert, &i_tamArchivo))
                   error = sgiErrorBase(x509->getSerial((const uchar*)by_Cert, i_tamArchivo, serial));
                else Bitacora->escribePV(BIT_ERROR, "CAcuses Error en getSerialAR: Error al procesar el archivo del Certificado Digital. (%i) errno: %i",
                     error, errno);
             }
             else Bitacora->escribe(BIT_ERROR, "CAcuses Error en getSerialAR: Error no se asigno memoria al buffer para el Certificado de la AR.");
          }
          else Bitacora->escribePV(BIT_ERROR, "CAcuses Error en getSerialAR: Error al abrir el Certificado Digital. errno: (%i)", errno);
      }
      else Bitacora->escribe(BIT_ERROR, "CAcuses Error en getSerialAR: Error al obtener el valor de la variable de configuraci�n.");
   }
   if(x509)
      delete x509;
   if(fd)
      delete fd;
   if(by_Cert)
      delete []by_Cert;
   x509    = NULL;
   fd      = NULL;
   by_Cert = NULL;
   return error;
}
//----------------------------------------------------------------------------------------

int CAcuses::
obtieneDatosRev( const char *cNumSerie, char* cMedio, char *cNumTram )
{
   if ( m_BDAR->consultaReg(ACU_OBTEN_DATOS_CERTIFICADO_REVOCADO, cNumSerie) )
   {
      if (m_BDAR->getValores("bb", cNumTram,cMedio))
         Bitacora->escribePV(BIT_INFO, "CAcuses en obtieneDatosRev: Se obtuvo de la base de datos  Tramite [%s], Motivo [%s]",cNumTram,cMedio);
      else
         Bitacora->escribe(BIT_ERROR, "CAcuses Error en obtieneDatosRev: Error al obtener los datos de la consulta.");
   }

   return 0;
}

