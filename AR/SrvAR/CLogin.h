#ifndef _CLOGIN_H_
#define _CLOGIN_H_

#include <COperacion.h>

#define ERR_MSG_NO_ESPERADO	-100

///////////////////////////////////////////////////////////////////////////////
// Valida el rfc en la base de datos
///////////////////////////////////////////////////////////////////////////////
class CLogin : public COperacion {
    public:
        CLogin(CConfigFile& config, MensajesSAT& msgcli, CSSL_Skt* skt);
        ~CLogin();
        static int m_iMsgRecib;
        static int errMsgRev;
        char rfcLogin[14];
        char rolUser[12];
        string nombre;
        
    protected:
        //*** Clases nuevas para redefinir en cada operacion
        virtual bool preProceso();
        virtual bool Proceso();
        virtual const char* getNombreOperacion(){ return "VALIDARLOGIN"; };
        virtual int getTipOper(){ return VALIDARLOGIN; };
        //***
        
    private:
        bool validarLogin();
};
#endif//_CLOGIN_H_
