#ifndef _CCONSULTACERT_H_
#define _CCONSULTACERT_H_
static const char* _CCONSULTACERT_H_VERSION_ ATR_USED = "SrvAR @(#)"\
"DSIC09041AR_ 2007-12-13 CConsultaCert.h 1.1.1/3";

//#VERSION: 1.1.1

#include <COperacion.h>

#define TAG_PATH_CERTS	"[AR]"
#define PATH_REP_CERTS	"REPOSITORIO"

#define ERR_OP_NO_RECONOCIDA	1000
#define ERR_BD_REGOPERDET	   1001
#define ERR_NO_CONS_BD		   1002
#define ERR_SET_MSG		   1003
#define ERR_NO_OBT_CERT		   1004
#define ERR_NO_EXISTE_REG	   1005
#define ERR_RFC_NO_COIN		   1006
#define ERR_NO_EXISTE_NSERIE       1007
//>+ ERGL (070108)
#define ERR_NO_EXISTE_RFC	1008
using namespace std;
class CConsultaCert : public COperacion
{
   private:
      char   m_numSer[21], m_fec_ini[50], m_fec_fin[50], m_bandera[10], m_Dato[128], m_agc[14];
      char   m_firmaCon[685], m_rfcCon[14];
      uchar *m_byCert;
      int    m_ibyCert, m_inumSer, m_ifec_ini, m_ifec_fin, m_ibandera, m_iDato, m_iagc;
      int    m_ifirmaCon, m_irfcCon;
      string m_pathCerts;
      static int m_MsgConsulta;
      char   psz_pathCerts[384];
      int    msgProceso(int i_op);
      int    consultaBD(void);
      int    getCertificado(void);
      void   iniciaVarsCons(void);
      void   terminaVarsCons(void);
   protected:
      bool        preProceso();
      bool        Proceso();
      const char* getNombreOperacion();
      int         getTipOper();
   public:
      CConsultaCert(CConfigFile& config, MensajesSAT& msgcli, CSSL_Skt* skt);
      ~CConsultaCert();
   
};

#endif //_CCONSULTACERT_H_
