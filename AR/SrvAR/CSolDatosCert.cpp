static const char* _CSOLDATOSCERT_CPP_VERSION_ ATR_USED = "SrvAR @(#)"\
"DSIC09041AR_ 2007-12-13 CSolDatosCert.cpp 1.1.1/3";

//#VERSION: 1.1.1
/*##############################################################################
  ###  PROYECTO:              PKI-SAT                                        ###
  ###  MODULO:                Solicitud de datos de certificado              ###
  ###                                                                        ###
  ###                                                                        ###
  ###  DESARROLLADORES:       H�ctor Ornelas Arciga           HOA            ###
  ###                                                                        ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       Lunes 23, enero de 2006                        ###
  ###                                                                        ###
  ##############################################################################
         1         2         3         4         5         6         7         8
12345678901234567890123456789012345678901234567890123456789012345678901234567890

################################################################################
  VERSION:
     V.1.0.0  (20060123- )    HOA: Version Original
     V.1.0.1  (20141014 -         ) JAB: Se agrupan las sentencias SQL utilizadas en el SrvAR en un archivo de
                                              cabecera

#################################################################################*/

//################################################################################
//<< rors 28 julio 2006   Se requiere por la migracion
#include <assert.h>
//>>
#include <SrvAR.h>
#include <CSolDatosCert.h>
#include <DefDatos.h>
//################################################################################
CSolDatosCert::CSolDatosCert(CConfigFile& config, MensajesSAT& msgcli, CSSL_Skt* skt) :
   COperacion(config, msgcli, skt)
{
}
//################################################################################
CSolDatosCert::~CSolDatosCert()
{
}
//################################################################################
bool CSolDatosCert::preProceso()
{
   assert(m_MsgCli.tipOperacion() == SOLDATOSCERT);

   char firma[TAM_FIRMA];
   int  lfirma = sizeof(firma), lnserie = sizeof(m_nSerie);
   
   intE error = m_MsgCli.getMensaje(SOLDATOSCERT, firma, &lfirma, m_nSerie, &lnserie);
   if (error)
      msgCliError('I', "Error en CSolDatosCert en PreProceso: Error al extraer los datos del mensaje", error, eMSGSAT);
   else
      Bitacora->escribePV(BIT_INFO, "CSolDatosCert: Recibe solicitud datos de certificado: %s", m_nSerie);
      
   return !error; 
}
//################################################################################
bool CSolDatosCert::Proceso()
{
   bool ok = false;
   char edo, vigFin[20], tip[10];
   
   if ( m_BDAR->consultaReg(SOL_DATOS_CERT_OBTEN_DATOS_CERTIFICADO_POR_NUM_SERIE, m_nSerie))
   {
      if ( m_BDAR->getValores("cbb", &edo, tip, vigFin))
      {
         Bitacora->escribePV(BIT_INFO, "CSolDatosCert: Datos de %s: edo=%c, tipo=%s, vig_fin=%s", m_nSerie, edo, tip, vigFin);
         intE error = m_MsgCli.setMensaje(DATOSCERT, &edo, 1, tip, strlen(tip), vigFin, strlen(vigFin));
         if (error)
            msgCliError('I', "Error en CSolDatosCert, en Proceso: Error al armar el mensaje de respuesta al cliente", error, eMSGSAT);
         else
            ok = !envia(CERTISAT);
      }
      else
      {
         char* msg = "CSolDatosCert: No existe registro del certificado solicitado";
         Bitacora->escribe(BIT_INFO, msg);
         sprintf(msg,"%s%s","Error en proceso en ",msg);
         msgCliError('N', msg, NOEXISTECERT, eAPL);
      }
   }
   else
   {
      char *msg = "Error al consultar informaci�n en la base de datos";
      
      //Para concatenar el nombre de la Clase
      sprintf(msg,"%s%s","Error en CSolDatosCert, en Proceso: ", msg);
      Bitacora->escribe(BIT_ERROR, msg);      
      
      
      //Bitacora->escribe(BIT_ERROR, msg);
      msgCliError('I', msg, 0, eIFX); 
   }
   
   return ok;   
}
//################################################################################
//################################################################################
//################################################################################

