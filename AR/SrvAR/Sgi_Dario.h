#ifndef _SGI_DARIO_H_ 
#define _SGI_DARIO_H_ 
static const char* _SGI_DARIO_H_VERSION_ ATR_USED = "SrvAR @(#)"\
"DSIC09041AR_ 2009-01-28 Sgi_Dario.h 1.1.0/3";

//#VERSION: 1.1.0
#include <Sgi_BD.h>
#include <Sgi_Bitacora.h>

#if ORACLE
   #include <CosltSrvAR_ORA.h>   // + JAB (20141014)
#else
   #include <CosltSrvAR.h>   // + JAB (20141014)
#endif

class Sgi_Dario
{
   private:
      CBD *bd;

   public:
      Sgi_Dario();
     ~Sgi_Dario();
      bool Conecta(const char* cInstancia, const char* cBD ,const char* cUsuario, const char* cPassword, const char* cRol );
      bool Consulta(const char* cDato, char* cResultado);

};

extern CBitacora* Bitacora;
#endif
