static const char* _CUTILBDAR_CPP_VERSION_ ATR_USED = "@(#) SrvAR ( L : DSIC10392AR_ : CUtilBDAR.cpp : 1.1.2 : 4 : 28/09/10)";

//#VERSION: 1.1.1
/*######################################################################################################################
  ###  PROYECTO:              PKI-SAT                                                                                ###
  ###  MODULO:                Clase que realiza las consultas, validaciones y otras funciones en la BD de la AR      ###
  ###                                                                                                                ###
  ###  DESARROLLADORES:       Gudelia Hern�ndez Molina        GHM                                                    ###
  ###  FECHA DE INICIO:       Miercoles 25 de enero del 2006                                                         ###
  ###                                                                                                                ###
  ######################################################################################################################
         1         2         3         4         5         6         7         8         9        10        11        12
1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*######################################################################################################################
   VERSION:  
      V.1.00      (20060125 -         ) GHM: Primera Versi�n
                                             Esta clase se gener� para realizar las validaciones en DARIO.
      V.1.01      (20081211 -         ) GHM: Agregar las consultas a la BD sobre la validaci�n de CRM para SDG
            .01                              - Verificar que la CveRegimen exista en la BD y obtener su operador
            .02   (20141001 -         ) JAB: Agrega el m�todo que ejecuta el procedimiento almacenado para validar si un
                                             regimen puede generar sellos digitales. Se eliminan m�todos ya no utilizados
                                             para la validaci�n
            .03   (20141011 -         ) JAB: Se agrupan las sentencias SQL utilizadas en el SrvAR en un archivo de
                                              cabecera
   CAMBIOS:
######################################################################################################################*/

#include <CUtilBDAR.h>

//######################################################################################################################
//Declaraci�n de los CONSTRUCTORES
CUtilBDAR::CUtilBDAR()
{
   m_CnxBDAR = NULL;
   m_inicializado = false;
}

//######################################################################################################################
//Declaraci�n del Destructor
CUtilBDAR::~CUtilBDAR()
{
   m_CnxBDAR = NULL;
}

//FUNCIONES DE INICIALIZACION
//######################################################################################################################
bool CUtilBDAR::Inicializa(CBD* varCnxBDAR)
{
   m_CnxBDAR = varCnxBDAR;

   if (!(m_inicializado = (m_CnxBDAR != NULL)))
   {
      TrataErrorProc('I', "Ha ocurrido un error al realizar la conexi�n a la base de datos, puesto que el objeto CBD recibido es nulo");
   }

   return m_inicializado;
}

bool CUtilBDAR::HayConexion()
{
   if (!m_inicializado)
   {
      TrataErrorProc('I', "No hay conexi�n activa con la base de datos");
   }

   return m_inicializado;
}

//FUNCIONES QUE OBTIENEN DATOS DE AR
//######################################################################################################################
bool CUtilBDAR::ObtSitFiscal(int cveSitFis, string * DescSF, char* tpoCerVal, char* valDomic )
{
   //GHM (000218): Se modific� para poder tener simultaneamente la version anterior y la nueva con CRM
   if ( !(m_CnxBDAR->consultaReg(UTIL_BDAR_OBTEN_DATOS_SITUACION_FISCAL_POR_CLAVE_1, cveSitFis)) )
   {
      sprintf(m_buffer, "Ha ocurrido un error al realizar la consulta de la Situaci�n Fiscal con clave (%d)", cveSitFis);
      TrataErrorProc('I', m_buffer);

      return false;
   }

   if ( !(m_CnxBDAR->getValores("sbb", DescSF, tpoCerVal, valDomic)) )
   {
      sprintf(m_buffer, "Ha ocurrido un error al obtener la Situaci�n Fiscal con clave (%d)", cveSitFis);
      TrataErrorProc('I', m_buffer);

      return false;
   }

   if (DescSF->length() == 0)
   {
       sprintf(m_buffer, "La Situaci�n Fiscal con clave (%d) no tiene descripci�n", cveSitFis);
       TrataErrorProc('O', m_buffer);

       return false;
   }

   return true;
}

//######################################################################################################################
/* No se utiliza
bool CUtilBDAR::ObtSitDomic(int cveSitDom, string * DescSD, char* tpoCerVal )
{
   //GHM (000218): Se modific� para poder tener simultaneamente la version anterior y la nueva con CRM
   if ( !(m_CnxBDAR->consultaReg(UTIL_BDAR_OBTEN_DATOS_SITUACION_FISCAL_POR_DOMICILIO, cveSitDom)) )
   {
      sprintf(m_buffer, "Ha ocurrido un error al realizar la consulta de la Situaci�n Domiciliaria con clave (%d)", cveSitDom);
      TrataErrorProc('I', m_buffer);

      return false;
   }

   if ( !(m_CnxBDAR->getValores("sb", DescSD, tpoCerVal)) )
   {
      sprintf(m_buffer, "Ha ocurrido un error al obtener la Situaci�n Domiciliaria con clave (%d)", cveSitDom);
      TrataErrorProc('I', m_buffer);

      return false;
   }

   if (DescSD->length() == 0)
   {
       sprintf(m_buffer, "La Situaci�n Domiciliaria con clave (%d) no tiene descripci�n", cveSitDom);
       TrataErrorProc('O', m_buffer);

       return false;
   }

   return true;
}
*/
//######################################################################################################################
bool CUtilBDAR::ObtFEAacxRFC(const char* RFCBsq, int* numCertAct, char* rfc, char* numSerie)
{
   if (!HayConexion())
      return false;

   if ( !m_CnxBDAR->ejecutaSP(UTIL_BDAR_SP_GET_FEAacxRFC_NOMBRE, UTIL_BDAR_SP_GET_FEAacxRFC_PARAMS, RFCBsq) )
   {
      sprintf(m_buffer, "Ha ocurrido un error al ejecutar el procedimiento sp_getFEAacxRFC con el RFC (%s)", RFCBsq);
      TrataErrorProc('I', m_buffer);

      return false;
   }

   if ( !m_CnxBDAR->getValores("ibb", numCertAct, rfc, numSerie) )
   {
      sprintf(m_buffer, "Ha ocurrido un error al obtener el resultado de la ejecuci�n del procedimiento sp_getFEAacxRFC con el RFC (%s)", RFCBsq);
      TrataErrorProc('I', m_buffer);

      return false;
   }

   if (*numCertAct > 0)
   {
      sprintf(m_buffer, "El RFC (%s) cuenta con (%i) certificados activos y el primero de ellos tiene el n�mero de serie (%s)", rfc, *numCertAct, numSerie);
      m_MsgDesc = string("CUtilBDAR(P): ") + string(m_buffer);
      Bitacora->escribe(BIT_INFO, m_MsgDesc.c_str());
       
      return true;
   }

   return false;
}

//######################################################################################################################
bool CUtilBDAR::tieneFEAact(char* RFCBsq)
{
   int numCertAct = 0;
   char rfcUtilCert[14];
   char numSerieCert[22];
   rfcUtilCert[0] = 0;

   return (ObtFEAacxRFC( RFCBsq, &numCertAct, rfcUtilCert, numSerieCert));
}

//######################################################################################################################
int CUtilBDAR::getNumCerts(const char* rfc, int tipo, char estado)
{
   //GHM:060201 Pendiente, se debe contemplar la construcci�n de un store procedure que haga esta tarea
   //           utilizando los RFC asociados y banderas del tipo de certificados 
   int  cuantos;
   bool ok = false;

   if (tipo && estado)
   {
      ok = m_CnxBDAR->consultaReg(UTIL_BDAR_OBTEN_TOTAL_CERTIFICADOS_POR_RFC_TIPO_Y_EDO, rfc, tipo, estado);
   }
   else if (tipo && !estado)
   {
      ok = m_CnxBDAR->consultaReg(UTIL_BDAR_OBTEN_TOTAL_CERTIFICADOS_POR_RFC_Y_TIPO, rfc, tipo);
   }
   else if (!tipo && estado)
   {
      ok = m_CnxBDAR->consultaReg(UTIL_BDAR_OBTEN_TOTAL_CERTIFICADOS_POR_RFC_Y_ESTADO, rfc, estado);
   }
   else
   {
      ok = m_CnxBDAR->consultaReg(UTIL_BDAR_OBTEN_TOTAL_CERTIFICADOS_POR_RFC, rfc);
   }

   if (!ok)
   {
      sprintf(m_buffer, "Ha ocurrido un error al realizar la consulta del total de certificados del RFC (%s)", rfc);
      TrataErrorProc('I', m_buffer);

      return -1;
   }

   if (!m_CnxBDAR->getValores("i", &cuantos))
   {
      sprintf(m_buffer, "Ha ocurrido un error al obtener el total de certificados del RFC (%s)", rfc);
      TrataErrorProc('I', m_buffer);

      return -1;
   }

   return cuantos;
}

//######################################################################################################################
bool CUtilBDAR::revSituacionFyD(uint16 tipoCert, int cveSitFis, int cveSitDom)
{
   int  tipCerVal   = 0; 
   char valDomic    = 'N';

   if ( !HayConexion())
      return false;

   //GHM (000218): Se modific� para poder tener simultaneamente la version anterior y la nueva con CRM
   if ( !(m_CnxBDAR->consultaReg(UTIL_BDAR_OBTEN_DATOS_SITUACION_FISCAL_POR_CLAVE_2, cveSitFis)) )
   {
       sprintf(m_buffer, "Ha ocurrido un error al realizar la consulta de la Situaci�n Fiscal con clave (%i)", cveSitFis);
       TrataErrorProc('I', m_buffer);

       return false;
   }

   if ( !(m_CnxBDAR->getValores("ic", &tipCerVal, &valDomic)) )
   {
      sprintf(m_buffer, "Ha ocurrido un error al obtener la Situaci�n Fiscal con clave (%i)", cveSitFis);
      TrataErrorProc('I', m_buffer);

      return false;
   }

   if ((tipCerVal & tipoCert) == 0)
   {
      sprintf(m_buffer, "La Situaci�n Fiscal (%i) no permite generar certificados del tipo solicitado (%i)", cveSitFis, tipoCert);
      TrataErrorProc('N', m_buffer);

      return false;
   }

   if (valDomic == 'S')
   {
      //GHM (000218): Se modific� para poder tener simultaneamente la version anterior y la nueva con CRM
      if ( !(m_CnxBDAR->consultaReg(UTIL_BDAR_OBTEN_TIPO_SITUACION_DOM_POR_CLAVE, cveSitDom)) )
      {
          sprintf(m_buffer, "Ha ocurrido un error al realizar la consulta de la Situaci�n Domiciliaria con clave (%i)", cveSitDom);
          TrataErrorProc('I', m_buffer);

          return false;
      }

      if ( !(m_CnxBDAR->getValores("i", &tipCerVal)) )
      {
          sprintf(m_buffer, "Ha ocurrido un error al obtener la Situaci�n Domiciliaria con clave (%i)", cveSitDom);
          TrataErrorProc('I', m_buffer);

          return false;
      }

      if ((tipCerVal & tipoCert) == 0)
      {
         sprintf(m_buffer, "La Situaci�n Domiciliaria (%i) no permite generar certificados del tipo solicitado (%i)", cveSitDom, tipoCert);
         TrataErrorProc('N', m_buffer);

         return false;
      }
   }
   //else No es necesario evaluar la situaci�n de domicilio

   return true;
}

//######################################################################################################################

//######################################################################################################################
int CUtilBDAR::valGenTipCDxAGC(const char* agc, int tipo)
{
   int cuantos;

   if (!m_CnxBDAR->consultaReg(UTIL_BDAR_OBTEN_TOTAL_AGENTE_POR_CLAVE_Y_TIPO_CERT, agc, tipo))
   {
      sprintf(m_buffer, "Ha ocurrido un error al realizar la consulta del total de certificados de tipo (%d) del agente (%s)", tipo, agc);
      TrataErrorProc('I', m_buffer);

      return -1;
   }
   
   if (!m_CnxBDAR->getValores("i", &cuantos))
   {
      sprintf(m_buffer, "Ha ocurrido un error al obtener el total de certificados de tipo (%d) del agente (%s)", tipo, agc);
      TrataErrorProc('I', m_buffer);

      return -1; 
   }

   return cuantos;
}

//######################################################################################################################
bool CUtilBDAR::getEdoAgCyCert(const char* agc, char* edoAgC, char* edoCert)
{
   if (!m_CnxBDAR->consultaReg(UTIL_BDAR_OBTEN_DATOS_AGENTE_Y_CERT_POR_CLAVE_AGC, agc))
   {
      sprintf(m_buffer, "Ha ocurrido un error al realizar la consulta del estado del agente (%s)", agc);
      TrataErrorProc('I', m_buffer);

      return false;
   }
   
   if (!m_CnxBDAR->getValores("cc", edoAgC, edoCert))
   {
      sprintf(m_buffer, "Ha ocurrido un error al obtener el estado del agente (%s)", agc);
      TrataErrorProc('I', m_buffer);

      return false; 
   }

   return true;
}

//######################################################################################################################
/* Retorna:   -1      Error interno
               0      Si no hay ningun registro de esa llave
               1      Se ejecuto correctamente
*/
int CUtilBDAR::getEdoyTipCert(const char* numSerie, char* edoCert, int *tipCert)
{
   if ( !(m_CnxBDAR->consultaReg(UTIL_BDAR_OBTEN_DATOS_CERTIFICADO_POR_NUM_SERIE,numSerie)) )
   {
      sprintf(m_buffer, "Ha ocurrido un error al realizar la consulta del estado y tipo del certificado con n�mero de serie (%s)", numSerie);
      TrataErrorProc('I', m_buffer);

      return -1;
   }

   if ( !(m_CnxBDAR->getValores("ci", edoCert, tipCert)) ) //Puede no existir datos
   {
      //GHM:080407 Se expresa mal el error
      //Bitacora->escribe(BIT_ERROR, "CUtilBDAR(I): Al extraer los datos obtenidos del store sp_valLlaveUnica");
      sprintf(m_buffer, "No se obtuvo datos del certificado con n�mero de serie (%s)", numSerie);
      TrataErrorProc('I', m_buffer);

      return 0;
   }

   return 1;
}

//######################################################################################################################
/* Retorna:   -1      Error interno
               0      Si no hay ningun registro de esa llave  
               n > 0  Clave de error de negocio
               La descripci�n del error de negocio se puede ver en m_MsgDesc
*/
int CUtilBDAR::valLlaveUnica(const char* RFCBsq, const char* digPKmd5, const char* digPKsha1 )
{
   int cveErrorNeg;

   //Valida el n�mero de serie en la BD
   if ( !(m_CnxBDAR->ejecutaSP(UTIL_BDAR_SP_VAL_LLAVE_UNICA_NOMBRE, UTIL_BDAR_SP_VAL_LLAVE_UNICA_PARAMS, RFCBsq, digPKmd5, digPKsha1)) )
   {
      sprintf(m_buffer, "Ha ocurrido un error al ejecutar el procedimiento sp_valLlaveUnica para el RFC (%s)", RFCBsq);
      TrataErrorProc('I', m_buffer);

      return -1;
   }

   //Verifica el resultado obtenido
   if ( !(m_CnxBDAR->getValores("is", &cveErrorNeg, &m_MsgDesc)) )
   {
      sprintf(m_buffer, "Ha ocurrido un error al obtener el resultado de la ejecuci�n del procedimiento sp_valLlaveUnica para el RFC (%s)", RFCBsq);
      TrataErrorProc('I', m_buffer);

      return -1;
   }

   if (cveErrorNeg > 0)
   {
      sprintf(m_buffer, "%s", m_MsgDesc.c_str());
      TrataErrorProc('N', m_buffer);
   }

   return cveErrorNeg;
}

//>>> ERGL (070116)
bool CUtilBDAR::getFecVigFin(const char *numSerie, char *fecha)
{
   if( !m_CnxBDAR->consultaReg(UTIL_BDAR_OBTEN_VIG_FIN_CERTIFICADO_POR_NUM_SERIE, numSerie) )
   {
      sprintf(m_buffer, "Ha ocurrido un error al realizar la consulta de la fecha de vigencia final del certificado con n�mero de serie (%s)", numSerie);
      TrataErrorProc('I', m_buffer);

      return false;
   }

   if( !m_CnxBDAR->getValores("b", fecha) )
   {
      sprintf(m_buffer, "Ha ocurrido un error al obtener la fecha de vigencia final del certificado con n�mero de serie (%s)", numSerie);
      TrataErrorProc('I', m_buffer);

      return false;
   }

   return true;
}
//<<<

//######################################################################################################################
/**
 * Ejecuta el procedimiento almacenado que valida si un regimen puede generar sellos digitales
 *
 * @param cve_regimen La clave del regimen a validar
 * @param cve_roles   La lista de claves de roles a validar junto con el regimen
 * @param cve_obligs  La lista de claves de obligaciones a validar junto con el regimen
 * @param cve_activs  La lista de claves de actividades a validar junto con el regimen
 * @param mensaje     El mensaje resultado de la ejecuci�n del procedimiento almacenado
 *
 * @return  -1  Error interno
 *          0   No puede generar sellos
 *          1   Puede generar sellos
 */
int CUtilBDAR::valGenSellos(int cve_regimen, const char* cve_roles, const char* cve_obligs, const char* cve_activs, string* mensaje)
{
  int validacion;

  // Si no hay conexi�n a la base de datos termina
  if (!HayConexion())
  {
    return -1;
  }

  // Ejecuta el procedimiento almacenado para validar si el regimen puede generar sellos digitales
  if (!(m_CnxBDAR->ejecutaSP(UTIL_BDAR_SP_VAL_GEN_SELLOS_NOMBRE, UTIL_BDAR_SP_VAL_GEN_SELLOS_PARAMS, cve_regimen, cve_roles, cve_obligs, cve_activs)))
  {
    sprintf(m_buffer, "Ha ocurrido un error al ejecutar el procedimiento sp_valida_gen_sd para el r�gimen con clave (%d)", cve_regimen);
    TrataErrorProc('I', m_buffer);

    return -1;
  }
  
  // Obtiene el resultado
  if (!(m_CnxBDAR->getValores("is", &validacion, mensaje)))
  {
    sprintf(m_buffer, "Ha ocurrido un error al obtener el resultado de la ejecuci�n del procedimiento sp_valida_gen_sd para el r�gimen con clave (%d)", cve_regimen);
    TrataErrorProc('I', m_buffer);
    
    return -1;
  }
  
  return validacion;
}
//######################################################################################################################


//FUNCIONES DE AFECTACION EN LA BD
//######################################################################################################################
int CUtilBDAR::RegOperDetalleBDAR(int cveProc, char* numOp, int numError)
{
   int consecutivo, errorSP;

   if ( !m_CnxBDAR->ejecutaSP(UTIL_BDAR_SP_REGISTRA_OPER_NOMBRE, UTIL_BDAR_SP_REGISTRA_OPER_PARAMS, numOp, cveProc, numError) )
   {
      sprintf(m_buffer, "Ha ocurrido un error al ejecutar el procedimiento sp_regOperDet para la operaci�n con n�mero (%s)", numOp);
      TrataErrorProc('I', m_buffer);

      return -1;
   }

   if (!m_CnxBDAR->getValores("ii", &errorSP, &consecutivo) )
   {
      sprintf(m_buffer, "Ha ocurrido un error al obtener el resultado de la ejecuci�n del procedimiento sp_regOperDet para la operaci�n con n�mero (%s)", numOp);
      TrataErrorProc('I', m_buffer);

      return -1;
   }

   if (errorSP != 0)
   {
      sprintf(m_buffer, "Ha ocurrido el error (%d) al ejecutar el procedimiento sp_regOperDet para la operaci�n con n�mero (%s)", errorSP, numOp);
      TrataErrorProc('I', m_buffer);

      return -1;
   }

   return consecutivo;
}

//######################################################################################################################
/* Retorna:   -1      Error interno
               0      Si no hay ningun registro de esa llave
               n > 0  Clave de error de negocio
               La descripci�n del error de negocio se puede ver en m_MsgDesc
*/
bool CUtilBDAR::delLlaveApartada(const char* numOper, int secuencia )
{
   bool Ok;

   if( !(Ok = m_CnxBDAR->ejecutaOper(UTIL_BDAR_BORRA_LLAVE_APARTADA_POR_OPERACION, numOper, secuencia)))
   {
      sprintf(m_buffer, "Ha ocurrido un error al ejecutar la consulta para eliminar la llave apartada de la operaci�n con n�mero (%s) y secuencia (%i)", numOper, 
      secuencia);
      TrataErrorProc('I', m_buffer);
   }

   return Ok;
}

//######################################################################################################################
/* Ya no se utiliza
bool CUtilBDAR::RegErrorOpDetBDAR( char* numOp, int numError )
{
  if (!m_CnxBDAR->ejecutaSP(UTIL_BDAR_SP_REGISTRA_ERROR_NOMBRE, UTIL_BDAR_SP_REGISTRA_ERROR_PARAMS, numOp, numError))
  {
      sprintf(m_buffer, "Ha ocurrido un error al ejecutar el procedimiento sp_regError para la operaci�n con n�mero (%s) y el error con n�mero (%d)", numOp, 
      numError);
      TrataErrorProc('I', m_buffer);

      return false;
  }

  return true;
}*/

//######################################################################################################################
int CUtilBDAR::ApartaLlave(const char* numOper, const char* rfc, const char* dig_md5, 
                           const char* dig_sha1, int* oper_sec)
{
   bool ok_ejecuta = false;

   #if ORACLE
      ok_ejecuta = m_CnxBDAR->ejecutaFuncion(UTIL_BDAR_SP_REGISTRA_LLAVE_APARTADA_NOMBRE, UTIL_BDAR_SP_REGISTRA_LLAVE_APARTADA_RETURN, UTIL_BDAR_SP_REGISTRA_LLAVE_APARTADA_PARAMS, numOper, rfc, dig_md5, dig_sha1);
   #else
      ok_ejecuta = m_CnxBDAR->ejecutaSP(UTIL_BDAR_SP_REGISTRA_LLAVE_APARTADA_NOMBRE, UTIL_BDAR_SP_REGISTRA_LLAVE_APARTADA_PARAMS, numOper, rfc, dig_md5, dig_sha1);
   #endif

   if (!ok_ejecuta)
   {
      sprintf(m_buffer, "Ha ocurrido un error al ejecutar el procedimiento sp_regLlvApart para la operaci�n con n�mero (%s) y el RFC (%s)", numOper, rfc);
      TrataErrorProc('I', m_buffer);

      return false;
   }

   if (!m_CnxBDAR->getValores("i", oper_sec))
   {
      sprintf(m_buffer, "Ha ocurrido un error al obtener el resultado de la ejecuci�n del procedimiento sp_regLlvApart para la operaci�n con n�mero (%s) y el RFC (%s)", numOper, rfc);
      TrataErrorProc('I', m_buffer);

      return false;
   }

   return true;
}


//#############################################################################################################
/* Obtiene el num. de nivel del AgC de la DB, de la tabla AGENTE, campo: nivel_cve
   (in)  agcCve   : es la clave del Agente Certificador i.e. COperacion::m_agcCve
   OjO MELM RETORNA 0 si hay error � regresa en la variable error el campo 'nivel_cve' de la tabla 'AGENTE'
*/
int CUtilBDAR::getNivelAGC(const char *agcCve)
{
   int error = 0;

   if ( !m_CnxBDAR->consultaReg(UTIL_BDAR_OBTEN_NIVEL_AGENTE_POR_CLAVE, agcCve) || 
        !m_CnxBDAR->getValores("i", &error ) ) 
   {
      error = 0; //error = ERR_GET_NIVEL_AGC;
      sprintf(m_buffer, "Ha ocurrido un error al realizar la consulta del nivel del agente con clave (%s)", agcCve);
      TrataErrorProc('I', m_buffer);
   }

   return error;
}

//#############################################################################################################
bool CUtilBDAR::RegistraRFC( char * cRFCOrig , char * cRFC )
{
    char  cRFCAux[15];
    char  cRFCOAux[15];
    string  sRFCA_aux;

    if( strlen(cRFC) == 12)
       sprintf(cRFCAux ," %s",cRFC);
    else 
       sprintf(cRFCAux ,"%s",cRFC);
    
    if( strlen(cRFCOrig) == 12)
       sprintf(cRFCOAux ," %s",cRFCOrig);
    else
       sprintf(cRFCOAux ,"%s",cRFCOrig);
        
    
    if( m_CnxBDAR->consultaReg(UTIL_BDAR_OBTEN_RFC_RFC_ASOCIADO_POR_RFC_ORIG, cRFCOAux ,cRFCAux) )
    {
       
       if(m_CnxBDAR->getValores("s", &sRFCA_aux ))
       {
          if(!strcmp(sRFCA_aux.c_str(), cRFCAux))
             return true;
       }
       else
       {
          sprintf(m_buffer, "Ha ocurrido un error al obtener el RFC de la consulta del RFC Asociado al RFC Orig (%s)", cRFCOAux);
          TrataErrorProc('I', m_buffer);
       }
       
       if( !m_CnxBDAR->ejecutaOper(UTIL_BDAR_AGREGA_RFC_ASOCIADO, cRFCOAux , cRFCAux  ) )
       {
         sprintf(m_buffer, "Ha ocurrido un error al realizar la inserci�n de los datos (%s, %s) del RFC Asociado", cRFCOAux ,cRFCAux);
         TrataErrorProc('I', m_buffer);

         return false;
       }
       else 
         return true;             
    }

    sprintf(m_buffer, "Ha ocurrido un error al realizar la consulta del RFC Asociado al RFC Orig (%s)", cRFCOAux);
    TrataErrorProc('I', m_buffer);

    return false;
}

//#############################################################################################################
// Valida la Vigencia del num_serie en la DB de la AR
/* Retorna:   -1      Error interno
               0      Si no hay ningun registro de esa llave
               1      Se ejecuto correctamente
*/
int CUtilBDAR::verificaVigencia(const char* numSerie, int *vigente)
{
   bool ok_ejecuta = false;

   #if ORACLE
      ok_ejecuta = m_CnxBDAR->ejecutaFuncion(UTIL_BDAR_SP_VAL_FECHA_CERTIFICADO_NOMBRE, UTIL_BDAR_SP_VAL_FECHA_CERTIFICADO_RETURN, UTIL_BDAR_SP_VAL_FECHA_CERTIFICADO_PARAMS, numSerie);
   #else
      ok_ejecuta = m_CnxBDAR->ejecutaSP(UTIL_BDAR_SP_VAL_FECHA_CERTIFICADO_NOMBRE, UTIL_BDAR_SP_VAL_FECHA_CERTIFICADO_PARAMS, numSerie);
   #endif

   if ( !ok_ejecuta )
   {
      sprintf(m_buffer, "Ha ocurrido un error al ejecutar el procedimiento sp_valFecCert para el certificado con n�mero de serie (%s)", numSerie);
      TrataErrorProc('I', m_buffer);

      return -1;
   }

   if ( !(m_CnxBDAR->getValores("i", vigente)) ) // Puede no existir datos
   {
      sprintf(m_buffer, "Ha ocurrido un error al obtener el resultado de la ejecuci�n del procedimiento sp_valFecCert para el certificado con n�mero de serie (%s)", numSerie);
      TrataErrorProc('I', m_buffer);

      return 0;
   }

   return 1;
}

//######################################################################################################################
/* Retorna:   -1      Error interno
               0      Si el rfc no esta bloqueado
               1      Si el rfc esta Bloqueado
*/
int CUtilBDAR::RFCBloqueado(const char* RFCBsq )
{
   int bloqueado;
   bool ok_ejecuta = false;

   #if ORACLE
      ok_ejecuta = m_CnxBDAR->ejecutaFuncion(UTIL_BDAR_SP_VAL_BLOQUEO_NOMBRE, UTIL_BDAR_SP_VAL_BLOQUEO_RETURN, UTIL_BDAR_SP_VAL_BLOQUEO_PARAMS, RFCBsq );
   #else
      ok_ejecuta = m_CnxBDAR->ejecutaSP(UTIL_BDAR_SP_VAL_BLOQUEO_NOMBRE, UTIL_BDAR_SP_VAL_BLOQUEO_PARAMS, RFCBsq );
   #endif
   
   if ( !ok_ejecuta )
   {
      sprintf(m_buffer, "Ha ocurrido un error al ejecutar el procedimiento sp_verif_bloq para el RFC (%s)", RFCBsq);
      TrataErrorProc('I', m_buffer);

      return -1;
   }

   if ( !(m_CnxBDAR->getValores("i", &bloqueado)) )
   {
      sprintf(m_buffer, "Ha ocurrido un error al obtener el resultado de la ejecuci�n del procedimiento sp_verif_bloq para el RFC (%s)", RFCBsq);
      TrataErrorProc('I', m_buffer);

      return -1;
   }

   return bloqueado;
}

//#############################################################################################################
/* Obtiene el CURP del  RFC daddo
   rfc   : Es el RFC del contribuyente que se va buscar la curp
*/
bool  CUtilBDAR::getCURP(const char *rfc, string* sCURP )
{
   bool  resp = false;
   int error;

   if ( !m_CnxBDAR->consultaReg(UTIL_BDAR_OBTEN_CURP_CERTIFICADO_POR_RFC, rfc) || !m_CnxBDAR->getValores("i", &error ) )
   {
      sprintf(m_buffer, "Ha ocurrido un error al realizar la consulta de la CURP del RFC (%s)", rfc);
      TrataErrorProc('I', m_buffer);
   }
   else
   {
      if ( !(m_CnxBDAR->getValores("s", sCURP)) )
      {
         sprintf(m_buffer, "Ha ocurrido un error al obtener la CURP del RFC (%s)", rfc);
         TrataErrorProc('I', m_buffer);
      }
      else
      {
        resp = true;
      }
   }

   return resp;
}


//######################################################################################################################
//FUNCIONES PARA EL MANEJO DE ERRORES
//######################################################################################################################

// - <<< JAB (20141014)
/*bool CUtilBDAR::TrataErrorProc(int itpoMsg, const char* sCausa )
{
   //indicador de tipo de error (Interno)
   m_tipoError = 'I';
   char inicio[] = "Se present� un error ";
   char soluc[]  = ", \n favor de intentar nuevamente, si se vuelve a presentar el error solicite "
                   "apoyo al inform�tico local";
   switch (itpoMsg)
   {
      case MSGDESC_ICS:
         m_MsgDesc = string(inicio) + string(sCausa) + string(soluc);
         break;
      case MSGDESC_CS:
         m_MsgDesc = string(sCausa) + string(soluc);
         break;
      case MSGDESC_VACIO :
         m_MsgDesc = "";
         break;
   }
   return false;
}
*/
// >>> - JAB (20141014)

/**
 * Trata el error ocurrido
 *
 * @param tipoError El tipo de error ocurrido, ('I')Interno, ('N')Negocio u otro
 * @param error El mensaje del error ocurrido
 */
void CUtilBDAR::TrataErrorProc(char tipoError, const char* error)
{
   m_tipoError = tipoError;
   m_MsgDesc   = string(error);

   Bitacora->escribePV(BIT_ERROR, "CUtilBDAR(%c): %s", m_tipoError, error);
}
