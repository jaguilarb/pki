static const char* _CSOLCERT_CPP_VERSION_ ATR_USED = "@(#) SrvAR ( L : DSIC10392AR_ : CSolCert.cpp : 1.1.4 : 5 : 28/09/10)";

//#VERSION: 1.1.2
/*######################################################################################################################
  ###  PROYECTO:              PKI-SAT                                                                                ###
  ###  MODULO:                Clase que realiza las funciones de soporte de la generaci�n de certificados            ###
  ###                                                                                                                ###
  ###  DESARROLLADORES:       Gudelia Hern�ndez Molina        GHM                                                    ###
  ###                         H�ctor Ornelas Arciga           HOA                                                    ###
  ###  FECHA DE INICIO:       Lunes 28, noviembre del 2005                                                           ###
  ###                                                                                                                ###
  ######################################################################################################################
         1         2         3         4         5         6         7         8         9        10        11        12
1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*######################################################################################################################
   VERSION:
      V.1.00      (20051206 - 20060101) GHM: Primera Versi�n
                                             Atiende las solicitudes de generaci�n de certificados.
      V.1.1.1.1  (080424) SERR: Se  agregaron las consultas de los tres tipos de agentes  para que solo se pueda tener un certificado de agente.
      V.1.1.3    (100414) MAML: se agrega validacion del tama�o del nombre del contribuyente
   CAMBIOS:
      V.1.1.4     (20141014 -         ) JAB: Se agrupan las sentencias SQL utilizadas en el SrvAR en un archivo de
                                              cabecera
######################################################################################################################*/
#include <sys/socket.h>

//<< RORS  28 julio 2006
#include <assert.h>
//>> 
#include <string.h>       //Manejo de Cadena

#include <CGeneraCert.h>   //Clase Padre
#include <CUtilBDAR.h>

#include <SrvAR.h>        //Definiciones del Servicio
//- #include <Sgi_Pki.h> //- GHM (070321): Se modific� el nombre de la librer�a 
#include <Sgi_PKI.h> //+ GHM (070321)
#include <CAcuses.h>
#include <CSolCert.h>

//######################################################################################################################
//Declaraci�n de los CONSTRUCTORES
CSolCert::CSolCert(CConfigFile& config, MensajesSAT& msgcli, CSSL_Skt* skt) :
   CGeneraCert(config, msgcli, skt)
{
   m_valDat_FIEL = NULL;
   m_valDat_SDG  = NULL;
}
//######################################################################################################################
//Declaraci�n del Destructor
CSolCert::~CSolCert()
{
   if (m_valDat_FIEL)
   {
      delete m_valDat_FIEL;
      m_valDat_FIEL = NULL;
   }
   if (m_valDat_SDG)
   {
      delete m_valDat_SDG;
      m_valDat_SDG = NULL;
   }
}

//######################################################################################################################
bool CSolCert::preProceso()
{
   assert(m_MsgCli.tipOperacion() == SOLCERT);

   bool ok = false;
   char bandera[2], tipo[6], rfc[512];
   int  lrfc = sizeof(rfc), lbandera = sizeof(bandera), ltipo = sizeof(tipo);

   if (!setMemReqYCert())
   {
      msgCliError(m_errTipo, m_errDesc, m_errCod, eAPL); 
      return false;
   }

   intE error = m_MsgCli.getMensaje(SOLCERT, m_firmaOp, &m_ifirmaOp, rfc, &lrfc, bandera, &lbandera,
                                    tipo, &ltipo, m_req, &m_lreq);
   if (error)
      msgCliError('I', "Error en CSolCert en preProceso: Error al extraer los datos del mensaje", error, eMSGSAT);
   else
   {
      strncpy(m_rfc, rfc, 13);
      m_rfc[13 - (rfc[12] == '|')] = 0;
      
      m_tipo = atoi(tipo);

      // Modificaci�n por la revocaci�n juridica
      // Si se quiere generar un certificado de Sello digital se deber� revisar que no este bloqueado, para fiel no es necesario
      if (m_tipo == 2)
      {
         if ( GetRFCBloqueado(m_rfc) )
         {
            Bitacora->escribePV(BIT_INFO, "CSolCert: El RFC se encuentra bloqueado, no puede generar certificados. %s:%s", m_rfc, tipo);
            setVarsErr('N', 1,"Error en CSolCert en preProceso: El RFC se encuentra bloqueado, no puede generar certificados.");
            msgCliError(m_errTipo, m_errDesc, m_errTipo, eAPL);
            return false;
         }
      }


      if(m_tipo != 1 && m_tipo != 2)
      {

        if( !VerificacionDatos(rfc))
        { 
           //msgCliError('I', "Error en Verificar Datos", error, eMSGSAT);
           //MELM 20/Dic/2006 : Se supone que la funcion: setVarsErr() YA inicio las variables d Error => asi las mandamos
           msgCliError(m_errTipo, m_errDesc, m_errTipo, eAPL);
           return ok;
        }
      }
      
      if (m_tipo != TIPCER_FEA && m_tipo != TIPCER_SD)
         m_rfcext = m_rfc;
         
      ok = (m_utilBDAR->valGenTipCDxAGC(m_agcCve, m_tipo) > 0); // RevisaNivelAGC
      if (ok)
      {
         Bitacora->escribePV(BIT_INFO, "CSolCert: Recibe solicitud de certificado: %s:%s", m_rfc, tipo);
         error = m_MsgCli.getCadOrig(m_cadOriOp, &m_icadOriOp);
         if ((ok = !error) == false)
            msgCliError('I', "Error en CSolCert en preProceso: Error al obtener la cadena original", error, eMSGSAT);
         else
         {
            strcpy(m_rfcOp, m_rfc);
            m_refArchOp[0] = 0;
            m_firmaOp[m_ifirmaOp] = 0;
         }
      }
      else
      {
         //string msg = string("El AGC ") + m_agcCve + " no tiene nivel para generar CDs de tipo " + tipo;
         string msg = string("Error en CSolCert en preProceso: El AGC ") + m_agcCve + " no tiene nivel para generar CDs de tipo " + tipo;
         msgCliError('N', msg.c_str(), ERR_NIVAGC, eAPL);
      }

      assert(m_tipo == TIPCER_FEA || m_tipo == TIPCER_SD || m_tipo == TIPCER_AGC ||
             m_tipo == TIPCER_AGC_SGI || m_tipo == TIPCER_AGC_ADM || m_tipo == TIPCER_AGC_REV);
   }

   return ok;
}
//######################################################################################################################


//######################################################################################################################
bool CSolCert::Proceso()
{
   bool  ok = false;
   int   res;

   if ((res = AlmacenaReq(0, true)) == 0  &&
       (res = ValidaSujeto() )      == 0  &&
       (res = ValidaReq()    )      == 0  &&
       (res = ApartaLlave()  )      == 0  &&
       (res = Genera()       )      == 0  )
   {  
      m_utilBDAR->RegOperDetalleBDAR(CERTGEN, m_numOp);

      // YA TENGO LA FECHA DE OPERACION d CERTGEN => PUEDO GENERAR EL ACUSE
      CAcuses m_acuse(m_BDAR, m_Configuracion, m_numOp, m_numTram);
      if ( genAcuseGenCert( &m_acuse)  )
      {
         m_MsgCli.setMensaje(CERTGEN, m_acuse.cad_orig, strlen(m_acuse.cad_orig), 
                                      m_acuse.firma,    strlen(m_acuse.firma), m_cert, m_lcert);
         envia(CERTISAT);
         ok = true;
      }   
      else msgCliError('N', "Error en CSolCert en Proceso: Certificado Generado, se present� un problema al generar informaci�n del Acuse.\n Favor de levantar un" 
      "reporte a" 
      "Mesa de Ayuda.", res, eAPL);
               
   }
   else 
      msgCliError(m_errTipo, m_errDesc, res, eAPL); 
   
   return ok;
}
//######################################################################################################################
// MAML 16/Enero/2007: p/hacer eficiente manejo de errores
int CSolCert::ValidaSujeto()
{
   int iError = TRAMNOEXITO;
   
   switch (m_tipo)
   {
      case TIPCER_FEA: // FEA
                       iError = ValidaFIELWS(); break;
      case TIPCER_SD:  // Sellos Digitales
                       iError = ValidaSDGWS();  break;
      case TIPCER_AGC_SGI:
      case TIPCER_AGC_ADM:
      case TIPCER_AGC: // AGC
      case TIPCER_AGC_REV: 
                       iError = ValidaAGC(); break;
      default:
      {
         Bitacora->escribePV(BIT_ERROR, "Error en CSolCert en ValidaSujeto: Solicitud de tipo de CD inv�lido (%d)", m_tipo);
         //msgCliError('N', "Tipo de certificado no permitido para esta versi�n.", 10, eAPL);
         setVarsErr('N', TRAMNOEXITO, "Error en CSolCert en ValidaSujeto: Tipo de certificado no permitido para esta versi�n.");
      }
   }
   return iError;
}
//######################################################################################################################
//OJO: ESta funci�n debe ser sustituida por CValDatRFC_FIEL.
bool CSolCert::ValidaSFyD()
{
   int  isf, isd;

   return m_utilBDAR->revSituacionFyD((uint16) m_tipo, isf, isd);
}

//#####################

int CSolCert::
ValidaFIELWS()
{
   int iError = 0;

   if ( IniValidaWS_IDC() )
   {
      bool bio = false, bfea = false;// , bsits =false;
      int  cCerts;
      char rfcfea[14], nserie[23];

      // RORS797E
     
      bio   = m_valDat_FIEL->TieneBiom();
      //bsits = m_valDat_FIEL->ValidaSFyD();

      if( getTipOper() != SOLREN)
      {
         m_utilBDAR->ObtFEAacxRFC(m_rfc, &cCerts, rfcfea, nserie);
         bfea  = cCerts > 0;
      }

	  if (!bio || bfea)
      {
         string msg("Error en CSolCert en ValidaFIELWS: No se puede generar el Certificado Digital de FIEL solicitado:");
         if (!bio) {
            msg+= string("\n\tNo se encuentran registrados los datos biom�tricos");
            iError = BIOMETRICOS_NOREGS;
         }
         if (bfea) {
            msg += string("\n\tCuenta con un Certificado Digital de FIEL activo (") + rfcfea + nserie + ")";
            iError = TIENECERTFEA_ACTIVO;
         }
         /*if (!bsits) {
            msg += string("\n\tSituaci�n de domicilio o fiscal no permitida");
            iError = SITDOMFISNOPERM;
         }*/

         setVarsErr ('N', iError, msg.c_str());
      }
     /*if (!bio || !bsits || bfea)
      {
         string msg("No se puede generar el Certificado Digital de FIEL solicitado:");
         if (!bio) {
            msg+= string("\n\tNo se encuentran registrados los datos biom�tricos");
            iError = BIOMETRICOS_NOREGS;
         }
         if (bfea) {
            msg += string("\n\tCuenta con un Certificado Digital de FIEL activo (") + rfcfea + nserie + ")";
            iError = TIENECERTFEA_ACTIVO;
         }
         if (!bsits) {
            msg += string("\n\tSituaci�n de domicilio o fiscal no permitida");
            iError = SITDOMFISNOPERM;
         }

         setVarsErr ('N', iError, msg.c_str());
      }*/
      else if (m_nombre.length() > TAM_NOMBRE) 
      {
         // MAML 100413: Validamos el tama�o del nombre o razon social del Contribuyente de tal manera q se guarde en la DB
         //        NOTA: S�lo lo hacemos para certif. FIEL ya que es el primero que se crea, el de Sellos es Derivado y el de AGC <= 150
         Bitacora->escribePV(BIT_ERROR, "Error en CSolCert en ValidaFIELWS: Inv�lido tama�o(%d) en nombre del contribuyente=%s\n\t contra el limite en la BD (%d)",
                                         m_nombre.length(), m_nombre.c_str() , TAM_NOMBRE);
         iError = NOINTEGRO;
         setVarsErr ('N', iError, "Error en CSolCert en ValidaFIELWS: No se puede generar el Certificado Digital de FIEL solicitado:\n\tEl tama�o del nombre" 
         "(o razon social) es inv�lido" );
      }
      //- FinValidaWS_IDC(); //GHM (090402): Se env�o la liberaci�n de variables al destructor
   }
   else
      setVarsErr('I', iError = ERR_INIWS, "Error en CSolCert en ValidaFIELWS: Error al iniciar IniValidaWS_IDC");

    
   return iError;
}
//######################################################################################################################
//######################################################################################################################
//######################################################################################################################
int CSolCert::
ValidaSDGWS()
{
   int iError = 0;

   if ( IniValidaSDWS_IDC() )
   {
      bool bfea, bsits, bobl;
      int  cCerts;
      char rfcfea[14], nserie[23];

      m_utilBDAR->ObtFEAacxRFC(m_rfc, &cCerts, rfcfea, nserie);
      bfea  = cCerts > 0;
      bsits = m_valDat_SDG->ValidaSFyD();   //Valida situcai�n Fiscal y domicilio
      bobl  = m_valDat_SDG->CerObl();       //Validaciones fiscales

      if (!bfea || !bsits || !bobl)
      {
         string msg("Error en CSolcert en validaSDGWS: No se puede generar el Certificado Digital de Sellos Digitales solicitado:");
         if (!bfea)
         {
            msg += string("\n\tNo cuenta con un Certificado Digital de FIEL activo");
            iError = CERTFEANOACTIVO;
         }
         if (!bsits)
         {
            msg += string("\n\tSituaci�n de domicilio o fiscal no permitida");
            iError =  SITDOMFISNOPERM ;
         }
         if (!bobl)
         {
            msg += string("\n\tRevisar las obligaciones fiscales");
            iError =  OBLIGACIONESFISINV;
         }

         if ( iError )
            setVarsErr ('N', iError, msg.c_str());
      }
      //FinValidaWS_IDC(); //GHM (090402): Se env�o la liberaci�n de variables al destructor
   }
   else
      setVarsErr('I', iError = ERR_INIWS, "Error en CSolCert en ValidaSDGWS: Error al iniciar IniValidaSDGWS_IDC");

   return iError;
}

//######################################################################################################################
int CSolCert::ValidaAGC()
{
   int iError = ERR_VALGENCERT;

   // SERR Thu Apr 24 13:43:56 CDT 2008   Se  agregaron las consultas de los tres tipos de agentes  para que solo se pueda tener un certificado de agente.
   int cCerts = m_utilBDAR->getNumCerts(m_rfc, 3,'A');
   cCerts += m_utilBDAR->getNumCerts(m_rfc, 4, 'A');
   cCerts += m_utilBDAR->getNumCerts(m_rfc, 5, 'A');

   if (cCerts < 0)
      setVarsErr ('I', ERR_VALGENCERT, "Error en CSolCert en ValidaAGC: Error al obtener el n�mero de CDs del AGC"); 
   else if (cCerts > 0)
   {
      string msg("Error en CSolCert en ValidaAGC: No se puede generar el Certificado Digital de AGC solicitado:"
                 "\n\tEl AGC cuenta con un Certificado Digital de AGC activo");
      setVarsErr ('N', TRAMNOEXITO, msg.c_str() );
   }
   else
      iError = 0;
   
   return iError;
}
//######################################################################################################################
void CSolCert::Error(bool enviaError, char tipo, const char* msg, int error, eIdLib eId, int errorBD)
{
   
      //Concatenar el nombre de la clase
      char msgConClase[100]="Error en CSolCert: ";
      sprintf(msgConClase,"%s%s",msgConClase, msg);
      
   if (enviaError)
   {
      Bitacora->escribe(BIT_ERROR, error, msgConClase);
      //Bitacora->escribe(BIT_ERROR, error, msg);
      msgCliError(tipo, msg, errorBD, eId); 
   }
   else
      //Bitacora->escribe(BIT_ERROR, error, msg);
      Bitacora->escribe(BIT_ERROR, error, msgConClase);
}

//######################################################################################################################
// MAML 16/Enero/2007: P/hacer eficiente manejo de errores
void CSolCert::ArmaError( char tipo, const char* msg, int error, int errorBD)
{

   //Concatenar el nombre de la clase
   char msgConClase[200]="Error en CSolCert ";
   sprintf(msgConClase,"%s%s",msgConClase, msg);
   Bitacora->escribe(BIT_ERROR, error, msgConClase);
      
   //Bitacora->escribe(BIT_ERROR, error, msg);
   setVarsErr(tipo, errorBD, msg);  
}
//######################################################################################################################
// MAML 16/Enero/2007: de bool a int, P/hacer eficiente manejo de errores
int CSolCert::AlmacenaReq(int idx, bool enviaError)
{
   enviaError = enviaError;
   //bool ok = false;
   intE error = TRAMNOEXITO;
   string repositorio, rutaReq;
   
   if ((error = m_Configuracion.getValorVar("[REPOSITORIO]", "REQUERIMIENTOS", &repositorio)) == 0)
   {
      if ((error = pkiRutaReq(repositorio, m_numTram, rutaReq, true, idx)) == 0)
      {
         if (access(rutaReq.c_str(), F_OK) && (errno == ENOENT))
         {
            ofstream freq(rutaReq.c_str(), ios::out | ios::trunc | ios::binary);
            if (freq)
            {
               //<< rors   28 julio 2006  se agrego el cast a char* ya se requeriea por la migraci�n             
               freq.write((char*)m_req, m_lreq);
               //>>
               if (freq.good())
                  error = 0; //ok = true;
               else
                  ArmaError( 'I', "en AlmacenaReq: Error al escribir el archivo de requerimiento", errno, TRAMNOEXITO);
            }
            else
               ArmaError( 'I', "en AlmacenaReq: Error al crear el archivo de requerimiento", errno, TRAMNOEXITO);
         }
         else
            ArmaError( 'I', "en AlmacenaReq: El archivo existe en la ruta del repositorio de requerimientos", errno, TRAMNOEXITO);
      }
      else
         ArmaError( 'I', "en AlmacenaReq: Error al armar la ruta del repositorio de requerimientos", error, TRAMNOEXITO);      
   }
   else
      ArmaError( 'I', "en AlmacenaReq: Error al leer los datos de configuraci�n del repositorio de requerimientos", error, TRAMNOEXITO);

   return error;
}

//########################################################################################################################
bool CSolCert::getAcuseDatos(string *sNo_Serie,string *sNombre,string  *sPubK,string *sModulo,string *sNombreRL,string *sRFCRL)
{
   if( !m_BDAR->consultaReg(SOL_CERT_OBTEN_DATOS_CERTIFICADO_POR_RFC_Y_TIPO,  m_rfc, m_tipo ) )
   {  
      Bitacora->escribePV(BIT_ERROR, "Error en CSolCert en GetAcuseDatos: Error al consultar los datos del certificado por RFC y tipo");
      return false;
    }

   if( !m_BDAR->getValores("sss",sNo_Serie , sNombre , sPubK ))
   {
      Bitacora->escribePV(BIT_ERROR, "Error en CSolCert en GetAcuseDatos: Error al obtener los valores",sNo_Serie,sNombre,sPubK);
      return false;
    }

   if( !m_BDAR->consultaReg(SOL_CERT_OBTEN_MODULO_AGENTE_POR_CLAVE, m_agcCve) )
   {
      Bitacora->escribePV(BIT_ERROR, "Error en CSolCert en GetAcuseDatos: Error al consultar los datos del Agente por clave");      
      return false;
    }
    
   if( !m_BDAR->getValores("s", sModulo))
   {
      Bitacora->escribePV(BIT_ERROR, "Error en CSolCert en GetAcuseDatos: Error al obtener el valor",sModulo);
      return false;
    }

   if( strlen(m_rfc) == 12 || (m_reqRFCRL[0] != 0) )
   {
      if( !m_BDAR->consultaReg(SOL_CERT_OBTEN_NOMBRE_CERTIFICADO_POR_RFC, m_reqRFCRL))
      {
         Bitacora->escribePV(BIT_ERROR, "Error en CSolCert en GetAcuseDatos: Error al consultar los datos del certificado por RFC y el requerimientyo del RFC del" 
         "RF");
         return false;
       }
      if( !m_BDAR->getValores("s",sNombreRL ))
      {
         Bitacora->escribePV(BIT_ERROR, "Error en CSolCert en GetAcuseDatos: Error al obtener los valores", sNombreRL);
         return false;
       }
       
      *sRFCRL = m_reqRFCRL;
   }
   else
   {
      *sNombreRL = *sNombre;
      *sRFCRL    = m_rfc;
   }   
   return (true);
}



bool CSolCert::genAcuseGenCert(CAcuses *pacuse)
{
   int error = 0;
   string sNo_Serie, sNombre, sPubK, sModulo, sNombreRL, sRFCRL;
   
   if (  (error = !getAcuseDatos(&sNo_Serie, &sNombre, &sPubK, &sModulo, &sNombreRL, &sRFCRL)) != 0 ||
         (error = pacuse->generaAcuse(CERTGEN, GENERACION_CERT_DIG, m_rfc, sNombre.c_str(),
                 sNo_Serie.c_str(), sPubK.c_str(), sRFCRL.c_str(), sNombreRL.c_str(), sModulo.c_str(), 
                 m_tipo, -1, "", "") ) != 0 )
   {
      sprintf (m_errDesc, "Error en CSolCert en genAcuseGenCert: no pudo generarse el Acuse de Gen Certif. en la DB, numTramite=%s", m_numTram );
      setVarsErr('I', error, m_errDesc);
   }
   else
      Bitacora->escribePV(BIT_INFO, "CSolCert en genAcuseGenCert: Se genero Acuse en Gen. Certif. numTramite=%s, cad_orig=%s, firma=%s", 
                                          m_numTram, pacuse->cad_orig, pacuse->firma );
   return (!error);
}

//######################################################################################################################
//>>+ GHM (090402): Para la sustituci�n de validaciones en DARIO, se cambio de CGenera a CSolCert
bool CSolCert::IniValidaWS_IDC()
{
   limpiaVarsErr();

   m_valDat_FIEL = new CValidaDatosRFC(m_Configuracion, m_utilBDAR, TIPCER_FEA);
   if ( m_valDat_FIEL == NULL)
   {
      Bitacora->escribe(BIT_ERROR, "Error en CSolCert en IniValidaWS_IDC: No se inicializ� el objeto CValidaDatosRFC");
      return false;
   }
   if ( !m_valDat_FIEL->ObtieneDatosWS(m_rfc) )
   {
      Bitacora->escribe(BIT_ERROR, "Error en CSolCert en IniValidaWS_IDC: No se obtuvo la informaci�n del RFC consultado");
      return false;
   }
   if ( !m_valDat_FIEL->getNombreRFC(m_nombre) )
   {
      Bitacora->escribe(BIT_ERROR, "Error en CSolCert en IniValidaWS_IDC: No se pudo obtener el nombre del RFC consultado");
      return false;
   }
   char rfcOrigCons[15];
   if ( ! m_valDat_FIEL->getRFCOrig((char*)rfcOrigCons) )
   {
      Bitacora->escribe(BIT_ERROR, "Error en CSolCert en IniValidaWS_IDC: No se obtuvo correctamente el RFC de consulta");
      setVarsErr('N', 1, m_valDat_FIEL->m_MsgDesc.c_str());
      return false;
   }
//   Se mueve a otro lado
//   if ( GetRFCBloqueado(rfcOrigCons) )
//   {
//      setVarsErr('N', 1,"El RFC se encuentra bloqueado, no puede generar certificados.");
//      return false;
//   }
   if (m_valDat_FIEL->getStatusRFC() == -1)
      setVarsErr('N', 1, m_valDat_FIEL->m_MsgDesc.c_str());
   Registra_RFC();
   return true;
}

bool CSolCert::IniValidaSDWS_IDC()
{
   limpiaVarsErr();

   m_valDat_SDG = new CValDatRFC_SDG(m_Configuracion, m_utilBDAR, TIPCER_SD);
   if (m_valDat_SDG == NULL)
   {
      Bitacora->escribe(BIT_ERROR, "Error en CSolCert en IniValidaSDWS_IDC: No se inicializ� el objeto CValDatRFC_SDG");
      return false;
   }
   if (!m_valDat_SDG->ObtieneDatosWS(m_rfc) )
   {
      Bitacora->escribe(BIT_ERROR, "Error en CSolCert en IniValidaSDWS_IDC: No se obtuvo la informaci�n del RFC consultado");
      return false;
   }
   char rfcOrigCons[15];
   if ( ! m_valDat_SDG->getRFCOrig((char*)rfcOrigCons) )
   {
      Bitacora->escribe(BIT_ERROR, "Error en CSolCert en IniValidaSDWS_IDC: No se obtuvo correctamente el RFC de consulta");
      setVarsErr('N', 1, m_valDat_SDG->m_MsgDesc.c_str());
      return false;
   }
//   if ( GetRFCBloqueado(rfcOrigCons) )
//   {
//      setVarsErr('N', 1,"El RFC se encuentra bloqueado, no puede generar certificados.");
//      return false;
//   }

   if ( !m_valDat_SDG->getNombreRFC(m_nombre) )
   {
      Bitacora->escribe(BIT_ERROR, "Error en CSolCert en IniValidaSDWS_IDC: No se pudo obtener el nombre del RFC consultado");
      return false;
   }
   if (m_valDat_SDG->getStatusRFC() == -1)
      setVarsErr('N', 1, m_valDat_SDG->m_MsgDesc.c_str());
      Registra_RFC();
   return true;
}

bool CSolCert::Registra_RFC()
{
   char cRFCOrig[14];
   int iAvance = 0;
   bool bResp  = false;

   if ( m_tipo == TIPCER_FEA )
      bResp = m_valDat_FIEL->getRFCOrig(cRFCOrig);
   else if( m_tipo == TIPCER_SD )
      bResp = m_valDat_SDG->getRFCOrig(cRFCOrig);

   if(bResp)
   {
      if( cRFCOrig[0] == ' ')
         iAvance = 1 ;
      if(strcmp(m_rfc ,cRFCOrig + iAvance) )
        return m_utilBDAR->RegistraRFC( cRFCOrig , m_rfc );
   }
   return false;
}

//>>- GHM (090402): Se env�o al destructor de CSolCert
//<<+ GHM (090402)

