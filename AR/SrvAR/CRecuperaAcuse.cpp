static const char* _CRECUPERAACUSE_CPP_VERSION_ ATR_USED = "@(#) SrvAR ( L : DSIC10392AR_ : CRecuperaAcuse.cpp : 1.1.2 : 3 : 28/09/10)";

//#VERSION: 1.1.1
/*##############################################################################
  ###  PROYECTO:              PKI-SAT                                        ###
  ###  MODULO:                Recuperar los datos del tabla ACUSE de la AR   ###
  ###                                                                        ###
  ###                                                                        ###
  ###  DESARROLLADORES:       MELM				             ###
  ###                                                                        ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       Jueves 16, febrero del 2006                    ###
  ###                                                                        ###
  ##############################################################################
         1         2         3         4         5         6         7         8
12345678901234567890123456789012345678901234567890123456789012345678901234567890*/

#include <assert.h>
#include <Sgi_BD.h>
//- #include <CConfigFile.h> //- GHM (070430)
#include <Sgi_ConfigFile.h>  //+ GHM (070430)
#include <CAcuses.h>
#include <COperacion.h>
#include <CRecuperaAcuse.h>

CRecuperaAcuse::CRecuperaAcuse(CConfigFile& config, MensajesSAT& msgcli, CSSL_Skt* skt):
   COperacion(config, msgcli, skt)
{
   m_tipoRec = ' ';	// Tipo de la Recuperacion: 'T', 'G', 'R'
   m_Tram_Serie[0] = 0;	// Numero de Tramite o de Serie
}

//################################################################################
bool CRecuperaAcuse::preProceso()
{
   assert(m_MsgCli.tipOperacion() == RECACUSE);

   char firma[TAM_FIRMA];
   int  lfirma   = sizeof(firma), ltipoRec = sizeof(m_tipoRec), lTram_Serie = sizeof(m_Tram_Serie);

   intE error = m_MsgCli.getMensaje(RECACUSE, firma, &lfirma, &m_tipoRec,    &ltipoRec, m_Tram_Serie, &lTram_Serie);
   if (error)
   {
      msgCliError('I', "CRecuperaAcuse.preProceso: Error al extraer los datos del mensaje RECACUSE", error, eMSGSAT);
   }
   else
   {
      Bitacora->escribePV(BIT_INFO, "CRecuperaAcuse: Recepci�n de la solicitud de datos del acuse (tipoRec='%c', numTram_Serie='%s'", m_tipoRec, m_Tram_Serie);
   }

   return !error;
}

//################################################################################
// Busca el Acuse de las operaciones EXITO: CERTREN, CERTREV, CERTGEN y TRANSCOMPLETA
bool CRecuperaAcuse::Proceso()
{
   int    tipacu_cve, tipcer_cve, error = 0, icadSolRev = 0;
   char   buff_tipacu_cve[3], buff_tipcer_cve[3], m_errDesc[512], cadSolRev[300];
   icadSolRev = sizeof(cadSolRev);
   
   CAcuses m_acuse(m_BDAR, m_Configuracion);
   if ( (error = m_acuse.buscaAcuse(&tipacu_cve, &tipcer_cve, m_tipoRec, m_Tram_Serie)) == 0 ) 
   {
      Bitacora->escribePV(BIT_INFO, "CRecuperaAcuse: OK se recupero el acuse, tipoRec='%c', nTramSerie='%s', tipacu_cve=%d, tipcer_cve=%d, cad_orig=%s, firma=%s",
                          m_tipoRec, m_Tram_Serie, tipacu_cve, tipcer_cve, m_acuse.cad_orig, m_acuse.firma );
      
      sprintf(buff_tipacu_cve, "%d", tipacu_cve);
      sprintf(buff_tipcer_cve, "%d", tipcer_cve);
      
      if ( tipacu_cve == 2 || tipacu_cve == 3 )
      {
         error = m_acuse.buscaCadSolRev((char*)m_Tram_Serie, cadSolRev, &icadSolRev);
         if (icadSolRev <= 28)
         {
            cadSolRev[0] = '.';
            icadSolRev = 1;
            cadSolRev[1] = '\0';
         }
      }
      else
      {
         cadSolRev[0] = '.';
         icadSolRev = 1;
         cadSolRev[1] = '\0';
      }
      
      Bitacora->escribe(BIT_INFO, "CRecuperaAcuse: Creaci�n del mensaje DATACUSE");

      m_MsgCli.setMensaje(DATACUSE, cadSolRev, icadSolRev,
                                    m_acuse.cad_orig, strlen(m_acuse.cad_orig),
                                    m_acuse.firma,    strlen(m_acuse.firma),
                                    buff_tipacu_cve,  strlen(buff_tipacu_cve), 
                                    buff_tipcer_cve,	strlen(buff_tipacu_cve) );

      Bitacora->escribe(BIT_INFO, "CRecuperaAcuse: Envio del mensaje DATACUSE al CERTISAT");

      envia(CERTISAT); // manda el Mensaje al CERTISAT
   }
   else if ( error == ACUSE_VERS_ANT ) 
   {
      // Es un Error q se muestra en el Certisat
      sprintf (m_errDesc, "CRecuperaAcuse.Proceso: Acuse de la versi�n anterior, datos no disponibles(%s)", m_Tram_Serie );
      msgCliError('N', m_errDesc, error, eIFX);
   }
   else if (error == ERR_GET_REN_ACUSE)
   {
      sprintf (m_errDesc, "CRecuperaAcuse.Proceso: El certificado fue revocado en un proceso de renovacion" );
      msgCliError('N', m_errDesc, error, eIFX);
   }
   else if (error == ERR_GET_AUT_ACUSE)
   {
      sprintf (m_errDesc, "CRecuperaAcuse.Proceso: El certificado fue revocado en un proceso de revocacion por autoridad" );
      msgCliError('N', m_errDesc, error, eIFX);
   }
   else if (error == ERR_GET_NUM_ACUSE)
   {
      sprintf (m_errDesc, "CRecuperaAcuse.Proceso: No se encuentra el acuse registrado" );
      msgCliError('N', m_errDesc, error, eIFX);
   }
   

/*
   else  
   { // Si no hay acuse => AVISA
      //>- ERGL (070108)
      sprintf (m_errDesc, "Error: no pudo ENCONTRARSE el Acuse en la DB, tipoRec='%c', nTramSerie='%s'", 
               m_tipoRec, m_Tram_Serie );
      -</
      //>+ ERGL (070108)
      sprintf (m_errDesc, "El certificado fue revocado en un proceso de renovacion." );
      />- ERGL (070108)
      msgCliError('I', m_errDesc, error, eIFX);
      -</
      //>+ ERGL (070108)
      msgCliError('N', m_errDesc, error, eIFX);
   }
*/
   return (!error);
}

//################################################################################
