static const char* _CEDOTRAMITE_CPP_VERSION_ ATR_USED = "SrvAR @(#)"\
"DSIC09041AR_ 2007-12-13 CEdoTramite.cpp 1.1.1/3";

//#VERSION: 1.1.1
/*##############################################################################
  ###  PROYECTO:              Librer�as generales                            ###
  ###  MODULO:                CEdoTram: Clase con la que se obtiene la lista ###
  ###                         de operaciones de un tramite. Los datos para   ###
  ###                         la clase son: el n�mero de tramite y la banera ###
  ###                         que indica si es el ultimo(0) o todos(1)       ###
  ###                                                                        ###
  ###  DESARROLLADORES:                                                      ###
  ###                         Roc�o Alejandra Mart�nez Olayo  RAMO           ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       enero 2006                                     ###
  ###                                                                        ###
  ##############################################################################
  1         2         3         4         5         6         7         8
  12345678901234567890123456789012345678901234567890123456789012345678901234567890
*/

/*#################################################################################
    VERSION:
      V.1.00      (20060117 - 20060118 RAMO: Versi�n original
          .1      (20141014 -         ) JAB: Se agrupan las sentencias SQL utilizadas en el SrvAR en un archivo de
                                              cabecera
    CAMBIOS:
 #################################################################################*/
#include <CEdoTramite.h>
#include <string.h>
/*#################################################################################*/
CEdoTramite::CEdoTramite(CConfigFile& config, MensajesSAT& msgCli, CSSL_Skt* sktProc):
             COperacion(config, msgCli, sktProc)
{
}
//*#################################################################################*/
bool CEdoTramite::preProceso()
{
   return true;
}
/*#################################################################################*/
bool CEdoTramite::Proceso()
{
   char firma[TAM_FIRMA];
   int lfirma = sizeof(firma);
   int resultado = 0;
   char num_tram[13];
   int lnum_tram;
   char bandera[1];
   int lbandera;
   int mensajeRegreso = RESULNUMTRAM; 
   
   char tipErr[2];
   
   sprintf(tipErr,"%s","P"); 
   
   if((resultado = m_MsgCli.getMensaje(SOLEDOTRAM, firma, &lfirma, num_tram, &lnum_tram,
                                    bandera, &lbandera)) == 0 )
   {
      Bitacora->escribePV(BIT_INFO, "CEdoTramite.Proceso: Se obtienen los datos ( %s , %s )",num_tram,bandera);

      if(f_obtenerCadenaTramite(num_tram, bandera))
      {
         if(m_cadenaTramite == "")
         {
            Bitacora->escribe(BIT_INFO, "CEdoTramite.Proceso: No existe n�mero de operaci�n");
            mensajeRegreso = MSGERROR;
         }
         if((resultado = enviaMensaje(mensajeRegreso, "N", NOEXISTETRAM, "No existe n�mero de operaci�n")) == 0)
            return true;
      }
      else
      {
         Bitacora->escribe(BIT_ERROR,"Error en CEdoTramite en Proceso: No se obtuvo cadena con datos del tramite ");
         resultado = enviaMensaje(MSGERROR, "I", ERR_PROCESO, "No se obtuvieron datos del tr�mite");
      }
   }
   else
   {
      resultado = enviaMensaje(mensajeRegreso, "I", ERR_PROCESO, "Al procesar mensaje");
      Bitacora->escribe(BIT_ERROR,"Error en CEdoTramite en Proceso: Al obtener cuerpo(n�mero, bandera)del mensaje: SOLEDOTRAM");
   }
   return false;   
}
/*#################################################################################*/
int CEdoTramite::enviaMensaje(int mensajeRegreso, char* tip_Err, int error_cve, char *err_Desc )
{
   int res = 1;
   
   switch(mensajeRegreso)
   {
      case MSGERROR:
         //char err_Desc[40];
         char cod_error[3];
         sprintf(cod_error,"%d", error_cve);
         res = m_MsgCli.setMensaje(MSGERROR,
                                   tip_Err, strlen(tip_Err), cod_error, strlen(cod_error),
                                   err_Desc, strlen(err_Desc));
         break;
         
      case RESULNUMTRAM:
          res = m_MsgCli.setMensaje(RESULNUMTRAM, m_cadenaTramite.c_str(),
                                               m_cadenaTramite.length());
          break;     
   }
                
   if (res == 0)
   {
      if((res = this->m_SktProc->Envia((uint8*)m_MsgCli.buffer,
                                                    m_MsgCli.tamMensaje())) == 0)
      {             
         m_BDAR->ejecutaSP(EDO_TRAM_SP_REGISTRA_OPER_NOMBRE, EDO_TRAM_SP_REGISTRA_OPER_PARAMS, m_numOp, RESULNUMTRAM, 0);
         return res;
      }
      else
      {
         m_BDAR->ejecutaSP(EDO_TRAM_SP_REGISTRA_OPER_NOMBRE, EDO_TRAM_SP_REGISTRA_OPER_PARAMS, m_numOp, MSGERROR, 93);
         Bitacora->escribe(BIT_ERROR, "Error en CEdoTramite en enviaMensaje: No se envio el mensaje al cliente");
      }
   }
   else
   {
      m_BDAR->ejecutaSP(EDO_TRAM_SP_REGISTRA_OPER_NOMBRE, EDO_TRAM_SP_REGISTRA_OPER_PARAMS, m_numOp, MSGERROR, 93);
      Bitacora->escribe(BIT_ERROR,"Error en CEdoTramite en enviaMensaje: No se formo el mensaje para enviar al cliente");
   }
   return res;    
}
/*#################################################################################*/
bool CEdoTramite::f_obtenerCadenaTramite(const char * num_tramite, char *bandera)
{
   bool ok_ejecuta = false;

   #if ORACLE
      ok_ejecuta = m_BDAR->ejecutaFuncion(EDO_TRAM_SP_SEGUIMIENTO_TRAMITE_NOMBRE, EDO_TRAM_SP_SEGUIMIENTO_TRAMITE_RETURN, EDO_TRAM_SP_SEGUIMIENTO_TRAMITE_PARAMS,num_tramite, bandera);
   #else
      ok_ejecuta = m_BDAR->ejecutaSP(EDO_TRAM_SP_SEGUIMIENTO_TRAMITE_NOMBRE, EDO_TRAM_SP_SEGUIMIENTO_TRAMITE_PARAMS,num_tramite, bandera);
   #endif

   if( !ok_ejecuta )
      return false;
   if(!m_BDAR->getValores("s", &m_cadenaTramite ))
      return false;
   return true;
}


