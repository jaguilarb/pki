/*##############################################################################
  ###  PROYECTO:              Librer�as generales                            ###
  ###  MODULO:                CEdoTram: Clase con la que se obtiene la lista ###
  ###                         de operaciones de un tramite. Los datos para   ###
  ###                         la clase son: el n�mero de tramite y la banera ###
  ###                         que indica si es el ultimo(0) o todos(1)       ###
  ###                                                                        ###
  ###  DESARROLLADORES:                                                      ###
  ###                         Roc�o Alejandra Mart�nez Olayo  RAMO           ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       enero 2006                                     ###
  ###                                                                        ###
  ##############################################################################
  1         2         3         4         5         6         7         8
  12345678901234567890123456789012345678901234567890123456789012345678901234567890
*/

/*#################################################################################
    VERSION:
      V.1.00      (20060117 - 20060118 RAMO: Versi�n original
          .1      (20141014 -         ) JAB: Se agrupan las sentencias SQL utilizadas en el SrvAR en un archivo de
                                              cabecera
    CAMBIOS:
#################################################################################*/

#ifndef _CEDOTRAMITE_H_
#define _CEDOTRAMITE_H_
static const char* _CEDOTRAMITE_H_VERSION_ ATR_USED = "SrvAR @(#)"\
"DSIC09041AR_ 2007-12-13 CEdoTramite.h 1.1.1/3";

//#VERSION: 1.1.1
/*###############################################################################*/
#include <COperacion.h>
#include <Sgi_SrvUnx.h>
#include <Sgi_Bitacora.h>
/*#################################################################################*/
using namespace std;
class CEdoTramite:public COperacion
{
   public:

   CEdoTramite(CConfigFile &archivo_conf, MensajesSAT &msgCli, CSSL_Skt *sktProc);
   virtual bool Proceso();   

   protected:
   string m_cadenaTramite; 
   int    lm_cadenaTramite;
      
   virtual const char * getNombreOperacion() {return "SOLEDOTRAM"; };
   virtual int          getTipOper () {return SOLEDOTRAM;};
   virtual bool         preProceso();
   int                  enviaMensaje(int mensajeRegreso, char *tip_Err, int error_cve, char *err_Desc);
   //int                  enviaMensaje(int mensajeRegreso, char* tip_Err);
   bool                 f_obtenerCadenaTramite(const char * num_tramite,char *bandera);

};

#endif //_CEDOTRAMITE_H_



