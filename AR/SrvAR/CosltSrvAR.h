/*######################################################################################################################
  ###  PROYECTO:              PKI-SAT                                                                                ###
  ###  MODULO:                Contiene las sentencias sql utilizadas en el SrvAR                                     ###
  ###                                                                                                                ###
  ###  DESARROLLADORES:       Jonathan Aguilar Baltazar   JAB                                                        ###
  ###  FECHA DE INICIO:       Viernes 10, octubre de 2014                                                            ###
  ###                                                                                                                ###
  ######################################################################################################################
         1         2         3         4         5         6         7         8         9        10        11        12
123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
*/
/*######################################################################################################################
   VERSION:
       V.1.00      (20141010 -         ) JAB: Primera Versión
   CAMBIOS:
######################################################################################################################*/

#ifndef _COSLTSRVAR_H_
#define _COSLTSRVAR_H_


// ******************************  SENTENCIAS SQL ACUSES  *********************************
#define ACU_EXISTE_ACUSE_POR_NUM_DE_TRAMITE       "SELECT 1 FROM acuse WHERE tramite_cve = '%s'"
#define ACU_OBTEN_DATOS_ACUSE_POR_NUM_DE_TRAMITE  "SELECT tipacu_cve, nombre, cad_orig, firma, tipcer_cve FROM acuse WHERE tramite_cve = '%s'"
#define ACU_AGREGA_ACUSE                          "INSERT INTO acuse(tramite_cve,tipacu_cve,mod_cve,nombre,llave_pub,rfc_sol,nombre_sol,cad_orig,firma,fecha_acu,tipcer_cve) values ('%s',%d,'%s','%s','%s','%s','%s','%s','%s','%s',%d)"
#define ACU_OBTEN_ULT_FECHA_DE_OPERACION_EXITOSA  "SELECT FIRST 1 fec_op  FROM operacion WHERE soloper_cve = '%s' AND proc_cve = %d ORDER BY fec_op DESC"
#define ACU_OBTEN_CAMPO_X_DE_TABLA_X_POR_CAMPO_X  "SELECT %s FROM %s WHERE %s = %s"
#define ACU_OBTEN_DATOS_CERTIFICADO_REVOCADO      "SELECT TRAMITE_CVE,MEDIO FROM REV_DET  WHERE NO_SERIE ='%s'"


// ******************************  SENTENCIAS SQL CONEXION  *********************************
#define CON_OBTEN_DATOS_AGENTE_POR_NUM_SERIE            "SELECT nivel_cve, mod_cve FROM Agente WHERE no_serie = '%s'"
#define CON_OBTEN_PUERTO_AUT_POR_APLICACION_1           "SELECT puerto FROM Ver_Aut WHERE id_apl = %i AND fec_ini <= CURRENT AND NVL(fec_fin||'.000', '2099-12-31 23:59:59.000') >= CURRENT"
#define CON_OBTEN_PUERTO_AUT_POR_APLICACION_2           "SELECT puerto FROM Ver_Aut WHERE id_apl = %i AND fec_ini <= CURRENT AND NVL(fec_fin||'.000', '2100-12-31 23:59:59.000') >= CURRENT"
#define CON_OBTEN_DATOS_MOTIVOS_DE_REVOCACION           "SELECT motrev_cve, motrev_desc FROM cat_motivo_revoca"
#define CON_OBTEN_DATOS_TIPO_DE_CERTIFICADOS_POR_AGENTE "SELECT C.tipcer_cve,C.tipcer_desc FROM nivel_tip_cer N, cat_tip_cer C WHERE  N.nivel_cve = (select nivel_cve from agente where agc_cve = upper('%s')) AND C.tipcer_cve = N.tipcer_cve"
#define CON_OBTEN_NIVEL_AGENTE_POR_CLAVE                "SELECT nivel_cve FROM Agente WHERE AgC_cve = '%s'"
#define CON_OBTEN_DATOS_MOTIVOS_DE_REVOCACION_MOSTRADOS "SELECT motrev_cve, motrev_desc FROM cat_motivo_revoca WHERE mostrar = 'S'"
#define CON_SP_VALIDA_AGC_NOMBRE                        "sp_validaAgC"
#define CON_SP_VALIDA_AGC_PARAMS                        "'%s'"
#define CON_SP_CONEXION_V2_NOMBRE                       "sp_conexion_v2"
#define CON_SP_CONEXION_V2_PARAMS                       "'%s',%i,'%s',%i,%i"
#define CON_SP_DESCONEXION_V2_NOMBRE                    "sp_desconexion_v2"
#define CON_SP_DESCONEXION_V2_PARAMS                    "'%s', %i"


// ******************************  SENTENCIAS SQL CONSULTA CERT  *********************************
#define CONS_CERT_SP_REGISTRA_OPER_NOMBRE                             "sp_regOperDet"
#define CONS_CERT_SP_REGISTRA_OPER_PARAMS                             "'%s',%d,%d"
#define CONS_CERT_OBTEN_CADENA_DATOS_CERTIFICADO_POR_NUM_SERIE_1      "select edo_cer||'|'||tipcer_cve||'|'||rfc from certificado where no_serie = '%s'"
#define CONS_CERT_OBTEN_CADENA_DATOS_CERTIFICADO_POR_NUM_SERIE_2      "select edo_cer ||'|'|| tipcer_cve,rfc from certificado where no_serie = '%s'"
#define CONS_CERT_OBTEN_ULT_NUM_SERIE_DE_FIEL_POR_RFC                 "select first 1 no_serie from certificado where rfc = '%s' and tipcer_cve < 3 order by no_serie desc"
#define CONS_CERT_OBTEN_CADENA_DATOS_CERTIFICADO_POR_NUM_SERIE_Y_RFC  "select no_serie||'|'||edo_cer||'|'||vig_ini||'|'||vig_fin||'|'||tramite_cve||'|'||fec_reg_ies, nombre, tipcer_cve from certificado where no_serie = '%s' and rfc = '%s'"
#define CONS_CERT_OBTEN_CADENA_DATOS_FIEL_POR_RFC                     "select no_serie||'|'||edo_cer||'|'||vig_ini||'|'||vig_fin||'|'||tramite_cve||'|'||fec_reg_ies, nombre,tipcer_cve||'|' from certificado where rfc = '%s' and tipcer_cve < 3 order by no_serie desc"


// ******************************  SENTENCIAS SQL ESTADO TRAMITE  *********************************
#define EDO_TRAM_SP_REGISTRA_OPER_NOMBRE         "sp_regOperDet"
#define EDO_TRAM_SP_REGISTRA_OPER_PARAMS         "'%s',%d,%d"
#define EDO_TRAM_SP_SEGUIMIENTO_TRAMITE_NOMBRE   "sp_segTramite"
#define EDO_TRAM_SP_SEGUIMIENTO_TRAMITE_PARAMS   "'%s','%s'"


// ******************************  SENTENCIAS SQL GENERA CERT  *********************************
#define GEN_CERT_OBTEN_CLAVE_MODULO_POR_AGENTE                    "SELECT  MOD_CVE FROM CAT_MODULO WHERE MOD_CVE='%s'"
#define GEN_CERT_OBTEN_MODULO_AGENTE_POR_CLAVE                    "SELECT MOD_CVE  FROM AGENTE WHERE AGC_CVE = '%s'"
#define GEN_CERT_OBTEN_ULT_NUM_SERIE_DE_CERTIFICADO_AGC_POR_RFC   "SELECT FIRST 1 NO_SERIE FROM CERTIFICADO WHERE RFC= '%s' AND EDO_CER ='A' and TIPCER_CVE > '2' ORDER BY NO_SERIE DESC"
#define GEN_CERT_OBTEN_NUM_SERIE_AGENTE_POR_CLAVE                 "SELECT NO_SERIE FROM AGENTE WHERE AGC_CVE= '%s'"
#define GEN_CERT_OBTEN_DATOS_CERTIFICADO_POR_RFC_Y_TIPO           "SELECT curp, nombre, rfc_rl, curp_rl FROM certificado WHERE rfc='%s' AND tipcer_cve=%d AND edo_cer='A'"
#define GEN_CERT_OBTEN_RFCRL_CERTIFICADO_POR_RFC_Y_TIPO           "SELECT rfc_rl FROM certificado WHERE rfc='%s' AND tipcer_cve=%d AND edo_cer='A'"
#define GEN_CERT_ACTUALIZA_AGENTE_POR_CLAVE                       "UPDATE  AGENTE SET NOMBRE ='%s' , AP_PAT='%s' , AP_MAT='%s', MOD_CVE = '%s', NO_SERIE ='%s' ,BIT_TIPMOV ='C' ,BIT_USRMOV ='%s' , BIT_FECMOV = CURRENT ,BIT_REFMOV ='CERTISAT:%s' ,NIVEL_CVE='%s'  WHERE AGC_CVE='%s'"
#define GEN_CERT_OBTEN_ESTADO_RFC                                 "select edo_rfc from bloqueado where rfc = %s and edo_rfc='B'"
#define GEN_CERT_SP_AGREGA_AGENTE_NOMBRE                          "sp_insAgente"
#define GEN_CERT_SP_AGREGA_AGENTE_PARAMS                          "'%s','%s','%s','%s','%s','%s','%s','%s','CERTISAT:%s'"


// ******************************  SENTENCIAS SQL LOGIN  *********************************
#define LOGIN_OBTEN_NOMBRE_AGENTE_ADM_POR_RFC   "select nombre from segc_agente_admin where rfc = '%s'"


// ******************************  SENTENCIAS SQL OPERACION  *********************************
#define OPE_SP_GENERA_NUM_OPERACION_NOMBRE   "sp_GenNumOper"
#define OPE_SP_GENERA_NUM_OPERACION_PARAMS   "%d,'%s'"
#define OPE_SP_GENERA_NUM_TRAMITE_NOMBRE     "sp_GenNumTram"
#define OPE_SP_GENERA_NUM_TRAMITE_PARAMS     "'%d','%s','%s','%s','%s','%s','%s'"


// ******************************  SENTENCIAS SQL RECUPERA CERT  *********************************
#define RECP_CERT_SP_REC_DATOS_NOMBRE            "SP_recDatos"
#define RECP_CERT_SP_REC_DATOS_PARAMS            "%s,'%s'"
#define RECP_CERT_SP_REGISTRA_OPER_NOMBRE        "SP_REGOPERDET"
#define RECP_CERT_SP_REGISTRA_OPER_PARAMS        "'%s',%d,%d"
#define RECP_CERT_OBTEN_TIPO_ACUSE_POR_TRAMITE   "SELECT  TIPACU_CVE FROM ACUSE WHERE TRAMITE_CVE = '%s'"


// ******************************  SENTENCIAS SQL RENOVACION  *********************************
#define RENOV_AGREGA_REVOCACION   "insert into rev_det values('%s','%s','%c','%s','%d','%s')"


// ******************************  SENTENCIAS SQL REVOCACION  *********************************
#define REVOC_AGREGA_REVOCACION                                 "insert into rev_det values ('%s','%s','%s','%s','%s','%s')"
#define REVOC_OBTEN_RFCRL_CERTIFICADO_POR_NUM_SERIE             "select rfc_rl from certificado where no_serie = '%s'"
#define REVOC_OBTEN_ULT_NOMBRE_DE_CERTIFICADO_POR_RFC           "select first 1 nombre from certificado where rfc = '%s' order by no_serie desc"
#define REVOC_OBTEN_DATOS_CERTIFICADO_POR_NUM_SERIE             "select nombre,tipcer_cve from certificado where no_serie = '%s'"
#define REVOC_OBTEN_MODULO_AGENTE_POR_CLAVE                     "select mod_cve from agente where agc_cve = '%s'"
#define REVOC_OBTEN_NUM_SERIE_CERTIFICADO_POR_RFC_Y_NUM_SERIE   "select no_serie from certificado where rfc = '%s' and tipcer_cve = 1 and edo_cer = 'A' and no_serie in('%s', '%s')"
#define REVOC_OBTEN_TOTAL_FIEL_POR_RFC                          "select  count (no_serie)  from certificado where rfc='%s' and edo_cer='A' and tipcer_cve=2"
#define REVOC_OBTEN_DATOS_FIEL_POR_RFC                          "select  no_serie,tipcer_cve  from certificado where rfc='%s' and edo_cer='A' and tipcer_cve=2"
#define REVOC_OBTEN_FECH_VIG_FIN_CERTIFICADO_POR_NUM_SERIE      "select  vig_fin  from certificado where no_serie='%s'"
#define REVOC_SP_GENERA_NUM_OPER_NOMBRE                         "SP_GENNUMOPER"
#define REVOC_SP_GENERA_NUM_OPER_PARAMS                         "'%d','%s'"
#define REVOC_SP_GENERA_NUM_TRAM_NOMBRE                         "SP_GENNUMTRAM"
#define REVOC_SP_GENERA_NUM_TRAM_PARAMS                         "'%d','%s','%s','%s','CADENAR ORIGINAL','FIRMA','ARCHIVO'"


// ******************************  SENTENCIAS SQL SELLOS DIGS  *********************************
#define SELLOS_OBTEN_PRIMER_NOMBRE_CERTIFICADO_POR_RFC    "select first 1 nombre from certificado where rfc = '%s'"
#define SELLOS_OBTEN_FECHA_OPERACION_POR_SOLICITUD        "select fec_op from operacion where soloper_cve = '%s'"
#define SELLOS_ACTUALIZA_REQ_SELLO                        "UPDATE reqs_sellos SET no_serie='%s' WHERE soloper_cve='%s' AND oper_sec=%d"
#define SELLOS_SP_AGREGA_REQ_SELLO_NOMBRE                 "sp_insReqSellos"
#define SELLOS_SP_AGREGA_REQ_SELLO_PARAMS                 "'%s', '%s'"


// ******************************  SENTENCIAS SQL SOL CERT  *********************************
#define SOL_CERT_OBTEN_DATOS_CERTIFICADO_POR_RFC_Y_TIPO   "SELECT NO_SERIE , NOMBRE ,PUBK FROM CERTIFICADO WHERE RFC='%s' AND TIPCER_CVE=%d AND EDO_CER = 'A'"
#define SOL_CERT_OBTEN_MODULO_AGENTE_POR_CLAVE            "SELECT MOD_CVE FROM AGENTE WHERE AGC_CVE ='%s'"
#define SOL_CERT_OBTEN_NOMBRE_CERTIFICADO_POR_RFC         "SELECT  NOMBRE FROM CERTIFICADO WHERE RFC='%s' AND EDO_CER = 'A'"


// ******************************  SENTENCIAS SQL SOL DATOS CERT  *********************************
#define SOL_DATOS_CERT_OBTEN_DATOS_CERTIFICADO_POR_NUM_SERIE   "SELECT edo_cer, tipcer_cve, vig_fin FROM certificado WHERE no_serie = '%s'"


// ******************************  SENTENCIAS SQL SGI DARIO  *********************************
#define SGI_DARIO_SP_DATOS_ADIC_CRM_NOMBRE    "spDatosAdicio_crm"
#define SGI_DARIO_SP_DATOS_ADIC_CRM_PARAMS    "'%s'"


// ******************************  SENTENCIAS SQL UTIL BDAR  *********************************
#define UTIL_BDAR_OBTEN_DATOS_SITUACION_FISCAL_POR_CLAVE_1    "SELECT sitfis_desc, tipcerval, valdomic FROM cat_sit_fisCRM WHERE sitfis_cve = %i"
#define UTIL_BDAR_OBTEN_DATOS_SITUACION_FISCAL_POR_DOMICILIO  "SELECT sitdom_desc, tipCerVal FROM cat_sit_domicCRM WHERE sitdom_cve = %i"
#define UTIL_BDAR_OBTEN_TOTAL_CERTIFICADOS_POR_RFC            "SELECT count(*) FROM certificado WHERE rfc = '%s'"
#define UTIL_BDAR_OBTEN_TOTAL_CERTIFICADOS_POR_RFC_Y_TIPO     "SELECT count(*) FROM certificado WHERE rfc = '%s' and tipcer_cve = %d"
#define UTIL_BDAR_OBTEN_TOTAL_CERTIFICADOS_POR_RFC_Y_ESTADO   "SELECT count(*) FROM certificado WHERE rfc = '%s' and edo_cer = '%c'"
#define UTIL_BDAR_OBTEN_TOTAL_CERTIFICADOS_POR_RFC_TIPO_Y_EDO "SELECT count(*) FROM certificado WHERE rfc = '%s' and tipcer_cve = %d and edo_cer = '%c'"
#define UTIL_BDAR_OBTEN_DATOS_SITUACION_FISCAL_POR_CLAVE_2    "SELECT tipCerVal, valDomic FROM cat_sit_fisCRM WHERE sitfis_cve = %d"
#define UTIL_BDAR_OBTEN_TIPO_SITUACION_DOM_POR_CLAVE          "SELECT tipCerVal FROM cat_sit_domicCRM WHERE SitDom_cve = %d"
#define UTIL_BDAR_OBTEN_TOTAL_AGENTE_POR_CLAVE_Y_TIPO_CERT    "SELECT count(*) FROM AGENTE a, NIVEL_TIP_CER n WHERE a.agc_cve = '%s' AND a.nivel_cve = n.nivel_cve AND n.tipcer_cve = %d"
#define UTIL_BDAR_OBTEN_DATOS_AGENTE_Y_CERT_POR_CLAVE_AGC     "SELECT  a.estado, c.edo_cer FROM AGENTE a, CERTIFICADO c WHERE a.agc_cve = '%s' AND a.no_serie = c.no_serie"
#define UTIL_BDAR_OBTEN_DATOS_CERTIFICADO_POR_NUM_SERIE       "SELECT edo_cer, tipcer_cve FROM Certificado WHERE no_serie = '%s'"
#define UTIL_BDAR_OBTEN_VIG_FIN_CERTIFICADO_POR_NUM_SERIE     "SELECT vig_fin FROM CERTIFICADO WHERE no_serie = '%s'"
#define UTIL_BDAR_OBTEN_NIVEL_AGENTE_POR_CLAVE                "SELECT nivel_cve FROM Agente WHERE AgC_cve = '%s'"
#define UTIL_BDAR_OBTEN_RFC_RFC_ASOCIADO_POR_RFC_ORIG         "SELECT rfc FROM RFC_ASOCIADO WHERE rfc_orig ='%s' and rfc ='%s'"
#define UTIL_BDAR_OBTEN_CURP_CERTIFICADO_POR_RFC              "SELECT curp FROM certificado  WHERE rfc = '%s' and edo_cer='A' and tipcer_cve=1"
#define UTIL_BDAR_SP_GET_FEAacxRFC_NOMBRE                     "sp_getFEAacxRFC"
#define UTIL_BDAR_SP_GET_FEAacxRFC_PARAMS                     "'%s'"
#define UTIL_BDAR_SP_VAL_LLAVE_UNICA_NOMBRE                   "sp_valLlaveUnica"
#define UTIL_BDAR_SP_VAL_LLAVE_UNICA_PARAMS                   "'%s','%s','%s'"
#define UTIL_BDAR_SP_VAL_GEN_SELLOS_NOMBRE                    "sp_valida_gen_sd"
#define UTIL_BDAR_SP_VAL_GEN_SELLOS_PARAMS                    "%d,'SET{%s}','SET{%s}','SET{%s}'"
#define UTIL_BDAR_SP_REGISTRA_OPER_NOMBRE                     "sp_regOperDet"
#define UTIL_BDAR_SP_REGISTRA_OPER_PARAMS                     "'%s', %i, %i"
#define UTIL_BDAR_SP_REGISTRA_ERROR_NOMBRE                    "sp_regError"
#define UTIL_BDAR_SP_REGISTRA_ERROR_PARAMS                    "'%s', %i"
#define UTIL_BDAR_SP_REGISTRA_LLAVE_APARTADA_NOMBRE           "sp_regLlvApart"
#define UTIL_BDAR_SP_REGISTRA_LLAVE_APARTADA_PARAMS           "'%s','%s','%s','%s'"
#define UTIL_BDAR_SP_VAL_FECHA_CERTIFICADO_NOMBRE             "sp_valFecCert"
#define UTIL_BDAR_SP_VAL_FECHA_CERTIFICADO_PARAMS             "'%s'"
#define UTIL_BDAR_SP_VAL_BLOQUEO_NOMBRE                       "sp_verif_bloq"
#define UTIL_BDAR_SP_VAL_BLOQUEO_PARAMS                       "'%s'"
#define UTIL_BDAR_AGREGA_RFC_ASOCIADO                         "INSERT INTO RFC_ASOCIADO  VALUES ( '%s' , '%s')"
#define UTIL_BDAR_BORRA_LLAVE_APARTADA_POR_OPERACION          "DELETE FROM llave_apartada WHERE SolOper_cve = '%s' and oper_sec = %i"

#endif  // _CSENTENCIASSQL_H_