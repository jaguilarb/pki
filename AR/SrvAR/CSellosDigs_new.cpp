static const char* _CSELLOSDIGS_CPP_VERSION_ ATR_USED = "SrvAR @(#)"\
"DSIC09041AR_ 2007-12-13 CSellosDigs.cpp 1.1.1/3";

//#VERSION: 1.1.1
// Esta en la ruta de Informix: /usr/informix/incl/c++/  

#include <assert.h>
#include <unzip.h>
//#include <unistd.h>
#include <dirent.h>

#include <Sgi_BD.h>
//- #include <CConfigFile.h> //- GHM (070430)
#include <Sgi_ConfigFile.h>  //+ GHM (070430)
#include <CAcuses.h>
#include <CGeneraCert.h>
#include <CSolCert.h>

//- #include <ARunZip.h>  //- GHM (070430)
//- #include <ARinZip.h>  //- GHM (070430)
#include <Sgi_Zip.h>      //+ GHM (070430)
#include <CUtilBDAR.h>		// Funciones de Utileria de la AR
#include <CArchivoEnsob.h>
#include <CSellosDigs_new.h>

#include <Sgi_PKI.h>       //+ GHM (070321): Para utilizar la funci�n CreaDir

// POR FAVOR, NO USAR VARIABLES GLOBALES CON LAS CLASES DE INFORMIX: IT*
// Donde esta el delete de las clases se les hace new en PbaBaseSrvPKI.cpp (ejemplo: *ptrSrvArPKI)
// Si estan la funcion abreBD_AR(), falta la cierraBD_AR() ??

/* **************************************************************************************** */

LISTA_CERT *pListaNodo; // OjO: depurar esta variable global, Q tal si le pasa lo mismo q a las de Informix

// Inserta un nuevo nodo en la lista ligada
bool insertaNodo(char *cNameCer) // Reg
{
   LISTA_CERT		*pNew = NULL, *pAux = NULL;

   if ( (pNew = new LISTA_CERT()) != NULL) 
   {
      memcpy(&(pNew->cNomCer), cNameCer, strlen(cNameCer)); // cNameCer debe terminar en nulo 
      if (pListaNodo == NULL) // es el primero	
         pListaNodo = pNew;
      else 
      { // hay q buscar el ultimo y asignarlo 
         pAux = pListaNodo;
         while (pAux->pNext != NULL)
            pAux = pAux->pNext;
         // segun yo llego al ultimo y lo Inserta al final de la lista
         pAux->pNext = pNew;
      }
   }
   else 
   {
      Bitacora->escribe(BIT_ERROR, "(EM01) Error de memoria en new LISTA_CERT");
      liberaNodos();
      return (true);
   }
   return (false);
}

// libera todos los nodos de la actualizaci�n masiva
void liberaNodos() 
{
   LISTA_CERT		*pAux = NULL, *pSig = NULL;

   if (pListaNodo == NULL) 
      return; 
   pAux = pListaNodo; // apunta al 1ero. al menos existe 1 nodo
   do 
   {
      pSig = pAux->pNext;
      delete pAux;
      pAux = NULL; 
      if (pSig != NULL)
         pAux = pSig;
   } while (pSig != NULL);
   pListaNodo = NULL; // segun yo aqui TODOS pAux y pSig APUNTAN a NULL
}

/* *********************************************************************** */
/* *********************************************************************** */
CSellosDigs::CSellosDigs(CConfigFile& config, MensajesSAT& msgcli, CSSL_Skt* skt):
   CArchivoEnsob(config, msgcli, skt)
{
   // SIEMPE VA A SER DEL TIPO 2 DE SELLOS DIGITALES y Esta en la clase CGeneraCert
   m_tipo = TIPCER_SD;
   //GeneroCERT = false;		// Al menos trata de generar un certificado Digital
   memset(cArchRen, 0, sizeof(cArchRen));
   memset(cDirArchsReq, 0, sizeof(cDirArchsReq));
   memset(cDirArchsCer, 0, sizeof(cDirArchsCer));
   pdescComp = NULL;
}

/* *********************************************************************** */
CSellosDigs::~CSellosDigs()
{
   //char cComando[2*MAX_RUTA_ARCH];

   if (pdescComp)	// Objeto global para ver siempre la lista ligada de la descompresion
   {
      delete pdescComp;
      pdescComp = NULL;
   }
   // SE crearon los directorios .REQ y .CER => los borramos y too los archivos Generados o NO
   if (cDirArchsCer[0]) 
   {
      borraArchivos(cDirArchsCer);
      borraDir(cDirArchsCer);
   }
   if (cDirArchsReq[0]) 
   {
      borraArchivos(cDirArchsReq);
      borraDir(cDirArchsReq);
   }
   if (cArchRen[0] && m_numTram[0])
      // MAML 080902: ahora respalda el archivo ensobretado. // unlink(cArchRen);  
      AlmacenaEns( cArchRen, ".sdg" ); 
}

/* *********************************************************************** */

// FUNCION INICIAL
// llama a los 3 primeros mensajes iniciales para recibir el archivo ensobretado
// Si no llega bien el archivo Ensobretado => PELAS, regresa false
// Recibe el mensaje INICIOTRANSSDG y debiera hacer validaciones y dejar listo para Generar el certificado
bool CSellosDigs::preProceso()
{
   // Obtiene los datos de la Solicitud de Sellos, nombre/archivo	
   assert(m_MsgCli.tipOperacion() == INICIOTRANSSDG);

   if ( !error &&  msgInicial() && msgRecArch() ) 
      msgFinArch();

   // Iniciamos las varibles de una tramite : m_cadOriOp, m_refArchOp
   if ( !error && DescomprimeReqsSellos() ) 
      IniciaFin_preProceso();

   if (error)
      msgCliError(m_errTipo, m_errDesc, error, eAPL);
   return( !error );
   // despues de aqui se inicializa m_numOp y m_numTram
}

// FUNCION PRINCIPAL de la clase Derivada
bool CSellosDigs::Proceso()
{
   // OJO MELM : tal vez la funcion envTramite(NUMTRAMITE) deberia ser virtual por que el mensaje 
   //            q Sellos necesita mandar es TRANSCOMPLETA
      
   int oper_sec = m_utilBDAR->RegOperDetalleBDAR(TRANSCOMPLETA, m_numOp );  
   Bitacora->escribe(BIT_INFO, "La clase CSellosDigs de la AR registr� TRANSCOMPLETA en la tabla 'operacion'");

   // YA TENGO LA FECHA DE OPERACION d TRANSCOMPLETA => PUEDO GENERAR EL ACUSE
   //CAcuses m_acuse(m_BDAR, m_Configuracion, m_numOp, m_numTram );
   //if ( !(error = (oper_sec == -1)) && genAcuseSellos(&m_acuse) )
   if ( !(error = (oper_sec == -1)) )
   {	
      //m_MsgCli.setMensaje(TRANSCOMPLETA, m_acuse.cad_orig, strlen(m_acuse.cad_orig),
      //                                   m_acuse.firma,    strlen(m_acuse.firma) );  
      m_MsgCli.setMensaje(TRANSCOMPLETA, "Transf_OK_AR", 13, "firma", 5 );
      envia(CERTISAT); // manda el Mensaje al CERTISAT
   }

   Bitacora->escribe(BIT_INFO, "Antes de llamar a GeneraSellosDigs()");
   // Trata de Generar todos los certs. digitales, y Verifica si al menos uno se genero
   if ( !error )
      GeneraSellosDigs();

   ComprimeSellosDigs(); 
	
   if (error)
      msgCliError(m_errTipo, m_errDesc, error, eAPL); // Para q llamarla en tanto lugar
   return( !error );
}
/* *********************************************************************** */
// Funcion que envia el acuse exitoso de al menos genero 1 cert. de Sello 
bool CSellosDigs::acuseExito()
{
   string  m_cadenaTramite, m_cadena;

   int oper_sec = m_utilBDAR->RegOperDetalleBDAR(TRANSCOMPLETA, m_numOp );
   Bitacora->escribe(BIT_INFO, "Se registr� TRANSCOMPLETA en la tabla 'operacion'");

   CAcuses m_acuse(m_BDAR, m_Configuracion, m_numOp, m_numTram );
   if ( !(error = (oper_sec == -1)) && genAcuseSellos(&m_acuse) /* && 
        m_utilBDAR->f_obtenerCadenaTramite( m_numTram, "0", &m_cadenaTramite) && m_cadenaTramite != "" */ )
   {
      // Hay que obtener ListaSellos, con los datos de los Sellos Generados => verificar si el sp_ NO tenga problemas
      // para concatenar:  "m_acuse.firma|ListaSellos"  <<-- manda imprimir por el momento lo q regresa el StoreProcedure
      // m_firma = m_acuse.firma + string("|") + m_cadenaTramite; 
//      m_cadena = m_acuse.cad_orig + string("|S|0|2009-08-18 10:16:59|Certificado Digital generado. Cons�ltelo en la p�gina de Internet.|gomv730116qp6_0908180934s.req|2009-08-18 10:17:00|Certificado Digital generado. Cons�ltelo en la p�gina de Internet.|gomv730116qp6_0908180935s.req|2009-08-18 10:17:01|Certificado Digital generado. Cons�ltelo en la p�gina de Internet.|gomv730116qp6_0908180936s.req|2009-08-18 10:17:02|Certificado Digital generado. Cons�ltelo en la p�gina de Internet.|gomv730116qp6_0908180937s.req|2009-08-18 10:17:03|Certificado Digital generado. Cons�ltelo en la p�gina de Internet.|gomv730116qp6_0908180939s.req|GOMV730116QP6");
//      "30001000000100000360|2009-08-18 15:17:03|2011-08-18 15:17:03|30001000000100000359|2009-08-18 15:17:02|2011-08-18 15:17:02|30001000000100000358|2009-08-18 15:17:01|2011-08-18 15:17:01|30001000000100000357|2009-08-18 15:17:00|2011-08-18 15:17:00|30001000000100000356|2009-08-18 15:16:58|2011-08-18 15:16:58|";
      Bitacora->escribePV(BIT_DEBUG, "antes de setMensaje(TRANSCOMPLETA), m_cadena=%s", m_cadena.c_str() );
 
      m_MsgCli.setMensaje(TRANSCOMPLETA,// m_acuse.cad_orig, strlen(m_acuse.cad_orig),
                                           m_cadena.c_str(), m_cadena.length(),
                                           m_acuse.firma,    strlen(m_acuse.firma) );  
      envia(CERTISAT); 
   }
   return !error;
}
/* *********************************************************************** */
/* *********************************************************************** */
/* *********************************************************************** */
/* *********************************************************************** */
bool CSellosDigs::msgInicial()
{
   char   cNumPartes[4];
   string claves[] = { "PATH_TMPSDIG" },  spath_TmpSDig;	

   iNumPartes = sizeof(cNumPartes);
   m_ifirmaOp = MAX_TAM_FIRMA_B64; 
   m_irfcOp   = MAX_TAM_RFC;
   error = m_MsgCli.getMensaje(INICIOTRANSSDG, m_firmaOp,  &m_ifirmaOp,
                                               cNumPartes, &iNumPartes,
                                               m_rfcOp,    &m_irfcOp );
   if (error)	
      setVarsErr('I', error, "Error: Posible mensaje mal formado, al decodificar mensaje INICIOTRANSSDG");
   else
      error = m_Configuracion.getValoresConf("[AR_ALMACENA_SELLOS]", 1, claves, &spath_TmpSDig );

   if (!error)
   {
      iNumPartes = atoi(cNumPartes);
      SHA1_Init(&m_ctx);

      //- CreaDir ( (char *)spath_TmpSDig.c_str() ); //- GHM (070321)
      pkiCreaDir ( (char *)spath_TmpSDig.c_str() );   //+ GHM (070321): Para utilizar la funci�n definida en Sgi_PKI
      
      sprintf (cArchENS, "%s%s_XXXXXX", spath_TmpSDig.c_str(), m_rfcOp); // m_Configuracion.get("PATH_TMPSDIG")
      mkstemp(cArchENS);

      iFd = open(cArchENS, O_WRONLY | O_CREAT | O_TRUNC | O_APPEND, S_IRUSR | S_IWUSR);
      if (iFd == -1) 
      {
         error = sgiError(0,eERRNO,errno);
         return setVarsErr('I', error, "Error al crear archivo temporal de recepci�n de solicitud de sellos digitales");
      }

      Bitacora->escribePV(BIT_INFO, "Creacion del archivo temporal de recepci�n de solicitud de sellos: %s", cArchENS);
      error = m_MsgCli.setMensaje(OPE_EXITO);
      if (!error)
         error = envia(CERTISAT);

   }
   else
      setVarsErr('I', error, "Error: al obtener el valor de PATH_TMPSDIG en [AR_ALMACENA_SELLOS] en getValoresConf()");

   return !error;
}
/* *********************************************************************** */
bool CSellosDigs::msgRecArch()
{
   char  cNumParte[4];
   int   iNumParte = sizeof(cNumParte);
   
   for (int i = 1, j=0; i <= iNumPartes; i++)
   {
      uint8 iCoderror = 0;
      static char cBufArch[sizeof(m_MsgCli.buffer)];
      int iBufArch = sizeof(cBufArch);
      //int iTamBufer = sizeof(m_MsgCli.buffer);
      iNumParte = sizeof(cNumParte);

      //intE error = m_SktProc->Recibe(m_MsgCli.buffer, iTamBufer); //>- ERGL mi� sep 12 16:35:38 CDT 2007
      intE error = m_MsgCli.Recibe(m_SktProc);
      if (!error) 
      {
         m_ifirmaOp = MAX_TAM_FIRMA_B64;
         error = m_MsgCli.getMensaje(PARTTRANSSDG,  m_firmaOp,  &m_ifirmaOp,
                                                    cNumParte , &iNumParte,
                                                    cBufArch  , &iBufArch);
      }
      if (error || (i != atoi(cNumParte)))
      {
         uint8 eFte  = sgiErrorFuente(error);
         int16 eBase = sgiErrorBase  (error);

         if (!error || (eFte == eSGISSL && eBase == ERR_SKTTIMEOUT)) 
            iCoderror = 9;
         else 
            iCoderror = 1;
      }
      else
      {
         if (write(iFd, cBufArch, iBufArch) != iBufArch)
         {
            Bitacora->escribe(BIT_ERROR, sgiError(0, eERRNO, errno),
                      "Error al escribir en el archivo temporal de recepci�n de solicitud de sellos digitales");
            iCoderror = 9;
         }
      }

      sprintf(cNumParte, "%d", i);
      sprintf(m_firmaOp, "%d", iCoderror);
      error = m_MsgCli.setMensaje(EDOTRANSSDG, cNumParte, strlen(cNumParte),
                                               m_firmaOp, strlen(m_firmaOp)   );
      error = m_SktProc->Envia(m_MsgCli.buffer, m_MsgCli.tamMensaje()); 
         
      if ( iCoderror || error ) 
      { 
         if (iCoderror == 1 && j++ < 3) 
         { // Hasta 3 veces se puede mandar la parte i
            i--;
            continue;
         }
         cerrarYeliminarArch();
         error = (!error) ? iCoderror : error;
         setVarsErr('I', error, "Error en la funci�n msgRecArch()");
         return error;
      }
      j = 0; // Si no hay problema la parte i  se envio bien
      SHA1_Update(&m_ctx, cBufArch, iBufArch);
   }
   
   close(iFd);
   iFd = -1;
   return !error;
}

/* *********************************************************************** */
bool CSellosDigs::msgFinArch()
{
   /*>- ERGL mi� sep 12 16:39:41 CDT 2007
   int iTamBufer = sizeof(m_MsgCli.buffer);
   error = m_SktProc->Recibe(m_MsgCli.buffer, iTamBufer);
   */
   error = m_MsgCli.Recibe(m_SktProc); 
   if (error || m_MsgCli.tipOperacion() != FINTRANSSDG)
   {  
      cerrarYeliminarArch();
      return setVarsErr('I', error, "Error: no se recibi� el mensaje de fin de transmisi�n del archivo");
   }

   //char cDigFte[30]; 
   int	idig_archivo = sizeof(dig_archivo), // iDigFte
      	iref_arch=sizeof(ref_arch);

   m_ifirmaOp = MAX_TAM_FIRMA_B64;
   error = m_MsgCli.getMensaje(FINTRANSSDG, m_firmaOp,   &m_ifirmaOp,	
                                            dig_archivo, &idig_archivo, 
                                            ref_arch,	 &iref_arch );
   if (error)
   {  
      cerrarYeliminarArch();
      return setVarsErr('I', error, "Error la procesar mensaje FINTRANSSDG");
   }

   uchar uDigArch[SHA_DIGEST_LENGTH];
   SHA1_Final(uDigArch, &m_ctx);

   uint8	ui8DigFte[21];
   uint16	ui16DigFte = sizeof(ui8DigFte);
   B64_Decodifica(dig_archivo, idig_archivo, ui8DigFte, &ui16DigFte);

   if ( (error = memcmp(uDigArch, ui8DigFte, SHA_DIGEST_LENGTH)) != 0)
   {
      cerrarYeliminarArch();
      setVarsErr('I', error, "Error: la digesti�n recibida no coincide con la del archivo a procesar");
   }

   // 08/Jul/2005: Tenemos q implementar una validacion para verificar si el archivo .sdg esta correctamente ensobretado
   if ( !error && !valArchEns() ) //Se hace esta validacion p/NO mandar un No. d Operacion y por tanto un ACUSE en el CW
      cerrarYeliminarArch();

   /*
   // MELM Enero 30: Esto ya se hara en las 2 funciones genOperacion() y genTramite()
   // OjO: se guarda el Numero de Operacion PADRE q trae la operacion inicial y el tipo de Mensaje
   regBitacora(nSerie_, m_tipo, m_rfcOp, nombreReq_, alacCve_," "," ","0",""); // nombreReq_: no lleva es el proceso PADRE
   */
   return !error;	
}


// Busca los caracteres & y los escapa i.e les agrega el caracter de escape '\\' NO FUNCIONA, NO SE PORQUE
// suponemos cCadDes con el suficiente espacio
void CSellosDigs::sustituyeAmp(char *cCadSrc, char *cCadDes)
{
   memset(cCadDes, 0, sizeof(cCadDes)); // Solo limpia los 1eros 4 caracteres
   int i, iLen = strlen( cCadSrc );

   for (i = 0;i < iLen; i++) 
   {
      if (cCadSrc[i] == '&')  // Agrega los 2 caracteres
         strcat(cCadDes, "3"); //strcat(cCadDes, "\\&"); //strcat(cCadDes, "\x26");
      else // Agrega solo 1 caracter
         sprintf(cCadDes, "%s%c", cCadDes, cCadSrc[i]);
   }
   cCadDes[i+1]=0;
}


// Funcion para llamarse desde la funcion de Preproceso y descomprime los archivos .req
bool CSellosDigs::DescomprimeReqsSellos()
{  
   char      cArchZip[MAX_RUTA_ARCH], cRfcAux[30]; //, cInstruccion[2][2*MAX_RUTA_ARCH];
   string    claves[] = { "PATH_PROCESANDO", "PATH_PROCESADOS" }, spath_Procesando;

   memset(&stMiNodo, 0, sizeof(nomzip));	
   memset(&cDirArchsCer, 0, sizeof(cDirArchsCer));
// if ( m_Configuracion.open("arInicia.ini","AR_ALMACENA_SELLOS") ) { // Existe el arch. de configuraci�n
   if ( !( error = m_Configuracion.getValoresConf("[AR_ALMACENA_SELLOS]", 2, claves, &spath_Procesando, &spath_Procesados )) )
   {
      //- CreaDir ( (char *)spath_Procesando.c_str() );  //- GHM (070321)
      pkiCreaDir ( (char *)spath_Procesando.c_str() ); //+ GHM (070321): Para utilizar la funci�n definida en Sgi_PKI


     //- CreaDir ( (char *)spath_Procesados.c_str() ); //- GHM (070321)
      pkiCreaDir ( (char *)spath_Procesados.c_str() ); //+ GHM (070321): Para utilizar la funci�n definida en Sgi_PKI

      sprintf(cArchRen, "%s%s.sdg", spath_Procesando.c_str(), m_rfcOp ); // Las rutas traen al final el /  
      //sprintf(cArchRen, "%s%s_%s", spath_Procesando.c_str(),  m_rfcOp, m_numTram); En esta parte todavia m_numTram == '\0'
      sprintf(cArchZip, "%s%s.zip", spath_Procesando.c_str(), m_rfcOp );
      if ( !(error = rename(cArchENS, cArchRen)) ) 
      {  // renombro el archivo temporal al lugar q quiero
         // 1.- se renombra en FinARCH   
         // 2.- Amilkar le paso el archivo renombrado 	 RETORNA 1 vector de 1 s�lo elemento
         strcpy(cArchENS, cArchRen);	// Solo se movio de lugar, pero sigue siendo el archivo ensobretado
         if ( valArchEnsAutentico(cArchZip) ) 
         {
            // Se crean los directorios donde se dejan los archivos .req y los .cer
            sustituyeAmp(m_rfcOp, cRfcAux); // se hace esto por los RFCs con '&' Se cambia por el '3'
            sprintf(cDirArchsReq, "%s%s/", spath_Procesando.c_str(), cRfcAux); 
            sprintf(cDirArchsCer, "%s%s/", spath_Procesados.c_str(), cRfcAux); 
            //sprintf(cInstruccion[0],"mkdir -p %s", cDirArchsReq);
            //sprintf(cInstruccion[1],"mkdir -p %s", cDirArchsCer);
            Bitacora->escribePV(BIT_INFO, "cRfcAux=%s",      cRfcAux);
            Bitacora->escribePV(BIT_INFO, "cDirArchsReq=%s", cDirArchsReq);
            // CAMBIAR: nombre del directorio en lugar de guion bajo, guion normal ya q coincide con nombre del archivo

            //if (system(cInstruccion[0]) != -1 && system(cInstruccion[1]) != -1) // MAML errno == 17: Aun si el directorio existe, descomprime 
            if ( (!pkiCreaDir(cDirArchsReq) || errno == 17) && (!pkiCreaDir(cDirArchsCer) || errno == 17) )
            {
               //   "ABA900101ML2.pso" == Archivo de PRUEBAS
               pdescComp = new desCompresion(cArchZip, cDirArchsReq, &stMiNodo, error); // esta en ARunZip.h
               if ( pdescComp == NULL || error ) 
                  //OjO: Que pasa si aqui ocurre un error => manda el acuse
                  //     pero cual error va a marcar en el CW (OjO: Probar)
                  setVarsErr('I', error, "Error: en la funci�n de desCompresion()");
            }	
            else 
            {
               error = errno; // OjO MELM
               setVarsErr('I', error, "Error: al crear los directorios para la descompresi�n de archivos requerimiento.");
            }
            if (!error) 
               unlink(cArchZip); // borra EL ARCh. COMPRIMIDO d REQUERIMIENTOS Q SE PROCESO BIEN
            // else con error DEJA TODOS los archivos COMPROMIDOS donde hubo ERROR en PATH_PROCESANDO

         } // end de valArchEnsAutentico
      }
      else 
      {
         sprintf (m_errDesc, "Error: no se puede renombrar el archivo .sdg :%s,en DescomprimeReqsSellos()", cArchENS);	
         setVarsErr('I', error, m_errDesc);
         unlink(cArchENS);  // BORRA EL ARCHIVO QUE NO SE PROCESO, por que no se pudo mover
      }	
   }
   else
      setVarsErr('I', error, "Error: al obtener el valor de PATH_PROCESANDO en [AR_ALMACENA_SELLOS] en getValoresConf()");

   return !error;
}

/* *********************************************************************** */
int CSellosDigs::numero_reqs()
{
   int cont = 0;
   struct  nomzip  *stSigNodo;
  
   for( stSigNodo = &stMiNodo; stSigNodo != NULL; stSigNodo = stSigNodo->sig ) 
      cont++;
   return cont;
}
/* *********************************************************************** */
// Aqui envia al ARCliente.jar un mensaje del numero de REQuerimiento de Sellos procesado
bool CSellosDigs::enviaMsgSello(int cReq, int n_reqs )
{
   // tal vez el CSW mande el Msg "Procesando Requerimiento num_?" y al final mandamos TRANSCOMPLETA
   char num_parte[3], num_reqs[3]; // Ponemos limite procesamos hasta 99;

/* error = m_MsgCli.setMensaje(EDOTRANSSDG, num_parte, strlen(num_parte), num_reqs, strlen(num_reqs) );
   if ( !error )
      error = envia(CERTISAT); */
   char  cNumParte[4];
   int   iNumParte = sizeof(cNumParte);
   static char cBufArch[sizeof(m_MsgCli.buffer)];
   int iBufArch = sizeof(cBufArch);
   
   // Se cambia por un ciclo en q la AR le va diciendo al ARCliente.jar cada certtificado de Sello q va haciendo
   /*
   intE error = recibe(CERTISAT);
   if (!error &&  m_MsgCli.tipOperacion() == PARTTRANSSDG )
   {
      m_ifirmaOp = MAX_TAM_FIRMA_B64;
      error = m_MsgCli.getMensaje(PARTTRANSSDG,  m_firmaOp,  &m_ifirmaOp,
                                                 cNumParte , &iNumParte,
                                                 cBufArch  , &iBufArch);
   }
   if ( !error )
   {
      sprintf( num_parte, "%d", cReq );
      sprintf( num_reqs,  "%d", n_reqs );     // Variable auxiliar para enviar Msg al ARCliente
      error = m_MsgCli.setMensaje(EDOTRANSSDG, num_parte, strlen(num_parte), num_reqs, strlen(num_reqs) );
      if ( !error )
         error = envia(CERTISAT);       
   }
   */
   intE error ;

   sprintf( num_parte, "%d", cReq );
   sprintf( num_reqs,  "%d", n_reqs );     // Variable auxiliar para enviar Msg al ARCliente
   error = m_MsgCli.setMensaje(EDOTRANSSDG, num_parte, strlen(num_parte), num_reqs, strlen(num_reqs) );
   if ( !error )
      error = envia(CERTISAT);
   if ( !error )  
      error = recibe(CERTISAT);
   if (!error && m_MsgCli.tipOperacion() == PARTTRANSSDG )
   {
      m_ifirmaOp = MAX_TAM_FIRMA_B64;
      error = m_MsgCli.getMensaje(PARTTRANSSDG,  m_firmaOp,  &m_ifirmaOp,
                                                 cNumParte , &iNumParte,
                                                 cBufArch  , &iBufArch);
   }

   if ( error )
      setVarsErr('I', error, "Error en la funci�n enviaMsgSello()");
   return !error;
}
/* *********************************************************************** */
// OJO MELM : la class CGeneraCert necesita iniciado m_rfc
// --- empieza la generacion de requerimento�s
bool CSellosDigs::GeneraSellosDigs()
{
   char	  cArchivo[MAX_RUTA_ARCH], cInstruccion[2*MAX_RUTA_ARCH], cAuxil[50], *pPos;
   struct  nomzip  *stSigNodo;
   int     oper_secSellos, cReq = 0, n_reqs = numero_reqs(); // Contador d archs. reqs. q necesita la funcion AlmacenaReq()

   // regBitacoraDetalle(ENSPROCESANDO);  // DEBE traer el n�mero de operacion Original
   if (( error = ValidaSujeto() ) != 0)
      return !error; 

   pListaNodo = NULL;	
   for( stSigNodo = &stMiNodo; stSigNodo != NULL; stSigNodo = stMiNodo.sig ) 
   { // !error && aunque haya error q intente mandar otros .req
      memcpy( &stMiNodo, stSigNodo, sizeof(nomzip));	
      Bitacora->escribePV(BIT_INFO, "Nombre del arch. requerimiento=%s", stMiNodo.nom); // para ver la depuraci�n

      // OjO MELM : Se graban los intentos de generar cada Sello Digital en la DB
      if ( (error = (!(regFileSellos_BDAR(stMiNodo.nom, &oper_secSellos)) != 0 )) != 0 ) 
      {
         sprintf (m_errDesc, "Error: no puede registrarse el archivo de Sellos:%s, en la DB de la AR ", stMiNodo.nom);
         setVarsErr('I', error, m_errDesc);
      }
		
      // Obtiene el nombre del archivo .req en una variable q no se usa
      memset(cArchivo, 0, sizeof(cArchivo));
      sprintf(cArchivo, "%s%s", cDirArchsReq, stMiNodo.nom);
      if (  !error && !(error = leeReq(cArchivo)) && !(error = AlmacenaReq(cReq++, true)) &&
                      !(error = ValidaReq())      && !(error = ApartaLlave()) && !(error = Genera()) ) 
      {
         // separamos el nombre y abrimos un nuevo archivo cer para escribirlo
         memset(cAuxil, 0, sizeof(cAuxil));
         memcpy(cAuxil, stMiNodo.nom, strlen(stMiNodo.nom)-strlen(pPos = strstr(stMiNodo.nom,".req")));
         sprintf(cInstruccion,"%s%s.cer", cDirArchsCer, cAuxil); //ponerle la extensi�n
         if ( guardaCertSelloDig(cInstruccion) )	
         {
            // va metiendo el nombre de este certificado en una variable con memoria 
            strcat(cInstruccion, "|"); // le agregamos el pipe
            if ( !(error = insertaNodo(cInstruccion)) ) // para despues hacer la compresion
               error = updateSellos_BDAR(oper_secSellos);
         }   
      }

      if ( !error && enviaMsgSello(cReq, n_reqs) )
         m_utilBDAR->RegOperDetalleBDAR( CERTGEN, m_numOp);
      else 
      {   
         error = (error != DUPLICADOCERT && error != REQSELLOS_DATOSDIF_CERTENS)? CERTNOGEN: error;
         m_utilBDAR->RegOperDetalleBDAR( MSGERROR, m_numOp, error); // MELM 071105: el SP sp_SegTramite necesita est� regto. p/detalles de sellos. 
      }   
      memset( stMiNodo.nom, 0, 50 ); // limpiamos solo la estructura donde se llena el nombre, p/siguiente archivo .req
   } // End del for
   return !error;
}


//Comprimir todo cDirArchsCer 
bool CSellosDigs::ComprimeSellosDigs()
{
   char	cInstruccion[2*MAX_RUTA_ARCH];

   if ( pListaNodo ) 
   { // Al menos se genero 1 certificado
      sprintf(cInstruccion,"%s%s.zip", spath_Procesados.c_str(), m_numTram);  // m_rfcOp, m_Configurac.get("PATH_PROCESADOS")
      if ( !(error = comprime( getListaCer(), cInstruccion, false) ) ) 
      { 

         if(!MarcasSellos())
            setVarsErr('I', error, "Error al crear la marca del archivo zip.");
         else
         {
            Bitacora->escribe(BIT_INFO, "Ok, OPE_EXITO al menos se gener� un certif. de sello digital.");
            /* // Solo si al menos se genero un certf. de Sello => se manda generar el Acuse de Sellos
            if ( !acuseExito() )
               setVarsErr('I', error, "Error al generar el acuse de Sellos."); */
         }
      }
      else
         setVarsErr('I', error, "Error en la funci�n que comprime() los certificados de Sello Dig.");
      liberaNodos();
   }
   else 
   {  // No se GENERARON SELLOS 
      setVarsErr('N', error, m_errDesc ); // Creo q ya la mando CGeneraCert.cpp => se puede repetir
      //setVarsErr('N', error, "No se genero ningun certificado de Sello Digital");
      //>>> MAML (070531): No es necesario reasignar el error puesto q las 3 etiquetas ya estan en la tabla 'cat_error_oper'  
      //if (error == DUPLICADOCERT || error == REQSELLOS_DATOSDIF_CERTENS)
      //   error = SDGYAPROCESADO;	
      //<<<
      //m_utilBDAR->RegOperDetalleBDAR( MSGERROR, m_numOp, SDGYAPROCESADO); // MELM 16/Enero/07: YA lo hace msgCliError()
   }

   // MAML: borra todos los archivos .REQ y .CER Generados o NO, en el destructor 
   //sprintf(cInstruccion, "rm -f %s/*.*;rm -f %s/*.*", cDirArchsReq, cDirArchsCer); 
   //system( cInstruccion );
   return !error;
}

// De todos los nombres de certificados guardados en una struc genera una sola cadena concatenada
char *CSellosDigs::getListaCer()
{
   LISTA_CERT	*pAux = NULL;
   //char		cListaCer[TAM_CERT]; // VOY A USAR UN STACK GRANDE // NO ACEPTA vars. LOCALES 

   memset(m_cert, 0, TAM_CERT); // OjO MELM : HABER SI NO TIENES PROBLEMAS POR USAR VARS. DE
   for (pAux = pListaNodo; pAux != NULL ; pAux = pAux->pNext)	// LA CLASE PADRE
      strcat( (char *)m_cert, pAux->cNomCer);
   m_cert[ strlen((const char *)m_cert)-1] = 0;   // Para quitarle el ultimo PIPE
   return ( (char *)m_cert );
}

// Guardamos cada archivo certificado en el directorio cDirArchsCer
bool CSellosDigs::guardaCertSelloDig(char *cArchCer)
{
   FILE	*pFile = NULL;	//	para guardar el certificado

   if ((pFile = fopen (cArchCer,"wb")) != NULL && (int)(fwrite (m_cert, 1, m_lcert, pFile)) == m_lcert ) 
   { 
      fclose (pFile); // El certificado temporal ha sido creado y cerrado
      pFile = NULL;
   }
   else 
   {
      error  = ERR_SAFE_CERT_SELLO;
      sprintf (m_errDesc, "Error: No se puede crear el certificado temporal de Sello Digital:%s" , cArchCer);
      setVarsErr('I', error, m_errDesc);
   }
   return (!error );
}


// -----------------------------------------------------------------------------------------------------------
//	LA SIGUIENTE FUNCION ME AYUDA A GENERAR EL ACUSE DE SELLOS DIGS 
bool CSellosDigs::genAcuseSellos(CAcuses *pacuse)	
{
   char nameArch[MAX_RUTA_ARCH];

   if ( (error = pacuse->generaAcuse(TRANSCOMPLETA, SOLIC_CERT_SELLO_DIG, m_rfcOp, cNombreContrib, "", "", "",
		 "", MODULO_CERTISAT_WEB, m_tipo,	-1, quitaRutaArch(ref_arch, nameArch), dig_archivo) ) != 0 )
   {
      sprintf (m_errDesc, "Error: no pudo genersarse el Acuse d Sellos Digs en la DB, numTramite=%s", m_numTram );
      setVarsErr('I', error, m_errDesc); // error = ERR_INS_ACUSE_DB;
   }
   else 
      Bitacora->escribePV(BIT_INFO, "OK se gener� Acuse d Sellos en la AR, numTramite=%s, cad_orig=%s, firma=%s", 
                           m_numTram, pacuse->cad_orig, pacuse->firma );
   return (!error);
}
// -----------------------------------------------------------------------------------------------------------



/* *********************************************************************** */
// Registra nombre del file .req en la DB de la AR
int CSellosDigs::regFileSellos_BDAR(const char *NameFileREQ, int *oper_secSellos)
{	
   // Mandamos el store procedure de ROCIO para REQUERIMIENTOS de SELLOS DIGITALES
   return (m_BDAR->ejecutaSP("sp_insReqSellos", "'%s', '%s'", m_numOp, NameFileREQ) &&
           m_BDAR->getValores("i", oper_secSellos) );
   // Segun ROCIO regresa 0 si hay Error o > 0 es el numero consecutivo
}


//m_numSerRenSellos OjO  stMiNodo.nom
int CSellosDigs::updateSellos_BDAR(int oper_secSellos)
{	
   if (!m_BDAR->ejecutaOper("UPDATE reqs_sellos SET no_serie='%s' WHERE soloper_cve='%s' AND oper_sec=%d;",
                            m_numSerRenSellos, m_numOp, oper_secSellos) )
   {
      error =	ERR_UPDATE_REQS_SELLOS;
      sprintf (m_errDesc, "Error: No se actualiz� el n�mero de serie='%s' en la tabla reqs_sellos con nombre_req='%s'",
                          m_numSerRenSellos, stMiNodo.nom);
      setVarsErr('I', error, m_errDesc);
   }
   return (error);
}




/* **************************************************************************************** */
/* **************************************************************************************** */
/* **************************************************************************************** */
bool CSellosDigs::MarcasSellos()
{
   error = -1;
   char        archivo[300];
   char        completa[256];
   static char cRutaFTP[265];
   int         desc;
   string      sRuta;
   string      sNumFTP;

   memset(completa,0,sizeof(completa));
   memset(cRutaFTP ,0,sizeof(cRutaFTP));

   sprintf(archivo,"%s.zip", m_numTram);

   error = m_Configuracion.getValorVar("[AR_ALMACENA_SELLOS]","MARCAS"  ,&sRuta);
   if(error)
   {
      Bitacora->escribe(BIT_ERROR, "Error al obtener ruta inicial del archivo de configuraci�n");
      return false;
   }
   error = m_Configuracion.getValorVar("[AR_FTP]","NUMFTP"  ,&sNumFTP);
   if(error)
   {
      Bitacora->escribe(BIT_ERROR, "Error al obtener ruta inicial del archivo de configuraci�n");
      return false;
   }
      

   for(int i = 1 ; i< atoi(sNumFTP.c_str())+1 ;i++)
   {
        memset(cRutaFTP,0,sizeof(cRutaFTP));
        NombreRuta(i,(char*)sRuta.c_str(),cRutaFTP);
        mkdir(cRutaFTP , 0774);
        sprintf(completa,"%s%s",cRutaFTP,archivo);

        desc = open(completa,S_IRWXU);
        if(!desc)
        {
           Bitacora->escribePV(BIT_ERROR, "Error al crear la marca de Sellos %s error %d",m_numTram ,errno);
           return false;
        }
        close(desc);
   }
   return true;

}

bool CSellosDigs:: NombreRuta(int iNumFTP,char* cRutaIni,char* cRutaFin)
{

   strcpy(cRutaFin,cRutaIni);

   if(iNumFTP< 10)
   {
      strcat(cRutaFin,"FTPPUB0");
      sprintf(cRutaFin + strlen(cRutaFin),"%d/",iNumFTP);
   }
   else
   {
      strcat(cRutaFin,"FTPPUB");
      sprintf(cRutaFin + strlen(cRutaFin),"%d/",iNumFTP);
   }
   return 0;

}
//############################################################################################################
// 080827: Borra un directorio tomando solo la ruta. retorna 0 si OK else errno
int CSellosDigs::borraDir(const char *ruta)
{
   return ( rmdir(ruta) ); // <<-- errno   NOTA: solo borra los directorios vacios
}

//############################################################################################################
// 080827: Borra todos los archivos de un directorio dado
int CSellosDigs::borraArchivos(const char *ruta)
{
   DIR    *direc;
   struct dirent *lista_nom;
   char   borra[2*MAX_RUTA_ARCH];

   direc = opendir( ruta );
   if(direc == NULL)
   {
      Bitacora->escribePV(BIT_ERROR, "Error al abrir el Directorio=%s, errno=%d", ruta, errno);
      return -1;
   }
   while((lista_nom = readdir(direc))!= NULL )
   {
      if( !strcmp(lista_nom[0].d_name,".") )
        continue;
      if( !strcmp(lista_nom[0].d_name,".."))
        continue;
      sprintf(borra,"%s%s", ruta, lista_nom[0].d_name );
      unlink(borra);
   } // END del WHILE
   closedir(direc);
   direc = NULL;
   lista_nom = NULL;
   //Bitacora->escribePV(BIT_DEBUG, "OjO: valor final errno=%d en borraArchivos()", errno);
   return 0; // (errno != 0) ?; 
}

