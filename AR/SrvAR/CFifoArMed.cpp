static const char* _CFIFOARMED_CPP_VERSION_ ATR_USED = "SrvAR @(#)"\
"DSIC09041AR_ 2007-12-13 CFifoArMed.cpp 1.1.1/3";

//#VERSION: 1.1.1
#include <CFifoArMed.h>

CFifoArMed::CFifoArMed()
{}

CFifoArMed::~CFifoArMed()
{
   termina();
}

int CFifoArMed::inicia(MensajesSAT *mensajeMed)
{
   m_mensaje = mensajeMed;
   m_semaforo = NULL;
   m_semaforo = new CSemaforo(SERV_FIFO, 1);
   if(!m_semaforo)
      return ERR_SEM_NOT_CREATE;
   m_pid = getpid();
   fd_LecFifo = -1;
   fd_EscFifo = -1;
   memset(m_fifoLec, 0, sizeof(m_fifoLec));
   memset(m_fifoEsc, 0, sizeof(m_fifoEsc));
   Iniciado = true;
   return 0;
}

void CFifoArMed::termina(void)
{
   if(m_semaforo)
      delete m_semaforo;
   if(fd_EscFifo != -1)
   {
      close(fd_EscFifo);
      fd_EscFifo = -1;
   }
   if (fd_LecFifo != -1)
   {
      close(fd_LecFifo);
      fd_LecFifo = -1;
   }
   if (m_fifoLec[0])
      unlink(m_fifoLec);
   if (m_fifoEsc[0])
      unlink(m_fifoEsc);
   //>>> V.1.01.01 MEML (060711)
   Iniciado = false;
   memset(m_fifoLec, 0, sizeof(m_fifoLec));
   memset(m_fifoEsc, 0, sizeof(m_fifoEsc));   
   //<<< V.1.01.01 MEML (060711)
}

long CFifoArMed::m_leeLong(int fd, long &numLeer)
{
   ssize_t n , rc;
   char c, strLong[100];
   int i = 0;
   for ( ;;)
   {
      if ( (rc = read(fd,&c,1)) == 1 )
      {
         strLong[i++] = c;
         if (  c == ' ' )
            break;
      }
      else if (rc == 0 )
      {
         if ( n == 1 )
            return (0);
         break;
      }
      else
      {
         if ( errno == EINTR )
            continue;
         return (-1);
      }
   }
   strLong[i] = 0;
   numLeer = atol(strLong);
   return i;
}

int  CFifoArMed::m_leeBuffer(int fd, char *vptr, long cantBytes)
{
   ssize_t n , rc;
   char c, *ptr;
   ptr = vptr;
   for ( n = 1; n <= cantBytes ; n++)
   {
      if ( (rc = read(fd,&c,1)) == 1 )
         *ptr++ = c;
      else if (rc == 0 )
      {
         if ( n == 1 )
            return (0);
         break;
      }
      else
      {
         if ( errno == EINTR )
            continue;
         return (-1);
      }
   }
   return (n-1);
}

intE CFifoArMed::escribe(void)
{
   char buffer[TAM_DATOS];
   char buffPid[20];
   int lbuffer = sizeof(buffer);
   m_error = m_mensaje->setDatos(buffer,&lbuffer);
   if(m_error != 0)
      return m_error;
   if ( (mkfifo(SERV_FIFO, FILE_MODE) < 0 ) && ( errno != EEXIST ) )
      return sgiError(eAPL, 105);
   snprintf(m_fifoLec, sizeof(m_fifoLec), "/tmp/fifoLect.%ld", (long) m_pid);
   snprintf(buffPid, sizeof(buffPid), "%ld %ld ", (long) m_pid, (long) lbuffer);
   memcpy(m_bufferMen, buffPid, strlen(buffPid));
   memcpy(this->m_bufferMen + strlen(buffPid) , buffer, lbuffer);
   fd_EscFifo = open(SERV_FIFO, O_WRONLY);
   if (fd_EscFifo == -1)
      return sgiError(eAPL, ERR_NO_ABRE_FIFO);
   m_semaforo->bloquea();
   write(fd_EscFifo, m_bufferMen, strlen(buffPid)+lbuffer);
   m_semaforo->desBloquea();
   if ( (mkfifo(m_fifoLec, FILE_MODE) < 0 ) && ( errno != EEXIST ) )
      return sgiError(eAPL, ERR_NO_CREA_FIFO_LECT);
   return 0;
}

intE CFifoArMed::lee(void)
{
   ssize_t n;
   char buffer[TAM_DATOS];
   long pid;
   long cantBytes;
   
   fd_LecFifo = open(m_fifoLec, O_RDONLY);
   if ( m_leeLong(fd_LecFifo, pid ) < 0 )
      return sgiError(eAPL, ERR_NO_CREA_FIFO_LECT); 
   if ( pid != m_pid )
      return sgiError(eAPL, ERR_PID_NO_COINCIDEN);
   if ( m_leeLong(fd_LecFifo, cantBytes) < 0 )
      return sgiError(eAPL, ERR_NO_LEE_FIFO);
   if ( !(n = m_leeBuffer(fd_LecFifo, buffer, cantBytes )) )
      return sgiError(eAPL, ERR_NO_LEE_BUF_MED);
   if ( n != cantBytes )
      return sgiError(eAPL, ERR_NO_LEE_FIFO);
   if ( (m_error = m_mensaje->getDatos(buffer,cantBytes)) != 0)
      return sgiError(eAPL, ERR_GET_DATOS);
   if (fd_LecFifo != -1)
   {
      close(fd_LecFifo);
      fd_LecFifo = -1;
   }
   unlink(m_fifoLec);
   //>>> V.1.01.01 MEML (060711) : Para No borrarlo 2 veces en el destructor, => limpiamos el nombre del fifoLec
   memset(m_fifoLec, 0, sizeof(m_fifoLec));
   //<<< V.1.01.01 MEML (060711)
   return 0;
}

bool CFifoArMed::getIniciado()
{
   return Iniciado;
}
