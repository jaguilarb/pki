static const char* _CDATOSRFC_CPP_VERSION_ ATR_USED = "SrvAR @(#)"\
"DSIC09041AR_ 2009-01-29 CDatosRFC.cpp 1.1.0/2";

//#VERSION: 1.1.0
/*######################################################################################################################
  ###  PROYECTO:              PKI-SAT                                                                                ###
  ###  MODULO:                Clase que realiza las consultas a WS y  LDAP                                           ###
  ###                                                                                                                ###
  ###  DESARROLLADORES:       Silvia Euridice Rocha Reyes  RORS797E                                                  ###
  ###  FECHA DE INICIO:       11 noviembre  2008                                                                     ###
  ###                                                                                                                ###
  ######################################################################################################################
         1         2         3         4         5         6         7         8         9        10        11        12
1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678*/


#include<CDatosRFC.h>

 //######################################################################################################################
 // Constructor , Inicializa variables.
 //######################################################################################################################


CDatosRFC::
CDatosRFC(const std::string &sRFCc, CConfigFile& config, int iTipoCert):
valWS(NULL), 
ldap(NULL), dario(NULL), archConf(config), sRFC(sRFCc), iTipCert(iTipoCert)
{
}

CDatosRFC::
~CDatosRFC()
{  
   Bitacora->escribePV(BIT_DEBUG, "class CDatosRFC: Antes de dar delete a valWS %s", ( valWS ) ? "!= NULL": "== NULL");
   if(valWS)
   {
      delete valWS;
      valWS = NULL;
   } 
   Bitacora->escribePV(BIT_DEBUG, "class CDatosRFC: Antes de dar delete a ldap %s", ( ldap ) ? "!= NULL": "== NULL");
   if(ldap)
   {
      ldap->Desconexion();
      delete ldap;
      ldap = NULL;
   }
   //Bitacora->escribePV(BIT_DEBUG, "class CDatosRFC, Antes de dar delete a dario %s", ( dario ) ? "!= NULL": "== NULL");
   if(dario)
   {
      delete dario;
      dario = NULL;
   }
}
 //######################################################################################################################
 //                           Abre la conexion con el servidor el WS.
 // 
 // 
 // 
 // 
 //######################################################################################################################


int CDatosRFC::
ConectaWS()
{
   if ((iError = ObtieneConfiguracionWS()))
      return iError;

   //Bitacora->escribePV(BIT_DEBUG, "class CDatosRFC, Antes de llamar a la funcion valWS.Inicia(%s)", sRFC.c_str() );
   //valWS = new CSolDatosWS_IDC(sRFC, iTipCert, sUsuarioWS, sPasswordWS, sConHTTPSWS,sCertWS);
   valWS = new CSolDatosWS_IDC();
   
   if (valWS == NULL)
   {
      Bitacora->escribe(BIT_ERROR, "Error en CDatosRFC en ConectaWS: No se inicializ� el objeto CSolDatosWS_IDC");
      return ERR_INIC_WS;
    }
    
   else{
      valWS->Inicia(sRFC, iTipCert, sUsuarioWS, sPasswordWS, sConHTTPSWS,sCertWS);
      Bitacora->escribePV(BIT_INFO, "CDatosRFC en ConectaWS: Se inicializan correctamente las variables para la Web Service");
    }

   if ( !valWS->InicializaWS() )
   {
      Bitacora->escribe(BIT_ERROR, "Error en CDatosRFC en ConectaWS: No se inicializ� correctamente la Web Service.");
      return ERR_INIC_WS;
    }
   
   return EXITO;
}


 //######################################################################################################################
 //                           Abre la conexion con el servidor LDAP.
 //
 //
 //
 //
 //######################################################################################################################

int CDatosRFC::
ConectaLDAP()
{
    if (!(iError = ObtieneConfiguracionLDAP()))
    {
       //Bitacora->escribePV(BIT_DEBUG, "class CDatosRFC, Antes de dar memoria a ldap %s", ( ldap ) ? "!= NULL": "== NULL"); 
       ldap =  new Sgi_LDAP(*Bitacora, sRutaBaseLDAP);
       if(!ldap)
       {
         Bitacora->escribe(BIT_ERROR, "Error en CDatosRFC en ConectaLDAP: No se inicializ� el objeto ldap para hacer la conexi�n");
         return ERROR_DATOS_NULL; 
         }

       if (!(iError = ldap->Conexion( sIPLDAP ,atoi(sPuertoLDAP.c_str()), sUsuarioLDAP,sPasswordLDAP)))
         return EXITO;
       else
         return iError;
    }
    return iError;
}
/* MAML
void CDatosRFC::
DesconectaLDAP()
{
   if (ldap)
      ldap->Desconexion();
   /// Conque se desconecta WS ?? 

}*/

 //#####################################################################################################################
 //   Conexion con DARIO
 //#####################################################################################################################
int CDatosRFC::
ConectaDARIO()
{
   if (!(iError = ObtieneConfiguracionDARIO()))
   {
      //Bitacora->escribePV(BIT_DEBUG, "class CDatosRFC, Antes de dar memoria a dario %s", ( dario ) ? "!= NULL": "== NULL");
      dario = new Sgi_Dario();
      Bitacora->escribePV(BIT_INFO, "CDatosRFC ConectaDARIO: Se crea el Objeto dario");
      if (dario )
      {
         if ( dario->Conecta(sServDARIO.c_str(), sBDDARIO.c_str(), sUsuarioDARIO.c_str(), sPasswordDARIO.c_str() ,sRoleDARIO.c_str()) )          
            return iError = EXITO;
      }
      //else  poner error fatal. 
   }
   return -1; // Modificar

}


 //######################################################################################################################
 //                           Obtiene datos del Archivo de Configuracion.
 //
 //
 //
 //
 //######################################################################################################################


int CDatosRFC::
ObtieneConfiguracionLDAP()
{
   std::string sClaves [] = { "IP","PUERTO","USUARIO_PROXY","PASSWORD_PROXY","RUTA_BASE","RUTA_BASE_USU" };
   std::string sPassAux; 
   char cPwdDes[30];
   int  iPwdDes = sizeof(cPwdDes);

   archConf.cargaCfgVars();

   iError = archConf.getValoresConf("[CONEXION_LDAP]", 6, sClaves, &sIPLDAP, &sPuertoLDAP, &sUsuarioLDAP, &sPassAux,&sRutaBaseLDAP,&sRutaBaseUsuLDAP);
   if (!iError)
   {
      desencripta((uint8*)sPassAux.c_str(),sPassAux.length(),(uint8*)cPwdDes,&iPwdDes);
      cPwdDes[iPwdDes] = 0;
      sPasswordLDAP = cPwdDes;
      iError = EXITO;
   }
   return iError;
}

int CDatosRFC::
ObtieneConfiguracionWS()
{
   
   std::string sPassAux;

   iError = ERROR;

   int r1 = archConf.getValorVar("[WEB_SERVICE_IDC]" , "USUARIO"  , &sUsuarioWS);
   int r2 = archConf.getValorVar("[WEB_SERVICE_IDC]" , "PASSWORD" , &sPassAux);
   int r3 = archConf.getValorVar("[WEB_SERVICE_IDC]" , "TIPOMSGWS", &sTipoMensWS);
   int r4 = archConf.getValorVar("[WEB_SERVICE_IDC]" , "HTTPS",     &sConHTTPSWS);
   int r5 = ERROR;

   if ( r1 || r2 || r3 || r4 )
      return iError;

   //NOTA: El valor de URLWS debe leerse desde el programa principal
   if ( m_URLWS.length() == 0 )
      return iError;


   if ( sConHTTPSWS.compare("S") == 0)
   {
      r5 = archConf.getValorVar("[WEB_SERVICE_IDC]" , "CERT", &sCertWS);
      if ( r5 )
         return iError;
      if ( sCertWS.length() > TAM_PATH_CERT )
         return iError;
   }
   else
   {
      //Bitacora->escribePV(BIT_DEBUG, "WS_IDC: Los par�metros de configuraci�n le�dos son: URLWS(%s), USUARIO(%s), TIPOMSGWS(%s), "
      //                                       "conHTTPS(%s)",
      //                                m_URLWS.c_str(), strUsrWS.c_str(), strTpoMsgWS.c_str(), strConHTTPS.c_str() );
   }

   //Valida y habilita el usuario del WS en la clase
   if ( sUsuarioWS.length() > TAM_ARREGLO)
      return iError;

   if ( sPassAux.length() > TAM_ARREGLO)
      return iError;

   char cPwdDes[30];
   int  iPwdDes = sizeof(cPwdDes);

   desencripta((uint8*)sPassAux.c_str(),sPassAux.length(),(uint8*)cPwdDes,&iPwdDes);
   cPwdDes[iPwdDes] = 0;
   sPasswordWS  = cPwdDes;

   //m_tipoMsgWS = atoi(strTpoMsgWS.c_str());
   return EXITO;

}

 //######################################################################################################################
 //  Obtiene Datos del archivo de configuracion.
 //######################################################################################################################
 
 int CDatosRFC::
 ObtieneConfiguracionDARIO()
 {
   iError = ERROR;

   std::string sClaves [] = { "SERVIDOR","BD","USUARIO","PASSWORD","ROLE"};
   std::string sPassAux;
   char cPwdDes[30];
   int  iPwdDes = sizeof(cPwdDes);

   archConf.cargaCfgVars();

   iError = archConf.getValoresConf("[BASE_DATOS_DARIO]", 5, sClaves, &sServDARIO, &sBDDARIO, &sUsuarioDARIO, &sPassAux,&sRoleDARIO);

   if (iError)
   {
      desencripta((uint8*)sPassAux.c_str(),sPassAux.length(),(uint8*)cPwdDes,&iPwdDes);
      cPwdDes[iPwdDes] = 0;
      sPasswordDARIO = cPwdDes;
      iError = EXITO;
   }
   return iError;


 } 


 //######################################################################################################################
 //                           Obtiene datos.
 //
 //
 //
 //
 //######################################################################################################################

 int CDatosRFC::
 ObtieneDatosLDAP()
 {
    iError = ERROR;
    char *cAtributos[] = { "SATMarcaBiometricos",NULL};
    char *cTieneBiometricos[1];

    if( !sRFC.compare("") )
       return ERROR_DATOS_NULL; 

    if ( !ldap->consulta( (char*)sRFC.c_str(), (const char**)cAtributos, cTieneBiometricos ) ) 
       iError = ERROR_OBT_DATOS; 
    else
    {
       sTieneBiom = *cTieneBiometricos[0];
       Bitacora->escribePV(BIT_INFO,"CDatosRFC ObtieneDatosLDAP: Se obtienen los Biometricos [%s]", sTieneBiom.c_str() );
       iError = EXITO;
    }
    return iError; 

 }



int CDatosRFC::
ObtieneDatosWS()
{
   iError  = ERROR;
  
   if( !sRFC.compare("") )
       return ERROR_DATOS_NULL;

   if ( !valWS->m_inicializado  || ( ( iError = valWS->SolInfoRFCWS( sRFC.c_str())) != EXITO) )
   {
      Bitacora->escribePV(BIT_ERROR, "Error en CDatosRFC en obtieneDatosWS: m_inicializado != true || ValWS->obtieneDatosWS() != EXITO");
      return iError;
    }
   return EXITO;
}


int CDatosRFC::
ObtieneDatosDARIO()
{
   if( !sRFC.compare("") )
      return ERROR_DATOS_NULL;

   char cBiometricos[2];
   //if (dario->Consulta(sRFC.c_str(), cBiometricos ) )
   if (dario->Consulta(getRFCOriginal().c_str(), cBiometricos ) )
   {
      sTieneBiom = cBiometricos[0];
      return iError = EXITO;
   }
   return ERROR;
}

/*
std::string CDatosRFC::
getTieneBiom()
{
   return sTieneBiom;
}*/

std::string CDatosRFC::
getNombre()
{
   std::string sNombreCont;
   if ( !valWS->m_inicializado || valWS->getNombre(&sNombreCont) != EXITO)
    {  
      Bitacora->escribePV(BIT_ERROR, "Error en CValidaDatosRFC en obtieneDatosWS: m_inicializado != true || valWS->getNombre != EXITO");
      return " ";
     }

   return sNombreCont;
}

std::string CDatosRFC::
getDomicilio()
{
   std::string sDomicCont;  
   if ( !valWS->m_inicializado || valWS->getStrDomicilio(&sDomicCont) != EXITO )
      return " ";
   return sDomicCont;

}

std::string CDatosRFC::
getDescSitDom()
{
   std::string sDescSitDom = " ";
   if ( valWS->m_inicializado )
      sDescSitDom = valWS->getDescDom();
   return sDescSitDom;
}

int  CDatosRFC::
getSitDomicilio()
{
   return ( valWS->m_inicializado ) ? valWS->getSitDomicilio(sRFC.c_str()) : -1;
}



std::string CDatosRFC::
getSitFiscal()
{
   std::string sSitFiscal;
   char cSitFis[4];
   int iSit ; 

   if ( valWS->m_inicializado && valWS->getSitFiscal(sRFC.c_str(), iSit) )
   {
      sprintf(cSitFis,"%d",iSit);
      sSitFiscal = cSitFis;
   }
   else sSitFiscal = " ";

   return sSitFiscal;    
}

std::string CDatosRFC::
getTipContrib()
{
   std::string sTipCont;
   if (!valWS->m_inicializado || !valWS->getTipoContrib(&sTipCont))
      return " ";

   return sTipCont;

}

std::string CDatosRFC::
getRFCOriginal()
{
   
    char RFCOrig[14];

    if ( valWS->m_inicializado && valWS->getRFCOrig(RFCOrig))
       return (std::string)RFCOrig; 
    else
        Bitacora->escribe(BIT_ERROR, "Error en CDatosRFC en getRFCOriginal: (m_inicializado!=true)&&(valWS->getRFCOrig(RFCOrig)!=true)");
    

   return " ";
}

std::string CDatosRFC::
getCURP()
{
    char cCURP[20];
    cCURP[0] = 0;

    if ( valWS->m_inicializado && valWS->getCURP(cCURP))
       return (std::string)cCURP;

   return " ";

}

std::string CDatosRFC::
getDescSitFis()
{
    std::string sDescSitFisc;
    if ( valWS->m_inicializado && valWS->getDesSitFis(&sDescSitFisc))
    {
       if (sDescSitFisc.compare("") != 0  )
         return (std::string)sDescSitFisc;
    }

   return " ";

}

bool CDatosRFC::
getReg(int indice, std::string *Regimen, bool *Activo)
{
   return ( valWS->m_inicializado ) ? valWS->getRegimen(indice, Regimen, Activo) : false ;
}

bool CDatosRFC::
getObl(int indice, std::string *Obligacion, bool *Activo )
{
    return ( valWS->m_inicializado ) ? valWS->getObligacion(indice, Obligacion, Activo ): false;
}

bool CDatosRFC::
getRole(int indice, std::string *Rol, bool *Activo )
{
    return ( valWS->m_inicializado ) ? valWS->getRol(indice, Rol, Activo ) : false;
}

bool CDatosRFC::
getAct(int indice, std::string *Activ, bool *Activo )
{
    return ( valWS->m_inicializado ) ? valWS->getActiv(indice, Activ, Activo ) : false;
}

int CDatosRFC::getsNumRegs()
{
   return ( valWS->m_inicializado ) ? valWS->getNumRegs(): -1 ;
}

int CDatosRFC::getsNumObligs()
{
   return ( valWS->m_inicializado ) ? valWS->getNumObligs(): -1;
}

int CDatosRFC::getsNumRoles()
{
   return ( valWS->m_inicializado ) ? valWS->getNumRoles(): -1;
}

int CDatosRFC::getsNumActivs()
{
   return ( valWS->m_inicializado ) ? valWS->getNumActivs(): -1;
}

int CDatosRFC::getsStatusRFC()
{
   return ( valWS->m_inicializado ) ? valWS->getStatusRFC(): -1;
}

