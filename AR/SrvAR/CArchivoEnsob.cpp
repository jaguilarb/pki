static const char* _CARCHIVOENSOB_CPP_VERSION_ ATR_USED = "SrvAR @(#)"\
"DSIC09041AR_ 2007-12-13 CArchivoEnsob.cpp 1.1.1/3";

//#VERSION: 1.1.1
// Clase CArchivoEnsob: 'Abstracta' para manejar los archivos : .sdg y .ren

//+++ OjO : se tienen q incluir ASI, por que la class CSolCert DEPENDE de las q' estan arriba
#include <Sgi_BD.h>
//- #include <CConfigFile.h> //- GHM (070427)
#include <Sgi_ConfigFile.h>  //+ GHM (070427
#include <Sgi_PKI.h>

#include <CAcuses.h>
#include <CGeneraCert.h>
#include <CSolCert.h>
//+++

#include <CUtilBDAR.h>		// Funciones de Utileria de la AR
#include <CArchivoEnsob.h>
//- #include <ArchivosConfg.h> //- GHM (070321): Se modific� para utilizar la librer�a CConfigFile

/* **************************************************************************************** */
/* >> MELM : FUNCIONES Q VALIDAN ARCHIVOS ENSOBRETADOS (sirve p/Sellos y Renovacion) */

CArchivoEnsob::CArchivoEnsob(CConfigFile& config, MensajesSAT& msgcli, CSSL_Skt* skt): 
   CSolCert(config, msgcli, skt)  // , true // YA SE HACE EN SU class Papa : CGeneraCert
// MELM: el ultimo param d COperacion es "bool tramite" y Sellos o Renova siempre seran Tramites
{
   iFd = -1;
   cArchENS[0] = 0;	// Se inicia en las Clases Derivadas
   error = 0;

   if ( !setMemReqYCert() )
      error = m_errCod;
}

CArchivoEnsob::~CArchivoEnsob() // sera necesario el destructor para q pase por los otros ??
{
}

void CArchivoEnsob::cerrarYeliminarArch()
{
   if (iFd != -1) {
      close(iFd);
      iFd = -1;
   }
   if (cArchENS) {
      unlink(cArchENS);
      cArchENS[0] = 0;	
   }

   Bitacora->escribe(BIT_INFO, "CArchivoEnsob: Se elimina archivo temporal de la solicitud");
}

/* **************************************************************************************** */

//Funcion para validar un archivo ensobretado (.sdg � .ren), Q� puede ser independiente p estara en class Padre
//en Sellos: antes de mandar un Num. de operacion en la enviaIES()		en REN: para validarlo
bool CArchivoEnsob::valArchEns()
{
   SGIPKCS7             sPKCS7;
   STACK_OF(X509)       *pSkCerts;
   P7Datos              *pPKCS7Datos = NULL;
   int                  iTp = 0, iNumD = 0;
   
   pSkCerts = sk_X509_new_null();

   // cArchENS ya viene inicializado
   if (cArchENS && (error = sPKCS7.obtenDatos(cArchENS, &pPKCS7Datos, &iNumD, &iTp, &pSkCerts)) == 0 && pPKCS7Datos != NULL) 
      sk_X509_free(pSkCerts);
   else
   {  
      error = (error) ? error : ERR_VAL_ARCH_ENS;
      //sprintf (m_errDesc, "Error:  El archivo ensobretado es inv�lido. %s", cArchENS);
      sprintf (m_errDesc, "El archivo ensobretado es inv�lido");

      setVarsErr('N', error, m_errDesc);
   }

   return (!error);
}  

// Funcion hace la AC para q' cuando llamar a la funci�n ProcesaPKCS7(m_rfcOp)
// El de acuerdo con el RFC del contribuyente, recupere el ultimo certificado vigente y desensobrete el archivo.
// Valida q el RFC del archivo ensobretado sea el mismo q el RFC del usuario del CERTISAT-WEB
bool CArchivoEnsob::valArchEnsAutentico(char *cArchDes, string proceso)
{
   SGIPKCS7		sPKCS7;
   STACK_OF(X509)	*pSkCerts;
   P7Datos		*pPKCS7Datos = NULL;
   X509			*pCert = NULL;
   int			iTp = 0, iNumD = 0;
   char			cNumSerieCER_ArchEns[22]; // Antes era vars. de la clase

   pSkCerts = sk_X509_new_null();
   if ( !(error =  sPKCS7.obtenDatos(cArchENS, &pPKCS7Datos, &iNumD, &iTp, &pSkCerts)) && pPKCS7Datos && (pPKCS7Datos->si != NULL)) 
   {
      /* una vez que tengas todos los certificados ya puedes llamar a la funci�n que procesa el PKCS7 con el stack lleno.
         Ya que lo verifiques listo has terminado con la verificaci�n del PKCS7. */
      pCert = sk_X509_value(pSkCerts, 0); // Falta Obtener aqui el NOMBRE y el NUM_SERIE q trae el certificado
     
      strcpy(cNumSerieCER_ArchEns, (const char *)(pPKCS7Datos->si->issuer_and_serial->serial->data) );
      cNumSerieCER_ArchEns[20] = 0;
      // if () {	
      //<<<AGZ:Variables para la obtenci�n del nombre del certificado.
      X509_NAME *pNombre = NULL; //Para obtener el objeto que contiene los datos del nombre
      X509_NAME_ENTRY *pEntrada = NULL; //Para obtener la entrada  exacta de los datos en la estructura
      if ( !(pNombre = X509_get_subject_name(pCert) ) ) // Obtiene el objeto X509_NAME del Certificado
      {
         error = ERR_GET_NOMBRE_CONT;
         Bitacora->escribe(BIT_ERROR, "CArchivoEnsob.valArchEnsAutentico: No se pudo obtener el nombre del contribuyente en X509_get_subject_name()");

         setVarsErr('I', error, "No se pudo obtener el nombre del contribuyente en X509_get_subject_name()");
      }
      else 
      {
         // Procesa los elementos que me interesan: RFC y el Nombre del certificado 
         // MAML 080222: Cambia 'NID_commonName' por 'NID_name', est� si da el nombre completo
         int iarrElem[] = { NID_x500UniqueIdentifier, NID_name, -1 }, iPosicion, i;
		
         for (i = 0; iarrElem[i] != -1 && !error; i++) 
         {
            iPosicion = X509_NAME_get_index_by_NID(pNombre, iarrElem[i], -1); //Saca la posici�n exacta del elemento
            if ( (iPosicion != -1) && (iPosicion != -2)) 
            { // Si existe
               pEntrada = X509_NAME_get_entry(pNombre, iPosicion); //Obtiene la entrada
               memset(cNombreContrib, 0, sizeof(cNombreContrib)); 
               strcpy(cNombreContrib, (char*)pEntrada->value->data); // , pEntrada->value->length ); //Copia los datos

               Bitacora->escribePV(BIT_INFO, "CArchivoEnsob: cNombreContrib=%s, ->data=%s, ->length=%d, ->type=%d", cNombreContrib, (char*)pEntrada->value->data, pEntrada->value->length, pEntrada->value->type );
               // l = pPKCS7Datos->si->issuer_and_serial->serial->length; //  cout << l << endl;
               if ( !i ) // Solo checa q' los RFCs sean iguales SINO => adios
               {
                  // Guardamos todo lo que trae el RFC con Rep. Legal ?? MELM
                  //strcpy(cUniqueId_ArchEns, cNombreContrib); 
                  // Puede tener Representante Legal, se lo quitamos
                  char *pb = strchr(cNombreContrib, ' ');
                  if (pb)  
                     cNombreContrib[ strlen(cNombreContrib) - strlen(pb)] = '\0'; 
                  if ( memcmp( cNombreContrib, m_rfcOp, strlen(m_rfcOp)) ) 
                  { 
                     // Mensaje para saber que paso con el archivo ensobretado
                     error = SDGNOCORRESPONDE;

                     Bitacora->escribePV(BIT_ERROR, "CArchivoEnsob.valArchEnsAutentico: El RFC %s del contribuyente actual no corresponde con el RFC %s del archivo ensobretado", m_rfcOp, cNombreContrib);
                     sprintf (m_errDesc, "El RFC %s del contribuyente actual no corresponde con el RFC %s del archivo ensobretado", m_rfcOp, cNombreContrib);
                     setVarsErr('N', error, m_errDesc);
                     
                     Bitacora->escribePV(BIT_INFO, "CArchivoEnsob: RFC autenticado=%s, m_irfcOp=%d, Mensaje=%d", m_rfcOp, strlen(m_rfcOp), SDGNOCORRESPONDE);
                  }
                  else{ // Si son sus .ens (.sdg � .ren) del RFC autenticado =>
                     if(proceso=="RENOVA"){
                        valArchEnsCertVigente(cNumSerieCER_ArchEns, proceso);    
                     }else if(proceso=="SELLOS"){
                        valArchEnsCertVigente(cNumSerieCER_ArchEns, proceso); 
                     }
                  }
               } // end de !i
               else //>>> MAML 081021: Validamos el 'NID_name' en el archivo ensobretado, esta en UTF8 pasa a (char *)
               {
                  if ( pEntrada->value->type == V_ASN1_UTF8STRING )
                  { 
                     memset(cNombreContrib, 0, sizeof(cNombreContrib));                   
                     int res = ASN1_ascii_to_utf8( (char*)pEntrada->value->data, cNombreContrib, true );
                     if (res == -1 )
                     {
                        Bitacora->escribePV(BIT_ERROR, "CArchivoEnsob.valArchEnsAutentico: El nombre/razon social=%s, del archivo ensobretado no puede convertirse a valor ascii(%d)", (char*)pEntrada->value->data, res);
                        sprintf (m_errDesc, "El nombre/razon social=%s, del archivo ensobretado no puede convertirse a valor ascii(%d)", (char*)pEntrada->value->data, res);

                        return setVarsErr('I', ERR_CONV_UTF8, m_errDesc );
                     }
                  }
               } 
               //<<<
            }
            else // end de iPosicion
            {
               error = iPosicion;

               Bitacora->escribePV(BIT_ERROR, "CArchivoEnsob.valArchEnsAutentico: Error al obtener los datos del contribuyente posici�n del commonName, en archivo: %s", cArchENS);
               sprintf (m_errDesc, "Error al obtener los datos del contribuyente posici�n del commonName, en archivo %s", cArchENS);

               setVarsErr('I', error, m_errDesc);
            }	
         } // end del for	
         if ( !error ) // Aqui ya desensobreta el archivo, si no hubo Broncas
         {
            if ( !(error = sPKCS7.procesaP7(iTp, cArchENS, cArchDes, pSkCerts, NULL, NULL)) ) 
               terminaP7Datos(iNumD, pPKCS7Datos);
            else
            {
               Bitacora->escribePV(BIT_ERROR, "CArchivoEnsob.valArchEnsAutentico: Error al procesar el archivo ensobretado en la funcion sPKCS7.procesaP7(), en archivo %s", cArchENS);
               sprintf (m_errDesc, "Error al procesar el archivo ensobretado en la funcion sPKCS7.procesaP7(), en archivo %s", cArchENS);

               setVarsErr('I', error, m_errDesc);
            }
         } // end de !error
      }  // end del else	
      // } 
      sk_X509_free(pSkCerts);
   }
   else
   {
      error = (error) ? error : ERR_VAL_ARCH_ENS;
      //sprintf (m_errDesc, "Error:  El archivo ensobretado es inv�lido. %s", cArchENS);

      Bitacora->escribe(BIT_ERROR, "CArchivoEnsob.valArchEnsAutentico: El archivo ensobretado es inv�lido");
      sprintf (m_errDesc, "El archivo ensobretado es inv�lido.");

      setVarsErr('N', error, m_errDesc);
   }

   //   setVarsErr('I', error, "Error: en la funcion sPKCS7.obtenDatos()");
   return !error;
}

// Validamos q el certificado del contribuyentes este ACTIVO y q sea de tipo FEA. (se hace en la DB)
bool CArchivoEnsob::valArchEnsCertVigente(const char *numSerie, string modulo)
{
   char	edoCert;
   int	tipCert, vigente;

   if ( (error = (m_utilBDAR->getEdoyTipCert(numSerie, &edoCert, &tipCert) != 1)) != 0)
   {
      //>>> ERGL (070129)   
      //>- ERGL (070129)
      //setVarsErr('I', error, "Error: no se pueden obtener datos en la DB de la AR en getEdoyTipCert()");
      Bitacora->escribe(BIT_ERROR, "CArchivoEnsob.valArchEnsCertVigente: El certificado con el que se ensobreto el requerimiento no existe");
      setVarsErr('N', error, "No se pudo verificar la informaci�n del certificado, el certificado con el que se ensobreto el requerimiento no existe");
      //<<<
   }   
   else if (edoCert != 'A') // Ok, Verificar si el certificado esta ACTIVO
   {
      error = CERTNOACTIVO;
      Bitacora->escribePV(BIT_ERROR, "CArchivoEnsob.valArchEnsCertVigente: El certificado correspondiente al n�mero de serie %s, no est� ACTIVO", numSerie);
      sprintf (m_errDesc, "El certificado correspondiente al n�mero de serie %s, no est� ACTIVO", numSerie);
      setVarsErr('N', error, m_errDesc);
   }
   else if (tipCert != TIPCER_FEA ) // Ok, Verificar si el certificado es de tipo FEA
   {
      error = CERTNOFEA;
      Bitacora->escribePV(BIT_ERROR, "CArchivoEnsob.valArchEnsCertVigente: El certificado correspondiente al n�mero de serie %s, no es de tipo FIEL", numSerie);
      sprintf (m_errDesc, "El certificado correspondiente al n�mero de serie %s, no es de tipo FIEL", numSerie);
      setVarsErr('N', error, m_errDesc);
   }
   else if ( (error = (m_utilBDAR->verificaVigencia(numSerie, &vigente) != 1)) != 0 )
   {
      Bitacora->escribe(BIT_ERROR, "CArchivoEnsob.valArchEnsCertVigente: No se pudo ejecutar el sp_valFecCert() en la DB de la AR");
      setVarsErr('I', error, "No se pudo ejecutar el sp_valFecCert() en la DB de la AR");
   }
   else if ( vigente != 0) // S� el Store Procedure Regresa en vigente 0 si esta vigente > 0 si hay algun error
   {
      error = CERTNOVIGENTE;
      Bitacora->escribePV(BIT_ERROR, "CArchivoEnsob.valArchEnsCertVigente: El certificado correspondiente al n�mero de serie %s, no esta vigente(%d)", numSerie, vigente);
      sprintf (m_errDesc, "El certificado correspondiente al n�mero de serie %s, no esta vigente(%d)", numSerie, vigente);
      //sprintf (m_errDesc, "INFO: Modulo %s ", modulo);
      //sprintf (m_errDesc, "Error:  proceso %s, no esta vigente(%d).",
      //      modulo, vigente);
      setVarsErr('N', error, m_errDesc);
   }

   return !error;
}


// Abre el archivo de Requerimiento y obtiene el m_req para despues procesarlo
int CArchivoEnsob::leeReq(char *cArchREQ)
{
   struct stat		stArchReq;
   //CSemaforo		*pMiSemaforo = NULL;	// MELM 06/Feb/2006 CREO YA LOS USO
   FILE			*pFile = NULL;	// para manejo de los archivos requerimento

   Bitacora->escribePV(BIT_INFO, "CArchivoEnsob: Lectura del archivo de requerimiento %s", cArchREQ);

   //  Hay q leer del archivo renombrado, necesitamos abrirlo y memoria p/meterlo a un buffer
   error = stat(cArchREQ, &stArchReq);
   if ( !error && (pFile = fopen (cArchREQ,"rb")) != NULL) 
   {
      m_lreq =  stArchReq.st_size;
      if ( m_lreq > 0  ) 
      { // lo acabamos de cambiar de tama�o
         memset(m_req, 0, TAM_REQ); // limpiamos a nulos el buffer de la clase, para un new .req
         if ( fread (m_req, 1, m_lreq, pFile) <= 0 )	
         { // Inicializo el buffer del requerimiento
            error = errno;
            sprintf (m_errDesc, "Error de lectura del archivo de requerimiento %s, errno=%d", cArchREQ, errno);
            Bitacora->escribePV(BIT_ERROR, "CArchivoEnsob.leeReq: %s", m_errDesc);
            setVarsErr('I', error, m_errDesc);
         }
         /*else  {
            if ( (pMiSemaforo = new CSemaforo(SERV_FIFO, 2)) != NULL ) { // "/tmp/bloqueatbl"
               if ( !(error = pMiSemaforo->bloquea()) ) {
                  // OjO: Q se llame a la funcion de la clase Derivada
                  error = sol_sellCert(); // esta envia y recibe FIFO ??? 
                  pMiSemaforo->desBloquea();
               }  // Falta checar el resultado del Semaforo
               delete pMiSemaforo;
            }
            else
               error = bitacoraError("Error de memoria cSemaforo() en ", 0, "MandaReq()" );
         }*/
      }
      fclose (pFile);
      pFile = NULL;
   }
   else 
   {  
      sprintf (m_errDesc, "Error de apertura del archivo de requerimiento %s, error=%d, errno=%d", cArchREQ, error, errno);
      Bitacora->escribePV(BIT_ERROR, "CArchivoEnsob.leeReq: %s", m_errDesc);
      error = errno;
      setVarsErr('I', error, m_errDesc);
   }

   return (error);
}

/* **************************************************************************************** */
/* **************************************************************************************	*/
/* La Siguiente funcion es para hacer variable la obtencion de datos del archivo de Configuracion
   sTag (in), : De cual sTag o subtabla obtenemos los valores
   numClaves (in) : numero limite de valores a obtener del archivo de configuracion 
   claves1 (in), ..., clavesnumValores (in) : array con las claves de entrada
   dato1 (out), ..., datonumValores (out) : datos de salida							   * / 
bool CArchivoEnsob::getValoresConf(string sTag, int numClaves, string claves[], ...) 
{
   va_list lista;
   va_start(lista, claves);

   string	*dato = NULL;
   int		i;

   for(i = 0 ; !error && i < numClaves;i++ ) {
      dato = va_arg(lista, string*);
      error = ( dato == NULL || m_Configuracion.getValorVar(sTag.c_str(), claves[i].c_str(), dato) );
   }
   va_end(lista);

   if( error ) 
   {
      Bitacora->escribePV(BIT_ERROR, "Error en getValoresConf(%s,numClaves=%d,%s,...),  error=%d", 
                                     sTag.c_str(), numClaves, claves[i-1].c_str(), error);
      sprintf (m_errDesc, "Error: no se pudo obtener los valores del arch. de configuraci�n en getValoresConf() en %s",
                                     sTag.c_str());
      setVarsErr('I', error, m_errDesc);
   }
   return !error;
} */

/* ******************************************************************** */

// Inicia las vars. q va a escribir en la DB [en genTramite()], esta funcion debe estar COperacion
// En una funcion q recibiera como parametros los valores para hacer ese StoreProcedure
bool CArchivoEnsob::IniciaFin_preProceso() // char *cArchEns
{
   if ( (error = (m_MsgCli.getCadOrig(m_cadOriOp, &m_icadOriOp))) != 0 )
   {
      Bitacora->escribe(BIT_ERROR, "CArchivoEnsob.IniciaFin_preProceso: Error al obtener la cadena original del m_MsgCli");
      setVarsErr('I', error, "Error al obtener la cadena original del m_MsgCli");
   }
   else 
   {
      strcpy(m_rfc,m_rfcOp); // OjO segun yo m_rfcOp ya trae el RFC
      quitaRutaArch(cArchENS, m_refArchOp); //   strcpy(m_refArchOp, cArchENS); // Sin Ruta
      m_firmaOp[m_ifirmaOp] = 0;
   }
   
   return (!error);
}

// Al nombre del archivo le quita la ruta inicial y solo deja el nombre
char* CArchivoEnsob::quitaRutaArch(const char *ref_arch, char *nameArch)
{
   char *pa;

   strcpy(nameArch, ref_arch);
   pa = strrchr(ref_arch, '/' );
   Bitacora->escribePV(BIT_INFO, "CArchivoEnsob en quitaRutaArch: Se quita la ruta y s�lo se deja el nombre del archivo ",pa);
   if (pa)  
      strcpy(nameArch, (pa+1) );
   return(nameArch);
}

/* >>> - GHM (070321): Se elimin� porque esta funci�n esta disponible en la librer�a Sgi_PKI
// MELM 17/Enero/2007: Si no existe el directorio => lo crea else no hace nada
void CArchivoEnsob::CreaDir(char *directorio)
{
   ArchivosConfg cfg;

   cfg.CreaDir( directorio );  
}
<<<- GHM (070321) */

//######################################################################################################################
// Almacena los archivos ensobretados con extension ".sdg" o ".ren". NOTA: solo si el numero de tramite existe
int CArchivoEnsob::AlmacenaEns( const char *archENS, const char *ext)
{
   error = TRAMNOEXITO;
   string repositorio, rutaEns;

   if ((error = m_Configuracion.getValorVar("[REPOSITORIO]", "REQUERIMIENTOS", &repositorio)) == 0)
   {
      if ((error = pkiRutaEns(repositorio, m_numTram, rutaEns, ext)) == 0)
      {
         if (access(rutaEns.c_str(), F_OK) && (errno == ENOENT))
         {
            if ( !(error = rename(archENS, rutaEns.c_str())) )
               error = 0;
            else
            {
               sprintf (m_errDesc, "Error en CArchivoEnsob en AlmacenaEns: Error al mover el archivo ensobretado:%s hacia el archivo:%s,en AlmacenaEns()", archENS, rutaEns.c_str());
               setVarsErr('I', error, m_errDesc);
               unlink(archENS);  // BORRA EL ARCHIVO ENSOBRETADO QUE NO SE PROCESO ie. el q esta en PATH_TMPSDIG 
            }
         }
         else
            ArmaError( 'I', "Error en CArchivoEnsob en AlmacenaEns: El archivo existe en la ruta del repositorio de archivos ensobretados", errno, TRAMNOEXITO);
      }
      else
         ArmaError( 'I', "Error en CArchivoEnsob en AlmacenaEns: Error al armar la ruta del repositorio de archivos ensobretados", error, TRAMNOEXITO);
   }
   else
      ArmaError( 'I', "Error en CArchivoEnsob en AlmacenaEns: Error al leer los datos de configuraci�n del repositorio de requerimientos", error, TRAMNOEXITO);
 
   return error;
}

// ****************************************************************************************
// optiene la ruta de un archivo ensobretado 
int CArchivoEnsob::pkiRutaEns(const string& repositorio, const string& num_tramite, string& rutaEns, const char *ext )
{
   int error = pkiDirReq(repositorio, num_tramite, rutaEns, true, LINUX );
   if (!error)
      rutaEns += num_tramite + ext;

   return error;
}

