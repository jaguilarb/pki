static const char* _CRECUPERACERT_CPP_VERSION_ ATR_USED = "SrvAR @(#)"\
"DSIC10294AR_ 2010-08-02 CRecuperaCert.cpp 1.1.2/5";

//#VERSION: 1.1.2
// MAML 20100802: se implementa la funci�n msgCliError
// GHM  Abril 2010: se amplia el buffer de mensajes
// V.1.1.3  (20141014) JAB: Se agrupan las sentencias SQL utilizadas en el SrvAR en un archivo de cabecera

#include <CRecuperaCert.h>

/*##################################################################################
 * 
 *    PROPOSITO: Construir la clase.
 *    PREMISA  : ******
 *    EFECTO   : ******  
 *    ENTRADAS : ******
 *    SALIDAS  : ******  
 *
 * * #################################################################################*/

CRecuperaCert :: 
CRecuperaCert(CConfigFile& config, MensajesSAT& msgcli, CSSL_Skt* skt) :
   COperacion(config, msgcli, skt)
{
   esSello = false;
}

bool CRecuperaCert::
preProceso()
{
   bool ok = ObtieneMensaje(cTipRec,m_cDato);
   //-->>  MAML 20100730: siempre en todas las funciones de preProceso() debe terminar con esta funci�n cuando hay error
   if (!ok)
      msgCliError('I', "Error en CRecuperaCert en Preproceso: No se obtiene el mensaje" , (error)?error:ERR_EDO_AGC, eAPL);
   //--<<
   return ok;
}   

/*##################################################################################
 *
 *    PROPOSITO  : Obtener los datos del mensaje recibido.
 *    PREMISA    : Que el mensaje ya haya sido recibido.
 *    EFECTO     : Tener en variables los datos recibidos.
 *    ENTRADAS   : Variables sin datos.
 *
 *    SALIDAS    : Variables con los datos necesarios.  
 *                 cTipRec --> Tipo de Recuperacion:
 *                    0  N�mero de Tr�mite
 *                    1  RFC
 *                    2  N�mero de Serie
 *                 cDato   --> Depender� del Tipo de Operaci�n.
 *                     
 *#################################################################################*/

 bool CRecuperaCert :: 
 ObtieneMensaje(char * cTipRec,char * cDato )
 {
    char cFirma[TAM_FIRMA];
    int  iFirma  =  sizeof(cFirma);
    int  iTipRec =  2;
    int  iDato   =  100;
   
    
    if( (error = m_MsgCli.getMensaje(RECUPERACERT, cFirma, &iFirma, cTipRec, &iTipRec, cDato, &iDato ) ) )
    {
       //char  sDesc[] = "GetMensaje ";
       char sDesc[100]="Error en CRecuperaCert en ObtieneMensaje: No se obtiene el mensaje con GetMensaje ";
       Bitacora->escribe(BIT_ERROR , error ,sDesc); 
       return false; 
    } 
    return true;

 }

/*##################################################################################
 *
 *    PROPOSITO  : Obtener el certicado del repositorio.
 *    PREMISA    : Que se haya obtenido el n�mero de serie que se va a buscar.
 *    EFECTO     : Tener el certificado en una variable.
 *    ENTRADAS   : String con el n�mero de Serie del certificado a buscar.
 *    SALIDAS    : Buffer del certificado   Exitoso
 *                 NULL                     Si hubo alg�n error. 
 *
 *#################################################################################*/

int  CRecuperaCert ::
BuscaCert(char* cNumSerie)
{

   string sRuta;
   string sResp;


   //Char con el nombre de la clase CRecuperaCert
   char ErrorNombreCRecuperaCert[25]="Error en CRecuperaCert ";
   
   
   int idesc; 
   struct stat stDatArch;

   char  sDesc[100] ;


   if(!strcmp (cNumSerie, "") || cNumSerie == NULL)
   {
      //sprintf(sDesc , "No se le paso el numero de Serie a la funcion");
      sprintf(sDesc , "%s,%s", ErrorNombreCRecuperaCert,"En BuscaCert: No se le paso el numero de Serie a la funcion");
      Bitacora->escribe(BIT_ERROR, 1 ,sDesc);
      return -1;
   }
      

   error = m_Configuracion.getValorVar("[AR]","REPOSITORIO",&sResp);
   if(error)
   {
       //sprintf(sDesc , "Error en CRecuperaCert: Error en getValorVar %d", error);
       sprintf(sDesc , "%s,%s : %d",ErrorNombreCRecuperaCert, "En BuscaCert: Error en getValorVar", error);
       Bitacora->escribe(BIT_ERROR, error ,sDesc);
       return error; 
   }


   //pkiRutaCert(sResp, cNumSerie , sRuta,false);
   
   
   //Revisar la posibilidad de un error en obtener la ruta del certificado
   error=pkiRutaCert(sResp, cNumSerie , sRuta,false);
   if(error)
   {
       sprintf(sDesc , "%s,%s : %d",ErrorNombreCRecuperaCert, "En BuscaCert: Error al obtener la ruta del certificado", error);
       Bitacora->escribe(BIT_ERROR, error, sDesc);
       return error; 
   }
   


 
   idesc = open(sRuta.c_str() , O_RDONLY);
   if(idesc < 0)  
   {  
   
      //Se le suma el error con el nombre de la clase CRecuperaCert
      //sprintf(sDesc , "%s : %s", strerror(errno) ,sRuta.c_str());
      sprintf(sDesc , "%s,%s,%s : %s", ErrorNombreCRecuperaCert, "En BuscaCert: ", strerror(errno) ,sRuta.c_str());
      Bitacora->escribe(BIT_ERROR, errno ,sDesc);
      return errno;
   }
  

   error = fstat(idesc, &stDatArch);
   if(error < 0)
   {
      //sprintf(sDesc , "%s : %s", strerror(errno) ,sRuta.c_str());
      sprintf(sDesc , "%s,%s,%s : %s", ErrorNombreCRecuperaCert, "En BuscaCert: ", strerror(errno) ,sRuta.c_str());
      Bitacora->escribe(BIT_ERROR,errno,sDesc);
      return errno;
   }


   error =  read(idesc , stCert.cBufCert, stDatArch.st_size);
   if(error < 0)
   {
      //sprintf(sDesc , "%s : %s", strerror(errno) ,sRuta.c_str());
      sprintf(sDesc , "%s,%s,%s : %s",ErrorNombreCRecuperaCert, "En BuscaCert: ", strerror(errno) ,sRuta.c_str());
      Bitacora->escribe(BIT_ERROR, errno ,sDesc);
      return errno;
   }
   close(idesc);
   Bitacora->escribe(BIT_INFO,"CRecuperaCert.BuscaCert: Certificado encontrado" );
   stCert.iBufCert = stDatArch.st_size;
   return 0;

}

/*##################################################################################
 *    PROPOSITO  : Obtener los Datos de la BD.
 *    PREMISA    : Que se haya obtenido el tipo de recuperacion que se quiere por :
 *                 - N�mero de Tr�mite. (0)
 *                 - RFC.               (1)
 *                 - N�mero de serie.   (2)
 *    EFECTO     : Tener el Datos certificado en las variable.
 *    ENTRADAS   : ???.(En verificaci�n)
 *    SALIDAS    : ???.(En verificaci�n)
 *
 *#################################################################################*/

int  CRecuperaCert ::
Recuperacion( char *cTipRec , char *cDato )
{
   /*>- ERGL (070109) el tama�o del buffer es insuficiente y sobreescribe Resul.
   char cCad[71];
   -<*/
   //>>> ERGL (070109)
   char cCad[72];
   memset(cCad, 0, 72);
   int Resul = 0;
   //<<<
   /*>- ERGL (070109)
   char cResul[2];
   -<*/
   int iResp = 0;

   Bitacora->escribePV(BIT_INFO,"CRecuperaCert.Recuperacion: Recuperacion: Se Recibio solicitud por : ( %s , %s )", cTipRec, cDato);

   if(atoi(cTipRec) == 0 )
   {
      iResp = esSellos(cDato);
      if(iResp == ERROR_BD_CONSULTA || iResp == ERROR_BD_GETVALORES )
         return iResp;
      
      if(iResp == ES_SELLO )
      {
          esSello = true;
          //- GHM (080422) strcpy(stCert.cBufCert, "." );
          //- GHM (080422) stCert.iBufCert = 1;
      }    
   }   

   #if ORACLE
     //No hay procedimiento SP_recDatos
   
   #else
		   if( m_BDAR->ejecutaSP(RECP_CERT_SP_REC_DATOS_NOMBRE, RECP_CERT_SP_REC_DATOS_PARAMS, cTipRec, cDato) )
		   {
		 
			 if(m_BDAR->getValores ("ib", &Resul ,cCad))
			 {
			   if ( Resul == 0 )
			   {
				  SeparaCad(cCad,stCert.cNumSerie,stCert.cTipCert, stCert.cEdoCert,
							  stCert.cRFC     ,stCert.cNumOper, stCert.cFecIni );
				  //>>> ERGL (070116)
				  if ( !(stCert.cEdoCert[0] == 'A') ) 
				  {
					 if( !m_utilBDAR->getFecVigFin((char *)stCert.cNumSerie, (char *)stCert.cFecIni) )
					 {
						sError = "En Recuperacion: No se pudo recuperar la fecha de revocacion o caducidad.";
						return FEC_VIG_FIN;
					 }
				  }
				  Bitacora->escribePV(BIT_INFO,"( %s,%s,%s,%s,%s,%s,%s)","CRecuperaCert.Recuperacion: Se recuperaron los datos: ",               
				  stCert.cNumSerie,stCert.cTipCert,stCert.cEdoCert,stCert.cRFC,stCert.cNumOper,stCert.cFecIni);
				  return 0;
			   }
			   else
			   {
				  switch(atoi(cTipRec))
				  {
					 case  0:  sError = "En Recuperacion: No existe n�mero de operaci�n.";                       return 17; break;
					 case  1:  sError = "En Recuperacion: Este RFC no cuenta con un certificado.";             return 15; break;
					 case  2:  sError = "En Recuperacion: Este n�mero de serie no cuenta con un certificado."; return 18; break;
				  
				  }
				 
				  Bitacora->escribePV(BIT_ERROR,"Error en CRecuperaCert en Recuperacion:  %d",Resul);
			   }
			 }
			 sError = "En Recuperacion: No se obtuvieron los datos en getValores";
			 return 42;
		   }
	#endif
	
   sError = "En Recuperacion: Error interno favor de volver a intentarlo m�s tarde.";
   return 42;


}


/*##################################################################################
 *   PROPOSITO  : Responder a la solicitud.
 *    PREMISA    :
 *    EFECTO     :
 *    ENTRADAS   : ???.(En verificaci�n)
 *    SALIDAS    : ???.(En verificaci�n)
 *
 *#################################################################################*/

int CRecuperaCert::
esSellos(char *cNumtram)
{
   string sResulS;

   if( !m_BDAR->consultaReg(RECP_CERT_OBTEN_TIPO_ACUSE_POR_TRAMITE, cNumtram) )
   {
      sError = "Error interno favor de volver a intentarlo m�s tarde.";
      return ERROR_BD_CONSULTA; 
   }
   if(!m_BDAR->getValores ("s", &sResulS ))
   {
      /*>- ERGL (070108)
      sError = "Error interno favor de volver a intentarlo m�s tarde.";
      -<*/
      //>+ ERGL (070108) Verificar si el cambio del mensaje no afecta a otras funciones.
      sError = "No existe el numero de operacion.";
      return ERROR_BD_GETVALORES;
   }

   if(atoi(sResulS.c_str()) == SOLIC_CERT_SELLO_DIG )
      return ES_SELLO;
   else
      return NO_ES_SELLO;
}

/*##################################################################################
 *   PROPOSITO  : Responder a la solicitud.
 *    PREMISA    :
 *    EFECTO     : 
 *    ENTRADAS   : ???.(En verificaci�n)
 *    SALIDAS    : ???.(En verificaci�n)
 *
 *#################################################################################*/

int CRecuperaCert ::
Respuesta()
{

    int   iEdoCert =  strlen(stCert.cEdoCert);
    int   iTipCert =  strlen(stCert.cTipCert);
    int   iNumOper =  strlen(stCert.cNumOper);
    int   iNumSerie=  strlen(stCert.cNumSerie);
    int   iFecIni  =  strlen(stCert.cFecIni);
    int   iRFC     =  strlen(stCert.cRFC);
    
    error =  SIN_ERROR;

    error = m_MsgCli.setMensaje(RECUPERACION, stCert.cEdoCert,  iEdoCert,  stCert.cTipCert,  iTipCert , 
                                                 stCert.cNumOper,  iNumOper,  stCert.cFecIni ,  iFecIni  ,
                                                 stCert.cRFC, iRFC ,stCert.cNumSerie, iNumSerie, 
                                                 stCert.cBufCert , stCert.iBufCert);
    return error;

}


/*##################################################################################
 *    PROPOSITO  : Separa una cadena donde cada dato esta separado por un |
                   en  5 variables.
 *    PREMISA    : Obtener la cadena a separar del Store Procedure de Recuperacion.
 *    EFECTO     : Tener separados en variables diferentes los datos contenidos en la cadena
 *    ENTRADAS   : La cadena a separar cCad y las 5 variables donde quedaran los datos.
 *    SALIDAS    : 5 variables con los diferentes datos contenidos.
 *
 *#################################################################################*/


int CRecuperaCert ::
SeparaCad(char *cCad ,...)
{
   va_list lista;
   va_start(lista, cCad);

   char cCadAux[72];
   char *cRes;
   char *dato = NULL;

   int   iTamR = 0 ;

   strcpy (cCadAux ,cCad );

   for(int i = 0 ; i < 6 ;i++ )
   {
      dato = va_arg(lista,char*);
      if(dato == NULL)
         return 10;
      cRes = strtok(cCadAux + iTamR + i,"|");
      strcpy(dato,cRes);
      iTamR = iTamR + strlen(cRes);

   }
   va_end(lista);
   return 0;
}


/*##################################################################################
 *    PROPOSITO  : .
 *    PREMISA    : 
 *    EFECTO     : .
 *    ENTRADAS   : ??.(En verificaci�n)
 *    SALIDAS    : ??.(En verificaci�n)
 *
 *#################################################################################*/

int CRecuperaCert ::
RegistraOpe(char *cOper , int iOper , int iErr)
{

   if( m_BDAR->ejecutaSP(RECP_CERT_SP_REGISTRA_OPER_NOMBRE, RECP_CERT_SP_REGISTRA_OPER_PARAMS, cOper, iOper, iErr))
   {
      Bitacora->escribePV(BIT_INFO, "CRecuperaCert.RegistraOpe: Operacion registrada (%s ,%d ,%d)",cOper ,iOper,iErr );   
      return 0;
   }
   else
      return -1;

}


/*##################################################################################
 *    PROPOSITO  : Funci�n que :
 *                  Arma el mensaje de  error 
 *                  Escribe en la bitacora.
 *                  Manda el mensaje al CertiSAT.
 *    PREMISA    :
 *    EFECTO     : .
 *    ENTRADAS   : cOper   N�mero de Operacion.
 *                 cTipErr Tipo de Error (O --> Operacion P --> Proceso )
 *                 cError  N�mero de Error.
 *                 cDesc   Descripcion del error.
 *    
 *    SALIDAS    : ??.(En verificaci�n)
 *#################################################################################*/

int CRecuperaCert ::
Error(char * cOper, char *cTipErr ,int iError , char* cDesc)  
{
   char cError[3]; 
   sprintf(cError, "%d",iError);
   
   // creaError(int destino, char *psz_numTram, char *psz_tipError, char *psz_cdgErr, char *psz_desc)
      
   m_MsgCli.setMensaje(MSGERROR,  cTipErr, strlen(cTipErr) , cError ,strlen(cError)  , cDesc  , strlen(cDesc) );   
   
   
   //Se le contatena el nombre de la clase CRecuperaCert
   sprintf(cDesc,"%s,%s","Error en CRecuperaCert ",cDesc);
   Bitacora->escribe(BIT_ERROR, cDesc);
   
   RegistraOpe(cOper, MSGERROR , iError);
   return Envia();
   
}


/*##################################################################################
 *    PROPOSITO  : Envia el mensaje de respuesta al certisat.
 *    PREMISA    :
 *    EFECTO     : 
 *    ENTRADAS   : Sin Parametros.
 *    SALIDAS    : ?? (En verificaci�n)
 * 
 *#################################################################################*/

int CRecuperaCert ::
Envia()
{
   //GHM(100413): Se incremento el tama�o del buffer xq al crecer el tama�o del certificado,
   //             al agregarse la firma y las otras partes del mensaje el buffer se desbordaba 
   //- char cBuffer[2048];
   char cBuffer[2048*2];
   int  iBuffer =  sizeof(cBuffer);
   
   error =  m_MsgCli.setDatos(cBuffer, &iBuffer );
   if(error)
   {
      Bitacora->escribe(BIT_ERROR,"Error en CRecuperaCert en Envia: Error en setDatos");
      return error; 
   }
      
   if ( ! (error = m_SktProc->Envia((uint8 *)cBuffer,iBuffer)) )
   {
       Bitacora->escribePV(BIT_INFO, "CRecuperaCert.Envia: Operacion Enviada (%d)" , m_MsgCli.tipOperacion());
       return error;
   }
   else   
   return error;
   

}


/*##################################################################################
 *    PROPOSITO  : Funcionalidad principal de la clase.
 *    PREMISA    : Que se haya obtenido el tipo de recuperacion que se quiere por :
 *    EFECTO     : Tener el Datos certificado en las variable.
 *    ENTRADAS   : ???.(En verificaci�n)
 *    SALIDAS    : ???.(En verificaci�n)
 *
 *#################################################################################*/


bool CRecuperaCert ::
Proceso()
{

   error =  Recuperacion(cTipRec ,m_cDato);
   if(error)
      return Error(m_numOp, "N",error, (char*)sError.c_str());   

   //if(!esSello) //- GHM (080422): Se debe permitir el regreso de datos del �ltimo certificado de sello 
   //{ //- GHM (080422)
      error =  BuscaCert(stCert.cNumSerie);
      if(error)
      //>+ GHM (080416): Se env�a un mensaje espec�fico para el caso de no encontrar el certificado en el repositorio
      {
         if (error==2)
            return Error(m_numOp, "I",error , "En Proceso: El Certificado no esta disponible, solicite apoyo a su �rea central.");
         else
            return Error(m_numOp, "I",error , strerror(error));
      }
      //<+ GHM (080416)
      //>- GHM (080416) return Error(m_numOp, "I",error , strerror(error));
   //}//- GHM (080422)
   
   error = Respuesta();
   if(error)
      return Error(m_numOp, "I",error , "Error en la Respuesta");
         
   error =  RegistraOpe(m_numOp, RECUPERACION , 0 ); //"Aqui va el mensaje que se  regreso al certisat");
   if(error)
      return Error(m_numOp, "I",error , "Error en el RegistraOpe");

   error =  Envia();
   if(error)
     return Error(m_numOp, "I",error , "Error en Envia");

   return true;
}

