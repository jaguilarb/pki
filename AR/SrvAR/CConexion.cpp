static const char* _CCONEXION_CPP_VERSION_ ATR_USED = "@(#) SrvAR ( L : DSIC10392AR_ : CConexion.cpp : 1.1.3 : 5 : 28/09/10)";

//#VERSION: 1.1.1
/*######################################################################################################################
  ###  PROYECTO:              PKI-SAT                                                                                ###
  ###  MODULO:                Clase que realiza la Conexi�n al SrvAR                                                 ###
  ###                                                                                                                ###
  ###  DESARROLLADORES:       Gudelia Hern�ndez Molina        GHM                                                    ###
  ###                         H�ctor Ornelas Arciga           HOA                                                    ###
  ###  FECHA DE INICIO:       Martes 06, diciembre del 2005                                                          ###
  ###                                                                                                                ###
  ######################################################################################################################
         1         2         3         4         5         6         7         8         9        10        11        12
1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*######################################################################################################################
   VERSION:  
      V.1.00      (20051206 - 20060101) GHM: Primera Versi�n
                                             Tiene una funci�n de preproceso que obtiene la clave de AgC para dejarla
                                             disponible en otros procesos y atiende las solicitudes de conexi�n al SrvAR
      V.1.01      (20060711 - 20060711) MELM
            .01                          - Se agrega funci�n para concluir una conexi�n si se pierde la comunicaci�n 
                                           con el cliente o se recibe indicaci�n de finalizar el proceso       
      V.1.1.2     (20100722)             - se corrige la llamada a la clase CSeparaToken
                  (20080810 -         ) GHM: Se requiere hacer ajustes para introducir la autenticaci�n por LDAP
            .02                          - Se modifica el primer intercambio de datos entre Cliente - Servidor
                                           De version|noSerieAgC A version|noSerieAgC|RFCAgC|ipRealCliente|pwdLDAPencriptado|
                                           el cual se espera encriptada.
            .03                          - Se modifica la forma de separar las cadenas con pipes
            .04                          - Se requiere recibir encriptado el primer intercambio de datos
            .05                          - Se reajustan las validaciones de conexi�n del cliente (version, 
                                           reg en Agente, reg del Cert)
            .06                          - Se modifica el despliegue del primer mensaje en la bit�cora, para evitar vulnerabilidad
                                           el pwdLDAPencriptado del primer mensaje se muestra como * quedando la cadena como
                                           version|noSerieAgC|RFCAgC|ipRealCliente|*|
            .07   (20141014 -         ) JAB: Se agrupan las sentencias SQL utilizadas en el SrvAR en un archivo de
                                              cabecera
                                           
######################################################################################################################*/
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>

#include <SrvAR.h>
//#include <CSeparaToken.h>     //+ V.1.01.03 GHM (081028)
#include <Sgi_PKI.h>  // Se cambia CSeparaToken.h por Sgi_PKI
#include <CConexion.h>
#include <CUtilBDAR.h>

//- #include <MensajesSAT.h>  //Manejo de mensajes  //- GHM (070427)
#include <Sgi_MsgPKI.h>       //+ GHM (070427)
#include <Sgi_B64.h>          //+ V.1.01.04 GHM (081103)
#include <Sgi_LDAP.h>

//################################################################################
//   Variables globales
//################################################################################
int m_IDProceso = -1;

//######################################################################################################################
//Declaraci�n de los CONSTRUCTORES
CConexion::CConexion(CConfigFile& config, MensajesSAT& msgcli, CSSL_Skt* skt):
   COperacion(config, msgcli, skt)
{
   //Inicializacion de variables
   Tkn         = NULL;
   m_numError  = 0;
   m_nivelAgC  = -1;
   m_version   = -1;
   m_IDProceso = -1;
}

//######################################################################################################################
//Declaraci�n del Destructor
CConexion::~CConexion()
{
   //Libera los apuntadores que se hayan creado
   if (Tkn != NULL)
   {
      delete Tkn;
      Tkn = NULL;
   }
}

//FUNCIONES DE APOYO AL SERVIDOR
//######################################################################################################################
bool CConexion::preProceso()
{
   char         DatoRecib[1024*4], codError[3];
   int          lDatoRecib = sizeof(DatoRecib);
   char         nSerieRec[TAM_NOSERIECERT];
   int          iErrorLDAP ;
   bool         verAntRec = false;
 
   memset(DatoRecib,0,sizeof(DatoRecib));
   if ( ( m_numError =  m_SktProc->Recibe((uint8*) DatoRecib, lDatoRecib)) != 0)
   {
      Bitacora->escribePV(BIT_ERROR, "Error en CConexion en preProceso: Al recibir el 1er msg del Cliente %d",m_numError);
      return false;
   }

   m_MsgDesc = ""; // con longitud cero
   m_tipoError = 'N';
   sprintf( codError, "1" );

   if ( !DetVerMsg(DatoRecib, lDatoRecib, &verAntRec, &m_version, nSerieRec) )
      return EnviaMensaje(codError, "en preProceso: El primer mensaje recibido tiene la estructura incorrecta, verifique la versi�n de la" 
      "aplicaci�n" );
   else if ( !VerifNumSerieBDAR(nSerieRec) ) 
      return EnviaMensaje(codError, "en preProceso: Error al validar los datos del certificado de AgC");
   
   else if ( !IniciaMsgSrv(nSerieRec) )
   {
      m_tipoError = 'I';
      m_MsgDesc = "Se present� un error con su conexi�n, favor de reiniciar la aplicaci�n";
   }
   else
   {
      //+ GHM(081028): Para dar soporte a las aplicaciones anteriores a la 2
      if ( verAntRec )
      { //>>> Tratamiento anterior: ver|no_serie AgC 
         if ( !VerifVersion(nSerieRec) )
         {
            m_tipoError = 'I';
            m_MsgDesc = "La versi�n del CertiSAT no es valida.";
         }
      } //<<< 
      else
      { //>>> V.1.01.02 GHM (081028): Tratamiento nuevo: ver|noSerieAgC|IPreal|RFCAgC|pwdLDAP|
         if ( Tkn->getCounter() != 5 )
         {
            Bitacora->escribePV(BIT_ERROR, "Error en CConexion en preProceso: La cadena recibida es incorrecta. Cadena (%s)", DatoRecib);
            m_MsgDesc = "Favor de verificar que su versi�n de la aplicaci�n sea la correcta.";
         }
         else if ( !Envia1erMsgBit(DatoRecib) || !VerifVersionCliente() )
            m_MsgDesc = "Verificar que la versi�n de la aplicaci�n esta autorizada para ingresar al servidor";

         //  Validacion del agente certificador en LDAP
         else if ( ( iErrorLDAP = VerifAgCLDAP(Tkn->items[2], Tkn->items[4]) ) > 0 )
         {
            sprintf( codError, "%d", iErrorLDAP );
            m_MsgDesc = "Falla en la autenticaci�n del AgC con LDAP";
         }
         else
            Bitacora->escribePV(BIT_INFO, "CConexion en preProceso: ============== Verificacion de usuario en LDAP  %s =============",Tkn->items[2]);
      } //<<<
   }
   return EnviaMensaje(codError);
}

bool CConexion::EnviaMensaje(char *codError, char *MsgDesc )
{
   if ( MsgDesc != NULL )
      m_MsgDesc = MsgDesc;

   if ( m_MsgDesc.length() != 0) // Error
      EnviaSinFirma(m_MsgDesc.c_str(), m_tipoError, codError, NULL);
   else
      EnviaSinFirma(" ", 'N', "0", NULL);
   return (m_MsgDesc.length() == 0);
}

// Regresa true: si la estructura del 1er mensaje es correcta else regresa false
bool CConexion::DetVerMsg(char *DatoRecib, int lDatoRecib, bool *verAntRec, int *iversionRec, char *nSerieRec)
{
   char* bufDesenc = NULL;
   int   lbufDesenc = 1024*4;
   char* posicion = NULL;
   bool  ok = false;
  
   //+GHM (Para recibir cadena con formato anterior)
   if ( (DatoRecib[0]== ' ' && DatoRecib[1]== ' ' && DatoRecib[2]== '|') ||
        (DatoRecib[0]== ' ' && DatoRecib[1]== '1' && DatoRecib[2]== '|') )
   {
      DatoRecib[lDatoRecib-1] = '|';
      DatoRecib[lDatoRecib] = 0;
      *verAntRec = true;
      if ( lDatoRecib != ( MAX_TAM_IDVER + TAM_NOSERIECERT + 1 ) )
         Bitacora->escribePV(BIT_ERROR, "Error en CConexion en DetVerMsg: La longitud recibida es incorrecta '%i'", lDatoRecib);
      else
      {
         //Se recibe el primer dato: (id de versi�n | n�mero de Serie ).
         Bitacora->escribePV(BIT_INFO,"CConexion en DetVerMsg: Se recibi� la cadena de versi�n y n�mero de serie: %s", DatoRecib);
         posicion = strchr(DatoRecib,'|');
         if (posicion == NULL)
            Bitacora->escribePV(BIT_ERROR, "Error en CConexion en DetVerMsg: Cadena recibida %s, falta el separador de datos",DatoRecib);
         else
            ok = true;
      }
   }
   else
   {
      //>>> V.1.01.04 GHM(081104): Proceso de desencripci�n para cadena con nuevo formato
      bufDesenc = new char[lbufDesenc];
      if ( bufDesenc == NULL )
         Bitacora->escribe(BIT_ERROR, "Error en CConexion en DetVerMsg: No hay memoria suficiente para buffer desencripci�n del 1er msg");
      else
      {
         lbufDesenc = 1024*4;  // RORS 4 dic 2008 se cambio  porque sizeof solo regresa 4. (sizeof(bufDesenc));
         Bitacora->escribePV(BIT_DEBUG,"CConexion en DetVerMsg: %s",DatoRecib);
         DesencriptaBuffer(DatoRecib, lDatoRecib, bufDesenc, &lbufDesenc);
         memset(DatoRecib, 0, lDatoRecib);
         memcpy(DatoRecib, bufDesenc, lbufDesenc);
         delete[] bufDesenc;
         bufDesenc = NULL;
         *verAntRec = false;
         ok = true;
      }
      //<<< V.1.01.04
   }
   if ( ok )
   {
      Tkn = new CSeparaToken(DatoRecib,'|');
      if ( Tkn->getCounter() < 2 )
      {
         Bitacora->escribe(BIT_ERROR, "Error en CConexion en DetVerMsg: Error al procesar el primer buffer en CSeparaToken");
         ok = false;
      }
      else
      {
         *iversionRec = atoi(Tkn->items[0]);
         strcpy(nSerieRec, Tkn->items[1] );
      } 
   }
   return ( ok );
}

//######################################################################################################################
bool CConexion::IniciaMsgSrv(const char* noSerie)
{
   string cert, privk, pwd, rCerAgcs;
   int r1, r2, r3, r4;

   r1 = m_Configuracion.getValorVar("[LLAVES_MENSAJES_SRV]", "CERT"    , &cert);
   r2 = m_Configuracion.getValorVar("[LLAVES_MENSAJES_SRV]", "PRIVK"   , &privk);
   r3 = m_Configuracion.getValorVar("[LLAVES_MENSAJES_SRV]", "PWDPK"   , &pwd);
   r4 = m_Configuracion.getValorVar("[LLAVES_MENSAJES_SRV]", "RCERAGCS", &rCerAgcs);

   if (r1 || r2 || r3 || r4)
   {
      Bitacora->escribePV(BIT_ERROR, "Error en CConexion en IniciaMsgSrv: Problemas al leer la configuraci�n de 'LLAVES_MENSAJES_SRV' (%d,%d,%d,%d)", 
                          r1, r2, r3, r4);
      return false;
   }
   rCerAgcs += string("/") + noSerie + ".cer";
  
   int   i_pwdDes = pwd.length();
   uint8 *pwdDes = new uint8[i_pwdDes + 1];

   if(!desencripta((uint8*)pwd.c_str(), i_pwdDes, pwdDes, &i_pwdDes))
   {
      Bitacora->escribe(BIT_ERROR, "Error en CConexion en IniciaMsgSrv: Error al desencriptar el password de la llave de mensajes del servidor");
      delete[] pwdDes;
      return false;
   }
   pwdDes[i_pwdDes] = 0;
   
   intE error = m_MsgCli.Mensajes::Inicia(AR, (char*) cert.c_str(), (char*) privk.c_str(), (char*) pwdDes, 
                                        i_pwdDes, (char*) rCerAgcs.c_str());
   delete[] pwdDes;
   pwdDes = NULL; // RORS 4 dic 2008
   if (error)
   {
      Bitacora->escribe(BIT_ERROR, error, "Error en CConexion en IniciaMsgSrv: Error al iniciar la librer�a para el intercambio de mensajes del cliente");
      return false;
   }
   return true;
}

//FUNCIONES DE CALCULO
//######################################################################################################################
int CConexion::GenNumeroAleatorio()
{
   SGIRSA rsa;
   return rsa.GenAleatorio(128, m_numAleatorio, &m_lnumAleatorio);
}

//>>> V.1.01.04 GHM(081104): Funci�n de Desencriptado con llave privada
//######################################################################################################################
bool CConexion::DesencriptaBuffer(char *DatoRecib, int lDatoRecib, char* bufDesenc, int *lbufDesenc)
{
   bool       correcto = false;
   char       *bufPte   = NULL, *dirPVK = NULL;
   int        lbufPte   = 1024*2;
   int        r1 = -1, r2 = -1, i_pwdDes = 100, ierror = 0;

   uint8      pwdDes[i_pwdDes];
   string     pathKeyPriv, pwd;
   ManArchivo archivo;
   SGIX509    sgiX509;
   SGIRSA     objSgiRSA;
      
   r1 = m_Configuracion.getValorVar("[LLAVES_MENSAJES_SRV]", "PRIVK"   , &pathKeyPriv);
   r2 = m_Configuracion.getValorVar("[LLAVES_MENSAJES_SRV]", "PWDPK"   , &pwd);
  
   if (r1 || r2 )
      Bitacora->escribePV(BIT_ERROR, "Error en CConexion en DesencriptaBuffer: En la lectura de la configuraci�n de 'LLAVES_MENSAJES_SRV' para 1er msg (%d,%d)", 
      r1, r2);
   else
   {   
      memset(pwdDes, 0, sizeof(pwdDes));

      if(!desencripta((uint8*)pwd.c_str(), pwd.length(), pwdDes, &i_pwdDes))
         Bitacora->escribe(BIT_ERROR, "Error en CConexion en DesencriptaBuffer: Error al desencriptar el password de la llave de mensajes para el 1er msg");
      else
      {
         pwdDes[i_pwdDes] = 0; 
         if ( (bufPte = new char[lbufPte] ) == NULL )
            Bitacora->escribe(BIT_ERROR, "Error en CConexion en DesencriptaBuffer: No hay memoria suficiente para procesar B64 en el 1er msg");
         else
         {
            memset(bufPte,0,lbufPte); 
            if ( !B64_Decodifica(DatoRecib, (uint16) lDatoRecib, (uint8*) bufPte, (uint16*) &lbufPte ))
               Bitacora->escribe(BIT_ERROR, "Error en CConexion en DesencriptaBuffer: No se decodific� B64 en el 1er msg");
            else
            {
               ierror = objSgiRSA.inicia();
               if (ierror != 0)
                  Bitacora->escribePV(BIT_ERROR, "Error en CConexion en DesencriptaBuffer: N�mero de error (%i) en la desencripci�n RSA del 1er msg", ierror);
               else
               {
                  RSA   *rsaPvKey = NULL;

                  rsaPvKey = RSA_new();
                  if ( !rsaPvKey )
                     Bitacora->escribe(BIT_ERROR, "Error en CConexion en DesencriptaBuffer: No hay memoria para generar objeto RSA para desencripci�n del 1er" 
                     "msg");
                  else
                  {
                     if ( (dirPVK = new char[pathKeyPriv.length() + 1] ) == NULL )
                        Bitacora->escribe(BIT_ERROR, "Error en CConexion en DesencriptaBuffer: No hay memoria suficiente para procesar pathKeyPriv");
                     else
                     {
                        strcpy(dirPVK, pathKeyPriv.c_str());
                        ierror = objSgiRSA.DecodPvKey(dirPVK, (char*) pwdDes, &rsaPvKey);
                        if (ierror != 0)
                           Bitacora->escribePV(BIT_ERROR, "Error en CConexion en DesencriptaBuffer: Error (%i), al procesar la llave privada para desencripci�n" 
                           "del 1er msg", ierror);
                        else
                        {
                           ierror = objSgiRSA.DesencPv((uchar*) bufPte, lbufPte, rsaPvKey, (uchar*) bufDesenc, lbufDesenc, true);
                           if (ierror != 0)
                              Bitacora->escribePV(BIT_ERROR, "Error en CConexion en DesencriptaBuffer: Error (%i), al desencriptar el 1er msg", ierror);
                           else
                              correcto = true;
                        }
                        delete[] dirPVK;
                        dirPVK = NULL;
                     }
                     RSA_free(rsaPvKey);
                     rsaPvKey = NULL;
                  }
                  /*if ( rsaPvKey )
                  {
                     RSA_free(rsaPvKey);
                     rsaPvKey = NULL;
                  }*/
                  objSgiRSA.termina();
               }
               //objSgiRSA.termina();
            }
            delete[] bufPte;
            bufPte = NULL;
         }
      }
   }
   /*if ( dirPVK )
   {
      delete[] dirPVK;
      dirPVK = NULL;
   }
   if ( bufPte )
   {
      delete[] bufPte;
      bufPte = NULL;
   }*/
   return correcto;
}
//<<< V.1.01.04 GHM(081104)

//######################################################################################################################
bool CConexion::ObtenIP(char* IP)
{
   static struct sockaddr_in sa_in;                 
   static socklen_t          lsa_in = sizeof(sa_in);
   
   //Proceso para obtener la direcci�n IP
   uint8* dir = (uint8*) (&sa_in.sin_addr.s_addr);
   if ( (m_numError = getpeername(STDIN_FILENO, (sockaddr*) &sa_in, &lsa_in)) != 0 )
   {
      Bitacora->escribePV(BIT_ERROR, "Error en CConexion en ObtenIP: Se present� el error '%i' al recuperar la IP del proceso", m_numError); 
      return TrataErrorProc(MSGDESC_ICS, "al obtener datos de la conexi�n cliente");
   }
   sprintf(IP,"%d.%d.%d.%d", dir[0], dir[1], dir[2], dir[3]);
   return true;
}

//FUNCIONES PARA EL MANEJO DE ERRORES
//######################################################################################################################
bool CConexion::TrataErrorProc(int itpoMsg, const char* sCausa )
{
   //indicador de tipo de error (PROCESO)
   m_tipoError   = 'I';
   m_numError    = ERR_PROCESO;
   char inicio[] = "Se present� un error "; 
   char soluc[]  = ", \n favor de reiniciar la aplicaci�n, si se vuelve a presentar el error solicite "
                   "apoyo al inform�tico local";
   switch (itpoMsg)
   {
      case MSGDESC_ICS:
         m_MsgDesc = string(inicio) + string(sCausa) + string(soluc);
         break;
      case MSGDESC_CS:
         m_MsgDesc = string(sCausa) + string(soluc);
         break;
      case MSGDESC_VACIO :
         m_MsgDesc = "";
         break;
      case MSGDESC_C:
          m_MsgDesc = string(sCausa);
          break;
   }
   return false;
}


//FUNCIONES DE CONSULTA EN LA BD
//######################################################################################################################
bool CConexion::VerifVersion(const char* nSerie)
{
   int nivelObt = 0, result = 0;

   if ( !m_BDAR->consultaReg(CON_OBTEN_DATOS_AGENTE_POR_NUM_SERIE, nSerie) )
      Bitacora->escribePV(BIT_ERROR, "Error en CConexion en VerifVersion: No se ejecut� correctamente la consulta del nivel para validar el agente(%s)", nSerie );
   else if ( !m_BDAR->getValores("ib", &nivelObt, m_agcMod ) )
      Bitacora->escribePV(BIT_ERROR, "Error en CConexion en VerifVersion: No se obtuvo valores para validar los datos del agente(%s)", nSerie );
   else if ( nivelObt == 6)
      return true;
   //GHM (090319): Se modific� para determinar si el puerto
   //>- if ( !m_BDAR->consultaReg("SELECT count(*) FROM Ver_Aut WHERE id_apl = %i "
   

   #if ORACLE
	   if (result == 0) //No se encontraron registros para la versi�n
		  Bitacora->escribePV(BIT_ERROR, "Error en CConexion en VerifVersion: No se obtuvo registros para la versi�n(%i)", m_version);
	   else if (iPuerto != result)
	   {
		  Bitacora->escribePV(BIT_ERROR, "Error en CConexion en VerifVersion: El puerto de invocaci�n es (%i) y el puerto autorizado es (%i)", iPuerto, 
		  result);
		  result = 0;
	   }
   #else
        if ( !m_BDAR->consultaReg(CON_OBTEN_PUERTO_AUT_POR_APLICACION_1, m_version) )
           Bitacora ->escribePV(BIT_ERROR, "Error en CConexion en VerifVersion: No se realizo la consulta de los datos de la versi�n(%i) en la tabla Ver_Aut", 
           m_version);
        else if ( !m_BDAR->getValores("i", &result) )
           Bitacora->escribePV(BIT_ERROR, "Error en CConexion en VerifVersion: No se obtuvo correctamente los datos de la versi�n(%i) en la tabla Ver_Aut", 
           m_version);
        else
         if (result == 0) //No se encontraron registros para la versi�n
		  Bitacora->escribePV(BIT_ERROR, "Error en CConexion en VerifVersion: No se obtuvo registros para la versi�n(%i)", m_version);
	    else if (iPuerto != result)
	    {
		  Bitacora->escribePV(BIT_ERROR, "Error en CConexion en VerifVersion: El puerto de invocaci�n es (%i) y el puerto autorizado es (%i)", iPuerto, 
		  result);
		  result = 0;
	    }   
   #endif
   
   return (result > 0);
}
  
//######################################################################################################################
bool CConexion::VerifNumSerieBDAR(const char* nSerie)
{
   bool ok = false;

   //Valida el n�mero de serie en la BD
   if ( !(m_BDAR->ejecutaSP(CON_SP_VALIDA_AGC_NOMBRE, CON_SP_VALIDA_AGC_PARAMS, nSerie)) )
      Bitacora->escribePV(BIT_ERROR, "Error en CConexion en VerifNumSerieBDAR: Falla el sp_validaAgC en VerifNumSerieBDAR(%s)", nSerie);
   //Verifica el resultado obtenido 
   else if ( !(m_BDAR->getValores("ibs", &m_numError, m_agcCve, &m_MsgDesc)) )
      Bitacora->escribePV(BIT_ERROR, "Error en CConexion en VerifNumSerieBDAR: Falla el sp_validaAgC en getValores en VerifNumSerieBDAR(%s)", nSerie);
   else if (m_numError != 0) //Error de la operaci�n si != 0: NOEXISTECERT, CERTNOACTIVO o CERTSINVIG
      Bitacora->escribePV(BIT_ERROR, "Error en CConexion en VerifNumSerieBDAR: Certificado de AgC no registrado en la BD, nSerie=%s, m_numError(%d)", nSerie, 
      m_numError);
   else
      ok = true;
   return ( ok );
}

//######################################################################################################################
// Verificar la Vigencia del Certificado del Agc (Aqui por el momento tambien se incluye el CSW) 
bool CConexion::VerifVigencia(const char* nSerie)
{
   int  vigente;
   char msg_err[200], codError[5];

   if ( (m_numError = ( m_utilBDAR->verificaVigencia(nSerie, &vigente) != 1 )) != 0 )
      EnviaSinFirma("Error en VerifVigencia: no pudo ejecutarse el sp_valFecCert() en la DB de la AR", 'I', "1");

   else if ( vigente != 0) // S� el Store Procedure Regresa en vigente 0 si esta vigente, Si > 0 si => hay algun error 
   {
      sprintf(codError, "%i", CERTNOVIGENTE);
      sprintf (msg_err, "Error en VerifVigencia: el certificado correspondiente al n�mero de serie %s, no esta vigente(%d).",
                           nSerie, vigente);
      EnviaSinFirma(msg_err, 'N', codError);
   }
   return ( !m_numError && !vigente );
}

//######################################################################################################################
//+  V.1.01.06 GHM(081114): Funci�n que modifica el despliegue del primer mensaje en la bit�cora sustituyendo el dato de 
//                          pwdLDAPencriptado con *
bool CConexion::Envia1erMsgBit(char* dato)
{
   int i = 0, pos = 0, ldato = strlen(dato);
   char *buffdato = new char[ldato + 1];

   memcpy(buffdato, dato, ldato);

   for ( i= ldato-2; i > -1; i--)
   {
      pos = i;
      if ( buffdato[i] == '|' )
         break;
   }
   buffdato[pos+1] = '*';
   buffdato[pos+2] = '|';
   for (i = ldato-1; i > pos+2; i--)
      buffdato[i] = 0;
   Bitacora->escribePV(BIT_INFO,"CConexion en Envia1erMsgBit: Se recibi� el 1er msg: (%s)", buffdato);
   delete[] buffdato;
   return true;
}

//######################################################################################################################
//+ V.1.01.05 GHM(081107): Funci�n que verifica la version del cliente sea valida
bool CConexion::VerifVersionCliente()
{
   int result = 0;
   
   //GHM (090319): Se modific� para determinar si el puerto 
   //if ( !m_BDAR->consultaReg("SELECT count(*) FROM Ver_Aut WHERE id_apl = %i "
   
   #if ORACLE
      if (iPuerto != result)
	   {
		  Bitacora->escribePV(BIT_ERROR, "Error en CConexion en VerifVersionCliente: El puerto de invocaci�n es (%i) y el puerto autorizado es (%i)", 
		  iPuerto, result);
		  result = 0;
	   }
   #else
       if ( !m_BDAR->consultaReg(CON_OBTEN_PUERTO_AUT_POR_APLICACION_2, m_version) )
           Bitacora ->escribePV(BIT_ERROR, "Error en CConexion en VerifVersionCliente: No se realizo la consulta de los datos de la versi�n (%i)", m_version);
       if ( !m_BDAR->getValores("i", &result) )
           Bitacora ->escribePV(BIT_ERROR, "Error en CConexion en VerifVersionCliente: No se obtuvo correctamente los datos de la versi�n (%i)", m_version);
       if (result == 0) // No se encontraron registros de la versi�n
           Bitacora->escribePV(BIT_ERROR, "Error en CConexion en VerifVersionCliente: No se obtuvo registros para la versi�n (%i)", m_version);
       else
         if (iPuerto != result)
        {
           Bitacora->escribePV(BIT_ERROR, "Error en CConexion en VerifVersionCliente: El puerto de invocaci�n es (%i) y el puerto autorizado es (%i)", iPuerto, 
           result);
           result = 0;
        }
   #endif
   
   
 
   return (result > 0);
}

//######################################################################################################################
bool CConexion::EnviaSinFirma(const char* Msg, char tipoError, const char* codError, const char* bitac )
{
   char bufEnvio[500];
   int lbufEnvio = sizeof(bufEnvio);

   if ( CreaError(tipoError, codError, Msg, bufEnvio, &lbufEnvio) != 0 ||
        m_SktProc->Envia( (uchar*)bufEnvio, lbufEnvio ) != 0 )
   {
      if (bitac != NULL)
         Bitacora ->escribePV(BIT_ERROR,"Error en CConexi�n %s",Msg, bitac);
      return false;
   }
   return true;
}

//######################################################################################################################
int CConexion::ObtenNivelAGC()
{
   int nivelObt;
   
   if ( !(nivelObt = m_utilBDAR->getNivelAGC(m_agcCve)) )
      TrataErrorProc(MSGDESC_ICS, "al verificar los datos del Agente Certificador");
   
   return ((!nivelObt)? -1:nivelObt); // MELM esto es *q Gude regresaba -1 cuando habia error en el nivel del AgC
}
//######################################################################################################################
bool CConexion::RegConexionBD()
{
   char dirIP[TAM_IP];
   char dirIPAnt[TAM_IP], fec_ini[TAM_FECHAS], fec_ult[TAM_FECHAS];
   int  IDprocAnt, startTm = 0, starTmAnt = 0;
   //GHM (080919): variable para stored procedure modificado
   int  verAplic = 0;

   pid_t IDProceso = getpid(); //Obtiene el ID del proceso actual
   m_IDProceso = IDProceso;
   startTm = getStartTm(IDProceso);  
   
   if ( !ObtenIP(dirIP) )
      return false;
   //GHM (080919): Se modific� el stored procedure para permitir la convivencia entre versiones.
   //>- if ( !(m_BDAR->ejecutaSP("sp_conexion", "'%s',%i,'%s',%i", m_agcCve,IDProceso, dirIP, startTm)) )
   if ( !(m_BDAR->ejecutaSP(CON_SP_CONEXION_V2_NOMBRE, CON_SP_CONEXION_V2_PARAMS, m_agcCve,IDProceso, dirIP, startTm, m_version)) )
   {
      Bitacora->escribe(BIT_ERROR, "Error en CConexion en RegConexionBD: Al ejecutar en la BD el registro de la conexi�n");
      return TrataErrorProc(MSGDESC_ICS, "al registrar la conexi�n en el servidor");
   }
   //Verifica el resultado obtenido
   //GHM (080919): Se modific� la salida del stored procedure para permitir la convivencia entre versiones.
   //>- if ( !(m_BDAR->getValores("ibbibi", &m_numError, fec_ini, fec_ult, &IDprocAnt, dirIPAnt, &starTmAnt)) )  
   if ( !(m_BDAR->getValores("ibbibii", &m_numError, fec_ini, fec_ult, &IDprocAnt, dirIPAnt, &starTmAnt, &verAplic)) )  
   {  
      Bitacora->escribe(BIT_ERROR, "Error en CConexion en RegConexionBD: Al obtener los datos del registro de la conexi�n");
      return TrataErrorProc(MSGDESC_ICS, "al recuperar los datos de la conexi�n del Servidor");
   }
   if ( m_numError != 0 ) //Obtiene la descripcion del error de operaci�n
   {
      m_tipoError = 'I';
      //GHM (080919): Modificaciones para la convivencia entre versiones.
      /* >>- Bitacora->escribe( BIT_ERROR, "Conexi�n(I): Existe una conexi�n previa" );
      if ( m_numError != USUYACONEC )
         return TrataErrorProc(MSGDESC_CS,"Error inesperado al revisar la existencia de una conexi�n previa");
      << -*/
      if ( m_numError == USUYACONEC )
      {
         if ( !CierraConexPrev(fec_ini, dirIPAnt, IDprocAnt, starTmAnt) )
         {
            Bitacora->escribe(BIT_ERROR, "Error en CConexion en RegConexionBD: Al cerrar la conexi�n previa");
            if ( m_MsgDesc.length() == 0 )
               TrataErrorProc(MSGDESC_ICS, "al cerrar la conexi�n previa al Servidor");
            return false;
         }
         //>- if ( !(m_BDAR->ejecutaSP("sp_conexion", "'%s',%i,'%s',%i", m_agcCve,IDProceso, dirIP, startTm)) )
         if ( !(m_BDAR->ejecutaSP(CON_SP_CONEXION_V2_NOMBRE, CON_SP_CONEXION_V2_PARAMS, m_agcCve,IDProceso, dirIP, startTm, m_version)) )
         {
            Bitacora->escribe(BIT_ERROR, "Error en CConexion en RegConexionBD: Al registrar en la BD la conexi�n nueva");
            return TrataErrorProc(MSGDESC_ICS, "al registrar la conexi�n nueva al servidor");
         }
         //Verifica el resultado obtenido
         //>- if ( !(m_BDAR->getValores("ibbibi", &m_numError, fec_ini, fec_ult, &IDprocAnt, dirIPAnt, &starTmAnt)) )
         if ( !(m_BDAR->getValores("ibbibii", &m_numError, fec_ini, fec_ult, &IDprocAnt, dirIPAnt, &starTmAnt, &verAplic)) )
         {
            Bitacora->escribe(BIT_ERROR, "Error en CConexion en RegConexionBD: Al obtener el registro de la conexi�n nueva");
            return TrataErrorProc(MSGDESC_ICS, "al recuperar los datos de la conexi�n nueva del Servidor");
         }
         if ( m_numError != 0 )
         {
            Bitacora->escribe(BIT_ERROR, "Error en CConexion en RegConexionBD: Al obtener el registro de la conexi�n nueva");
            return TrataErrorProc(MSGDESC_ICS, "al recuperar los datos de la conexi�n nueva del Servidor");
         }
      }
      else
      {
         if ( m_numError == IPVER1_DIF_IPVER2 )
         {
            if ( !EnviaErrorIPs(fec_ini, dirIPAnt, dirIP ) )
            {
               Bitacora->escribe(BIT_ERROR, "Error en CConexion en RegConexionBD: Al enviar al cliente los datos de la conexi�n de otra versi�n ");
               if ( m_MsgDesc.length() == 0 )
                  TrataErrorProc(MSGDESC_ICS, "al enviar la informaci�n de la conexi�n de otro versi�n del cliente");
            }
            return false;
         }else
            return TrataErrorProc(MSGDESC_CS,"Error inesperado al revisar la existencia de una conexi�n de otra versi�n");
      }
   }
   //if (!m_utilBDAR->RegOperDetalleBDAR(USUYACONEC, m_numOp))
   //    Bitacora->escribe(BIT_ERROR, "Conexi�n(I): Al registrar USUYACONEC en el detalle de operaci�n");
   return true;
}

//######################################################################################################################
bool CConexion::ObtListaMot(string *lista_motivo)
{
   string motivo;
   char indice[5];
   if ( !m_BDAR->consulta(CON_OBTEN_DATOS_MOTIVOS_DE_REVOCACION) )
      return false;
   while( m_BDAR->sigReg() )
   {
      if(!m_BDAR->getValores("bs", indice, &motivo))
         return false;
      *lista_motivo = *lista_motivo + indice + "|" +  motivo + "|";
   }
   return true;
}

//######################################################################################################################
bool CConexion::ObtListaCert(string *lista_tipos)
{
   string tipos;
   char indice[5];
   if( !m_BDAR->consulta(CON_OBTEN_DATOS_TIPO_DE_CERTIFICADOS_POR_AGENTE, m_agcCve) )
      return false;
   while( m_BDAR->sigReg() )
   {
      if ( !m_BDAR->getValores("bs", indice,&tipos) )
          return false;
      *lista_tipos = *lista_tipos + indice + "|" + tipos + "|";
   }
   return true;
}

/*######################################################################################################################
   SALIDAS:   Regresa un valor num�rico que indica
              0 = Operaci�n con �xito
              1 = Se recibio un mensaje de error
              2 = No se pudo obtener los datos del mensaje correcto
              3 = No se pudo obtener los datos del mensaje de error
              4 = El mensaje recibido no es el esperado, ni es un mensaje de error
######################################################################################################################*/
int CConexion::EsMsgCorrecto(int numMsgRcb, int numMsgEsp)
{
   bool tratMsgCorr = false;
   if (numMsgRcb == numMsgEsp)
   {
      //Se recibe el resultado del procesamiento
      Bitacora->escribePV(BIT_INFO, "CConexion en EsMsgCorrecto: Se recibe el resultado del procesamiento");
      tratMsgCorr = TrataMensaje(numMsgRcb);
      return (tratMsgCorr) ? 0 : 2 ;
   }
   else
   {
      if (numMsgRcb == MSGERROR)
      {
         tratMsgCorr = TrataMensaje(numMsgRcb);
         return (tratMsgCorr) ? 1 : 3;
      }
      else
      {  
         m_numError = ERR_MSGNOCORRECTO;
         Bitacora->escribePV(BIT_ERROR, "Error en CConexion en EsMsgCorrecto: Se present� el mensaje '%i' que es inesperado",
                             numMsgRcb);
         m_tipoError = 'I';
         m_MsgDesc = "Error en CConexion en EsMsgCorrecto: Se recibi� un mensaje inesperado";
         return 4;
      }
   }
}

//######################################################################################################################
bool CConexion::TrataMensaje(int numMsgRecibido)
{
   char firma[TAM_FIRMA+1];
   int lfirma =  sizeof(firma);  // RORS 4 dic 2008
   string lista;
            
   switch (numMsgRecibido)
   {
      case SOLCONEXION:
         char numSerieAgC[TAM_NOSERIECERT];
         int lnumSerieAgC;
         //Inicializa variables
         lnumSerieAgC = sizeof(numSerieAgC);
         if ( (m_numError = m_MsgCli.getMensaje(numMsgRecibido, firma, &lfirma, numSerieAgC, &lnumSerieAgC))
              != 0 ) //Se present� un error
         {  
            Bitacora->escribePV(BIT_ERROR, "Error en CConexion en TrataMensaje: Se present� el error '%i' al obtener par�metros de "
                                           "SOLCONEXION", m_numError);
            return TrataErrorProc(MSGDESC_ICS, "al leer los par�metros en la comunicaci�n");
         }
         m_lnumAleatorio = sizeof(m_numAleatorio);  // RORS 4 dic 2008
         m_numAleatorio[0] = 0;                     // RORS 4 dic 2008
         //Genera el n�mero aleatorio para PruebaID
         if ( ( m_numError = GenNumeroAleatorio()) !=  0 )
         {
            Bitacora->escribePV(BIT_ERROR, "Error en CConexion en TrataMensaje: Se present� el error '%i' al calcular el n�mero "
                                           "aleatorio ", m_numError);
            return TrataErrorProc(MSGDESC_ICS, "al leer los par�metros en la comunicaci�n");
         }
         m_numAleatorio[m_lnumAleatorio] = 0;   // RORS 4 dic 2008
         Bitacora->escribePV(BIT_DEBUG, "CConexion en TrataMensaje: N�mero aleatorio generado es (%s) longitud es (%d)",
                             m_numAleatorio, (long) m_lnumAleatorio);
         if ( (m_numError =  m_MsgCli.setMensaje(PRUEBAID, m_numAleatorio, m_lnumAleatorio)) != 0 )
         {
            Bitacora->escribePV(BIT_ERROR, "Error en CConexion en TrataMensaje: Se present� el error (%i) al generar el mensaje de "
                                           "PRUEBAID", m_numError);
            return TrataErrorProc(MSGDESC_ICS, "al enviar datos en la comunicaci�n") ;
         }
         if ( (m_numError = envia(CERTISAT)) != 0)
         {
            Bitacora->escribe(BIT_ERROR, "Error en CConexion en TrataMensaje: AL enviar el mensaje de PRUEBAID");
            return TrataErrorProc(MSGDESC_VACIO, "");
         }
         if (!m_utilBDAR->RegOperDetalleBDAR(PRUEBAID, m_numOp))
            Bitacora->escribe(BIT_ERROR, "Error en CConexion en TrataMensaje: Al registrar PRUEBAID en el detalle de operaci�n");
         if (recibe(CERTISAT) != 0)
         {
            Bitacora->escribe(BIT_ERROR, "Error en CConexion en TrataMensaje: Al recibir el mensaje de contestaci�n de PRUEBAID");
            return TrataErrorProc(MSGDESC_VACIO, "");
         }
         return (m_numError = EsMsgCorrecto(m_MsgCli.tipOperacion(), CONFIDENT)) == 0 ;
         break;
      case CONFIDENT:
         char numAleatRecib[TAM_NUMALEATORIO];
         int lnumAleatRecib;
         //Inicializa variables
         lfirma = sizeof(firma);
         lnumAleatRecib = sizeof(numAleatRecib);
         if (!m_utilBDAR->RegOperDetalleBDAR(CONFIDENT, m_numOp))
            Bitacora->escribe(BIT_ERROR, "Error en CConexion en TrataMensaje: Al registrar CONFIDENT en el detalle de operaci�n");
         if ( (m_numError = m_MsgCli.getMensaje(numMsgRecibido, firma, &lfirma, numAleatRecib, &lnumAleatRecib))
              != 0 )
         {
             Bitacora->escribePV(BIT_ERROR, "Error en CConexion en TrataMensaje: Se present� el error (%i) al obtener par�metros de "
                                            "CONFIDENT", m_numError);
             return TrataErrorProc(MSGDESC_ICS, "al leer los par�metros en la comunicaci�n");
         }
         //Revisi�n del n�mero aleatorio
         //m_numError = strcmp(m_numAleatorio, numAleatRecib);
         // RORS  3 dic 2008  Se inicializo m_numError,numAleatRecib y se cambio strcmp por strncmp  
         m_numError = -1; 
         numAleatRecib[lnumAleatRecib] = 0;
         m_numError = strncmp(m_numAleatorio, numAleatRecib,lnumAleatRecib);
         // 
         if ( !(m_lnumAleatorio == lnumAleatRecib) &&
              !memcmp(m_numAleatorio, numAleatRecib, m_lnumAleatorio) )
         {
            Bitacora->escribePV(BIT_ERROR, "Error en CConexion en TrataMensaje: Se envio el n�mero (%s) y se recibi� del AGC (%s)",
                                m_numAleatorio, numAleatRecib);
            return TrataErrorProc(MSGDESC_CS, "No se recibi� correctamente el c�digo de identificaci�n "
                                              "del Agente Certificador");
         }
         if ( (m_numError =  m_MsgCli.setMensaje(CONEXION)) != 0 )
         {
            Bitacora->escribePV(BIT_ERROR, "Error en CConexion en TrataMensaje: Se present� el error (%i) al generar el mensaje de "
                                           "CONEXION", m_numError);
            return TrataErrorProc(MSGDESC_ICS, "al enviar datos en la comunicaci�n") ;
         }
         if ( (m_numError = envia(CERTISAT)) != 0 )
         {
            Bitacora->escribe(BIT_ERROR, "Error en CConexion en TrataMensaje: Al enviar el mensaje de CONEXION");
            return TrataErrorProc(MSGDESC_VACIO, "");
         }
         if (!m_utilBDAR->RegOperDetalleBDAR(CONEXION, m_numOp))
            Bitacora->escribe(BIT_ERROR, "Error en CConexion en TrataMensaje: Al registrar CONEXION en el detalle de operaci�n");
         if ( (m_numError = recibe(CERTISAT)) != 0 )
         {
            Bitacora->escribe(BIT_ERROR, "Error en CConexion en TrataMensaje: Al recibir el mensaje de respuesta de CONEXION");
            return TrataErrorProc(MSGDESC_VACIO, "");
         }
         return (m_numError = EsMsgCorrecto(m_MsgCli.tipOperacion(), IDENAGC)) == 0;
         break;
      case IDENAGC:
         char numALAC[4]; // RORS 4 dic 2008 Se cambio de 3 a 4 ya que el  numero puede ser de  3 + 1 del blanco
         int lnumALAC;
         char CveAgCRec[TAM_CVEAGC];
         int lCveAgCRec;
         //Inicializa de variables
         lnumALAC   =  sizeof(numALAC);
         lCveAgCRec = sizeof(CveAgCRec); 
         if (!m_utilBDAR->RegOperDetalleBDAR(IDENAGC, m_numOp))
            Bitacora->escribe(BIT_ERROR, "Error en CConexion en TrataMensaje: Al registrar IDENAGC en el detalle de operaci�n");
         if ( (m_numError = m_MsgCli.getMensaje(numMsgRecibido, firma, &lfirma, numALAC, &lnumALAC,
                                                CveAgCRec, &lCveAgCRec)) != 0 )
         {
            Bitacora->escribePV(BIT_ERROR, "Error en CConexion en TrataMensaje: Se present� el error (%i) al obtener par�metros de "
                                           "IDENAGC", m_numError);
            return TrataErrorProc(MSGDESC_ICS, "al leer los par�metros en la comunicaci�n");
         }
         if ((m_nivelAgC = ObtenNivelAGC()) == 6) //Conexi�n del CertiSATWeb
         {
            if ( (m_numError =  m_MsgCli.setMensaje(CONEXIONUSU)) != 0 )
            {
               Bitacora->escribePV(BIT_ERROR, "Error en CConexion en TrataMensaje: Se present� el error '%i' al generar el mensaje de "
                                              "CONEXIONUSU para CertiSAT Web", m_numError);
               return TrataErrorProc(MSGDESC_ICS, "al enviar datos en la comunicaci�n") ;
            }
            if( (m_numError = envia(CERTISAT)) != 0 )
            {
               Bitacora->escribePV(BIT_ERROR, "Error en CConexion en TrataMensaje: Se present� el error '%i' al enviar el mensaje de "
                                              "CONEXIONUSU a CertiSAT Web", m_numError);
               return TrataErrorProc(MSGDESC_VACIO, "");
             }
            if (!m_utilBDAR->RegOperDetalleBDAR(CONEXIONUSU, m_numOp))
               Bitacora->escribe(BIT_ERROR, "Error en CConexion en TrataMensaje: Al registrar CONEXIONUSU en el detalle de operaci�n");
            return true;
         }
         if (m_nivelAgC == -1) //no se obtevo correctamente el nivel de AgC
             return false;
         if (!RegConexionBD())
         { 
            char codError[5];
            sprintf(codError, "%i", ERR_PROCESO);
            if ( (m_MsgDesc.length() > 0 ) )
            {
               if (  (m_numError = setMsgErr(CERTISAT, m_tipoError, codError, m_MsgDesc.c_str())) != 0 )
               {
                  Bitacora->escribe(BIT_ERROR, "Error en CConexion en TrataMensaje: Error al enviar mensaje de usuario ya conectado");
                  return TrataErrorProc(MSGDESC_VACIO, "");
               }
               if( (m_numError = envia(CERTISAT)) != 0 )
               {
                  Bitacora->escribePV(BIT_ERROR, "Error en CConexion en TrataMensaje: Se present� el error '%i' al enviar el mensaje de "
                                                 "error en la conexi�n", m_numError);
                  return TrataErrorProc(MSGDESC_VACIO, "");
               }
            }
         }else
         {
            if ( (m_numError =  m_MsgCli.setMensaje(CONEXIONUSU)) != 0 )
            {
               Bitacora->escribePV(BIT_ERROR, "Error en CConexion en TrataMensaje: Se present� el error '%i' al generar el mensaje de "
                                              "CONEXIONUSU", m_numError);
               return TrataErrorProc(MSGDESC_ICS, "al enviar datos en la comunicaci�n") ;
            }
         }
         if ( (m_numError = envia(CERTISAT)) != 0)
         {
            Bitacora->escribe(BIT_ERROR, "Error en CConexion en TrataMensaje: Al enviar el mensaje de CONEXIONUSU");
            return TrataErrorProc(MSGDESC_VACIO, "");
         }
         if (!m_utilBDAR->RegOperDetalleBDAR(CONEXIONUSU, m_numOp))
            Bitacora->escribe(BIT_ERROR, "Error en CConexion en TrataMensaje: Al registrar CONEXIONUSU en el detalle de operaci�n");
         return true;
         break;
      case SOLTPOCERT:
         if (!m_utilBDAR->RegOperDetalleBDAR(SOLTPOCERT, m_numOp))
            Bitacora->escribe(BIT_ERROR, "Error en CConexion en TrataMensaje: Al registrar SOLTPOCERT en el detalle de operaci�n");
         if (ObtListaCert(&lista))
         {
            if( (m_numError = this->m_MsgCli.setMensaje(LISTATPOCERT, lista.c_str(), lista.length())) !=0 )
            {
               Bitacora->escribe(BIT_ERROR, m_numError, "Error en CConexion en TrataMensaje: Al establecer mensaje LISTATPOCERT");
               return false;
            }
            if( (m_numError = envia(CERTISAT)) != 0 )
            {
               Bitacora->escribe(BIT_ERROR, m_numError, "Error en CConexion en TrataMensaje: Al enviar mensaje a cliente");
               return false;
            }
            if (!m_utilBDAR->RegOperDetalleBDAR(LISTATPOCERT, m_numOp))
               Bitacora->escribe(BIT_ERROR, "Error en CConexion en TrataMensaje: Al registrar SOLTPOCERT en el detalle de operaci�n");
            return true;
         }
         Bitacora->escribe(BIT_ERROR, "Error en CConexion en TrataMensaje: Al obtener la lista de tipos de Certificado ");
         return false;            
         break;
      case CIERRAUSUANT:
         if (!m_utilBDAR->RegOperDetalleBDAR(CIERRAUSUANT, m_numOp))
            Bitacora->escribe(BIT_ERROR, "Error en CConexion en TrataMensaje: Al registrar CIERRAUSUANT en el detalle de operaci�n");
         return true;
         break;
      case SOLDESCONEXION:
         return false;
         break;
         //Cerrar el proceso 
      default :
         m_numError = ERR_MSGNOCORRECTO;
         Bitacora->escribePV(BIT_ERROR, "Error en CConexion en TrataMensaje: Se present� el mensaje '%i' que es inesperado",
                             numMsgRecibido);
         m_tipoError = 'I'; 
	      m_MsgDesc = "Error en CConexion en TrataMensaje: Se recibi� un mensaje inesperado";
         return false;
         break;
   }
   return true;
}
//######################################################################################################################
bool CConexion::CierraConexPrev(char* fec_inic, char* dirIPAnt, int ID_procAnt, int startTmAnt)
{
   char codError[5];
   sprintf(codError, "%i", USUYACONEC);
   m_MsgDesc = "Se detecto una conexi�n previa con fecha: " + string(fec_inic) +
               "\nconectado desde el equipo con direcci�n IP: " + string(dirIPAnt) + "\n�Desea desconectarla?";
   m_tipoError = 'N';
   
   if (  (m_numError = setMsgErr(CERTISAT, m_tipoError, codError, m_MsgDesc.c_str())) != 0 )
   {
      Bitacora->escribe(BIT_ERROR, "Error en CConexion en CierraConexPrev: Error al enviar mensaje de usuario ya conectado");
      return TrataErrorProc(MSGDESC_VACIO, "");
   }
   if ( envia(CERTISAT) != 0)
   {
      Bitacora->escribe(BIT_ERROR, "Error en CConexion en CierraConexPrev: Al enviar el mensaje de una conexi�n previa USUYACONEC");
      return TrataErrorProc(MSGDESC_VACIO, "");
   }
   if (!m_utilBDAR->RegOperDetalleBDAR(USUYACONEC, m_numOp))
            Bitacora->escribe(BIT_ERROR, "Error en CConexion en CierraConexPrev: Al registrar USUYACONEC en el detalle de operaci�n");

   int rec = -1;
   for (int i = 0; i < 2 && rec != 0; i++ )
      rec = sgiErrorBase(recibe(CERTISAT));

   if (rec != 0)
   {
      Bitacora->escribePV(BIT_ERROR, "Error en CConexion en CierraConexPrev: Al recibir el mensaje de contestaci�n de conexi�n previa USUYACONEC", rec);
      return TrataErrorProc(MSGDESC_C, "Se termin� el tiempo para recibir su contestaci�n, vuelva a intentar la conexi�n");
   }
   if ( m_MsgCli.tipOperacion() != CIERRAUSUANT )
   {
      Bitacora->escribePV(BIT_ERROR, "Error en CConexion en CierraConexPrev: Se recibio el mensaje '%i' en vez de CIERRAUSUANT",
                                     m_MsgCli.tipOperacion() );
      return TrataErrorProc(MSGDESC_VACIO, "");
   }
   delProceso(ID_procAnt, startTmAnt);

   bool ok_ejecuta = false;

   #if ORACLE
      ok_ejecuta = m_BDAR->ejecutaFuncion(CON_SP_DESCONEXION_V2_NOMBRE, CON_SP_DESCONEXION_V2_RETURN, CON_SP_DESCONEXION_V2_PARAMS, m_agcCve, ID_procAnt);
   #else
      ok_ejecuta = m_BDAR->ejecutaSP(CON_SP_DESCONEXION_V2_NOMBRE, CON_SP_DESCONEXION_V2_PARAMS, m_agcCve, ID_procAnt);
   #endif

   if ( !ok_ejecuta )
   {
      Bitacora->escribe(BIT_ERROR, "Error en CConexion en CierraConexPrev: Al ejecutar en la BD el cierre del registro de la conexi�n anterior");
      return TrataErrorProc(MSGDESC_CS, "Problemas al cerrar la conexi�n previa");;
   }
   int resultado=0;
   //Verifica el resultado obtenido
   if ( !(m_BDAR->getValores("i", &resultado)) )
   {
      Bitacora->escribe(BIT_ERROR, "Error en CConexion en CierraConexPrev: Al obtener el resultado de eliminar el registro de la conexi�n");
      return false;
   }
   if ( resultado!= 0 ) //Obtiene la descripcion del error de operaci�n
   {
      Bitacora->escribe( BIT_ERROR, m_numError, "Error en CConexion en CierraConexPrev: %s, al leer el resultado del borrado del registro"
                                                " de conexi�n"  );
      return false;
   }
   if (!m_utilBDAR->RegOperDetalleBDAR(CIERRAUSUANT, m_numOp))
      Bitacora->escribe(BIT_ERROR, "Error en CConexion en CierraConexPrev: Al registrar CIERRAUSUANT en el detalle de operaci�n");
   return true;
}

//GHM (090319): Procesa el manejo de IPs para la conexi�n
bool CConexion::EnviaErrorIPs(char* fec_inic, char* dirIPAnt, char* dirIPAct)
{
   char codError[5];
   sprintf(codError, "%i", IPVER1_DIF_IPVER2);
   m_MsgDesc = "Se detect� una conexi�n con la versi�n anterior con fecha: " + string(fec_inic) +
               "\nconectado desde el equipo con direcci�n IP: " + string(dirIPAnt) +
               ".\nLa nueva conexi�n se recibio de la IP:" + string(dirIPAct) +
               "\n\nPara permitir �sta �ltima, se requiere abrirla desde el mismo equipo de la versi�n anterior" +
               "\n o cerrar dicha conexi�n.";
   m_tipoError = 'N';
   Bitacora->escribePV(BIT_ERROR,"Error en CConexion en EnviaErrorIPs: Se detect� una conexi�n de la versi�n anterior desde la IP (%s) y "
                                 "la conexi�n de �sta versi�n se solicita desde la IP", dirIPAnt, dirIPAct );
   if (  (m_numError = setMsgErr(CERTISAT, m_tipoError, codError, m_MsgDesc.c_str())) != 0 )
   {
      Bitacora->escribe(BIT_ERROR, "Error en CConexion en EnviaErrorIPs: Error al enviar mensaje de error en las ips");
      return TrataErrorProc(MSGDESC_VACIO, "");
   }
   if ( envia(CERTISAT) != 0)
   {
      Bitacora->escribe(BIT_ERROR, "Error en CConexion en EnviaErrorIPs: Al enviar el mensaje de error de ips IPVER1_DIF_IPVER2");
      return TrataErrorProc(MSGDESC_VACIO, "");
   }
   return true;
}

//######################################################################################################################
bool CConexion::delProceso(int ID_procAnt, int startTmAnt)
{
   int startTm = -1;
   if ( (startTm = getStartTm(ID_procAnt)) != -1 )
   {
      if (startTm == startTmAnt)
      { 
         int error = kill(ID_procAnt, SIGINT);
         if (error && errno!= ESRCH)
         {
            Bitacora->escribePV(BIT_ERROR, "Error en CConexion en delProceso: Se present� el error '%i' al cerrar el proceso %i ",
                                errno, ID_procAnt);
            return false;
         }
      }else
         Bitacora->escribePV(BIT_ERROR, "Error en CConexion en delProceso: El proceso (%i) no pertenecia al usuario ", ID_procAnt);
   }
   return true;
}

//######################################################################################################################
int CConexion::getStartTm(int ID_proc)
{
   char   dirProcSys[50];
   char   lineaDatos[400];
   int    numStatTm = 0, tamLineaDat = sizeof(lineaDatos);
   
   Bitacora->escribePV(BIT_DEBUG, "CConexion en getStartTm: strEjecutable=%s, iPuerto=%d", strEjecutable.c_str(), iPuerto );
   /* MAML 19/Mar/2009
   #ifdef DBG
      const char* ejecutable = "(SrvAR_dbg)";
   #else
      const char* ejecutable = "(SrvAR)";
   #endif
   */   
   sprintf(dirProcSys, "/proc/%i/environ", ID_proc );
   if ( access((char*)dirProcSys,F_OK|R_OK) != 0 )
   {
      Bitacora->escribePV(BIT_ERROR, "Error en CConexion en getStartTm: No existe o no se puede leer el proceso %i", ID_proc);
      return -1;
   }
   sprintf(dirProcSys, "/proc/%i/stat", ID_proc );
   tamLineaDat = sizeof(lineaDatos);
   if ( !CargaArchivoMem((const char*)dirProcSys, (char*)lineaDatos, &tamLineaDat) )
   {
      Bitacora->escribe(BIT_ERROR, "Error en CConexion en getStartTm: No se pudo extraer el comando del proceso");
      return -1;
   }
   bool esAplic = false;
   char *token;
   int dato = 1;
   token = strtok( (char*)lineaDatos, " " );
   while( token != NULL )
   {
      if ( dato == 2)
      {
        if ( !(esAplic = (strcmp(token, strEjecutable.c_str() )== 0)) ) // , ejecutable
           break;
      }
      if ( dato == 22 )
      {
         numStatTm = atoi(token);
         break;
      }
      dato++;
      token = strtok(NULL, " " );
   }
   if ( !esAplic)
   {
      Bitacora->escribePV(BIT_ERROR, "Error en CConexion en getStartTm: El proceso (%i) no corresponde a la aplicaci�n", ID_proc);
      return -1;
   }
   if ( numStatTm <= 0 )
   {
      Bitacora->escribe(BIT_ERROR, "Error en CConexion en getStartTm: No se pudo extraer el valor de StatTime");
      return -1;
   }
   return numStatTm;
}

//######################################################################################################################
bool CConexion::CargaArchivoMem(const char* dirProcSys, char* lineaDatos, int *tamLineaDat) 
{
   FILE *arch;
   char c;

   if( (arch = fopen(dirProcSys, "r")) == NULL )
   {
      Bitacora->escribePV(BIT_ERROR, "Error en CConexion en CargaArchivoMem: No se pudo leer el archivo: %s", dirProcSys);
      return false;
   }
   int cont = 0;
   while (cont < (*tamLineaDat - 2))
   {
      if ( fread(&c, sizeof(c),1, arch) == 0)
         break;
      lineaDatos[cont] = c;
      cont++;
   }
   lineaDatos[cont] = 0;
   //>>> V.1.01.01 MEML (060711)
   fclose (arch);
   arch = NULL;
   //<<< V.1.01.01 MEML (060711)      
   return true;
}

//######################################################################################################################
bool CConexion::ProcesarRecep(int numMsg, const char* nombMsg, const char* nombProc)
{
   string letrero;
   char codError[5];
   if ( (m_numError = recibe(CERTISAT)) != 0 )
   {
      letrero =  "Error en CConexion en ProcesarRecep: Conexi�n(I): Al recibir mensaje " + string(nombMsg);
      Bitacora ->escribe(BIT_ERROR, m_numError, letrero.c_str());
      return false;
   }
   int msgRec = m_MsgCli.tipOperacion();
   if ( (m_numError = EsMsgCorrecto(msgRec, numMsg)) != 0 )
   {
      if (m_MsgDesc.length() != 0)
      {
         sprintf(codError, "%i", ERR_PROCESO);
         setMsgErr(CERTISAT, m_tipoError, codError, m_MsgDesc.c_str());
         if (envia(CERTISAT) != 0)
         {
            letrero =  "Error en CConexion en ProcesarRecep: Al enviar el mensaje de error del proceso de " + string(nombProc);
            Bitacora->escribe(BIT_ERROR, m_numError, letrero.c_str());
         }
      }
      return false;
   }
   return true;
}

/*######################################################################################################################
   PROPOSITO: Dar seguimiento a un proceso de conexi�n.
   PREMISA:   Se utilizan los recursos de la clase padre CProceso, la conexi�n a la base de datos y la verificaci�n de
              esta son funciones realizadas desde otra clase.
   EFECTO:    Se hacen las validaciones para el proceso de conexi�n.
   ENTRADAS:  NA
   SALIDAS:   Regresa un valor num�rico que indica
              0          = operaci�n con �xito
              Otro valor = c�digo de Error
######################################################################################################################*/
bool CConexion::Proceso()
{
   //Recibe primer mensaje de conexion
   if (!ProcesarRecep(SOLCONEXION, "SOLCONEXION", "Conexi�n"))
      return false;
   if ( (m_nivelAgC != -1) && (m_nivelAgC != 6) )
   {
      if (!ProcesarRecep(SOLTPOCERT, "SOLTPOCERT", "Solicitud de la Lista de Tipos de Certificados"))
         return false;
   }
   return true;
}
// RORS  5 dic 2008
int CConexion::
VerifAgCLDAP(std::string sUsuarioAgente, std::string sPwdAgente)
{
    int iError;

    string sIPLDAP, sPuertoLDAP, sUsuarioLDAP, sPasswordLDAP, sRutaBaseLDAP, sRutaBaseUsuLDAP;

    iError = ObtieneConfiguracionLDAP(sIPLDAP, sPuertoLDAP, sUsuarioLDAP, sPasswordLDAP, sRutaBaseLDAP, sRutaBaseUsuLDAP);
    if( !iError ) 
    {
       Sgi_LDAP ldap(*Bitacora, sRutaBaseUsuLDAP);
       
       ldap.setFiltro("cn");

       sUsuarioAgente = m_agcCve;

       int iPuerto = atoi(sPuertoLDAP.c_str());
       Bitacora->escribePV(BIT_DEBUG,"CConexion en VerifAgCLDAP: Usuario Agente [ %s ] ", (char*)sUsuarioAgente.c_str());
       Bitacora->escribePV(BIT_DEBUG,"CConexion en VerifAgCLDAP: Usuario LDAP   [ %s ] ", (char*)sUsuarioLDAP.c_str());

       bool bCone = ldap.ConexionAgente( sIPLDAP, iPuerto, sUsuarioLDAP, sPasswordLDAP,
                                 sUsuarioAgente, sPwdAgente);
       if ( bCone )
       { 
          Bitacora->escribePV(BIT_INFO,"CConexion en VerifAgCLDAP: Conexion Exitosa LDAP %s",m_agcCve);
          ldap.Desconexion();
       }
       else
       {
          iError = ldap.getError();
          Bitacora->escribePV(BIT_ERROR,"Error en CConexion en VerifAgCLDAP: Error [%d] en la autenticacion LDAP Usuario  [ %s ] ", iError, 
          (char*)sUsuarioAgente.c_str());
          Bitacora->escribePV(BIT_ERROR,"CConexion en VerifAgCLDAP: Error [%d] en la autenticacion LDAP m_agcCve [ %s ] ", iError, m_agcCve);
          if ( iError == LDAP_INVALID_CREDENTIALS)
             Bitacora->escribePV(BIT_ERROR,"CConexion en VerifAgCLDAP: El password es incorrecto");
       
       }
    } 
    return iError;
}

int CConexion::
ObtieneConfiguracionLDAP(string& sIPLDAP, string& sPuertoLDAP, string& sUsuarioLDAP, string &sPasswordLDAP,string& sRutaBaseLDAP,string& sRutaBaseUsuLDAP)
{
   int iError ;
   std::string sClaves [] = { "IP","PUERTO","USUARIO_EMP","PASSWORD_EMP","RUTA_BASE","RUTA_BASE_USU" };
   std::string sPassAux;
   char cPwdDes[30];
   int  iPwdDes = sizeof(cPwdDes);

   m_Configuracion.cargaCfgVars();
   iError = m_Configuracion.getValoresConf("[CONEXION_LDAP]",6, sClaves, &sIPLDAP, &sPuertoLDAP, &sUsuarioLDAP, &sPassAux,&sRutaBaseLDAP,&sRutaBaseUsuLDAP);
   if (!iError)
   {
      desencripta((uint8*)sPassAux.c_str(),sPassAux.length(),(uint8*)cPwdDes,&iPwdDes);
      cPwdDes[iPwdDes] = 0;
      sPasswordLDAP = cPwdDes;
      iError = EXITO;
   }
   return iError;
}



//CLASE DE LA DESCONEXION
//######################################################################################################################
//Declaraci�n de los CONSTRUCTORES
CDesconecta::CDesconecta(CConfigFile& config, MensajesSAT& msgcli, CSSL_Skt* skt):
   COperacion(config, msgcli, skt)
{
      //Inicializacion de variables
      m_numError = 0;
}

//######################################################################################################################
//Declaraci�n del Destructor
CDesconecta::~CDesconecta()
{
      //Libera los apuntadores que se hayan creado
}

//######################################################################################################################
int CDesconecta::ObtenNivelAGC()
{
   int nivelObt;
   intE ierror;

   if ( !m_BDAR->consultaReg(CON_OBTEN_NIVEL_AGENTE_POR_CLAVE, m_agcCve) )
   {
      Bitacora->escribe(BIT_ERROR, "Error en CDesconecta en ObtenNivelAGC: No se ejecut� correctamente la consulta del nivel del AGC");
      return -1;
   }
   if ( !(ierror = m_BDAR->getValores("i", &nivelObt) ) )
   {
      Bitacora ->escribePV(BIT_ERROR, "Error en CDesconecta en ObtenNivelAGC: No se obtuvo correctamente el nivel del AGC");
      return -1;
   }
   return nivelObt;
}

//######################################################################################################################
bool CDesconecta::Proceso()
{
   int resultado;
   int nivelAgC = ObtenNivelAGC();
   if ( (nivelAgC == 6) || (nivelAgC < 0) ) //Conexi�n del CertiSATWeb
   {
      if (!m_utilBDAR->RegOperDetalleBDAR(DESCONEXION, m_numOp))
         Bitacora->escribe(BIT_ERROR, "Error en CDesconecta en Proceso: Al registrar DESCONEXION en el detalle de operaci�n");
      return true;
   }

   bool ok_ejecuta = false;

   #if ORACLE
      ok_ejecuta = m_BDAR->ejecutaFuncion(CON_SP_DESCONEXION_V2_NOMBRE, CON_SP_DESCONEXION_V2_RETURN, CON_SP_DESCONEXION_V2_PARAMS, m_agcCve, m_IDProceso);
   #else
      ok_ejecuta = m_BDAR->ejecutaSP(CON_SP_DESCONEXION_V2_NOMBRE, CON_SP_DESCONEXION_V2_PARAMS, m_agcCve, m_IDProceso);
   #endif

   if ( !ok_ejecuta )
   {
      Bitacora->escribe(BIT_ERROR, "Error en CDesconecta en Proceso: Al ejecutar en la BD el borrado del registro de la conexi�n");
      return false;
   }
   //Verifica el resultado obtenido
   if ( !(m_BDAR->getValores("i", &resultado)) )
   {
      Bitacora->escribe(BIT_ERROR, "Error en CDesconecta en Proceso: Al obtener el resultado de eliminar el registro de la conexi�n");
      return false;
   }
   if ( resultado!= 0 ) //Obtiene la descripcion del error de operaci�n
   {
      Bitacora->escribe( BIT_ERROR, m_numError, "Error en CDesconecta en Proceso: %s, al leer el resultado del borrado del registro"
                                                " de conexi�n"  );
   }
   if ( (m_numError =  m_MsgCli.setMensaje(DESCONEXION)) != 0 )
   {
       Bitacora->escribe(BIT_ERROR, "Error en CDesconecta en Proceso: Se present� al generar el mensaje de DESCONEXION");
       return false ;
   }
   if ( (m_numError = envia(CERTISAT)) != 0 )
   {
      Bitacora->escribe(BIT_ERROR, "Error en CDesconecta en Proceso: Al enviar el mensaje de DESCONEXION al CERTISAT");
      return false;
   }
   if (!m_utilBDAR->RegOperDetalleBDAR(DESCONEXION, m_numOp))
      Bitacora->escribe(BIT_ERROR, "Error en CDesconecta en Proceso: Al registrar DESCONEXION de detalla en la base de datos de AR");
   return false;
}
//######################################################################################################################
//>>> V.1.01.01 MEML (060711)
// Procesa desconexi�n por perdida de comunicaci�n con el cliente
bool CDesconecta::ProcesoXError()
{  
   int  resultado = 0;

   int base_datos = AR_BD;

   #if ORACLE
      base_datos = AR_BD_ORACLE;
   #endif
   
   if (!conectaBD(base_datos))
      return false;
   
   int nivelAgC = ObtenNivelAGC();
   if ( (nivelAgC != 6) && (nivelAgC >= 0) )
   {  
      bool ok_ejecuta = false;

      #if ORACLE
         ok_ejecuta = m_BDAR->ejecutaFuncion(CON_SP_DESCONEXION_V2_NOMBRE, CON_SP_DESCONEXION_V2_RETURN, CON_SP_DESCONEXION_V2_PARAMS, m_agcCve, m_IDProceso);
      #else
         ok_ejecuta = m_BDAR->ejecutaSP(CON_SP_DESCONEXION_V2_NOMBRE, CON_SP_DESCONEXION_V2_PARAMS, m_agcCve, m_IDProceso);
      #endif

      if ( !ok_ejecuta || !(m_BDAR->getValores("i", &resultado)) || resultado != 0 )
         Bitacora->escribePV(BIT_ERROR, "Error en CDesconecta en ProcesoXError: m_agcCve=%s, resultado=%d", m_agcCve, resultado );
      else 
         Bitacora->escribePV(BIT_INFO, "CDesconecta en ProcesoXError: Desconexion correcta del AGC=%s en la DB por error desconocido.", m_agcCve);         
   }
   desconectaBD(base_datos);
   return true;
} 
//<<< V.1.01.01 MEML (060711)
//######################################################################################################################
//######################################################################################################################
//CLASE DE SOLICITUD DE MOTIVOS DE REVOCACION 
//######################################################################################################################
//Declaraci�n de los CONSTRUCTORES
CSolMotivRev::CSolMotivRev(CConfigFile& config, MensajesSAT& msgcli, CSSL_Skt* skt):
   COperacion(config, msgcli, skt)
{

}

//######################################################################################################################
//Declaraci�n del Destructor
CSolMotivRev::~CSolMotivRev()
{
   //Libera los apuntadores que se hayan creado
}

//######################################################################################################################
bool CSolMotivRev::ObtListaMot(string *lista_motivo)
{
   string motivo;
   char indice[5];
   //>>> ERGL (070130) RELATIVO AL ACUSE DE REVOCACION.
   //>-
   //if ( !m_BDAR->consulta("SELECT motrev_cve, motrev_desc FROM cat_motivo_revoca") )
   //>+
   if( !m_BDAR->consulta(CON_OBTEN_DATOS_MOTIVOS_DE_REVOCACION_MOSTRADOS) )
      return false;
   //<<<
   while( m_BDAR->sigReg() )
   {
      if(!m_BDAR->getValores("bs", indice, &motivo))
         return false;
      *lista_motivo = *lista_motivo + indice + "|" +  motivo + "|";
   }
   return true;
}

//######################################################################################################################
bool CSolMotivRev::Proceso()
{
   string lista;

   if ( ObtListaMot(&lista) )
   {
      if((m_numError = this->m_MsgCli.setMensaje(LISTAMOTIV, lista.c_str(), lista.length())) == 0)
      {
         if( (m_numError = envia(CERTISAT)) == 0 )
         {
            if (!m_utilBDAR->RegOperDetalleBDAR(LISTAMOTIV, m_numOp))
                Bitacora->escribe(BIT_ERROR, "Error en CSolMotivRev en Proceso: Al registrar LISTAMOTIV en el detalle de operaci�n");
            return true;
         }
         else
            Bitacora->escribe(BIT_ERROR, m_numError ,"Error en CSolMotivRev en Proceso: Al enviar mensaje a cliente");
      }
      else
         Bitacora->escribe(BIT_ERROR, m_numError, "Error en CSolMotivRev en Proceso: Al establecer el mensaje LISTAMOTIV");
   }
   else
      Bitacora->escribe(BIT_ERROR, "Error en CSolMotivRev en Proceso: Al obtener la lista de motivos de revocacion");
   return false;
}

