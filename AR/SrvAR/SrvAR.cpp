static const char* _SRVAR_CPP_VERSION_ ATR_USED = "@(#) SrvAR ( L : DSIC10392AR_ : SrvAR.cpp : 1.1.2 : 3 : 28/09/10)";

//#VERSION: 1.1.1
/*##############################################################################
  ###  PROYECTO:              PKI-SAT                                        ###
  ###  MODULO:                SrvAR    Servidor de la AR                     ###
  ###                                                                        ###
  ###  DESARROLLADORES:       H�ctor Ornelas Arciga           HOA            ###
  ###                         Amilcar Guerrero Zu�iga         AGZ            ###
  ###                         Gudelia Hern�ndez Molina        GHM            ###
  ###                         Miguel Angel Mendoza L�pez      MAML           ###
  ###                         Roc�o Alejandra Mart�nez Olayo  RAMO           ###
  ###                         Silvia Eur�dice Rocha Reyes     SERR           ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       Martes 6, diciembre del 2005                   ###
  ###                                                                        ###
  ##############################################################################
         1         2         3         4         5         6         7         8
12345678901234567890123456789012345678901234567890123456789012345678901234567890
*/
/*#####################################################################################################################
   VERSION:
      V.02.00      (20051206 - 20060225) HOA: 2a versi�n: reconstrucci�n total del servicio
                                M�dulo de control, recibe las solicitudes de las
                                aplicaciones clientes CertiSAT (ventanilla y Web)
      V.02.01      (20060706 - 20060711) HOA: Modificaciones de comportamiento:
             .01                - Se agregan manejadores de se�ales para una correcta terminaci�n de la aplicaci�n
             .02                - Se agrega c�digo para quitar el registro de conexi�n en la BD en caso de problemas al
                                   recibir peticiones del cliente
             .03                GHM (081112): Se agrega el uso de IDC-WS
             .04                GHM (081118): Se establece el nivel de bitacora por archivo de configuraci�n
             .05                GHM (081121): Se ajusta para leer del archivo de configuraci�n la URL del IDC-WS
####################################################################################################################*/

//>>> V.02.01.01 HOA (060706)
#include <signal.h>
//<<< V.02.01.01

//>>> V.02.01.03 GHM (081112)
#ifdef WS_EXTERNO
   #include "IdCSOAP.nsmap"
#else
   #include "IdCInternoSOAP.nsmap"
#endif
//<<< V.02.01.03

#include <string>

#include <SrvAR.h>
//#include <CConfigFile.h>
#include <Sgi_BD.h>
#include <CAcuses.h>
//#include <CSeparaToken.h>     //+ V.1.01.03 GHM (081028)
#include <Sgi_PKI.h>  // Se cambia CSeparaToken.h por Sgi_PKI
#include <CConexion.h>
#include <CRecuperaAcuse.h>
#include <CRecuperaCert.h>
#include <CRevocacion.h>
#include <CEdoTramite.h>
#include <CSolDatosCert.h>
#include <CConsultaCert.h>
#include <CSolCert.h>
#include <CArchivoEnsob.h>
#include <CRenovacion.h>
#include <CSolDatosRFC.h>
//- #include <ARunZip.h>  //- GHM (070430)
//- #include <ARinZip.h>  //- GHM (070430)
#include <Sgi_Zip.h>      //+ GHM (070430)
#include <CSellosDigs.h>
#include <CLogin.h>

//#################################################################################
#define PATH_CONF            "/usr/local/SAT/PKI/etc/"
#define PATH_DBIT            "/var/local/SAT/PKI/"

#ifdef DBG
   #define ARCH_CONF            PATH_CONF "SrvAR_dbg.cfg"
   #define PATH_BITA            PATH_DBIT "SrvAR_dbg.log"
#else
   #define ARCH_CONF            PATH_CONF "SrvAR.cfg"
   #define PATH_BITA            PATH_DBIT "SrvAR.log"
#endif

//################################################################################
//   Variables globales
//################################################################################

CSrvAR* ptrSrvAR = NULL;
static string  pSSL;
string m_URLWS; //+ V.02.01.05 GHM (081121)

//################################################################################
//>>> V.02.01.01 HOA (060706)
namespace nsSrvAR
{
   bool Operar = true; // true == opera normalmente, false == ya no se aceptan m�s operaciones y sale
}
//<<< V.02.01.01 HOA (060706)


//################################################################################
// Creaci�n del objeto de Bitacora
//################################################################################
CBitacora* Crea_CBitacora()
{
   #ifdef DBG_NO_SRV
      #ifdef PUERTO
         DBG_NS_Puerto = PUERTO;
      #else
         DBG_NS_Puerto = 4001;
      #endif
   #endif
   return new CBitacora(PATH_BITA);
}
//################################################################################
// Creaci�n del objeto principal para el servicio de la AR
//################################################################################
CSrvUnx* Crea_CSrvUnx()
{
   return ptrSrvAR = new CSrvAR;
}
//################################################################################
bool libSrvUnx_Ini()
{
   #if defined( DBG ) && !defined( DBG_NO_SRV )
      sleep(15);
   #endif
   #if defined( DBG ) 
      Bitacora->setNivel(BIT_DEBUG);
   #endif
   iniciaLibOpenSSL();
   return true;
}
//################################################################################
bool libSrvUnx_Fin()
{
   terminaLibOpenSSL();
   return true;
}
//################################################################################
//>>> V.02.01.01 HOA (060706)
void finProceso(int senal)
{
   if (senal == SIGINT  ||
       senal == SIGQUIT ||
       senal == SIGTERM )
      nsSrvAR::Operar = false;         
}
//<<< V.02.01.01
//################################################################################
static int verificacionSSL(int ok, X509_STORE_CTX *store)
{
   char data[256];
   
   if (!ok)
   {
      X509 *cert = X509_STORE_CTX_get_current_cert(store);
      int depth  = X509_STORE_CTX_get_error_depth(store);
      int err    = X509_STORE_CTX_get_error(store);

      Bitacora->escribePV(BIT_ERROR, "Error en CSrvAR: Error con el certificado %i", depth);
      X509_NAME_oneline(X509_get_issuer_name(cert), data, sizeof(data));
      Bitacora->escribePV(BIT_ERROR, "Error en CSrvAR: \tissuer  = %s", data);
      X509_NAME_oneline(X509_get_subject_name(cert), data, sizeof(data));
      Bitacora->escribePV(BIT_ERROR, "Error en CSrvAR: \tsubject = %s", data);
      Bitacora->escribePV(BIT_ERROR, "Error en CSrvAR: error = %i: %s", err, X509_verify_cert_error_string(err));
   }
   return ok;
}

static int passwordSSL(char *buf, int size, int rwflag, void *password)
{
   if (((string::size_type) size) < pSSL.size())
   {
      Bitacora->escribePV(BIT_ERROR, "Error en CSrvAR: Error al asignar el password de SSL, buffer menor del requerido(%d -> %d)", 
                          size, pSSL.size());

      return 0;
   }
  
   if (!desencripta((uint8*) pSSL.c_str(), pSSL.size(), (uint8*) buf, &size))
   {
      Bitacora->escribe(BIT_ERROR, "Error en CSrvAR: Error al desencriptar  el password de SSL"); 
      return 0;
   }
   
   return size;
}

//################################################################################               
//################################################################################
//################################################################################
                        
CSrvAR::CSrvAR()
   : Configuracion(ARCH_CONF),
     MsgCli()
{
}
//################################################################################
CSrvAR::~CSrvAR()
{
}
//################################################################################
#ifdef DBG
   #define ESPERA  45
#else
   #define ESPERA  30 
#endif
bool CSrvAR::SetParmsSktProc(CSSL_parms& parms)
{
   int    r1, r2, r3, r4, r5;
   string ac, acPath, cert, privK;

   r1 = Configuracion.getValorVar("[SSL_SRV]", "AC"    , &ac);
   r2 = Configuracion.getValorVar("[SSL_SRV]", "ACPATH", &acPath);
   r3 = Configuracion.getValorVar("[SSL_SRV]", "CERT"  , &cert);
   r4 = Configuracion.getValorVar("[SSL_SRV]", "PRIVK" , &privK);
   r5 = Configuracion.getValorVar("[SSL_SRV]", "PWDPK" , &pSSL);
   //>>> V.02.01.04 GHM (081118): Se obtiene el nivel de bit�cora
   string nvlBitac;
   int r6 = Configuracion.getValorVar("[BITACORA]", "NIVELBIT" , &nvlBitac);
   //<<< V.02.01.04
   //>>> V.02.01.05 GHM (081121): Se obtiene la URL del archivo de configuraci�n
   int r7 = Configuracion.getValorVar("[WEB_SERVICE_IDC]", "URLWS" , &m_URLWS); 
   //<<< V.02.01.05 GHM (081121)
   if (r1 || r2 || r3 || r4 || r5 /*+ V.02.00.04*/ || r6 /*+ V.02.00.05*/ || r7)
   {
     //- Bitacora->escribePV(BIT_ERROR, "Error en CSrvAR: Error al leer los par�metros de configuraci�n SSL_SRV (%d,%d,%d,%d,%d)",
     //       r1, r2, r3, r4, r5 );
      Bitacora->escribePV(BIT_ERROR, "Error en CSrvAR: Error al leer los par�metros de configuraci�n SSL_SRV (%d,%d,%d,%d,%d), BIT (%d), URL-IDCWS (%d)", 
            r1, r2, r3, r4, r5 /*+ V.02.00.04*/, r6 /*+ V.02.00.05*/, r7);
      return false;
   }
   //>>> GHM (081112): Se agrega para contar con informaci�n si el nivel de bit�cora es DBG
   Bitacora->escribePV(BIT_DEBUG, "De archivo de configuraci�n se obtuvo: AC(%s), ACPATH (%s), CERT(%s), PRIVK(%s), "
                                  " NivelBit(%s) y URL-IDCWS (%s)",
                                  ac.c_str(), acPath.c_str(), cert.c_str(), privK.c_str(), nvlBitac.c_str(), m_URLWS.c_str() );
   BIT_NIVEL iNvlBitac = (BIT_NIVEL) atoi(nvlBitac.c_str());
   //<<< GHM
   //>>> V.02.01.04 GHM (081118): Se establece el nivel de bit�cora
   if (Bitacora)
      Bitacora->setNivel( iNvlBitac);
   //<<< V.02.01.04 GHM 
   struct timeval espera = {ESPERA, 0};
   parms.SetServicio(ESSL_simple, &espera, ac.c_str(), acPath.c_str(), cert.c_str(), privK.c_str(), 
                                 verificacionSSL, passwordSSL);
   return true;
}
//********************************************************************************
bool CSrvAR::envOperInv()
{
   char errorn[128];
   sprintf(errorn, "Error en CSrvAR en envOperInv: Operaci�n invalida (%d)",  MsgCli.tipOperacion());
   MsgCli.setMensaje(MSGERROR, "I", 1, "0", 1, errorn, strlen(errorn)); 
   SktProc->Envia(MsgCli.buffer, MsgCli.tamMensaje()); 
   Bitacora->escribe(BIT_ERROR, errorn);

   return true;
}
//################################################################################
bool CSrvAR::Inicia()
{
   //>>> V.02.01.01 HOA (060706)
   signal(SIGINT , finProceso);
   signal(SIGQUIT, finProceso);
   signal(SIGTERM, finProceso);
   //<<< V.02.01.01
   
   // Cargar archivo de configuraci�n
   int error = Configuracion.cargaCfgVars();
   if (error)
      Bitacora->escribePV(BIT_ERROR, "Error en CSrvAR en Inicia: Problemas al leer el archivo de configuraci�n (%d): %s", error, ARCH_CONF);
   
   return !error;
}
//################################################################################
bool CSrvAR::InicioDeSesion()
{
   bool ok = false;
   CConexion* conexion = new CConexion(Configuracion, MsgCli, SktProc);
   if (conexion)
   {
      ok = conexion->Operacion();
      delete conexion;
   }
   return ok;
}

//################################################################################
bool CSrvAR::Operaciones()
{
   intE error;
   //int lSktBufCli; //>- ERGL mi� sep 12 16:41:31 CDT 2007

//>>> V.02.01.01 HOA (060707)
//>-   for (;;)
   for (;nsSrvAR::Operar;)  //<+ Sale del ciclo cuando Operar es establecido a falso en el manejador de se�ales
//<<< V.02.01.01 HOA (060707)      
   {
      /*>- ERGL mi� sep 12 16:41:59 CDT 2007
      lSktBufCli = sizeof(MsgCli.buffer);
      error = SktProc->Recibe(MsgCli.buffer, lSktBufCli);
      */
      error = MsgCli.Recibe(SktProc);
      if (!error)
      {
         COperacion *operacion = NULL;
         int oper = MsgCli.tipOperacion();
         switch (oper)
         {
            Bitacora->escribePV(BIT_DEBUG, "MsgCli.tipOperacion(): [%d]", oper);
            case VALIDARLOGIN   : operacion = new CLogin(Configuracion, MsgCli, SktProc); break;
            case SOLMOTIVREV    : operacion = new CSolMotivRev   (Configuracion, MsgCli, SktProc); break;
            case SOLDATOSRFC    : 
            case SOLDATOSRFCRL  : operacion = new CSolDatosRFC   (Configuracion, MsgCli, SktProc); break;
            case SOLEDOTRAM     : operacion = new CEdoTramite    (Configuracion, MsgCli, SktProc); break;
            case RECUPERACERT   : operacion = new CRecuperaCert  (Configuracion, MsgCli, SktProc); break;
            case SOLDATOSCERT   : operacion = new CSolDatosCert  (Configuracion, MsgCli, SktProc); break;
            case SOLCERT        : operacion = new CSolCert       (Configuracion, MsgCli, SktProc); break;
            case SOLREN         : operacion = new CRenovacion    (Configuracion, MsgCli, SktProc); break;
            case INICIOTRANSSDG : operacion = new CSellosDigs    (Configuracion, MsgCli, SktProc); break;
            case SOLREV         :
            case SOLREVOF       : operacion = new CRevocacion    (Configuracion, MsgCli, SktProc); break;
            case VERCERTNOSER   :
            case VERCERTRFC     : operacion = new CConsultaCert  (Configuracion, MsgCli, SktProc); break;
            case RECACUSE       : operacion = new CRecuperaAcuse (Configuracion, MsgCli, SktProc); break;
            case SOLDESCONEXION : operacion = new CDesconecta    (Configuracion, MsgCli, SktProc); break;
            default : envOperInv(); continue;
         }
         if (!operacion)
            Bitacora->escribePV(BIT_ERROR, "Error en CSrvAR en Operaciones: Error al generar el objeto de operaci�n %d (errno = %d)", oper, errno);
         else 
         {
            operacion->Operacion();
            delete operacion;
            if (oper == SOLDESCONEXION)
               return false;
         }
         
      }
      else
      {
         if (sgiErrorBase(error) != ERR_SKTTIMEOUT)
         {
            Bitacora->escribe(BIT_ERROR, error, "Error en CSrvAR en Operaciones: Error al recibir solicitud de operaci�n");
            break;
         }
         struct timespec ts = { 0, 1000000};
         nanosleep(&ts, NULL);
      }
   }
   //>>> V.02.01.02 MELM (060711): 
   DesconectaAGC();
   //<<< V.02.01.02 MELM (060711)
   
   return !error;         
}
//################################################################################
bool CSrvAR::Proceso()
{
   bool ok;
	   
   ok = InicioDeSesion() &&
        Operaciones();
	      
   return ok;
}
//################################################################################
//>>>  V.02.01.02 MELM (060711)
void CSrvAR::DesconectaAGC()
{
   CDesconecta *operacion = new CDesconecta(Configuracion, MsgCli, SktProc);
   if (operacion)
   {
      operacion->ProcesoXError();
      delete operacion;
   }
}
//<<<  V.02.01.02 MELM (060711)
//################################################################################

//VARIABLE GLOBAL
std::string strEjecutable;   // Nombre del ejecutable del servidor AR
int         iPuerto;       // Numero de puerto q esta en los parametros del archivo /etc/xinetd.d/*

bool CSrvAR::ProcesaParametros(int argc, char* argv[], char* env[])
{  /*
   #if defined(DBG) && !defined(DBG_NO_SRV)
      sleep(15);
   #endif
   int opcion; */

   // MAML 19/Mar: Guardamos el nombre del ejecutable en esta variable
   //              solo trae el nombre del ejecutable
   strEjecutable = "(" + std::string(argv[0]) + ")";
   iPuerto = 2001; // El puerto por default es est�
   if ( argc == 2 && ( iPuerto = atoi(argv[1]) ) < 0 )
   { 
      cout << "ERROR: el numero de puerto en el archivo /etc/xinetd/SrvAR... es inv�lido: " << argv[1] <<endl;
      return false; 
   }
   return true;
}

//################################################################################
//################################################################################
//################################################################################

