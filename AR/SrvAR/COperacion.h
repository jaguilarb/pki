/*##############################################################################
  ###  PROYECTO:              PKI-SAT                                        ###
  ###  MODULO:                Servicio AR                                    ###
  ###                                                                        ###
  ###                                                                        ###
  ###  DESARROLLADORES:       Amilcar Guerrero Zu�iga         AGZ            ###
  ###                                                                        ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       Miercoles 30, noviembre del 2005               ###
  ###                                                                        ###
  ##############################################################################
         1         2         3         4         5         6         7         8
12345678901234567890123456789012345678901234567890123456789012345678901234567890

################################################################################
  VERSION:
     V.1.0.0  (20051130- )    AGZ: Version Original
                              Servicio de Autoridad Registradora para la PKI-SAT
     V.1.1.3  (20141014 -         ) JAB: Se agrupan las sentencias SQL utilizadas en el SrvAR en un archivo de
                                              cabecera
                              
#################################################################################*/
#ifndef _COPERACION_H_
#define _COPERACION_H_
static const char* _COPERACION_H_VERSION_ ATR_USED = "SrvAR @(#)"\
"DSIC09041AR_ 2007-12-13 COperacion.h 1.1.1/3";

//#VERSION: 1.1.1

#include <unistd.h>

#include <iostream>
#include <vector>
#include <string>

//- #include <MensajesSAT.h>  //- GHM (070427)
#include <Sgi_MsgPKI.h>       //+ GHM (070427)
#include <Sgi_Semaforo.h>
#include <CFifoArMed.h>
#include <Sgi_BD.h>
//- #include <CConfigFile.h>  //- GHM (070427)
#include <Sgi_ConfigFile.h>   //+ GHM (070427)

#include <CUtilBDAR.h>

#if ORACLE
   #include <CosltSrvAR_ORA.h>   // + JAB (20141014)
#else
   #include <mitypes.h>
   #include <CosltSrvAR.h>   // + JAB (20141014)
#endif

using namespace std;
//################################################################################
typedef enum 
{
   AR_MEDIADOR = 0,
   CERTISAT    = 1
} DST_MSG;

#define AR_BD         0  // MAML 080122: solo quedo apuntar a la DB de la AR 
#define AR_BD_ORACLE  1  // JAB 20141028: DB Oracle de la AR

#define TAG_MSG_MED             "[LLAVES_MENSAJES_SRV]"

#define CIERRA_CLIENTE			-500

//-- Estos Defines de Error deben ir en Defdatos.h
#define ERR_NO_ASG_MEM_OBJ_MEN		200
#define ERR_NO_ASG_MEM_OBJ_FIFO		201
//#define ERR_NOT_CONN_BD_ 	      202
#define ERR_NOT_CONN_BD_AR		      203
#define ERR_NOT_ROLE_BD_AR		      204
#define ERR_NO_CREO_FIFO            205
#define ERR_NO_ESCRIBE_FIFO         206
#define ERR_ID_BD_NOT_EXIST         207
#define ERR_NO_SET_DATOS_MSG        208
#define ERR_NO_ENVIA_SKT            209
#define ERR_OPC_DEST_NO_RECON       210
#define ERR_NO_RECIBE_SKT           211
#define ERR_OPC_ORIGEN_NO_RECON     212
#define ERR_FIFO_MED_INI		      213
#define ERR_ASG_MEM_VAR			      214
#define ERR_CREA_ERROR			      215
#define ERR_BD_REG_OPER_SEG		   216
#define ERR_NO_ENVIA_CSAT		      217
#define ERR_NO_INICIA_VARS_MED		218
#define ERR_EDO_AGC			         219
#define ERR_EDO_CERT_AGC		      220
#define ERR_GEN_NUM_OPER		      220
//--


/*##############################################################################
   CLASE    : COperacion
   PROPOSITO: Esta clase se utiliza como clase base para todas las clases que
              integran el servicio de AR de la PKI del SAT.

#################################################################################*/
class COperacion
{
   //private:
      //int getVarConBD(const char* sz_id, string* s_srv, string* s_db, string* s_usr, string* s_pswd, string* s_role);
   protected:
      int getVarConBD(const char* sz_id, string* s_srv, string* s_db, string* s_usr, string* s_pswd, string* s_role);
      int getVarConBDOracle(const char* sz_id, string* s_host, string* s_pto, string* s_sid, string* s_usr, string* s_pswd);
      //<<<AGZ: Variables para la generacion del numero de tramites.
      //<<<     cada tramite debe establecerlas si las necesita y liberarlas.
      char  *m_firmaOp, *m_rfcOp, *m_cadOriOp, *m_refArchOp, *m_secOp;
      int    m_ifirmaOp, m_irfcOp, m_icadOriOp, m_irefArchOp, m_isecOp; 
      //>>>AGZ: Fin
      static char   m_agcCve[9];
      char   m_agcMod[4];
      CBD          *m_BDAR;
      CUtilBDAR    *m_utilBDAR;
      MensajesSAT  *m_msgMed; 
      CFifoArMed   *m_fifoMed; 
      char          m_numOp[13];
      char          m_numTram[13];
      bool          m_tramite;

      CConfigFile   &m_Configuracion;
      MensajesSAT   &m_MsgCli;
      CSSL_Skt      *m_SktProc;

      intE escribeBitacora(int i_apl, int i_cdgErr, const char *psz_descripcion);
      int  iniciaMed(char *psz_cerDestino, char *psz_cerOrigen, char *psz_keyOrigen, char *psz_passOrigen);
      void terminaMed(void);
      bool conectaBD(int id_db);      
      void desconectaBD(int i_db);
      int  envia(DST_MSG i_destino);
      int  recibe(DST_MSG i_origen);
      int  getMsgErr(DST_MSG origen, char& psz_tipError, char *psz_cdgErr, int i_cdgErr, char *psz_desc, int i_desc);
      int  setMsgErr(DST_MSG destino, char psz_tipError, const char *psz_cdgErr, const char *psz_desc);
      bool genOperacion();
      bool genTramite();
      bool envTramite();
      bool procesaPassword(const string& s_pwdEnc, string *s_pwdDes);
      int  iniciaVarsTram(void);
      void terminaVarsTram(void);
      bool msgCliError(char c_tipErr, const char* psz_msg, int i_error, eIdLib i_apl);
      int  iniMed();
      
      //Para los errores en la Bitacora con nombre de la clase o sin nombre.
      const char* SeparaNombreClase(const char *QuitarClase, int NumeroQuitarClase);

      virtual bool preProceso() = 0; 
      virtual bool Proceso() = 0;
      virtual const char* getNombreOperacion() = 0;
      virtual int getTipOper() = 0;
      
   public:
      
      COperacion(CConfigFile& config, MensajesSAT& msgcli, CSSL_Skt* skt, bool tramite = false);
      virtual ~COperacion();      
      virtual bool Operacion();          
};

#endif //_COPERACION_H_
