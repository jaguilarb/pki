#ifndef _CSEPARATOKEN_H_
#define _CSEPARATOKEN_H_
static const char* _CSEPARATOKEN_H_VERSION_ ATR_USED = "SrvAR @(#)"\
"DSIC10294AR_ 2010-07-22 CSeparaToken.h 1.1.1/2";

//#VERSION: 1.1.0
   
// Suponemos en cadena respuesta el sig. formato: y siempre valor0 != null
// "valor0|valor1|||valor4|valor5|||valor8|"
//#VERSION: 1.1.1: se agrega lencadena y acepta cadenas como: "valor0", "valor0||valor2" ie ya no importa si termina o no en el carater separador

#define MAX_SEPARADORES     50
 
class CSeparaToken{
    int i, lencadena;

public:	
	char *items[MAX_SEPARADORES];

	CSeparaToken(char *, char caracter);
	int getCounter();
	~CSeparaToken();
   void inicia(char *);
};

extern CBitacora* Bitacora;

#endif
