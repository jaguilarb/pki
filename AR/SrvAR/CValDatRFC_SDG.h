#ifndef _CVALDATRFC_SDG_H_
#define _CVALDATRFC_SDG_H_
static const char* _CVALDATRFC_SDG_H_VERSION_ ATR_USED = "SrvAR @(#)"\
"DSIC09041AR_ 2009-01-29 CValDatRFC_SDG.h 1.1.0/2";

//#VERSION: 1.1.0

#include <string.h>
#include <list>
#include <stdio.h>

class CValDatRFC_SDG: public CValidaDatosRFC
{
   protected:

      std::list<int> pListaReg;
      std::string pListaRol;
      std::string pListaOblig;
      std::string pListaActiv;
      std::string m_MsgError;

      void liberaListas();
      bool generaListaReg();
      bool generaListaRol();
      bool generaListaObl();
      bool generaListaAct();

   private:

   public:

      CValDatRFC_SDG(CConfigFile& config, CUtilBDAR *UtilBDAR, int iTipoCert);
      virtual ~CValDatRFC_SDG();
      bool CerObl();
};

#endif