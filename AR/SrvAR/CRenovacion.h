#ifndef _CRENOVACION_H_
#define _CRENOVACION_H_
static const char* _CRENOVACION_H_VERSION_ ATR_USED = "SrvAR @(#)"\
"DSIC09041AR_ 2007-12-13 CRenovacion.h 1.1.1/3";

//#VERSION: 1.1.1


#define NSERIE_ANTERIOR_REG_REVREN 444   // No se puede obtener el numero de Serie anterior p/registralo en la DB
#define ERR_DB_REG_REV_DET         445   // No se pudo actualizar la DB para actulizar la Revocacion en una Renovacion

///////////////////////////////////////////////////////////////////////////////
// Solicita la Renovacion de un certificado
///////////////////////////////////////////////////////////////////////////////

class CRenovacion : public CArchivoEnsob
{
   public:
      CRenovacion(CConfigFile&, MensajesSAT&, CSSL_Skt* );
      ~CRenovacion();

   protected:
      //*** Clases nuevas para redefinir en cada operacion
      virtual bool preProceso();
      virtual bool Proceso();
      virtual const char* getNombreOperacion(){ return "SOLREN"; };
      virtual int getTipOper(){ return SOLREN; };
      virtual int getOperCodSol() {return SOLRENAR;}
      virtual int getOperCodRes() {return CERTRENAR;}
      //***

      //virtual bool valArchEnsCertVigente(const char *numSerie);
      // void SustituyeAmp(char *cadsrc, char *caddes); // P/crear directorios sin '&'

   private:
      char	cArchREQ[MAX_RUTA_ARCH];   // Aqui no hay .Zip seria directamente el arch. de Requerimiento
      char	cArchRenov[MAX_RUTA_ARCH]; // == cArchENS, solo esta cambiado de directorio

      int       get_digArchivoREN(const char *cBufArch, int iBufArch);
      virtual bool msgRecibeArch();        // Recibe el archivo .ren
      bool	DescomprimeReqRenov();
      bool	GeneraRenov();
      bool	genAcuseRenov(CAcuses *pacuse);
      //>+ ERGL (070130) REFERENTE AL ACUSE DE RENOVACION.
      int       regRevDetBD();
};

#endif // _CRENOVACION_H_

