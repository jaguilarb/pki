#ifndef _CREVOCACION_H_
#define _CREVOCACION_H_
/*##############################################################################
  ###  PROYECTO:              PKI-SAT                                        ###
  ###  MODULO:                Servicio AR                                    ###
  ###                                                                        ###
  ###                                                                        ###
  ###  DESARROLLADORES:       Amilcar Guerrero Zu�iga         AGZ            ###
  ###                                                                        ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       Miercoles 14, diciembre del 2005               ###
  ###                                                                        ###
  ##############################################################################
         1         2         3         4         5         6         7         8
12345678901234567890123456789012345678901234567890123456789012345678901234567890

################################################################################
  VERSION:
     V.1.0.0  (20051214- )    AGZ: Version Original
                              Servicio de Autoridad Registradora para la PKI-SAT
                              Clase para el manejo de revocacion de certificado.

#################################################################################*/
static const char* _CREVOCACION_H_VERSION_ ATR_USED = "SrvAR @(#)"\
"DSIC09041AR_ 2007-12-13 CRevocacion.h 1.1.1/3";

//#VERSION: 1.1.1


#include <COperacion.h>
#include <DefDatos.h>

#define ERR_MSG_NO_ESPERADO	-100
#define ERR_BD_REG_OPER_SEG_RV	-101
#define ERR_BD_REG_REV_DET	-102
#define ERR_MENS_MED_NO_ESP	-103
#define ERR_NO_INICIA_VARS_MED_RV	-104
#define ERR_ASG_MEM_VAR_RV		-105
#define ERR_SEP_CAD_RFC		-106
#define ERR_SEP_CAD_AGC		-107
#define ERR_SEP_CAD_MOT		-108
#define ERR_NO_INSERT_REV_DET	-109
#define ERR_CREA_ERROR_RV		-110
#define ERR_NO_ENVIA_MED	-111
#define ERR_REV_AGC		-112
#define ERR_GET_AGCCVE_VAL	-113
#define ERR_GET_AGCCVE		-114
#define ERR_GEN_CADENA		-115
#define ERR_GET_DATOS_ACUSE	-116
#define ERR_GET_DATOS_CO_FIRMA   -117
#define ERR_VALIDA_DATOS_MSG     -118
using namespace std;
class CRevocacion : public COperacion
{
   private:
      string m_cadMed, m_cadOrigAcuse, m_datosImp;
      static int m_iMsgRecib;
      char m_numSerOp[21],m_numSerFIEL[21], m_cveRev[MAX_TAM_CVE_REV], m_cadenaOriginal[256], m_motivCve[14];
      char m_refMov[128], m_NomRL[1024], m_rfcRL[14], m_FirmaCadOrig[MAX_TAM_FIRMA_B64], m_numSerieAR[21];
      char m_cadSolRev[256],m_firmaSolRev[1024];
      char m_rfcRec[14], m_numSerieRev[21], m_numSerieFiel[21];
      int  m_inumSerOp, m_icveRev, m_icadOriOp;
      int  m_irefArchOp, m_icadenaOriginal, m_imotivCve, m_irefMov, m_irfcRL, m_iNomRL ,m_icadSolRev,m_ifirmaSolRev,m_inumSerFIEL;
      uint16 m_iFirmaCadOrig;
      bool revFirma;
      bool revJuridica;
      CBD  *revBD;

      int  getVarsCnxMed(string tag, string *cert, string *key, string *psw, 
                        string *certAd);
      int  regRevDetBD();
      bool respCSATErr(int apl, int err, const char* msg, char medio);
      void iniciaVarsOp();
      int  iniciaCnxMed(string tag);
      const char* getNombreOperacion();
      int  getTipOper();
      bool getLlave(RSA *rsa);
      bool getNombreRL(char *nombre);
      bool genAcuseRevoc(CAcuses *pacuse);
      bool procesaRevocacion();
      bool RevocacionJuridica();
      bool CreaTramite(int iOperacion , char * cNumTramRev ,char *cRfc);
      bool Revocacion();
      bool conexionBDRev();
      string consultaFechaRev();





   public:
      CRevocacion(CConfigFile& config, MensajesSAT& msgcli, CSSL_Skt* skt);
      ~CRevocacion();
      virtual bool preProceso();
      virtual bool Proceso();
};

#endif
