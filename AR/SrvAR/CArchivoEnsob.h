/* ##############################################################################
 **###  PROYECTO:              PKI-SAT                                        ###
 **###  MODULO:                SrvAR	Servidor de la AR                     ###
 **###                         Lee Archivios Ensobretados.                    ###
 **###                                                                        ###
 **###  DESARROLLADORES:       MELM                                           ###
 **###                                                                        ###
 **###  FECHA DE INICIO:       Lunes 23, enero del 2006		              ###
 **###                                                                        ###
 **##############################################################################
 **  1         2         3         4         5         6         7         8
 **12345678901234567890123456789012345678901234567890123456789012345678901234567890
 **
 **/

#ifndef _CARCHIVOENSOB_H_
#define _CARCHIVOENSOB_H_
static const char* _CARCHIVOENSOB_H_VERSION_ ATR_USED = "SrvAR @(#)"\
"DSIC09041AR_ 2007-12-13 CArchivoEnsob.h 1.1.1/3";

//#VERSION: 1.1.1

/* class CArchivoEnsob : Para leer y validar archivos ensobretados, clase base para las 
                         clases de Renovacion y de Solicitud de Sellos Digitales
*/

// Defines para manejar los errores de los archivos ensobretados

#define	MAX_TAM_RECE	(TAM_PATH*8)   // M�ximo tama�o de UN archivo .req y/o .cer
#define	MAX_RUTA_ARCH	256	       // Tama�o de la ruta con el nombre del archivo (rutas cortas en AR)
                                       // � usamos TAM_PATH definida en MensajesSAT.h

// Registro en la tabla ar_cat_proc de la BD de la AR, para mandar un mensaje al Certisat-WEB
//#define ENSPROCESANDO		70  // El Cert Digital usado para ensobretar no es de tipo FEA o no est� activo.

#define	ERR_GET_VALORES_CONF    500
#define	ERR_SAFE_CERT_SELLO     501
#define	ERR_GET_NOMBRE_CONT    	502		
#define	ERR_GET_FECHA_TRAMITE   503
#define	ERR_VAL_ARCH_ENS        504

using namespace std;
// Clase intermedia entre el Tramite y la RENovacion y la Solicitud de Sellos
class CArchivoEnsob : public CSolCert
{
   public:
      CArchivoEnsob(CConfigFile&, MensajesSAT&, CSSL_Skt* );
      ~CArchivoEnsob();

   protected:
      //-- Esta variable debe estar en COperacion p/no repetirla en las clases Hijas
      //   HAY Q REVISAR LAS NUEVAS CLASES PAPA: CSolCert -> CGeneraCert -> COperacion 
      int  error;		        //  en la class CGeneraCert hay una variable: "int m_errCod" 
      //--

      // Para manejar el archivo ensobretado temporal q se coloca en la AR
      int   iFd;
      char  cArchENS[MAX_RUTA_ARCH];
      char	cNombreContrib[1024];	// Es el nombre del contribuyente q esta ejecutando el Tramite
      char	ref_arch[255+1]; 	// Nombre del Archivo Ensobretado, necesario en la cadena Original
      char	dig_archivo[30];	// Digestion del Archivo Ensobretado, necesario en la cadena Original

      //+++ Solo declaro las funciones virtuales para q esta clase siga siendo abstracta
      //+++ o tal vez no sea ni necesario declararlas ??
      virtual bool preProceso() = 0;
      virtual bool Proceso() = 0;	// en la clase padre recibe SOLCERT y hace un RevisaNivelAGC()
      virtual const char* getNombreOperacion() = 0;
      virtual int getTipOper() = 0;

      void cerrarYeliminarArch();
      bool valArchEns();
      bool valArchEnsAutentico(char *cArchDes, string proceso);

      int  leeReq(char *cArchREQ);
      //bool getValoresConf(string sTag, int numClaves, string claves[], ...);
      bool IniciaFin_preProceso();
      char* quitaRutaArch(const char *ref_arch, char *nameArch);
      //void CreaDir(char *directorio); //- GHM (070321): Se elimin� porque esta funci�n esta disponible en la librer�a Sgi_PKI
      int  AlmacenaEns( const char *archENS, const char *ext);
      //virtual bool valArchEnsCertVigente(const char *numSerie);

   private:
      // Se cambio para hacer nuevas validaciones (Que se pueda renovar aunque el certificado este caduco)
      virtual bool valArchEnsCertVigente(const char *numSerie, string metodo);
      int  pkiRutaEns ( const  std::string& repositorio, const string& num_tramite, std::string& rutaEns, const char *ext );

};

#endif // _CARCHIVOENSOB_H_

