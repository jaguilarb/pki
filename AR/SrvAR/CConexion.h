/*######################################################################################################################
  ###  PROYECTO:              PKI-SAT                                                                                ###
  ###  MODULO:                Clase que realiza la Conexi�n al SrvAR                                                 ###
  ###                                                                                                                ###
  ###  DESARROLLADORES:       Gudelia Hern�ndez Molina        GHM                                                    ###
  ###                         H�ctor Ornelas Arciga           HOA                                                    ###
  ###  FECHA DE INICIO:       Martes 06, diciembre del 2005                                                          ###
  ###                                                                                                                ###
  ######################################################################################################################
         1         2         3         4         5         6         7         8         9        10        11        12
123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
*/
/*######################################################################################################################
   VERSION:
      V.1.00      (20051206 - 20060101) GHM: Primera Versi�n
      V.1.01      (20060711 - 20060711) MELM
            .01                          - Se agrega funci�n para concluir una conexi�n si se pierde la comunicaci�n
                                                 con el cliente o se recibe indicaci�n de finalizar el proceso
      V.1.1.2
            .07   (20141014 -         ) JAB: Se agrupan las sentencias SQL utilizadas en el SrvAR en un archivo de
                                              cabecera
                                                 
######################################################################################################################*/

#ifndef _CCONEXION_H_
#define _CCONEXION_H_
static const char* _CCONEXION_H_VERSION_ ATR_USED = "SrvAR @(#)"\
"DSIC09041AR_ 2007-12-13 CConexion.h 1.1.1/3";

//#VERSION: 1.1.1

#include <string.h>       //Manejo de Strings

#include <COperacion.h>     //Clase padre
#include <DefDatos.h>
#include <CUtilBDAR.h>

#if ORACLE
   #include <CosltSrvAR_ORA.h>   // + JAB (20141014)
#else
   #include <CosltSrvAR.h>   // + JAB (20141014)
#endif

// ******************************  DEFINICIONES DE TAMA�OS EXCLUSIVOS DE ESTA CLASE***********************************
#define TAM_NUMALEATORIO       40
#define TAM_NOSERIECERT        21
#define MAX_TAM_IDVER           2
#define TAM_CVEAGC             14   //GHM (081112): Paso de 9 a 14 porque el usuario de AGC de omnigard cambio al rfc del cert de AGC
#define TAM_IP                 20
#define TAM_FECHAS             25
using namespace std;
// ******************************  DEFINICIONES DE LA CLASE  ********************************************************
class CConexion:public COperacion
{
   public:
      //DEFINICION DE VARIABLES Y METODOS PUBLICOS
      CConexion(CConfigFile& config, MensajesSAT& msgcli, CSSL_Skt* skt);
      virtual  ~CConexion();
      bool     Proceso();
   private:
      //DEFINICION DE VARIABLES Y METODOS PRIVADOS
   protected:
      //DEFINICION DE VARIABLES Y METODOS PROTEGIDOS
      string       m_MsgDesc;                         // Describe Error,Situacion, etc
      int          m_numError;                        // N�mero de Error
      char         m_tipoError;                       // Tipo de error 'O'peraci�n � 'P'roceso
      int          m_nivelAgC;                        // Nivel del Agente Certificador
      char         m_numAleatorio[TAM_NUMALEATORIO];  // N�mero Aleatorio
      int          m_lnumAleatorio;                   // Longitud de la variable.
      int          m_version;                         //
 
      CSeparaToken *Tkn; 
      //FUNCIONES DE APOYO AL SERVIDOR 
      virtual const char * getNombreOperacion() {return "CONEXION"; };
      virtual int          getTipOper () {return SOLCONEXION;}; 
      virtual bool         preProceso();
      bool                 EnviaMensaje(char *codError, char *MsgDesc = NULL );
      bool                 DetVerMsg(char *DatoRecib, int lDatoRecib, bool *verAntRec, int *iversion, char *nSerieRec);
      bool                 IniciaMsgSrv(const char* noSerie);

      //FUNCIONES DE CALCULO
      //Genera el n�mero aleatorio que se utilizar� en el protocolo
      int      GenNumeroAleatorio();
      bool     DesencriptaBuffer(char *DatoRecib, int lDatoRecib, char* bufDesenc, int *lbufDesenc);
      bool     ObtenIP(char* IP);
          
      //FUNCIONES PARA EL MANEJO DE ERRORES
      bool     TrataErrorProc(int itpoMsg, const char* sCausa);
               
      //FUNCIONES DE AFECTACION EN LA BD     
      //V�lida el n�mero de serie de AgC recibido contra la BD de la AR 
      bool     VerifVersion(const char* nSerie);
      bool     VerifNumSerieBDAR(const char* nSerie);
      bool     VerifVigencia(const char* nSerie);
      //+V.1.01.06 GHM(081107): Funciones para el despliegue del primer mensaje
      bool     Envia1erMsgBit(char* dato);
      //>>> V.1.01.05 GHM(081107): Funciones de verificaci�n de la conexi�n del cliente
      bool     VerifVersionCliente();
      bool     VerifNumSerCliBDAR(const char* nSerie);
      //<<< V.1.01.05 GHM(081107)
      bool     EnviaSinFirma(const char* Msg, char tipoError, const char* codError, const char* bitac = NULL );
      //Registra el error de operaci�n en la base de datos
      int      ObtenNivelAGC();
      //Registra la conexi�n del AgC
      bool     RegConexionBD();
      bool     ObtListaMot(string *lista_motivo);
      bool     ObtListaCert(string *lista_tipos);
      
      //FUNCIONES PARA EL MANEJO DE MENSAJES
      intE     RutinaRecepc(int *lSktBufCli);
      intE     RutinaEnvio();
      //Verifica que el mensaje recibido sea el esperado
      int      EsMsgCorrecto(int numMsgRcb, int numMsgEsp);
      //Extrae los datos del mensaje recibido y hace las validaciones del flujo de datos
      bool     TrataMensaje(int numMsgRecibido);
      bool     CierraConexPrev(char* fec_inic, char* dirIPAnt, int ID_procAnt, int startTmAnt);
      bool     EnviaErrorIPs(char* fec_inic, char* dirIPAnt, char* dirIPAct);
      bool     delProceso(int ID_procAnt, int startTmAnt);
      int      getStartTm(int ID_proc);
      bool     CargaArchivoMem(const char* dirProcSys, char* lineaDatos, int *tamLineaDat);
      bool     ProcesarRecep(int numMsg, const char* nombMsg, const char* nombProc);
      int      VerifAgCLDAP(std::string sUsuarioAgente, std::string sPwdAgente);
      int      ObtieneConfiguracionLDAP(string& sIPLDAP,string& sPuertoLDAP,string& sUsuarioLDAP,string &sPasswordLDAP,string& sRutaBaseLDAP,string& sRutaBaseUsuLDAP); 

};

class CDesconecta:public COperacion
{
   public:
      //DEFINICION DE VARIABLES Y METODOS PUBLICOS
      CDesconecta(CConfigFile& config, MensajesSAT& msgcli, CSSL_Skt* skt);
      virtual    ~CDesconecta();
      bool       Proceso();
//>>> V.1.01.01 MEML (060711)
      bool       ProcesoXError(); // Procesa desconexi�n por perdida de comunicaci�n con el cliente
//<<< V.1.01.01 MEML (060711)
   protected:
      //DEFINICION DE VARIABLES Y METODOS PROTEGIDOS
      string     m_MsgDesc;                         //Describe Error,Situacion, etc
      int        m_numError;                        //N�mero de Error
      int        ObtenNivelAGC();

      //FUNCIONES DE APOYO AL SERVIDOR
      virtual const char * getNombreOperacion() {return "DESCONEXION"; };
      virtual int          getTipOper () {return SOLDESCONEXION;};
      virtual bool         preProceso() {return true;};
};


class CSolMotivRev:public COperacion
{
   public:
   //DEFINICION DE VARIABLES Y METODOS PUBLICOS
   CSolMotivRev(CConfigFile& config, MensajesSAT& msgcli, CSSL_Skt* skt);
   virtual     ~CSolMotivRev();
   bool        Proceso();

   protected:
   //DEFINICION DE VARIABLES Y METODOS PROTEGIDOS
   string      m_MsgDesc;                         //Describe Error,Situacion, etc
   int         m_numError;                        //N�mero de Error
   bool        ObtListaMot(string *lista_motivo);

   //FUNCIONES DE APOYO AL SERVIDOR
   virtual const char * getNombreOperacion() {return "SOLMOTIVREV"; };
   virtual int          getTipOper () {return SOLMOTIVREV;};
   virtual bool         preProceso() {return true;};
};

#endif // _CCONEXION_H_

