//#VERSION: 1.1.0
/*##############################################################################
  ###  PROYECTO:              PKI-SAT                                        ###
  ###  MODULO:                Servicio AR                                    ###
  ###                                                                        ###
  ###                                                                        ###
  ###  DESARROLLADORES:       Guadalupe Negrete Castro         GNC           ###
  ###                                                                        ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       Viernes 10, octubre del 2014                   ###
  ###                                                                        ###
  ##############################################################################
         1         2         3         4         5         6         7         8
12345678901234567890123456789012345678901234567890123456789012345678901234567890

################################################################################
  VERSION:
     V.1.0.0  (20141010- )    GNC: Version Original
                              Consutas para el Servicio de Autoridad Registradora PKI-SAT
  

#################################################################################*/



//#VERSION: 1.1.0
#ifndef _COSLTSRVAR_ORA_H_
#define _COSLTSRVAR_ORA_H_


// ******************************  SENTENCIAS SQL ACUSES  *********************************
#define ACU_EXISTE_ACUSE_POR_NUM_DE_TRAMITE                          "SELECT 1 FROM SEG_PKI.SEGT_ACUSE WHERE TRAMITECVE = '%s'"
#define ACU_OBTEN_DATOS_ACUSE_POR_NUM_DE_TRAMITE                     "SELECT TIPACUCVE, NOM, CADORIG, FIRMA, TIPCERCVE FROM SEG_PKI.SEGT_ACUSE WHERE TRAMITECVE = '%s'"
#define ACU_AGREGA_ACUSE                                             "INSERT INTO SEG_PKI.SEGT_ACUSE(TRAMITECVE,TIPACUCVE,MODCVE,NOM,LLAVEPUB,RFCSOL,NOMSOL,CADORIG,FIRMA,FECACU,TIPCERCVE) values ('%s',%d,'%s','%s','%s','%s','%s','%s','%s','%s',%d)"
#define ACU_OBTEN_ULT_FECHA_DE_OPERACION_EXITOSA                     "SELECT * FROM (SELECT FECOP  FROM SEG_PKI.SEGT_OPERACION WHERE SOLOPERCVE = '%s' AND PROCCVE = %d ORDER BY FECOP DESC) WHERE ROWNUM = 1"
#define ACU_OBTEN_CAMPO_X_DE_TABLA_X_POR_CAMPO_X                     "SELECT %s FROM %s WHERE %s = %s"
#define ACU_OBTEN_DATOS_CERTIFICADO_REVOCADO                         "SELECT TRAMITECVE,MEDIO FROM SEG_PKI.SEGT_REV_DET  WHERE NOSERIE ='%s'"


// ******************************  SENTENCIAS SQL CONEXION  *********************************
#define CON_OBTEN_DATOS_AGENTE_POR_NUM_SERIE                         "SELECT NIVELCVE, MODCVE FROM SEG_PKI.SEGT_AGENTE WHERE NOSERIE = '%s'"

#define CON_OBTEN_PUERTO_AUT_POR_APLICACION_1                        "SELECT PUERTO FROM SEG_PKI.SEGC_VERAUT WHERE IDAPL = %i AND FECINI <= SYSTIMESTAMP AND NVL(FECFIN||'.000', '2099-12-31 23:59:59.000') >= SYSTIMESTAMP"  
#define CON_OBTEN_PUERTO_AUT_POR_APLICACION_2                        "SELECT PUERTO FROM SEG_PKI.SEGC_VERAUT WHERE IDAPL = %i AND FECINI <= SYSTIMESTAMP AND NVL(FECFIN||'.000', '2100-12-31 23:59:59.000') >= SYSTIMESTAMP"  

#define CON_OBTEN_DATOS_MOTIVOS_DE_REVOCACION                        "SELECT MOTREVCVE, MOTREVDESC FROM SEG_PKI.SEGC_MOTIVO_REVOCA"
#define CON_OBTEN_DATOS_TIPO_DE_CERTIFICADOS_POR_AGENTE              "SELECT C.TIPCERCVE,C.TIPCERDESC FROM SEG_PKI.SEGT_NIVEL_TIP_CER N, SEG_PKI.SEGC_TIP_CER C WHERE  N.NIVELCVE = (select NIVELCVE from SEG_PKI.SEGT_AGENTE where AGCCVE = upper('%s')) AND C.TIPCERCVE = N.TIPCERCVE"
#define CON_OBTEN_NIVEL_AGENTE_POR_CLAVE                             "SELECT NIVELCVE FROM SEG_PKI.SEGT_AGENTE WHERE AGCCVE = '%s'"
#define CON_OBTEN_DATOS_MOTIVOS_DE_REVOCACION_MOSTRADOS              "SELECT MOTREVCVE, MOTREVDESC FROM SEG_PKI.SEGC_MOTIVO_REVOCA WHERE MOSTRAR = 'S'"

#define CON_SP_VALIDA_AGC_NOMBRE                                     "SEG_PKI.segsp_validaagc"  
#define CON_SP_VALIDA_AGC_PARAMS                                     "'%s',:i,:b,:s"
#define CON_SP_CONEXION_V2_NOMBRE                                    "SEG_PKI.segsp_conexion_v2"
#define CON_SP_CONEXION_V2_PARAMS                                    "'%s',%i,'%s',%i,%i,:i,:b,:b,:i,:b,:i,:i"
#define CON_SP_DESCONEXION_V2_NOMBRE                                 "SEG_PKI.segsf_desconexion_v2"
#define CON_SP_DESCONEXION_V2_PARAMS                                 "'%s', %i"
#define CON_SP_DESCONEXION_V2_RETURN                                 ":i"


// ******************************  SENTENCIAS SQL CONSULTA CERT  *********************************
#define CONS_CERT_SP_REGISTRA_OPER_NOMBRE                             "SEG_PKI.segsp_regoperdet" 
#define CONS_CERT_SP_REGISTRA_OPER_PARAMS                             "'%s',%d,%d,:i,:i"
#define CONS_CERT_OBTEN_CADENA_DATOS_CERTIFICADO_POR_NUM_SERIE_1      "select EDOCER||'|'||TIPCERCVE||'|'||RFC from FROM SEG_PKI.SEGT_CERTIFICADO where NOSERIE = '%s'"
#define CONS_CERT_OBTEN_CADENA_DATOS_CERTIFICADO_POR_NUM_SERIE_2      "select EDOCER ||'|'|| TIPCERCVE,RFC from FROM SEG_PKI.SEGT_CERTIFICADO where NOSERIE = '%s'"
#define CONS_CERT_OBTEN_ULT_NUM_SERIE_DE_FIEL_POR_RFC                 "select * from (select NOSERIE from FROM SEG_PKI.SEGT_CERTIFICADO where RFC = '%s' and TIPCERCVE < 3 order by NOSERIE desc) where ROWNUM = 1"
#define CONS_CERT_OBTEN_CADENA_DATOS_CERTIFICADO_POR_NUM_SERIE_Y_RFC  "select NOSERIE||'|'||EDOCER||'|'||VIGINI||'|'||VIGFIN||'|'||TRAMITECVE||'|'||FECREGIES, nombre, TIPCERCVE from SEG_PKI.SEGT_CERTIFICADO where NOSERIE = '%s' and RFC = '%s'"
#define CONS_CERT_OBTEN_CADENA_DATOS_FIEL_POR_RFC                     "select NOSERIE||'|'||EDOCER||'|'||VIGINI||'|'||VIGFIN||'|'||TRAMITECVE||'|'||FECREGIES, nombre,TIPCERCVE||'|' from SEG_PKI.SEGT_CERTIFICADO where RFC = '%s' and TIPCERCVE < 3 order by NOSERIE desc"


// ******************************  SENTENCIAS SQL ESTADO TRAMITE  *********************************
#define EDO_TRAM_SP_REGISTRA_OPER_NOMBRE                              "SEG_PKI.segsp_regoperdet"  
#define EDO_TRAM_SP_REGISTRA_OPER_PARAMS                              "'%s',%d,%d,:i,:i"
#define EDO_TRAM_SP_SEGUIMIENTO_TRAMITE_NOMBRE                        "SEG_PKI.segsf_segtramite"
#define EDO_TRAM_SP_SEGUIMIENTO_TRAMITE_PARAMS                        "'%s','%s'"
#define EDO_TRAM_SP_SEGUIMIENTO_TRAMITE_RETURN                        ":s"


// ******************************  SENTENCIAS SQL GENERA CERT  *********************************
#define GEN_CERT_OBTEN_CLAVE_MODULO_POR_AGENTE                        "SELECT  MODCVE FROM SEG_PKI.SEGC_MODULO WHERE MODCVE='%s'"
#define GEN_CERT_OBTEN_MODULO_AGENTE_POR_CLAVE                        "SELECT MODCVE  FROM SEG_PKI.SEGT_AGENTE WHERE AGCCVE = '%s'"
#define GEN_CERT_OBTEN_ULT_NUM_SERIE_DE_CERTIFICADO_AGC_POR_RFC       "SELECT * FROM (SELECT NOSERIE FROM SEG_PKI.SEGT_CERTIFICADO WHERE RFC= '%s' AND EDOCER ='A' and TIPCERCVE > '2' ORDER BY NOSERIE DESC) WHERE ROWNUM = 1"
#define GEN_CERT_OBTEN_NUM_SERIE_AGENTE_POR_CLAVE                     "SELECT NOSERIE FROM FROM SEG_PKI.SEGT_AGENTE WHERE AGCCVE= '%s'"
#define GEN_CERT_OBTEN_DATOS_CERTIFICADO_POR_RFC_Y_TIPO               "SELECT CURP, NOM, RFCRL, CURPRL FROM SEG_PKI.SEGT_CERTIFICADO WHERE RFC='%s' AND TIPCERCVE=%d AND EDOCER='A'"
#define GEN_CERT_OBTEN_RFCRL_CERTIFICADO_POR_RFC_Y_TIPO               "SELECT RFCRL FROM SEG_PKI.SEGT_CERTIFICADO WHERE RFC='%s' AND TIPCERCVE=%d AND EDOCER='A'"
#define GEN_CERT_ACTUALIZA_AGENTE_POR_CLAVE                           "UPDATE  SEG_PKI.SEGT_AGENTE SET NOM ='%s' , APPAT='%s' , APMAT='%s', MODCVE = '%s', NOSERIE ='%s' ,BITTIPMOV ='C' ,BITUSRMOV ='%s' , BITFECMOV = SYSTIMESTAMP ,BITREFMOV ='CERTISAT:%s' ,NIVELCVE='%s'  WHERE AGCCVE='%s'"
#define GEN_CERT_OBTEN_ESTADO_RFC                                     "select EDORFC from SEG_SIREFE.SEGT_BLOQUEADO where RFC = %s and EDORFC='B'"
#define GEN_CERT_SP_AGREGA_AGENTE_NOMBRE                              "SEG_PKI.segsf_insagente"
#define GEN_CERT_SP_AGREGA_AGENTE_PARAMS                              "'%s','%s','%s','%s','%s','%s','%s','%s','CERTISAT:%s'"
#define GEN_CERT_SP_AGREGA_AGENTE_RETURN                              ":i" 


// ******************************  SENTENCIAS SQL LOGIN  *********************************
#define LOGIN_OBTEN_NOMBRE_AGENTE_ADM_POR_RFC                         "select NOM from SEG_MAEFIEL.SEGC_AGENTE_ADMIN where RFC = '%s'"


// ******************************  SENTENCIAS SQL OPERACION  *********************************
#define OPE_SP_GENERA_NUM_OPERACION_NOMBRE                            "SEG_PKI.segsf_gennumoper"
#define OPE_SP_GENERA_NUM_OPERACION_PARAMS                            "%d,'%s'"
#define OPE_SP_GENERA_NUM_OPERACION_RETURN                            ":b"
#define OPE_SP_GENERA_NUM_TRAMITE_NOMBRE                              "SEG_PKI.segsf_gennumtram"
#define OPE_SP_GENERA_NUM_TRAMITE_PARAMS                              "'%d','%s','%s','%s','%s','%s','%s'"
#define OPE_SP_GENERA_NUM_TRAMITE_RETURN                              ":b"


// ******************************  SENTENCIAS SQL RECUPERA CERT  *********************************
#define RECP_CERT_SP_REC_DATOS_NOMBRE                                 "SEG_PKI.segsp_recdatos"  
#define RECP_CERT_SP_REC_DATOS_PARAMS                                 "%s,'%s'"
#define RECP_CERT_SP_REGISTRA_OPER_NOMBRE                             "SEG_PKI.segsp_regoperdet"
#define RECP_CERT_SP_REGISTRA_OPER_PARAMS                             "'%s',%d,%d,:i,:i"
#define RECP_CERT_OBTEN_TIPO_ACUSE_POR_TRAMITE                        "SELECT  TIPACUCVE FROM SEG_PKI.SEGT_ACUSE WHERE TRAMITECVE = '%s'"


// ******************************  SENTENCIAS SQL RENOVACION  *********************************
#define RENOV_AGREGA_REVOCACION                                       "insert into SEG_PKI.SEGT_REV_DET values('%s','%s','%c','%s','%d','%s')"


// ******************************  SENTENCIAS SQL REVOCACION  *********************************
#define REVOC_AGREGA_REVOCACION                                       "insert into SEG_PKI.SEGT_REV_DET values ('%s','%s','%s','%s','%s','%s')"
#define REVOC_OBTEN_RFCRL_CERTIFICADO_POR_NUM_SERIE                   "select RFCRL from SEG_PKI.SEGT_CERTIFICADO where NOSERIE = '%s'"
#define REVOC_OBTEN_ULT_NOMBRE_DE_CERTIFICADO_POR_RFC                 "select * from (select NOM from SEG_PKI.SEGT_CERTIFICADO where RFC = '%s' order by NOSERIE desc) where ROWNUM = 1"
#define REVOC_OBTEN_DATOS_CERTIFICADO_POR_NUM_SERIE                   "select NOM,TIPCERCVE from SEG_PKI.SEGT_CERTIFICADO where NOSERIE = '%s'"
#define REVOC_OBTEN_MODULO_AGENTE_POR_CLAVE                           "select MODCVE from SEG_PKI.SEGT_AGENTE where AGCCVE = '%s'"
#define REVOC_OBTEN_NUM_SERIE_CERTIFICADO_POR_RFC_Y_NUM_SERIE         "select NOSERIE from SEG_PKI.SEGT_CERTIFICADO where RFC = '%s' and TIPCERCVE = 1 and EDOCER = 'A' and NOSERIE in('%s', '%s')"
#define REVOC_OBTEN_TOTAL_FIEL_POR_RFC                                "select  count (NOSERIE)  from SEG_PKI.SEGT_CERTIFICADO where RFC='%s' and EDOCER='A' and TIPCERCVE=2"
#define REVOC_OBTEN_DATOS_FIEL_POR_RFC                                "select  NOSERIE,TIPCERCVE  from SEG_PKI.SEGT_CERTIFICADO where RFC='%s' and EDOCER='A' and TIPCERCVE=2"
#define REVOC_OBTEN_FECH_VIG_FIN_CERTIFICADO_POR_NUM_SERIE            "select  VIGFIN  from SEG_PKI.SEGT_CERTIFICADO where NOSERIE='%s'"
#define REVOC_SP_GENERA_NUM_OPER_NOMBRE                               "SEG_PKI.segsf_gennumoper"
#define REVOC_SP_GENERA_NUM_OPER_PARAMS                               "'%d','%s'"
#define REVOC_SP_GENERA_NUM_OPER_RETURN                               ":b"
#define REVOC_SP_GENERA_NUM_TRAM_NOMBRE                               "SEG_PKI.segsf_gennumtram"
#define REVOC_SP_GENERA_NUM_TRAM_PARAMS                               "'%d','%s','%s','%s','CADENAR ORIGINAL','FIRMA','ARCHIVO'"
#define REVOC_SP_GENERA_NUM_TRAM_RETURN                               ":b"


// ******************************  SENTENCIAS SQL SELLOS DIGS  *********************************
#define SELLOS_OBTEN_PRIMER_NOMBRE_CERTIFICADO_POR_RFC                "select NOM from SEG_PKI.SEGT_CERTIFICADO where RFC = '%s' and ROWNUM = 1"
#define SELLOS_OBTEN_FECHA_OPERACION_POR_SOLICITUD                    "select FECOP from SEG_PKI.SEGT_OPERACION where SOLOPERCVE = '%s'"
#define SELLOS_ACTUALIZA_REQ_SELLO                                    "UPDATE SEG_PKI.SEGT_REQS_SELLOS SET NOSERIE='%s' WHERE SOLOPERCVE='%s' AND OPERSEC=%d"
#define SELLOS_SP_AGREGA_REQ_SELLO_NOMBRE                             "SEG_PKI.segsf_insreqsellos"
#define SELLOS_SP_AGREGA_REQ_SELLO_PARAMS                             "'%s', '%s'"
#define SELLOS_SP_AGREGA_REQ_SELLO_RETURN                             ":i"


// ******************************  SENTENCIAS SQL SOL CERT  *********************************
#define SOL_CERT_OBTEN_DATOS_CERTIFICADO_POR_RFC_Y_TIPO              "SELECT NOSERIE , NOM ,PUBK FROM SEG_PKI.SEGT_CERTIFICADO WHERE RFC='%s' AND TIPCERCVE=%d AND EDOCER = 'A'"
#define SOL_CERT_OBTEN_MODULO_AGENTE_POR_CLAVE                       "SELECT MODCVE FROM SEG_PKI.SEGT_AGENTE WHERE AGCCVE ='%s'"
#define SOL_CERT_OBTEN_NOMBRE_CERTIFICADO_POR_RFC                    "SELECT  NOM FROM SEG_PKI.SEGT_CERTIFICADO WHERE RFC='%s' AND EDOCER = 'A'"


// ******************************  SENTENCIAS SQL SOL DATOS CERT  *********************************
#define SOL_DATOS_CERT_OBTEN_DATOS_CERTIFICADO_POR_NUM_SERIE         "SELECT EDOCER, TIPCERCVE, VIGFIN FROM SEG_PKI.SEGT_CERTIFICADO WHERE NOSERIE = '%s'"


// ******************************  SENTENCIAS SQL SGI DARIO  *********************************
#define SGI_DARIO_SP_DATOS_ADIC_CRM_NOMBRE                           "SEG_NEGOCIO.segsp_datosadicio_crm"
#define SGI_DARIO_SP_DATOS_ADIC_CRM_PARAMS                           "'%s',:i,:b,:b"


// ******************************  SENTENCIAS SQL UTIL BDAR  *********************************


#define UTIL_BDAR_OBTEN_DATOS_SITUACION_FISCAL_POR_DOMICILIO         "SELECT SITDOMDESC, TIPCERVAL FROM SEG_NEGOCIO.SEGC_SITDOMIC WHERE SITDOMCVE = %i"
#define UTIL_BDAR_OBTEN_DATOS_SITUACION_FISCAL_POR_CLAVE_1           "SELECT SITFIS_DESC, TIPCERVAL, VALDOMIC FROM SEG_NEGOCIO.SEGC_SITFIS WHERE SITFIS_CVE = %i"
#define UTIL_BDAR_OBTEN_DATOS_SITUACION_FISCAL_POR_CLAVE_2           "SELECT TIPCERVAL, VALDOMIC FROM SEG_NEGOCIO.SEGC_SITFIS WHERE SITFIS_CVE = %d"
#define UTIL_BDAR_OBTEN_TIPO_SITUACION_DOM_POR_CLAVE                 "SELECT TIPCERVAL FROM SEG_NEGOCIO.SEGC_SITDOMIC WHERE SITDOMCVE = %d"


#define UTIL_BDAR_OBTEN_TOTAL_CERTIFICADOS_POR_RFC                   "SELECT count(*) FROM SEG_PKI.SEGT_CERTIFICADO WHERE RFC = '%s'"
#define UTIL_BDAR_OBTEN_TOTAL_CERTIFICADOS_POR_RFC_Y_TIPO            "SELECT count(*) FROM SEG_PKI.SEGT_CERTIFICADO WHERE RFC = '%s' and TIPCERCVE = %d"
#define UTIL_BDAR_OBTEN_TOTAL_CERTIFICADOS_POR_RFC_Y_ESTADO          "SELECT count(*) FROM SEG_PKI.SEGT_CERTIFICADO WHERE RFC = '%s' and EDOCER = '%c'"
#define UTIL_BDAR_OBTEN_TOTAL_CERTIFICADOS_POR_RFC_TIPO_Y_EDO        "SELECT count(*) FROM SEG_PKI.SEGT_CERTIFICADO WHERE RFC = '%s' and TIPCERCVE = %d and EDOCER = '%c'"




#define UTIL_BDAR_OBTEN_TOTAL_AGENTE_POR_CLAVE_Y_TIPO_CERT           "SELECT count(*) FROM SEG_PKI.SEGT_AGENTE a, FROM SEG_PKI.SEGT_NIVEL_TIP_CER n WHERE a.AGCCVE = '%s' AND a.NIVELCVE = n.NIVELCVE AND n.TIPCERCVE = %d"
#define UTIL_BDAR_OBTEN_DATOS_AGENTE_Y_CERT_POR_CLAVE_AGC            "SELECT  a.ESTADO, c.EDOCER FROM SEG_PKI.SEGT_AGENTE a, SEG_PKI.SEGT_CERTIFICADO c WHERE a.AGCCVE = '%s' AND a.NOSERIE = c.NOSERIE"
#define UTIL_BDAR_OBTEN_DATOS_CERTIFICADO_POR_NUM_SERIE              "SELECT EDOCER, TIPCERCVE FROM SEG_PKI.SEGT_CERTIFICADO WHERE NOSERIE = '%s'"
#define UTIL_BDAR_OBTEN_VIG_FIN_CERTIFICADO_POR_NUM_SERIE            "SELECT vig_fin FROM SEG_PKI.SEGT_CERTIFICADO WHERE NOSERIE = '%s'"
#define UTIL_BDAR_OBTEN_NIVEL_AGENTE_POR_CLAVE                       "SELECT NIVELCVE FROM SEG_PKI.SEGT_AGENTE WHERE AGCCVE = '%s'"
#define UTIL_BDAR_OBTEN_RFC_RFC_ASOCIADO_POR_RFC_ORIG                "SELECT RFC FROM SEG_SIREFE.SEGT_RFCASOCIADO WHERE RFCORIG ='%s' and RFC ='%s'"
#define UTIL_BDAR_OBTEN_CURP_CERTIFICADO_POR_RFC                     "SELECT CURP FROM SEG_PKI.SEGT_CERTIFICADO  WHERE RFC = '%s' and EDOCER='A' and TIPCERCVE=1"
#define UTIL_BDAR_AGREGA_RFC_ASOCIADO                                "INSERT INTO SEG_SIREFE.SEGT_RFCASOCIADO  VALUES ( '%s' , '%s')"
#define UTIL_BDAR_BORRA_LLAVE_APARTADA_POR_OPERACION                 "DELETE FROM SEGT_LLAVE_APARTADA WHERE SOLOPERCVE = '%s' and OPERSEC = %i"


#define UTIL_BDAR_SP_GET_FEAacxRFC_NOMBRE                            "SEG_PKI.segsp_getfeaacxrfc"
#define UTIL_BDAR_SP_GET_FEAacxRFC_PARAMS                            "'%s',:i,:b,:b"
#define UTIL_BDAR_SP_VAL_LLAVE_UNICA_NOMBRE                          "SEG_PKI.segsp_valllaveunica"
#define UTIL_BDAR_SP_VAL_LLAVE_UNICA_PARAMS                          "'%s','%s','%s',:i,:s"
#define UTIL_BDAR_SP_REGISTRA_OPER_NOMBRE                            "SEG_PKI.segsp_regoperdet"
#define UTIL_BDAR_SP_REGISTRA_OPER_PARAMS                            "'%s', %i, %i,:i,:i"
#define UTIL_BDAR_SP_REGISTRA_LLAVE_APARTADA_NOMBRE                  "SEG_PKI.segsf_regllvapart"
#define UTIL_BDAR_SP_REGISTRA_LLAVE_APARTADA_PARAMS                  "'%s','%s','%s','%s'"
#define UTIL_BDAR_SP_REGISTRA_LLAVE_APARTADA_RETURN                  ":i"
#define UTIL_BDAR_SP_VAL_FECHA_CERTIFICADO_NOMBRE                    "SEG_PKI.segsf_valfeccert"
#define UTIL_BDAR_SP_VAL_FECHA_CERTIFICADO_PARAMS                    "'%s'"
#define UTIL_BDAR_SP_VAL_FECHA_CERTIFICADO_RETURN                    ":i"
#define UTIL_BDAR_SP_VAL_BLOQUEO_NOMBRE                              "SEG_SIREFE.segsf_verif_bloq"
#define UTIL_BDAR_SP_VAL_BLOQUEO_PARAMS                              "'%s'"
#define UTIL_BDAR_SP_VAL_BLOQUEO_RETURN                              ":i"
//No tienen definicion en ORACLE
#define UTIL_BDAR_SP_REGISTRA_ERROR_NOMBRE                           "sp_regError"
#define UTIL_BDAR_SP_REGISTRA_ERROR_PARAMS                           "'%s', %i"
#define UTIL_BDAR_SP_VAL_GEN_SELLOS_NOMBRE                           "sp_valida_gen_sd"
#define UTIL_BDAR_SP_VAL_GEN_SELLOS_PARAMS                           "%d,'SET{%s}','SET{%s}','SET{%s}'"

#endif  // _CSENTENCIASSQL_H_