/*##############################################################################
  ###  PROYECTO:              PKI-SAT                                        ###
  ###  MODULO:                SrvAR    Servidor de la AR                     ###
  ###                                                                        ###
  ###  DESARROLLADORES:       H�ctor Ornelas Arciga           HOA            ###
  ###                         Amilcar Guerrero Zu�iga         AGZ            ###
  ###                         Gudelia Hern�ndez Molina        GHM            ###
  ###                         Miguel Angel Mendoza L�pez      MAML           ###
  ###                         Roc�o Alejandra Mart�nez Olayo  RAMO           ###
  ###                         Silvia Eur�dice Rocha Reyes     SERR           ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       Martes 6, diciembre del 2005                   ###
  ###                                                                        ###
  ##############################################################################
         1         2         3         4         5         6         7         8
12345678901234567890123456789012345678901234567890123456789012345678901234567890
*/
/*##############################################################################
   VERSION:  
      V.02.00      (20051206 - 20051231) HOA: 2a versi�n: reconstrucci�n total 
                                del servicio
                                M�dulo de control, recibe las solicitudes de las
                                aplicaciones clientes CertiSAT (ventanilla y Web)
#################################################################################*/

#ifndef _SRVAR_H_
#define _SRVAR_H_
static const char* _SRVAR_H_VERSION_ ATR_USED = "SrvAR @(#)"\
"DSIC09041AR_ 2007-12-13 SrvAR.h 1.1.1/3";

//#VERSION: 1.1.1
//#############################################################################
#include <string>
#include <SgiTipos.h>
#include <Sgi_SrvUnx.h>        //Lib para crear el Servicio
//- #include <MensajesSAT.h>   //- GHM (070427)
#include <Sgi_MsgPKI.h>        //+ GHM (070427)
//- #include <CConfigFile.h>   //- GHM (070427)
#include <Sgi_ConfigFile.h>    //+ GHM (070427)

//#include <Sgi_Conexion.h>
//#include <Sgi_ArchCfg.h>

//VARIABLE GLOBAL
extern std::string strEjecutable;   // Nombre del ejecutable del servidor AR
extern int         iPuerto;         // Numero de puerto q esta en los parametros del archivo /etc/xinetd.d/*

//#############################################################################
class CSrvAR : public CSrvUnx
{
      CConfigFile   Configuracion;
      MensajesSAT   MsgCli;
      
   protected:

      bool InicioDeSesion();
      void DesconectaAGC();

      virtual bool Inicia();
      virtual bool SetParmsSktProc(CSSL_parms& parms);
      virtual bool Proceso();
      virtual bool Operaciones();
      
      virtual bool envOperInv();
      virtual bool ProcesaParametros(int /*argc*/, char* /*argv*/[], char* /*env*/[]);

   public:
      CSrvAR();
      virtual ~CSrvAR();
};
//#############################################################################

extern CSrvAR* ptrSrvAR;

//#############################################################################

#endif //_SRVAR_H_

