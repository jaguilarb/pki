#ifndef _CSOLQR_H_
#define _CSOLQR_H_

//header para clase de MensajesSAT
#include <Sgi_Bitacora.h>
#include <COperacion.h>
#include <assert.h>


class CSolQR : public COperacion
{
   protected:

   string      m_MsgDesc;                         //Describe Error,Situacion, etc
   int         iError;                        //N�mero de Error
   char     cadOriginal[4096+4];
   int      lcadOriginal; 

      virtual const char * getNombreOperacion() {return "SOLQR"; }
      virtual int          getTipOper () {return SOLQR;}
      virtual bool         preProceso();
      bool                 Proceso();

   public:
      CSolQR(CConfigFile& config, MensajesSAT& msgcli, CSSL_Skt* skt);
      virtual ~CSolQR();
     // bool mensajeCodigoQR();
   

};

#endif //_SOL_QR_H_

