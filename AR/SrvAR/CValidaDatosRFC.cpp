static const char* _CVALIDADATOSRFC_CPP_VERSION_ ATR_USED = "SrvAR @(#)"\
"DSIC09041AR_ 2009-01-29 CValidaDatosRFC.cpp 1.1.0/2";

//#VERSION: 1.1.0
/*######################################################################################################################
  ###  PROYECTO:              PKI-SAT                                                                                ###
  ###  MODULO:                Clase que realiza las validaciones del RFC en el WS                                    ###
  ###                                                                                                                ###
  ###  DESARROLLADORES:       Gudelia Hern�ndez Molina        HEMG70AL                                               ###
  ###  FECHA DE INICIO:       25 de noviembre de 2008                                                                ###
  ###                                                                                                                ###
  ######################################################################################################################
         1         2         3         4         5         6         7         8         9        10        11        12
1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678*/


#include <Sgi_ConfigFile.h>

#include "CValidaDatosRFC.h"

extern CBitacora *Bitacora;

//######################################################################################################################

CValidaDatosRFC::CValidaDatosRFC(CConfigFile& config, CUtilBDAR *UtilBDAR, int iTipoCert):
   m_datosRFC(NULL),
   m_UtilBDAR(UtilBDAR),
   m_iTipoCer(iTipoCert),
   m_inicializado(false),
   m_Config(config)
{
   m_rfc[0] = '\0';
   //Bitacora->escribe(BIT_INFO, "pondra m_inicializado == false esta en el constructor CValidaDatosRFC()::");
}

CValidaDatosRFC::~CValidaDatosRFC()
{
   if (m_datosRFC)
   {
      delete m_datosRFC;
      m_datosRFC = NULL;
   }
   m_UtilBDAR = NULL;
}

std::string CValidaDatosRFC::getError()
{
   return m_MsgDesc;
}

bool CValidaDatosRFC::ObtieneDatosWS(char* rfc)
{
   sprintf(m_rfc, "%s", rfc);
   m_datosRFC = new CDatosRFC(string(rfc), m_Config, m_iTipoCer);
   
   if ( m_datosRFC == NULL )
   {
      Bitacora->escribe(BIT_ERROR, "Error en CValidaDatosRFC en ObtieneDatosWS: No se inicializ� el objeto m_datosRFC");
      return false;
    }
   
   if ( m_datosRFC->ConectaWS())
   {
      Bitacora->escribe(BIT_ERROR, "Error en CValidaDatosRFC en ObtieneDatosWS: No hubo conexi�n a la Web Service");
      return false;
    }
 
   if ( m_datosRFC->ObtieneDatosWS() == EXITO )
   {
      //Bitacora->escribePV(BIT_INFO, "m_inicializado == true (m_datosRFC->ObtieneDatosWS() == EXITO) en CValidaDatosRFC::ObtieneDatosWS(%s)", rfc);  
      Bitacora->escribePV(BIT_INFO, "CValidaDatosRFC obtieneDatosWS: m_inicializado == true && m_datosRFC->obtieneDatosWS() == EXITO");
      m_inicializado = true;
      return true; 
   }
   return false;
}

bool CValidaDatosRFC::getNombreRFC(std::string &nombre)
{
   if ( m_datosRFC )
   {
      nombre = m_datosRFC->getNombre();
      if ( nombre.size() > 0 )
         return true;
   }
   return false;
}

bool CValidaDatosRFC::ValidaSFyD()
{
   int         isd = m_datosRFC->getSitDomicilio();
   std::string strSitFiscal = m_datosRFC->getSitFiscal();
   int         isf = atoi(strSitFiscal.c_str());
   
   return m_UtilBDAR->revSituacionFyD((uint16) m_iTipoCer, isf, isd);
}

bool CValidaDatosRFC::TieneBiom()
{
   int         resultado = EXITO;
   //std::string sTieneBiom;

   if (m_inicializado)
   {
      if ( ObtieneConfigBiometricos() )
      {
         Bitacora->escribe(BIT_INFO, "CValidaDatosRFC TieneBiom: Error al obtener la fuente  de los biom�tricos del archivo de configuracion.");   
         return false;
      }
      
      if ( strcmp( sFuenteBiom.c_str(),"L") == 0 )
      {
         if ( (resultado = m_datosRFC->ConectaLDAP()))
         {
            Bitacora->escribe(BIT_INFO, "CValidaDatosRFC TieneBiom: Se realiza la conexi�n a la LDAP."); 
            if ( (resultado = m_datosRFC->ObtieneDatosLDAP()) == 0 )
            {
               Bitacora->escribe(BIT_INFO, "CValidaDatosRFC TieneBiom: Se obtienen datos de la LDAP.");
               if ( m_datosRFC->sTieneBiom.size() < 1 || m_datosRFC->sTieneBiom.at(0) != 'Y' )
               {
                  Bitacora->escribe(BIT_INFO, "CValidaDatosRFC TieneBiom: No se obtuvieron los biom�tricos");
                  resultado = SIN_BIOMETRICO;
               }//m_datosRFC->DesconectaLDAP();
            }
         }
      }
      else if (strcmp( sFuenteBiom.c_str(),"D") == 0)
      {
         if ( (resultado = m_datosRFC->ConectaDARIO()))
         {
            Bitacora->escribe(BIT_INFO, "CValidaDatosRFC TieneBiom: Se realiza la conexi�n a la DARIO.");
            if ( (resultado = m_datosRFC->ObtieneDatosDARIO()) == 0 )
            {
               Bitacora->escribe(BIT_INFO, "CValidaDatosRFC TieneBiom: Se obtienen datos de DARIO.");
               if ( m_datosRFC->sTieneBiom.size() < 1 || m_datosRFC->sTieneBiom.at(0) != 'Y' )
                  Bitacora->escribe(BIT_INFO, "CValidaDatosRFC TieneBiom: No se obtuvieron los biom�tricos");
                  resultado = SIN_BIOMETRICO;
               //m_datosRFC->DesconectaDARIO();
            }
         }
      }

/*      //if ( (resultado = m_datosRFC->ConectaLDAP()))
      if ( (resultado = m_datosRFC->ConectaDARIO()))
      {
         //if ( (resultado = m_datosRFC->ObtieneDatosLDAP()) == 0 )
         if ( (resultado = m_datosRFC->ObtieneDatosDARIO()) == 0 )
         {
            //sTieneBiom = m_datosRFC->getTieneBiom();
            //Bitacora->escribePV(BIT_INFO, "0.- m_inicializado==true, sTieneBiom=%s, resultado=%d", m_datosRFC->sTieneBiom.c_str() , resultado );
            if ( m_datosRFC->sTieneBiom.size() < 1 || m_datosRFC->sTieneBiom.at(0) != 'Y' ) 
               resultado = SIN_BIOMETRICO;
            //Bitacora->escribePV(BIT_INFO, "1.- m_inicializado==true, sTieneBiom=%s, resultado=%d", m_datosRFC->sTieneBiom.c_str() , resultado ); 
         }
         //m_datosRFC->DesconectaLDAP();
         m_datosRFC->DesconectaDARIO();
      } 
*/
   }
   else
   {
      Bitacora->escribe(BIT_INFO, "CValidaDatosRFC TieneBiom: m_inicializado es igual a 'false'");
      resultado = NO_INICIALIZADO;
   }

   return (resultado == EXITO);
}

bool CValidaDatosRFC::getRFCOrig(char* rfcOrig)
{
   std::string strRFCOrig = "";
   
   strRFCOrig = m_datosRFC->getRFCOriginal( );
   if (strRFCOrig[0] == 0)
   {
      rfcOrig[0]=0;
      Bitacora->escribe(BIT_ERROR, "Error en CValidaDatosRFC en getRFCOrig: Error al tratar de obtener el RFC Original");
      return false;
   }
   sprintf(rfcOrig, "%s", strRFCOrig.c_str());
   return true;
}

int CValidaDatosRFC::getStatusRFC()
{
  return m_datosRFC->getsStatusRFC();
}

bool CValidaDatosRFC::
ObtieneConfigBiometricos()
{
    m_Config.cargaCfgVars();
    return m_Config.getValorVar("[BIOMETRICOS]", "FUENTE",&sFuenteBiom);

}

