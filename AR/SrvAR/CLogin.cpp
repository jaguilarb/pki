static const char* _CLOGIN_CPP_VERSION_ ATR_USED = "SrvAR @(#)"\
"DSIC09041AR_ 2014-09-25 CLogin.cpp 1.1.2/3";

#include <Sgi_ConfigFile.h>
#include <Sgi_PKI.h>
#include <CLogin.h>

int CLogin::m_iMsgRecib = -1;
int CLogin::errMsgRev = -1;

/*##############################################################################
   PROPOSITO: Constructor.
#################################################################################*/
CLogin::CLogin(CConfigFile& config, MensajesSAT& msgcli, CSSL_Skt* skt):
    COperacion(config, msgcli, skt, false) {
    rfcLogin[0] = 0;
    rolUser[0] = 0;
    m_iMsgRecib = 0;
    errMsgRev = 0;
}

/*##############################################################################
   Destructor.
#################################################################################*/
CLogin::~CLogin() {
}

/*##############################################################################
   PROPOSITO: Este metodo es el encargado de procesar el mensaje recibido 
              del CertiSAT

   PREMISA:
   EFECTO:    Este metodo va a inicializar algunas variables de la clase
              COperacion que deberan ser liberadas en el destructor.
   ENTRADAS:
   SALIDAS:
#################################################################################*/
bool CLogin::preProceso() {
    m_iMsgRecib = m_MsgCli.tipOperacion();
    switch(m_iMsgRecib) {
        case VALIDARLOGIN: {
            //int lFirmaLogin = sizeof(firmaLogin);
            int lRfcLogin = sizeof(rfcLogin);
            int lRolUser = sizeof(rolUser);
            //errMsgRev = m_MsgCli.getMensaje(VALIDARLOGIN, firmaLogin, &lFirmaLogin, rfcLogin, &lRfcLogin, rolUser, &lRolUser);
            errMsgRev = m_MsgCli.getMensaje(VALIDARLOGIN, rfcLogin, &lRfcLogin, rolUser, &lRolUser);
            if(errMsgRev ==  0) {
                Bitacora->escribePV(BIT_INFO, "Datos Recibidos CLogin::preProceso(): [%s - %s]", rfcLogin, rolUser);
                return true;
            } else {
                msgCliError('I', "Error en CLogin: Error al obtener el mensaje.", errMsgRev, eMSGSAT);
                return false;
            }
        }
        default: {
            msgCliError('I', "Error en CLogin: Error mensaje recibido no esperado.", ERR_MSG_NO_ESPERADO, eAPL);
            return false;
        }
    }
}
/*##############################################################################
   PROPOSITO: Esta funcion es la encargada de llevar a cabo todos los pasos
              para realizar una revocacion de Certificado Digital.

   PREMISA:
   EFECTO:
   ENTRADAS:
   SALIDAS:
#################################################################################*/
bool CLogin::Proceso() {
    char response[200];
    if(validarLogin()) {
        sprintf(response, "0|%s", nombre.c_str());
    } else {
        sprintf(response, "-1|El rfc '%s' no fue encontrado.", rfcLogin);
    }
    Bitacora->escribePV(BIT_INFO, "CLogin en Proceso: Response: [%s]", response);
    errMsgRev = m_MsgCli.setMensaje(VALIDARLOGINAR, response, strlen(response));
    if(errMsgRev ==  0) {
        errMsgRev = envia(CERTISAT);
        if(errMsgRev ==  0) {
            Bitacora->escribePV(BIT_INFO, "CLogin en Proceso: Mensaje VALIDARLOGINAR enviado exitosamente con status: [%d]", errMsgRev);
            return true;
        } else {
            msgCliError('I', "Error en CLogin en Proceso: Error al enviar el mensaje.", errMsgRev, eMSGSAT);
            return false;
        }
    } else {
        msgCliError('I', "Error en CLogin en Proceso: Error al setear el mensaje.", errMsgRev, eMSGSAT);
        return false;
    }
}

bool CLogin::validarLogin() {
    if(m_BDAR->consultaReg(LOGIN_OBTEN_NOMBRE_AGENTE_ADM_POR_RFC, rfcLogin)) {
        if(m_BDAR->getValores("s", &nombre)) {
            return true;
        }
    }
    return false;
}
