#ifndef _CSOLDATOSRFC_H_
#define _CSOLDATOSRFC_H_
static const char* _CSOLDATOSRFC_H_VERSION_ ATR_USED = "SrvAR @(#)"\
"DSIC09041AR_ 2009-01-29 CSolDatosRFC.h 1.1.0/0";

//#VERSION: 1.1.0
#include <COperacion.h>
#include <CDatosRFC.h>
#include <string>

#define EXITO                     0
//#define ERROR                    -1  // MAML: ya esta definido en CDatosRFC CDatosRFC.h 
#define ERROR_INI_CDATOSRFC      -2
#define ERROR_CONECTA_LDAP       -3 
#define ERROR_CONECTA_WS         -4
#define ERROR_OBT_DATOS_LDAP     -5
#define ERROR_OBT_DATOS_WS       -6
#define ERROR_OBT_DATOS_SIT_FIS  -7
#define ERROR_DOMICILIO          -8
#define ERROR_SITFISCAL          -9
#define ERROR_TIPCONT            -10 
#define ERROR_NOMBRE             -11
#define ERROR_DESCSITDOM         -12
#define ERROR_BIOMETRICOS        -13
#define ERROR_CONECTA_DARIO      -14

class CSolDatosRFC: public COperacion
{

   private:
   
      CConfigFile  &Config;
      CDatosRFC   *datosRFC;
      CDatosRFC   *datosRFCRL;


      int         iError;
      int         iMensajeRecibido;
   
      char        cRFC[14];
      char        cRFCRL[14];
      char        cDatosRFC[1024*2];
   
      int         iRFC;
      int         iRFCRL;
  
      bool        conRL;   
  
      std::string sTieneCert;
      std::string sTieneCertRL;
      std::string sFuenteBiom;
      std::string sCurpFiel;
   
      int ObtieneMensaje(int iMensaje); 
      int ConectaWSLDAP();
      int ConectaWSLDAPContrib();
      int ConectaWSLDAPRL();
      int ArmaError(int iError, char *cDescError);
      bool ArmaCadena(std::string &sMensaje );
      int ObtieneDatosRFC();
      int ArmaRespuesta(int iOperacion);
      int Envia();
      //void DesconectaLDAP();
      bool ObtieneDatosBD(char *cRFCb);
      bool ObtieneConfigBiometricos();
    
   public:

      bool Proceso();
      CSolDatosRFC(CConfigFile& config, MensajesSAT& msgcli, CSSL_Skt* skt);
     ~CSolDatosRFC();

  protected:

     virtual const char * getNombreOperacion() { return "SOLDATOSRFC"; };   
     virtual int          getTipOper ()        { return iMensajeRecibido; };
     virtual bool         preProceso();
};

#endif
