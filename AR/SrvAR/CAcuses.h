/*######################################################################################################################
  ###  PROYECTO:              PKI-SAT                                                                                ###
  ###  MODULO:                Clase que realiza el manejo de los datos de los acuses                                 ###
  ###                                                                                                                ###
  ###  DESARROLLADORES:       H�ctor Ornelas Arciga           HOA                                                    ###
  ###  FECHA DE INICIO:       Domingo 12, febrero 2006                                                               ###
  ###                                                                                                                ###
  ######################################################################################################################
         1         2         3         4         5         6         7         8         9        10        11        12
123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
 #######################################################################################################################
   VERSION:
      V.1.00      (20060212 - 20060212) HOA: Primera Versi�n
   CAMBIOS:
######################################################################################################################*/

#ifndef _CACUSES_H_
#define _CACUSES_H_
static const char* _CACUSES_H_VERSION_ ATR_USED = "@(#) SrvAR ( L : DSIC10392AR_ : CAcuses.h : 1.1.2 : 3 : 28/09/10)";

//#VERSION: 1.1.1

//+++ Defines de error para Generar y obtener un ACUSE en los tramites de: TRANSCOMPLETA, CERTREN, CERTREV y CERTGEN
#define ERR_LLAMA_BUSCA_ACUSE	80
#define ERR_GET_FECHA_EXITO	81
#define	ERR_GET_CAMPO		82

#define ERR_SET_REG_ACUSE	83
#define ERR_GET_NUM_ACUSE	84
#define ERR_GET_DAT_ACUSE	85

#define ERR_GET_REN_ACUSE  86
#define ERR_GET_AUT_ACUSE  87
//+++


//*** MELM 14/Feb/06: Estas variables se usan cunado se llaman un SEGUIMIENTO DESDE CEdoTramite � 
//		      cuando se hace una Recuperacion desde el CERTISAT	=> Ahi too se Recupera el Acuse

//			Opciones para el SEGuimiento o la RECuperacion de un Acuse 
#define	ACUSE_TRAMITE		'T'	// Acuse por Numero d Tramite, tabla: ACUSE,       clase: CEdoTramite
#define	ACUSE_GENERA		'G'	// Acuse por Numero de Serie,  tabla: CERTIFICADO, clase: 
#define	ACUSE_REVOCA		'R'	// Acuse por Numero de Serie,  tabla: REV_DET,	   clase:
//***

#define	MODULO_CERTISAT_WEB	"CSW"	// Clave del modulo del CertisatWEB clave q debe coincidir en CAT_MODULO
#define	MODULO_REVOCACIONJUR	"RJU"	// Clave del modulo para revocacion juridica
using namespace std;
class CAcuses
{
   //***  Objetos de la clase COperacion 
   CBD		*m_BDAR;		// COperacion::m_BDAR q suponemos ya conectado a la DB
   CConfigFile	&m_Configuracion;	// COperacion::m_Configuracion, Para leer el archvo de Configuracion
   char		*m_numOp;		// COperacion::m_numOp[]	Numero de Operacion del tramite solo p/Generar
   char          m_numTram[13];		// COperacion::m_numTram[]	Num d Tramite del Acuse se graba en: tramite_cve
					// No es char * por que se usa a la funcion getCampo()->m_BDAR->getValores()
   //***

   public:
   int		error;			// Variable para guardar el numero de Error del ACUSE � cero si no hay error
   char		cad_orig[4096+1];	// campo de la tabla: ACUSE
   char		firma[684+1];		// campo de la tabla: ACUSE
   char     fcad_orig[512]; //Aqu� va a poner la cadena original de la solicitud de revocacion que usa
   int      ifcad_orig;

   CAcuses(CBD* ar, CConfigFile& config, char *numOp = NULL, char *numTram = NULL);

   //  Funcion para obtener el acuse de la operacion de SEGuimiento
   int buscaAcuse(int *tipacu_cve, int *tipcer_cve, const char tipoRec, const char *no_Tram_Serie);

   //  Funcion para generar los acuses del Certisat
   int generaAcuse(int idMsg, int tipacu_cve, const char *rfc, const char *nombre, const char *no_serie, 
                   const char *llave_pub, const char *rfc_sol, const char *nombre_sol, const char *mod_cve,
                   int   tipcer_cve, int motrev_cve, const char *ref_arch, const char *dig_archivo,
                   const char* cadOrigRev = NULL, const char *firmCadOrigRev = NULL);
   int buscaCadSolRev(char* no_Tramite, char* cadSolRev, int *icadSolRev);

   private:
   void InicializaCad();
   //+++ Funciones para manejar los datos del ACUSE en la DB de la AR
   int obtenFechaMsgExitoso(int idMsg, char *fecha_acu);
   int getCampo(const char *nameCampo, const char *tabla, const char *nameClave, const char *tipoClave, 
                void *clave, char *campo);
   int guardaAcuse(int tipacu_cve, const char *mod_cve, const char *nombre, const char *llave_pub,
		const char *rfc_sol, const char *nombre_sol, const char *fecha_acu, int tipcer_cve);
   //+++
   int getTitulosCertisat(int tipacu_cve, int tipcer_cve, char *tipacu_desc);

   // 2 Function�s de Amilkor
   int getFirmaCadena(char *cadOri, uint i_cadOri, char *firma, uint16 *i_firma, char* mod_cve, int idMsg_);
   int getSerialAR(char *serial);

   int obtieneDatosRev( const char *cNumSerie, char* cMedio, char *cNumTram );
};

extern CBitacora* Bitacora;

#endif // _CACUSES_H_
