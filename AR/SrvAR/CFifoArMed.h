#ifndef _CFIFOARMED_H_
#define _CFIFOARMED_H_
static const char* _CFIFOARMED_H_VERSION_ ATR_USED = "SrvAR @(#)"\
"DSIC09041AR_ 2007-12-13 CFifoArMed.h 1.1.1/3";

//#VERSION: 1.1.1

#include <fcntl.h>
#include <sys/uio.h>
#include <unistd.h>

#include <cstdlib>
#include <iostream>
#include <cstdio>

//- #include <MensajesSAT.h>   //- GHM (070427)
#include <Sgi_MsgPKI.h>        //+ GHM (070427)
#include <Sgi_Semaforo.h>
//- #include <PwdProtection.h> //- GHM (070427)
#include <Sgi_ProtegePwd.h>    //+ GHM (070427)

#ifdef DBG
   #define SERV_FIFO "/tmp/fifod.serv"
#else
   #define SERV_FIFO "/tmp/fifo.serv"
#endif

#define FILE_MODE (S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH )

#define ERR_SEM_NOT_CREATE		 110
#define ERR_NO_ABRE_FIFO		 111
#define ERR_NO_CREA_FIFO_LECT	 112
#define ERR_PID_NO_COINCIDEN	 113
#define ERR_NO_LEE_FIFO			 114
#define ERR_NO_LEE_BUF_MED		 115
#define ERR_GET_DATOS			 116

class CFifoArMed
{
   private:
      MensajesSAT *m_mensaje;
      CSemaforo   *m_semaforo;
      char m_fifoLec[255];
      char m_fifoEsc[255];
      intE m_error;
      char m_bufferMen[TAM_DATOS];
      int  i_bufferMen;
      int  m_leeBuffer(int, char*, long);
      long m_leeLong(int, long&);
      pid_t m_pid;
      int fd_LecFifo;
      int fd_EscFifo;
      bool Iniciado;
   public:
      CFifoArMed();
      ~CFifoArMed();
      int  inicia(MensajesSAT *);
      void termina(void);
      intE escribe(void);
      intE lee(void);
      bool getIniciado();
};

#endif //_CFIFOARMED_H_
