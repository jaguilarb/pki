#ifndef _CRECUPERACERT_H_
#define _CRECUPERACERT_H_
static const char* _CRECUPERACERT_H_VERSION_ ATR_USED = "SrvAR @(#)"\
"DSIC09041AR_ 2007-12-13 CRecuperaCert.h 1.1.1/3";

//#VERSION: 1.1.1
/*##############################################################################
 **###  PROYECTO:              PKI-SAT                                        ###
 **###  MODULO:                SrvARA   Servidor de la AR                     ###
 **###                         Recuperación de Certificados.                  ###
 **###                                                                        ###
 **###  DESARROLLADORES:       Silvia Eurídice Rocha Reyes     SERR           ###
 **###                                                                        ###
 **###  FECHA DE INICIO:       Jueves 1 , diciembre del 2005                  ###
 **###                                                                        ###
 **##############################################################################
 **  1         2         3         4         5         6         7         8
 **12345678901234567890123456789012345678901234567890123456789012345678901234567890
 **
 **/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <COperacion.h>
//- #include <MensajesSAT.h> //- GHM (070427)
#include <Sgi_MsgPKI.h>      //+ GHM (070427)
//- #include <Sgi_Pki.h>     //- GHM (070321): Se modificó el nombre de la librería 
#include <Sgi_PKI.h>         //+ GHM (070321)
//- #include <CConfigFile.h> //- GHM (070427)
#include <Sgi_ConfigFile.h>  //+ GHM (070427)
#include <Sgi_Bitacora.h>
#include <Sgi_BD.h>
#include <Sgi_SrvUnx.h>


#define  SIN_ERROR    -1
#define  NOEXISTECERT  18
#define  EXISTECERT    161
#define  ERROR_BD_CONSULTA 162
#define  ERROR_BD_GETVALORES 163
#define  ES_SELLO 104
#define  NO_ES_SELLO 105
using namespace std;

struct stDatosCert
{
   char cNumSerie[21];
   char cTipCert [2];
   char cEdoCert [2];
   char cRFC     [14];
   char cNumOper [13];
   char cFecIni  [20];
   char cBufCert [2048];
   int  iBufCert;
};


class CRecuperaCert: public COperacion 
{
   public:
               CRecuperaCert(CConfigFile& config, MensajesSAT& msgcli, CSSL_Skt* skt);	   
   protected:
               virtual const char* getNombreOperacion() { return "Recuperación de Certificado"; }
               virtual int getTipOper() { return 1; } // PENDIENTE
               virtual bool preProceso();
               virtual bool Proceso();
   private:
               stDatosCert stCert;
               string sError;
               char m_cDato    [21];
               char cTipRec  [2];
               int  error;
               bool esSello;

               bool   ObtieneMensaje(char * cTipRec,char * cDato );
	            int    BuscaCert(char* cNumSerie);
               int    Recuperacion(char *cTipRec , char *cDato);	    
               int    SeparaCad(char *cCad ,...);
               int    RegistraOpe(char *cOper , int iOper , int iErr);
	            int    Respuesta();
               int    Error(char * cOper, char *cTipErr ,int iError , char* cDesc);
               int    Envia();
               int    esSellos (char * cNumtram);
};

#endif
