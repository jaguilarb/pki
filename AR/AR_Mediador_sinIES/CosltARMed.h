#ifndef _COSLTARMED_H_
#define _COSLTARMED_H_

static const char* _COSLTARMED_H_VERSION_ ATR_USED = "@(#) SrvAC ( L : DSIC10392AR_ : ConsltARMed.h : 1.0.0 : 10/10/14)";

//#VERSION: 1.1.0
/*##############################################################################
  ###  PROYECTO:              PKI-SAT                                        ###
  ###  MODULO:                Servicio AR                                    ###
  ###                                                                        ###
  ###                                                                        ###
  ###  DESARROLLADORES:       Guadalupe Negrete Castro         GNC           ###
  ###                                                                        ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       Viernes 10, octubre del 2014                   ###
  ###                                                                        ###
  ##############################################################################
         1         2         3         4         5         6         7         8
12345678901234567890123456789012345678901234567890123456789012345678901234567890

################################################################################
  VERSION:
     V.1.0.0  (20141010- )    GNC: Version Original
                              Consutas para el Servicio de Autoridad Registradora PKI-SAT
  

#################################################################################*/



//#VERSION: 1.1.0


#define QS_EDO_CERT               "SELECT  EDO_CER FROM CERTIFICADO WHERE NO_SERIE = '%s';"
#define QU_FREGIES_CERT           "UPDATE CERTIFICADO SET FEC_REG_IES = '%s' WHERE NO_SERIE = '%s';"
#define QU_EDO_VF_CERT            "UPDATE CERTIFICADO SET  EDO_CER = 'R', VIG_FIN ='%s' WHERE NO_SERIE = '%s';"
#define QU_EDO_VF_FIES_CERT       "UPDATE CERTIFICADO SET  EDO_CER = 'R', VIG_FIN ='%s' , FEC_REG_IES ='%s' WHERE NO_SERIE = '%s';"
#define QI_CERTIFICADO            "INSERT INTO CERTIFICADO(no_serie, tipcer_cve, edo_cer, rfc, curp, nombre, correo, rfc_rl, curp_rl, vig_ini, vig_fin, fec_reg_ies, dig_pubk_md5, dig_pubk_sha1, es_sat, tramite_cve ,pubk) VALUES ('%s',%s,'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')"
#define QS_TRAMITE_CVE            "SELECT TRAMITE_CVE FROM TRAMITE WHERE SOLOPER_CVE = '%s'"
#define QS_SERIE_CERT             "SELECT FIRST 1 NO_SERIE FROM CERTIFICADO WHERE RFC= '%s' AND EDO_CER ='A' and TIPCER_CVE IN ('1', '2')  ORDER BY NO_SERIE DESC;"
#define QU_EDO_CERT               "UPDATE CERTIFICADO SET EDO_CER='R' WHERE NO_SERIE = '%s'"
#define QS_EDO_VF_FIES_CERT       "SELECT EDO_CER ,VIG_FIN , FEC_REG_IES FROM CERTIFICADO WHERE NO_SERIE = '%s';"
#define QS_RFC_TIP_CERTIFICADO    "SELECT rfc, tipcer_cve from certificado where no_serie = '%s'"
             
#define SP_GEN_NUMOPER              "SP_GENNUMOPER" ,"'%s','AR_SAT'"
#define SP_GEN_NUMTRAM              "SP_GENNUMTRAM" , "'%d','%s','%s','AR_SAT','ALERTADO','ALERTADO','ALERTADO'"
#define SP_REG_OPERDET              "SP_REGOPERDET", "'%s',%d,%d"









#endif




