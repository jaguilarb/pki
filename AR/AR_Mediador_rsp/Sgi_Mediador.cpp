static const char* _SGI_MEDIADOR_CPP_VERSION_ ATR_USED = "@(#) AR_Mediador ( L : DSIC10392AR_ : Sgi_Mediador.cpp : 1.1.6 : 5 28/09/10)";

//#VERSION: 1.1.5
//02.00.01 (070219) GHM: Modificaciones para utilizar la librer�a Sgi_Fecha
//02.00.02 (070313) GHM: Ajustes en el uso de bit�cora para los errores de la librer�a Sgi_Fecha
//V.1.1.1.1 - (080422) SERR: Se agrego un nuevo mensaje en el cual se recibibe de AC la fecha final del certificado en el momento de la revocacion, para que se tenga la misma fecha en la base de la AC y AR.
//V.1.1.1.2 -(090918)  SERR : Se cambio en la funcion de MarcaARA la fecha de revocacion ya que tenia la fecha de la vigencia final del certificado cuando se hacia una renovacion.
//V.1.1.5   (100414) MAML: Se amplia el buffer que recibe del SrvAR conteniendo un nombre de 255 caracteres.
#include<Sgi_Mediador.h>   

int verify_callback(int ok, X509_STORE_CTX *store)
{
        char data[256];
        if (!ok)
        {
        X509 *cert = X509_STORE_CTX_get_current_cert(store);
        int depth = X509_STORE_CTX_get_error_depth(store);
        int err = X509_STORE_CTX_get_error(store);

        fprintf(stderr,"Error con el certificado %i\n",depth);
        X509_NAME_oneline(X509_get_issuer_name(cert),data,256);
        fprintf(stderr,"issuer = %s\n",data);
        X509_NAME_oneline(X509_get_subject_name(cert),data,256);
        fprintf(stderr,"subject=%s\n",data);
        fprintf(stderr,"err%i:%s\n",err,X509_verify_cert_error_string(err));
        }
        return (ok);
}

//*****************************************************************************************

char  PASSLLAVCLISKT[TAM_PWD];

static int password_cb(char *buf, int size, int rwflag, void *password)
{

  password =PASSLLAVCLISKT;
  strncpy(buf, (char *)(password), size);
  buf[size - 1] = '\0';
  return(strlen(buf));
}


void Mediador::dumpBuffer(const char* texto, const uint8* buffer, int lbuf)
{
   std::string msg(texto), hex, asc;
   char tmp[64];
   int i, j;

   sprintf(tmp, " (%d bytes):\n\t", lbuf);
   msg += tmp;

   for (i = j = 0; i < lbuf; i++, j++)
   {
      if (j == 16)
      {
         (((msg += hex) += "   ") += asc) += "\n\t";
         hex.clear();
         asc.clear();
         j = 0;
      }
      sprintf(tmp, "%02X ", buffer[i]);
      hex += tmp;

      sprintf(tmp, "%c"  , buffer[i] < 32 ? '.' :  buffer[i]);
      asc += tmp;
   }

   for (; j < 16; j++)
      hex += "   ";

   ((msg += hex) += "   ") += asc;
   bit->escribe(BIT_INFO, msg.c_str());
}


/*##################################################################################

 PROPOSITO: Construir la clase.
 PREMISA  : ******
 EFECTO   : ******
 ENTRADAS : ******
 SALIDAS  : ******

 #################################################################################*/

Mediador::
Mediador()
{
#ifndef DBG
  conf = new CConfigFile("/usr/local/SAT/PKI/etc/AR_Mediador_IES.cfg");
#else
  conf = new CConfigFile("/usr/local/SAT/PKI/etc/AR_Mediador_IES_dbg.cfg");
#endif
  error = conf->cargaCfgVars();
  esGeneracion = false;
  enviadoAR    = false;
  terminarMed  = false;
  enRegistraAC = false;
  esRenovacion = false;
  bError       = false;
  sktAC        = NULL;
  sktIES       = NULL;

}

/*##################################################################################

 PROPOSITO: Limpia Variables. para cada peticion recibida
 PREMISA  : ******
 EFECTO   : ******
 ENTRADAS : ******
 SALIDAS  : ******

 #################################################################################*/

void Mediador :: 
Limpia()
{
   conRepr = false;       //>>> MAML (070529)
   enRegistraAC = false;
   esRenovacion = false;
   memset(firma     , 0 , ifirma     = sizeof(firma));
   memset(cNumTram  , 0 , iNumTram   = sizeof(cNumTram));
   memset(cSec      , 0 , iSec       = sizeof(cSec)); 
   memset(rfc_contr , 0 , irfc_contr = sizeof(rfc_contr));
   memset(cert.TipCer.cont , 0 , cert.TipCer.icont = sizeof(cert.TipCer.cont) );
   memset(cert.req.cont    , 0 , cert.req.icont = sizeof(cert.req.cont) );
   memset(cert.PwdRev.cont , 0 , cert.PwdRev.icont = sizeof(cert.PwdRev.cont) );
   memset(cert.Correo.cont , 0 , cert.Correo.icont = sizeof(cert.Correo.cont) );
   memset(cert.VigIni.cont , 0 , cert.VigIni.icont = sizeof(cert.VigIni.cont) );
   memset(cert.VigFin.cont , 0 , cert.VigFin.icont = sizeof(cert.VigFin.cont) );
   memset(cert.NumSer.cont , 0 , cert.NumSer.icont = sizeof(cert.NumSer.cont) );
   memset(cert.Nombre.cont , 0 , cert.Nombre.icont = sizeof(cert.Nombre.cont) );
   memset(cert.RFC.cont , 0 , cert.RFC.icont = sizeof(cert.RFC.cont) );
   memset(cert.Curp.cont , 0 , cert.Curp.icont = sizeof(cert.Curp.cont) );
   memset(cert.RFCRL.cont , 0 , cert.RFCRL.icont = sizeof(cert.RFCRL.cont) );
   memset(cert.RFCCurp.cont , 0 , cert.RFCCurp.icont = sizeof(cert.RFCCurp.cont) );
   memset(cert.Edo.cont , 0 , cert.Edo.icont = sizeof(cert.Edo.cont) );
   memset(cert.FecRegIES.cont , 0 , cert.FecRegIES.icont = sizeof(cert.FecRegIES.cont) );
   memset(cert.cert.cont , 0 , cert.cert.icont = sizeof(cert.cert.cont) );
   memset(cert.LlavePub.cont , 0 , cert.LlavePub.icont = sizeof(cert.LlavePub.cont));
   memset(cert.LlaveMD5.cont , 0 , cert.LlaveMD5.icont = sizeof(cert.LlaveMD5.cont));
   memset(cert.LlaveSHA1.cont , 0 , cert.LlaveSHA1.icont = sizeof(cert.LlaveSHA1.cont));
   memset(cNumSerie_ren ,0 , iNumSerie_ren = sizeof(cNumSerie_ren));
   

}

/*##################################################################################

 PROPOSITO: Hacer las acciones necesarias para cuando llega un certificado alertado de la IES.
 PREMISA  : ******
 EFECTO   : ******
 ENTRADAS : ******
 SALIDAS  : ******

 #################################################################################*/



int Mediador::
CertAlertaIES(bool  esGeneracion)
{
   error = SIN_OPER;
   
   char cfirma_al[TAM_FIRMA] ; int ifirma_al  = sizeof(cfirma_al);
   char cfecha_al[13];         int ifecha_al  = sizeof(cfecha_al);
   char cNumSer_al[21];        int iNumSer_al = sizeof(cNumSer_al);

   string sConsulta = "SELECT  EDO_CER FROM CERTIFICADO WHERE NO_SERIE = '%s';"; 
   string sNumAR;
   string sEdoCert;

   
   error = mensIES.getMensajeIES(OP_CRTENALERTA , cfirma_al, &ifirma_al , cfecha_al ,&ifecha_al , cNumSer_al ,&iNumSer_al);
   if(error)
   {
      bit->escribePV(BIT_ERROR,"     Error en getMensajeIES OP_CRTENALERTA  %d",error);
      return error;
   }
   cNumSer_al[20] = 0; 

   error = conf->getValorVar("[IES]","NUMAR" ,&sNumAR);
   Alertados(cNumSer_al);

   if( strncmp(cNumSer_al , sNumAR.c_str(), sNumAR.length() ) != 0)
   {
         bit->escribePV(BIT_ERROR, "     Certificado Alertado no SAT : %s", cNumSer_al);
         ErrorAR_AC(AR,"Error con el requerimiento favor de generar uno nuevo" , 102 , "N");
         return EXITO;
   }

   if(esGeneracion)
   {
  //     error =   mensAC.setMensaje(MSGERROR , "N" ,1 ,"1000" , 4 , "Certificado alertado", strlen("Certificado alertado"));
       error =   mensAR.setMensaje(MSGERROR , "N" ,1 , "100" , 3 , "Error al generar el certificado",
                                   strlen("Error al generar el certificado"));

       bit->escribePV(BIT_ERROR,"     Certificado alertado %s",cNumSer_al);

       return  OP_CRTENALERTA ;//  Envia(AC);
   }
      
   // 090918 SERR revisar ya que esto nunca pasara 
   if( bd->consultaReg(sConsulta.c_str(), cNumSer_al ) )
   {
      if(bd->getValores("s" ,&sEdoCert))
      {
         if( !strcmp(sEdoCert.c_str(), "R" ) )
         {
            mensIES.setMensajeIES(OP_REVBRDCST,cNumSer_al,strlen(cNumSer_al));
            mensAR.setMensaje(MSGERROR , "N" ,1 , "19" ,2 , "Certificado Alertado");
            bit->escribePV(BIT_ERROR, "     Certificado alertado ya revocado SAT %s",cNumSer_al);
            return Envia(IES);
         }
         else if( !strcmp(sEdoCert.c_str(), "A" ) )
         {
            char cRfc_al[34];
            char cNumTram_al[13];
            if(!DatosCert( cNumSer_al ,cRfc_al))      
            {  
                char cOper[4];
                sprintf(cOper ,"%d",SOLREVOFAR);
                if( CreaTramite(cOper ,cNumTram_al ,cRfc_al) )
                {
                   strcat(cRfc_al, "|00000000000000000000");
                   error = mensAC.setMensaje( SOLREVOFAR , cNumTram_al, strlen(cNumTram_al) ,cRfc_al , strlen(cRfc_al),                                                  cNumSer_al , strlen(cNumSer_al) );  
                   if(error)
                   {
                       bit->escribePV(BIT_ERROR ,"     Error en setMensaje %d",  error);
                       return error;       
                   }
                   error =  Envia(AC);                   
                   if(!error)
                   { 
                      error = ServidorAC(CERTREVAR); 
                      if( mensAC.tipOperacion() == CERTREVAR)
                      {
                          error = Revocacion_AR( cNumTram_al , cNumSer_al ,NULL);
                          if(!error)
                          { 
                             mensIES.setMensajeIES(OP_REVBRDCST,cNumSer_al,strlen(cNumSer_al)); 
                             MarcaDario(cRfc_al,"N");
                             Actualiza_FecRegIES(cfecha_al, cFecACRev_ren );
                             MarcaARA("R", cNumSer_al);
                             mensAR.setMensaje(MSGERROR , "N" ,1 , "19" ,2 , "Certificado Alertado"  );
                             
                          }
                       
                      } 
                   }  
 
                }  
            }
            
         }
      }//getValores
   }// consultaReg
   
   return error;
}


/*##################################################################################

 PROPOSITO: Crea el n�mero de tramite 
 PREMISA  : ******
 EFECTO   : ******
 ENTRADAS : ******
 SALIDAS  : ******

 #################################################################################*/


bool  Mediador::
CreaTramite(char *cOper , char * cNumTram_al ,char *cRfc )
{
   string sOper;
   
   if ( bd->ejecutaSP("SP_GENNUMOPER" ,"'%s','AR_SAT'", cOper ) ) // Operacion
   {
   
      if (bd->getValores("s",&sOper))
      {
          if( bd->ejecutaSP("SP_GENNUMTRAM" , "'%d','%s','%s','AR_SAT','ALERTADO','ALERTADO','ALERTADO'", 
                            atoi(cOper),sOper.c_str(),cRfc ))         
          {
             if (bd->getValores("b",cNumTram_al))
             {
                return true;
             }
             
          } 
           
      }
 
   }
   return false;
 
}


/*##################################################################################

 PROPOSITO: Lee de un archivo un numero determiniado de bytes
 PREMISA  : ******
 EFECTO   : ******
 ENTRADAS : ******
 SALIDAS  : ******

 #################################################################################*/


long Mediador::
leeLong( int fd, long &numLeer)
{
        ssize_t rc;
        char c, strLong[100];
        long i = 0;
        for ( ;;)
        {
             if ( (rc = read(fd,&c,1)) == 1 )
             {
                 strLong[i++] = c;
                 if (  c == ' ' )
                    break;
             }
             else if (rc == 0 )
             {   
                 if ( i == 1 )
                     return (0);
                  break;
              }
              else
              {
                 if ( errno == EINTR )
                    continue;
                 return (-1);
              }
        }
        strLong[i] = 0;
        numLeer = atol(strLong);
        return 0;
}


/*##################################################################################

 PROPOSITO: Lee el buffer del fifo.
 PREMISA  : ******
 EFECTO   : ******
 ENTRADAS : ******
 SALIDAS  : ******

 #################################################################################*/

long Mediador::
leeBuffer(int fd, char *vptr, long cantBytes)
{
        long n , rc;
        char c, *ptr;
        ptr = vptr;
        for ( n = 1; n <= cantBytes ; n++)
        {
             if ( (rc = read(fd,&c,1)) == 1 )
                 *ptr++ = c;
             else if (rc == 0 )
             {
                 if ( n == 1 )
                     return (0);
                  break;
              }
              else
              {
                 if ( errno == EINTR )
                    continue;
                 return (-1);
              }
        }
        *ptr = 0;
        return (n);
}

/*##################################################################################

 PROPOSITO: Recibe MSGERROR
 PREMISA  : ******
 EFECTO   : ******
 ENTRADAS : ******
 SALIDAS  : ******

 #################################################################################*/

int Mediador::
RecibeMSGERROR()
{
 
  char  cTipError[2]; int iTipError = sizeof(cTipError);
  char  cCodError[6]; int iCodError = sizeof(cCodError);
  char  cDesc[2048];  int iDesc     = sizeof(cDesc);

  iNumTram   = sizeof(cNumTram);
  ifirma     = sizeof(firma);
  iTipError  = sizeof(cTipError);

  error = mensAC.getMensaje(MSGERROR, firma ,&ifirma ,cTipError , &iTipError ,
                            cCodError , &iCodError , cDesc ,&iDesc );
  if(error)
    return error;
  
  error = mensAR.setMensaje(MSGERROR, cTipError , iTipError, cCodError, iCodError, cDesc ,iDesc);
  if(error)
    return error;
  
  RegistraOper(atoi(cCodError) ,cNumTram, MSGERROR );
  if(!strncmp(cDesc,"Operaci�n inv�lida", strlen("Operaci�n inv�lida")) )
  {
     conectAC = false;
     bit->escribe(BIT_ERROR,"    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
     bit->escribePV(BIT_ERROR,"    ~~                Operaci�n Inv�lida  %s                  ~~" , cCodError);
     bit->escribe(BIT_ERROR,"    ~~EL AR_MEDIADOR SE CERRARA POR DESINCRONIZACION CON LA AC~~");
     bit->escribe(BIT_ERROR,"    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
     bit_ef->escribe(BIT_ERROR , "AR_MEDIADOR DESINCRONIZADO CON LA AC");
     Envia(AR);
     Termina();
     exit(0);
     return DESCONEXIONDELAAC;        
  }      
     
  bit->escribePV(BIT_ERROR,"     %s",cDesc);
  return error;

}

/*##################################################################################

 PROPOSITO: Registra Operacion
 PREMISA  : ******
 EFECTO   : ******
 ENTRADAS : ******
 SALIDAS  : ******

 #################################################################################*/


int Mediador::
RecibeCERTREVAR()
{
  char  cNum_t[1024];  int iNum_t     = sizeof(cNum_t);
 

  ifirma = sizeof(firma);
  memset(cert.VigFin.cont,0,cert.VigFin.icont = sizeof(cert.VigFin.cont));
  // V.1.1.1.1 (080422) SERR: Se recibe de la AC el nuevo mensaje que incluye la vigencia final del certificado despues de haber sido revocado.
  error = mensAC.getMensaje(CERTREVARAC, firma ,&ifirma ,cNum_t ,&iNum_t, cert.VigFin.cont, &cert.VigFin.icont );
  if(error)
  {
    ErrorAR_AC(AR,"Error al obtener el mensaje." ,error , "I");
    return error;
  }
  RegistraOper(0 ,cNumTram, CERTREVARAC );
  error = mensAR.setMensaje(CERTREVAR, cNum_t, iNum_t);
  if(error)
  {
    ErrorAR_AC(AR,"Error al obtener el mensaje." ,error , "I"); 
    return error;
  }

  RegistraOper(0 ,cNumTram, CERTREVAR  );
  return error;



}
 

/*##################################################################################

 PROPOSITO: Registra Operacion
 PREMISA  : ******
 EFECTO   : ******
 ENTRADAS : ******
 SALIDAS  : ******

 #################################################################################*/

int Mediador::
RegistraOper(int iError ,char *cTramite, int iOper )
{
  
   if (bd->ejecutaSP("SP_REGOPERDET", "'%s',%d,%d", cTramite, iOper ,iError ))
      return 0;
   else
      return -1;

}

/*##################################################################################

 PROPOSITO: Revoca el Certificado en la AR
 PREMISA  : ******
 EFECTO   : ******
 ENTRADAS : ******
 SALIDAS  : ******

 #################################################################################*/

int Mediador::
BROADCAST_IES()
{
   error = SIN_OPER;

   char   cFechaRev  [30];
   char   cNumSerRev [TAM_NS_M];
   char   cSentencia [300];
   char   fecha_IES_t[20];

   int    iFechaRev   =  sizeof(cFechaRev);
   int    iNumSerRev  =  sizeof(cNumSerRev);

   memset(cFechaRev   ,0, iFechaRev);
   memset(cNumSerRev  ,0, iNumSerRev);
   memset(cSentencia  ,0, sizeof(cSentencia));
   memset(firma       ,0, ifirma = sizeof(firma));
   memset(fecha_IES_t ,0, sizeof(fecha_IES_t));

   error =  mensIES.getMensajeIES( OP_REVBRDCST ,firma ,&ifirma ,cFechaRev , &iFechaRev ,cNumSerRev , &iNumSerRev );
   if(error)
   {
      bit->escribe(BIT_ERROR,"     Error al obtener el mensaje de la IES OP_REVBRDCST");
      return error;
   }

   bit->escribePV(BIT_INFO, "OP_REVBRDCST %s", cNumSerRev);

   //cNumSerRev[20] = 0;
 
   /*
   string sNumAR;
   conf->getValorVar("[IES]","NUMAR" ,&sNumAR);

   
   if( strncmp(cNumSerRev , sNumAR.c_str() , sNumAR.length() ) != 0)//Verifica que el n�mero de serie no sea del SAT
   {
     
      fec.FechaCal( cFechaCal , atoi( cFechaRev));
      fec.FechaBD ( cFechaCal , cFechaBD);
      cFechaBD[19] = 0;
  */    
     // >>> MAML 070727 : actualizamos el campo fec_reg_ies en la AR, oper=REVOCACION, los campos edo_cer y vig_fin YA fueron actualizados antes.
     Actualiza_FecRegIES(cFechaRev, fecha_IES_t );
     if ( !bd->ejecutaOper( "UPDATE CERTIFICADO SET FEC_REG_IES = '%s' WHERE NO_SERIE = '%s';",
                            fecha_IES_t, cNumSerRev ) )
     {
        bit->escribePV(BIT_INFO,"No se hizo el update en la AR para fec_reg_ies=%s del no_serie=%s de la Revocacion. ver bitacora de BD.", 
                                fecha_IES_t, cNumSerRev);
        return -1;
     }
     // <<<   
   //}
   
   return 0;
     

}

/*##################################################################################

 PROPOSITO: Revoca el Certificado en la AR: Se usa pra las 2 operaciones: REV y REN
 PREMISA  : ******
 EFECTO   : ******
 ENTRADAS : ******
 SALIDAS  : ******

 #################################################################################*/

int Mediador::
Revocacion_AR( char * cNumTram_f  , char * cNumSerie_f , char * cFecRev_f )
{
    char   query[256];
       
    if ( !esRenovacion ) // Es una Revocacion
    {  
       //RORS vie mar 14 10:46:09 CST 2008
       string sFecRev;
       
       if( cFecRev_f == NULL  )
       {
          int error = fec.GetCurrentUTC( &sFecRev ); 
          if ( error != 0 ) 
          {
             ErrorAR_AC(AR,"Error al obtener la fecha de vigencia final para la Revocacion" , error , "I");
             return ERR_REVCERT;
          }
          sprintf ( query, "UPDATE CERTIFICADO SET  EDO_CER = 'R', VIG_FIN ='%s' WHERE NO_SERIE = '%s';", sFecRev.c_str(), cNumSerie_f );
       }
       else 
       {
          sprintf ( query, "UPDATE CERTIFICADO SET  EDO_CER = 'R', VIG_FIN ='%s' WHERE NO_SERIE = '%s';", cFecRev_f, cNumSerie_f );
       }
    }
    else // Es una Renovacion : De la AC recibio la Fecha de vigencia final en UTC
    {

       // RORS   vie mar 14 12:58:52 CST 2008   Se   Quita Actualiza_FecRegIES ya que esta funcion no se esta utilizando como es debido ya que
       // esta funcion necesita la fecha en formato como la manda la IES y no en formato de BD como se esta utilizando.

       /*  
          char   fecha_IES_t[20];

          memset(fecha_IES_t ,0, sizeof(fecha_IES_t)); 
          Actualiza_FecRegIES(cFecACRev_ren, fecha_IES_t ); // convertir a hora local la fecha de vig_fin y guardarlo en el campo fec_reg_ies de la AR

          sprintf ( query, "UPDATE CERTIFICADO SET  EDO_CER = 'R', VIG_FIN ='%s' , FEC_REG_IES ='%s' WHERE NO_SERIE = '%s';",
                        cFecACRev_ren, fecha_IES_t, cNumSerie_f );
       */
       sprintf ( query, "UPDATE CERTIFICADO SET  EDO_CER = 'R', VIG_FIN ='%s' , FEC_REG_IES ='%s' WHERE NO_SERIE = '%s';",
                        cFecACRev_ren, cFecACRev_ren, cNumSerie_f );
    }

    
    if( !bd->ejecutaOper(query))
    {
       if (esRenovacion)
          ErrorAR_AC(AR, "Error en la BD en Revocaci�nxRenovacion.", 102 , "I");
       else
          ErrorAR_AC(AR, "Error en la BD en Revocaci�n.", 102 , "I");
       return ERR_EN_BD;  
    }


    RegistraOper(0 ,cNumTram, CERTREVAR);
    mensAR.setMensaje(CERTREVAR, cNumTram_f, strlen(cNumTram_f));
    bit->escribePV(BIT_INFO, (esRenovacion)? "     Certficado RevocadoxRenovacion en la AR %s" : "     Certficado Revocado en la AR %s", cNumSerie_f ); 
    return 0;

}
/*##################################################################################

 PROPOSITO: Arma y envia la revocaci�n a la AC.
 PREMISA  : ******
 EFECTO   : ******
 ENTRADAS : ******
 SALIDAS  : ******

 #################################################################################*/



int Mediador::
Revocacion_AC()
{
   error= SIN_OPER;
   string sError;

   if(mensAR.tipOperacion()== SOLREVOFAR)
      error = mensAC.setMensaje(SOLREVOFAR ,cNumTram,iNumTram ,cSec ,iSec, rfc_contr ,irfc_contr ,cert.NumSer.cont,cert.NumSer.icont);
   else if ( mensAR.tipOperacion()== SOLREVAR)
      error = mensAC.setMensaje(SOLREVAR,cNumTram,iNumTram,cSec ,iSec ,rfc_contr, irfc_contr , 
                                         cert.NumSer.cont,cert.NumSer.icont, pwdrev,ipwdrev);
   else if ( mensAR.tipOperacion()== SOLREVFAR)
      error = mensAC.setMensaje(SOLREVFAR,cNumTram,iNumTram,cSec ,iSec ,rfc_contr, irfc_contr , 
                                         cert.NumSer.cont,cert.NumSer.icont);
   if(error != 0)
   {
      ErrorAR_AC(AR,"Error al obtener los datos del mensaje Revocacion_AC" , error , "I");
      return ERR_REVCERT;
      
   }

   RegistraOper(0 ,cNumTram, mensAC.tipOperacion() );
   bit->escribePV(BIT_INFO,"     Envio de revocacion a la AC  %s", cert.NumSer.cont);
   return Envia(AC);

}


/*##################################################################################

 PROPOSITO: Arma y envia  mensaje de revocacion a la IES.
 PREMISA  : ******
 EFECTO   : ******
 ENTRADAS : ******
 SALIDAS  : ******

 #################################################################################*/


int Mediador::
Revocacion_IES()
{
   error = mensIES.setMensajeIES(OP_REVBRDCST,cert.NumSer.cont,strlen(cert.NumSer.cont));
   if(error != 0)
   {
      ErrorAR_AC(AR,"Error en setMensaje OP_REVBRDCST" , error , "I");
      ErrorAR_AC(AC,"Error en setMensaje OP_REVBRDCST" , error , "I");
      return ERR_REVCERT;
   }
   RegistraOper(0 ,cNumTram, OP_REVBRDCST +  1000  );
   bit->escribePV(BIT_INFO, "     Envio de Revocacion a la IES %s",cert.NumSer.cont);
   return Envia(IES);

}

/*##################################################################################

 PROPOSITO: Recibe el mensaje de revocaci�n.
 PREMISA  : ******
 EFECTO   : ******
 ENTRADAS : ******
 SALIDAS  : ******

 #################################################################################*/

int Mediador::
RecibeRev()
{
   error = SIN_OPER;
   string sError;

   memset(firma      ,0, ifirma     = sizeof(firma));
   memset(cNumTram   ,0, iNumTram   = sizeof(cNumTram));
   memset(rfc_contr  ,0, irfc_contr = sizeof(rfc_contr));
   memset(pwdrev     ,0, ipwdrev    = sizeof(pwdrev));
   memset(cSec       ,0, iSec       = sizeof(cSec) );
   
   

   cert.NumSer.icont = sizeof(cert.NumSer.cont);


   if(mensAR.tipOperacion()== SOLREVOFAR)
   {
      bit->escribe(BIT_INFO,"--> SOLREVOF");   
      error = mensAR.getMensaje(mensAR.tipOperacion(),firma,&ifirma,cNumTram, &iNumTram , cSec , &iSec ,
                                                      rfc_contr,&irfc_contr,
                                                      cert.NumSer.cont,&cert.NumSer.icont);
   }
   else if (mensAR.tipOperacion()== SOLREVAR )
   {
     bit->escribe(BIT_INFO,"--> SOLREV");   
     error = mensAR.getMensaje(mensAR.tipOperacion(),firma , &ifirma, cNumTram, &iNumTram, cSec , &iSec ,
                                                     rfc_contr, &irfc_contr,
                                                     cert.NumSer.cont ,&cert.NumSer.icont , pwdrev, &ipwdrev);
   }
   else  if (mensAR.tipOperacion()== SOLREVFAR )
   {
     bit->escribe(BIT_INFO,"--> SOLREVF");   
     error = mensAR.getMensaje(mensAR.tipOperacion(),firma , &ifirma, cNumTram, &iNumTram, cSec , &iSec ,
                                                     rfc_contr, &irfc_contr,
                                                     cert.NumSer.cont ,&cert.NumSer.icont);
   }
   if(error != 0)
   {
      ErrorAR_AC(AR,"Error al obtener los datos del mensaje  RecibeRev" , error , "I");
      return ERR_REVCERT;
   }
   RegistraOper(0 ,cNumTram, mensAR.tipOperacion() );
   bit->escribePV(BIT_INFO,"     numt: %s \n%59s %s\n %64s %s",cNumTram,"rfc: ",rfc_contr,"num_serie: ", cert.NumSer.cont);
   return error;
}


/*##################################################################################

 PROPOSITO: Flujo principal de revocacion
 PREMISA  : ******
 EFECTO   : ******
 ENTRADAS : ******
 SALIDAS  : ******

 #################################################################################*/

int Mediador::
Revocacion()
{

    error = RecibeRev();
    if(!error)
    { 
       error = Revocacion_AC();
       if(!error)
       {
          // V.1.1.1.1 (080422) SERR: Se cambio el mensaje que se recibe de la AC CERTREVARAC 
          error = ServidorAC(CERTREVARAC);
          if(!error)
          {    
             error = Revocacion_AR(cNumTram, cert.NumSer.cont ,cert.VigFin.cont);
             if(!error)
             {   
		 //>+ ERGL(070125)
                 ObtieneDatosRevocacion(cert.NumSer.cont, cert.RFC.cont, cert.TipCer.cont);
                 //>>>
		           MarcaARA("R");
                 if (atoi(cert.TipCer.cont) == 1 ) //>>> MAML (070530): Solo cert. de FIEL marcan en DARIO
                    MarcaDario(cert.RFC.cont,"N");
                 // << 17 mayo 2006  Los agentes no se revocan en la IES. 
                 //MAML 27/Dic/06: solo de FEA y Sellos. IF (cert.TipCer.cont != "") => OK poner if ELSE Hacer query SELECT  
		 //if (atoi(cert.TipCer.cont) == 1 || atoi(cert.TipCer.cont) == 2)
                 //bit->escribe(BIT_INFO,"     DEBUG: Revocacion(): cert.TipCer.cont=%d, cert.RFC.cont=%s", 
                 //            atoi(cert.TipCer.cont), cert.RFC.cont );
		 //>+ ERGL(070125)
		          if (atoi(cert.TipCer.cont) == 1 || atoi(cert.TipCer.cont) == 2)
		              error = Revocacion_IES();
		 /*>- ERGL(070125)
                 if (strncmp("000010999999" , cert.NumSer.cont ,12)) 
                    error = Revocacion_IES();
                 -<*/
                 Respuesta();
                 return 0;
             }
          }
       }
    }

    Respuesta();
    return 0;
}
   

/*##################################################################################

 PROPOSITO: Deja las marcas para la ARA y FTP's.
 PREMISA  : ******
 EFECTO   : ******
 ENTRADAS : ******
 SALIDAS  : ******

 #################################################################################*/


void  Mediador::
MarcaARA(char *cEstado , char *cNumSer )
{
   
   char cNomArch[256];
   char cDatos[90];
   string sARA;
   int iArch;

   conf->getValorVar("[MED]","ARA" ,&sARA);
   
   if(cEstado == "A")
     sprintf(cNomArch,"%s%s.A",sARA.c_str() , cert.NumSer.cont);
   else if(cEstado == "R")
   {
     if( cNumSer == NULL )   
        sprintf(cNomArch,"%s%s.R",sARA.c_str() , cert.NumSer.cont);
     else
        sprintf(cNomArch,"%s%s.R",sARA.c_str() , cNumSer);
   }


   iArch = open(cNomArch ,O_WRONLY|O_CREAT,S_IRWXU);
   if(iArch < 0 )
   { 
      bit->escribePV(BIT_INFO,"     PendientesARA : Open  %d",errno);
      return;
   }

   if(cEstado == "A")
   {

      sprintf(cDatos,"A|%s|%s|%s|%s|%s|%s",cert.NumSer.cont , cert.Edo.cont   , cert.TipCer.cont, 
                                     cert.VigIni.cont , cert.VigFin.cont, cert.RFC.cont);

      write(iArch,cDatos,strlen(cDatos));
      close(iArch);

   }
   else if( cEstado == "R" || cEstado == "L" || cEstado == "C")
   {
       // RORS 14 marzo 2008 Se agrego Fecha Rev ya que estaba actualizando con current
      if(cNumSer == NULL) 
         sprintf(cDatos,"%s|%s|%s" , cEstado, cert.NumSer.cont,cert.VigFin.cont);  
      else
         sprintf(cDatos,"%s|%s|%s" , cEstado, cNumSer,cFecACRev_ren);
      write(iArch ,cDatos,strlen(cDatos));
      close(iArch);
   }
   else  close(iArch);

   bit->escribe(BIT_INFO,"     Marca de ARA : OK");

}

/*##################################################################################

 PROPOSITO: Deja las marcas para el registro en Dario.
 PREMISA  : ******
 EFECTO   : ******
 ENTRADAS : ******
 SALIDAS  : ******

 #################################################################################*/

int Mediador::
MarcaDario( char *cRfc , char *cEdoCert)
{
   error = SIN_OPER;
   char  cNomArch[256];
   int iArch;
   string sDario;

   conf->getValorVar("[MED]","MARCASFIEL" ,&sDario);

   sprintf( cNomArch ,"%s%s",sDario.c_str() , cRfc );

   iArch = open(cNomArch ,O_WRONLY|O_CREAT,S_IRWXU);
   if(iArch < 0)
   {
      bit->escribePV(BIT_ERROR,"     No se abri� el archivo errno %d" , errno);
      return  errno;
   }
   write(iArch , cEdoCert ,strlen(cEdoCert));
   close(iArch);
   bit->escribe(BIT_INFO,"     MarcaFiel : OK");
   return 0;
}

/*##################################################################################

 PROPOSITO: Construir la clase.
 PREMISA  : ******
 EFECTO   : ******
 ENTRADAS : ******
 SALIDAS  : ******

 #################################################################################*/

int Mediador ::
EnviarAR()
{

   error = SIN_OPER;
   int iOper = mensAC.tipOperacion();
   
   if(iOper == CERTREGAC)
   {
      iOper = CERTGENAR;
      if(!strcmp(cert.TipCer.cont,"2") )
       //sprintf(rfc_contr + strlen(rfc_contr), "|%s" ,cert.NumSer.cont);   
         sprintf(rfc_contr + strlen(rfc_contr), "|%s|" ,cert.NumSer.cont); // MAML 100412: la AR usa la clase CSeparaToken y necesita terminar en | 
   }
   else if(iOper == CERTRENAC)
   {
   // sprintf(rfc_contr + strlen(rfc_contr), "|%s / %s" ,cert.NumSer.cont, cNumSerie_ren);   
      sprintf(rfc_contr + strlen(rfc_contr), "|%s / %s|" ,cert.NumSer.cont, cNumSerie_ren); // MAML 100412
      iOper = CERTRENAR;
   }
      
   error = mensAR.setMensaje(iOper,rfc_contr ,strlen(rfc_contr) ,cert.cert.cont,cert.cert.icont);
   if(error != 0)
   {
      ErrorAR_AC(AR,"En SetMensaje EnviaCertificado" ,error , "I");
      return ERR_GENCERT;
   }
   
   RegistraOper(0,cNumTram,iOper);
   error  = Envia(AR);
   if(error)
   {
      enviadoAR = false;
      ErrorAR_AC(AR,"Enviar Certificado a la AR" ,error , "I");
      return ERR_GENCERT;
   }
   enviadoAR = true;
   bit->escribe(BIT_INFO,"     EnviaCertificadoAR : OK");
   return error;

}

/*##################################################################################

 PROPOSITO:  Registra los datos del certificado en  la BD de la AR.
 PREMISA  : ******
 EFECTO   : 
 ENTRADAS : 
 SALIDAS  : ******

 #################################################################################*/

int Mediador::
RegistroBD()
{
   string sNumTram_bd; 
   char cCadTrat[1024];

   bd->prepCadDelim(1 , cert.Nombre.cont , cCadTrat);  

   string sFormato = "'%s',%s,'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'";

   string sIns = "INSERT INTO CERTIFICADO(no_serie, tipcer_cve, edo_cer, rfc, curp, nombre, correo, rfc_rl, curp_rl, vig_ini, vig_fin, fec_reg_ies, dig_pubk_md5, dig_pubk_sha1, es_sat, tramite_cve ,pubk) VALUES (" + sFormato + " )";


   if(!conRepr)
   {
       cert.RFCRL.cont[0]   = '\0';
       cert.RFCCurp.cont[0] = '\0';
   }

   if(bd->consultaReg("SELECT TRAMITE_CVE FROM TRAMITE WHERE SOLOPER_CVE = '%s'", cNumTram))
   {
      if( !bd->getValores("s" , &sNumTram_bd) )
      {
         ErrorAR_AC(AR,"Error al obtener el n�mero de tramite." ,102 , "I");
         return -1;
      }
   }   

   if (bd->ejecutaOper( sIns.c_str() ,  cert.NumSer.cont , cert.TipCer.cont ,cert.Edo.cont , cert.RFC.cont ,
                                        cert.Curp.cont   , cCadTrat ,cert.Correo.cont , cert.RFCRL.cont ,
                                        cert.RFCCurp.cont , cert.VigIni.cont ,cert.VigFin.cont , cert.FecRegIES.cont,                                             cert.LlaveMD5.cont, cert.LlaveSHA1.cont, "S" ,sNumTram_bd.c_str() ,
                                         cert.LlavePub.cont ) )
   {   
 
       bit->escribePV(BIT_INFO,"     Certificado Registrado en la BD de la AR %s " ,cert.NumSer.cont );
       return 0;
   }
   else
   {   
       ErrorAR_AC(AR,"Al registrar el certificado en la BD de la AR" ,102 , "I");
       return -1;
   }
   
   
}

/*##################################################################################

 PROPOSITO:
 PREMISA  : ******
 EFECTO   : ******
 ENTRADAS : ******
 SALIDAS  : ******

#################################################################################*/
int Mediador::
GuardaCert()
{
   error = SIN_OPER;
   int iArch;
   string sRuta;
   string sRutaFin;

   conf->getValorVar("[MED]","REPOSITORIO"  ,&sRuta);

   pkiRutaCert( sRuta.c_str() , cert.NumSer.cont  , sRutaFin ,true);
    
   iArch  = open(sRutaFin.c_str(), O_WRONLY|O_CREAT,S_IRWXU);
   if(iArch < 0)
   {
      bit->escribe(BIT_INFO,sRutaFin.c_str());
      ErrorAR_AC(AR,"CrearArchivo en repositorio." ,errno , "I");  
      return errno;
   }
   
   if (write( iArch,cert.cert.cont, cert.cert.icont ) < 0) 
   {
      close(iArch);
      ErrorAR_AC(AR,"Al escribir en el archivo" ,errno , "I");
      return errno;   
   }
   close(iArch);
   bit->escribePV(BIT_INFO,"     CertEnRepositorio:\n%65s %s\n%61s %s\n%64s %s",
              "No.Serie : ",cert.NumSer.cont,"Ruta : ",sRutaFin.c_str(),"Tramite  :", cNumTram);
   
   return 0; 
}

/*##################################################################################

 PROPOSITO:
 PREMISA  : ******
 EFECTO   : ******
 ENTRADAS : ******
 SALIDAS  : ******

 #################################################################################*/

int Mediador::
RegistraAC(bool aceptado )
{
   error = SIN_OPER;
   char cError[10];
   
   enRegistraAC = true;

   
   if(aceptado)
   {
      RegistraOper(0,cNumTram, CERTREGIES);   
      error = mensAC.setMensaje(CERTREGIES,cNumTram,iNumTram);
   }
   else
   {   
      RegistraOper(0,cNumTram, CERTNOREGIES);
      sprintf(cError , "%d" , CERTNOREGIES);
      error = mensAC.setMensaje(MSGERROR ,"O" ,1 , cError , strlen(cError) ,"El certificado no fu� aceptado en la IES",
                               strlen("El certificado no fu� aceptado en la IES")  ); 

      ErrorAR_AC(AR,"El certificado no fu� aceptado en la IES" ,CERTNOREGIES , "N");
   }
   if(error != 0)
   {
      ErrorAR_AC(AR,"ALERTA : GenCert:setMensajeAC:CERTREGIES" ,error , "I");
      ErrorAR_AC(AC,"ALERTA : GenCert:setMensajeAC:CERTREGIES" ,error , "I");
      return ERR_GENCERT;
   }
   error = Envia(AC);
   if(error != 0)
   {
      ErrorAR_AC(AR,"ALERTA : GenCert:EnviaAC" ,error , "I");
      return error;
   }
   if(aceptado)
   {
      //>>> ERGL (070330)
      if( !ServidorAC( (mensAR.tipOperacion() == SOLCERTAR) ? CERTREGAC : CERTRENAC ) ) 
      { 
      //if(!ServidorAC())
         return 0;
      }
      //<<< ERGL
      return -1; // temporal colocar una variable de error.
   }
      
   return EXITO;


}

/*##################################################################################

 PROPOSITO:
 PREMISA  : ******
 EFECTO   : ******
 ENTRADAS : ******
 SALIDAS  : ******

 #################################################################################*/
int Mediador::
CertAcept_Rech( int iAoR)
{
   char cFecRegIES [30];
   char cNumSerie  [21];
   char fecha_IES  [20];
   char fecha_IES_t[20];

   int  iNumSerie  = sizeof(cNumSerie); 
   int  iFecRegIES = 4;


   memset(firma      ,0  ,ifirma  = sizeof(firma));
   memset(cFecRegIES ,0  ,sizeof(cFecRegIES));
   memset(fecha_IES  ,0  ,sizeof(fecha_IES));
   memset(fecha_IES_t,0  ,sizeof(fecha_IES_t));
   memset(cNumSerie  ,0  ,sizeof(cNumSerie));

   error = mensIES.getMensajeIES(iAoR , firma , &ifirma ,cFecRegIES ,&iFecRegIES ,cNumSerie ,&iNumSerie);
   if(error)
   {
      ErrorAR_AC(AR,"En  getMensajeIES CertAcept_Rech" ,error , "I");
      ErrorAR_AC(AC,"En  getMensajeIES CertAcept_Rech" ,error , "I");
      return error;
   }

   cNumSerie[20] = 0;
   //<< 5 julio 2006

   if(strcmp(cert.NumSer.cont , cNumSerie ))
   {
      bit->escribePV(BIT_INFO,"No serie Generado #%s#\n No Serie Recibido #%s#", cert.NumSer.cont, cNumSerie);
      ErrorAR_AC(AR,"Al comparar los numeros de serie a procesar y recibido." , -10 , "I");
      ErrorAR_AC(AC,"Al comparar los numeros de serie a procesar y recibido." , -10 , "I");
      return -10;
   }

   Actualiza_FecRegIES(cFecRegIES, fecha_IES_t ); // Es una Generacion / Renovacion del nuevo Certificado
   cert.FecRegIES.icont = strlen(fecha_IES_t);
   strcpy(cert.FecRegIES.cont,fecha_IES_t);

   return 0;
}

/*##################################################################################

PROPOSITO: MAML 070727 : Actualiza la fecha de Registro en la IES con la hora local en Generacion � Revocacion 
PREMISA  : ******
EFECTO   : ******
ENTRADAS : ******
SALIDAS  : ******

#################################################################################*/
int Mediador::
Actualiza_FecRegIES(char *cFecRegIES, char *fecha_IES_t )
{
   //>>> 02.00.02 (070313) GHM: Cambios para el manejo de errores y su escritura en el bit�cora
   //>>> 02.00.01 (070219) GHM: Modificaciones para utilizar la librer�a Sgi_Fecha
   //fec.FechaCal(fecha_IES, atoi(cFecRegIES)); //- 02.00.01
   //fec.FechaBD (fecha_IES, fecha_IES_t);      //- 02.00.01
   time_t tfechAct = atoi(cFecRegIES);
   //struct tm *sfechAct; //- 02.00.01
   //int errorfec = 0; //+ 02.00.02
   //struct tm *sfechAct = fec.FechaCal(&tfechAct); //- 02.00.02
   //>- struct tm *sfechAct = fec.FechaCal(&tfechAct, &errorfec); // ERGL (070328) FEC_reg_ies debe ser LOCAL.
   struct tm *sfechAct = localtime(&tfechAct);
   fec.strTimeTOfechBD(sfechAct, fecha_IES_t);
   // MAML 070727 : La fec_reg_ies se pone en hora local. NO viene en UTC, VIENE en el Mensaje de la IES // Si es UTC usar CFecha::TransFecha() 
#ifdef DBG   
   ;//bit->escribePV(BIT_INFO, "     Actualiza_FecRegIES( cFecRegIES=%s, FechaHoraLocal=%s ); ", cFecRegIES, fecha_IES_t ); 
#endif
   //<<< 02.00.01
   return 0;
}
/*##################################################################################

PROPOSITO: Arma el mensaje para  registrar el certificado ,lo envia y recibe de la IES
PREMISA  : ******
EFECTO   : ******
ENTRADAS : ******
SALIDAS  : ******

#################################################################################*/


bool  Mediador::
RegistraIES()
{
	static  char cCertIES[TAM_DATOS];
	int    iCertIES = sizeof(cCertIES);
	int     iOperacion;

	error = ReqCert.der2pem((uchar*)cert.cert.cont,cert.cert.icont,(uchar*)cCertIES,&iCertIES);
	if(error != 0)
	{
		ErrorAR_AC(AR,"En der2pen" ,error , "I");
		ErrorAR_AC(AC,"En der2pen" ,error , "I");
		return ERR_GENCERT;
	}

	error = mensIES.setMensajeIES(OP_ALTACRT,cert.PwdRev.cont,cert.PwdRev.icont,cCertIES,iCertIES);
	if(error != 0)
	{
		ErrorAR_AC(AR,"En setMensaje OP_ALTACRT" ,error , "I");
		ErrorAR_AC(AC,"En setMensaje OP_ALTACRT" ,error , "I");
		return ERR_GENCERT;
	}


   iBuffIESAct  = sizeof(cBuffIESAct);
   memset( cBuffIESAct, 0, iBuffIESAct);

   mensIES.setDatos( cBuffIESAct, &iBuffIESAct); 
   //memcpy(cBuffIESAct, mensIES.buffer, iBuffIESAct);
 
	error = Envia(IES);
	if(error != 0)
	{
		ErrorAR_AC(AR,"En Envia IES" ,error , "I");   
		ErrorAR_AC(AC,"En Envia IES" ,error , "I");   
		return ERR_GENCERT;
	}
   bit->escribe(BIT_INFO,"     Se envio solicitud de registro a la IES");

	RegistraOper(0,cNumTram, OP_ALTACRT + 1000);
	for(;;)
	{
		ServidorIES();
		iOperacion = mensIES.tipOperacion();

		if(iOperacion == OP_CRTACEPTADO)
		{
			return true;
		} 
		else if(iOperacion == OP_CRTRECHAZADO)
		{
			return false;
		}
		else if(iOperacion == OP_CRTENALERTA)
		{
			continue;//<< 18-mayo-2006 Cambio porque se debe tomar como numero de salida el rechazado o aceptado return false;
		}
      else if( iOperacion == OP_ALTACRT )
      {
         break; // 18 julio 2008 RORS Se ciclaba cuando llegaba la misma operacion.
      }
      else if( iOperacion == SIN_OPER)//129
      {
         ErrorAR_AC(AC,"ALERTA : GenCert:RegistraIES:Desconexion con  la IES." ,iOperacion , "I");
         ErrorAR_AC(AR,"ALERTA : GenCert:RegistraIES:Desconexion con  la IES." ,iOperacion , "I");
         bit->escribe(BIT_INFO," ============================================================= ");
         bit->escribe(BIT_INFO," ===============    REINICIAR AR_MEDIADOR  =================== ");
         bit->escribe(BIT_INFO," ============================================================= ");
         return false;
      }
		else continue;
	}

	bit->escribePV(BIT_INFO,"     RegistraIES     : %s",cert.NumSer.cont);
	return false;
}


/*##################################################################################

PROPOSITO:
PREMISA  : ******
EFECTO   : ******
ENTRADAS : ******
SALIDAS  : ******

#################################################################################*/


int Mediador::
ServidorIES()
{
	error = SIN_OPER;
   mensIES.mensaje->encabezado.operacion = (uint8)SIN_OPER;
 
//   char  cBuffIESAct[TAM_DATOS];
//   int   iBuffIESAct = sizeof();
//   memset ( cBuffIESAct ,0,sizeof(cBuffIESAct));
//   memcpy(cBuffIESAct, mensIES.buffer, lbufferIES);

	if(errno == ECONNRESET)
	{
		bit->escribePV(BIT_ERROR," %s errno = %d socket = %d",strerror(errno),errno,sktIES->getSocket());
		sleep(2);
		bit->escribePV(BIT_INFO,"Reconexion IES");
		error = ConectaIES();
		if(error)
		{
			bit->escribePV(BIT_ERROR,"No se pudo reconectar a la IES error= %d",error);
			return error;
		}
		bit->escribe(BIT_INFO,"Reconexion efectuada");

/*
      memcpy(mensIES.buffer, cBuffIESAct , lbufferIES);

      error = Envia(IES);
      if (!error)
          bit->escribePV(BIT_INFO,"Se envio la peticion %d",mensIES.tipOperacion());
      else
         bit->escribePV(BIT_ERROR,"Se envio la peticion %d",error);
*/
	}

	if(para_ies == true )
	{
		error =  sktIES->LecPendiente();
		if(!error )
			return NO_PEND;
	}

	error = Recibe(IES);
   if (error == ERR_SKTTIMEOUT)
   {
      //bit->escribe(BIT_INFO,"Se termino el tiempo de espera para recibir del Socket");  
      return error;
   }
 
	if(error)
		return error;

	switch(mensIES.tipOperacion())
	{

		case OP_CRTACEPTADO :{ 
					     bit->escribe(BIT_INFO,"--> OP_CRTACEPTADO");
					     RegistraOper(0,cNumTram, OP_CRTACEPTADO + 1000);
					     return  CertAcept_Rech(OP_CRTACEPTADO);  
					     break;
				     }
		case OP_CRTRECHAZADO:{
					     bit->escribe(BIT_ERROR,"--> OP_CRTRECHAZADO");
					     RegistraOper(OP_CRTRECHAZADO ,cNumTram, OP_CRTRECHAZADO +1000 );
					     return CertAcept_Rech(OP_CRTRECHAZADO);
					     break;
				     }
		case OP_CRTENALERTA:{
					    bit->escribe(BIT_ERROR,"--> OP_CRTENALERTA");
					    RegistraOper(OP_CRTENALERTA ,cNumTram, OP_CRTENALERTA + 1000 );
					    CertAlertaIES(esGeneracion);
					    return OP_CRTENALERTA;
					    break;
				     }
		case OP_REVBRDCST:  {
					    bit->escribe(BIT_INFO,"--> OP_REVBRDCST");
					    BROADCAST_IES();   
					    break; 
				     }
		case OP_PIDECRTNVOFMT:{
					      bit->escribe(BIT_INFO,"**********************  INICIO OP_PIDECRTNVOFMT ************************");
					      ConsultaIES();
					      bit->escribe(BIT_INFO,"***********************  FIN  OP_PIDECRTNVOFMT ****************************");
					      break;

				     }
		case OP_REGCRTNVOFMT:{
					     bit->escribe(BIT_INFO,"--> OP_REGCRTNVOFMT");           
					     ConsultaAIES();
					     break; 
				     }
      case OP_LOGOUTAUT:{
                     bit->escribe(BIT_INFO,"--> OP_LOGOUT");
                     DesconectaIES(); 
                     error = ConectaIES();
                     if(error)
                     {
                         bit->escribe(BIT_ERROR,"     No se pudo re-conectar a la IES");
                         return error;
                     }

 
                 }
      case OP_CRTNOEXISTE:
                      {
                      bit->escribe(BIT_INFO,"--> OP_CRTNOEXISTE");
                      break;
                    }
      case OP_AUTNOCONN:
                        {
                            bit->escribePV(BIT_ERROR,"La Autoridad Registradora no esta conectada."); 
                            break;
                        }

                         
      //case 129:
      case SIN_OPER:
                 {  
                    if(!bError)
                    {
                       //bit->escribePV(BIT_ERROR,"     Error inesperado %d", mensIES.tipOperacion());  
                       bit->escribePV(BIT_ERROR,"     No se recibio nada de la IES  %d.", mensIES.tipOperacion());  
                       bError = true;
                    }
                    break;
                 } 
                
      default: 
                    //>- bit->escribePV(BIT_INFO, // ERGL mi� ene 30 17:57:32 CST 2008
                    bit->escribePV(BIT_ERROR, 
                    "en ServidorIES(), despues de Recibe(IES), mensIES.tipOperacion()=%d", 
                    mensIES.tipOperacion() );
	}


	return -1;
}
/*##################################################################################

PROPOSITO:
PREMISA  : ******
EFECTO   : ******
ENTRADAS : ******
SALIDAS  : ******

#################################################################################*/

int   Mediador::
ServidorAC(int rEsperada)
{
	error = SIN_OPER;

	error = Recibe(AC);
	if(error)
	{
		ErrorAR_AC(AR,"Error al recibir de la AC.", error, "I");   
		return error;
	}


   //>>> ERGL (070330) Se agregaron todas las validaciones de __tipo_de_operacion.
   if(rEsperada == mensAC.tipOperacion())
   {
	   switch( mensAC.tipOperacion() )
	   {
		      case CERTGENAR:
                           {
				                  bit->escribe(BIT_INFO,"--> CERTGENAR");
                              return RecibeCertificado();
		                     }

            case CERTREGAC:
                           {
                              RegistraOper(0,cNumTram, CERTREGAC);
				                  bit->escribe(BIT_INFO,"--> CERTREGAC");
				                  sprintf(cert.Edo.cont,"A");
				                  return 0;
			                  }  
                           // V.1.1.1.1 (080422) SERR: Se cambio el mensaje que se recibe de la AC CERTREVARAC
                           // Tue Apr 22 10:52:39 CDT 2008  
		                     //case CERTREVAR:
		      case CERTREVARAC:
			                   {
			                     bit->escribe(BIT_INFO,"--> CERTREVARAC");
				                  return RecibeCERTREVAR();
			                   }
		      case CERTRENAC:
                           {
         			            RegistraOper(0,cNumTram, CERTRENAC);
				                  if(!RecibeCERTRENAC())
				                  {
				                     error = Revocacion_AR(cNumTram, cNumSerie_ren,NULL );
				                     if(!error)
				                     {
				                        MarcaARA("R", cNumSerie_ren);
				 	                     mensIES.setMensajeIES(OP_REVBRDCST, cNumSerie_ren, strlen(cNumSerie_ren));
					                     Envia(IES);
					                     bit->escribePV(BIT_INFO, "     Envio de Revocacion a la IES %s", cNumSerie_ren);
					                     return 0;
				                     }
				                  }
				                  return -1;
			                  }
		          /*>- ERGL (070402) ya no son necesarias estas llamadas dentro del switch.
                case MSGERROR: {

					RecibeMSGERROR();
					return MSGERROR;
					break;

				  }   
		      default:{
				   ErrorAR_AC(AR,"Se recibio un mensaje inesperado" , mensAC.tipOperacion() , "N");             
			   	break;
			    }
                  -<*/
	   };
   }
   else if ( mensAC.tipOperacion() == MSGERROR )
   {
      RecibeMSGERROR();
      return MSGERROR;
   }
   ErrorAR_AC(AR,"Se recibio un mensaje inesperado" , mensAC.tipOperacion() , "N");
   //<<<
	return -1;             

}

/*##################################################################################

PROPOSITO:
PREMISA  : ******
EFECTO   : ******
ENTRADAS : ******
SALIDAS  : ******

#################################################################################*/

int Mediador::
EnviaReqAC()
{
	error = SIN_OPER;
	int iOper = mensAR.tipOperacion();

	error = mensAC.setMensaje(iOper , cNumTram,iNumTram,cSec, iSec,rfc_contr,irfc_contr,cert.TipCer.cont,cert.TipCer.icont,
			cert.req.cont,cert.req.icont );
	if(error != 0)
	{
		ErrorAR_AC(AR,"Error en setMensaje." , error , "I");
		return  ERR_GENCERT;
	}

	RegistraOper(0 ,cNumTram, iOper);
	error = Envia(AC);
	if(error)
	{
		ErrorAR_AC(AR,"Error al enviar a la AC" , error , "I");
		return error;
	}

	bit->escribePV(BIT_INFO,"     EnviaReqAC      : %s",cNumTram);

	return error;

}


/*##################################################################################

PROPOSITO: Obtiene los datos del certificado.
PREMISA  : ******
EFECTO   : ******
ENTRADAS : ******
SALIDAS  : ******

#################################################################################*/

int Mediador::
DatosCert()
{

   char cVigIni_[25];
   char cVigFin_[25];
   
   memset(cVigIni_ ,0 , sizeof(cVigIni_));
   memset(cVigFin_ ,0 , sizeof(cVigFin_));
   
   memset(cert.VigIni.cont,0,sizeof(cert.VigIni.cont));
   memset(cert.VigFin.cont,0,sizeof(cert.VigFin.cont));
   
   error = ReqCert.ProcCERT((uchar*)cert.cert.cont ,cert.cert.icont ,NULL,NULL, (uchar*)cVigIni_,
           (uchar*)cVigFin_,(uchar*)cert.NumSer.cont,NULL,NULL,0);
   //>> 02.00.01 (070220) GHM: Modificaciones para utilizar la librer�a Sgi_Fecha
   //fec.FechaBD(cVigIni_ , cert.VigIni.cont ); //--
   //fec.FechaBD(cVigFin_ , cert.VigFin.cont);  //--
   fec.ConvCadFtoCertTOBD((string)cVigIni_, cert.VigIni.cont);
   fec.ConvCadFtoCertTOBD((string)cVigFin_, cert.VigFin.cont);
   //<< 02.00.01
   //bit->escribePV(BIT_INFO,"Numero de Serie del Certificado Generado [%s]" ,cert.NumSer.cont);
   bit->escribePV(BIT_INFO,"     Num Serie Generado [%s]" , cert.NumSer.cont);
   return error;


}

/*##################################################################################

 PROPOSITO: Obtiene el RFC del certificado
 PREMISA  : ******
 EFECTO   : ******
 ENTRADAS : ******
 SALIDAS  : ******

 #################################################################################*/

int Mediador::
DatosCert( char *num_serie ,char *rfc)
{
   string sRuta;
   char   bCert[2048];
   string sRutaFin;

   SNames     sujeto[2];
   SNames     sujeto1[2];
   ManArchivo arch;

   int   datos[1] = {503};
   int   lbCert =sizeof(bCert);


   conf->getValorVar("[MED]","REPOSITORIO"  ,&sRuta);

   pkiRutaCert( sRuta.c_str() , cert.NumSer.cont  , sRutaFin ,false);  


   error = arch.ProcesaArchivo((char*)sRutaFin.c_str() ,(uchar*)bCert,&lbCert);
   if(!error)
      return !error;

   error = ReqCert.ProcCERT((uchar*)bCert,lbCert,sujeto, sujeto1,NULL,NULL,NULL,NULL,datos,1);
   if(error)
      return error;

   if(sujeto[0].dato == NULL)
   {
       bit->escribe(BIT_ERROR,"     No Contiene RFC el certificado");
       return ERROR;
   }
   strncpy(rfc,sujeto[0].dato,strlen(sujeto[0].dato));
   return  EXITO;
}




/*##################################################################################

 PROPOSITO: Obtener el cerificado del mensaje y sacar los datos necesarios.
 PREMISA  : ******
 EFECTO   : ******
 ENTRADAS : ******
 SALIDAS  : ******

 #################################################################################*/

int Mediador::
RecibeCertificado()
{
   error = SIN_OPER;
   memset(firma,        0,ifirma      = sizeof(firma)     );

   error = mensAC.getMensaje(CERTGENAR,firma,&ifirma,cert.RFC.cont ,&cert.RFC.icont,
                                                     cert.cert.cont,&cert.cert.icont);
   if(error)
   {
      ErrorAR_AC(AR,"Error al obtener los datos del mensaje." , error , "I");
      ErrorAR_AC(AC,"Error al obtener los datos del mensaje." , error , "I");
      return -1;
   }

   error = DatosCert();
   if(error)
   {
      ErrorAR_AC(AR,"Error al obtener los datos del Certificado." , error , "I");
      ErrorAR_AC(AC,"Error al obtener los datos del Certificado." , error , "I");
      return -1;//error;
   }

   RegistraOper(0 ,cNumTram, CERTGENAR);
   bit->escribePV(BIT_INFO,"     RecibeCertificado", cNumTram);
   return error;

}


/*##################################################################################

 PROPOSITO:
 PREMISA  : ******
 EFECTO   : ******
 ENTRADAS : ******
 SALIDAS  : ******

 #################################################################################*/


int Mediador::
DatosReq()
{
   error = SIN_OPER;

   SGIRSA     rsa_;
   RSA*        llavepb  = NULL;
   SGIPUBLICA  modPublico;

   SNames      pwd[1];
   SNames      correo[3];
   char        *aux = NULL;
   char        aux1[40];
   char        dig_md5[25];
   char        dig_sha1[30];
   int         idig_md5 = sizeof(dig_md5);
   int         idig_sha1= sizeof(dig_sha1);

   bool        es = false;
   int         icorreo[3] = {48,503,105};
   int         ipwdr[1]   = {54};

   int         imod = -1;

   int         error = -1 ;

   llavepb = RSA_new();


   memset(dig_md5 ,0,sizeof(dig_md5));
   memset(dig_sha1,0,sizeof(dig_sha1));
   memset(aux1,    0,sizeof(aux1));

   cert.LlaveMD5.icont  = idig_md5;
   cert.LlaveSHA1.icont = idig_sha1;


   error = ReqCert.ProcREQ( (uchar*)cert.req.cont ,cert.req.icont ,correo, pwd, &llavepb,icorreo, 3,ipwdr,1);
   if(error != 0)
      return sgiErrorBase(error);

   strcpy(aux1 ,correo[1].dato);

   if(strlen(aux1) > 13)
      es = true;

   aux = strtok(aux1,"/");
   strcpy(cert.RFC.cont,aux);
   if(es)
   {   
      cert.RFC.cont[strlen(cert.RFC.cont)-1] = 0;
      conRepr  = true;
   }

   cert.RFC.icont =  strlen(cert.RFC.cont);

   cert.RFCRL.cont[0] = '\0';

   strcpy(cert.RFCRL.cont, aux1 + cert.RFC.icont + 3 );

   if( correo[2].dato == NULL)
   {
      if( !strcmp(cert.TipCer.cont,"1"))
         return -1;  
   }
   else
   {
      strcpy(aux1 ,correo[2].dato);
      if(strlen(aux1) > 18)
         es = true;

      aux = strtok(aux1,"/");
      strcpy(cert.Curp.cont, aux);
      if(es)
        cert.Curp.cont[strlen(cert.Curp.cont)-1] = 0;
      strcpy(cert.RFCCurp.cont, aux1 + strlen(cert.Curp.cont)+ 3 );
   }

   if(pwd[0].dato == NULL)
        return ERR_DATONULO;

   if(atoi(cert.TipCer.cont) != 2)
   {
      if(correo[0].dato == NULL) // Correo
          return ERR_DATONULO;
      cert.Correo.icont = strlen(correo[0].dato);
      strcpy(cert.Correo.cont, correo[0].dato);

      if(correo[1].dato == NULL) // CURP
         return ERR_DATONULO;
   }
   cert.PwdRev.icont    = strlen(pwd[0].dato);
   strcpy(cert.PwdRev.cont   , pwd[0].dato);
   
   error = ObtieneLlave(llavepb ,cert.LlavePub.cont , &cert.LlavePub.icont);
   if(error)
      return error;

   error =  rsa_.setPublica(llavepb,&modPublico);
   RSA_free(llavepb);
   llavepb = NULL;
   if(error != 0)
      return error;

   switch(modPublico.bits)
   {
      case 1024:{imod = 128;break;}
      case 2048:{imod = 256;break;}
      case 4096:{imod = 512;break;}
   }


   error = rsa_.DigestAlg(modPublico.moduloP,imod,MD5_ALG,(uchar*)dig_md5,&idig_md5);
   if(error != 0)
   {
      ErrorAR_AC(AR,"DigestAlg MD5 %d",error ,"I");
      return error;
   }
   error = rsa_.DigestAlg(modPublico.moduloP,imod,SHA1_ALG,(uchar*)dig_sha1,&idig_sha1);
   if(error != 0)
   {
      ErrorAR_AC(AR,"DigestAlg SHA1 %d",error ,"I");
      return error;
   }

   error =B64_Codifica((uint8*)dig_md5, (uint16) idig_md5,cert.LlaveMD5.cont,
                              (uint16*)&cert.LlaveMD5.icont);
   if(error != 1 )
   {
      ErrorAR_AC(AR,"B64_Codifica MD5" ,error ,"I");
      return 10;
   }
   error =B64_Codifica((uint8*)dig_sha1, (uint16) idig_sha1, cert.LlaveSHA1.cont ,
                               (uint16*)&cert.LlaveSHA1.icont);
   if(error != 1 )
   {
      ErrorAR_AC(AR,"B64_Codifica sha1",error , "I");
      return 10;
   }
 

  
   return 0;
}

/*##################################################################################

 PROPOSITO: 
 PREMISA  : ******
 EFECTO   : ******
 ENTRADAS : ipIES   : IP de la IES
            servIES : Puerto del ServidorIES
            ipAC    : ipAC
            servAC  : Puerto del ServidorAC
 SALIDAS  : Conexion con la IES y la AC.

 #################################################################################*/
 
int Mediador :: 
RecibeReq()
{

   char dato1[20];
   char dato2[20];

   int iOper = mensAR.tipOperacion();

   if( iOper ==  SOLCERTAR)
      bit->escribe(BIT_INFO,"--> SOLCERTAR");
   else
   {   
      bit->escribe(BIT_INFO,"--> SOLRENAR");
      esRenovacion = true;
   } 

   error = mensAR.getMensaje( iOper, firma ,&ifirma , cNumTram , &iNumTram , cSec , &iSec ,rfc_contr  ,&irfc_contr,
                                            cert.TipCer.cont , &cert.TipCer.icont ,cert.req.cont ,&cert.req.icont );
   
   if(error)
   {
      ErrorAR_AC(AR,"Error al obtener los datos." , error , "I");
      return error;
   }

   SeparaCad(rfc_contr ,dato1 , cert.Nombre.cont,dato2);

   error = DatosReq();
   if(error)
   {
      ErrorAR_AC(AR ,"Error al obtener los datos." , error , "I");
      return error;
   }
   RegistraOper(0 ,cNumTram, iOper );
   
   //bit->escribePV(BIT_INFO,"     numt : %s \n     rfc  : %s \n     tipCert : %s",cNumTram ,rfc_contr,cert.TipCer.cont);
   bit->escribePV(BIT_INFO,"     numt : %s\n%61s %s \n%64s %s",cNumTram ,"rfc  : ",rfc_contr,"tipCert : ",cert.TipCer.cont);
   return error;
}

/*##################################################################################

 PROPOSITO: Generaci�n de Certificados.
 PREMISA  : ******
 EFECTO   : ******
 ENTRADAS : ******

 #################################################################################*/

void Mediador ::
Generacion()
{

    esGeneracion = true;
    
    error = RecibeReq();
    if(!error)
    {
       error = EnviaReqAC();
       if(!error)
       {       
          error = ServidorAC(CERTGENAR);
          if(error == 0)
          {   
              
              switch(atoi(cert.TipCer.cont))
              {
                 case 1:
                 case 2: {
                           if ( RegistraIES())
                            { 
                               if ( !GuardaCert() )
                               {
                                  if( !RegistraAC(true))
                                  {   
                                     if( !RegistroBD() )
                                     {    
                                        if( !EnviarAR())
                                        {   
                                           MarcaARA("A");
                                           if(atoi(cert.TipCer.cont) == 1)
                                              error = MarcaDario(cert.RFC.cont,"Y");  // RORS 16 ene 2009 se cambio S por Y
                                        }
                                     }
                                  }
                               }
                           }
                           else
                           {
                              error = RegistraAC(false); 
 
                           }
                           break;  
                         }   
        
                 case 3:
                 case 4:
                 case 5:         
                 case 7:{         
                           if (!RegistraAC(true)) 
                           {   
                              if(!GuardaCert())
                              {
                                 if(!RegistroBD())
                                 {   
                                    if(!EnviarAR())
                                       MarcaARA("A");
                                 }
                               }
                           }
                           break;
                        }
    
              };
          }// 3
       } // 2
    } // 1 
    if(!enviadoAR)
       Respuesta();
   
}

/*##################################################################################

 PROPOSITO: Consulta de Certificados.
 PREMISA  : ******
 EFECTO   : ******
 ENTRADAS : ******

 #################################################################################*/
//>+ ERGL (16072006)

int Mediador::
pideCertNvoFmto()
{
   lbufferIES = mensAR.getTamDatos();
   //mensIES.getDatos(mensAR.buffer,lbufferIES);
   //memcpy(bufferIES, mensAR.buffer, lbufferIES);
   memcpy(mensIES.buffer, mensAR.buffer, lbufferIES);
   int eEnvia = Envia( IES );
   if ( eEnvia )
   {
      bit->escribe(BIT_ERROR, eEnvia, "Error al intentar enviar hacia la IES.");
      return 0;
   }
   bit->escribePV(BIT_INFO,"Numero de Serie Consultado %s",mensIES.buffer+4);
   //eEnvia = Recibe( IES );
   eEnvia = ServidorIES();
   //if ( eEnvia )
   //{
   //   bit->escribe(BIT_ERROR, eEnvia, "Error al intentar recibir desde la IES.");
   //   return 0;
   //}
   memcpy(mensAR.buffer, mensIES.buffer, mensIES.getTamDatos());
   if ( mensAR.tipOperacion() == OP_TIPODESC )
      bit->escribePV(BIT_INFO, "La IES respondio (%d) (%s)", mensAR.tipOperacion(), "OP_TIPODESC"); 
   //Envia(AR);
   return EXITO;
}

/*##################################################################################

 PROPOSITO: Recibe del socket
 PREMISA  : ******
 EFECTO   : ******
 ENTRADAS : DES   Si va recibir de la IES o de la AC.
 SALIDAS  : 

 #################################################################################*/
int Mediador::
Recibe(int DES)
{
   error = SIN_OPER;

   //lbufferIES = sizeof(bufferIES);
   //lbufferAC  = sizeof(bufferAC);
   //memset(bufferAC, 0, sizeof(bufferAC));
   //memset(bufferIES, 0, sizeof(bufferIES));

   if(DES == AC)
   {
      //error = sktAC->Recibe((uint8 *)bufferAC,lbufferAC); //>- ERGL mi� sep 12 13:47:01 CDT 2007
      error = mensAC.Recibe(sktAC);
      /* ERGL mi� sep 12 13:49:48 CDT 2007
      if( error == 0 )
      {
         error = mensAC.getDatos(bufferAC,lbufferAC);
         if(!error)
            return EXITO;
      }
      else //if(error == 5)
      */
      if ( error != 0 )
      {
         // << 5 julio 2006
         
         conectAC = false;
         char cNumSerie_[21];
         if(enRegistraAC)
         {
            if(esRenovacion)
            {
               if(bd->consultaReg("SELECT FIRST 1 NO_SERIE FROM CERTIFICADO WHERE RFC= '%s' AND EDO_CER ='A' and "
                                     "TIPCER_CVE IN ('1', '2')  ORDER BY NO_SERIE DESC;",cert.RFC.cont ))
               {   
                  if(bd->getValores("b" ,cNumSerie_))
                  {
                    bd->ejecutaOper("UPDATE CERTIFICADO SET EDO_CER='R' WHERE NO_SERIE = '%s'", cNumSerie_);   
                    bit->escribePV(BIT_INFO,"     Numero de serie  revocado  %s", cNumSerie_);
                  }
               }else bit->escribe(BIT_ERROR,"     No se pudo revocar el certificado anterior.");             
                  
                    
            }
            
            sprintf(cert.Edo.cont,"A");   
            RegistroBD();
            mensAC.mensaje->encabezado.operacion = CERTREGAC;
            if( !EnviarAR())
            {
               MarcaARA("A");
               if(atoi(cert.TipCer.cont) == 1)
                 error = MarcaDario(cert.RFC.cont,"Y");
            }


         }
         
         mensAR.setMensaje(MSGERROR, "I" , 1, "6020",4,  "Problemas de comunicacion" , 
                                                  strlen("Problemas de comunicacion"));
         bit->escribe(BIT_ERROR,"    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
         bit->escribe(BIT_ERROR,"    ~~             Desconexion con la AC (Recibe)                  ~");
         bit->escribe(BIT_ERROR,"    ~~  EL AR_MEDIADOR SE CERRARA YA QUE NO HAY CONEXION CON LA AC ~");
         bit->escribe(BIT_ERROR,"    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
         bit_ef->escribe(BIT_ERROR , "AR_MEDIADOR DESCONEXION CON LA AC");
         Termina();
         exit(0);
         return DESCONEXIONDELAAC;
         // >> 
      }
      //else return sgiErrorBase(error);
   }
   else if( DES == IES)
   {
      /*
      //error = sktIES->Recibe((uint8 *)bufferIES,lbufferIES); //>- ERGL mi� sep 12 14:18:27 CDT 2007
      error = mensIES.Recibe(sktIES);    //se cambia para obtener todos los datos.
      // ERGL mi� sep 12 14:20:36 CDT 2007
      //if(!error)
      //{
       //  error = mensIES.getDatos(bufferIES,lbufferIES);
        // if(!error)
         //   return EXITO;
     //}
      //else if(error == 5)
      //
      */

     error = mensIES.Recibe(sktIES);
     if( error == 5 )
     {
         conectIES = false;
         bit->escribe(BIT_INFO,"Desconexion con la IES");
     }
     //else return sgiErrorBase(error);
     else return error;
   }
   else return ERR_RECV;

   return error;
}



/*##################################################################################

 PROPOSITO: Envia
 PREMISA  : ******
 EFECTO   : ******
 ENTRADAS : 
 SALIDAS  : 

 #################################################################################*/

int Mediador::
Envia(int DES )
{
   error = SIN_OPER;

   if( DES  == AC )
   {
      if(!mensAC.setDatos(bufferAC,&lbufferAC))
      error =  sktAC->Envia((uint8 *)bufferAC,lbufferAC);
      if(error!=0)
      {
         conectAC = false;
         mensAR.setMensaje(MSGERROR, "I" , 1, "6020",4,  "Problemas de comunicacion" ,
                                                  strlen("Problemas de comunicacion"));
         
         bit->escribe(BIT_ERROR,"    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
         bit->escribe(BIT_ERROR,"    ~~          Desconexion con la AC (Envia)                      ~");
         bit->escribe(BIT_ERROR,"    ~~  EL AR_MEDIADOR SE CERRARA YA QUE NO HAY CONEXION CON LA AC ~");
         bit->escribe(BIT_ERROR,"    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
         bit_ef->escribe(BIT_ERROR , "AR_MEDIADOR DESCONEXION CON LA AC");
         Termina();
         Envia(AR);
         exit(0);
         return DESCONEXIONDELAAC;
      }
      else return EXITO;
   }
   if( DES  == IES)
   {
      if(sktIES == NULL)
      {
          error = ConectaIES();
          if(error)
          {
             bit->escribe(BIT_ERROR,"     No se pudo conectar a la IES");
             return error;
          }
      }
      if(!mensIES.setDatos(bufferIES,&lbufferIES))

      error =  sktIES->Envia((uint8 *)bufferIES,lbufferIES);

      if(error!=0)
      {
         ErrorAR_AC(AR,"Desconexion con la IES." , error , "N");
         error = ConectaIES();
         if(error)
            return error;
         error =  sktIES->Envia((uint8 *)bufferIES,lbufferIES);
         if(error)
            return error;
      }
      else return EXITO;

   }
   if ( DES ==  AR)
   {

      char n_fifo[TAM_RUTAS];
      char b_fifo[TAM_DATOS];
      char b_es_fifo[TAM_DATOS];
      char b_es_fifo_[TAM_DATOS];

      int  des_fifo;
      int  lb_fifo    =  sizeof(b_fifo);
      int  lb_es_fifo =  sizeof(b_es_fifo);


      snprintf(n_fifo,sizeof(n_fifo),"/tmp/fifoLect.%ld", pid);


      struct timespec tsleep = { 0, 100000000L};
      nanosleep(&tsleep, NULL);
      for (int intento = 0; intento < 50; intento++)
      {
        des_fifo = open(n_fifo, O_WRONLY , 0);
        if (des_fifo >= 0)
             break;
        nanosleep(&tsleep, NULL);
        bit->escribePV(BIT_ERROR,"     n_fifo %s errno :%d intento: %d",n_fifo,errno,intento);
      }
      if (des_fifo < 0)
      {
        bit->escribePV(BIT_ERROR, "     Error abriendo  %s errno = %d",n_fifo,errno);
        return errno;
      }

      mensAR.setDatos(b_fifo, &lb_fifo);

      snprintf(b_es_fifo,lb_es_fifo , "%ld %ld ", (long) pid, (long) lb_fifo);


      lb_es_fifo = strlen(b_es_fifo);

      memcpy(b_es_fifo_,b_es_fifo, lb_es_fifo);
      
      memcpy(b_es_fifo_ + lb_es_fifo,b_fifo , lb_fifo);

      error = write(des_fifo,b_es_fifo_,lb_es_fifo + lb_fifo);
      if(error != (lb_es_fifo + lb_fifo))
         return ERR_ENV;
      close(des_fifo);
      pid = 0;
      return EXITO;

   }
      return ERR_ENV;
}

                                                    



/*##################################################################################

 PROPOSITO: Conecta a la AC y a la IES
 PREMISA  : ******
 EFECTO   : ******
 ENTRADAS : ipIES   : IP de la IES
            servIES : Puerto del ServidorIES
            ipAC    : ipAC
            servAC  : Puerto del ServidorAC 
 SALIDAS  : Conexion con la IES y la AC.

 #################################################################################*/


int Mediador::
Conecta()
{
   error = SIN_OPER;

   bit->escribe(BIT_INFO,"******************* CONEXION ************************");
   bit->escribe(BIT_INFO,"******************* AC - IES ************************");

   for(int i = 0; i< 3;i++ )
   {
      if(conectAC == true && conectIES == true)
         return EXITO;

      if(conectAC == false)
      {
         error = ConectaAC();//Primero
         if(error)
            continue;
      }

      if(conectIES == false)
      {
         error = ConectaIES();//Segundo
         if(error)
            continue;
      }
   }

   return error;

}


/*##################################################################################

 PROPOSITO: Conecta a la IES
 PREMISA  : ******
 EFECTO   : ******
 ENTRADAS : servidor : IP de la IES
            servicio : Puerto del ServidorIES
 SALIDAS  : Conexion con la IES.

 #################################################################################*/

int Mediador::
ConectaIES()
{
  error  = SIN_OPER;

  memset(firma,0,sizeof(firma));
  ifirma = sizeof(firma);

  string sIES[3];
  string sSkt[5];
  char cPwdSkt[30];
  int  iPwdSkt  = sizeof(cPwdSkt);

  conf->getValorVar("[IES]","IP"     ,&sIES[0]);
  conf->getValorVar("[IES]","PUERTO" ,&sIES[1]);
  conf->getValorVar("[IES]","NUMAR"  ,&sIES[2]);


  bit->escribePV(BIT_INFO,"Conectandose IES\n%53s %s\n%57s %d\n%63s %s\n",
                          "IP: ",sIES[0].c_str(),"PUERTO: ",atoi(sIES[1].c_str()),"NUMERO DE AR: ",sIES[2].c_str() );

  int iPuerto  = atoi(sIES[1].c_str());
  conf->getValorVar("[SKT]","CERTAC"   ,&sSkt[0]);
  conf->getValorVar("[SKT]","RUTA"     ,&sSkt[1]);
  conf->getValorVar("[SKT]","CERTCLI"  ,&sSkt[2]);
  conf->getValorVar("[SKT]","LLAVCLI"  ,&sSkt[3]);
  conf->getValorVar("[SKT]","PWDCLI"   ,&sSkt[4]);
 


 if (!DesPWD(sSkt[4] , cPwdSkt , &iPwdSkt))
 {
    bit->escribe(BIT_ERROR,"     No se pudo desencriptar el pwd");
      return -1;
 }

 strcpy(PASSLLAVCLISKT, cPwdSkt);

  paramIES.SetCliente( EPlano ,NULL,(char*)sIES[0].c_str(),iPuerto , (char *)sSkt[0].c_str()); 
 
   // RORS  25.03.09 Se agrega timeout en el socket 
  //struct timeval tTiempoEspera;
  //tTiempoEspera.tv_sec = 20;     // Leer del archivo de configuracion 
  //paramIES.SetCliente( EPlano ,&tTiempoEspera,(char*)sIES[0].c_str(),iPuerto , (char *)sSkt[0].c_str()); 
  sslIES.Inicia();


  error = sslIES.Cliente(paramIES,sktIES);
  if(error)
  {
     bit->escribePV(BIT_ERROR,"     Error en el SocketIES %d",error);
     conectIES = false;
     return CONECTA_IES;
  }
 
  error = sktIES->setKEEPALIVE(1800, 2,60);
  if(error)
     bit->escribePV(BIT_ERROR,"     Error KEEPALIVE IES %d",error);
  
  error = mensIES.ConectaCliente(sktIES, (char*)sIES[2].c_str());
  if(error)
  {
     bit->escribePV(BIT_ERROR,"     Error en mensIES  %d , %s", error,  mensIES.buffer);  
     return CONECTA_IES;
  }

  bit->escribe(BIT_INFO,"Conexion Abierta IES");
  conectIES = true;
  return EXITO;

}

/*##################################################################################

 PROPOSITO: Conecta a la AC
 PREMISA  : ******
 EFECTO   : ******
 ENTRADAS : servidor : IP de la AC
            servicio : Puerto del ServidorAC
 SALIDAS  : Conexion con la AC.

 #################################################################################*/

int Mediador::
ConectaAC()
{
   error = SIN_OPER;

   string sSkt[5];   
   string sAC[2];

   char cPwdSkt[TAM_PWD];
   int  iPwdSkt =  sizeof(cPwdSkt);  

   conf->getValorVar("[SKT]","CERTAC"   ,&sSkt[0]);
   conf->getValorVar("[SKT]","RUTA"     ,&sSkt[1]);
   conf->getValorVar("[SKT]","CERTCLI"  ,&sSkt[2]);
   conf->getValorVar("[SKT]","LLAVCLI"  ,&sSkt[3]);
   conf->getValorVar("[SKT]","PWDCLI"   ,&sSkt[4]);

   conf->getValorVar("[AC]","IP"     ,&sAC[0]);
   conf->getValorVar("[AC]","PUERTO" ,&sAC[1]);


   if (!DesPWD(sSkt[4] , cPwdSkt , &iPwdSkt))
   {
      bit->escribe(BIT_ERROR,"     No se pudo desencriptar el pwd");
      return -1;
   }  

   strcpy(PASSLLAVCLISKT, cPwdSkt);

   bit->escribePV(BIT_INFO,"Conectandose AC\n%53s %s\n%57s %d", "IP: ", sAC[0].c_str() ,"PUERTO: ",atoi(sAC[1].c_str()));

   paramAC.SetCliente( ESSL_entero ,NULL, (char *)sAC[0].c_str(), atoi(sAC[1].c_str()), (char *)sSkt[0].c_str() , 
                                          (char *)sSkt[1].c_str() ,(char *) sSkt[2].c_str(),
                                          (char *)sSkt[3].c_str() , verify_callback,password_cb);

   sslAC.Inicia();

   error = sslAC.Cliente(paramAC,sktAC);
   if(error)
   {
      bit->escribePV(BIT_ERROR,"No se creo el SocketAC %d",error);
      return CONECTA_AC;
   }

   error = sktAC->setKEEPALIVE(900, 2,60);
   if(error)
     bit->escribePV(BIT_ERROR,"Error KEEPALIVE AC %d",error);

   error = mensAC.ConectaCliente(sktAC,"1",NULL);
   if(error)
     return  CONECTA_AC;

   bit->escribe(BIT_INFO,"Conexion Abierta AC");
   conectAC = true;
   return EXITO;

}

/*##################################################################################

 PROPOSITO: Inicializar los objetos
 PREMISA  : ******
 EFECTO   : ******
 ENTRADAS : ******
 SALIDAS  : ******

#################################################################################*/

int Mediador::
IniciaAR()
{
  bit_ef->escribe(BIT_INFO , "IniciaAR");
  string sAR[4];
  char   cPwdAR[100]; 
  int    iPwdAR = sizeof(cPwdAR);

  error = conf->getValorVar("[AR]","CERTAR" ,&sAR[0]);
  error = conf->getValorVar("[AR]","LLAVPRIVAR" ,&sAR[1]);
  error = conf->getValorVar("[AR]","PWDAR"  ,&sAR[2]);
  error = conf->getValorVar("[AR]","CERTAR" ,&sAR[3]);

  if (!DesPWD(sAR[2] , cPwdAR , &iPwdAR))
     return -1;   

  error = mensAR.Inicia( AR ,(char*) sAR[0].c_str(), (char*) sAR[1].c_str() , cPwdAR, iPwdAR ,(char*) sAR[3].c_str() );
  if(error)
     return error;

  return 0;

}

/*##################################################################################

 PROPOSITO: Inicializar los objetos
 PREMISA  : ******
 EFECTO   : ******
 ENTRADAS : ******
 SALIDAS  : ******

#################################################################################*/

int Mediador::
IniciaAC()
{
  bit_ef->escribe(BIT_INFO , "IniciaAC");
  string sAC[6];
 
  char   cPwdAC[100];
  int    iPwdAC = sizeof(cPwdAC);

  error = conf->getValorVar("[AC]","IP"     ,&sAC[0]);
  error = conf->getValorVar("[AC]","PUERTO" ,&sAC[1]);
  error = conf->getValorVar("[AC]","CERTAR" ,&sAC[2]);
  error = conf->getValorVar("[AC]","LLAVPRIVAR" ,&sAC[3]);
  error = conf->getValorVar("[AC]","PWDAR"  ,&sAC[4]);
  error = conf->getValorVar("[AC]","CERTAC" ,&sAC[5]);

  if(!DesPWD(sAC[4] , cPwdAC , &iPwdAC))
     return -1;

  error = mensAC.Inicia( AC , (char*) sAC[2].c_str(), (char*) sAC[3].c_str() , cPwdAC , iPwdAC, (char*) sAC[5].c_str() );
  if(error)
     return error;

  return 0;

}

/*##################################################################################

 PROPOSITO: Inicializar los objetos
 PREMISA  : ******
 EFECTO   : ******
 ENTRADAS : ******
 SALIDAS  : ******

#################################################################################*/

int Mediador::
IniciaIES()
{
  bit_ef->escribe(BIT_INFO, "IniciaIES");
  string sIES[7];
  char cPwdIES[100];
  int  iPwdIES =  sizeof(cPwdIES);

  error = conf->getValorVar("[IES]","IP"     ,&sIES[0]);
  error = conf->getValorVar("[IES]","PUERTO" ,&sIES[1]);
  error = conf->getValorVar("[IES]","CERTAR" ,&sIES[2]);
  error = conf->getValorVar("[IES]","LLAVPRIVAR" ,&sIES[3]);
  error = conf->getValorVar("[IES]","PWDAR"  ,&sIES[4]);
  error = conf->getValorVar("[IES]","CERTIES",&sIES[5]);
  error = conf->getValorVar("[IES]","NUMAR"  ,&sIES[6]);

  if( !DesPWD(sIES[4] , cPwdIES , &iPwdIES))
     return -1;
 
  error = mensIES.Inicia( IES , (char*) sIES[2].c_str(), (char*) sIES[3].c_str(),
                                (char*) cPwdIES, iPwdIES, (char*) sIES[5].c_str());
  if(error)
     return error;


  return 0;
}

/*##################################################################################

 PROPOSITO: Desencripta  los passwords
 PREMISA  : ******
 EFECTO   : ******
 ENTRADAS : ******
 SALIDAS  : ******
 
#################################################################################*/

bool Mediador::
DesPWD(string sPwdEnc , char *cPwdDes ,int *iPwdDes)
{
  if( !desencripta((uint8*)sPwdEnc.c_str(),sPwdEnc.length(),(uint8*)cPwdDes,iPwdDes))
     return false;
     
  cPwdDes[*iPwdDes]=0;
  return true;
     
}

/*##################################################################################

 PROPOSITO: Inicializar los objetos
 PREMISA  : ******
 EFECTO   : ******
 ENTRADAS : ******
 SALIDAS  : ******
 
#################################################################################*/

int Mediador::
ConectaBD()
{
 
  string sBD[5];
  char cPwdBD[100];
  int  iPwdBD =  sizeof(cPwdBD);

  error = conf->getValorVar("[BD]","BD"        ,&sBD[0]);
  error = conf->getValorVar("[BD]","INSTANCIA" ,&sBD[1]);
  error = conf->getValorVar("[BD]","USUARIO"   ,&sBD[2]);
  error = conf->getValorVar("[BD]","ROL"       ,&sBD[3]);
  error = conf->getValorVar("[BD]","PWDBD"     ,&sBD[4]);

  if( !DesPWD(sBD[4] , cPwdBD , &iPwdBD))
     return -1;

  bit_bd = new CBitacora("/var/local/SAT/PKI/AR_Mediador_BD.log");
  if(!bit_bd)
    return -2;

  bd = new CBD(bit_bd);
  if(!bd)
     return -3;

  if( ! bd->setConfig( sBD[1].c_str() ,sBD[0].c_str() ,sBD[2].c_str(),cPwdBD, sBD[3].c_str() ))
     return -4;
   
  if (!bd->conecta())
     return -5;
 
 return 0;
   
}


/*##################################################################################

 PROPOSITO: Inicializar los objetos
 PREMISA  : ******
 EFECTO   : ******
 ENTRADAS : ******
 SALIDAS  : ******

#################################################################################*/

int Mediador::
Inicia()
{

#ifndef DBG
    bit = new CBitacora("/var/local/SAT/PKI/AR_Mediador.log");
#else
    bit = new CBitacora("/var/local/SAT/PKI/AR_Mediador_dbg.log");
#endif
    
    bit_ef  = new CBitacora("/var/local/SAT/PKI/ERROR.log ");
    
    bit->escribe(BIT_INFO,"IniciaAR");
    error = IniciaAR();
    if(error)
       return error;
    bit->escribe(BIT_INFO,"IniciaAC");
    error = IniciaAC();
    if(error)
       return error;
    bit->escribe(BIT_INFO,"IniciaIES");
    error = IniciaIES();
    if(error)
       return error;
    bit->escribe(BIT_INFO,"IniciaConectaBD");
    error = ConectaBD();
    if(error)
       return error;  

    bit->escribe(BIT_INFO,"******************************************************");
    bit->escribe(BIT_INFO,"******************************************************");
    bit->escribe(BIT_INFO,"******************************************************");
    bit->escribe(BIT_INFO,"****************  AR_MEDIADOR V. 1.6 *****************");
    bit->escribe(BIT_INFO,"******************************************************");
    bit->escribe(BIT_INFO,"******************************************************");
    bit->escribe(BIT_INFO,"******************************************************");


    error = Conecta();
    if(error)
       return error;
    return error;

}

/*##################################################################################

 PROPOSITO: Construir la clase.
 PREMISA  : ******
 EFECTO   : ******
 ENTRADAS : ******
 SALIDAS  : ******

 #################################################################################*/

 int Mediador::
 Servicio()
 {
   static char buffer[TAM_DATOS];
   static char buffPid[TAM_DATOS];
   char    fifoname[100];
   int     readfifo, writefifo;
   int     lbuffer = sizeof(buffer);
   long    cantBytes;
   int     lbuffPid;
   struct  timespec espera = { 0, 100000000L};

   lbuff = sizeof(buff);

   error = SIN_OPER;

   if ( (mkfifo(SERV_FIFO, FILE_MODE) < 0 ) && ( errno != EEXIST ) )
      bit->escribePV(BIT_ERROR,"Error Creando Fifo errno %d",errno);
   readfifo = open(SERV_FIFO ,O_RDONLY |O_NONBLOCK);
   bit->escribePV(BIT_ERROR,"SERV_FIFO errno = %d",errno);


   for (;;)
   {

      nanosleep(&espera, NULL);
      memset(buffer,0,sizeof(buffer));
      para_ies = true;
      mensIES.mensaje->encabezado.operacion = (uint8)SIN_OPER;

      error =  ServidorIES();
      if(error == NO_PEND )
         para_ies = false;

      pid = 0;
      iRechazado = 0;
      enviadoAR = false;
      leeLong(readfifo, pid);
      if ( pid == 0 )
         continue;

      leeLong(readfifo, cantBytes);
      leeBuffer(readfifo, buffer , cantBytes);
      mensAR.getDatos(buffer,lbuffer);

      bit->escribePV(BIT_INFO,"Operacion Recibida %d pid [%d] ", mensAR.tipOperacion(),pid);
      Limpia();

      switch ( mensAR.tipOperacion() )
      {
         case SOLREVAR:
         case SOLREVOFAR:
         case SOLREVFAR:
                        {
                          bit->escribe(BIT_INFO,"*************************  INICIO REVOCACION ****************************");
                          Revocacion();
                          bit->escribe(BIT_INFO,"*************************  FIN REVOCACION *******************************");
                          break;
                        }

         case SOLRENAR:
         case SOLCERTAR:
                        {
                          bit->escribe(BIT_INFO,"*************************  INICIO GENERACION  ****************************");
                          Generacion();
                          bit->escribe(BIT_INFO,"*************************  FIN GENERACION  *******************************");
                          break;
                        }
         case VERCERTNOSERAR:
                        {
                          bit->escribe(BIT_INFO,"*************************  INICIO CONSULTA  ******************************");
                          //>+ ERGL (16072006)
                          //error = Consulta_Certs(buffer, lbuffer);
                          //<-
                          bit->escribe(BIT_INFO,"*************************  FIN CONSULTA  *********************************");
                          break;
                        }
         //>>>+ ERGL (19072006)
         case OP_PIDECRTNVOFMT:
                        {
                          bit->escribe(BIT_INFO,"************************ INICIO CONSULTA IES *********************************");
                             pideCertNvoFmto();
                          bit->escribe(BIT_INFO,"************************ FIN CONSULTA IES ************************************");
                          break;
                        }
         default:
                {
                       bit->escribe(BIT_INFO,   "************************ Op No Esperada ***************************");
                       continue;
                }  
         }//switch
          
         if(mensAR.tipOperacion() == MSGERROR)
           continue;
         if(mensAR.tipOperacion() == CERTGENAR)
           continue;
         if(mensAR.tipOperacion() == CERTRENAR)
           continue;
         if(mensAR.tipOperacion() == CERTREVAR)
            continue;
         if(mensAR.tipOperacion() == RECUPERACION)
            continue;
         if(mensAR.tipOperacion() == DATCERTNUMSERIE)
            continue;
         if(mensAR.tipOperacion() == OP_TIPODESC)
            continue;

         snprintf(fifoname,sizeof(fifoname),"/tmp/fifoLect.%ld", pid);
           
         struct timespec tsleep = { 0, 100000000L};
         for (int intento = 0; intento < 50; intento++)
         {
            writefifo = open(fifoname, O_WRONLY, 0);
            if (writefifo >= 0)
               break;
            nanosleep(&tsleep, NULL);
            bit->escribePV(BIT_INFO,"fifoname %s",fifoname);
         }
         
         if (writefifo < 0)
         {
            bit->escribePV(BIT_ERROR, "Error Abriendo : %s errno : %d", fifoname,errno);
            continue;
         }
           
         mensAR.setDatos(buff, &lbuff);
         snprintf(buffPid,sizeof(buffPid) , "%ld %ld ", (long) pid, (long) lbuff);

         lbuffPid = strlen(buffPid);

         memcpy(buffer,buffPid, lbuffPid);
         memcpy(buffer + lbuffPid , buff, lbuff);
         write(writefifo,buffer,lbuffPid + lbuff);

         bit->escribePV(BIT_INFO,"Operacion Enviada %d", mensAR.tipOperacion());
         close(writefifo);

     }

    return 1;
 }

/*##################################################################################

 PROPOSITO: Construir la clase.
 PREMISA  : ******
 EFECTO   : ******
 ENTRADAS : ******
 SALIDAS  : ******

#################################################################################*/


int Mediador::
Termina()
{
   mensAR.Finaliza();
   mensIES.Finaliza();
   mensAC.Finaliza();
   bd->desconecta();
   Desconexion();

   delete conf;
   delete sktIES;
   delete sktAC;
   delete bd;
  
   conf   = NULL;
   bd     = NULL;
   sktAC  = NULL;
   sktIES = NULL;

  return EXITO;
}



/*##################################################################################

 PROPOSITO: Construir la clase.
 PREMISA  : ******
 EFECTO   : ******
 ENTRADAS : ******
 SALIDAS  : ******

#################################################################################*/
int Mediador::
Desconexion()
{
   error = SIN_OPER;

   if(conectAC)
   {
      error = mensAC.setMensaje(SOLDESCONEXION);
      if(error != 0)
        return error;
      error = Envia(AC);
      if(error != 0)
         return error;
      conectAC = false;
      bit->escribe(BIT_INFO,"Desconexion  AC");
   }
   if(conectIES)
      DesconectaIES();

   if((conectAC == false)&& (conectIES == false) )
      return EXITO;

   return -1;
}


/*##################################################################################

 PROPOSITO: Construir la clase.
 PREMISA  : Desconexion con la IES
 EFECTO   : ******
 ENTRADAS : ******
 SALIDAS  : ******

#################################################################################*/


int Mediador::
DesconectaIES()
{
   error = SIN_OPER;
   error = mensIES.setMensaje(OP_LOGOUT);
   if(error != 0)
      return error;
   error = Envia(IES);
   if(error != 0)
      return error;
   conectIES = false;
   bit->escribe(BIT_INFO,"Desconexion  IES");
   return EXITO;

}




/*##################################################################################

 PROPOSITO: Envia la respuesta a la AR.
 PREMISA  : ******
 EFECTO   : ******
 ENTRADAS : ******
 SALIDAS  : ******

#################################################################################*/

void Mediador::
Respuesta()
{
    esGeneracion = false;
    error = Envia(AR);
   
}

/*##################################################################################

 PROPOSITO: Escribe en la bitacora y arma el mensaje de error.
 PREMISA  : ******
 EFECTO   : ******
 ENTRADAS : ******
 SALIDAS  : ******

#################################################################################*/


void Mediador::
ErrorAR_AC( int iDest ,char *cTextErr , int iError , char *cTipEr)
{
   char cCodErr[8];

   string sError = cTextErr ;
   sprintf(cCodErr , "%d" , iError);
   bit->escribePV(BIT_ERROR,"%s %d",cTextErr, iError); //>>> MAML (070530): cambia BIT_INFO por BIT_ERROR
  
   if(iDest == AR)
   {
      mensAR.setMensaje(MSGERROR , cTipEr ,strlen(cTipEr) ,cCodErr, strlen(cCodErr) ,
                        sError.c_str() ,sError.length() );
   }
   else if(iDest == AC)
   {
      mensAC.setMensaje(MSGERROR , cTipEr ,strlen(cTipEr) ,cCodErr, strlen(cCodErr) ,
                                   sError.c_str() ,sError.length() );
      Envia(AC);
   }

   RegistraOper(0 ,cNumTram, MSGERROR );
}
 

/*##################################################################################

 PROPOSITO: Recupera los datos del certificado solicitado. 
 PREMISA  : ******
 EFECTO   : ******
 ENTRADAS : ******
 SALIDAS  : ******

#################################################################################*/
int  Mediador::
ConsultaIES()
{
   char cNumSerie_c[21];
   char cCert[2048];
   int  iCert = sizeof(cCert);
   int  iNumSerie_c =  sizeof(cNumSerie_c);

   string sConsulta;
   string sDatos[3];

   memset(cCert, 0 , sizeof(cCert));

   error = mensIES.getMensaje(OP_PIDECRTNVOFMT , cNumSerie_c , &iNumSerie_c ); 
   if(error)
   {
      bit->escribePV(BIT_ERROR, "     Error getMensaje  OP_PIDECRTNVOFMT %d ", error);  
      return error;
   }
   
   mensIES.setDestino(mensIES.getDestino());
   error = BuscaCert(cNumSerie_c , cCert , &iCert);
   if(!error)
   {
      sConsulta = "SELECT EDO_CER ,VIG_FIN , FEC_REG_IES FROM CERTIFICADO WHERE NO_SERIE = '%s';";
  
      if ( bd->consultaReg(sConsulta.c_str(), cNumSerie_c) )
      {
         if( bd->getValores( "sss", &sDatos[0],&sDatos[1], &sDatos[2] )) 
         {
            int iEstado;
            if (!strcmp(sDatos[0].c_str() ,"A"))
               iEstado = 0;
            if (!strcmp(sDatos[0].c_str() ,"R"))
               iEstado = 1;
            if (!strcmp(sDatos[0].c_str() ,"C"))
               iEstado = 3;

            time_t  f_cad,f_regies,f_act;
            char    cFechaCad[22];
            char    cFechaReg[22];
            memset(cFechaCad,0,22);
            memset(cFechaReg,0,22);
            //>>> 02.00.01 (070219) GHM: Para utilizar las funciones de Sgi_Fecha
            memset(cFechaCad,0,22);  //++ 02.00.01
            memset(cFechaReg,0,22);  //++ 02.00.01
            //--fec.FechaBD2((char*)sDatos[1].c_str() , cFechaCad );  // Caducidad //- 02.00.01
            //--fec.FechaBD2((char*)sDatos[2].c_str() , cFechaReg );  // Registro IES //- 02.00.01

            //fec.ConvCadFtoBDTOCER(sDatos[1], cFechaCad);  // RORS 15 julio 2008 no se utiliza
            //--cFechaCad[12] = 0;  //- 02.00.01
            //--cFechaReg[12] = 0;  //- 02.00.01

            //--fec.Fecha(cFechaCad,&f_cad); //- 02.00.01
            //--fec.Fecha(cFechaReg,&f_regies);  //- 02.00.01
            //>>> 02.00.02 (070313) GHM: Manejo del error de la librer�a de Sgi_Fecha
            int errorfec = 0;  //+ 02.00.02
            //- f_cad = fec.ConvFechFtoBDTOtime_t( (string)cFechaCad, (string)"GMT"); //- 02.00.02
            //- f_regies = fec.ConvFechFtoBDTOtime_t( (string)cFechaReg, (string)"CST"); //- 02.00.02
            //
            //f_cad = fec.ConvFechFtoBDTOtime_t( (string)cFechaCad, (string)"GMT", &errorfec); 
            //f_regies = fec.ConvFechFtoBDTOtime_t( (string)cFechaReg, (string)"CST", &errorfec); 
            // RORS    mar jul 15 14:01:04 CDT 2008 Se cambiaron las dos lineas de arriba por las siguientes.
            f_cad    = fec.ConvFechFtoBDTOtime_t( sDatos[1], (string)"GMT", &errorfec); 
            f_regies = fec.ConvFechFtoBDTOtime_t( sDatos[2], (string)"CST", &errorfec); 
            //<<< 02.00.02 (070313)
            //<<< 02.00.01
            f_act = time(NULL);
            error = mensIES.setMensajeIES(OP_REGCRTNVOFMT , iEstado, cCert , iCert , f_cad , 4 , f_regies , 4, f_act,4);
            if (!error)
            {
                Envia(IES);
                mensIES.setDestino(0);
                bit->escribePV(BIT_INFO, "     Certificado enviado a la IES %s" , cNumSerie_c);
                return 0;
            }
      
         } // getValores 
      } // ConsultaReg
   } // if BuscaCert

   bit->escribePV(BIT_ERROR, "     Certificado no encontrado %s ", cNumSerie_c);
   mensIES.setMensajeIES(OP_CRTNOEXISTE, cNumSerie_c , strlen(cNumSerie_c));
   Envia(IES);
   mensIES.setDestino(0);
   return 0;

}

/*##################################################################################

 PROPOSITO: Recupera los datos del certificado solicitado.
 PREMISA  : ******
 EFECTO   : ******
 ENTRADAS : ******
 SALIDAS  : ******

#################################################################################*/

int  Mediador::
ConsultaAIES()
{

   char      cVigFin[13];
   char      cEstado[2];
   char      cFecRegIES[14];
   char      cFecActual[14];
   char      cCert[TAM_DATOS];
   char      cFirma_c[TAM_FIRMA];
   char      cEstado_con[4];

   int       iFirma_c      = sizeof(cFirma_c);
   int       iCert         = sizeof(cCert);
   int       iFecRegIES    = sizeof(cFecRegIES);
   int       iEstado       = sizeof(cEstado);
   int       iVigFin       = sizeof(cVigFin);
   int       iFecActual    = sizeof(cFecActual);



   error =  mensIES.getMensajeIES(OP_REGCRTNVOFMT,cFirma_c,&iFirma_c, cEstado ,&iEstado, cVigFin ,&iVigFin,
                                              cFecRegIES ,&iFecRegIES ,cFecActual,&iFecActual, cCert,&iCert);

   if(error)
   {
      ErrorAR_AC(AR, "Error al obtener el mensaje OP_REGCRTNVOFMT" , error , "I");
      return error; 
     
   }

   switch(cEstado[0])
   {
      case '\000':sprintf(cEstado_con,"A|1"); break;
      case '\001':sprintf(cEstado_con,"R|1"); break;
      case '\002':sprintf(cEstado_con,"L|1"); break;
      case '\003':sprintf(cEstado_con,"C|1"); break;
   }


   error  = mensAR.setMensaje(DATCERTNUMSERIE , cEstado_con ,strlen(cEstado_con) ,cCert ,iCert);
   if(error)
   {
      ErrorAR_AC(AR,"Error al obtener el mensaje  DATCERTNUMSERIE" , error , "I");
      return error;

   }

   return error;


}


/*##################################################################################

 PROPOSITO: Recupera los datos del certificado solicitado.
 PREMISA  : ******
 EFECTO   : ******
 ENTRADAS : ******
 SALIDAS  : ******

#################################################################################*/



int  Mediador ::
BuscaCert(char* cNumSerie , char *cCert , int *iCert)
{

   string sRuta;
   string sResp;
   char cBuffCert[2048];
 

   int idesc;
   int iBuffCert  = sizeof(cBuffCert);
   struct stat stDatArch;

   string  sDesc;


   if(!strcmp (cNumSerie, "") || cNumSerie == NULL)
   {
      sDesc =  "     No se le paso el numero de Serie a la funcion";
      bit->escribe(BIT_ERROR , sDesc.c_str());
      return -1;
   }


   error = conf->getValorVar("[MED]","REPOSITORIO",&sResp);
   if(error)
   {
       sDesc  =  "     Error en getValorVar %d" ;
       bit->escribePV(BIT_ERROR , sDesc.c_str() , error);
       return error;
   }

   pkiRutaCert(sResp.c_str() , cNumSerie , sRuta,false);

   idesc = open(sRuta.c_str() , O_RDONLY);
   if(idesc < 0)
   {
      sDesc =  "     %s : %s" ; 
      bit->escribePV(BIT_ERROR , sDesc.c_str() , strerror(errno) ,sRuta.c_str() );
      return errno;
   }


   error = fstat(idesc, &stDatArch);
   if(error < 0)
   {
      sDesc  = "     %s : %s" ; 
      bit->escribePV(BIT_INFO , sDesc.c_str() , strerror(errno) ,sRuta.c_str());
      return errno;
   }


   error =  read(idesc , cBuffCert , stDatArch.st_size);
   if(error < 0)
   {
      sDesc  = "     %s : %s";
      bit->escribePV(BIT_INFO , sDesc.c_str() ,  strerror(errno) ,sRuta.c_str());
      return errno;
   }
   close(idesc);


   error = ReqCert.der2pem((uchar*)cBuffCert , iBuffCert ,(uchar*)cCert ,iCert);
   if(error != 0)
   {
     sDesc = "     Error al trasformar el certificado de DER a PEM (%d)" ; 
     bit->escribePV(BIT_ERROR,sDesc.c_str() ,error);
     return ERR_GENCERT;
   }

   //*iCert = stDatArch.st_size;
   return 0;

}

/*##################################################################################

 PROPOSITO: Recibe el mensaje CERTRENAR.
 PREMISA  : ******
 EFECTO   : ******
 ENTRADAS : ******
 SALIDAS  : ******

#################################################################################*/

int Mediador::
RecibeCERTRENAC()
{
   error = SIN_OPER;
   iNumSerie_ren = sizeof(cNumSerie_ren);
   int iFechaRev_ren = sizeof(cFecACRev_ren);

   memset( cFecACRev_ren, 0, iFechaRev_ren);
   error = mensAC.getMensaje(CERTRENAC ,firma ,&ifirma , cFecACRev_ren ,&iFechaRev_ren , cNumSerie_ren ,&iNumSerie_ren );
   if(error)
   {
       ErrorAR_AC(AR ,"Error al obtener el mensaje CERTRENAC", error ,"I");
       return error;
   }
   
   strcpy(cert.Edo.cont ,"A");
   return error;

}

/*##################################################################################

 PROPOSITO:.       La clase CSeparaToken deberia estar en alguna libreria de la PKI y asi eliminar esta Funcion  
 PREMISA  : ****** la longitud de cCadAux debe ser la misma q cCad
 EFECTO   : ******
 ENTRADAS : ******
 SALIDAS  : ******

#################################################################################*/

int Mediador ::
SeparaCad(char *cCad ,...)
{
   va_list lista;
   va_start(lista, cCad);

   //char cCadAux[256]; MAML 100408: Esta Cadena contiene el nombre o razon social del Contribuy y en la DB esta limitado a 255 
   char cCadAux[768]; // Tama�o real de la cadena q usa la funcion es: 13+602+8+1: TAM_RFC + TAM_NOMBRE + TAM_CLAVE_AGC + NULO 
   char *cRes;
   char *dato = NULL;

   int   iTamR = 0 ;

   strcpy (cCadAux ,cCad );

   for(int i = 0 ; i < 3 ;i++ )
   {
      dato = va_arg(lista,char*);
      if(dato == NULL)
         return 10;
      cRes = strtok(cCadAux + iTamR + i,"|");
      if(cRes == NULL)
        strcpy(dato,"");
      else
        strcpy(dato,cRes);
      iTamR = iTamR + strlen(cRes);

   }
   va_end(lista);
   return 0;
}

/*##################################################################################

 PROPOSITO: Deja un archivo que contiene los datos del certificado alertado.
 PREMISA  : ******
 EFECTO   : ******
 ENTRADAS : ******
 SALIDAS  : ******

#################################################################################*/


int Mediador::
Alertados(char * num_serie)
{
   char  nombreA[TAM_RUTAS];
   char  Datos[1024];
   char  cFecha[20];
   //char  cFechaCal[20]; //-- 02.00.01
   
   int archivo;
   string sAlertados;

   memset(Datos   ,0,sizeof(Datos));
   memset(nombreA ,0,sizeof(nombreA));
   memset(cFecha,  0,sizeof(cFecha)); 
   
   //>>> 02.00.01 (070219) GHM: Modificaciones para utilizar la librer�a Sgi_Fecha
   //--fec.FechaCal(cFechaCal , time(NULL) ); //--
   //--fec.FechaBD(cFechaCal, cFecha);        //--

   struct tm *sfechAct ;
   time_t temp_t = 0 ;   //Se inicializa en cero para obtener la fecha actual. >>> MAML 080422 

   int errorfec = 0; //+ 02.00.02 (070313) GHM: se anexa para el manejo de errores del Sgi_Fecha
   //- sfechAct = fec.FechaCal(0); //- 02.00.02 (070313)
   sfechAct = fec.FechaCal(&temp_t, &errorfec);

   // >>> MAML 080415: se incluye el manejo de errores
   if ( errorfec )  
      bit->escribePV(BIT_ERROR,"     Error(%d) en FechaCal en Alertados(), error: %s", errorfec, fec.getTxtError(errorfec).c_str() );
   else if ( (errorfec = fec.strTimeTOfechBD(sfechAct, cFecha)) != 0 )
      bit->escribePV(BIT_ERROR,"     Error(%d) en strTimeTOfechBD en Alertados(), error: %s", errorfec, fec.getTxtError(errorfec).c_str() );
   if ( errorfec ) 
       return ERROR;
   // <<<  

   //<<< 02.00.01

   conf->getValorVar("[MED]","ALERTADOS" ,&sAlertados);

   sprintf(nombreA,"%sCerts_Revocados_Alerta.log",sAlertados.c_str()); // MAML 080415: cambie '.ale' por '.log'

   archivo = open(nombreA,O_WRONLY|O_CREAT|O_APPEND,S_IRWXU);
   if(archivo < 0)
   {
      bit->escribePV(BIT_ERROR,"     Error(%d) en open en Alertados(), no pudo crear el archivo %s", archivo, nombreA);
      return ERROR;
   }
   sprintf(Datos,"%s Llave Comprometida  %s\n", cFecha, num_serie);
   write(archivo,Datos,strlen(Datos));

   close(archivo);
   return EXITO;

}

/*##################################################################################

 PROPOSITO: Deja un archivo que contiene los datos del certificado alertado.
 PREMISA  : ******
 EFECTO   : ******
 ENTRADAS : ******
 SALIDAS  : ******

#################################################################################*/


int Mediador :: 
ObtieneLlave( RSA * rLlave ,char *cLlavePub , int *iLlavePub )
{

   BIO   *bio = NULL;
   uchar uLlave[1024];
   int   iLlave = sizeof(uLlave);

   memset(uLlave, 0, sizeof(uLlave));

   bio = BIO_new(BIO_s_mem());
   if(bio == NULL)
      return -1;

   i2d_RSAPublicKey_bio(bio,rLlave);

   iLlave = BIO_read(bio, uLlave, bio->num_write);   
   if(iLlave != (int)bio->num_write)
      return -1;

   error =B64_Codifica((uint8*)uLlave, (uint16) iLlave , cLlavePub,(uint16*)&iLlavePub);
   if(!error)
      return error;

   BIO_free_all(bio);

   return 0;

}

//>>> ERGL (070125)
bool Mediador::
ObtieneDatosRevocacion(char *no_serie, char *rfc, char *tipo)
{
   if(!bd->consultaReg("SELECT rfc, tipcer_cve from certificado where no_serie = '%s'", no_serie))
   {
      bit_ef->escribePV(BIT_ERROR, "No se pudo recuperar la informacion para el certificado con n�mero de serie %s", no_serie);
      return false;   
   }
  //- if(bd->getValores("bb", rfc, tipo)) //- GHM(070528): Obtiene los datos pero env�a error
   if(!bd->getValores("bb", rfc, tipo))
   {
      bit_ef->escribePV(BIT_ERROR, "No se pudieron recuperar los datos de la consulta para el n�mero de serie %s", no_serie);
      return false;
   }
   return true;
}
//<<<

