static const char* _GENERACRL_CPP_VERSION_ ATR_USED = "CrearCRLsSAT @(#)"\
"DSIC10384AR_ 2010-09-08 GeneraCRL.cpp 1.0.0/0";

#include<GeneraCRL.h>

GeneraCRL::
GeneraCRL()
{
   ERR_load_crypto_strings();
   OpenSSL_add_all_algorithms();
   iFechaProxAct = 25;
   revocado = NULL;
   lista  = NULL;
   iError = -1;
}

GeneraCRL::
~GeneraCRL()
{
   if (revocado != NULL)
      X509_REVOKED_free(revocado);
   if (lista != NULL)
      X509_CRL_free(lista);
}


int GeneraCRL::
getError()
{
   return iError;
}

bool GeneraCRL::
agregaRevocado(char* cNumSerie,char* fecha_rev)
{
   X509_REVOKED *revocado = NULL;
   bool bResp = false;

   revocado = generaRevocado(cNumSerie, fecha_rev);
   if (revocado != NULL)
   {
      X509_CRL_add0_revoked(lista, revocado);
      bResp = true;
   }
   else iError = ERR_AGR_REV;   
   return bResp;
}



X509_REVOKED* GeneraCRL::
generaRevocado(char* num_Serie, char* fecha_rev)
{

   X509_REVOKED *revocado = X509_REVOKED_new();

   const unsigned char *num_serie_apu = (const unsigned char*)num_Serie;
   CFecha fecha; 
   string sFecha = fecha_rev;
   string tipoCmb;
   int error;

   time_t fecha_rev_t = fecha.ConvFechFtoBDTOtime_t( sFecha, tipoCmb, &error);

   if ( X509_REVOKED_set_serialNumber(revocado,c2i_ASN1_INTEGER(NULL, &num_serie_apu,20)) )
   {
      if (!X509_REVOKED_set_revocationDate(revocado, ASN1_UTCTIME_set(NULL, fecha_rev_t)))
         iError = ERR_AGR_FECH_REV;
   }else iError = ERR_AGR_NO_SER;
  
   return revocado;
 
}


X509* GeneraCRL::
sujeto(const char *rutaCertAC)
{
   X509        *cert = NULL;
   X509_NAME *sujeto = NULL;
   FILE      *cert_f = NULL;

   cert_f = fopen(rutaCertAC,"r");
   if ( cert_f )
   {
      cert = d2i_X509_fp(cert_f,NULL);
      if (cert == NULL)
      {
         rewind(cert_f);
         cert = PEM_read_X509(cert_f,NULL,NULL,NULL);
      }
      if (cert != NULL)
      {
         sujeto = X509_get_subject_name(cert);
         if (sujeto != NULL)
         {
            X509_CRL_set_issuer_name(lista, sujeto);
         } else iError = ERR_OBT_SUJ;

      }else iError = ERR_OBT_CERT;

   }else iError = ERR_ABR_CERT;

   if (cert_f != NULL)
   {
      fclose(cert_f);
      cert_f = NULL;
   }
   if (sujeto != NULL)
      X509_NAME_free(sujeto);

   return cert;
}


bool GeneraCRL::
inicia(int iHorasActCRL)
{
   lista = X509_CRL_new();
   if( lista != NULL)
   {
      if (iHorasActCRL > 0)
         iFechaProxAct = iHorasActCRL;
      bIniciado = true;
   }
   return bIniciado;
}

bool GeneraCRL::
fechas()
{
   ASN1_TIME  *hoy     = ASN1_TIME_new();
   ASN1_TIME  *proximo = ASN1_TIME_new();
   time_t t_proximo;
   bool bResp = false;

   time(&t_proximo);

   if (iFechaProxAct < 0)
      iFechaProxAct = 25; 

   t_proximo =  t_proximo + (iFechaProxAct*3600);
   
   if ( ASN1_TIME_set(proximo,t_proximo) != NULL)
   {
      if( X509_gmtime_adj(hoy,0) != NULL)
      {
         if( X509_CRL_set_lastUpdate(lista, hoy))
         {   
            if (X509_CRL_set_nextUpdate(lista, proximo))
               bResp = true;
            else iError = ERR_AGR_PROX_ACT;

         }else iError = ERR_AGR_FECHA_ACT;

      }else iError = ERR_AGR_FECHA;

   }else iError = ERR_ARM_TIEMPO_PROX;

   if( hoy != NULL)
      ASN1_TIME_free(hoy);
   if(proximo != NULL)
      ASN1_TIME_free(proximo);

   return bResp;
}

EVP_PKEY* GeneraCRL::
llavePrivada(const char* cLlavePrivada, const char* cPassword, int iPassword)
{

   EVP_PKEY    *llaveprivada = NULL;
   PKCS8_PRIV_KEY_INFO *info = NULL;
   FILE           *llavepriv = NULL;
   X509_SIG           *pkcs8 = NULL;

   llavepriv = fopen(cLlavePrivada,"r");
   if (llavepriv != NULL)
   {
      pkcs8 = d2i_PKCS8_fp(llavepriv,NULL);
      if (pkcs8 != NULL)
      {
         info = PKCS8_decrypt( pkcs8, cPassword, iPassword);
         if (info != NULL)
            llaveprivada  = EVP_PKCS82PKEY(info);
         else iError = ERR_OBT_LLAVE_PRIV;

      }else iError = ERR_OBT_LLAVE_PRIV_F;

   }else iError = ERR_ABR_LLAVE_PRIV;

   if (info != NULL)
      PKCS8_PRIV_KEY_INFO_free(info);
   if (llavepriv !=  NULL)
   {
      fclose(llavepriv);
      llavepriv = NULL;
   }
   if (pkcs8 != NULL)
      X509_SIG_free(pkcs8);

   return llaveprivada;
}

bool GeneraCRL::
guardaCRL(char *cRutaNombreLista)
{

   FILE *lista_crl = NULL;
   bool bResp =false;

   lista_crl = fopen(cRutaNombreLista, "w");
   if (lista_crl != NULL)
   {
      int res = i2d_X509_CRL_fp(lista_crl, lista);
      if(res)
         bResp = true;
      else iError = ERR_GDR_CRL;

   }else iError = ERR_CREAR_CRL;

   if (lista_crl != NULL)
   {
      fclose(lista_crl);
      lista_crl = NULL;
   }
   return bResp;

}


bool GeneraCRL::
generaCRL(const char* cCertificado, const char* cLlavePrivada, const char* cPassword, int iPassword, const char* cRutaNombreLista)
{

   if (!bIniciado )
      return false;

   X509      *x509Certificado = NULL;
   EVP_PKEY  *evpLlavePrivada = NULL;
   const EVP_MD    *digestion = NULL;
   bool bResp = false;


   X509_CRL_set_version(lista, 0);

   x509Certificado = sujeto(cCertificado);
   if (x509Certificado != NULL)
   {
      evpLlavePrivada = llavePrivada(cLlavePrivada,cPassword, iPassword);
      if (evpLlavePrivada != NULL)
      {
         if ( X509_check_private_key(x509Certificado,evpLlavePrivada) )
         {
            if ( fechas() )
            {
               X509_CRL_sort(lista);

               digestion  = EVP_sha1();
               if (digestion != NULL)
               {
                  if ( X509_CRL_sign(lista,evpLlavePrivada,digestion))
                  {
                     if ( guardaCRL((char*)cRutaNombreLista) )
                        bResp = true;
                  }
               }
            }
         }else iError = ERR_CERT_LLAVE_PRIV_NO_CORR;
      }
   }

   return bResp;
}

