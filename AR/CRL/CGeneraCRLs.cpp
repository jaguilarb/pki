static const char* _CGENERACRLS_CPP_VERSION_ ATR_USED = "CrearCRLsSAT @(#)"\
"DSIC10384AR_ 2010-09-08 CGeneraCRLs.cpp 1.0.0/0";

/*##############################################################################
  ###  DESARROLLADORES:                                                      ###
  ###                         Gudelia Hernández Molina     GHM               ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       septiembre 2010                                ###
  ###                                                                        ###
  ##############################################################################
         1         2         3         4         5         6         7         8
12345678901234567890123456789012345678901234567890123456789012345678901234567890
*/

/*#################################################################################
   VERSION:
      V.1.00      (20100908 - 20100927) GHM: Versión original

   CAMBIOS:

#################################################################################*/

#include <CGeneraCRLs.h>

#if ORACLE
   #define QS_NSERIE_VF_CERT "SELECT NOSERIE, VIGFIN FROM SEG_PKI.SEGT_CERTIFICADO WHERE TIPCERCVE = '2' AND (EDOCER = 'R' OR EDOCER = 'C')"
#else
   #define QS_NSERIE_VF_CERT "SELECT no_serie, vig_fin FROM certificado WHERE tipcer_cve = '2' AND (edo_cer = 'R' OR edo_cer = 'C')"
#endif

//Constructor
CGeneraCRLs::CGeneraCRLs(): m_Config( NULL ), m_Bitacora( NULL ) //Recibe como parámetros
{
   //Inicializa variables
   m_inicializado = false;
   m_LeeDatosCfg  = NULL;
   m_BD           = NULL;
   m_generaCRL    = NULL;
}

//Destructor
CGeneraCRLs::~CGeneraCRLs()
{
   if (m_LeeDatosCfg != NULL)
   {
      delete m_LeeDatosCfg;
      m_LeeDatosCfg = NULL;
   }
   if (m_BD != NULL)
   {
      m_BD->desconecta();
      delete m_BD;
      m_BD = NULL;
   }
   if (m_generaCRL != NULL)
   {
      delete m_generaCRL;
      m_generaCRL = NULL;
   }
}

//#################################################################################
bool CGeneraCRLs::inicia(CConfigFile *config, CBitacora *bitacora)
{
   m_Config = config;
   m_Bitacora = bitacora;
   if ( m_Config && m_Bitacora)
   {
      m_LeeDatosCfg = new CLeerCfgs();
      if ( m_LeeDatosCfg->inicia(m_Config, m_Bitacora) )
         m_inicializado = true;
   }
   return m_inicializado;
}
//#################################################################################
int CGeneraCRLs::iniciaBD()
{
   int         error = EXITO;
   
   std::string strMsgError = "";
   std::string instBD, nombBD, nombRol;      // Informix
   std::string hostBD, puertoBD, sidBD;      // Oracle
   std::string usrBD, pwdBD, condSQL;        // Oracle e Informix
   
   m_idProceso = "CGeneraCRLs(iniciaBD)";

   #if ORACLE
      error = m_LeeDatosCfg->getVarsCfgBDOracle(&hostBD, &puertoBD, &sidBD, &usrBD, &pwdBD, &condSQL);
   #else
      error = m_LeeDatosCfg->getVarsCfgBD(&instBD, &nombBD, &usrBD, &nombRol, &pwdBD, &condSQL);
   #endif

   if ( error != EXITO )
      return error;

   m_sqlextrac = QS_NSERIE_VF_CERT;
   if (condSQL.length() > 0)
      m_sqlextrac = m_sqlextrac + " AND no_serie LIKE '" + condSQL + std::string("\%';");

   m_BD = new CBD(m_Bitacora);
   if ( m_BD == NULL )
   {
       strMsgError = m_idProceso + " " + m_ObjMsgError.getMsgError(ERR_BD_CREAROBJ);
       m_Bitacora->escribe(BIT_ERROR, strMsgError.c_str());

      return ERR_BD_CNX;
   }

   bool resp = false;

   #if ORACLE
      resp = m_BD->setConfig(usrBD.c_str(), pwdBD.c_str(), hostBD.c_str(), puertoBD.c_str(), sidBD.c_str());
   #else
      resp = m_BD->setConfig(instBD.c_str(), nombBD.c_str(), usrBD.c_str(), pwdBD.c_str(), nombRol.c_str());
   #endif

   if ( !resp )
   {
      strMsgError = m_idProceso + " " + m_ObjMsgError.getMsgError(ERR_BD_SETCONFIG);
      m_Bitacora->escribe(BIT_ERROR, strMsgError.c_str());

      return ERR_BD_SETCONFIG;
   }

   if ( !m_BD->conecta() )
   {
      strMsgError = m_idProceso + " " + m_ObjMsgError.getMsgError(ERR_BD_CNX);
      m_Bitacora->escribe(BIT_ERROR, strMsgError.c_str());

      return ERR_BD_CNX;
   }

   return EXITO;
}

int CGeneraCRLs::procesoBD()
{
   std::string strMsgError = "";
   int error = EXITO; 
   std::string no_serie, vig_fin;
   m_idProceso = "CGeneraCRLs(procesoBD)";
    
   uint32 no_rows = 0L;
   if( m_BD->consulta( m_sqlextrac.c_str()) )
   {
      while( m_BD->sigReg() ) 
      {
         no_rows++;
         if(!m_BD->getValores("ss", &no_serie, &vig_fin))
         {
            m_Bitacora->escribePV(BIT_ERROR,"Error en CGeneraCRLs en procesoBD: Error de lectura de datos en el registro %i", no_rows);
            error = ERR_BD_DATOS;
            continue;
         }
         else
         {
            //Crea la lista y guarda los valores
             m_Bitacora->escribePV(BIT_DEBUG,"CGeneraCRLs.procesoBD: %s|%s|", no_serie.c_str(), vig_fin.c_str());
             addDatos(no_serie, vig_fin);
         }
      }
   }
   else
   {
      strMsgError = m_idProceso + " " + m_ObjMsgError.getMsgError(ERR_BD_EJECUTARQRY);
      m_Bitacora->escribePV(BIT_ERROR, "Error en CGeneraCRLs en procesoBD: %s - %s", strMsgError.c_str(), m_sqlextrac.c_str());
      error = ERR_BD_EJECUTARQRY;
   }
   return error;
}
//#################################################################################
int CGeneraCRLs::inicObjOpenSSL()
{
   int error = EXITO;
   m_idProceso = "CGeneraCRLs(inicObjOpenSSL)";
   if ( error != EXITO )
      return error;
   std::string sActHoras;
   error = m_LeeDatosCfg->getVarsActualizacionCRL(&sActHoras);
   if (!error)
   {   
      int horas = atoi(sActHoras.c_str());
      m_generaCRL = new GeneraCRL();
      if ( m_generaCRL != NULL)
      {
         m_generaCRL->inicia(atoi(sActHoras.c_str()));
         m_Bitacora->escribePV(BIT_DEBUG, "CGeneraCRLs.inicObjOpenSSL: Numero de horas para  actualizar %d", horas);
         error = EXITO;
      }
      else m_Bitacora->escribePV(BIT_ERROR,"Error en CGeneraCRLs en inicObjOpenSSL: Error al crear el objeto para generacion de CRL.");      
   
   }   
   return error;  
}

int CGeneraCRLs::addDatos(std::string numSerie, std::string vigFin)
{
   int error = EXITO;
   m_generaCRL->agregaRevocado((char*)numSerie.c_str(),(char*)vigFin.c_str()); 
   return error;
}

//#################################################################################
int CGeneraCRLs::Transmite(std::string nombCRL, std::string rutaCRL, std::string hostLocal, int numFTPs )
{
   int error = EXITO, i = 0;
   char        mens_err  [1204];
   std::string strCRLTransm, strCRLDestino;
   std::string strHostRemoto, strUsrRemoto, strDirRemoto;
   
   m_idProceso = "CGeneraCRLs(Transmite)";
   strCRLTransm = rutaCRL + "/" + nombCRL + ".crl";
   
   for ( i = 1; i <= numFTPs; i++)
   {
      error = m_LeeDatosCfg->getVarsCfgFTPs(i, &strHostRemoto, &strUsrRemoto, &strDirRemoto);
      if ( error == EXITO )
      {
         std::string comando = "mkdir "+strDirRemoto;
         error = EjecutaCmdoRem( (char*)strHostRemoto.c_str(),(char*)strUsrRemoto.c_str(),(char*)comando.c_str(),mens_err  );
         if( (error == EXITO) or (error = -41) ) //Este error indica que el directorio ya existe
         {
            strCRLDestino = strDirRemoto + "/" + nombCRL + ".crl";
            error = TransmiteArchivo((char*)strHostRemoto.c_str(), (char*)strUsrRemoto.c_str(),
                                  (char*)strCRLTransm.c_str(), (char*)strCRLDestino.c_str(), mens_err);
            if(error != EXITO)
            {
               m_Bitacora->escribePV(BIT_ERROR, "Error en CGeneraCRLs en inicObjOpenSSL: Al enviar el archivo (%s) al FTP (%d), error --> %s",
                                             strCRLTransm.c_str(), i, mens_err);
               error = ERR_SOCKET_NOTRASMITIO;
            }
         }
      }
   }
   return error;
}

//#################################################################################
int CGeneraCRLs::Proceso()
{
   std::string strMsgError = "";
   std::string strNombCRL = "", strRutaCRL = "", strHostLocal = "", strAC_AR;
   std::string certCRL, PrivKCRL, pwdCRL;
   int         error = EXITO, iNumFTPs = 0;
   m_idProceso = "CGeneraCRLs(Proceso)";
 
   error = m_LeeDatosCfg->getVarsCfgListas(&strNombCRL, &strRutaCRL, &strHostLocal, &strAC_AR, &iNumFTPs);
   if ( error == EXITO )
   {  
      if ( (error = inicObjOpenSSL()) == EXITO )
      {
         if ( (error = procesoBD()) == EXITO )
         {
            std::string strCRL = strRutaCRL + "/" + strNombCRL;
            error = m_LeeDatosCfg->getVarsCfgLlaves(&certCRL, &PrivKCRL, &pwdCRL);
            if ( error == EXITO )
            {
               std::string sCRL= strCRL +".crl";
               error = m_generaCRL->generaCRL(certCRL.c_str(), PrivKCRL.c_str(),pwdCRL.c_str(), pwdCRL.size(), sCRL.c_str());
               if(error)
                  m_Bitacora->escribePV(BIT_INFO, "CGeneraCRLs.Proceso: CRL generada  y guardada en [%s]" ,sCRL.c_str());
               else
                  m_Bitacora->escribePV(BIT_ERROR, "Error en CGeneraCRLs en Proceso: Ocurrio el error %d al generar la CRL" ,m_generaCRL->getError());   

            }
         }
      }
   }
   if( error)
   {
      if (!Transmite(strNombCRL, strRutaCRL, strHostLocal, iNumFTPs))
         m_Bitacora->escribePV(BIT_INFO, "CRL enviada al FTP");
      //Transmite el archivo
   }
   return error;
}
