#ifndef _CGENERACRLS_H_  
#define _CGENERACRLS_H_

static const char* _CGENERACRLS_H_VERSION_ ATR_USED = "CrearCRLsSAT @(#)"\
"DSIC10384AR_ 2010-09-08 CGeneraCRLs.h 1.0.0/0";

//#VERSION: 1.0.0
/*##############################################################################
  ###  PROYECTO:              Aplicación                                     ###
  ###  MODULO:                CGeneraCRLs: clase que maneja las operaciones  ###
  ###                                      para generar listas de Revocación ###
  ###  DESARROLLADORES:                                                      ###
  ###                         Gudelia Hernández Molina     GHM               ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       septiembre 2010                                ###
  ###                                                                        ###
  ##############################################################################
         1         2         3         4         5         6         7         8
12345678901234567890123456789012345678901234567890123456789012345678901234567890
*/

/*#################################################################################
   VERSION:
      V.1.00      (20100908 - 20100927) GHM: Versión original

   CAMBIOS:

#################################################################################*/
#include <SockSeg.h>
#include <dirent.h>
#include <time.h>

#include <Definiciones_CRLs.h>
#include <CLeerCfgs.h>
#include <Sgi_BD.h>
#include <Sgi_Fecha.h>
#include <GeneraCRL.h>

//#################################################################################
class CGeneraCRLs 
{
   protected:
      //Se esperan como parámetros
      CConfigFile   *m_Config;
      CBitacora     *m_Bitacora;
      //Se utilizan en toda la clase
      bool           m_inicializado;
      CLeerCfgs     *m_LeeDatosCfg;
      CBD           *m_BD;
      std::string    m_sqlextrac;
      GeneraCRL     *m_generaCRL;
      //Manejo de errores
      std::string    m_idProceso;
      CMsgError      m_ObjMsgError;
   
   public:
      CGeneraCRLs();
      ~CGeneraCRLs();
      
      bool inicia(CConfigFile *config, CBitacora *bitacora);
      int  iniciaBD();
      int  procesoBD();
      int  inicObjOpenSSL();

      int  addDatos(std::string numSerie, std::string vigFin);
      int  Transmite(std::string nombCRL, std::string rutaCRL, std::string hostLocal, int numFTPs );
      int  Proceso();
};

#endif //_CGENERACRLS_H_
