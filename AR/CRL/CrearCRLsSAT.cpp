static const char* _CREARCRLSSAT_CPP_VERSION_ ATR_USED = "CrearCRLsSAT @(#)"\
"DSIC10384AR_ 2010-09-08 CrearCRLsSAT.cpp 1.0.0/0";

//#VERSION: 1.0.0
/*###############################################################################
  ###  PROYECTO:              Aplicaci�n                                      ###
  ###  MODULO:                CrearCRLsSAT: Programa principal que maneja las ###
  ###                                       operaciones para generar listas   ###
  ###                                       de Revocaci�n                     ###
  ###  DESARROLLADORES:                                                       ###
  ###                         Gudelia Hern�ndez Molina     GHM                ###
  ###                                                                         ###
  ###  FECHA DE INICIO:       septiembre 2010                                 ###
  ###                                                                         ###
  ##############################################################################
         1         2         3         4         5         6         7         8
12345678901234567890123456789012345678901234567890123456789012345678901234567890
*/

/*#################################################################################
   VERSION:
      V.1.00      (20100908 - 20100927) GHM: Versi�n original

   CAMBIOS:

#################################################################################*/

#include <string>
#include <iostream>
#include <Definiciones_CRLs.h>
#include <CGeneraCRLs.h>

//#################################################################################
void printMsgInicError(int errorRecib, std::string dato)
{
   std::string msgError = "";

   switch (errorRecib)
   {
      case ERR_LEER_ARCHCONFIG:
         std::cout << "\tNo se ley� correctamente el archivo de configuraci�n:" << endl;
         std::cout << "\t\t" << dato.c_str() << endl;
         break;
      case ERR_LEER_ARCHBITAC:
         std::cout << "\tNo se abri� correctamente la bit�cora" << endl;
         std::cout << "\t\t" << dato.c_str() << endl;
         break;
      case ERR_NUM_PARAM_RECIB:
      default:
         std::cout << endl;
         std::cout << "\tUso: " << endl << "\t\t" << dato << "   /ruta/Archivo_de_Configuraci�n" << endl << endl;
         std::cout << "\t�" << endl << endl;
         std::cout << "\t\t" << dato << endl << endl;
         break;
   }
   return;
}

//#################################################################################
int procesaParametros(int argc, char* argv[], CConfigFile **ptrConfig, CBitacora **ptrBitac )
{
   std::string dato;
   int         error = EXITO; 

   //Determina el archivo de configuraci�n
   if ( argc == 1 )
      dato = std::string(ARCH_CONF);
   else
   {
      if ( argc == 2 )
         dato = std::string(argv[1]);
      else
      {
         dato = std::string(argv[0]);
         error = ERR_NUM_PARAM_RECIB;
      }
   }
   if ( error == EXITO )
   {
      if ( (*ptrConfig = new CConfigFile((const char *) dato.c_str())) != NULL )
      {
         //Inicializa objeto de bit�cora
         std::string strArchCfg = dato;
         int errorCarga = EXITO;
         if ( (errorCarga = (*ptrConfig)->cargaCfgVars()) )
         {
            std::cout << endl << "Se present� el error (" << errorCarga; 
            std::cout << ") al leer variables del ArchConfig(" << strArchCfg.c_str() << endl << endl;
            error = ERR_CARGAR_VARSCFG;
         }
         else
         {
            int r1 = 0;
            r1 = (*ptrConfig)->getValorVar("[BITACORA]", "RUTA", &dato);
            if ( r1 != 0 )
               dato = std::string(PATH_BITA);
            if ( (*ptrBitac = new CBitacora(dato.c_str())) != NULL )
            {
               //Establece el nivelde bit�cora
               std::string nvlBit = "";
               int r1 = (*ptrConfig)->getValorVar("[BITACORA]", "NIVELBIT", &nvlBit);
               if ( r1 != 0)
                  nvlBit = "3";
               (*ptrBitac)->setNivel( (BIT_NIVEL) atoi(nvlBit.c_str()) );
               (*ptrBitac)->escribePV(BIT_DEBUG, "Archivo de Configuraci�n utilizado: %s", strArchCfg.c_str());
            }
            else
               error = ERR_LEER_ARCHBITAC;
         }
      }
      else
         error = ERR_LEER_ARCHCONFIG;
   }
   if ( error != EXITO )
      printMsgInicError(error, dato);
   return error;
}

//#################################################################################
int main (int argc, char* argv[])
{
   int valRetorno = EXITO;
   CConfigFile *cfg = NULL;
   CBitacora   *bit = NULL;
   CGeneraCRLs *generadorCRLs = NULL;
   
   if ( (valRetorno = procesaParametros(argc, argv, &cfg, &bit)) == EXITO )
   {
      generadorCRLs = new CGeneraCRLs();
      if ( generadorCRLs->inicia(cfg, bit) )
      {
         if ( (valRetorno = generadorCRLs->iniciaBD()) == EXITO )
         {
            if ( (valRetorno = generadorCRLs->Proceso()) == EXITO )
               return EXITO;
               //Pendiente 
         }
      }
      else
      {
         std::cout << "No se inicializ� el generador correctamente" << endl << endl; 
         valRetorno = -2;
      }
   }
   else
      valRetorno = -1;
   //Cierre de apuntadores
   if (cfg != NULL)
   {
      delete cfg;
      cfg = NULL;
   }
   if (bit != NULL)
   {
      delete bit;
      bit = NULL;
   }
   if (generadorCRLs != NULL) 
   {
      delete generadorCRLs;
      generadorCRLs = NULL;
   }
   return valRetorno;
}

