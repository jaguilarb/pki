static const char* _CLEERCFGS_CPP_VERSION_ ATR_USED = "CrearCRLsSAT @(#)"\
"DSIC10384AR_ 2010-09-08 CLeeraCfgs.cpp 1.0.0/0";

#include <Definiciones_CRLs.h>
#include <CLeerCfgs.h>

//Constructor
CLeerCfgs::CLeerCfgs(): objConfig( NULL ), objBitac( NULL ) //Recibe como parámetros
{
   //Inicializa variables
   m_inicializado = false;   
}

//Destructor
CLeerCfgs::~CLeerCfgs()
{
}

//#################################################################################
bool CLeerCfgs::inicia(CConfigFile *config, CBitacora *bitacora)
{
   objConfig = config;
   objBitac  = bitacora;
   if ( objConfig && objBitac)
      m_inicializado = true;
   return m_inicializado;
}

//#################################################################################
//Desencrita el password
bool CLeerCfgs::procesaPwd(const std::string& s_pwdEnc, std::string *s_pwdDes)
{
   bool b_resOperacion = true;
   int i_pwdSalida = s_pwdEnc.size();
   char *psz_pwdSalida = new char[i_pwdSalida];
   if (!desencripta((uint8*) s_pwdEnc.c_str(), i_pwdSalida, (uint8*) psz_pwdSalida, &i_pwdSalida))
      b_resOperacion = false;
   else
      s_pwdDes->assign(psz_pwdSalida, i_pwdSalida);
   delete[] psz_pwdSalida;
   return b_resOperacion;
}

//#################################################################################
//Carga Variables de Configuración de la Base de Datos
int CLeerCfgs::getVarsCfgBD(std::string *instancia, std::string *nombreBD, std::string *usuario,
                            std::string *nombreRol, std::string *Pwd     , std::string *datoCond)
{
   int         r1, r2, r3, r4, r5, r6;
   std::string strMsgError = "", strAuxPwd = "";
   m_idProceso = "CLeerCfgs(getVarsCfgBD)";

   r1 = objConfig->getValorVar("[BD_SAT]"     , "INSTANCIA", instancia);
   r2 = objConfig->getValorVar("[BD_SAT]"     , "BD"       , nombreBD);
   r3 = objConfig->getValorVar("[BD_SAT]"     , "USR"      , usuario);
   r4 = objConfig->getValorVar("[BD_SAT]"     , "PWD"      , &strAuxPwd);
   r5 = objConfig->getValorVar("[BD_SAT]"     , "ROLE"     , nombreRol);
   r6 = objConfig->getValorVar("[GENERAL_CRL]", "AC_AR"    , datoCond);
   //Verifica si se leyeron todos los parámetros correctamente
   if ( r1 || r2 || r3 || r4 || r5)
   {
      strMsgError = m_idProceso + " " + m_ObjMsgError.getMsgError(ERR_LEER_VARSCONFIG);
      objBitac->escribePV(BIT_ERROR, "Error en CLeerCfgs en getVarsCfgBD: %s (%d, %d, %d, %d, %d)",
                                     strMsgError.c_str(), r1, r2, r3, r4, r5);
      return ERR_LEER_VARSCONFIG;
   }
   objBitac->escribePV(BIT_DEBUG, "CLeerCfgs.getVarsCfgBD: %s: Instancia(%s), BD(%s), Usuario(%s), Rol(%s), AC_AR(%s)",
                                  m_idProceso.c_str(), instancia->c_str(), nombreBD->c_str(),
                                  usuario->c_str(), nombreRol->c_str(), datoCond->c_str());
   if ( !procesaPwd(strAuxPwd, Pwd) )
   {
      strMsgError = m_idProceso + " " + m_ObjMsgError.getMsgError(ERR_DESENCRIPTA);
      objBitac->escribe(BIT_ERROR, strMsgError.c_str());
      return ERR_DESENCRIPTA;
   }
   return EXITO;
}

//#################################################################################
//Carga Variables de Configuración de la Base de Datos
int CLeerCfgs::getVarsCfgBDOracle(std::string *host, std::string *puerto, std::string *sid, std::string *usuario, std::string *Pwd, std::string *datoCond)
{
   int         r1, r2, r3, r4, r5, r6;

   std::string strMsgError = "", strAuxPwd = "";

   m_idProceso = "CLeerCfgs(getVarsCfgBDOracle)";

   r1 = objConfig->getValorVar("[BD_SAT_ORACLE]"     , "HOST"  , host);
   r2 = objConfig->getValorVar("[BD_SAT_ORACLE]"     , "PORT"  , puerto);
   r3 = objConfig->getValorVar("[BD_SAT_ORACLE]"     , "SID"   , sid);
   r4 = objConfig->getValorVar("[BD_SAT_ORACLE]"     , "USR"   , usuario);
   r5 = objConfig->getValorVar("[BD_SAT_ORACLE]"     , "PWD"   , &strAuxPwd);
   r6 = objConfig->getValorVar("[GENERAL_CRL]"       , "AC_AR" , datoCond);

   //Verifica si se leyeron todos los parámetros correctamente
   if ( r1 || r2 || r3 || r4 || r5 || r6)
   {
      strMsgError = m_idProceso + " " + m_ObjMsgError.getMsgError(ERR_LEER_VARSCONFIG);

      objBitac->escribePV(BIT_ERROR, "Error en CLeerCfgs en getVarsCfgBDOracle: %s (%d, %d, %d, %d, %d)", strMsgError.c_str(), r1, r2, r3, r4, r5);

      return ERR_LEER_VARSCONFIG;
   }

   objBitac->escribePV(BIT_DEBUG, "CLeerCfgs.getVarsCfgBDOracle: %s: Host(%s), Puerto(%s), Sid(%s), Usuario(%s), AC_AR(%s)",
                                  m_idProceso.c_str(), host->c_str(), puerto->c_str(),
                                  sid->c_str(), usuario->c_str(), datoCond->c_str());

   if ( !procesaPwd(strAuxPwd, Pwd) )
   {
      strMsgError = m_idProceso + " " + m_ObjMsgError.getMsgError(ERR_DESENCRIPTA);

      objBitac->escribe(BIT_ERROR, strMsgError.c_str());

      return ERR_DESENCRIPTA;
   }
   
   return EXITO;
}
//#################################################################################
int CLeerCfgs::getVarsCfgLlaves(std::string *cert, std::string *llavePriv, std::string *pwdPrivK)
{
   std::string strAuxPwd ;
   int         r1, r2, r3;
   std::string strMsgError = "";
   m_idProceso = "CLeerCfgs(getVarsCfgLlaves)";
   r1 = objConfig->getValorVar("[LLAVES_FIRMA]", "CERT" , cert);
   r2 = objConfig->getValorVar("[LLAVES_FIRMA]", "PRIVK", llavePriv);
   r3 = objConfig->getValorVar("[LLAVES_FIRMA]", "PWD"  , &strAuxPwd);
   //Verifica si se leyeron todos los parámetros correctamente
   if ( r1 || r2 || r3 )
   {
      strMsgError = m_idProceso + " " + m_ObjMsgError.getMsgError(ERR_LEER_VARSCONFIG);
      objBitac->escribePV(BIT_ERROR, "Error en CLeerCfgs en getVarsCfgLlaves: %s (%d, %d, %d)",
                                     strMsgError.c_str(), r1, r2, r3);
      return ERR_LEER_VARSCONFIG;
   }
   objBitac->escribePV(BIT_DEBUG, "CLeerCfgs.getVarsCfgLlaves: %s: Cert(%s), privK(%s)",
                                  m_idProceso.c_str(), cert->c_str(), llavePriv->c_str());
   if(!procesaPwd(strAuxPwd, pwdPrivK))
   {
      strMsgError = m_idProceso + " " + m_ObjMsgError.getMsgError(ERR_DESENCRIPTA);
      objBitac->escribe(BIT_ERROR, strMsgError.c_str());
      return ERR_DESENCRIPTA;
   }
   return EXITO;
}

//#################################################################################
int CLeerCfgs::getVarsCfgListas(std::string *nombLista, std::string *pathCRL, std::string *hostLocal, 
                                std::string *ac_ar,     int *numFtps)
{
   int r1, r2, r3, r4, r5;
   std::string strMsgError = "";
   std::string strAuxNumFTPs = "";
   m_idProceso = "CLeerCfgs(getVarsCfgListas)";

   r1 = objConfig->getValorVar("[GENERAL_CRL]", "NOMBRE_CRL", nombLista);
   r2 = objConfig->getValorVar("[GENERAL_CRL]", "RUTA_CRLS" , pathCRL);
   r3 = objConfig->getValorVar("[GENERAL_CRL]", "HOST_LOCAL"  , hostLocal);
   r4 = objConfig->getValorVar("[GENERAL_CRL]", "NUM_FTPS"  , &strAuxNumFTPs);
   r5 = objConfig->getValorVar("[GENERAL_CRL]", "AC_AR"     , ac_ar);
   //Verifica si se leyeron todos los parámetros correctamente
   if ( r1 || r2 || r3 || r4 )
   {
      strMsgError = m_idProceso + " " + m_ObjMsgError.getMsgError(ERR_LEER_VARSCONFIG);
      objBitac->escribePV(BIT_ERROR, "Error en CLeerCfgs en getVarsCfgListas: %s (%d, %d, %d, %d)",
                                     strMsgError.c_str(), r1, r2, r3, r4);
      return ERR_LEER_VARSCONFIG;
   }
   *numFtps = atoi(strAuxNumFTPs.c_str());
   if ( (*numFtps < 0) || (*numFtps > 99) )
   {
      strMsgError = m_idProceso + " " + m_ObjMsgError.getMsgError(ERR_FTP_MAXNUM);
      objBitac->escribe(BIT_ERROR, strMsgError.c_str());
      return ERR_FTP_MAXNUM;
   }
   objBitac->escribePV(BIT_DEBUG, "CLeerCfgs.getVarsCfgListas: nombreArch(%s), ruta(%s), hostFte(%s), ac_ar(%s), numFTPs(%d) " ,
                                  nombLista->c_str(), pathCRL->c_str(), hostLocal->c_str(), ac_ar->c_str(), *numFtps);
   return EXITO;
}

//#################################################################################
int CLeerCfgs::getVarsCfgFTPs(int numFtp, std::string *hostDst, std::string *usrDst, std::string *dirDest)
{
   int r1, r2, r3;
   char etiq[10];
   std::string strMsgError = "";
   m_idProceso = "CLeerCfgs(getVarsCfgFTPs)";
   *hostDst = "";
   *usrDst = "";
   *dirDest = "";
    
   sprintf(etiq, "[FTP-%02d]", numFtp);
   r1 = objConfig->getValorVar(etiq, "HOST_DEST", hostDst);
   r2 = objConfig->getValorVar(etiq, "USER_DEST", usrDst);
   r3 = objConfig->getValorVar(etiq, "DIR_DEST" , dirDest);
   //Verifica si se leyeron todos los parámetros correctamente
   if ( r1 || r2 || r3 )
   {
      strMsgError = m_idProceso + " " + m_ObjMsgError.getMsgError(ERR_FTP_VARSCONFIG);
      objBitac->escribePV(BIT_ERROR, "Error en CLeerCfgs en getVarsCfgFTPs: %s (%d, %d, %d)",
                                     strMsgError.c_str(), r1, r2, r3);
      return ERR_FTP_VARSCONFIG;
   }
   objBitac->escribePV(BIT_DEBUG, "CLeerCfgs.getVarsCfgFTPs: hostDestino(%s), usrRemoto(%s), dirDestino(%s)",
                                  hostDst->c_str(), usrDst->c_str(), dirDest->c_str());
   return EXITO;
}

//#################################################################################
int CLeerCfgs::getVarsCfgExtens(std::string *AuthKeyId)
{
   int r1;
   std::string strMsgError = "";
   m_idProceso = "CLeerCfgs(getVarsCfgExtens)";
   r1 = objConfig->getValorVar("[EXTENSIONES_CRL]", "AuthorityKeyIdentifier", AuthKeyId);
   //Verifica si se leyeron todos los parámetros correctamente
   if ( r1 )
   {
      strMsgError = m_idProceso + " " + m_ObjMsgError.getMsgError(ERR_LEER_VARSCONFIG);
      objBitac->escribePV(BIT_ERROR, "Error en CLeerCfgs en getVarsCfgExtens: %s (%d)",
                                     strMsgError.c_str(), r1);
      return ERR_LEER_VARSCONFIG;
   }
   objBitac->escribePV(BIT_DEBUG, "CLeerCfgs.getVarsCfgExtens: AuthorKeyId(%s)", AuthKeyId->c_str()); 
   return EXITO;
}


int CLeerCfgs::
getVarsActualizacionCRL(std::string *sActHoras)
{
   int r1;
   std::string strMsgError = "";
   m_idProceso = "CLeerCfgs(getVarsActualizacionCRL)";
   r1 = objConfig->getValorVar("[ACTUALIZACION_CRL]", "HORAS", sActHoras);
   //Verifica si se leyeron todos los parámetros correctamente
   if ( r1 )
   {
      strMsgError = m_idProceso + " " + m_ObjMsgError.getMsgError(ERR_LEER_VARSCONFIG);
      objBitac->escribePV(BIT_ERROR, "Error en CLeerCfgs en getVarsActualizacionCRL: %s (%d)",
                                     strMsgError.c_str(), r1);
      return ERR_LEER_VARSCONFIG;
   }
   objBitac->escribePV(BIT_DEBUG, "CLeerCfgs.getVarsActualizacionCRL: HORAS (%s)", sActHoras->c_str());
   return EXITO;
}

