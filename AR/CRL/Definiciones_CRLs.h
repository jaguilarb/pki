#ifndef _DEFINICIONES_CRLS_H_  
#define _DEFINICIONES_CRLS_H_

static const char* _DEFINICIONES_CRLS_H_VERSION_ ATR_USED = "CrearCRLsSAT @(#)"\
"DSIC10384AR_ 2010-09-08 Definiciones_CRLs.h 1.0.0/0";

//#VERSION: 1.0.0
/*##############################################################################
  ###  PROYECTO:              Aplicación                                     ###
  ###  MODULO:                Definiciones CRLs: se definen los valores de   ###
  ###                                            error y otros datos.        ###
  ###  DESARROLLADORES:                                                      ###
  ###                         Gudelia Hernández Molina     GHM               ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       septiembre 2010                                ###
  ###                                                                        ###
  ##############################################################################
         1         2         3         4         5         6         7         8
12345678901234567890123456789012345678901234567890123456789012345678901234567890
*/

/*#################################################################################
   VERSION:
      V.1.00      (20100908 - 20100927) GHM: Versión original

   CAMBIOS:

#################################################################################*/

#include <string>
#include <Sgi_Bitacora.h>
#include <Sgi_ConfigFile.h>

#define MAX_TAM_NUMSERIAL          20

#define EXITO                       0
//Errores que van a pantalla
#define ERR_NUM_PARAM_RECIB        -1
#define ERR_LEER_ARCHBITAC         -2
#define ERR_LEER_ARCHCONFIG        -3

//Errores que van a bitácora
#define ERR_CARGAR_VARSCFG        -30
#define ERR_LEER_VARSCONFIG       -31

#define ERR_DESENCRIPTA           -41

#define ERR_BD_CNX                -51
#define ERR_BD_SETCONFIG          -52
#define ERR_BD_CREAROBJ           -53
#define ERR_BD_DATOS              -54
#define ERR_BD_EJECUTARQRY        -55

#define ERR_FTP_MAXNUM            -60
#define ERR_FTP_VARSCONFIG        -61 

#define ERR_SOCKET_NOTRASMITIO    -70

//#################################################################################
#define PATH_CONF            "/usr/local/SAT/PKI/etc/"
#define PATH_DBIT            "/var/local/SAT/PKI/"

#ifdef DBG
   #define ARCH_CONF            PATH_CONF "CRLs_SAT_dbg.cfg"
   #define PATH_BITA            PATH_DBIT "CRLsSAT_dbg.log"
#else
   #define ARCH_CONF            PATH_CONF "CRLs_SAT.cfg"
   #define PATH_BITA            PATH_DBIT "CRLsSAT.log"
#endif
//#################################################################################

class CMsgError
{
   public:
      std::string getMsgError(int errorRec);
};

#endif //_DEFINICIONES_CRLS_H
