#ifndef _CLEERCFGS_H_
#define _CLEERCFGS_H_

static const char* _CLEERCFGS_H_VERSION_ ATR_USED = "CrearCRLsSAT @(#)"\
"DSIC10384AR_ 2010-09-08 CLeerCfgs.h 1.0.0/0";

#include <Definiciones_CRLs.h>
#include <Sgi_ProtegePwd.h>

class CLeerCfgs
{
   protected:
      //Se espera como parámetros
      CConfigFile *objConfig;
      CBitacora   *objBitac;
      //Se utilizan en toda la clase
      bool        m_inicializado;
      //Manejo de errores
      std::string m_idProceso;
      CMsgError   m_ObjMsgError;
  
   public:
      CLeerCfgs();
      ~CLeerCfgs();

      bool inicia(CConfigFile *config, CBitacora *bitacora);
      bool procesaPwd(const std::string& s_pwdEnc, std::string *s_pwdDes);

      int  getVarsCfgBD(std::string *instancia, std::string *nombreBD, std::string *usuario,
                        std::string *nombreRol, std::string *Pwd     , std::string *datoCond);
      int  getVarsCfgBDOracle(std::string *host, std::string *puerto, std::string *sid, std::string *usuario, std::string *Pwd, std::string *datoCond);
      int  getVarsCfgLlaves(std::string *cert, std::string *llavePriv, std::string *pwdPrivK);
      int  getVarsCfgListas(std::string *nombLista, std::string *pathCRL, std::string *hostLocal, 
                            std::string *ac_ar,     int *numFtps);
      int  getVarsCfgFTPs(int numFtp, std::string *hostDst, std::string *usrDst, std::string *dirDest);
      int  getVarsCfgExtens(std::string *AuthKeyId);
      int  getVarsActualizacionCRL(std::string *sActHoras);
};

#endif //_CLEERCFGS_H_
