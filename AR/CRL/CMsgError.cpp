static const char* _CMSGERROR_CPP_VERSION_ ATR_USED = "CrearCRLsSAT @(#)"\
"DSIC10384AR_ 2010-09-08 CMsgError.cpp 1.0.0/0";

#include <Definiciones_CRLs.h>

//#################################################################################
std::string CMsgError::getMsgError(int errorRec)
{
   switch (errorRec)
   {
      //Errores directos en pantalla
      case EXITO                      : return "El proceso se realizo exitosamente.";
      case ERR_NUM_PARAM_RECIB        : return "Error: Revise el número y contenido en los parámetros enviados en CrearCRLsSAT.";
      case ERR_LEER_ARCHBITAC         : return "Error: Al utilizar el archivo de bitácora.";
      case ERR_LEER_ARCHCONFIG        : return "Error: No se puede leer el archivo de configuración.";

      //Errores para bitácora
      case ERR_CARGAR_VARSCFG         : return "No se cargaron a memoria las variables del archivo de configuración.";
      case ERR_LEER_VARSCONFIG        : return "No se leyó correctamente el archivo de configuración.";
      
      case ERR_DESENCRIPTA            : return "No se puede desencriptar.";
      
      case ERR_BD_CNX                 : return "No se estableció conexión a la BD.";
      case ERR_BD_SETCONFIG           : return "No se cargo configuracion al objeto de BD.";
      case ERR_BD_CREAROBJ            : return "No se puede crear el objeto de BD.";
      case ERR_BD_DATOS               : return "Error en los datos extraídos de la BD.";
      case ERR_BD_EJECUTARQRY         : return "Fallo la ejecución del Query.";
      
      case ERR_FTP_MAXNUM             : return "El numero de FTPs permitidos el >0 y <99, revise el dato en el archivo de configuración.";
      case ERR_FTP_VARSCONFIG         : return "Verifique que la configuración del FTP sea correcta.";
      case ERR_SOCKET_NOTRASMITIO     : return "No se trasmitio correctamente el archivo";
       
      default                         : return "Error no determinado.";
   }
   return "Error Interno";
}

