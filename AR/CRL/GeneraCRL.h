#ifndef _GENERACRL_H_
#define _GENERACRL_H_

static const char* _GENERACRL_H_VERSION_ ATR_USED = "CrearCRLsSAT @(#)"\
"DSIC10384AR_ 2010-09-08 GeneraCRL.h 1.0.0/0";

#include <openssl/asn1t.h>
#include <openssl/err.h>
#include <openssl/x509.h>
#include <openssl/pkcs12.h>
#include <openssl/pem.h>
#include <string>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <Sgi_Bitacora.h>
#include <Sgi_BD.h>
#include <Sgi_Fecha.h>

#define ERR_AGR_REV                    500
#define ERR_AGR_FECH_REV               501
#define ERR_AGR_NO_SER                 502
#define ERR_OBT_SUJ                    503
#define ERR_OBT_CERT                   504
#define ERR_ABR_CERT                   505
#define ERR_AGR_PROX_ACT               506
#define ERR_AGR_FECHA_ACT              507   
#define ERR_AGR_FECHA                  508
#define ERR_ARM_TIEMPO_PROX            509
#define ERR_OBT_LLAVE_PRIV             510
#define ERR_OBT_LLAVE_PRIV_F           511
#define ERR_ABR_LLAVE_PRIV             512
#define ERR_CERT_LLAVE_PRIV_NO_CORR    513
#define ERR_GDR_CRL                    514
#define ERR_CREAR_CRL                  515

class GeneraCRL
{
   public:

      GeneraCRL();
      ~GeneraCRL();
      bool inicia(int iHoras);
      bool generaCRL(const char* cCertificado, const char* cLlavePrivada, const char* cPassword, int iPassword, const char* cRutaNombreLista);
      bool agregaRevocado(char* cNumSerie,char* fecha_rev);
      void setFechaProxAct(int iHoras);
      int getError();

   private:  

      X509_CRL *lista;
      X509_REVOKED *revocado;
      bool bIniciado;
      int iFechaProxAct;
      int iError;
   
      bool guardaCRL(char* cRutaNombreCRL);
      bool fechas();
      X509_REVOKED* generaRevocado(char* num_Serie, char* fecha_rev);
      X509_NAME* sujeto1(const char *rutaCertAC);
      X509* sujeto(const char *rutaCertAC);
      EVP_PKEY* llavePrivada(const char* cLlavePrivada, const char* cPassword, int iPassword);
};

#endif // _GENERACRL_H_

