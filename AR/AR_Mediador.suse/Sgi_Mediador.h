#ifndef _SGI_MEDIADOR_H_
#define _SGI_MEDIADOR_H_
static const char* _SGI_MEDIADOR_H_VERSION_ ATR_USED = "AR_Mediador @(#)"\
"DSIC09041AR_ 2007-12-13 Sgi_Mediador.h 1.1.2/2";

//#VERSION: 1.1.2
#include <sys/wait.h>
#include <signal.h>

#include <sys/stat.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/uio.h>
#include <unistd.h>
#include <dirent.h>

#include <sys/sem.h>
#include <errno.h>
#include <sys/msg.h>


#include <Sgi_BD.h>
//--#include <Fechas.h>
#include <Sgi_Fecha.h> //++ 02.00.01 (070220) GHM: Librer�a de las funciones de fecha
#include <Sgi_SSL.h>
//- #include <MensajesSAT.h>   //- GHM (070427)
#include <Sgi_MsgPKI.h>        //+ GHM (070427)
//- #include <PwdProtection.h> //- GHM (070427)
#include <Sgi_ProtegePwd.h>    //+ GHM (070427)
//>- ERGL mar mar 20 17:09:34 CDT 2007 #include <ArchivosLog.h>
//>+ ERGL mar mar 20 17:09:57 CDT 2007
//- #include <CConfigFile.h>   //- GHM (070427)
#include <Sgi_ConfigFile.h>    //+ GHM (070427)
#include <Sgi_PKI.h>

//**********************

#define  CONECTA_IES       6001
#define  CONECTA_AC        6002
#define  ERR_RECV          6003 
#define  ERR_ENV           6004
#define  ERR_GENCERT       6005 
#define  ERR_REVCERT       6006
#define  ERROR             28
#define  ERR_DATONULO      6008
#define  ERR_RECREQ        6009 
#define  ERR_CREAARCHIVO   6010
#define  ERR_NOINICIADO    6011
#define  ERR_INICBD        6012
#define  ERR_LECT_ARCH_CONF   6014
#define  NO_PEND           6016
#define  SIN_OPER	         6017
#define  ERR_EN_BD         6018
#define CERTNOREGIES       6019
#define  DESCONEXIONDELAAC 6020



#define  TAM_AR            6
#define  TAM_NA            70 
#define  TAM_NS_M          21
#define  TAM_RUTAS         1024 
#define  TAM_NT            13
#define  TAM_PWD           30	
#define  TAM_DIVNOPER      6015
#define  TAM_BD            20
#define  TAM_VAR           50

#ifndef DBG
  #define SERV_FIFO "/tmp/fifo.serv"
#else
  #define SERV_FIFO "/tmp/fifod.serv"
#endif

#define FILE_MODE (S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH )


struct DATOSB
{
   char cont[TAM_DATOS];
   int  icont;
};

struct DATOS
{
   char cont[TAM_VAR];
   int  icont;
};


struct Req
{
   DATOSB  req;
   DATOS  pwd;
   DATOS  llaveMD5;
   DATOS  llaveSHA1;
   DATOS  correo;
};

struct Cert
{
  DATOSB cert;
  DATOS  vig_ini;
  DATOS  vig_fin;
  DATOS  num_serie;
  DATOS  rfc;
  DATOS  nombre;
};

struct DatCert
{
  DATOS estado;
  DATOS tipCert;
  DATOS fec_fin;
};

 
// SERR  12 dic 2005
// Nueva estructura que almacenar� lo de todas las anteriores.

struct stCert
{
   DATOSB  req;
   DATOS   PwdRev;
   DATOS   LlaveMD5;
   DATOS   LlaveSHA1;
   DATOSB  LlavePub;
   DATOS   Correo;
   DATOS   VigIni;
   DATOS   VigFin;
   DATOS   NumSer;
   DATOSB  Nombre;
   DATOS   RFC;
   DATOS   Curp;
   DATOS   RFCRL;
   DATOS   RFCCurp;
   DATOS   Edo;
   DATOS   TipCer;
   DATOS   FecRegIES;
   DATOSB  cert;
   

};



class Mediador
{

   public:

   //>- ERGL mar mar 20 17:12:02 CDT 2007 
   //ArchivosLog   log;
   //>+ ERGL mar mar 20 17:12:41 CDT 2007
   CBitacora *bit;
   //>> 02.00.01 (070220) GHM: Objeto para utilizar las funciones de fecha
   //--Fechas        fec;     //--
   CFecha     fec; //++
   //<< 02.00.01 
   bool       conectAC;
   bool       conectIES;


   bool   conRepr;

        Mediador();

   int  Inicia();
   int  Servicio();
   int  Termina();
   
   private :


   CSSL_parms  paramAC,paramIES;
   CSSL_Skt   *sktAC,*sktIES;
   Sgi_SSL     sslAC,sslIES;
  
   MensajesSAT mensAC,mensAR;
   MensajesIES mensIES;
   CBD  *bd;
   CBitacora  *bit_bd;
   CBitacora  *bit_ef;

   SGIX509 ReqCert;   
 

   long pid;

   char       bufferAC     [TAM_DATOS];
   char       bufferIES    [TAM_DATOS];

   char       buff          [TAM_DATOS];
   char   firma     [TAM_FIRMA];
   char   cNumTram  [TAM_NT];
   char   rfc_contr [TAM_RUTAS];
   char   pwdrev    [TAM_PWD];
   char   cSec[10];
   char   cNumSerie_ren[21];
   char   cFecACRev_ren[21];
   char   cBuffIESAct[TAM_DATOS];
   int    iBuffIESAct ;

   int    iSec ;
   int    iNumTram;
   int    ifirma;
   int    irfc_contr;      
   int    ipwdrev ;
   int    iNumSerie_ren;
   //int    iFechaRev_ren;
   int        lbuff;
   int        lbufferIES;
   int        lbufferAC;
   int        error;
   int    iRechazado;

   bool   para_ies;
   bool   esGeneracion;
   bool   esAlertado;
   bool   esRechazado;
   bool   enviadoAR;
   bool   terminarMed;
   bool   enRegistraAC;
   bool   esRenovacion;
   bool   bError;
    
   CConfigFile *conf;
  
   struct stCert cert;
   
   bool DesPWD(string sPwdEnc , char *cPwdDes ,int *iPwdDes);
   int  IniciaAR();
   int  IniciaAC();
   int  IniciaIES();
   int  ConectaBD();
   int  Conecta(); 
   int  ConectaIES();
   int  ConectaAC();
   int  Envia(int DES);   
   int  Recibe(int DES);
   int  ObtieneDatos();
   void Generacion();
   //>+ ERGL (16072007)
   int  pideCertNvoFmto();
   //<+
   int  DatosReq();
   int  RecibeReq();
   int  EnviaReqAC();
   int  RegistraAC(bool aceptado);
   bool RegistraIES();
   int  CertAcept_Rech(int iAoR);
   int  Actualiza_FecRegIES(char *cFecRegIES, char *fecha_IES_t );
   //int  ServidorAC();
   int  ServidorAC(int rEsperada);
   int  RegistroBD();
   int  EnviarAR();
   void MarcaARA(char *cEstado, char *cNumSer = NULL);
   int  MarcaDario( char *cRfc , char *cEdoCert);
   int  GuardaCert();
   int  RecibeRev();
   int  Revocacion_AC();
   int  Revocacion_IES();
   int  Revocacion_AR( char * cNumTram_f  , char * cNumSerie_f ,char * cFecRev_f = NULL);
   int  Revocacion();
   int  BROADCAST_IES();
   int  RegistraOper(int iError ,char *cTramite, int iOper );
   int  RecibeMSGERROR();
   void Respuesta();
   int  RecibeCERTREVAR();
   void ErrorAR_AC(int iDest  ,char *cTextErr , int iError , char *cTipE);
   bool CreaTramite(char *cOper , char * cNumTram_al ,char *cRfc );
   int  CertAlertaIES( bool esGeneracion);
   int  DatosCert(char *num_serie ,char *rfc);
   int  BuscaCert(char* cNumSerie , char *cCert , int *iCert);
   int  ConsultaAIES();
   void Limpia();
   int  RecibeCERTRENAC();
   int  Desconexion();
   long leeBuffer(int fd, char *vptr, long cantBytes);
   long leeLong( int fd, long &numLeer);
   int  ConsultaIES();
   int  ServidorIES();
   int  RecibeCertificado();
   int  DatosCert();
   int  SeparaCad (char *, ...);
   int  Alertados(char * num_serie);
   int  ObtieneLlave( RSA * rLlave ,char *cLlavePub , int *iLlavePub );
   //>+ ERGL (070125)
   bool ObtieneDatosRevocacion(char *no_serie, char *rfc, char *tipo);
   //
   //>+ RORS  
   int DesconectaIES();
   void dumpBuffer(const char* texto, const uint8* buffer, int lbuf);


};
#endif
