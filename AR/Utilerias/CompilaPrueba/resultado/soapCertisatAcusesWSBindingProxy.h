/* soapCertisatAcusesWSBindingProxy.h
   Generated by gSOAP 2.7.13 from resultado/CertisatAcusesWS.h
   Copyright(C) 2000-2009, Robert van Engelen, Genivia Inc. All Rights Reserved.
   This part of the software is released under one of the following licenses:
   GPL, the gSOAP public license, or Genivia's license for commercial use.
*/

#ifndef soapCertisatAcusesWSBindingProxy_H
#define soapCertisatAcusesWSBindingProxy_H
#include "soapH.h"
class CertisatAcusesWSBinding
{   public:
	/// Runtime engine context allocated in constructor
	struct soap *soap;
	/// Endpoint URL of service 'CertisatAcusesWSBinding' (change as needed)
	const char *endpoint;
	/// Constructor allocates soap engine context, sets default endpoint URL, and sets namespace mapping table
	CertisatAcusesWSBinding()
	{ soap = soap_new(); endpoint = "http://10.0.2.100:8080/certisat-acuses-webservice/CertisatAcusesWS"; if (soap && !soap->namespaces) { static const struct Namespace namespaces[] = 
{
	{"SOAP-ENV", "http://schemas.xmlsoap.org/soap/envelope/", "http://www.w3.org/*/soap-envelope", NULL},
	{"SOAP-ENC", "http://schemas.xmlsoap.org/soap/encoding/", "http://www.w3.org/*/soap-encoding", NULL},
	{"xsi", "http://www.w3.org/2001/XMLSchema-instance", "http://www.w3.org/*/XMLSchema-instance", NULL},
	{"xsd", "http://www.w3.org/2001/XMLSchema", "http://www.w3.org/*/XMLSchema", NULL},
	{"ns1", "http://ws.certisat.sat.gob.mx/", NULL, NULL},
	{NULL, NULL, NULL, NULL}
};
	soap->namespaces = namespaces; } };
	/// Destructor frees deserialized data and soap engine context
	virtual ~CertisatAcusesWSBinding() { if (soap) { soap_destroy(soap); soap_end(soap); soap_free(soap); } };
	/// Invoke 'generaCodigoQR' of service 'CertisatAcusesWSBinding' and return error code (or SOAP_OK)
	virtual int __ns1__generaCodigoQR(ns1__generaCodigoQR *ns1__generaCodigoQR_, ns1__generaCodigoQRResponse *ns1__generaCodigoQRResponse_) { return soap ? soap_call___ns1__generaCodigoQR(soap, endpoint, NULL, ns1__generaCodigoQR_, ns1__generaCodigoQRResponse_) : SOAP_EOM; };
};
#endif
