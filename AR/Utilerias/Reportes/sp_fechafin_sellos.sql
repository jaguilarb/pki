-- Obtiene la fecha final de la operacion de un Sello digital en la tabla de operacion
CREATE PROCEDURE sp_fechafin_sellos(p_soloper_cve CHAR(12), p_oper_sec SMALLINT)
   RETURNING VARCHAR(22);  --DATETIME YEAR TO SECOND;
   DEFINE p_proc_cve  SMALLINT;
   DEFINE p_fec_op    DATETIME YEAR TO SECOND;
   DEFINE p_val_sello SMALLINT;
   DEFINE p_ret       VARCHAR(22);
   LET p_val_sello = 2; 
   LET p_fec_op    = NULL;
   -- Hacer un SELECT a lo mas sobre los primeros 12 registros
   FOREACH
      SELECT proc_cve, fec_op INTO p_proc_cve, p_fec_op
      FROM  operacion  
      WHERE soloper_cve = p_soloper_cve AND oper_sec > p_oper_sec AND oper_sec <= (p_oper_sec + 12)
      IF ( p_proc_cve = 117) THEN
         EXIT FOREACH;
      ELIF ( p_proc_cve = 90) THEN
         LET p_val_sello = 1;
         EXIT FOREACH;
      END IF;
   END FOREACH;
   IF ( p_fec_op IS NULL) THEN
      LET p_ret = "|"  || p_val_sello;
   ELSE
      LET p_ret = p_fec_op || "|"  || p_val_sello;  
   END IF;
   RETURN p_ret; --p_fec_op;
END PROCEDURE;
REVOKE EXECUTE ON PROCEDURE sp_fechafin_sellos FROM "public";
GRANT EXECUTE ON PROCEDURE sp_fechafin_sellos ( CHAR(12), SMALLINT ) TO "rl_arsat";

