/*##############################################################################
  ###  PROYECTO:              PKI-SAT                                        ###
  ###  MODULO:                Reportes de la PKI                             ###
  ###                                                                        ###
  ###  DESARROLLADORES:       Miguel Angel Mendoza L�pez      MAML           ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       Martes 8, Agosto del 2008                      ###
  ###                                                                        ###
  ##############################################################################
*/


/*##############################################################################
   VERSION:
      V.1.0.1      (20080805 - 20080815) MAML: 1a versi�n: 
                   Cron /script: genera reportes de la BD y 
                                 los envia via socket a un equipo    
#################################################################################*/

#ifndef _REPORTES_H_
   #define _REPORTES_H_
static const char* _REPORTES_H_VERSION_ ATR_USED = "Reportes @(#)"\
"DSIC08341AR_ 2008-12-02 Reportes.h 1.0.1/0";



//#VERSION: 1.0.1

#define  EXITO               0

//#define  ERR_FALTA_MEMORIA   100  // Ya definido
#define  ERR_ABRIR_DB        101
#define  ERR_DES_PWD_DB      102
#define  ERR_CONECTA_DB      103
#define  ERR_EJECUTA_QUERY   110 
#define  ERR_ACCESOREPPEND   115 
#define  ERR_MOVARCHIVOREP   116
#define  ERR_ARCHMISMARUTA   117
#define  ERR_PARAMETROS      120
#define  ERR_ARCHNOCERRO     125

#define  MAX_PATH            256
#define  MAX_TAM_QUERY       999    

//#############################################################################
class CReportes 
{
   CConfigFile *configuracion;
   CBD         *m_BD; 

   int         NivelBit;
   bool        SoloPendientes;
   std::string s_fecha_ini;
   std::string s_fecha_fin;
   std::string s_fecha_nom;
   int         idArch;
   int         error;
   uint32      no_rows; // Numero de rows del Reporte en question  (long = 4 bytes) 
   int         max_campo_bd;
   char        nom_reporte[MAX_PATH]; // tiene el nombre del archivo reporte sin el path
   std::string s_arch_conf;
   std::string s_separador;
   std::string s_ruta_local;

   std::string s_ruta_reporte;        // tiene el nombre del archivo reporte con el path 

   std::string s_path_dst;
   std::string s_host_dst;
   std::string s_user_dst;   
   std::string s_rutaPendientes;
   std::string s_rutaProcesados;

   bool creaArchReporte(const char *query);
   bool cargaConfiguracion();
   void creaNombre( const char *nombre );
   void creaNombreCifrasC();
   int  creaArch ();
   int  escribeArch (const char *dato, int len_dato );
   void cierraArch();
   int  creaArchCifrasC();
   int  conectaBD();
   int  revisaReportesPendientes();
   int  transmiteReporte(); 
   bool SetFecha(bool inicial);
   bool PreparaQuery(std::string& query);

 
   public:
      CBitacora   *Bitacora;   

      CReportes();
      ~CReportes();
      bool inicia();
      bool genera();
      bool procesaParametros(int args, char* const argv[]);
};

//#############################################################################

#endif //_REPORTES_H_

