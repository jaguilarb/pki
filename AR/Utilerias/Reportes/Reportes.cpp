static const char* _REPORTES_CPP_VERSION_ ATR_USED = "Reportes @(#)"\
"DSIC08341AR_ 2008-12-02 Reportes.cpp 1.0.2/0";

/*##############################################################################
  ###  PROYECTO:              PKI-SAT                                        ###
  ###  MODULO:                Reportes de la PKI                             ###
  ###                                                                        ###
  ###  DESARROLLADORES:       MAML                                           ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       Martes 8, Agosto del 2008                      ###
  ###                                                                        ###
  ##############################################################################
*/


/*##############################################################################
   VERSION:
      V.1.00      (20080805 - 20080815) MAML: 1a versi�n:
                  Cron o script 
      V.1.00.01   Genera reportes de una BD y los envia via socket a otro equipo
      V.1.00.02   Se extiende la funcionalidad para darle mas flexibilidad
#################################################################################*/


#define VERSION "V.1.00.02"

#include <unistd.h>
#include <string>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <dirent.h>

#include <Sgi_Bitacora.h>
#include <Sgi_ConfigFile.h>
#include <Sgi_ProtegePwd.h>
#include <Sgi_BD.h>
#include <SockSeg.h>
#include <iostream>

#include <Reportes.h>


//#################################################################################
#define PATH_CONF            "/usr/local/SAT/PKI/etc/"
#define PATH_DBIT            "/var/local/SAT/PKI/"

#ifdef DBG
   #define ARCH_CONF            PATH_CONF "Reportes_dbg.cfg"
   #define PATH_BITA            PATH_DBIT "Reportes_dbg.log"
#else
   #define ARCH_CONF            PATH_CONF "Reportes.cfg"
   #define PATH_BITA            PATH_DBIT "Reportes.log"
#endif

int main(int args, char * argv[])
{
   int res = -1;

   CReportes *reportes = new CReportes();
   if ( reportes == NULL )
   {
      printf("Error fatal de memoria(CReportes).\n");
      return 1;
   }
   if (reportes->procesaParametros(args, argv) &&
       reportes->inicia() && 
       reportes->genera() )
   {
       reportes->Bitacora->escribe(BIT_INFO, "Se han creado exitosamente los reportes de la PKI.");
       res = 0;
   }
   delete reportes;
   reportes = NULL;

   return res;
}


CReportes::CReportes() :
   configuracion(NULL),
   m_BD(NULL),
   NivelBit(-99),
   SoloPendientes(false),
   idArch(0),
   error(0),
   Bitacora(NULL)
{
   memset(nom_reporte, 0, MAX_PATH);

   s_fecha_ini = "AYER"; // MAML 081226: por default hara reportes diarios
   s_fecha_fin = "AYER";
   s_arch_conf = ARCH_CONF;
}

CReportes::~CReportes()
{
   if (m_BD)
   {
      delete m_BD;
      m_BD = NULL;
   }
   if (configuracion)
   {
      delete configuracion;
      configuracion = NULL;
   }
   if(Bitacora)
   {
      delete Bitacora;
      Bitacora = NULL;
   }
}

bool CReportes::inicia()
{
   Bitacora = new CBitacora(PATH_BITA);

   if ( !Bitacora )
   {
      error = ERR_FALTA_MEMORIA;
      printf("Error fatal de memoria(CBitacora).\n");
   }
   else 
   {
      if (NivelBit >= 0)
         Bitacora->setNivel((BIT_NIVEL) NivelBit);

      Bitacora->escribe(BIT_INFO, "Iniciando proceso");

      if ( cargaConfiguracion() )
      {
         if (SetFecha(true) &&
             SetFecha(false))
            error = conectaBD();
      }
   }
   return !error;
}

bool CReportes::SetFecha(bool inicial)
{
   bool ok = true;
   std::string& fecha = inicial ? s_fecha_ini : s_fecha_fin;
   
   if (!fecha.empty())
   {
      if (!fecha.compare("AYER"))
         fecha = inicial ? "(TODAY - 1)" : "TODAY";
      else if ((fecha.length() == 8) || (fecha.find_first_not_of("0123456789") == std::string::npos)) 
      {
          int nanio, nmes, ndia;
          s_fecha_nom = fecha;
          string anio = fecha.substr(0, 4);
          string mes  = fecha.substr(4, 2);
          string dia  = fecha.substr(6, 2);

         nanio = atoi(anio.c_str());
         nmes  = atoi(mes.c_str());
         ndia  = atoi(dia.c_str());

         if (nanio < 2000 || nanio > 2050 ||
             nmes  < 1    || nmes  > 12   ||
             ndia  < 1    || ndia  > 31     )
            ok = false;
         else
         {
            fecha = "DATE('" + anio + "-" + mes + "-" + dia + "')";
            if (!inicial)
               fecha = "(" + fecha + " + 1)"; 
         } 
      }
      else
         ok = false;
   }   

   return ok;   
}

bool CReportes::PreparaQuery(std::string& query)
{
   size_t pos;

   if (!s_fecha_ini.empty())
   {
      while ((pos = query.find("$(FEC_INI)")) != std::string::npos)
         query.replace(pos, 10, s_fecha_ini);
   }
   if (!s_fecha_fin.empty())
   {
      while ((pos = query.find("$(FEC_FIN)")) != std::string::npos)
         query.replace(pos, 10, s_fecha_fin);
   }

   Bitacora->escribe(BIT_DEBUG, query.c_str());
   return true;
}

// Ejecuta todos los querys q haya en el archivo de Configuraci�n
bool CReportes::genera()
{
   std::string num_query[2]; //char  num_query[2][20]; 
   bool  hay_query;
   int   i = 0;
   std::string  s_query, s_nombre;       // nombre del archivo reporte 
   char  buffer[10];

   revisaReportesPendientes(); // Antes de mandar los reportes actuales, revisa si hay pendientes

   if (!SoloPendientes)
   {
      do 
      {
         sprintf( buffer, "%d", ++i);
         num_query[0] = "REP_" + string( buffer );
         num_query[1] = "NOM_" + string( buffer );   //sprintf (num_query[1], "NOM_%02d", i);
		 
		 #if ORACLE
		    hay_query = (configuracion->getValoresConf("[REPORTES_ORA]", 2, num_query, &s_query, &s_nombre ) == 0);
		 #else
		    hay_query = (configuracion->getValoresConf("[REPORTES]", 2, num_query, &s_query, &s_nombre ) == 0);
		 #endif
         
		 
         if ( hay_query )
         {
            PreparaQuery(s_query); 
            creaNombre( s_nombre.c_str() );
            error = creaArch();
            if ( !error )
               creaArchReporte( s_query.c_str() ); 
            if ( !error )
            { 
               error = transmiteReporte();
               creaArchCifrasC();  // indepedientemente de si hay o no rows en el archivo-reporte, manda too su cc
            } 
         }
      } while ( !error && hay_query );
   }
   return ( !error );
}


// Hace un query a la DB y manda los datos a un archivo de texto
bool CReportes::creaArchReporte(const char *query)
{
   std::string valor;

   Bitacora->escribePV(BIT_DEBUG, "Inicia reporte %s", s_ruta_reporte.c_str());

   no_rows = 0L;
   if( m_BD->consulta( query ) )
   { 
      while( m_BD->sigReg() ) // Procesa row * row
      {
         for (int i = 0; !error && i < m_BD->getNumColumnas() ; i++) // Procesa columna * columna 
         {
            if ( m_BD->getColumna (i, valor) )
            {
               //if ( i < m_BD->NumColumnas-1 ) // No es la ultima columna le pega el pipe 
               valor += s_separador;
               error = escribeArch( valor.c_str(), valor.length() );   
            }
         }
         if ( !error ) 
         {
            error = escribeArch( "\n", 1 );  // acabo un renglon, escribe el salto de linea
            no_rows++; 
         } 
      }
      cierraArch();
   }
   else 
   {
      error = ERR_EJECUTA_QUERY; 
      Bitacora->escribePV(BIT_ERROR, "fallo la ejecuci�n del query: %s ", query );
   }

   Bitacora->escribe(BIT_DEBUG, "Fin reporte");

   return !error; 
}

bool CReportes::cargaConfiguracion()
{
   Bitacora->escribe(BIT_DEBUG, "Inicia carga configuraci�n");

   configuracion = new CConfigFile( s_arch_conf.c_str(), MAX_TAM_QUERY ); // MALM: maximo de caracteres por linea va a leer del arch de config.
   if( configuracion )
   {
      if( (error = configuracion->cargaCfgVars()) != 0 )
         Bitacora->escribePV(BIT_ERROR, "No se pudieron cargar las variables de configuracion(%d) del archivo(%s).", error, s_arch_conf.c_str() );
      else
      {
         std::string max_campo,
                     config[] = { "SEPARADOR", "MAX_CAMPO_DB", "RUTA_LOCAL" },
                     envio[]  = { "HOST_DST",  "USER_DST",     "PATH_DST",  "RUTA_PENDIENTES", "RUTA_PROCESADOS" };

         error = ( configuracion->getValoresConf("[CONFIG]", 3, config, &s_separador, &max_campo,  &s_ruta_local ) || 
                   configuracion->getValoresConf("[ENVIO]",  5, envio,  &s_host_dst,  &s_user_dst, &s_path_dst, &s_rutaPendientes, &s_rutaProcesados ) );
         if (!error)
            max_campo_bd = atoi( max_campo.c_str() );
         else
            Bitacora->escribePV(BIT_ERROR, "fallo la funci�n configuracion->getValoresConf(), error(%d) del archivo(%s).", error, s_arch_conf.c_str() );
      }
   }
   else
   {
      error = ERR_FALTA_MEMORIA;
      Bitacora->escribe(BIT_ERROR, "Error fatal de memoria(CConfigFile).");
   }
   
   Bitacora->escribe(BIT_DEBUG, "Fin carga configuraci�n");
   
   return !error;
}

// crea el nombre del archivo del reporte con su ruta 
void CReportes::creaNombre(const char *nombre)
{
   struct tm today;
   char   *pPos, c_aux[5];  

   sprintf( nom_reporte, "%s.txt", nombre );

   if (!s_fecha_nom.empty() && (pPos = strstr(nom_reporte, "AAAAMMDD")) != NULL)
      strncpy(pPos, s_fecha_nom.c_str(), 8);
   else
   {
      time_t ltime = time(NULL);
      memcpy(&today, localtime( &ltime ), sizeof(today));
      //sprintf(nom_reporte, "%s_%02d%02d%02d.txt", nombre, today.tm_mday, today.tm_mon + 1, today.tm_year - 100); // + 1900
      //>>> MAML 080904
      // Hay q buscar las cadenas AAAA, AA, MM, DD y cambiarlas
      if ((pPos = strstr(nom_reporte,"AAAA")) != NULL)
      {
         sprintf (c_aux, "%04d", today.tm_year + 1900);
         strncpy (pPos, c_aux, 4);
      }
      else if ((pPos = strstr(nom_reporte,"AA")) != NULL)
      {
         sprintf (c_aux, "%02d", today.tm_year - 100);
         strncpy (pPos, c_aux, 2);
      }
      if ((pPos = strstr(nom_reporte,"MM")) != NULL)
      {
         sprintf (c_aux, "%02d", today.tm_mon + 1);
         strncpy (pPos, c_aux, 2);
      }
      if ((pPos = strstr(nom_reporte,"DD")) != NULL)
      {
         sprintf (c_aux, "%02d", today.tm_mday );
         strncpy (pPos, c_aux, 2);
      }
   }
   //<<< MAML 080904: OK: nom_reporte ya esta correctamente transformado.
   s_ruta_reporte = s_ruta_local + "/" + nom_reporte; // Suponemos q el directorio donde se depositan los archivos SI existe.
}

// crea el nombre del archivo del reporte de cifra de control ( cc ) con su ruta
void CReportes::creaNombreCifrasC()
{
   char aux_nom_reporte[MAX_PATH];

   strcpy(aux_nom_reporte, nom_reporte);
   aux_nom_reporte[strlen(aux_nom_reporte) - 4] = 0;
   sprintf(nom_reporte, "%s.cc", aux_nom_reporte);
   s_ruta_reporte = s_ruta_local + "/" + nom_reporte; // Suponemos q el directorio donde se depositan los archivos SI existe.
}

// Crea el archivo de texto ( plano ) donde se van a guardar los datos de los Reportes
int CReportes::creaArch()
{
   if (idArch)
   {
      Bitacora->escribePV(BIT_ERROR, "Id. Archivo no cerrado(%d), no se creara el archivo %s", idArch, s_ruta_reporte.c_str() );
      return ERR_ARCHNOCERRO;
   }
   idArch  = open(s_ruta_reporte.c_str(), O_WRONLY|O_CREAT, S_IRWXU);
   if(idArch < 0)
   {
      Bitacora->escribePV(BIT_ERROR, "No se pudo crear el archivo %s, errno(%d).", s_ruta_reporte.c_str(), errno );
      return errno;
   }
   return EXITO;
}

// Escribe un dato del archivo
int CReportes::escribeArch(const char *dato, int len_dato )
{
   if (write( idArch, dato, len_dato ) < 0)
   {
      cierraArch();
      Bitacora->escribePV(BIT_ERROR, "No se pudo escribir el dato %s en el archivo %s, errno(%d).", dato, s_ruta_reporte.c_str(), errno);
      return errno;
   }
   return EXITO;
}

void CReportes::cierraArch()
{
   if (idArch)
   {
      close(idArch);
      idArch = 0;
      //memset(nom_reporte, 0, MAX_PATH);
   }
}

// crea archivo de cifras de control y lo envia via socket al equipo destino
int CReportes::creaArchCifrasC()
{
   // total de registros del archivo 'x'
   Bitacora->escribePV(BIT_DEBUG, "total de registros=%ld, del archivo de Reportes %s", no_rows, s_ruta_reporte.c_str() );
   creaNombreCifrasC();
   // Ahora crea y guarda el total de rows q se encontraron en el query en el archivo cc
   error = creaArch();
   if ( !error )
   {
      char buffer[12];
      sprintf( buffer, "%d", no_rows );
      error = escribeArch( buffer, strlen(buffer) );
   }
   cierraArch();
   if ( !error )
      error = transmiteReporte();
   return( !error );
}

/*-------------------------------------------------------------------------------------------
   PROPOSITO: Esta funcion establece la conexion con la BD.

   EFECTO:    No tiene efecto sobre el ambiente.
   SALIDAS:   Regresa: 0 -> Exito o un codigo de error.
  -------------------------------------------------------------------------------------------*/
int CReportes::conectaBD()
{
   Bitacora->escribe(BIT_DEBUG, "Inicia conectaBD");

   std::string s_srv, s_db, s_paswd, s_nombre, s_role, 
               claves[] = { "SERVIDOR", "BD", "PASSWORD", "USUARIO", "ROL" },
			   claves_ora[] = { "usr", "pwd", "host", "port", "sid" };

   if (m_BD)
   {
      Bitacora->escribePV(BIT_ERROR, "Error(%d) al abrir BD: ya se encuentra abierta la conexi�n a la BD", ERR_ABRIR_DB );
      return ERR_ABRIR_DB;
   }
      //GNC (28102014)
   #if ORACLE
    error = configuracion->getValoresConf("[BD_AR_ORA]", 5, claves_ora, &s_nombre, &s_paswd, &s_srv, &s_db, &s_role);
   #else
    error = configuracion->getValoresConf("[BD_AR]", 5, claves, &s_srv, &s_db, &s_paswd, &s_nombre, &s_role);
   #endif
  

   if ( error != 0 )
   {
      Bitacora->escribePV(BIT_ERROR, "Error(%d) al obtener valores de conexi�n de la BD en getValoresConf()", error );
      return error;
   }

   // OjO: por si se llega a encriptar el password y asi se pone en el archivo de Config.
   uint8 password[128];
   int i_password = sizeof(password);

   if (!desencripta((uint8*)s_paswd.c_str(), s_paswd.length(), password, &i_password)  )
   {
     Bitacora->escribePV(BIT_ERROR, "Error(%d) al desencriptar el password de conexi�n a la BD", ERR_DES_PWD_DB );
      return ERR_DES_PWD_DB;
   }
   password[i_password] = 0;
   //

   m_BD = new CBD(Bitacora);
   bool ok  = m_BD != NULL;  
      //GNC (28102014)
   #if ORACLE
     ok = ok && m_BD->setConfig(s_nombre.c_str(), (char *)password, s_srv.c_str(), s_db.c_str(), s_role.c_str());
   #else
     ok = ok && m_BD->setConfig(s_srv.c_str(), s_db.c_str(), s_nombre.c_str(), (char *)password, s_role.c_str());
   #endif
      
   ok = ok && m_BD->conecta();
   if (!ok)
   {
     Bitacora->escribePV(BIT_ERROR, "Error(%d) al conectarse a la BD", ERR_CONECTA_DB );
      return ERR_CONECTA_DB;
   }

   Bitacora->escribe(BIT_DEBUG, "Fin conectaBD");

   return EXITO;
}

// #####################################################################################################################
// 2 Funciones q ocupan al Socket de Seguridad: para enviar archivos de Reportes 

int CReportes::revisaReportesPendientes()
{
   DIR    *dp;
   struct dirent *ep;
   int    res = 0;

   Bitacora->escribe(BIT_DEBUG, "Inicia revisa pendientes");

   dp = opendir(s_rutaPendientes.c_str());
   if(dp != NULL)
   {
      while((ep = readdir(dp)))
      {
         if((!strcmp(ep->d_name,".")) || (!strcmp(ep->d_name,"..")))
            continue;
         s_ruta_reporte = s_rutaPendientes + "/" + ep->d_name;
         strcpy(nom_reporte, ep->d_name);
         res = transmiteReporte();

         if(res != 0)
            Bitacora->escribePV(BIT_ERROR, "No se env�o el reporte pendiente %s", s_ruta_reporte.c_str());
         else
            Bitacora->escribePV(BIT_INFO, "Se env�o el reporte pendiente: %s", s_ruta_reporte.c_str());
      }
      closedir(dp);
   }
   else
   {
      Bitacora->escribePV(BIT_ERROR, "Al abrir el repositorio %s", s_rutaPendientes.c_str());
      return ERR_ACCESOREPPEND;
   }

   Bitacora->escribe(BIT_DEBUG, "Fin revisa pendientes");

   return EXITO;
}

// Ahora Envia via socket de Seguridad: un archivo de datos (Reporte)
int CReportes::transmiteReporte() //char nom_reporte
{
   int         envio;
   char        mens_err  [1204];
   char        nombreReporteRemoto[MAX_PATH*2];
   std::string dirDest, archDest;

   sprintf(nombreReporteRemoto, "%s%s", s_path_dst.c_str(), nom_reporte); // OjO: s_path_dst debe terminar con el separador de la plataforma '\\' � '/'

   envio = TransmiteArchivo((char*)s_host_dst.c_str(),
           (char*)s_user_dst.c_str(), (char*)s_ruta_reporte.c_str(), nombreReporteRemoto, mens_err);
   if(envio != 0)
   {
      Bitacora->escribePV(BIT_ERROR, "Al enviar el archivo local(%s), error --> %s",
                                                         s_ruta_reporte.c_str(), mens_err);
      dirDest = s_rutaPendientes;
   }else
   {
      Bitacora->escribePV(BIT_INFO, "Se transmiti� el reporte (%s)", nom_reporte ) ;
      dirDest = s_rutaProcesados;
   }
   archDest = dirDest + "/" + std::string(nom_reporte);
   // Solo haz el rename/mueve si los nombres de archivo son diferentes, si son iguales no hagas nada.
   if ( !strcmp( s_ruta_reporte.c_str(), archDest.c_str() ) )
   {
      Bitacora->escribePV(BIT_DEBUG, "El archivo local(%s) no se ha movido, sigue en la misma ruta.", s_ruta_reporte.c_str() );
      return ERR_ARCHMISMARUTA;
   }
   if ( rename(s_ruta_reporte.c_str(), archDest.c_str()) < 0 )
   {
      Bitacora->escribePV(BIT_ERROR, "No se movio el archivo local (%s) al directorio (%s)", s_ruta_reporte.c_str(), dirDest.c_str() );
      return ERR_MOVARCHIVOREP;
   }
   else
   {
      Bitacora->escribePV(BIT_DEBUG, "Se movi� el archivo (%s) al directorio (%s)", s_ruta_reporte.c_str(), dirDest.c_str() );
      return EXITO;
   }
}

// Se puede correr como cron de diferentes maneras usando un archivo de configuracion con diferentes querys.
bool CReportes::procesaParametros(int argc, char* const argv[])
{
   bool ok = true;
   int opcion;

   opterr = 0; // para que getopt no mande el mensaje de error al stderr
   while (ok && (opcion = getopt(argc, argv, "c:i:f:n:pv")) != -1)
   {
      switch (opcion)
      {
         case 'c':   // Archivo de configuraci�n
                     s_arch_conf = optarg;
                     break;
         case 'i':   // Fecha inicial
                     s_fecha_ini = optarg;
                     break;
         case 'f':   // Fecha fin
                     s_fecha_fin = optarg;
                     break;
         case 'n':   // Nivel de bitacora
                     NivelBit = atoi(optarg);
                     if (NivelBit <= 0)
                        ok = false;
                     break;
         case 'p':   // S�lo pendientes
                     SoloPendientes = true;
                     break;
         case 'v':   // Versi�n
                     std::cout << "Reportes " << VERSION << std::endl;
                     return false;
         default :   // Opci�n no reconocida
                     ok = false;
      }
   }
   if (!ok)
   {
      std::cout << "Uso: " << argv[0] << " [opciones]\n" << std::endl;
      std::cout << "\t-c arch_config    -- indica el archivo de configuraci�n para el proceso" << std::endl;
      std::cout << "\t-i fec_ini        -- fecha inicial del intervalo del reporte" << std::endl;
      std::cout << "\t                        YYYYMMDD" << std::endl;
      std::cout << "\t-f fec_fin        -- fecha final del intervalo del reporte" << std::endl;
      std::cout << "\t-n nivel_bitacora -- indica el nivel de mensajes en la bitacora" << std::endl;
      std::cout << "\t   2                 alertas" << std::endl;
      std::cout << "\t   3                 debug"   << std::endl;
      std::cout << "\t-p                -- s�lo transmite los archivos pendientes" << std::endl;
      std::cout << "\t-v                -- proporciona la versi�n de la aplicaci�n" << std::endl;
      std::cout << "\n" << std::endl;
   }

   return ok;
}

// #####################################################################################################################
