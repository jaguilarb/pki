#!/bin/bash
#**********************************************************************************************************************
#  SCRIPT GENERADO PARA LA AUTOMATIZACION DE LA GENERACION DE LA BD DE LA AR
#  Desarrollador:  Gudelia Hern�ndez Molina   GHM (070720 inicio)
#  Objetivo:       
#                  
#  Requisitos:     
#**********************************************************************************************************************

#-- Inicio del proceso
echo "*** Obtiene datos del WS_IDC" $1                          >> $7
if ! ./ObtDatosWS_IDC "$1" > Datos.dat 2>>$7 ; then
   echo "ERROR: fall� ./ObtDatosWS_IDC con " $1                 >> $7
   echo "para mayor informaci�n consulte ./ObtDatosWS_IDC.log " >> $7
   echo ""                                                      >> $7
   exit
fi

echo "*** Extrae el RFC_ORIGINAL DEL WS "                >> $7
if ! DATO=`grep "RFC_ORIGINAL" Datos.dat | cut -d '=' -f2 | sed 's/ //g'` >> $7 2>>$7 ; then
   echo "ERROR: fall� la extracci�n del RFC: " $1        >> $7
   echo ""                                               >> $7
else 
   echo $DATO"|"$3"|"$4"|"$5"|"$6"|"                     >> $8
   echo $DATO"|"$2"|"$3"|"$4"|"$5"|"$6"|"                >> $9
   echo ""                                               >> $7
fi

