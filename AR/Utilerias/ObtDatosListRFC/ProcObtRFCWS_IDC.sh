#!/bin/bash

#-- Declaraci�n de bit�cora
LOG="./BitPrObtRFCWS_IDC.log"

echo "******************************************" >> $LOG
echo "*****- INICIO DE PROCESO -*****"            >> $LOG
date >> $LOG
echo ""                                           >> $LOG
 
#if ! ex -c "% s/,/|/g" -c "wq" RFCs_Bloqueados.dat 3>> $LOG; then
#   echo "ERROR: No se transformo la , por | en el archivo de entrada " >> $LOG
#   exit
#fi

rm    ListaCmd.sh     >>$LOG 2>>$LOG
touch ListaCmd.sh     >>$LOG 2>>$LOG
chmod 750 ListaCmd.sh >>$LOG 2>>$LOG

rm    bloqueado.dat     >> $LOG 2>>$LOG
touch bloqueado.dat     >> $LOG 2>>$LOG
chmod 640 bloqueado.dat >> $LOG 2>>$LOG

rm    bit_bloqueado.dat     >> $LOG 2>>$LOG
touch bit_bloqueado.dat     >> $LOG 2>>$LOG
chmod 640 bloqueado.dat >> $LOG 2>>$LOG

awk 'BEGIN { FS="|" } { printf("./buscaRFC.sh %s %s %s \"%s\" %s \"%s\"  \"./BitPrObtRFCWS_IDC.log\" \"./bloqueado.dat\" \"./bit_bloqueado.dat\" \n", $2, $3, $4, $5, $6, $7) }' \
    RFCs_Bloqueados.dat >> ListaCmd.sh 2>> $LOG

./ListaCmd.sh 2>>$LOG

rm    Datos.dat >> $LOG 2>>$LOG

date >> $LOG
echo "******************************************" >> $LOG
echo ""                                           >> $LOG
