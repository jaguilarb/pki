static const char* _FTPARA_CPP_VERSION_ ATR_USED = "FTP @(#)"\
"DSIC07412AR_ 2007-12-14 FTPARA.cpp 1.1.0/0";

//#VERSION: 1.1.0

#include <FTP.h>
#include <string>

#ifdef DBG
   #define ARCH_CONF            PATH_CONF "FTPARA_%02d_dbg.cfg"
   #define PATH_BITA            PATH_DBIT "FTPARA_%02d_dbg.log"
#else
   #define ARCH_CONF            PATH_CONF "FTPARA_%02d.cfg"
   #define PATH_BITA            PATH_DBIT "FTPARA_%02d.log"
#endif

template<typename T>
int getOS(std::basic_string<T> &ruta){
   if (ruta.find_first_of("/") != std::basic_string<T>::npos){
      return LINUX;
   }
   else if (ruta.find_first_of("\\") != std::basic_string<T>::npos){
      return WINDOWS;
   }
   else{
      return LINUX;
   }
}

int main ( int iArg, char * argv[])
{
    int error;
    char aConf[256];
    char dir[256];
    FTP  ftp;

    if (argv[1] == NULL){
      printf("Uso: N�meroDeARA \n");
      printf("N�meroDeARA: N�mero de la ARA.\n");
      exit(0);
    }
    /*
    if(argv[1] == NULL)
    {
       printf("Uso : N�mero de ARA\n");
       return 0;   
    }*/
    
    
    memset(aConf,0,sizeof(aConf));
    memset(dir,0,sizeof(dir));

    ftp.NombreConfg(aConf, atoi(argv[1]), ARCH_CONF, PATH_BITA );
    
    error =  ftp.Inicia(aConf);
    if(!error)
    {
       std::string s_ruta = ftp.getRutaDestino();
       int varSO = getOS(s_ruta);
       memcpy(dir,ftp.NO_ENV,strlen(ftp.NO_ENV));
       ftp.Ftp(dir, varSO );
    }
}

