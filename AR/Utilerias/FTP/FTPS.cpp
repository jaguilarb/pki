static const char* _FTPS_CPP_VERSION_ ATR_USED = "FTP @(#)"\
"DSIC07412AR_ 2007-12-14 FTPS.cpp 1.1.0/0";

//#VERSION: 1.1.0

#include <FTP.h>
#include <string>

#ifdef DBG
   #define ARCH_CONF            PATH_CONF "FTP_%02d_dbg.cfg"
   #define PATH_BITA            PATH_DBIT "FTP_%02d_dbg.log"
#else
   #define ARCH_CONF            PATH_CONF "FTP_%02d.cfg"
   #define PATH_BITA            PATH_DBIT "FTP_%02d.log"
#endif

template<typename T>
int getSO(std::basic_string<T> &ruta){
   if (ruta.find_first_of("/") != std::basic_string<T>::npos){
      return LINUX;
   }
   else if (ruta.find_first_of("\\") != std::basic_string<T>::npos){
      return WINDOWS;
   }
   else {
      return LINUX;
   }
}

int main (int argc, char* argv[], char* env[])
{
    int error;
    char aConf[256];
    char dir[256];
    FTP  ftp;

    memset(aConf,0,sizeof(aConf));
    memset(dir,0,sizeof(dir));

    if( argv[1] == NULL)
    {
       printf("Uso : Poner numero de FTP\n");
       printf("N�mero FTP: El n�mero de FTP al que se va a eviar \n");
       exit(0);
    }
   
    ftp.NombreConfg(aConf, atoi(argv[1]), ARCH_CONF, PATH_BITA );
    error =  ftp.Inicia(aConf);
    if(!error)
    {
       std::string s_ruta = ftp.getRutaDestino();
       int varSO = getSO(s_ruta);
       ftp.NombreRuta (ftp.NO_ENV,dir,atoi(argv[1]) );

       memcpy(dir,ftp.NO_ENV,strlen(ftp.NO_ENV));
       ftp.Ftp(dir, varSO);
    }
}

