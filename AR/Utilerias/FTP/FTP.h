#ifndef _FTP_H_
#define _FTP_H_
static const char* _FTP_H_VERSION_ ATR_USED = "FTP @(#)"\
"DSIC07412AR_ 2007-12-14 FTP.h 1.1.0/0";

//#VERSION: 1.1.0
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/uio.h>
#include <strings.h>
#include <string>
#include <unistd.h>
#include <iostream>
#include <dirent.h>
//>- ERGL mi� mar 21 14:29:29 CDT 2007
//#include <RutaCerts.h>
//#include <ArchivosLog.h>
#include <Sgi_Bitacora.h>
#include <SockSeg.h>
#include <Sgi_PKI.h>

#define PATH_CONF               "/usr/local/SAT/PKI/etc/"
#define PATH_DBIT               "/var/local/SAT/PKI/"

class FTP
{

   public :
   //>>> ERGL jue mar 22 10:22:40 CDT 2007 char DIR_REP   [256];
   std::string s_dir_rep;
   std::string s_dir_des;
   CBitacora   *log;
   char IP_LOCAL  [15];
   char IP_DES    [15];
   char USU_LOCAL [10];
   char USU_REM   [10];
   char DIR_DES   [256];
   char NO_ENV    [256];
   char NUM_FTP   [2];
   char mens_err  [1204];
   char aLOG      [256];
   //<<<

   FTP(){}
   
   void NombreConfg(char *ArchConf,int numftp_ara, char *FmtoCfg, char *FmtoLog);
   void NombreRuta(char* Rutaini,char* RutaFin,int numftp);
   int Inicia(char *archconf);
   int Ftp(char * dir,int tipRepRem);
   std::string getRutaDestino();

};

#endif
