static const char* _FTP_CPP_VERSION_ ATR_USED = "FTP @(#)"\
"DSIC07412AR_ 2007-12-14 FTP.cpp 1.1.1/0";

/* ################################################################################################################# 
   ################################################################################################################# 

//#VERSION: 1.1.1
   V.1.1.1.1 - 080429 HOA: Se cambia el comando para crear los directorios para ajustar los permisos en linux
   V.1.1.1.2 - 080429 SERR: Se cambia la forma de borrar la marca del repositorio antes rm (system) y ahora con unlink.

   ################################################################################################################# 
   ################################################################################################################# */

//- #include <CConfigFile.h> //- GHM (070430)
#include <Sgi_ConfigFile.h>  //+ GHM (070430)
#include <FTP.h>

void FTP::NombreConfg(char *ArchConf,int numftp_ara, char *FmtoCfg, char *FmtoLog)
{
   sprintf(ArchConf, (const char*)FmtoCfg, numftp_ara );
   sprintf(aLOG, (const char*)FmtoLog, numftp_ara );
}


void FTP::NombreRuta(char* Rutaini,char* RutaFin,int numftp)
{
   //>- ERGL mi� mar 21 14:30:50 CDT 2007
   //ArchivosConfg cfg;
   memcpy(RutaFin,Rutaini,strlen(Rutaini));
   sprintf(RutaFin + strlen(RutaFin),"%02d/",numftp);
}

std::string FTP::getRutaDestino(){
   return s_dir_des;
}


int FTP::Inicia(char *archconf)
{
   int error;
   //>>> ERGL mi� mar 21 14:50:53 CDT 2007
   //>-ArchivosConfg cfg;
   CConfigFile *cfg = new CConfigFile(archconf);
   memset(NO_ENV   ,0,sizeof(NO_ENV));
   memset(IP_LOCAL ,0,sizeof(IP_LOCAL));
   memset(USU_LOCAL,0,sizeof(USU_LOCAL));
   //>- ERGL jue mar 22 10:21:23 CDT 2007 memset(DIR_REP  ,0,sizeof(DIR_REP));
   memset(IP_DES   ,0,sizeof(IP_DES));
   memset(USU_REM  ,0,sizeof(USU_REM));
   //memset(DIR_DES  ,0,sizeof(DIR_DES));

   //>-char* variables[]={"IP_LOCAL","USU_LOCAL","DIR_REP","IP_DES","USU_REM","DIR_DES","NO_ENV"};

   log = new CBitacora(aLOG);

   //>+ ERGL
   error = cfg->cargaCfgVars();
   std::string s_var;
   cfg->getValorVar("[GENERAL]", "IP_LOCAL", 	&s_var);
   strcpy(IP_LOCAL, s_var.c_str());
   cfg->getValorVar("[GENERAL]", "USU_LOCAL", 	&s_var);
   strcpy(USU_LOCAL, s_var.c_str());
   cfg->getValorVar("[GENERAL]", "DIR_REP", 	&s_dir_rep);
   cfg->getValorVar("[GENERAL]", "IP_DES", 	&s_var);
   strcpy(IP_DES, s_var.c_str());
   cfg->getValorVar("[GENERAL]", "USU_REM", 	&s_var);
   strcpy(USU_REM, s_var.c_str());
   cfg->getValorVar("[GENERAL]", "DIR_DES", 	&s_dir_des);
   //strcpy(DIR_DES, s_var.c_str());
   cfg->getValorVar("[GENERAL]", "NO_ENV", 	&s_var);
   strcpy(NO_ENV, s_var.c_str());
 
   /*>-error =  cfg.VarConfg(8,variables,archconf,
                                    IP_LOCAL,USU_LOCAL,DIR_REP,
                                    IP_DES,USU_REM,DIR_DES,NO_ENV
                                    );
   -<*/
   if ( error )
      log->escribePV(BIT_ERROR, "No Iniciado, error=%d", error);   

   return error;
}


int FTP::Ftp(char * dir,int tipRepRem)
{
   int error;

   //>>> ERGL jue mar 22 10:30:03 CDT 2007
   std::string s_num_serie;
   std::string s_ruta_local;
   std::string s_ruta_ftp;
   char comando[1024];
   char aviso[1024];
   //char num_serie[20];
   //char ruta_local[TAM_RUTAS];
   //char ruta_ftp[TAM_RUTAS];
   //char comando[TAM_RUTAS];
   //char aviso[TAM_RUTAS];
   //<<< ERGL jue mar 22 10:30:03 CDT 2007

   /*>- ERGL jue mar 22 10:13:37 CDT 2007
   RutaCerts rtcerts;
   -<*/
    

   DIR    *direc;
   struct dirent *lista_nom;

   direc = opendir(dir);
   if(direc == NULL)
   {
      log->escribePV(BIT_ERROR,"Error al abrir el Directorio %d",error); 
      log->escribe(BIT_ERROR,dir);
      return -1;
   }
#ifdef DBG
   log->escribePV( BIT_INFO,"Inicio de Directorio %s", dir);
#endif

   while((lista_nom = readdir(direc))!= NULL )
   {
      memset(comando,0,sizeof(comando));
      //>- ERGL jue mar 22 10:47:19 CDT 2007 memset(ruta_local,0,sizeof(ruta_local));
      //>- ERGL jue mar 22 10:47:19 CDT 2007 memset(ruta_ftp,0,sizeof(ruta_ftp));
      memset(aviso,0,sizeof(aviso)); 

      error = strcmp((lista_nom[0].d_name),"." );
      if(!error)
         continue;
      error = strcmp((lista_nom[0].d_name),".." );
      if(!error)
         continue;

      if(strlen(lista_nom[0].d_name) != 20 )
         continue;

      //>>> ERGL jue mar 22 10:11:56 CDT 2007
      //memcpy(num_serie,lista_nom[0].d_name,20);
      s_num_serie = lista_nom[0].d_name;
      //error = rtcerts.RutaDir(DIR_REP,num_serie,ruta_local,false,1);   // Ruta repositorio local
      //error = rtcerts.RutaDir(DIR_DES,num_serie,ruta_ftp,false,tipRepRem);   // Ruta repositorio FTP
      
      if ((error = pkiDirCert(s_dir_rep, s_num_serie, s_ruta_local, false)) != 0  )       
         log->escribePV(BIT_ERROR,"No puede construirse la ruta del repositorio local %d", error );
      else if ((error = pkiDirCert(s_dir_des, s_num_serie, s_ruta_ftp, false, tipRepRem)) != 0 )  
         log->escribePV(BIT_ERROR,"No puede construirse la ruta del repositorio FTP   %d", error );
      if (error)
         break;

      if(tipRepRem == WINDOWS)
         //>- ERGL jue mar 22 11:39:56 CDT 2007 sprintf(comando,"mkdir  %s ",ruta_ftp);
         sprintf(comando,"mkdir  %s ",s_ruta_ftp.c_str());
      else
         //>>> V.1.1.1.1 (080429) HOA: cambio para ajustar permisos
         //>-   //>- ERGL jue mar 22 11:39:56 CDT 2007 sprintf(comando,"mkdir -p -m 775  %s ",ruta_ftp);
         //>-   sprintf(comando,"umask 002; mkdir -p -m 775  %s ",s_ruta_ftp.c_str());
         sprintf(comando,"if test ! -d %s; then umask 022; mkdir -p -m 755 %s; fi", s_ruta_ftp.c_str(), s_ruta_ftp.c_str());
         //<<< V.1.1.1.1 (080429) HOA: cambio para ajustar permisos

      //memcpy(ruta_local + strlen(ruta_local),num_serie,20); 
      //strcat(ruta_local ,".cer");
   
      //memcpy(ruta_ftp + strlen(ruta_ftp),num_serie,20);
      //strcat(ruta_ftp ,".cer");
      
      s_ruta_local+= s_num_serie + ".cer";
      s_ruta_ftp  += s_num_serie + ".cer";
 
      //error =  access(ruta_local,R_OK);
      error = access(s_ruta_local.c_str(), R_OK);
      if(error)
      {
         log->escribePV(BIT_ERROR,"No existe Certificado %d",error );
         continue;
      }

      error = EjecutaCmdoRem( IP_DES,USU_REM,comando,mens_err  );
      if(error)
      {
         if(error != -41)
         {
            log->escribe(BIT_ERROR,mens_err);
            break; // MAML 070621  //  return error;
         }
      }
 
      
      //>- ERGL jue mar 22 10:49:57 CDT 2007 error =  TransmiteArchivo( IP_DES,USU_REM,ruta_local,ruta_ftp,mens_err);
      char *c_ruta_local = (char *)s_ruta_local.c_str();
      char *c_ruta_ftp = (char *)s_ruta_ftp.c_str();
      error = TransmiteArchivo( IP_DES, USU_REM, c_ruta_local, c_ruta_ftp, mens_err ); 
      if(error)
      {
         log->escribe(BIT_ERROR,mens_err);
         break;  // MAML 070621  // return error;
      }
     
      //>>> V.1.1.1.1 (080429) HOA: cambio para ajustar permisos
      if (tipRepRem != WINDOWS)
      {
         sprintf(comando, "chmod 644 %s", s_ruta_ftp.c_str());
         error = EjecutaCmdoRem( IP_DES, USU_REM, comando, mens_err);
         if (error)
            log->escribePV(BIT_ERROR, "No se pudo cambiar los permisos al certificado en la ARA (%d): %s",
                                error, mens_err);
      }
      //<<< V.1.1.1.1 (080429) HOA: cambio para ajustar permisos
 
      memset(comando,0,sizeof(comando));

      //>>> V.1.1.1.2 (080429) SERR: Solo se armara la ruta del archivo que se borrara (.../DISTRIBUCION/ARAs/FTP/..).
      //>-memcpy(comando,"rm ",3);
      //>-memcpy(comando +3 ,dir,strlen(dir));
      //>-strcpy(comando +3  +strlen(dir),lista_nom[0].d_name);
      //>-system(comando);
      sprintf(comando , "%s%s", dir,lista_nom[0].d_name );
      unlink(comando);
      //<<< V.1.1.1.2 (080429) SERR:
      
      sprintf(aviso,"Enviado %s",lista_nom[0].d_name);
      log->escribe(BIT_INFO,aviso);

   } // END DEL WHILE
   if (error)
      log->escribePV( BIT_ERROR, "Fin de Directorio, error=%d", error);
   //log->escribe(BIT_INFO,dir);

   closedir(direc);
   direc = NULL;
   lista_nom = NULL;
   //delete lista_nom; // MAML 070621: El delete NO VA, por que nunca se le hizo new
 
   return error;
}

