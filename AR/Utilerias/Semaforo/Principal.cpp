static const char* _PRINCIPAL_CPP_VERSION_ ATR_USED = "Semaforo @(#)"\
"DSIC07412AR_ 2007-12-14 Principal.cpp 1.1.0/0";

//#VERSION: 1.1.0
#include<Semaforo.h>

int main(int argc, char* argv[], char* env[] )
{
   Semaforo sem;
   int sema,op,res;
   
   if(argv[1] == NULL)
   {
      printf("Uso: [ 1 : block | 2 : unblock ] \n");
      return 0;
   } 
   op = atoi(argv[1]);

   if( op != 1 )
   {  
     if( op != 2)
     {
         printf("Uso: [ 1 : block | 2 : unblock ] \n"); 
         return 0;
     }
   }
    sema = sem.obtieneID( SERV_FIFO);
//   sema = sem.obtieneID(ARA_FIFO);
   if(sema < 0)
   {
     perror("Error ");
     printf("FIFO :%s\n",SERV_FIFO);  	   
     exit(errno);	   
   }

   switch(op)
   { 
      case 1 :{
                 res = sem.bloqueaSem();
                 if(res)
                    return 0;
                 printf("Semaphore blocked %d\n",sema);
                 break; 
              }   
     case 2 : {
                 res = sem.desbloqueaSem();
                 if(res)
                    return 0;
                 printf("Semaphore unblocked %d\n",sema);
                 break; 
              }
   };

}

