static const char* _SEMAFORO_CPP_VERSION_ ATR_USED = "Semaforo @(#)"\
"DSIC07412AR_ 2007-12-14 Semaforo.cpp 1.1.0/0";

//#VERSION: 1.1.0
#include<Semaforo.h>


int Semaforo::
obtieneID(char * fifo)
{
   key_t llave;
   semID = SIN_IDEN;

   llave = ftok(fifo,2);

   semID = semget(llave,1,0660);
   if(semID < 0)
      return semID;
   return semID;
}

int Semaforo::
bloqueaSem()
{
   struct sembuf oper;

   if(  semID == SIN_IDEN )
      return SIN_IDEN;
   
   oper.sem_num = 0 ;
   oper.sem_op  = -1 ;
   oper.sem_flg = IPC_NOWAIT;

   if( (  semop(semID,&oper,1 ) )< 0 ) 
   {
      perror("Falla en Semop");
       return YA_BLOQUEADO;
   }
   return 0;


}

int Semaforo::
desbloqueaSem()
{
   struct sembuf oper;

   if(  semID == SIN_IDEN )
      return SIN_IDEN;

   oper.sem_num = 0 ;
   oper.sem_op  = 1 ;
   oper.sem_flg = IPC_NOWAIT;

   if( (  semop(semID,&oper,1 ) )< 0 )
   {
      perror("Falla en Semop");
       return -1;
   }
   return 0;

}
