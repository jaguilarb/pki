#ifndef _SEMAFORO_H_
#define _SEMAFORO_H_
static const char* _SEMAFORO_H_VERSION_ ATR_USED = "Semaforo @(#)"\
"DSIC07412AR_ 2007-12-14 Semaforo.h 1.1.0/0";

//#VERSION: 1.1.0
#include <sys/sem.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#ifdef _DBG_
  #define SERV_FIFO  "/tmp/fifod.serv"
  #define ARA_FIFO   "/tmp/fifoMedARA_DBG"
#else 
  #define SERV_FIFO  "/tmp/fifo.serv"
  #define ARA_FIFO   "/tmp/fifoMedARA"
#endif


#define SIN_IDEN   1000
#define YA_BLOQUEADO -1979


class Semaforo
{
   int   semID;

   public:

   Semaforo(){};
   int obtieneID(char * fifo);
   int bloqueaSem();
   int desbloqueaSem();
   
};
#endif
