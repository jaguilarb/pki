#include<Sgi_LDAP.h>
#include<Sgi_Bitacora.h>
#include<Sgi_ConfigFile.h>
#include<Sgi_ProtegePwd.h>




void Actualizar(Sgi_LDAP &ldap, char* cRFC, char* cTiene)
{
   const char *bio[] = { "SATMarcaBiometricos",NULL }; 
   char *cBio[1];
 
   if ( ldap.consulta( cRFC, bio,  cBio ))
   {
      if (ldap.actualizacion(cRFC,"SATMarcaBiometricos",cTiene))
      {
         if ( ldap.consulta( cRFC, bio,  cBio ))
         {
            for(int i = 0;i<1 ;i++)
            {
               printf(" ======  Datos Obtenidos ========");
               printf("\n ======  RFC : %s        ========",cRFC);
               printf("\n ======  Marca Biometricos : %s \n", cBio[i]);
               free( cBio[i]);
               cBio[i] = NULL;
            }

         }
       }
   }
   else 
   {
      printf("=======================================\n");
      printf("Error al actualizar la MarcaBiometricos.\nRevisar si el RFC dado esta dado de alta en el directorio.\nVer bitacora /var/local/SAT/PKI/MarcaBiometricos.log\n");
      printf("=======================================\n");
  
   }

}


int main(int argc, char **argv)
{
   std::string sBitacora      = "/var/local/SAT/PKI/MarcaBiometricos.log";
   std::string sConfiguracion = "/usr/local/SAT/PKI/etc/MarcaBiometricos.cfg";
   std::string sClaves [] = { "IP","PUERTO","USUARIO_PROXY","PASSWORD_PROXY","RUTA_BASE"};
   
   std::string sIPLDAP, sPuertoLDAP, sUsuarioLDAP, sPassAux,sRutaBaseLDAP,sPasswordLDAP;

   CConfigFile  archConf(sConfiguracion.c_str());
   CBitacora * cBit = new CBitacora(sBitacora);
   
   char cPwdDes[100];
   int  iError = -1; 
   int  iPwdDes = sizeof(cPwdDes);


   if ( argv[1] == NULL  || argv[2] == NULL)
   {
      printf("Uso : RFC Y/N\n");
      return 0;
   }
   if ( memcmp( argv[2],"Y",1) && memcmp( argv[2],"N",1) )
   {
      printf("En el segundo parametro debe ser Y o N para indicar si el RFC tendra o no biometricos.\n");
      return 0;
   }

   int iRFC = strlen(argv[1]);
   if ( (iRFC != 12) && (iRFC != 13)  )
   {
      printf("Escribe el RFC correctamente.\n");
      return 0;
   }
   

   iError = archConf.cargaCfgVars();
   if(iError == -1)
   {
    cBit->escribePV(BIT_ERROR,"No se cargaron las variables del archivo de configuracion.\nVerificar [%s] que el archivo exista.\n",sConfiguracion.c_str());
      return 0;
   }

   iError = archConf.getValoresConf("[CONEXION_LDAP]", 5, sClaves, &sIPLDAP, &sPuertoLDAP, &sUsuarioLDAP, &sPassAux,&sRutaBaseLDAP);
   if(iError)
   {
      cBit->escribe(BIT_ERROR,
           "Error al obtener los datos del archivo de configuracion.\nVerificar que el archivo exista y que tenga las  etiquetas y permisos adecuados.\n");
      return 0;
   }
   
   Sgi_LDAP ldap(*cBit, sRutaBaseLDAP.c_str()); 
 
   desencripta((uint8*)sPassAux.c_str(),sPassAux.length(),(uint8*)cPwdDes,&iPwdDes);
   cPwdDes[iPwdDes] = 0;
   sPasswordLDAP = cPwdDes;


   if ( ldap.Conexion( sIPLDAP.c_str() ,atoi(sPuertoLDAP.c_str()), sUsuarioLDAP.c_str(), sPasswordLDAP.c_str() ))
   {
      Actualizar(ldap, argv[1], argv[2]);
      ldap.Desconexion();
   }
   
   delete  cBit;
   cBit = NULL;


}   
