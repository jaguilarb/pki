static const char* _CARMALLAVE_CPP_VERSION_ ATR_USED = "@(#) Shk2Pkcs8 ( L : DSIC10441AR_ : CArmaLlave.cpp : 1.0.0 : 0 : 08/10/10)";

/*##############################################################################
  ###  DESARROLLADORES:                                                      ###
  ###                         Gudelia Hern�ndez Molina     GHM               ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       octubre 2010                                   ###
  ###                                                                        ###
  ##############################################################################
*/
/*#################################################################################
   VERSION:
      V.1.00      (20101008 - 20101122) GHM: Versi�n original

   CAMBIOS:

#################################################################################*/

#define _CARMALLAVE_CPP_
#include "CArmaLlave.h"

//Constructor
CArmaLlave::CArmaLlave(bool activar)
{
   objValida = new CValCadena();
   activarCod = activar;
   inumKeys  = 0;
   strError  = "";
}

//Destructor
CArmaLlave::~CArmaLlave()
{
   if (objValida)
   {
      delete objValida;
      objValida = NULL;
   }
}

//Proporciona el mensaje de error
std::string CArmaLlave::getError()
{
   return strError;
}

//#################################################################################
int CArmaLlave::getTamSegmento(unsigned char *uSegmento, int iTam)
{
   int segmento = -1;

   if( uSegmento[0] == 0x30 )
   {
      if( uSegmento[1] == 0x82 )
         segmento = ((uint16(uSegmento[2]) << 8) | uint16(uSegmento[3]));
      else
         segmento = iTam -2;
   }
   return segmento;
}

int CArmaLlave::getTamSegAnt( unsigned char *uSegmento, uint8 *cars_lng, uint16 *lng_datos )
{
   if (uSegmento[0] == 0x30)
   {
      if ( (uSegmento[1] < 0x80) )
      {
         *cars_lng  = 1;
         *lng_datos = uSegmento[1];
      }
      else if (uSegmento[1] == 0x81)
      {
         *cars_lng  = 2;
         *lng_datos = uSegmento[2];
      }
      else if (uSegmento[1] == 0x82)
      {
         *cars_lng  = 3;
         *lng_datos = (uint16(uSegmento[2]) << 8) | uint16(uSegmento[3]);
      }else
      {
         strError = "El segmento no corresponde al Formato Anterior o el password no es el correcto. ";
         return ERR_FTO_ANTERIOR;
      }
   }else
   {
      strError = "El segmento no tiene el Formato Anterior o el password no es el correcto. ";
      return ERR_FTO_ANTERIOR;
   }
   return EXITO;
}

//#################################################################################
bool CArmaLlave::armaKeyAnt(std::string prarchKey, char *pwd, unsigned char **uSegmentos, int iTamSeg ,int iNumSeg)
{
   int            ierror, lpwd = 0;
   unsigned char  pwdEspecial[MAX_TAM_PWDESPECIAL];
   SGIRSA         objRSA;
   RSA           *rsaLlavePriv = NULL;
   unsigned char *cLlavePriv = NULL;
   unsigned char *cLlaveObt  = NULL;
   
   memset(pwdEspecial,0,MAX_TAM_PWDESPECIAL);
   cLlavePriv = new unsigned char[iTamSeg];
   CSecretoCompartido *sc = new CSecretoCompartido(iNumSeg, iNumSeg);
   if ( sc->decodifica(uSegmentos, iTamSeg, (uint8*) cLlavePriv) )
   {
      lpwd = strlen(pwd);
      memcpy(pwdEspecial, cLlavePriv, 8);
      int lcars = 0, ldatos = 0;
      ierror = getTamSegAnt(cLlavePriv+8, (uint8*) &lcars, (uint16*) &ldatos);
      ldatos = ldatos+lcars+1;
      cLlaveObt  = new unsigned char[ldatos];
      memcpy( (char*) cLlaveObt, cLlavePriv+8, ldatos);
      //Este c�digo solo se aplica cuando la llave es muy antigua y se corrige un error de longitud
      if ( (cLlaveObt[5] == 0x1A) && (cLlaveObt[18] == 0x0D) && activarCod )
      {
         cLlaveObt[ 5] = 0x1B;
         cLlaveObt[18] = 0x0E;
      }
      //Fin c�digo de correcci�n
      rsaLlavePriv = RSA_new();
      ierror = objRSA.getLlave(cLlaveObt, ldatos, pwdEspecial, &rsaLlavePriv);
      if ( ierror == EXITO)
      {
         FILE* fpp = fopen(prarchKey.c_str(), "wb");
         if (!fpp)
         {
            strError = "Error al generar el archivo de salida: " + prarchKey;
            ierror = ERR_PKCS7_NOARCH;
         }
         else
            ierror = (int) objRSA.CodPvKey1(rsaLlavePriv, (uchar*) pwd, lpwd, fpp, AES_256_CBC_ALG, DER_FORMAT);
      }else
      {
         strError = "Error (" + objValida->int2string(ierror) + "), al obtener la llave";
      }
      RSA_free(rsaLlavePriv);
      delete sc;
      sc = NULL;
   }
   else
   {
      strError = "No se descifraron correctamente los segmentos aplicando el formato anterior";
      ierror = ERR_FTO_ANT_NODECDATOS; 
   }
   delete[] cLlavePriv;
   cLlavePriv = NULL;
   return (ierror==EXITO);
}

//#################################################################################
bool CArmaLlave::armaKeyPKCS7(std::string prarchKey, char *pwd, unsigned char **uSegmentos, int iTamSeg ,int iNumSeg)
{
   int       ierror, lpwd = 0;
   SGIRSA    objRSA;
   EVP_PKEY *evpLlavePriv = NULL;
   int       iTamLlave  = (iTamSeg/2);

   CSecretoCompartido  *sc = new CSecretoCompartido(iNumSeg, iNumSeg);
   const unsigned char *cLlavePriv = new unsigned char[iTamSeg];

   lpwd = strlen(pwd);
   if (sc->decodifica(uSegmentos, iTamSeg, (uint8*) cLlavePriv) )
   {
      SGI_SHK* shk = SGI_SHK_new();
      shk  = d2i_SGI_SHK(&shk, &cLlavePriv, iTamLlave);
      if(shk)
      {
         BIO *bLlavePriv  = BIO_new(BIO_s_mem());
         BUF_MEM *memLlavePriv;
         if (bLlavePriv)
         {
            memLlavePriv = BUF_MEM_new();
            BUF_MEM_grow(memLlavePriv, shk->pkcs8->length);
            memcpy(memLlavePriv->data, shk->pkcs8->data, shk->pkcs8->length);
            BIO_set_mem_buf(bLlavePriv, memLlavePriv, BIO_CLOSE);
            evpLlavePriv = d2i_PKCS8PrivateKey_bio(bLlavePriv, NULL, NULL, shk->pwd->data);
            if (evpLlavePriv)
            {
               RSA *rsaLlavePriv  = EVP_PKEY_get1_RSA(evpLlavePriv);
               if( rsaLlavePriv)
               {
                  FILE* fpp = fopen(prarchKey.c_str(), "wb");
                  if (!fpp)
                     ierror = ERR_PKCS7_NOARCH;
                  else
                     ierror = (int) objRSA.CodPvKey1(rsaLlavePriv, (uchar*) pwd, lpwd, fpp,AES_256_CBC_ALG, DER_FORMAT);
                  BIO_free(bLlavePriv);
                  EVP_PKEY_free(evpLlavePriv);
                  RSA_free(rsaLlavePriv);
                  return true;
               }
               else
                  strError = "Error en la asignaci�n de la memoria del RSA para el PKCS7";
            }
            else
               strError = "Error en la memoria asignada al EVP para el PKCS7";
         }
         else
            strError = "Error en la memoria asignada al BIO para el PKCS7";
      }
      else
      {
         strError = "No se realizo el proceso SHK correctamente para el PKCS7";
         ierror = ERR_get_error();
         char bError[120];
         char *  cError  = ERR_error_string(ERR_get_error(), bError);
         strError = "\n" + std::string(cError);
      }
      SGI_SHK_free(shk);
      delete sc;
      sc = NULL;
   }
   else
      strError = "No se descifraron correctamente los segmentos en formato PKCS7";
   return false;
}

//#################################################################################
int CArmaLlave::armaLlavePriv(std::string prarchKey, char *pwd, int tipoFtoKey,
                              unsigned char **uSegmentos, int iTamSeg, int iNumSeg)
{
   bool result = false; 
   if (tipoFtoKey == TIPO_FTO_PKCS7)
      result = armaKeyPKCS7(prarchKey, pwd, uSegmentos, iTamSeg, iNumSeg);
   else 
      result = armaKeyAnt(prarchKey, pwd, uSegmentos, iTamSeg, iNumSeg);
   return result;
}
