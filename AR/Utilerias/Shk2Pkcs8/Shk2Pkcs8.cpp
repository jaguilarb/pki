static const char* _SHK2PKCS8_CPP_VERSION_ ATR_USED = "@(#) Shk2Pkcs8 ( L : DSIC10441AR_ : Shk2Pkcs8.cpp : 1.0.0 : 0 : 08/10/10)";

/*########################################################################################
  ###  PROYECTO:              Aplicación                                               ###
  ###  MODULO:                Shk2Pkcs8: Programa principal que maneja las operaciones ###
  ###                                    para transformar las secciones de llave en    ###
  ###                                    memoria compartida a un archivo Pkcs8         ###
  ###  DESARROLLADORES:                                                                ###
  ###                         Gudelia Hernández Molina     GHM                         ###
  ###                                                                                  ###
  ###  FECHA DE INICIO:       octubre 2010                                             ###
  ###                                                                                  ###
  ########################################################################################
*/

/*#################################################################################
   VERSION:
      V.1.00      (20101008 - 20101122) GHM: Versión original

   CAMBIOS:

#################################################################################*/
#include "Def_Shk2Pkcs8.h"
#include <CFunciones.h>

//#################################################################################
std::string archKeys, archOut;
bool activarCod;

//#################################################################################
void DespliegaUso(char *argv[])
{
   std::cout << "\nUso: " << std::endl << "\t" << argv[0] << " -i ARCH_LLAVES -o ARCH_SALIDA\n" << std::endl << std::endl;
   std::cout << "Donde ARCH_LLAVES: Ruta y nombre del archivo que contiene la ubicación de las llaves que participan en el proceso" << std::endl;
   std::cout << "      ARCH_SALIDA: Ruta y nombre del archivo donde se almacenará la llave privada en formato PKCS8" << std::endl << std::endl;
}

//#################################################################################
int extraeVars( int argc, char *argv[] )
{
   int    opt = 0, ierror = 0;
   size_t l_dato = 0;
   char   *dato  = NULL;

   while( (opt = getopt( argc, argv, "ai:o:" )) != -1 )
   {
     if ( optarg == NULL)
     {
        l_dato = 0;
        dato = NULL;
     }
     else {
        l_dato = strlen( optarg );
        dato = (char *)malloc( sizeof(char) * l_dato + 1 );
        dato[l_dato] = 0x00;
        strncpy(dato, optarg, l_dato);
     }
     switch( opt )
     {
         case 'i' :
            archKeys = std::string(dato); break;
         case 'o' :
            archOut = std::string(dato); break;
         case 'a' :
            activarCod = true; break;
         default :
            std::cout << "\nError parámetro incorrecto." << std::endl;
            ierror = ERR_PARAM_INCORRECTO;
            break;
      }
      free(dato);
   }
   return ierror;
}
//#################################################################################
/*
   -i : Path del archivo de llaves (/ruta/nombre)
   -o : Path del archivo de salida (/ruta/nombrePKCS8)
*/
int ProcesaParametros( int argc, char *argv[] )
{
   int ierror = 0;
   
   if ( argc == 1 )
   {
      DespliegaUso( argv );
      return ERR_SIN_PARAM;
   }
   ierror = extraeVars(argc, argv);
#ifdef DBG
   std::cout << "Arch. Llaves: " << archKeys.c_str() << std::endl;
   std::cout << "Arch. Salida: " << archOut.c_str() << std::endl;
#endif
   if ( ierror == EXITO )
   { 
      if ( !archKeys.empty() )
      {
         if ( !archOut.empty() )
            ierror = EXITO;
         else
         {
            std::cout << "\nNo se proporcionó el archivo de Salida" << std::endl;
            ierror = ERR_FALTA_ARCHOUT;
         }
      }
      else
      {  
         std::cout << "\nNo se proporcionó el archivo de Llaves" << std::endl;
         ierror = ERR_FALTA_ARCHKEYS;
      }
   }
   if ( ierror != EXITO )
      DespliegaUso( argv );
   return ierror;
}

//#################################################################################
int main (int argc, char* argv[])
{
   //Por default esta desactivado el proceso de corrección de llave antigua
   activarCod = false;
   //Inicializa el ambiente para utilizar las librerías de criptografía
   iniciaLibOpenSSL();
   int resp = ProcesaParametros( argc, argv );
   if ( resp == EXITO )
   {
      CFunciones funcGrales(activarCod);
      resp = funcGrales.procesaKeys(archKeys, archOut);
      if ( resp == EXITO )
          std::cout << "PROCESO TERMINADO EXITOSAMENTE" << std::endl << std::endl;
   }
   //Libera el ambiente utilizado por las librerías de criptografía.
   terminaLibOpenSSL();
   return resp;
}
