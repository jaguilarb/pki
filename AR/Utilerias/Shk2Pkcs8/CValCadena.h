#ifndef _CVALCADENA_H_
#define _CVALCADENA_H_

static const char* _CVALCADENA_H_VERSION_ ATR_USED = "@(#) Shk2Pkcs8 ( L : DSIC10441AR_ : CValCadena.h : 1.0.0 : 0 : 08/10/10)";

/*##############################################################################
  ###  DESARROLLADORES:                                                      ###
  ###                         Gudelia Hern�ndez Molina     GHM               ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       octubre 2010                                   ###
  ###                                                                        ###
  ##############################################################################
*/
/*#################################################################################
   VERSION:
      V.1.00      (20101008 - 20101122) GHM: Versi�n original

   CAMBIOS:

#################################################################################*/

#include <stdio.h>
#include <string>

//#################################################################################
// Etiquetas para saber el tipo de caracter
#define LBL_LETRA            1
#define LBL_LETRAM           2
#define LBL_NUMERO           3
#define LBL_CONSONANTE       4
#define LBL_CARACTVAL        5
#define LBL_ARROBA           6
#define LBL_PUNTO            7
#define LBL_EXTRANO          8
#define LBL_NOCONSID         9
#define LBL_CARACTVALRFC    10

//Define el tama�o m�nimo para un password
#define TAM_MINIMO_PWD      11
#define TAM_MAX_ENTERO      12

//#################################################################################
class CValCadena
{
   protected:
      std::string strError;
      
      int  evalChar( char caract );
      bool esLetra(char car, bool Mayusc, bool SoloCons);
      bool esNumero(char car);
      bool esCaractVal(char car);
      bool esArroba(char car);
      bool esPunto(char car);
      bool esExtrano(char car);
      bool esCaractValLect(char car);

   public:
      CValCadena();
      ~CValCadena();

      std::string getError();
      std::string int2string(int valor);
      bool evalTam(std::string sCadena); 
      bool valCveAcc(std::string sPwdValidar);
};
#endif //_CVALCADENA_H_
