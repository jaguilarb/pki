static const char* _CFUNCIONES_CPP_VERSION_ ATR_USED = "@(#) Shk2Pkcs8 ( L : DSIC10441AR_ : CFunciones.cpp : 1.0.0 : 0 : 08/10/10)";

/*##############################################################################
  ###  DESARROLLADORES:                                                      ###
  ###                         Gudelia Hern�ndez Molina     GHM               ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       octubre 2010                                   ###
  ###                                                                        ###
  ##############################################################################
*/
/*#################################################################################
   VERSION:
      V.1.00      (20101008 - 20101122) GHM: Versi�n original

   CAMBIOS:

#################################################################################*/

#include "CFunciones.h"

//Constructor
CFunciones::CFunciones(bool activar)
{
   tipoFormatoLlave = NO_EVALUADO;
   inumKeys         = 0;
   objValida        = new CValCadena();
   objArmaKey       = new CArmaLlave(activar);
   strError         = "";
}

//Destructor
CFunciones::~CFunciones()
{
   if (objValida)
   {
      delete objValida;
      objValida = NULL;
   }
   if (objArmaKey)
   {
      delete objArmaKey;
      objArmaKey = NULL;
   }
}

//Proporciona el mensaje de error
std::string CFunciones::getError()
{
   return strError;
}

//#################################################################################
int CFunciones::valFile(std::string rutaArch, int *lbuffer)
{
   struct stat statusFile;
   strError.erase();
   if ( (int) rutaArch.length() < 0 )
   {
      strError = "Revise la estructura de la ruta proporcionada.";
      return ERR_PATH_KEY;
   }
   if ( stat(rutaArch.c_str(), &statusFile) )
   {
      strError = "Se present� errno: " + objValida->int2string(errno) +". Al obtener el status del archivo de lectura.\n";
      return errno;
   } 
   *lbuffer = (int) statusFile.st_size;
   return EXITO;
}

//#################################################################################
int CFunciones::getFileKeys(std::string rutaArch)
{
   int           ierror = EXITO, lbufFileKeys = 0;
   std::fstream *ptrFileKeys = NULL;
   char         *ptrLinea = NULL, caract;
   
   strError.erase();
   if ( (ierror = valFile(rutaArch, &lbufFileKeys)) != 0 )
   {
      strError = strError + "Archivo: " + rutaArch +". Capturado en el par�metro -i";
      return ierror;
   }
   ptrFileKeys = new std::fstream(rutaArch.c_str(), std::ios::in | std::ios::binary);
   if (ptrFileKeys->fail())
   {
      strError = "Se present� el error: " + objValida->int2string(errno) + "\nArchivo: " + rutaArch;
      return errno;
   }
   if ( (ptrLinea = new char[(size_t) MAX_TAM_LINEA + 1]) == NULL )  // + el nulo
   {
      ptrFileKeys->close();
      delete ptrFileKeys;
      strError = "No hay memoria suficiente para extraer datos del archivo: " + rutaArch;
      return ERR_SPACE_MEM;
   } 
   while(!ierror && !ptrFileKeys->eof())
   {
      switch ( caract = ptrFileKeys->peek()) 
      {
         case '#' :
         case '\n':
         case '\0':
         case  -1 :
         {
            ptrFileKeys->ignore((long) MAX_TAM_LINEA, '\n');
            break;
         }
         default:
         {
            memset(ptrLinea, 0, (size_t) MAX_TAM_LINEA + 1);
            ptrFileKeys->getline(ptrLinea, (size_t) MAX_TAM_LINEA);
            if ( strlen(ptrLinea) > 0 )
            {
               arrayKeys[inumKeys] = std::string(ptrLinea);
               inumKeys++;
            }
         }
      }
   }
   ptrFileKeys->close();
   delete ptrFileKeys; 
   delete[] ptrLinea;
   return EXITO;
}

//#################################################################################
int CFunciones::getKeyBuff(std::string rutaArch, unsigned char **bufKey, int *lbufKey)
{
   int           ierror = EXITO;
   char         *tmpBuf = NULL;
   std::fstream *ptrFile = NULL;
   
   strError.erase();
   if ( (ierror = valFile(rutaArch, lbufKey)) != 0 )
   {
      strError = strError + "Archivo: " + rutaArch + ". Indicado en el archivo de llaves";
      return ierror;
   }
   ptrFile = new std::fstream(rutaArch.c_str(), std::ios::in | std::ios::binary);
   if (ptrFile->fail())
   {
      strError = "Se present� el errno: " + objValida->int2string(errno) + ". Segmento: " + rutaArch + ".\n";
      return errno;
   }
   tmpBuf = new char[(size_t)(*lbufKey)];
   if ( !tmpBuf )
   {
      strError = "No hay memoria suficiente para leer el segmento." + rutaArch;
      ptrFile->close();
      return ERR_SPACE_MEM;
   } 
   memset(tmpBuf, 0, *lbufKey); 
   ptrFile->read(tmpBuf, (size_t)(*lbufKey));
   if (ptrFile->fail())
   {
      strError = "Se present� el error: " + objValida->int2string(errno) +". En la lectura del archivo:" + rutaArch;
      delete[] tmpBuf;
      return errno;
   }
   *bufKey = new unsigned char[(size_t)(*lbufKey)];
   if ( !*bufKey )
   {
      strError = "No hay memoria suficiente para cargar el segmento:" + rutaArch;
      delete[] tmpBuf;
      ptrFile->close();
      return ERR_SPACE_MEM;
   }
   memset(*bufKey, 0, *lbufKey);
   memcpy(*bufKey, (unsigned char*)tmpBuf, *lbufKey);
   delete[] tmpBuf;
   ptrFile->close();
   return EXITO;
}

//#################################################################################
int CFunciones::getKeyBuffAnt(std::string rutaArch, unsigned char **bufKey, int *lbufKey)
{
   int            ierror = EXITO;
   FILE          *fp     = NULL;
   size_t         lfp    = 0;
   strError.erase();
   if ( (ierror = valFile(rutaArch, lbufKey)) != 0 )
   {
      strError = strError + "Archivo: " + rutaArch + ". Indicado en el archivo de llaves";
      return ierror;
   }
   fp = fopen(rutaArch.c_str(), "rb");
   if (fp)
   {
      fseek(fp, 0L, SEEK_END);
      lfp = ftell(fp);
      fseek(fp, 0L, SEEK_SET);
      *bufKey = new unsigned char[lfp];
      fread(*bufKey, 1, lfp, fp);
      *lbufKey = (int) lfp;
      return EXITO;
   }
   else
   {
      strError = "No hay memoria suficiente para cargar el segmento: " + rutaArch;
      return ERR_SPACE_MEM;
   }
}

//#################################################################################
int CFunciones::solicitaPwd(bool valida, char *pwd )
{
   int  ierror = EXITO;
   if (!valida)
   {
      memset(pwd, 0, MAX_TAM_PWD + 1);
      strcpy(pwd, getpass("\n   Proporcione la contrase�a: "));
      if ( strlen(pwd) == 0)
      {
         strError = "La contrase�a proporcionada esta vac�a.";
         ierror = ERR_PWD_INCORRECTO;
      }
   }
   else
   {
      char confPwd[ MAX_TAM_PWD + 1 ];
      for (int intentos=0; intentos < MAX_NUM_INTENTOS; intentos++)
      {
         memset(pwd,    0, MAX_TAM_PWD + 1);
         memset(confPwd,0, MAX_TAM_PWD + 1);
         strcpy(pwd, getpass("\n   Proporcione la contrase�a: "));
         strcpy(confPwd, getpass("   Confirme la contrase�a: "));
         if ( strcmp(pwd,confPwd) == 0 )
         {
            if ( objValida->evalTam(std::string(pwd)) )
            {
               if ( !objValida->valCveAcc(std::string(pwd)) )
               {
                  strError = objValida->getError();
                  std::cout << "   Error en el formato de la contrase�a: " << strError.c_str() << std::endl;
                  ierror = ERR_PWD_INCORRECTO;
               } 
               else
               {
                  ierror = EXITO;
                  break;
               }
            }
            else
            {
               strError = objValida->getError();
               strError = "No se captur� correctamente la contrase�a. " + strError;
               std::cout << "\n   Intento(" << intentos+1 << "): " << strError.c_str() << std::endl;
               ierror = ERR_PWD_CAPTURA;
            }
         }
         else
         {
            strError = "No se captur� correctamente la contrase�a. ";
            std::cout << "\n   Intento(" << intentos+1 << "): " << strError.c_str() << std::endl;
            ierror = ERR_PWD_CAPTURA;
         }
      }
   }
   if ( ierror != EXITO )
      memset(pwd, 0, MAX_TAM_PWD+1);
   return ierror;
}

//#################################################################################
int CFunciones::getFtoAnt(std::string pathSegmento, char *pwd, unsigned char **bufKey, int *lbufKey, int *tipo)
{
   int            ierror = EXITO;
   unsigned char *bufKeyTmp = NULL, *plana = NULL;
   int            lbufKeyTmp = 0;
   unsigned char *hash = new unsigned char[16];
   MD5_CTX        md5ctx;
   RC4_KEY        rc4key;
   uint8          cars_lng = 0;
   uint16         lng_datos =0;
  
   ierror = getKeyBuffAnt(pathSegmento, &bufKeyTmp, &lbufKeyTmp);
   if (ierror != EXITO )
      return ierror;
   if ( (bufKeyTmp[0] == 0x30) && (bufKeyTmp[1] == 0x82) )
      return ERR_FTO_ANTERIOR;
   plana = new unsigned char[lbufKeyTmp];
   memset(plana, 0, lbufKeyTmp);
   MD5_Init(&md5ctx);
   MD5_Update(&md5ctx, pwd, strlen(pwd));
   MD5_Final(hash, &md5ctx);
   RC4_set_key(&rc4key, 16, hash);
   RC4(&rc4key, lbufKeyTmp, bufKeyTmp, plana);
   ierror = objArmaKey->getTamSegAnt(plana, &cars_lng, &lng_datos);
   if ( ierror == EXITO )
   {
      *tipo = TIPO_FTO_ANTERIOR;
      *lbufKey = (int) lng_datos;
      *bufKey = new unsigned char[(size_t) *lbufKey];
      memcpy(*bufKey, plana + cars_lng + 1, *lbufKey);
      return EXITO;
   }
   else
      strError = objArmaKey->getError();
   return ierror;
}

//#################################################################################
PKCS7* CFunciones::obtienePKCS7(std::string pathSegmento)
{
   PKCS7* pkcs7 = NULL;
   FILE     *fp = NULL;

   fp = fopen(pathSegmento.c_str(), "r");
   if(fp != NULL)
    d2i_PKCS7_fp(fp,&pkcs7);

   if(fp)
   {
      fclose(fp);
      fp = NULL;
   }
   return pkcs7;
}

int CFunciones::getFtoPKCS7(std::string pathSegmento, char *pwd, unsigned char **bufKey, int *lbufKey, int *tipo)
{
   PKCS7 *pkcs7    =  NULL;
   int    lbufKeyTmp = -1;
   int    ierror = EXITO;
   
   unsigned char *bufKeyTmp = new unsigned char[MAX_TAM_KEY];
   pkcs7 = obtienePKCS7(pathSegmento);
   if ( pkcs7 != NULL)
   {
      if( pkcs7->d.encrypted->enc_data->algorithm && 
          pkcs7->d.encrypted->enc_data->enc_data->data && 
          pkcs7->d.encrypted->enc_data->enc_data->length )
      {
         if (!PKCS12_pbe_crypt( pkcs7->d.encrypted->enc_data->algorithm, pwd,  (int) strlen(pwd), 
                               (unsigned char*) pkcs7->d.encrypted->enc_data->enc_data->data, 
                               pkcs7 ->d.encrypted->enc_data->enc_data->length, 
                               &bufKeyTmp, &lbufKeyTmp, 0) )
         {
            ierror = ERR_get_error();
            char bError[120];
            char *  cError  = ERR_error_string(ERR_get_error(), bError);
            strError = strError + "\n" + std::string(cError) + 
                       ". Verifique que la contrase�a sea la correcta para el segmento en formato PKCS7";
         }
         else
         {
            if (lbufKeyTmp > 0)
            {
                *lbufKey = objArmaKey->getTamSegmento(bufKeyTmp, lbufKeyTmp);
                *bufKey = new unsigned char[*lbufKey];
                memset(*bufKey, 0, *lbufKey);
                memcpy(*bufKey, bufKeyTmp+(lbufKeyTmp - *lbufKey), (size_t) *lbufKey);
                *tipo = TIPO_FTO_PKCS7;
            }
            else
            {
               ierror = ERR_PKC7_NOCRYPT;
               strError = "\nNo se desencripto el segmento en formato PKCS7. ";
            }
          }
      }
      else
      {
         ierror = ERR_PKCS7_NODECDATOS;
         strError = strError + "\nDatos del PKCS7 incorrectos. ";
      }
   }
   else
   {
      ierror = ERR_PKCS7_NOGETPKCS7;
      strError = strError + "\nEl segmento tampoco corresponde al formato PKCS7 o el password es incorrecto. ";
   }
   return ierror;
}
//#################################################################################
int CFunciones::trataLlave(std::string pathLeido, int iReg, unsigned char **bufKey, int *lbufKey, int *tipoFto)
{
   int  ierror = EXITO;
   char pwd[ MAX_TAM_PWD + 1 ];
   memset(pwd, 0, MAX_TAM_PWD + 1);
   std::cout << "\nLlave(" << iReg << "): " << pathLeido.c_str() << std::endl;
   ierror = solicitaPwd(false, pwd);
   if (ierror == EXITO)
   {
      ierror = getFtoAnt(pathLeido, pwd, bufKey, lbufKey, tipoFto);
      if ( ierror != EXITO )
      {
         if ( ierror == ERR_FTO_ANTERIOR )
         {
            ierror = getFtoPKCS7(pathLeido, pwd, bufKey, lbufKey, tipoFto);
            if (ierror != EXITO)
            {
               strError = "\nNo se carg� el segmento en el buffer correctamente: \n" + strError;
               std::cout << strError.c_str() << "\nPresione cualquier tecla para continuar." << std::endl << std::endl;
               getchar();
            }
         }
         else
         {
            std::cout << "\nVerifique que el segmento este correctamente registrado en el archivo de entrada." << std::endl;
            std::cout << strError.c_str() << std::endl;
            std::cout << "Presione cualquier tecla para continuar." << std::endl << std::endl;
            getchar();
         }
      }
   }
   else
   {
      std::cout << "\nFall� la captura de la contrase�a del segmento: " << pathLeido.c_str() << std::endl; 
      std::cout << strError.c_str() << std::endl;
      std::cout << "Presione cualquier tecla para continuar." << std::endl << std::endl;
      getchar();
   }
   return ierror; 
}

//#################################################################################
int CFunciones::procesaKeys(std::string pathFileKeys, std::string pathSalida)
{
   int             ierror = EXITO, icont  = 0;
   int             tipoFto = NO_EVALUADO, tipoFtoAux = NO_EVALUADO;
   unsigned char **privKeySS = NULL;
   unsigned char  *tmpPrivKey = NULL;
   bool            armada;
   int             iTamSeg = 0, ltmpPrivKey = 0;
   char pwd[ MAX_TAM_PWD + 1 ];
   memset(pwd, 0, MAX_TAM_PWD + 1);
   
   if ( (ierror = getFileKeys(pathFileKeys)) == EXITO )
   {
      if ( inumKeys > 0 )
      {
         privKeySS = new unsigned char*[inumKeys];
         for ( icont = 0; icont < inumKeys; icont++)
         {
            ierror = trataLlave(arrayKeys[icont], icont+1, &tmpPrivKey, &ltmpPrivKey, &tipoFtoAux);
            if (ierror == EXITO )
            {
               if ( icont == 0 )
                  tipoFto = tipoFtoAux;
               else
               {
                  if ( tipoFtoAux != tipoFto )
                  {
                     std::cout << "\nLos segmentos registrados no tiene el mismo formato." << std::endl;
                     std::cout << "Presione cualquier tecla para continuar." << std::endl << std::endl;
                     ierror = ERR_KEYS_DIF_FTO;
                     break;
                  }
               }
               privKeySS[icont] = new unsigned char[ltmpPrivKey];
               memcpy(privKeySS[icont], (uint8*)tmpPrivKey, size_t(ltmpPrivKey));
               iTamSeg = ltmpPrivKey;
            }
            else
               break;
         }
         if (ierror == EXITO)
         {
            std::cout << "\nProceso de armado de Llave: " << std::endl;
            ierror = solicitaPwd(true, pwd);
            std::cout << std::endl;
            if (ierror != EXITO)
            {
               std::cout << "\nFall� la captura de la contrase�a de la llave: " << pathSalida.c_str() << std::endl;
               std::cout << strError.c_str() << std::endl;
               std::cout << "Presione cualquier tecla para continuar." << std::endl << std::endl;
               ierror = ERR_PWD_CAPTURA;
               getchar();
            }
            else
            {
               if ( !(armada = objArmaKey->armaLlavePriv(pathSalida, pwd, tipoFto, privKeySS, iTamSeg, inumKeys)) )
               {
                  std::cout << "\nFall� el armado de la llave: " << pathSalida.c_str() << std::endl;
                  std::cout << objArmaKey->getError().c_str() << std::endl;
                  std::cout << "Presione cualquier tecla para continuar." << std::endl << std::endl;
                  ierror = ERR_SC_NODECDATOS;
                  getchar();
               }
            }
         }
         delete []privKeySS;
         delete tmpPrivKey; 
         privKeySS = NULL;
         tmpPrivKey = NULL;
      }
   }
   else
      std::cout << strError.c_str() << std::endl;
   return ierror;
}
