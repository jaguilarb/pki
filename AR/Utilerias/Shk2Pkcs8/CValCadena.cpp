static const char* _CVALCADENA_CPP_VERSION_ ATR_USED = "@(#) Shk2Pkcs8 ( L : DSIC10441AR_ : CValCadena.cpp : 1.0.0 : 0 : 08/10/10)";

/*##############################################################################
  ###  DESARROLLADORES:                                                      ###
  ###                         Gudelia Hern�ndez Molina     GHM               ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       octubre 2010                                   ###
  ###                                                                        ###
  ##############################################################################
*/
/*#################################################################################
   VERSION:
      V.1.00      (20101008 - 20101122) GHM: Versi�n original

   CAMBIOS:

#################################################################################*/

#include "CValCadena.h"

//#################################################################################
//Constructor
CValCadena::CValCadena()
{
}

//Destructor
CValCadena::~CValCadena()
{
}

std::string CValCadena::getError()
{
   return strError;
}

//#################################################################################
std::string CValCadena::int2string(int valor)
{
   char dato[TAM_MAX_ENTERO+1];
   memset(dato, 0, TAM_MAX_ENTERO+1);
   sprintf(dato, "%i", valor);
   return std::string(dato);
}

bool CValCadena::evalTam(std::string sCadena) 
{
   strError.erase();
   if (sCadena.length() < TAM_MINIMO_PWD)
   {  
      strError = "La cadena debe tener un tama�o m�nimo de " + int2string(TAM_MINIMO_PWD) + " caracteres.";
      return false;
   }
   return true;
}

// ***************************************
// Valida que la clave de acceso tenga, al menos, letras Mayusculae, minusculas y numeros. Acepta caracteres extra�os
// ***************************************
bool CValCadena::valCveAcc(std::string sPwdValidar)
{
   int posicion, tipoDato, numLet = 0, numNum = 0, numOtr = 0;
   int final = sPwdValidar.length();
   strError.erase();
   if ( sPwdValidar.empty() )
   {
      strError = "La contrase�a proprocionada esta vac�a.";
      return false;
   }
   for (posicion = 0; posicion < final; posicion++)
   {
      tipoDato = evalChar(sPwdValidar[posicion]);
      switch (tipoDato)
      {
         case LBL_LETRA:
            numLet++;
            break;
         case LBL_LETRAM:
            numLet++;
            break;
         case LBL_NUMERO:
            numNum++;
            break;
         default:
            numOtr++;
            break;
      }
   }
   if (numLet == 0 || numNum == 0)
   {
      strError = "\nLa contrase�a: debe contener una combinaci�n de letras y n�meros, as� como, un tama�o m�nimo de " 
                 + int2string(TAM_MINIMO_PWD) + " caracteres.";
      return false;
   }
   return true;
}

// ***************************************
// Determina si el caracter es letra minuscula, mayuscula, numero, caracter valido,@, punto, extra�o o no considerado
// ***************************************
int CValCadena::evalChar( char caract )
{
   if ( esLetra(caract, false, false) ) 
      return LBL_LETRA;
   if ( esLetra(caract, true, false) ) 
      return LBL_LETRAM;              
   if ( esNumero(caract) )
      return LBL_NUMERO;
   if ( esCaractVal(caract) )
      return LBL_CARACTVAL;
   if ( esArroba(caract) )
      return LBL_ARROBA;
   if ( esPunto(caract) )
      return LBL_PUNTO;
   if ( esExtrano(caract) )
      return LBL_EXTRANO;
   return LBL_NOCONSID;
}

bool CValCadena::esLetra(char car, bool Mayusc, bool SoloCons)
{  
   if (!SoloCons) //Considera el alfabeto
   {
      if (Mayusc) //en mayusculas
         return strchr("ABCDEFGHIJKLMNOPQRSTUVWXYZ", car) != NULL;
      else        //en minusculas
         return strchr("abcdefghijklmnopqrstuvwxyz", car) != NULL;
   }
   else           //Solo consonantes
   {  
      if (Mayusc) //en may�sculas
         return strchr("BCDFGHJKLMNPQRSTVWXYZ", car) != NULL;
      else        //en minusculas
         return strchr("bcdfghjklmnpqrstvwxyz", car) != NULL;
   }
}

bool CValCadena::esNumero(char car)
{
   return strchr("0123456789", car) != NULL;
   //isdigit(tolower(car)) != 0; regresa true para �
}

bool CValCadena::esCaractVal(char car)
{
   return strchr("-_", car) != NULL;
}

bool CValCadena::esArroba(char car)
{
   return strchr("@", car) != NULL;
}

bool CValCadena::esPunto(char car)
{
   return strchr(".", car) != NULL;
}

bool CValCadena::esExtrano(char car)
{
   return strchr("�,;+*�!�?'/:><|��#$%&()=[]{}�����������", car) != NULL;
}
