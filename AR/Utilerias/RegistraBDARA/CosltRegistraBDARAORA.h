#ifndef _COSLTREGISTRABDARA_H_
#define _COSLTREGISTRABDARA_ARA_H_

static const char* _COSLTREGISTRABDARA_H_VERSION_ ATR_USED = "@(#) SrvAC ( L : DSIC10392AR_ : ConsltRegistraBDARA.h : 1.0.0 : 10/10/14)";

//#VERSION: 1.1.0
/*##############################################################################
  ###  PROYECTO:              PKI-SAT                                        ###
  ###  MODULO:                Servicio AR                                    ###
  ###                                                                        ###
  ###                                                                        ###
  ###  DESARROLLADORES:       Guadalupe Negrete Castro         GNC           ###
  ###                                                                        ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       Viernes 10, octubre del 2014                   ###
  ###                                                                        ###
  ##############################################################################
         1         2         3         4         5         6         7         8
12345678901234567890123456789012345678901234567890123456789012345678901234567890

################################################################################
  VERSION:
     V.1.0.0  (20141010- )    GNC: Version Original
                              Consutas para el Servicio de Autoridad Registradora PKI-SAT
  

#################################################################################*/



//#VERSION: 1.1.0


#define QS_NOSERIE_CERTIFICADO              "SELECT NOSERIE FROM SEG_PKI.SEGT_CERTIFICADO WHERE NOSERIE = '%s'; "        
#define QI_CERTIFICADO                      "INSERT INTO SEG_PKI.SEGT_CERTIFICADO (NOSERIE , RFC ,EDOCER , TIPCERCVE , VIGINI , VIGFIN ) VALUES ( '%s','%s','%c',%s,'%s','%s' );"
#define QS_CUANTOS_CERTIFICADO              "SELECT COUNT(*) FROM SEG_PKI.SEGT_CERTIFICADO WHERE EDOCER = '%s'"
#define QU_EDO_VF_CERTIFICADO               "UPDATE SEG_PKI.SEGT_CERTIFICADO SET EDOCER = '%c', VIGFIN = '%s' WHERE NOSERIE = '%s'"
#define QU_EDO_CERTIFICADO                  "UPDATE SEG_PKI.SEGT_CERTIFICADO  SET EDOCER = '%c' WHERE NOSERIE = '%s'"




#endif




