#ifndef _REGISTRABDARA_H_
#define _REGISTRABDARA_H_
static const char* _REGISTRABDARA_H_VERSION_ ATR_USED = "RegistraBDARA @(#)"\
"DSIC07412AR_ 2007-12-14 RegistraBDARA.h 1.1.0/0";

//#VERSION: 1.1.0
//#include <fstream>
#include <sys/stat.h>

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/uio.h>
#include <strings.h>
#include <unistd.h>
#include <iostream>
#include <dirent.h>
#include <Sgi_Bitacora.h>
//#include <SgiOpenSSL.h>
//#include <RutaCerts.h>
#if ORACLE
   #include <CosltRegistraBDARAORA.h>
#else
   #include <CosltRegistraBDARA.h>
   #include <mitypes.h>
   #include <it.h>
#endif

#include <Sgi_BD.h>
//- #include <PwdProtection.h> //- GHM (070430)
#include <Sgi_ProtegePwd.h>      //+ GHM (070430)
#include <Sgi_Fecha.h>
#include <errno.h>
//- #include <CConfigFile.h>   //- GHM (070430)
#include <Sgi_ConfigFile.h>

struct DatCert
{
   char operacion     ;
   char num_serie [21];
   char estado        ;
   char tipCert   [ 3];
   char fec_ini   [20];
   char fec_fin   [20];
   char rfc       [14]; 
  
   void Limpia();
   int  SeparaCad(char *cCad, int lcad);
};



class RegistraBDARA
{
      CBitacora  *log;
      CBD        *bdARA;
      
      DatCert    datos;

      //char RUTA_PEND    [PATH_MAX]; //>- ERGL (220607)
      std::string	ruta_pend; 
      char cArchConf    [PATH_MAX];
      char cArchLog     [PATH_MAX];

      void NombreConfg(int numARA);
      int  RegistraBD();
      int  Actualiza();

      bool ConfiguraBD(CConfigFile* cfg);
      
   public:

      RegistraBDARA();
      ~RegistraBDARA();
  
      int  Inicia(int iNumARA);
      int  LeeDatos();
      void Finaliza();
};


#endif
