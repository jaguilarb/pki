static const char* _REGISTRABDARA_CPP_VERSION_ ATR_USED = "RegistraBDARA @(#)"\
"DSIC07412AR_ 2007-12-14 RegistraBDARA.cpp 1.1.0/0";

//#VERSION: 1.1.0
//#include <fstream>
#include <RegistraBDARA.h>

char cErrorTexto[256];

/*##################################################################################

 PROPOSITO: Construir la clase.

 #################################################################################*/
RegistraBDARA::
RegistraBDARA() :
   log(NULL),
   bdARA(NULL)
{
}
RegistraBDARA::
~RegistraBDARA()
{
   if( log )
      delete log; 
   if( bdARA )
      delete bdARA; 
}
/*##################################################################################

 PROPOSITO: Crea las rutas de los siguientes archivos :
            Configuracion
            Archivos log del programa y de la BD.

 #################################################################################*/
void RegistraBDARA::
NombreConfg(int numARA)
{
   #ifdef DBG
      sprintf(cArchConf ,"%s/RegistraBDARA_%02d_dbg.cfg", "/usr/local/SAT/PKI/etc", numARA);
      sprintf(cArchLog  ,"%s/RegistraBDARA_%02d_dbg.log", "/var/local/SAT/PKI"    , numARA);
   #else
      sprintf(cArchConf ,"%s/RegistraBDARA_%02d.cfg", "/usr/local/SAT/PKI/etc", numARA);
      sprintf(cArchLog  ,"%s/RegistraBDARA_%02d.log", "/var/local/SAT/PKI"    , numARA);
   #endif
}
/*##################################################################################

 PROPOSITO: Lee del directorio indicado y Realiza todas las operaciones de actuali
            cion y registro en la BD de la ARA.

 #################################################################################*/
int RegistraBDARA::
LeeDatos()
{
   DIR  *direc;
   char cNomArch[256]; 
   char cNom [25];
   char lect[256]; 
   char cTextoBit[256];
   int  desc;
   int  error = 0; 
   struct dirent *lista_nom; 

   for (int vez = 0; vez < 3 ; vez++) // Registra: 1� Activos, 2� Revocados, 3� Caducos
   {
      direc = opendir(ruta_pend.c_str());
      if(direc == NULL)
      {
         log->escribe(BIT_ERROR, errno, "Error al abrir el directorio de marcas");
         return -1;
      } 
 
      while((lista_nom = readdir(direc))!= NULL )
      {
         int tipoMarca = -1; 
         if (lista_nom->d_name[20] == '.')
         {
            switch (lista_nom->d_name[21])
            {
               case 'A': tipoMarca = 0; break;
               case 'R': tipoMarca = 1; break;
               case 'C': tipoMarca = 2; break;
            }
         }
         if ((tipoMarca == -1) || (tipoMarca != vez))
            continue;
      
         datos.Limpia();  
         memset(cTextoBit  , 0, sizeof(cTextoBit)); 
         memset(cErrorTexto, 0, sizeof(cErrorTexto)); 

         strcpy(cNom, lista_nom[0].d_name);
         sprintf(cNomArch, "%s/%s", ruta_pend.c_str(), cNom);
         
         desc = open(cNomArch, O_RDONLY);
         if(desc < 0)
         {
            log->escribePV(BIT_ERROR, "No se pudo abrir el archivo '%s' (%d)", cNom, errno); 
            continue;
         }
         memset(lect,0,sizeof(lect));

         error = read(desc,lect,sizeof(lect)); 
         if (error <=  0 )
         {
            log->escribePV(BIT_ERROR, "Se leyeron %d caracteres del archivo '%s' (%d)", error, cNom, errno);
            close(desc);
            continue;
         }
  
         lect[error] = 0;
         if (datos.SeparaCad(lect, error))
         {
            log->escribePV(BIT_ERROR, "Contenido del archivo '%s' mal estructurado", cNom);
            close(desc);
            continue;
         }
            
         if (datos.operacion == 'A') 
            error = RegistraBD();
         else if (datos.operacion == 'C' ||
                  datos.operacion == 'R')
         {  
            datos.estado = datos.operacion; 
            error = Actualiza();
         }
         else  
         {
            log->escribePV(BIT_ERROR, "Error al procesar '%s', tipo operaci�n desconocida (%c)", cNom, datos.operacion);
            close(desc);
            continue;
         }

         if(!error)
         {
           sprintf(cTextoBit,"Certificado procesado  %s con operacion [ %c ]", cNom ,datos.operacion);
           log->escribe(BIT_INFO,cTextoBit);  
           close(desc);
           unlink(cNomArch);
         }    
         else
         {
           sprintf(cTextoBit,"Certificado NO procesado  %s con operacion [ %c ] error : %s", 
                                                        cNom , datos.operacion, cErrorTexto); 
           log->escribe(BIT_ERROR,cTextoBit);
           close(desc);
         } 

      }  // WHILE
      closedir(direc);
      direc = NULL;
      
   }  // FOR
   //log->escribePV (BIT_DEBUG,"Fin de directorio, error = %d", error ); // MAML 070627: Se usa BIT_DEBUG junto con CBitacora::setNivel(BIT_DEBUG) 
   if (error)
      log->escribePV (BIT_INFO, "Fin de directorio, error = %d", error );
   return 0;
}

/*##################################################################################

 PROPOSITO: Registra en la BD 

 #################################################################################*/
int RegistraBDARA::
RegistraBD()
{

   char  cNumSerie[21];
   
   string sSentencia = QS_NOSERIE_CERTIFICADO;

   cNumSerie[0] = 0;
   
   if( bdARA->consultaReg(sSentencia.c_str() , datos.num_serie ))
   {
      if( !bdARA->getValores("b",cNumSerie) )
      {
         sSentencia = QI_CERTIFICADO;

         strtok(datos.rfc,"\n"); //HOA ??

         if(  bdARA->ejecutaOper(sSentencia.c_str(), datos.num_serie ,datos.rfc    , datos.estado ,
                                                     datos.tipCert, datos.fec_ini  , datos.fec_fin) )
            return 0;
      }   
      else // Update
         //>- return Actualiza(); //<- HOA: actualiza cambia la fecha de vigencia final
         return 0;
      
   } // if consulta   
   return -1;
   
}

/*##################################################################################

 PROPOSITO: Actualiza el estado del certificado en la BD.
 PREMISA  : ******
 EFECTO   : ******
 ENTRADAS : ******
 SALIDAS  : ******

 #################################################################################*/
//>>> HOA (080313): Se agrega manejo de actualizaci�n a Caduco
/*>-
int RegistraBDARA::
Actualiza()
{
     string sSentencia;
     char cNumSerie[21];
     sSentencia = "SELECT NO_SERIE FROM CERTIFICADO WHERE NO_SERIE = '%s'; ";
     if( bdARA->consultaReg( sSentencia.c_str(), datos.num_serie ) )
     {
        if( bdARA->getValores( "b", cNumSerie ) )
        {
           sSentencia = "UPDATE CERTIFICADO SET EDO_CER = '%c' , VIG_FIN = CURRENT  where no_serie = '%s' ;";
           if( bdARA->ejecutaOper(sSentencia.c_str(), datos.estado, datos.num_serie ) )
              return 0;
        }
     }
     return -1;
}
-<*/
//>+
int RegistraBDARA::Actualiza()
{
   int  ctos = 0;

   if (bdARA->consultaReg(QS_CUANTOS_CERTIFICADO, datos.num_serie) &&
       bdARA->getValores( "i", &ctos ) &&
       ctos) 
   {
       if (datos.estado == 'R')
       {
          if (bdARA->ejecutaOper(QU_EDO_VF_CERTIFICADO,datos.estado, datos.fec_fin, datos.num_serie) )
              return 0;
       }
       else // datos.estado == 'C'
       {
          if (bdARA->ejecutaOper(QU_EDO_CERTIFICADO,datos.estado, datos.num_serie) )
              return 0;
       }
   }

   //  RORS  jue jun  5 11:52:49 CDT 2008  Se agrega ya que cuando no existe el certificado en la Base no se escribia nada en la bitacora y el arhivo se quedaba en el repositorio.
   //log->escribe(BIT_INFO,"No existe el Certificado en la BD de la ARA, no se puede hacer el update");
   sprintf(cErrorTexto,"No existe en BD ARA.");
   return -1;
}
//<+
//<<< HOA (080313)

/*##################################################################################

 PROPOSITO: Inicia los objetos que se necesitan para la clase 

 #################################################################################*/
int RegistraBDARA::
Inicia( int iNumARA)
{
   int error;

   NombreConfg(iNumARA);
   log = new CBitacora(cArchLog);
   
   CConfigFile cfg(cArchConf);
   
   error = cfg.cargaCfgVars();
   
   if(error)
   {
      log->escribe(BIT_ERROR,"No se leyeron las variables del archivo de configuracion"); 	   
      
      return error;
   }
   
   bdARA = new CBD( log ) ;
   
   if(!bdARA)
   {
      log->escribe(BIT_ERROR,"No se cre� el objeto bdARA");
      
      return 1;
   }

   if (!ConfiguraBD(&cfg))
   {
      log->escribe(BIT_ERROR,"Error al configurar los par�metros de conexi�n a la base de datos");

      return 1;
   }

   if (!bdARA->conecta())
   {
      log->escribe(BIT_ERROR,"Error al realizar la conexi�n a la base de datos");

      return -2;
   }

   #ifdef DBG
      log->escribe(BIT_INFO, "Iniciado"); // MAML 070627: cambie BIT_DEBUG por BIT_INFO
   #endif
   
   return error;
}
//##################################################################################
bool RegistraBDARA::ConfiguraBD(CConfigFile* cfg)
{
    std::string bd_ara, instancia_ara, role_ara;        // Para Informix
    std::string bd_host_ara, bd_puerto_ara, bd_sid_ara; // Para Oracle
    std::string usuario_ara, passbd_ara;                // Para Oracle e Informix

    int r1, r2, r3, r4, r5, r6;

    char passwd[128];
    int  lpasswd = 128;      

    memset(passwd , 0, sizeof(passwd));

    #if ORACLE
      r1 = cfg->getValorVar("[GENERAL-ORACLE]", "HOST_ARA"  ,    &bd_host_ara);
      r2 = cfg->getValorVar("[GENERAL-ORACLE]", "PORT_ARA"  ,    &bd_puerto_ara);
      r3 = cfg->getValorVar("[GENERAL-ORACLE]", "SID_ARA"   ,    &bd_sid_ara);
      r4 = cfg->getValorVar("[GENERAL-ORACLE]", "USR_ARA"   ,    &usuario_ara);
      r5 = cfg->getValorVar("[GENERAL-ORACLE]", "PWD_ARA"   ,    &passbd_ara);
      r6 = cfg->getValorVar("[GENERAL-ORACLE]", "RUTA_PEND" ,    &ruta_pend);
    #else
      r1 = cfg->getValorVar("[GENERAL]", "BD_ARA"       ,    &bd_ara);
      r2 = cfg->getValorVar("[GENERAL]", "USUARIO_ARA"  ,    &usuario_ara);
      r3 = cfg->getValorVar("[GENERAL]", "INSTANCIA_ARA",    &instancia_ara);
      r4 = cfg->getValorVar("[GENERAL]", "PASSBD_ARA"   ,    &passbd_ara);
      r5 = cfg->getValorVar("[GENERAL]", "ROLE_ARA"     ,    &role_ara);
      r6 = cfg->getValorVar("[GENERAL]", "RUTA_PEND"    ,    &ruta_pend);
    #endif

    if (r1 || r2 || r3 || r4 || r5 || r6)
    {
      log->escribePV(BIT_ERROR, "Error en los par�metros de configuraci�n (%d,%d,%d,%d,%d,%d)", r1, r2, r3, r4, r5, r6);
      
      return false;
    }

    if (!desencripta((uint8*)passbd_ara.c_str(), passbd_ara.size(), (uint8*)passwd, &lpasswd))
    {
      log->escribe(BIT_ERROR, "Error al descifrar la contrase�a para la conexi�n a la base de datos");

      return false;
    }

    bool ok_configura = false;

    #if ORACLE
      ok_configura = bdARA->setConfig(usuario_ara.c_str(), passwd, bd_host_ara.c_str(), bd_puerto_ara.c_str(), bd_sid_ara.c_str());
    #else
      ok_configura = bdARA->setConfig(instancia_ara.c_str(), bd_ara.c_str(), usuario_ara.c_str(), passwd, role_ara.c_str());
    #endif

    if(!ok_configura)
    {
      log->escribe(BIT_ERROR,"No se han podido configurar los par�metros de conexi�n para la base de datos");      
      
      return false;
    }
    
    return true;
}
//##################################################################################

/*##################################################################################

 PROPOSITO: Desconecta la  BD.
 PREMISA  : ******
 EFECTO   : ******
 ENTRADAS : ******
 SALIDAS  : ******

 #################################################################################*/


void  RegistraBDARA::
Finaliza()
{
   bdARA->desconecta();
}

/*##################################################################################

 PROPOSITO: Separa la cadena que contiene los datos separados por | y los deja en 
            las variables indicadas.

 #################################################################################*/

int DatCert ::
SeparaCad(char *cCad, int lcad)
{
   int   i, cuantos;
   
   Limpia();


   if (lcad <= 0 ||                                           // Tama�o invalido
      (cCad[0] != 'A' && cCad[0] != 'R' && cCad[0] != 'C'))   // Operaci�n desconocida
      return -1;
   
   operacion = cCad[0];
   if (cCad[1] != '|')
      return -2;
      
   for (i = 0, cuantos = 0; i < lcad; i++)
      cuantos += (cCad[i] == '|');

   if (operacion == 'A')
   {
      if (lcad != 79 && lcad != 80)
         return -10;
      if (cuantos != 6)
         return -11;
      if (cCad[22] != '|' || cCad[24] != '|' || cCad[26] != '|' || cCad[46] != '|' || cCad[66] != '|')
         return -22;

      strncpy(num_serie, cCad + 2, 20);
      estado = cCad[23];
      strncpy(tipCert, cCad + 25, 1);
      strncpy(fec_ini, cCad + 27, 19);
      strncpy(fec_fin, cCad + 47, 19);
      strcpy (rfc    , cCad + 67);
   }
   else if (operacion == 'R')
   {
      if (lcad != 42)
         return -20;
      if (cuantos != 2)
         return -21;
      if (cCad[22] != '|')
         return -22;

      strncpy(num_serie, cCad + 2, 20);
      num_serie[20] = 0;
      strcpy(fec_fin, cCad + 23);
   }   
   else // (operacion == 'C')
   {
      if (lcad != 22)
         return -20;
      if (cuantos != 1)
         return -21;

      strcpy(num_serie, cCad + 2);
   }   

   return 0;
}
//##################################################################

void  DatCert ::
Limpia()
{
   operacion = 0;
   memset(num_serie  , 0, sizeof(num_serie));
   estado    = 0;
   memset(tipCert    , 0, sizeof(tipCert));
   memset(fec_ini    , 0, sizeof(fec_ini));
   memset(fec_fin    , 0, sizeof(fec_fin));
   memset(rfc        , 0, sizeof(rfc));
}

