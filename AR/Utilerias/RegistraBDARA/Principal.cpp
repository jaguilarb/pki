static const char* _PRINCIPAL_CPP_VERSION_ ATR_USED = "RegistraBDARA @(#)"\
"DSIC07412AR_ 2007-12-14 Principal.cpp 1.1.0/0";

//#VERSION: 1.1.0
#include<RegistraBDARA.h>

int main(int narg,char* argv[])
{
   int error;

   RegistraBDARA regARA;

   if ( argv[1] == NULL )
   {
      printf("Uso : N�mero de ARA\n");
      return 0;
   }	   
   error =  regARA.Inicia(atoi(argv[1]));
   if(error)
   {
      regARA.Finaliza();	   
      printf("Error en Inicia: %d ", error );
      return error;
   }
   regARA.LeeDatos();
   regARA.Finaliza();
   return 0;
}

