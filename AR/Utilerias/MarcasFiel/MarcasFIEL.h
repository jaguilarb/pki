#ifndef _MARCASFIEL_H_
#define _MARCASFIEL_H_
static const char* _MARCASFIEL_H_VERSION_ ATR_USED = "MarcasFiel @(#)"\
"DSIC09041AR_ 2009-03-20 MarcasFIEL.h 1.1.0/1";


//#VERSION: 1.1.0

#include <Sgi_Bitacora.h>
#include <stdio.h>
#include <string>
#include <unistd.h>

#ifdef DBG
   #define PATH_LOG "/var/local/SAT/PKI/MarcasFIEL_dbg.log"
#else
   #define PATH_LOG "/var/local/SAT/PKI/MarcasFIEL.log"
#endif

std::string path_cfg;
CBitacora *Bitacora;

bool procesaArgumentos(int argc, char **argv);
void uso();
bool inicializaEntorno();

#endif //_MARCASFIEL_H_
