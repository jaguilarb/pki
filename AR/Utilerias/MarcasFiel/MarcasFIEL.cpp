static const char* _MARCASFIEL_CPP_VERSION_ ATR_USED = "MarcasFiel @(#)"\
"DSIC09041AR_ 2009-03-20 MarcasFIEL.cpp 1.1.0/1";

//#VERSION: 1.1.0
#include "MarcasFIEL.h"
#include "CMarcasFIEL.h"

int 
main(int argc, char **argv)
{
   if( argc > 1 )
   {
      if( inicializaEntorno() )
      {
         Bitacora->escribe(BIT_INFO, "Inicio del proceso de marcas FIEL.");
         if( procesaArgumentos(argc, argv) )
         {
            CMarcasFiel marcas( (char *)path_cfg.c_str(), *Bitacora );
            if( marcas.inicializa() )
            {
               if( marcas.procesoGeneral() )
                  Bitacora->escribe(BIT_INFO, "Fin del proceso de marcas.");
               else
                  Bitacora->escribe(BIT_ERROR, "Fin del proceso de marcas.");
            }
         }
         else
            Bitacora->escribe(BIT_ERROR, "Los argumentos pasados a la aplicación son incorrectos.");
      }
      else
         printf("No se pudo iniciar el entorno\n");
   }
   else
      uso(); 
   return 0;
}

void
uso()
{
   printf("\n\tUso: MarcasFIEL -c ARCHIVO_DE_CONFIGURACION\n\n"); 
}

bool
inicializaEntorno()
{
   Bitacora = new CBitacora(PATH_LOG);
   if( Bitacora )
      return true;
   return false;
}

bool 
procesaArgumentos(int argc, char **argv)
{
   while ( 1 )
   {
      int argumento = getopt( argc, argv, "c:" );
      if( argumento == -1 )
         break;
      else
      {
         switch ( argumento )
         {
            case 'c' :
               path_cfg = optarg; 
               break;
            default :
               break;
         }
      }
      if( !(strlen(path_cfg.c_str()) > 0) )
      { 
         printf("Uso incorrecto del comando: \n"
               "\tUSO: MarcasFiel -c ARCHIVO_DE_CONFIGURACION.\n"); 
         return false;
      }
   }
   return true;
}

