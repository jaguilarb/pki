#ifndef _CMARCASFIEL_H_ 
#define _CMARCASFIEL_H_ 
static const char* _CMARCASFIEL_H_VERSION_ ATR_USED = "MarcasFiel @(#)"\
"DSIC09041AR_ 2009-03-20 CMarcasFIEL.h 1.1.0/1";


//#VERSION: 1.1.0

#include <Sgi_ConfigFile.h>
#include <Sgi_ProtegePwd.h>
#include <Sgi_Bitacora.h>
#include <Sgi_LDAP.h>
#include <dirent.h>
#include <iostream>
#include <errno.h>
#include <vector>

class CMarcasFiel
{
   protected :
      CConfigFile config;
      CBitacora Bitacora;
      Sgi_LDAP sgi_ldap;
      std::string s_dmarcas, s_ldap_user, 
                  s_ldap_pass, s_ldap, 
                  s_ldap_pto, s_dn;
      std::vector<std::string> archivos;
      DIR *directorio;
      char *ruta_dir;
      const char *atributos[2];
      char *busqueda;
      char edo_actual;
      char *cDN;

   public :
      CMarcasFiel(const char *cfg_path, CBitacora &bit);
      ~CMarcasFiel();
      bool inicializa();
      bool procesaDirectorio();
      bool conectaLDAP();
      bool desconectaLDAP();
      bool actualizaLDAP(const char *rfc, const char *flag);
      bool consultaLDAP(const char *rfc, char &marca_fiel);
      bool procesaMarca(std::string s_archivo);
      bool procesoGeneral();

};

#endif //_CMARCASFIEL_H_
