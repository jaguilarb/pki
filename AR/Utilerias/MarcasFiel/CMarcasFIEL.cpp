static const char* _CMARCASFIEL_CPP_VERSION_ ATR_USED = "MarcasFiel @(#)"\
"DSIC09041AR_ 2009-03-20 CMarcasFIEL.cpp 1.1.0/1";

//#VERSION: 1.1.0
#include "CMarcasFIEL.h"
#include <sys/stat.h>
#include <fcntl.h>

CMarcasFiel::
CMarcasFiel(const char *cfg_path, CBitacora &bit) :
   config(cfg_path),
   Bitacora(bit),
   sgi_ldap(bit)
{
   busqueda = new char[64];
   cDN = new char[256];
   archivos = std::vector<std::string>(); 
   atributos[0] = "SATMarcaFIEL";
   atributos[1] = NULL;
}

CMarcasFiel::
~CMarcasFiel()
{
   delete [] busqueda;
   busqueda = NULL;

   delete [] cDN;
   cDN = NULL;
}

bool CMarcasFiel::
inicializa()
{
   int error = 0;
   if( !(error = config.cargaCfgVars()) )
   {
      bool retorno = true;
      if( config.getValorVar("[MARCAS_FIEL]", "DIRMARCAS",  &s_dmarcas) )
      {
         Bitacora.escribe(BIT_ERROR, "No se pudo cargar la variable \"DIRMARCAS\" desde el archivo de configuración.");
         retorno = false;
      }
      if( config.getValorVar("[MARCAS_FIEL]", "LDAP_HOST",  &s_ldap) )
      {
         Bitacora.escribe(BIT_ERROR, "No se pudo cargar la variable \"LDAP_HOST\" desde el archivo de configuración.");
         retorno = false;
      }
      if( config.getValorVar("[MARCAS_FIEL]", "LDAP_PTO",   &s_ldap_pto) )
      {
         Bitacora.escribe(BIT_ERROR, "No se pudo cargar la variable \"LDAP_PTO\" desde el archivo de configuración.");
         retorno = false;
      }
      if( config.getValorVar("[MARCAS_FIEL]", "USUARIO",    &s_ldap_user) )
      {
         Bitacora.escribe(BIT_ERROR, "No se pudo cargar la variable \"USUARIO\" desde el archivo de configuración.");
         retorno = false;
      }
      if( config.getValorVar("[MARCAS_FIEL]", "PASSWORD",   &s_ldap_pass) )
      {
         Bitacora.escribe(BIT_ERROR, "No se pudo cargar la variable \"PASSWORD\" desde el archivo de configuración.");
         retorno = false;
      }
      if( config.getValorVar("[MARCAS_FIEL]", "BASE_DN",    &s_dn) )
      {
         Bitacora.escribe(BIT_ERROR, "No se pudo cargar la variable \"BASE_DN\" desde el archivo de configuración.");
         retorno = false;
      }
      return retorno;
   }
   Bitacora.escribe(BIT_ERROR, error, "No se pudieron cargar las variables desde el archivo en la memoria.");
   return false;
}

bool CMarcasFiel::
procesaDirectorio()
{
   ruta_dir = (char *)s_dmarcas.c_str();
   directorio = opendir( ruta_dir );
   if( directorio )
   {
      Bitacora.escribePV(BIT_INFO, "Proceso del directorio %s.", ruta_dir);
      dirent *entrada = NULL;
      while( (entrada = readdir(directorio)) != NULL )
      {
         if( entrada->d_name[0] != '.' ) 
            archivos.push_back(std::string(entrada->d_name));
      }
      closedir(directorio);
      //if( (archivos.size() - 2) > 0 )
      if( archivos.size() > 0 )
      {
         Bitacora.escribePV(BIT_INFO, "Se procesaran las marcas de %d archivos.", archivos.size());
      }
      else
      {
         Bitacora.escribe(BIT_INFO, "No hay marcas en el directorio.");
         archivos.clear();
      }
      return true;
   }
   else
      Bitacora.escribePV(BIT_ERROR, "No se pudo abrir el directorio (%d)", errno);
   return false;
}

bool CMarcasFiel::
conectaLDAP()
{
   int pass_len = 256;
   uint8 *pass = new uint8[pass_len];
   memset(pass, 0, pass_len);
   if( desencripta( (const uint8 *)s_ldap_pass.c_str(), s_ldap_pass.size(), pass, &pass_len ) )
   {
      int pto = atoi(s_ldap_pto.c_str());
      return sgi_ldap.Conexion(s_ldap, pto, s_ldap_user, std::string((char *)pass));      
   }
   return false; 
}

bool CMarcasFiel::
desconectaLDAP()
{
   sgi_ldap.Desconexion();
   return true;
}

bool CMarcasFiel::
actualizaLDAP(const char *rfc, const char *flag)
{
   return sgi_ldap.actualizacion( rfc, atributos[0], flag ); 
}

bool CMarcasFiel::
consultaLDAP(const char *rfc, char &marca_fiel)
{
   char *respuesta[1];
   if( sgi_ldap.consulta( rfc, atributos, respuesta ) )
   {
      if( strlen(respuesta[0]) == 1 )
         marca_fiel = *respuesta[0];
      else
      {
         respuesta[0] = "Indefinido (contiene datos no validos)";
         marca_fiel = 'X';
      }
      Bitacora.escribePV(BIT_INFO, "El directorio contiene %s:%s", atributos[0], respuesta[0]);
      return true;
   }
   return false;
}

bool CMarcasFiel::
procesaMarca(std::string s_arc)
{
   bool retorno = false;
   std::string s_arch_marca = s_dmarcas + "/" + s_arc; 
   FILE *archivo = fopen((char *)s_arch_marca.c_str(), "r");
   if( archivo )
   {
      char *f_flag = new char[2];
      f_flag[1] = 0;
      if( (fread(f_flag, sizeof(char), 1, archivo)) == 1 )
      {
         if( consultaLDAP( (char *)s_arc.c_str(), edo_actual ) )
         {
            if ( edo_actual == f_flag[0] )
            {
               Bitacora.escribePV(BIT_INFO, "No se actualizo la marca para el RFC %s LDAP:%c / ARCHIVO:%s", 
                                    s_arc.c_str(), edo_actual, f_flag );
               return true;
            }
            return actualizaLDAP((char *)s_arc.c_str(), f_flag);
         }
      }
      else
         Bitacora.escribePV(BIT_ERROR, "No se pudo leer del archivo de marcas %s. errno: %d", s_arc.c_str(), errno);
      fclose(archivo);
   }
   else
      Bitacora.escribePV(BIT_ERROR, "No se pudo abrir el archivo de marcas %s. errno: %d", s_arch_marca.c_str(), errno );
   return retorno;
}

bool CMarcasFiel::
procesoGeneral()
{
   if( procesaDirectorio() )
   {
      if( !archivos.empty() )
      {
         if( conectaLDAP() )
         {
            unsigned int i=0;
            for( i=0; i < archivos.size(); i++ )
            {
               if( procesaMarca(archivos[i]) )
               {
                  Bitacora.escribePV(BIT_INFO, "Se borrara el archivo %s", (char *)(s_dmarcas+"/"+archivos[i]).c_str());
                  if( ( unlink((char *)(s_dmarcas+"/"+archivos[i]).c_str()) ) == -1 )
                     Bitacora.escribePV(BIT_ERROR, "No se pudo borrar el archivo. (errno: %d)", errno);
               }
               else
                  Bitacora.escribePV(BIT_ERROR, "No se pudo procesar la marca %s.", archivos[i].c_str());
            }
         }
         desconectaLDAP();
      }
      return true;
   }
   return false;
}
