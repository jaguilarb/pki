static const char* _PRINCIPAL_CPP_VERSION_ ATR_USED = "RegistraDARIO @(#)"\
"DSIC07412AR_ 2007-12-14 Principal.cpp 1.1.0/0";

//#VERSION: 1.1.0
#include <RegistraDario.h>

int main()
{
   int error;

   RegistraDario regDario;

   error =  regDario.Inicia();
   if(error)
      return error;
   regDario.LeeDatos();
   regDario.Finaliza();
   
   return error;

}

