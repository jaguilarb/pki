static const char* _REGISTRADARIO_CPP_VERSION_ ATR_USED = "RegistraDARIO @(#)"\
"DSIC09413AR_ 2009-03-27 RegistraDario.cpp 1.1.1/0";

//#VERSION: 1.1.1
//   V.1.1.1  (20090327)    MAML: Se corrige la llamada a los Stores Procedures de Plataforma (*_crm)

//#include <Sgi_ConfigFile.h>  //+ GHM (070503)
#include <RegistraDario.h>

//######################################################################

#if ORACLE
  #define SP_CERTIF_CRM             "SEG_NEGOCIO.segsf_act_certif_crm"
#else
  #define SP_CERTIF_CRM             "spact_certif_crm"
#endif

#define QE_PROCEDURE_CERTIF_CRM   "EXECUTE PROCEDURE SEG_NEGOCIO.segsf_act_certif_crm('%s','%s')"


//######################################################################

RegistraDario  ::
RegistraDario  ()
{
   bdDario = NULL;
   log     = NULL;
   logbd   = NULL;
}

RegistraDario  ::
~RegistraDario()
{
   if (bdDario)
   {
      delete bdDario;   
      bdDario = NULL;
   }
   if (log)
   {
      delete log;
      log     = NULL;
   }
   if (logbd)
   {
      delete log;
      logbd   = NULL;
   }
}

int RegistraDario::
LeeDatos()
{
   DIR  *direc;
   //char cert[256]; 
   char lect[3]; 
   char borra[256]; 
   char edo_cert[2];
   char rfc_res[14];
   int desc;
   int error; 
   struct dirent *lista_nom; 

   memset(rfc_res  ,0 , sizeof(rfc_res));
   memset(edo_cert ,0 , sizeof(edo_cert));
   memset(lect     ,0 , sizeof(lect));

   direc = opendir( ruta_pend.c_str() );
   if(direc == NULL)
   {
      log->escribe(BIT_ERROR,"Error al abrir el Directorio");
      return -1;
   }

   while((lista_nom = readdir(direc))!= NULL )
   {
  
      if( !strcmp(lista_nom[0].d_name,".") )
        continue;
      if( !strcmp(lista_nom[0].d_name,".."))
        continue;

      sprintf(borra,"%s%s", ruta_pend.c_str(), lista_nom[0].d_name );

      desc = open(borra,O_RDONLY);      
      if(!desc)
         continue;
      error = read(desc,lect,sizeof(lect) ); 
      if(error <= 0)
         continue;
      close(desc);

      strncpy(edo_cert,lect,1);

      MoraloFisica(lista_nom[0].d_name ,rfc_res);
     
      error = RegistraBD(rfc_res,edo_cert);
      if(!error)
      {
        memset(borra,0,sizeof(borra)); 
        sprintf(borra,"%s%s", ruta_pend.c_str(), lista_nom[0].d_name );
        close(desc);
        unlink(borra);
      }    
      else
        close(desc);
   } // END del WHILE
   if ( error )
      log->escribePV( BIT_ERROR, "Fin de directorio, error=%d", error); 
   closedir(direc);
   direc = NULL;
   lista_nom = NULL;
   //delete lista_nom; // MAML 070621: No hay new

   return error;
}

int RegistraDario::
RegistraBD(char* rfc,char *edo_cert)
{
   int error;

   static char sentencia[1024];
   char  resp[1];
   memset(sentencia,0,sizeof(sentencia));

   sprintf(sentencia,QE_PROCEDURE_CERTIF_CRM,rfc,edo_cert);

   bool ok_ejecuta = false;

   #if ORACLE
      ok_ejecuta = bdDario->ejecutaFuncion(SP_CERTIF_CRM, ":b", "'%s','%s'", rfc, edo_cert);
   #else
      ok_ejecuta = bdDario->ejecutaSP(SP_CERTIF_CRM, "'%s','%s'", rfc, edo_cert);
   #endif
   
    if ( !ok_ejecuta )
   {
      log->escribe(BIT_ERROR,"Error al ejecutar el Procedimiento");
      return  error;
   }
   else
   {
      if( bdDario->getValores("b", resp) )
      {   
         memset(sentencia,0,sizeof(sentencia)); 
         switch(atoi(resp)) 
         {
            case 0:
                       sprintf(sentencia,"RFC no existe  %s", rfc); 
                       log->escribe(BIT_ERROR,sentencia);
                       return 10;
                       break;
            case 1:
                       sprintf(sentencia,"RFC registrado %s", rfc);
                       log->escribe(BIT_INFO, sentencia);
                       return 0;
                       break;
            case 2:
                       sprintf(sentencia,"RFC sin biometricos %s", rfc);
                       log->escribe(BIT_ERROR, sentencia);
                       return atoi(resp);
                       break;
         }
      }  
      return 10;
   }
}

/* >>> - GHM (070503): Se reestructura funci�n para utilizar la librer�a Sgi_ConfigFile
int RegistraDario::
Inicia()
{
  int error;
   ArchivosConfg cfg;

   char BD_Dario         [20];
   char USUARIO_Dario    [10];
   char INSTANCIA_Dario  [20];
   char PASSBD_Dario     [20];
   char ROLE_Dario       [20];
   char passwd           [20];

   int  lpasswd = sizeof(PASSBD_Dario );

   memset(passwd          ,0  ,sizeof(passwd));

   char* variables[]={"BD_DARIO","USUARIO_DARIO","INSTANCIA_DARIO","PASSBD_DARIO","ROLE_DARIO","RUTA_PEND"};

   error =  cfg.VarConfg(6,variables,"/usr/local/SAT/PKI/etc/RegistraDario.cfg",
                                     BD_Dario ,USUARIO_Dario ,INSTANCIA_Dario ,
                                     PASSBD_Dario ,ROLE_Dario ,RUTA_PEND);
   if (error)
      return error;

   desencripta((uint8*)PASSBD_Dario ,strlen(PASSBD_Dario ),(uint8*)passwd,&lpasswd);

   log   = new CBitacora("/var/local/SAT/PKI/RegistraDario.log");
   logbd = new CBitacora("/var/local/SAT/PKI/RegistraDario_BD.log" );

   if( !(bdDario = new CBD(logbd)  ))
   {
      log->escribe(BIT_ERROR,"No se pudo asignar memoria al objeto");
      return -1;
   }
   if ( !bdDario->setConfig(INSTANCIA_Dario, BD_Dario ,USUARIO_Dario ,passwd,ROLE_Dario))
   {
      log->escribe(BIT_ERROR,"No se pudo setConfig");
      return -2;
   }

   if ( !bdDario->conectaGral() )
   {
      log->escribe(BIT_ERROR,"No se pudo conectar a la BD");
      return -3;
   }
   log->escribe(BIT_ERROR,"Iniciado");
   return error;
}
  >>> - GHM (070503) */


int RegistraDario::
Inicia()
{
   int error;
   
   CConfigFile Configuracion(ARCH_CONF);

   log           = new CBitacora(PATH_BITA);   
   logbd         = new CBitacora(PATH_BITABD);

   if (log == NULL || logbd == NULL)
   {
      return ERR_MEMORIA;
   }

   error = Configuracion.cargaCfgVars(); 

   if (error)
   {
      log->escribe(BIT_ERROR,"No se leyeron las variables del archivo de configuracion");

      return error;
   }

   if( !(bdDario = new CBD(logbd)  ))
   {
      log->escribe(BIT_ERROR,"No se pudo asignar memoria al objeto bdDario");

      return -1;
   }

   if ( !ConfiguraBD(&Configuracion) ) 
   {
      log->escribe(BIT_ERROR,"Error al configurar los par�metros de conexi�n a la base de datos");

      return -2;
   }

   bool ok_conecta = false;

   #if ORACLE
      ok_conecta = bdDario->conecta();
   #else
      ok_conecta = bdDario->conectaGral();
   #endif

   if ( !ok_conecta )
   {
      log->escribe(BIT_ERROR,"No se conect� a la BD");

      return -3;
   }

   #ifdef DBG
      log->escribe(BIT_INFO, "Iniciado");
   #endif
   
   return error;
}


bool RegistraDario::ConfiguraBD(CConfigFile* Configuracion)
{
    string BDatos, instancia, rl; // Informix
    string host, puerto, sid;     // Oracle
    string usuario, PasswdBD;     // Oracle e Informix
    
    int r1 = 0, r2 = 0 , r3 = 0, r4 = 0, r5 = 0, r6 = 0;

    char passwd[128];
    int  lpasswd = sizeof(passwd);
   
    memset(passwd, 0, sizeof(passwd));

    #if ORACLE
      r1 = Configuracion->getValorVar("[BD_DARIO_ORACLE]"    , "HOST"  ,    &host);
      r2 = Configuracion->getValorVar("[BD_DARIO_ORACLE]"    , "PORT"  ,    &puerto);
      r3 = Configuracion->getValorVar("[BD_DARIO_ORACLE]"    , "SID"   ,    &sid);
      r4 = Configuracion->getValorVar("[BD_DARIO_ORACLE]"    , "USR"   ,    &usuario);
      r5 = Configuracion->getValorVar("[BD_DARIO_ORACLE]"    , "PWD"   ,    &PasswdBD);
      r6 = Configuracion->getValorVar("[RUTA_PEND]"          , "RUTA"  ,    &ruta_pend);
    #else
      r1 = Configuracion->getValorVar("[BD_DARIO]" , "SERVIDOR", &instancia);
      r2 = Configuracion->getValorVar("[BD_DARIO]" , "BD"      , &BDatos   );
      r3 = Configuracion->getValorVar("[BD_DARIO]" , "USUARIO" , &usuario  );
      r4 = Configuracion->getValorVar("[BD_DARIO]" , "PASSWORD", &PasswdBD );
      r5 = Configuracion->getValorVar("[BD_DARIO]" , "ROLE"    , &rl       );
      r6 = Configuracion->getValorVar("[RUTA_PEND]", "RUTA"    , &ruta_pend);
    #endif

    if (r1 || r2 || r3 || r4 || r5 || r6)
    {
      log->escribePV(BIT_ERROR, "Error al leer los par�metros de configuraci�n (%d,%d,%d,%d,%d,%d)", r1, r2, r3, r4, r5, r6 );
      
      return false;
    }

    #ifdef DBG
      #if ORACLE
        log->escribePV(BIT_INFO, "Los par�metros de configuraci�n son: Host(%s), Puerto(%s), Sid(%s), Usuario(%s), ruta_pend(%s)", host.c_str(), puerto.c_str(), sid.c_str(), usuario.c_str(), ruta_pend.c_str());
      #else
        log->escribePV(BIT_INFO, "Los par�metros de configuraci�n son: Srv(%s), BD(%s), usuario(%s), role(%s), ruta_pend(%s)", instancia.c_str(), BDatos.c_str(), usuario.c_str(), rl.c_str(), ruta_pend.c_str());
      #endif
    #endif

    if (!desencripta((uint8*)PasswdBD.c_str(), PasswdBD.length(), (uint8*)passwd, &lpasswd))
    {
      log->escribePV(BIT_ERROR, "Fall� la funci�n desencripta( PasswdBD(%s), length(%d), passwd(%s), lpasswd(%d) ", PasswdBD.c_str(), PasswdBD.length(), passwd, lpasswd );
      
      return false;
    }

    bool ok_configura = false;

    #if ORACLE
      ok_configura = bdDario->setConfig(usuario.c_str(), passwd, host.c_str(), puerto.c_str(), sid.c_str() );
    #else
      ok_configura = bdDario->setConfig(instancia.c_str(), BDatos.c_str(), usuario.c_str(), passwd, rl.c_str() );
    #endif

    if ( !ok_configura ) 
    {
      log->escribe(BIT_ERROR,"No se han podido configurar los par�metros de conexi�n para la base de datos");
      
      return false;
    }

    return true;
}


void  RegistraDario::
Finaliza()
{
   bdDario->desconecta();
}

void RegistraDario::
MoraloFisica(char *rfc ,char *rfc_res)
{
   //- char RFC[TAM_RUTAS]; //-
   char RFC[30];
   char rfc_aux[13];
   memset(RFC,0,sizeof(RFC));

   memcpy(RFC,rfc,strlen(rfc));
   char * rfc_ = strtok(RFC,"|");

   if(strlen(rfc_)== 12 ) // Moral
   {
      sprintf(rfc_aux," %s",RFC);
      strcpy(rfc_res,rfc_aux);
   }
   else if(strlen(RFC) == 13) // Fisica
      strcpy(rfc_res,rfc_);
}


