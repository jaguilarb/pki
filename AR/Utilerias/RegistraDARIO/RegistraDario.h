#ifndef _REGISTRADARIO_H_
#define _REGISTRADARIO_H_
static const char* _REGISTRADARIO_H_VERSION_ ATR_USED = "RegistraDARIO @(#)"\
"DSIC07412AR_ 2007-12-14 RegistraDario.h 1.1.0/0";

//#VERSION: 1.1.0
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/uio.h>
#include <strings.h>
#include <unistd.h>
#include <iostream>
#include <dirent.h>
//- #include <RutaCerts.h>  //- GMH (070503)
#include <SgiTipos.h>
#include <Sgi_PKI.h>        //+ GMH (070503)
#include <Sgi_Bitacora.h>
//- #include <SgiOpenSSL.h> //- GMH (070503)
#include <Sgi_OpenSSL.h>    //+ GMH (070503)

#if ORACLE
  //
#else
  #include <mitypes.h>
#endif

#include <Sgi_BD.h>
//- #include <PwdProtection.h>  //- GMH (070503)
#include <Sgi_ProtegePwd.h>     //+ GMH (070503)
//- #include <Fechas.h>         //- GMH (070503)
#include <Sgi_Fecha.h>         //+ GMH (070503)
#include <Sgi_ConfigFile.h>  //+ GHM (070503)


//>>>+ GHM (070503) : Se agreg+o para unificar el acceso a los arhivos
#define PATH_CONF               "/usr/local/SAT/PKI/etc/"
#define PATH_DBIT               "/var/local/SAT/PKI/"

#ifdef DBG
   #define ARCH_CONF            PATH_CONF "RegistraDario_dbg.cfg"
   #define PATH_BITA            PATH_DBIT "RegistraDario_dbg.log"
   #define PATH_BITABD          PATH_DBIT "RegistraDario_BD_dbg.log"
#else
   #define ARCH_CONF            PATH_CONF "RegistraDario.cfg"
   #define PATH_BITA            PATH_DBIT "RegistraDario.log"
   #define PATH_BITABD          PATH_DBIT "RegistraDario_BD.log"
#endif
//<<<+ GHM (070503)

#define  TAM_VAR            50
#define  ERR_NO_LECT_CONF   -10
#define  ERR_MEMORIA        -20

struct DATOS
{
   char cont[TAM_VAR];
   int  lcont;
};

struct DatCert
{
  char operacion[1];
  char num_serie[20];
  char estado[1];
  char tipCert[1];
  char fec_ini[19];
  char fec_fin[19];
  char rfc[13]; 
 
};



class RegistraDario
{
  CBD        *bdDario;
  CBitacora  *log ,*logbd;
  string     ruta_pend;

  bool ConfiguraBD(CConfigFile* Configuracion);

  public:

  RegistraDario();
  virtual ~RegistraDario();
  
  int  Inicia();
  int  RegistraBD( char * rfc ,char *edo_cert);
  int  LeeDatos();
  void Finaliza();
  int  Actualiza();
  void MoraloFisica(char *rfc ,char *rfc_res);


};


#endif
