#ifndef _PUBLICASELLOS_H_
#define _PUBLICASELLOS_H_
static const char* _PUBLICASELLOS_H_VERSION_ ATR_USED = "PublicaSellos @(#)"\
"DSIC07412AR_ 2007-12-14 PublicaSellos.h 1.1.0/0";

//#VERSION: 1.1.0
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/uio.h>
#include <strings.h>
#include <unistd.h>
#include <iostream>
#include <dirent.h>
//>>> ERGL jue mar 22 12:42:18 CDT 2007
//#include <RutaCerts.h>
//#include <ArchivosLog.h>
//#include <CConfigFile.h> //- GHM (070430)
#include <Sgi_ConfigFile.h>
#include <Sgi_Bitacora.h>
#include <SockSeg.h>
#include <Sgi_PKI.h>
#include <string>
//<<<

//>+ ERGL jue mar 22 12:49:07 CDT 2007
#define TAM_RUTAS 1024
#define EXITO 0

#define PATH_CONF               "/usr/local/SAT/PKI/etc/"
#define PATH_DBIT               "/var/local/SAT/PKI/"

#ifdef DBG
   #define ARCH_CONF            PATH_CONF "PublicaSellos_%02d_dbg.cfg"
   #define PATH_BITA            PATH_DBIT "PublicaSellos_%02d_dbg.log"
#else
   #define ARCH_CONF            PATH_CONF "PublicaSellos_%02d.cfg"
   #define PATH_BITA            PATH_DBIT "PublicaSellos_%02d.log"
#endif


#endif
