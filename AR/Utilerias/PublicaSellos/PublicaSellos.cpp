static const char* _PUBLICASELLOS_CPP_VERSION_ ATR_USED = "PublicaSellos @(#)"\
"DSIC07412AR_ 2007-12-14 PublicaSellos.cpp 1.1.0/0";

//#VERSION: 1.1.0
#include<PublicaSellos.h>


int PendientesFTP(char * ruta, char * nomb_cert)
{
   char  nombreA[TAM_RUTAS];
   sprintf(nombreA,"%s%s.cer",ruta,nomb_cert);
   FILE *archivo;

   archivo = fopen(nombreA,"a");
   if(archivo == NULL)
      return   3;
   fclose(archivo);
   return EXITO;

}

int getOS(const char* path, int tam){
   int i = 0;
   int so = -1;
   for (i = 0; i < tam; i++){
      if (path[i] == '\\'){
         so = WINDOWS;
         break;
      }
      else if (path[i] == '/'){
         so = LINUX;
         break;
      }
      else continue;
   }
   return so;
}

int Ftp_Sellos(int num_ftp, int sisOP)
{
   int error;
   //>>> ERGL jue mar 22 12:53:39 CDT 2007 ArchivosConfg cfg;
   CConfigFile *cfg = NULL;
   int os = sisOP;

   /*>- ERGL jue mar 22 12:57:08 CDT 2007
   char IP_LOCAL  [20];
   char IP_DES    [20];
   char USU_LOCAL [10];
   char USU_REM   [10];
   char DIR_REP   [TAM_RUTAS];
   char DIR_DES   [TAM_RUTAS];
   char NO_ENV    [TAM_RUTAS];
   */
   std::string s_ip_local;
   std::string s_ip_des;
   std::string s_usu_local;
   std::string s_usu_rem;
   std::string s_dir_rep;
   std::string s_dir_des;
   std::string s_no_env;

   char mens_err  [1204];

   char ruta[TAM_RUTAS];
   char ruta_nombre[TAM_RUTAS];
   //>- char ruta_com[TAM_RUTAS]; //ERGL jue mar 22 14:37:27 CDT 2007
   std::string s_ruta_com;
   char comando[TAM_RUTAS];
   char archConfFTP[1024];
   char archLogFTP[1024];
   char borrar[TAM_RUTAS];
   char rutaNoEnviados[TAM_RUTAS];

   DIR    *direc;
   struct dirent *lista_nom;
   
   //>- ERGL char* jue mar 22 13:03:20 CDT 2007 variables[]={"IP_LOCAL","USU_LOCAL","DIR_REP","IP_DES","USU_REM","DIR_DES","NO_ENV"};

   memset(archConfFTP,0,sizeof(archConfFTP));
   memset(archLogFTP,0,sizeof(archLogFTP));
   memset(rutaNoEnviados, 0 ,sizeof(rutaNoEnviados));
   /*>- ERGL jue mar 22 13:04:47 CDT 2007
   memset(IP_LOCAL,0,sizeof(IP_LOCAL));
   memset(USU_LOCAL ,0 ,sizeof(USU_LOCAL));
   memset(DIR_REP , 0, sizeof(DIR_REP));
   memset(IP_DES,0,sizeof(IP_DES));
   memset(USU_REM,0,sizeof(USU_REM));
   memset(DIR_DES,0,sizeof(DIR_DES));
   memset(NO_ENV,0,sizeof(NO_ENV));
   -<*/

      sprintf(archConfFTP, ARCH_CONF, num_ftp );
      sprintf(archLogFTP , PATH_BITA, num_ftp );
      cfg = new CConfigFile(archConfFTP);
      if(cfg == NULL)
      {
         printf("No se pudo abrir el archivo de configuración\n");
         return -1;
      }
      
      CBitacora log(archLogFTP);
 
      //>- ERGL jue mar 22 13:16:23 CDT 2007
      //error =  cfg.VarConfg(7,variables,archConfFTP,IP_LOCAL,USU_LOCAL,DIR_REP,IP_DES,USU_REM,DIR_DES,NO_ENV);
      error = cfg->cargaCfgVars(); 
      if(error)
      {
         log.escribe(BIT_INFO,"Error al leer las variables del archivo de configuración");
         return error;
      }

      cfg->getValorVar("[GENERAL]", "IP_LOCAL", 	&s_ip_local);
      cfg->getValorVar("[GENERAL]", "USU_LOCAL", 	&s_usu_local);
      cfg->getValorVar("[GENERAL]", "DIR_REP", 		&s_dir_rep);
      cfg->getValorVar("[GENERAL]", "IP_DES", 		&s_ip_des);
      cfg->getValorVar("[GENERAL]", "USU_REM", 		&s_usu_rem);
      cfg->getValorVar("[GENERAL]", "DIR_DES", 		&s_dir_des);
      cfg->getValorVar("[GENERAL]", "NO_ENV", 		&s_no_env);

      //Determinar el SO a partir de la ruta destino por el serparador de directorio
      /*if (s_dir_des.find_first_of("/") >= 0){
         os = LINUX;
      }
      else if (s_dir_des.find_first_of("\\") >= 0){
         os = WINDOWS;
      }
      else {
         os = LINUX;
      }*/
      os = getOS(s_dir_des.c_str(), s_dir_des.size());
      if (os == -1){
         os = WINDOWS;
      }
      //Fin determinar la ruta

      // MAML 070621 : libreramos la memoria q ya no se usa
      delete cfg;
      cfg = NULL;      
   
      //>- ERGL jue mar 22 13:55:24 CDT 2007 sprintf(rutaNoEnviados,"%sFTPPUB%d/",NO_ENV,num_ftp);
      sprintf(rutaNoEnviados, "%sFTPPUB%02d/", s_no_env.c_str(), num_ftp);
    
   direc = opendir(rutaNoEnviados);
   if(!direc)
   {
      log.escribePV(BIT_ERROR,"Error al abrir el directorio %s ",rutaNoEnviados);	   
      return error;
   }
#ifdef DBG
   log.escribePV(BIT_INFO,"Inicio de directorio %s",rutaNoEnviados);
#endif

   //>- ERGL jue mar 22 14:00:04 CDT 2007 
   //RutaCerts rtramite; 
   char numop[13]; 
   
   std::string s_numop;
   memset(numop,0,sizeof(numop));

   while( (lista_nom = readdir(direc)) != NULL )
   {
      memset(borrar,0,sizeof(borrar));	   
      memset(ruta,0,sizeof(ruta));
      memset(ruta_nombre,0,sizeof(ruta_nombre));
      //memset(ruta_com,0,sizeof(ruta_com));
      memset(comando,0,sizeof(comando));

     if( strcmp(lista_nom[0].d_name,".") == 0 )
       continue;
     if(strcmp(lista_nom[0].d_name,"..") == 0)
       continue;

      memcpy(numop, lista_nom[0].d_name, 12); // ERGL jue mar 22 14:31:30 CDT 2007
      s_numop = numop;                        //>- = lista_nom[0].d_name; 070621 MAML : solo se copia el Num. Tramite

      //>- rtramite.RutaNumTram(DIR_DES,numop,ruta_com,false,2); //ERGL jue mar 22 13:53:10 CDT 2007
      error = pkiDirReq(s_dir_des, s_numop, s_ruta_com, false, os); 
      
      if (error)
      {
         log.escribePV(BIT_ERROR,"No puede construirse la ruta del repositorio FTP %d", error );
         break;
      }
      
      //>- sprintf(ruta,"%s%s.zip",DIR_REP,numop); // ERGL jue mar 22 13:25:27 CDT 2007 
      sprintf(ruta,"%s%s.zip",s_dir_rep.c_str(),s_numop.c_str());
      //>- sprintf(comando,"mkdir %s",ruta_com); // ERGL jue mar 22 14:39:46 CDT 2007
      if (os == WINDOWS){
         sprintf(comando, "mkdir %s", s_ruta_com.c_str()); 
      }
      else {
         sprintf(comando, "if test ! -d %s; then umask 022; mkdir -p -m 755 %s; fi", s_ruta_com.c_str(), s_ruta_com.c_str());
      }
      //>- sprintf(ruta_com + strlen(ruta_com),"%s.zip",numop); // ERGL jue mar 22 14:39:46 CDT 2007
      s_ruta_com += s_numop + ".zip";   

      //>- ERGL jue mar 22 13:25:27 CDT 2007 error = EjecutaCmdoRem( IP_DES,USU_REM,comando,mens_err  );
      char *c_ipdes = (char *)s_ip_des.c_str();
      char *c_usurem = (char *)s_usu_rem.c_str();
      error = EjecutaCmdoRem( c_ipdes, c_usurem, comando, mens_err );
      if(error)
      {
         if(error != -41)
         {
            log.escribe(BIT_ERROR,mens_err);		  
            break; // MAML 070621: Sale del While p/salir sin problemas de memoria // return error;
         }
      }

      //>- error =  TransmiteArchivo( IP_DES,USU_REM,ruta,ruta_com,mens_err); // ERGL jue mar 22 13:25:27 CDT 2007 
      error =  TransmiteArchivo( c_ipdes, c_usurem, ruta, (char *)s_ruta_com.c_str(), mens_err );
      if(error)
      {
	log.escribe(BIT_INFO,lista_nom[0].d_name);      
	log.escribe(BIT_INFO,mens_err);      
        // PendientesFTP(NO_ENV, numop);// Poner ruta correcta
        break; // MAML 070621: Sale del While p/salir sin problemas de memoria // return error;
      }
      sprintf(borrar,"rm %s%s.zip",rutaNoEnviados,s_numop.c_str());
      system(borrar);
      log.escribePV(BIT_INFO,"Enviado %s",lista_nom[0].d_name);

   } // WHILE
   if (error)
      log.escribePV(BIT_INFO, "Fin de Directorio, error=%d", error);

   closedir(direc);
   direc = NULL;
   lista_nom = NULL; 
   //delete lista_nom; // MAML 070621: El delete NO VA, por que nunca se le hizo new  

   return error;
}

int main(int argc, char* argv[], char* env[])
{
    //AGZ: Se agrega un parametro a la ejecucion del programa: 
    //1: LINUX
    //2: WINDOWS
    if(argv[1] == NULL)
    {
        printf("Uso:Numero de FTP'S \n");
	     return 0; 
    }	    
    int ftp = atoi(argv[1]);
    Ftp_Sellos(ftp, 0);

}
