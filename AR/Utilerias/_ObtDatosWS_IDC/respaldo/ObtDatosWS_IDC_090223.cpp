#ifdef WS_EXTERNO
   #include "IdCSOAP.nsmap"
#else
   #include "IdCInternoSOAP.nsmap"
#endif

#include <Sgi_ConfigFile.h>
#include <Sgi_Bitacora.h>
#include <Sgi_ProtegePwd.h>
#include "CSolDatosWS_IDC.h"
#include "ObtDatosWS_IDC.h"

CBitacora       *Bitacora      = NULL;
CConfigFile     *Configuracion = NULL;
CSolDatosWS_IDC *ObjWS_IDC = NULL;

std::string m_URLWS;

//#################################################################################
#define PATH_CONF            "./"
#define PATH_DBIT            "./"

#ifdef DBG
   #define ARCH_CONF            PATH_CONF "ObtDatosWS_IDC_dbg.cfg"
   #define PATH_BITA            PATH_DBIT "ObtDatosWS_IDC_dbg.log"
#else
   #define ARCH_CONF            PATH_CONF "ObtDatosWS_IDC.cfg"
   #define PATH_BITA            PATH_DBIT "ObtDatosWS_IDC.log"
#endif

// Creaci�n del objeto de Bitacora
CBitacora* Crea_CBitacora()
{
   return new CBitacora(PATH_BITA);
}

void convMayusc( char cadena[])
{
   int longitud = strlen(cadena);
   for ( int i = 0; i < longitud; i++)
      cadena[i] = toupper(cadena[i]);
}

// Valida los par�metros de entrada
bool procesaParametros(int argc, char* argv[])
{
   if ( argc == 2 )
   {
      if ( !(strlen(argv[1]) == 12 || strlen(argv[1]) == 13) )
      {
         std::cout << "La longitud del RFC es inv�lida (" << argv[1] << "), revise el dato." << std::endl;
         return false;
      }
      convMayusc(argv[1]);
      return true;
   }
   else
   {
      std::cout << std::endl << std::endl ;
      std::cout << "Uso: " << std::endl << "\t" << argv[0] << " RFC \n" << std::endl;
      std::cout << "Verifique que el par�metro sea correcto \n\n" << std::endl;
      return false;
   }
}

//Desencripta el pwd
bool procesaPassword(const std::string& s_pwdEnc, std::string *s_pwdDes)
{
   bool b_resOperacion = true;
   int i_pwdSalida = s_pwdEnc.size();
   char *psz_pwdSalida = new char[i_pwdSalida];
   if (!desencripta((uint8*) s_pwdEnc.c_str(), i_pwdSalida, (uint8*) psz_pwdSalida, &i_pwdSalida))
      b_resOperacion = false;
   else
      s_pwdDes->assign(psz_pwdSalida, i_pwdSalida);
   delete[] psz_pwdSalida;
   return b_resOperacion;
}

bool getVarConfWS(std::string *Usr, std::string *Pwd, std::string *conHTTPS, std::string *cert, int *iTpoConsulta)
{
   std::string NvlBitWS;
   std::string PwdWSEnc, TpoCons;
   int r1, r2, r3, r4, r5, r6;
   
   //Establece el nivelde bit�cora
   r1  = Configuracion->getValorVar("[BITACORA]"    , "NIVELBIT" , &NvlBitWS);
   if (r1 != 0)
      NvlBitWS = "3";
   if (Bitacora)
      Bitacora->setNivel( (BIT_NIVEL) atoi(NvlBitWS.c_str()) );
   //Lee los datos del WEB SERVICE IDC
   r1 = Configuracion->getValorVar("[WEB_SERVICE_IDC]" , "USUARIO"    , Usr      );
   r2 = Configuracion->getValorVar("[WEB_SERVICE_IDC]" , "PASSWORD"   , &PwdWSEnc);
   r3 = Configuracion->getValorVar("[WEB_SERVICE_IDC]" , "URLWS"      , &m_URLWS );
   r4 = Configuracion->getValorVar("[WEB_SERVICE_IDC]" , "HTTPS"      , conHTTPS );
   r5 = Configuracion->getValorVar("[WEB_SERVICE_IDC]" , "TPOCONSULTA", &TpoCons );
   //Determina si utiliza el certificado
   if ( conHTTPS->compare("S") == 0)
   {
      r6 = Configuracion->getValorVar("[WEB_SERVICE_IDC]", "CERT", cert);
      if ( r1 || r2 || r3 || r4 || r5 || r6 )
      {
         std::cout << "Problemas al leer la configuraci�n de WS_IDC: " << "usr(" << r1 << "), ";
         std::cout << "Pwd(" << r2 << "), URL(" << r3 << "), conHTTPS(" << r4 << ", TpoCons("  ;
         std::cout << r5 << "), Crt(" << r6 << ")" << std::endl;
         return false;
      }
   }
   else
   {
      if ( r1 || r2 || r3 || r4 || r5 )
      {
         std::cout << "Problemas al leer la configuraci�n de WS_IDC: " << "usr(" << r1 << "),";
         std::cout << "Pwd(" << r2 << "), URL(" << r3 << "), conHTTPS(" << r4 << ", TpoCons(" ; 
         std::cout << r5 << ")" << std::endl;
         return false;
      }
   } 
   if ( m_URLWS.length() == 0 )
   {
      std::cout << "No se recupero la URL correctamente" << std::endl;
      return false;
   }
   if ( !procesaPassword(PwdWSEnc, Pwd) )
   {
      std::cout << "Fall� la desencripci�n del pwd del WS" << std::endl;
      return false;
   }
   *iTpoConsulta = atoi(TpoCons.c_str());
   return true;
}

void borrarPtr()
{
   if (Bitacora)
   {
      delete Bitacora;
      Bitacora = NULL;
   }
   if (Configuracion)
   {
      delete Configuracion;
      Configuracion = NULL;
   }
   if (ObjWS_IDC)
   {
      delete ObjWS_IDC;
      ObjWS_IDC = NULL;
   }
}

std::string ObtLista( int iTpoLista, int iMaxNumElem )
{
   std::string sLista = "";
   std::string sDatoLeido;
   bool activo, seObtuvoDato;
   
   for (int indice=0; indice < iMaxNumElem; indice++)
   {
      seObtuvoDato = false;
      switch (iTpoLista)
      {
         case LISTA_REGIMEN:
            seObtuvoDato = ObjWS_IDC->getRegimen(indice, &sDatoLeido, &activo); break;
         case LISTA_ROLE:
            seObtuvoDato = ObjWS_IDC->getRol(indice, &sDatoLeido, &activo); break;
         case LISTA_OBLIGACION:
            seObtuvoDato = ObjWS_IDC->getObligacion(indice, &sDatoLeido, &activo); break;
         case LISTA_ACTIVIDAD:
            seObtuvoDato = ObjWS_IDC->getActiv(indice, &sDatoLeido, &activo); break;
      }
      if (seObtuvoDato)
         sLista += "|" + sDatoLeido + "," + (activo?"ACTIVO":"DESACTIVADO");
   }
   if ( sLista.length() == 0)
      sLista = "||";
   else
      sLista += "|";
   return sLista;
}

int main(int argc, char* argv[])
{
   std::string     usrWS, pwdWS, conHTTPSWS, certWS;
   int             iTipoCons=0, iError = 0;

   Bitacora       = Crea_CBitacora();
   Configuracion  = new CConfigFile(ARCH_CONF);
   
   if ( !(procesaParametros(argc, argv)) )
     iError = 1; 
   else
   {
      //Lectura de ArchConf
      if ( (iError = Configuracion->cargaCfgVars()) )
      {
         std::cout << "Problemas al leer el archivo de configuraci�n (" << iError << "): " << ARCH_CONF << std::endl;
         iError = 2;
      }
      else
      {
         if ( !getVarConfWS(&usrWS, &pwdWS, &conHTTPSWS, &certWS, &iTipoCons) )
            iError = 3;
         else
         {
            //Genera el objeto para interactuar con el WS_IDC
            ObjWS_IDC = new CSolDatosWS_IDC(argv[1], iTipoCons, usrWS, pwdWS, conHTTPSWS, certWS);
            if ( ObjWS_IDC == NULL)
            {
               std::cout << "Error al generar la interfaz con el WS" << std::endl;
               iError = 4;
            }
            else
            {
               if ( !ObjWS_IDC->InicializaWS() )
               {
                   std::cout << "Error al inicializar la interfaz del WS" << std::endl;
                  iError = 5;
               }
               else
               {
                  if ( (iError = ObjWS_IDC->SolInfoRFCWS(argv[1])) != EXITO )
                  {
                      std::cout << "Error (" << iError << ") al solicitar la informaci�n al WS" << std::endl;
                      iError = 6;
                  }
               }
            }
         }
      }
   }
   if (iError != 0)
   {
      borrarPtr();
      return iError;
   }
   //Variables para extraer datos
   int         lcadena = 30, iClave = 0, iElementos = 0;
   char        cadena[lcadena];
   std::string sDato;
   
   //Despliegue de datos
   //RFC Consultado
   std::cout << "RFC_CONTRIB=     "   << argv[1]   << std::endl;
   //RFC original char(13)
   memset(cadena, 0, lcadena); 
   ObjWS_IDC->getRFCOrig(cadena);
   std::cout << "RFC_ORIGINAL=    "  << cadena << std::endl;
   //StatusRFC
   iClave = ObjWS_IDC->getStatusRFC();
   std::cout << "STATUSRFC_CVE=   "  << iClave << std::endl;
   ObjWS_IDC->getTipoContrib(&sDato);
   std::cout << "TIPO_CONTRIB=    " << sDato.c_str() << std::endl;
   //Nombre
   ObjWS_IDC->getNombre(&sDato);
   std::cout << "NOMBRE_CONTRIB=  "<< sDato.c_str() << std::endl;
   //CURP
   memset(cadena, 0, lcadena);
   ObjWS_IDC->getCURP(cadena);
   std::cout << "CURP=            " << cadena << std::endl;
   //Situaci�n Fiscal
   ObjWS_IDC->getSitFiscal(argv[1], iClave);
   std::cout << "SITFIS_CLAVE=    " << (iClave>0?iClave:' ') << std::endl;
   ObjWS_IDC->getDesSitFis(&sDato);
   std::cout << "SITFIS_DESC=     " << sDato.c_str() << std::endl;
   //Situaci�n Domicilio
   iClave = ObjWS_IDC->getSitDomicilio(argv[1]);
   std::cout << "SITDOMIC_CVE     " << (iClave>0?iClave:' ') << std::endl;
   sDato = ObjWS_IDC->getDescDom();
   std::cout << "SITDOMIC_DESC=   " << sDato.c_str() << std::endl;
   if ( iTipoCons == 2 )
   {
      //R�gimen
      iElementos = ObjWS_IDC->getNumRegs();
      //std::cout << "NUM_REGIMEN=     " << iElementos << std::endl;
      sDato = ObtLista( LISTA_REGIMEN, iElementos );
      std::cout << "REGIMENES=       " << sDato.c_str() << std::endl;
      //Role
      iElementos = ObjWS_IDC->getNumRoles();
      //std::cout << "NUM_ROL=         " << iElementos << std::endl;
      sDato = ObtLista( LISTA_ROLE, iElementos );
      std::cout << "ROLES=           " << sDato.c_str() << std::endl;
      //Obligacion
      iElementos = ObjWS_IDC->getNumObligs();
      //std::cout << "NUM_OBLIG=       " << iElementos << std::endl;
      sDato = ObtLista( LISTA_OBLIGACION, iElementos );
      std::cout << "OBLIGACIONES=    " << sDato.c_str() << std::endl;
      //Actividad
      iElementos = ObjWS_IDC->getNumActivs();
      //std::cout << "NUM_ACTIVIDAD=   " << iElementos << std::endl;
      sDato = ObtLista( LISTA_ACTIVIDAD, iElementos );
      std::cout << "ACTIVIDADES=     " << sDato.c_str() << std::endl;
   }
   borrarPtr();
   return iError;
};
