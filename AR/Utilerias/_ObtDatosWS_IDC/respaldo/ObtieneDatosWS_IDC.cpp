#ifdef WS_EXTERNO
   #include "IdCSOAP.nsmap"
#else
   #include "IdCInternoSOAP.nsmap"
#endif

#include <Sgi_ConfigFile.h>
#include <Sgi_Bitacora.h>
#include <Sgi_ProtegePwd.h>
#include "CSolDatosWS_IDC.h"

CBitacora   *Bitacora      = NULL;
CConfigFile *Configuracion = NULL;

std::string m_URLWS;

//#################################################################################
#define PATH_CONF            "./"
#define PATH_DBIT            "./"

#ifdef DBG
   #define ARCH_CONF            PATH_CONF "ObtRFCOrigWS_dbg.cfg"
   #define PATH_BITA            PATH_DBIT "ObtRFCOrigWS_dbg.log"
#else
   #define ARCH_CONF            PATH_CONF "ObtRFCOrigWS.cfg"
   #define PATH_BITA            PATH_DBIT "ObtRFCOrigWS.log"
#endif

// Creaci�n del objeto de Bitacora
CBitacora* Crea_CBitacora()
{
   return new CBitacora(PATH_BITA);
}
// Valida los parametros de entrada
bool procesaParametros(int argc, char* argv[], CBitacora *Bitacora)
{
   if ( argc == 6 )
   {
      Bitacora->escribePV(BIT_INFO, "Argumentos recibidos (%s, %s, %s, %s, %s)", argv[0], argv[1], argv[2], argv[3], argv[4], argv[5]);
      if ( !(strlen(argv[1]) == 12 || strlen(argv[1]) == 13) )
      {
         Bitacora->escribePV( BIT_ERROR, "La longitud del RFC es inv�lida (%s), revise el dato.", argv[1] );
         return false;
      }
      return true;
   }
   else
   {
      std::cout << std::endl << std::endl ;
      std::cout << "Uso: " << std::endl << "\t" << argv[0] << " RFC edo_rfc fec_reg agc_cve motivo\n" << std::endl;
      std::cout << "Verifique que los par�metros sean correctos \n\n" << std::endl;
      return false;
   }
}

//Desencripta el pwd
bool procesaPassword(const std::string& s_pwdEnc, std::string *s_pwdDes)
{
   bool b_resOperacion = true;
   int i_pwdSalida = s_pwdEnc.size();
   char *psz_pwdSalida = new char[i_pwdSalida];
   if (!desencripta((uint8*) s_pwdEnc.c_str(), i_pwdSalida, (uint8*) psz_pwdSalida, &i_pwdSalida))
      b_resOperacion = false;
   else
      s_pwdDes->assign(psz_pwdSalida, i_pwdSalida);
   delete[] psz_pwdSalida;
   return b_resOperacion;
}

bool getVarConfWS(std::string *Usr, std::string *Pwd, /*std::string *URL,*/ std::string *conHTTPS, std::string *cert)
{
   std::string NvlBitWS;
   std::string PwdWSEnc;
   int r1, r2, r3, r4, r5;
   
   //Establece el nivelde bit�cora
   r1  = Configuracion->getValorVar("[BITACORA]"    , "NIVELBIT" , &NvlBitWS);
   if (r1 != 0)
      NvlBitWS = "3";
   if (Bitacora)
      Bitacora->setNivel( (BIT_NIVEL) atoi(NvlBitWS.c_str()) );
   //Lee los datos del WEB SERVICE IDC
   r1 = Configuracion->getValorVar("[WEB_SERVICE_IDC]" , "USUARIO" , Usr      );
   r2 = Configuracion->getValorVar("[WEB_SERVICE_IDC]" , "PASSWORD", &PwdWSEnc);
   r3 = Configuracion->getValorVar("[WEB_SERVICE_IDC]" , "URLWS"   , &m_URLWS );
   r4 = Configuracion->getValorVar("[WEB_SERVICE_IDC]" , "HTTPS"   , conHTTPS );
   //Determina si utiliza el certificado
   if ( conHTTPS->compare("S") == 0)
   {
      r5 = Configuracion->getValorVar("[WEB_SERVICE_IDC]", "CERT", cert);
      if ( r1 || r2 || r3 || r4 || r5 )
      {
         Bitacora->escribePV(BIT_ERROR, "Problemas al leer la configuraci�n de WS_IDC: "
                                        "usr(%d), Pwd(%d), URL(%d), conHTTPS(%d), Crt(%d)",
                             r1, r2, r3, r4, r5);
         return false;
      }
      Bitacora->escribePV(BIT_DEBUG, "Los par�metros de configuraci�n le�dos son: usr (%s), URL(%s), "
                                     "conHTTPS(%s), Crt(%s)",
                                     Usr->c_str(), m_URLWS.c_str(), conHTTPS->c_str(), cert->c_str());
   }
   else
   {
      if ( r1 || r2 || r3 || r4 )
      {
         Bitacora->escribePV(BIT_ERROR, "Problemas al leer la configuraci�n de WS_IDC: "
                                        "usr(%d), Pwd(%d), URL(%d), conHTTPS(%d)",
                             r1, r2, r3, r4);
         return false;
      }
      Bitacora->escribePV(BIT_DEBUG, "Los par�metros de configuraci�n le�dos son: usr (%s), URL(%s), "
                                     "conHTTPS(%s)",
                                     Usr->c_str(), m_URLWS.c_str(), conHTTPS->c_str());
   } 
   if ( m_URLWS.length() == 0 )
   {
      Bitacora->escribe(BIT_ERROR, "No se recupero la URL correctamente");
      return false;
   }
   if ( !procesaPassword(PwdWSEnc, Pwd) )
   {
      Bitacora->escribe(BIT_ERROR, "Fall� la desencripci�n del pwd del WS");
      return false;
   }
   return true;
}

void borrarPtr()
{
   if (Bitacora)
   {
      delete Bitacora;
      Bitacora = NULL;
   }
   if (Configuracion)
   {
      delete Configuracion;
      Configuracion = NULL;
   }
}

int main(int argc, char* argv[])
{
   int         lrfcOrig = 14;
   char        rfc_Orig[lrfcOrig];
   int         iError = 0;
   std::string usrWS, pwdWS, conHTTPSWS, certWS;

   Bitacora       = Crea_CBitacora();
   Configuracion  = new CConfigFile(ARCH_CONF);
   memset(rfc_Orig, 0, lrfcOrig); 
   
   if ( !(procesaParametros(argc, argv, Bitacora)) )
      iError = 1; 
   else
   {
      //Lectura de ArchConf
      if ( (iError = Configuracion->cargaCfgVars()) )
      {
         Bitacora->escribePV(BIT_ERROR, "Problemas al leer el archivo de configuraci�n (%d): %s", iError, ARCH_CONF);
         iError = 2;
      }
      else
      {
         if ( !getVarConfWS(&usrWS, &pwdWS, &conHTTPSWS, &certWS) )
            iError = 3;
         else
         {
            //Genera el objeto para interactuar con el WS_IDC
            CSolDatosWS_IDC ObjWS_IDC(argv[1], 1, usrWS, pwdWS, conHTTPSWS, certWS);
            if ( &ObjWS_IDC == NULL)
               iError = 4;
            else
            {
               if ( !ObjWS_IDC.InicializaWS() )
                  iError = 5;
               else
               {
                  if ( ((iError = ObjWS_IDC.SolInfoRFCWS( argv[1] )) == EXITO) )
                  {
                     if ( !ObjWS_IDC.getRFCOrig(rfc_Orig) )
                     
                        iError = 6;
                  }
               }
            }
         }
      }
   }
   if (iError == 0)
   {
      std::cout << argv[1] << "|" << rfc_Orig << "|" ;             //rfc_cons, rfc_orig
      std::cout << argv[2] << "|" << argv[3]  << "|" ;             //edo_rfc, fec_reg
      std::cout << argv[4] << "|" << argv[5]  << "|" << std::endl; //agc_cve, motivo
   }
   borrarPtr();
   return iError;
};
