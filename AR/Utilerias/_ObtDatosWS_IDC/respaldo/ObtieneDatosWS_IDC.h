#ifndef _OBTRFCORIGWS_H_
#define _OBTRFCORIGWS_H_

CBitacora* Crea_CBitacora();

bool procesaParametros(int argc, char* argv[], CBitacora *Bitacora);
bool procesaPassword(const std::string& s_pwdEnc, std::string *s_pwdDes);
bool getVarConfWS(std::string *Usr, std::string *Pwd, std::string *conHTTPS, std::string *cert);
void borrarPtr();

#endif // _OBTRFCORIGWS_H__

