/*######################################################################################################################
  ###  PROYECTO:              PKI-SAT                                                                                ###
  ###  MODULO:                Clase que sustituye las consultas a DARIO por consultas al WS Externo del SAT          ###
  ###                                                                                                                ###
  ###  DESARROLLADORES:       Gudelia Hern�ndez Molina        GHM                                                    ###
  ###  FECHA DE INICIO:       Lunes 11, agosto del 2008                                                              ###
  ###                                                                                                                ###
  ######################################################################################################################
         1         2         3         4         5         6         7         8         9        10        11        12
1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*######################################################################################################################
   VERSION:
      V.1.00      (20080811 -         ) GHM: Primera Versi�n
                                             Esta clase se gener� para sustituir las validaciones de DARIO en el
                                             WebService Externo a fin de utilizar el socket encriptado
   CAMBIOS:
######################################################################################################################*/

#ifndef _CSOLDATOSWS_IDC_H_
#define _CSOLDATOSWS_IDC_H__
static const char* _CSOLDATOSWS_IDC_H_VERSION_ ATR_USED = "ObtDatosWS_IDC @(#)"\
"DSIC09115AR_ 2009-03-20 CSolDatosWS_IDC.h 1.1.0/0";


//#VERSION: 1.1.0

#include <string.h>                            // Manejo de Strings
#include <sstream>                           // Manejo strstream para el manejo del error de gsoap

//#include "Sgi_MsgPKI.h"
#include "stdsoap2.h"

#ifdef WS_EXTERNO
   #include "soapIdCSOAPProxy.h"
#else
   #include "soapIdCInternoSOAPProxy.h"
#endif

extern std::string m_URLWS ;

//Agregar para el WS
#define TAM_ARREGLO              100
#define TAM_ERROR_GSOAP          1024
#define TAM_PATH_CERT            512

#define CARGA_UNPASO             1
#define CARGA_DOSPASOS           2


#define ERR_INIC_WS             301    //Falla al inicializar los par�metros del WS
#define RFC_NO_CARGADO          302    //No se ha cargado el RFC de busqueda en el objeto
#define NO_INICIALIZADO         303    //No esta inicilizado el objeto
#define NO_COINCIDE_RFCBSQ      304    //No coincide el RFC de busqueda con el cargado en el objeto
#define ERRORES_RECIBIDOWS      305    //El WS env�o error(es)
#define AVISOS_RECIBIDOWS       306    //El WS env�o aviso(s)
#define ERR_CONSULTA_RFC        307    //Error al efectuar la consulta
#define ERR_NOREC_RFC           308    //No se recupero informaci�n del RFC consultado
#define ERR_NOREC_IDENTIFIC     309    //No se recupero informaci�n de Identificacion
#define ERR_NOREC_DOMICILIO     310    //No se recupero informaci�n del domicilio
#define ERR_NOREC_REGIMENES     311    //No se recupero informaci�n de los regimenes
#define ERR_NOREC_OBLIG         312    //No se recupero informaci�n de las obligaciones
#define ERR_NOREC_ROLES         313    //No se recupero informaci�n de los roles
#define ERR_NOREC_ACTIV         314    //No se recupero informaci�n de las actividades
#define ERR_NOREC_NOMBRE        315    //No se recupero el nombre solicitado al WS
#define ERR_NOREC_PATERNO       316    //No se recupero el apellido paterno solicitado al WS
#define ERR_NOREC_CSITFIS       317    //No se recupero la clave de Situaci�n fiscal

#define ERR_NOREC_FECHASOL      318    //No se recupero la fecha solicitada al WS
#define ERR_LONGFECHA_WS        319    //La primera parte de la estructura de la fecha se extrae del WS y debe tener formato yyyy-mm-dd
#define ERR_ESTRUCTFECHA_WS     320    //Se le agrega 00:00:00 a la fecha para validar la estructura, en caso de no tener 
                                       //la estructur yyyy-mm-dd 00:00:00 se env�a el error 

// ******************************  DEFINICIONES DE LA CLASE  *******************************
class CSolDatosWS_IDC
{
   protected:
      //DEFINICION DE VARIABLES Y METODOS PROTEGIDOS
      //Variables manejo datos
      std::string m_MsgDesc;
      bool        m_RFCcargado;            //Para verificar si ya se solicito la consulta del rfc
      int         m_statusRFC;
      std::string sAvisosWS;
      std::string sErroresWS;

      //Variables WS
      std::string m_usrWS, m_pwdWS, m_CertWS;
      int    m_tipoMsgWS;             //Tipo de carga de datos: 1. una sola pasada, 2: en dos rubros
      char   m_conHTTPS;              //'S' se utiliza el Certificado; 'N' no se requiere certificado
      char   m_RFCObj[14];
      
      char*  SecObt1[2];
      char*  SecObt2[6];
      #ifdef WS_EXTERNO
         //Crea un objeto proxy
         IdCSOAPProxy        ClientIDCproxy;
         //Crea el objeto con el contenido de las secciones
         _ns2__IdC           ns2_IdCExtrae;
      #else
         //Crea un objeto proxy
         IdCInternoSOAPProxy ClientIDCproxy;
         //Crea el objeto con el contenido de las secciones
         _ns2__IdCInterno    ns2_IdCExtrae;
      #endif
      //Crea los objetos que almacenan la informaci�n
      _ns2__datosEntrada  ns2_datosEntrada;
      ns2__identificacion ns2_Identificacion;
      ns2__mensajes       ns2_mensajes;

   private:
      //DEFINICION DE VARIABLES Y METODOS PRIVADOS
   
   public:
      //DEFINICION DE VARIABLES Y METODOS PUBLICOS
      bool        m_inicializado;
      
      CSolDatosWS_IDC(const std::string &RFCBusq, int TpoCert,
                      const std::string &UsrWS,   const std::string &PwdWS, const std::string &ConHTTPS, const std::string &Cert);
      virtual ~CSolDatosWS_IDC();

      int  getStatusRFC() { return m_statusRFC; }

      std::string getDescError();
      bool TrataErrorProc(int itpoMsg, const char* sCausa );                    //Manejo de msg de error
      bool CoincideRFCObj(const char* RFCBsq);                                  //Verifica que coincidan los datos 
      bool transfTpoContr(const char cveTpo, char* TipoContrib, int lTpoCont ); //Modif. la presentaci�n del tipo de Contrib

      bool InicializaWS();
      bool ValObjInic(std::string Origen);

      void LimpiaSaltos(std::string *Cadena); //Quita los saltos de l�nea para el correcto despliegue de los datos en bit�cora
      void trataErrorGSOAP();            //Genera una cadena con el error de gsoap para el correcto despliegue en bit�cora
      bool hayAvisosWS();                //Extrae los avisos que envia el WS para su registro en la bit�cora
      bool hayErroresWS();               //Extrae los errores que envia el WS para su registro en la bit�cora
      int  validaRespWS(int seccion);    //Valida que el WS env�o toda la informaci�n solicitada

      int  SolInfoRFCWS(const char* RFCSolic);            //Env�a el msg de consulta al WS

      int  ExtraeNombre(std::string *nombre);
      int  getNombre(const char* RFCBsq, std::string *nombre); //Hace la solicitud de informacion del RFC al WS y regresa el nombre
      int  getNombre(std::string *nombre);                     //Extrae el nombre
      
      int  getSitDomicilio(const char* RFCBsq);           //Extrae la clave de la situaci�n de domicilio
      std::string getDescDom();
      int  getStrDomicilio(std::string *direccion);
      
      //int  getSitFiscal(const char* RFCBsq);
      bool getSitFiscal(const char* RFCBsq,int &iSitFiscal); //SERR: Modificacion para extraer la situacion fiscal
      bool getDesSitFis(std::string *DesSitFisc);
      bool getTipoContrib(std::string *TipoCont);

      bool CalcStatusRFC(); 
      bool getRFCOrig(char* RFCOrig);
      bool getCURP(char* CURP );

      bool getRegimen(int indice, std::string *cveRegimen, bool *Activo );
      bool getObligacion(int indice, std::string *Obligacion, bool *Activo );
      bool getRol(int indice, std::string *Rol, bool *Activo );
      bool getActiv(int indice, std::string *Activ, bool *Activo );

      int getNumRegs();
      int getNumObligs();
      int getNumRoles();
      int getNumActivs();

      int  valCadenaVig(const char fecha[], bool nullValido = false);
      bool valVigencia(const char seccion[], int indice, const char clave[], char* ptrVigInic, char* ptrVigFinal);
};

#endif //_CSOLDATOSWS_IDC_H_ 

