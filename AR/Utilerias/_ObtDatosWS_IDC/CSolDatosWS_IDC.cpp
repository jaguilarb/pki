static const char* _CSOLDATOSWS_IDC_CPP_VERSION_ ATR_USED = "ObtDatosWS_IDC @(#)"\
"DSIC09115AR_ 2009-03-20 CSolDatosWS_IDC.cpp 1.1.0/0";

//#VERSION: 1.1.0
/*######################################################################################################################
  ###  PROYECTO:              PKI-SAT                                                                                ###
  ###  MODULO:                Clase que sustituye las consultas a DARIO por consultas al WS Externo del SAT          ### 
  ###                                                                                                                ###
  ###  DESARROLLADORES:       Gudelia Hern�ndez Molina        GHM                                                    ###
  ###  FECHA DE INICIO:       Lunes 11, agosto del 2008                                                              ###
  ###                                                                                                                ###
  ######################################################################################################################
         1         2         3         4         5         6         7         8         9        10        11        12
1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*######################################################################################################################
   VERSION:  
      V.1.00      (20080811 -         ) GHM: Primera Versi�n
                                             Esta clase se gener� para sustituir las validaciones de DARIO en el
                                             WebService Externo a fin de utilizar el socket encriptado

   CAMBIOS:
######################################################################################################################*/
#include <Sgi_Bitacora.h>
#include <Sgi_ConfigFile.h>
#include <Sgi_ProtegePwd.h>
#include <Sgi_Fecha.h>
#include "CSolDatosWS_IDC.h"
#include <DefDatos.h>

extern CBitacora*   Bitacora;

//######################################################################################################################

//##### Declaraci�n del Constructor #####
CSolDatosWS_IDC::CSolDatosWS_IDC( const std::string &RFCBusq,  int TpoCert,
                                  const std::string &UsrWS,    const std::string &PwdWS, 
                                  const std::string &ConHTTPS, const std::string &Cert )
   :  m_RFCcargado(false),
      m_statusRFC(-1),
      m_usrWS(UsrWS),
      m_pwdWS(PwdWS),
      m_CertWS(Cert),
      m_tipoMsgWS(0),
      m_conHTTPS(ConHTTPS[0]),
      ClientIDCproxy(),
      ns2_IdCExtrae(),
      ns2_datosEntrada(),
      ns2_Identificacion(),
      ns2_mensajes(),
      m_inicializado(false)
          
{
   //Inicializacion de variables 1a vez
   if (! RFCBusq[0] == 0 )
   {
      sprintf(m_RFCObj, "%s", RFCBusq.c_str());
      m_RFCcargado = true ;
   }else
      m_RFCObj[0] = 0;
   m_tipoMsgWS = TpoCert;
   m_conHTTPS = ConHTTPS[0];

   //memset(SeccObtener, 0, sizeof(SeccObtener));
   memset(SecObt1    , 0, sizeof(SecObt1));
   memset(SecObt2    , 0, sizeof(SecObt2));
}

//##### Declaraci�n del Destructor #####
CSolDatosWS_IDC::~CSolDatosWS_IDC()
{
   //Libera los apuntadores que se hayan creado
}

//######################################################################################################################
//FUNCIONES TAREAS ESPECIFICAS
//######################################################################################################################

//##### Extrae el mensaje de error #####
std::string CSolDatosWS_IDC::getDescError()
{
   return m_MsgDesc;
}

//##### Manejo del mensaje de error #####
bool CSolDatosWS_IDC::TrataErrorProc(int itpoMsg, const char* sCausa )
{
   //indicador de tipo de error (PROCESO)
   char inicio[]= "WS_IDC: Se present� un error ";
   char soluc[] = ", \n favor de intentar nuevamente, si se vuelve a presentar el error solicite "
                   "apoyo al inform�tico local.";
   switch (itpoMsg)
   {
      case MSGDESC_ICS:
         m_MsgDesc = std::string(inicio)+ std::string(sCausa) + std::string(soluc);
         break;
      case MSGDESC_CS:
         m_MsgDesc = std::string(sCausa) + std::string(soluc);
         break;
      case MSGDESC_VACIO :
         m_MsgDesc = "";
         break;
   }
   return false;
}

//##### Validaci�n del RFC del objeto y el RFC de busqueda #####
bool CSolDatosWS_IDC::CoincideRFCObj(const char* RFCBsq)
{
   if ( strcmp(m_RFCObj, RFCBsq) == 0 )
      return true;
   m_MsgDesc = "WS_IDC: El RFC para la revisi�n ("+ std::string(RFCBsq) + ") no coincide con el RFC del objeto (" +
               std::string (m_RFCObj);
   Bitacora->escribePV(BIT_ERROR, "%s", m_MsgDesc.c_str());
   return false ;
}

//##### Modifica la presentaci�n del tipo de Contribuyente #####
bool CSolDatosWS_IDC::transfTpoContr(const char cveTpo, char* TipoContrib, int lTpoCont )
{
   std::string m_MsgDesc;

   bool ok = false;
   TipoContrib[0] = 0;
   if ( ((size_t)lTpoCont) < sizeof("No determinado") )
   {
      m_MsgDesc = "WS_IDC: Error de memoria, vuelva a intentar el proceso, si vuelve a presentarse el error \n"
                  "solicite apoyo a su inform�tico local";
      Bitacora->escribePV(BIT_ERROR, "WS_IDC(transfTpoContr): El tama�o de la variable es (%i) y minimo debe tener (%i)",
                                      lTpoCont, sizeof("No determinado")+1);
      return false;
   }
   switch ( cveTpo )
   {
      case 'M':  sprintf(TipoContrib, "%s", "Moral");          ok = true;  break;
      case 'F':  sprintf(TipoContrib, "%s", "Fisica");         ok = true;  break;
      case 'I':  sprintf(TipoContrib, "%s", "Fideicomiso");    ok = true;  break;
      default :  sprintf(TipoContrib, "%s", "No determinado"); ok = false; break;
   }
   return ok;
}

//######################################################################################################################
//FUNCIONES DE WEB SERVICE
//######################################################################################################################

//##### Inicializaci�n del Web Service #####
bool CSolDatosWS_IDC::InicializaWS()
{
   if ( m_tipoMsgWS == TIPCER_FEA  )
   {
      ns2_datosEntrada.__sizesecciones = 2;
      SecObt1[0]="Identificacion";
      SecObt1[1]="Ubicacion";
      ns2_datosEntrada.secciones = SecObt1;
   }
   else
   {
      ns2_datosEntrada.__sizesecciones = 6;
      //Todo en un paso
      SecObt2[0]="Identificacion";
      SecObt2[1]="Ubicacion";
      SecObt2[2]="Regimenes";
      SecObt2[3]="Obligaciones";
      SecObt2[4]="Roles";
      SecObt2[5]="Actividades";
      ns2_datosEntrada.secciones = SecObt2;
   }
   //Datos para la consulta
   ns2_datosEntrada.usuario   = (char*)m_usrWS.c_str();
   ns2_datosEntrada.password  = (char*)m_pwdWS.c_str();

   ClientIDCproxy.soap_endpoint = (const char*) m_URLWS.c_str();
   #ifndef WS_EXTERNO
      if ( m_conHTTPS == 'S' )
      {
   #endif
         //Carga ssl
         soap_ssl_init(); /* init OpenSSL (just once) */
         if ( soap_ssl_client_context(&ClientIDCproxy, 
                                      SOAP_SSL_DEFAULT | SOAP_SSL_SKIP_HOST_CHECK,
                                      NULL,               /* keyfile: required only when client must authenticate
                                                             to server (see SSL docs on how to obtain this file) */
                                      NULL,               /* password to read the keyfile */
                                      m_CertWS.c_str(),   /* cacert file to store trusted certificates (needed to verify server) */
                                      NULL,               /* optional capath to directory with trusted certificates */
                                      NULL                /* if randfile!=NULL: use a file with random data to seed randomness */
                                      )
          )
         {
            Bitacora->escribe(BIT_ERROR, "WS_IDC(InicializaWS): No se inici� correctamente el contexto de ssl.");
            trataErrorGSOAP();
            m_inicializado = false;
            return false;
         }
   #ifndef WS_EXTERNO
      }
   #endif
   m_inicializado = true;
   return true;
}

bool CSolDatosWS_IDC::ValObjInic(std::string Origen)
{
   if (!m_inicializado)
   {
      m_MsgDesc = "WS_IDC: Error con la conexi�n al Web Service, intente m�s tarde,\n" 
                  "si persiste el problema solicite apoyo al inform�tico local ";
      Bitacora->escribePV(BIT_ERROR, "%s No esta inicializada la conexi�n al WS utilizada.", Origen.c_str());
   }
   return m_inicializado;
}

void CSolDatosWS_IDC::LimpiaSaltos(std::string *Cadena)
{
    int pos = 0;
    int limite = Cadena->length();
    if (limite > 0)
    { 
       for (pos=0; pos < limite ; pos++)
       {
          if ( Cadena->at(pos) == '\n' )
             Cadena->replace(pos,1," ");
       }
    }
    return;
}

void CSolDatosWS_IDC::trataErrorGSOAP()
{
   std::ostringstream osErrorWS;
   std::string sError;

   ClientIDCproxy.soap_stream_fault(osErrorWS);
   osErrorWS.str(sError); 
   if ( sError.length() > 0 )
   {
      for (size_t pos=0; pos < sError.length(); pos++)
      {
         if ( sError[pos] == '\n' )
            sError[pos] = ' ';
      }
      Bitacora->escribePV(BIT_ERROR, "WS_IDC(trataErrorGSOAP): Se present� el error de GSOAP -> %s", sError.c_str());
   }else
      Bitacora->escribe(BIT_ERROR, "WS_IDC(trataErrorGSOAP): No se extraj� el error de GSOAP");
   m_MsgDesc = "Error interno, favor de reportarlo al inform�tico local.";
   return;
}

bool CSolDatosWS_IDC::hayAvisosWS()
{
   int    cont=0;
   //std::string sAvisosWS;
   char   strCont[4];
   bool   hay_avisos = false;

   sAvisosWS = "";
   if (ns2_IdCExtrae.Mensajes != NULL)
   {
      if (ns2_IdCExtrae.Mensajes->__sizeaviso != 0)
      {
         sAvisosWS = "AVISOS: ";
         for (cont=0; cont < ns2_IdCExtrae.Mensajes->__sizeaviso; cont++)
         { 
            if (ns2_IdCExtrae.Mensajes->aviso[cont] != NULL)
            {
               sprintf(strCont, "%d", cont+1);
               sAvisosWS += std::string(strCont) + ". ";
               sAvisosWS += std::string(ns2_IdCExtrae.Mensajes->aviso[cont]) + " ";
            }
         }
         LimpiaSaltos(&sAvisosWS);
         Bitacora->escribePV(BIT_INFO, "WS_IDC(hayAvisosWS): Se presentaron (%d) aviso(s). %s", 
                             ns2_IdCExtrae.Mensajes->__sizeaviso, sAvisosWS.c_str());
         hay_avisos = true;
      }
   }
   return hay_avisos;
}

bool CSolDatosWS_IDC::hayErroresWS()
{
   int    cont=0;
   //std::string sErroresWS;
   char   strCont[4];
   bool   hay_error = false;
   
   sErroresWS = "";
   if (ns2_IdCExtrae.Mensajes != NULL)
   {
      if (ns2_IdCExtrae.Mensajes->__sizeerror != 0)
      {
         sErroresWS += "ERRORES: ";
         for (cont=0; cont < ns2_IdCExtrae.Mensajes->__sizeerror; cont++)
         {
            sprintf(strCont, "%d", cont+1);
            sErroresWS += std::string (strCont) + ". ";
            sErroresWS += std::string (ns2_IdCExtrae.Mensajes->error[cont]) + " ";
         }
         LimpiaSaltos(&sErroresWS);
         Bitacora->escribePV(BIT_ERROR, "WS_IDC(hayErroresWS): Se presentaron (%d) error(es). %s",
                                        ns2_IdCExtrae.Mensajes->__sizeerror, sErroresWS.c_str());
         hay_error = true;
      }
   }
   return hay_error ;
}

//# TIPCER_FEA:   Identificacion, Ubicacion
//# TIPCER_SD :   Identificacion, Ubicacion, Regimenes, Obligaciones, Roles, Actividades
int CSolDatosWS_IDC::validaRespWS(int itipCer )
{
   if (ns2_IdCExtrae.RFC_USCOREVigente == NULL)
   {
      Bitacora->escribe(BIT_ERROR, "WS_IDC(ValidaRespWS): No se obtuvo el RFC Vigente");
      m_MsgDesc = "No se encontr� informaci�n de RFC Vigente";
      return ERR_NOREC_RFC;
   }
   if ( itipCer == TIPCER_FEA )
   {
      if (ns2_IdCExtrae.Identificacion == NULL)
      {
         Bitacora->escribe(BIT_ERROR, "WS_IDC(ValidaRespWS): No se encontr� informaci�n de Identificaci�n");
         m_MsgDesc = "WS_IDC: No se encontr� informaci�n de la secci�n de Identificaci�n"; 
         return ERR_NOREC_IDENTIFIC;
      }
      if (ns2_IdCExtrae.Ubicacion == NULL)
      {
         Bitacora->escribe(BIT_ERROR, "WS_IDC(ValidaRespWS): No se encontr� informaci�n de la Ubicaci�n");
         m_MsgDesc = "WS_IDC: No se encontr� informaci�n de la secci�n de Ubicaci�n"; 
         return ERR_NOREC_DOMICILIO;
      }
   }
   else  
   {
      if (ns2_IdCExtrae.Identificacion == NULL)
      {
         Bitacora->escribe(BIT_ERROR, "WS_IDC(ValidaRespWS): No se encontr� informaci�n de Identificaci�n");
         m_MsgDesc = "WS_IDC: No se encontr� informaci�n de la secci�n de Identificaci�n"; 
         return ERR_NOREC_IDENTIFIC;
      }
      if (ns2_IdCExtrae.Ubicacion == NULL)
      {
         Bitacora->escribe(BIT_ERROR, "WS_IDC(ValidaRespWS): No se encontr� informaci�n de la Ubicaci�n");
         m_MsgDesc = "WS_IDC: No se encontr� informaci�n de la secci�n de Ubicaci�n"; 
         return ERR_NOREC_DOMICILIO;
      }
      if (ns2_IdCExtrae.Regimenes == NULL)
      {
         Bitacora->escribe(BIT_ERROR, "WS_IDC(ValidaRespWS): No se encontr� informaci�n de los Regimenes");
         m_MsgDesc = "WS_IDC: No se encontr� informaci�n de la secci�n de Regimenes"; 
         return ERR_NOREC_REGIMENES;
      }
   }
   return 0;
}

//##### Consulta de datos en el Web Service por RFC #####
int CSolDatosWS_IDC::SolInfoRFCWS(const char* RFCSolic)
{
   int         iError = 0;
   std::string sAvisoErrorWS;
   std::string origError = "WS_IDC: SolInfoRFCWS (" + std::string(RFCSolic) + "):";

   if (!ValObjInic(origError))
      return NO_INICIALIZADO;

   if ( RFCSolic == NULL || RFCSolic[0] == 0 )
   {
      Bitacora->escribePV(BIT_ERROR, "%s No se tiene valor de RFC para la b�squeda.", origError.c_str());
      m_MsgDesc = "WS_IDC: No se inicializo el RFC para b�squeda de datos";
      m_RFCcargado = false;
      return ERR_NOREC_RFC;
   }
   sprintf(m_RFCObj, "%s", RFCSolic);
   ns2_datosEntrada.rfc = m_RFCObj;
   m_RFCcargado = true;
   //Invoca la operaci�n del WS
   #ifdef WS_EXTERNO
      if ( (iError = ClientIDCproxy.getIdC(&ns2_datosEntrada, &ns2_IdCExtrae))== SOAP_OK )
   #else
      if ( (iError = ClientIDCproxy.getIdCInterno(&ns2_datosEntrada, &ns2_IdCExtrae))== SOAP_OK )
   #endif
      Bitacora->escribePV(BIT_DEBUG, "%s Se env�o consulta del RFC (%s) correctamente al WS", origError.c_str(), RFCSolic);
   else
   {
      Bitacora->escribePV(BIT_ERROR, "%s Error en la consulta del RFC -> '%s', se recibio error no. (%d)", origError.c_str(), RFCSolic, iError);
      m_MsgDesc = "WS_IDC: Error Interno de gsoap, reportelo al inform�tico local.";
      return ERR_CONSULTA_RFC;
   }
   if ( hayErroresWS( ))
   {  
      hayAvisosWS();
      m_MsgDesc = sAvisosWS;
      return ERRORES_RECIBIDOWS;
   }
   if ( (iError = validaRespWS(m_tipoMsgWS)) != 0 )
   {
      hayAvisosWS();
      return iError;
   }else
   {
      hayAvisosWS();
   }
   std::string sNombre;
   if ( (iError = getNombre(&sNombre)) != 0 ) 
      Bitacora->escribePV(BIT_ERROR, "%s Se present� el error (%i) en la consulta del nombre de (%s)", 
                                     origError.c_str(), iError, RFCSolic);
   else    
      Bitacora->escribePV(BIT_DEBUG, "%s RFC(%s) -> Nombre(%s)", origError.c_str(), RFCSolic, sNombre.c_str());
   CalcStatusRFC();
   return iError;
}

//FUNCIONES DE SUSTITUCION DARIO
//######################################################################################################################

//##### Extrae el nombre #####

int CSolDatosWS_IDC::ExtraeNombre(std::string *nombre)
{
   if (ns2_IdCExtrae.Identificacion->Razon_USCORESoc != NULL &&
       ns2_IdCExtrae.Identificacion->Razon_USCORESoc[0] != 0 )
      *nombre = std::string(ns2_IdCExtrae.Identificacion->Razon_USCORESoc);
   else
   {
      /*if (ns2_IdCExtrae.Identificacion->Nom_USCOREComercial != NULL &&
          ns2_IdCExtrae.Identificacion->Nom_USCOREComercial[0] != 0 )
         *nombre = std::string (ns2_IdCExtrae.Identificacion->Nom_USCOREComercial);
      else
      {*/
         if (ns2_IdCExtrae.Identificacion->Nombre != NULL &&
             ns2_IdCExtrae.Identificacion->Nombre[0] != 0 )
            *nombre = std::string(ns2_IdCExtrae.Identificacion->Nombre);
         else
         {
            Bitacora->escribe(BIT_ERROR, "WS_IDC(ExtraeNombre): No se encontr� nombre");
            m_MsgDesc = "WS_IDC: No se encontr� nombre";
            return  ERR_NOREC_NOMBRE;
         }
         if ( ns2_IdCExtrae.Identificacion->Ap_USCOREPaterno != NULL && 
              ns2_IdCExtrae.Identificacion->Ap_USCOREPaterno[0] != 0 )
            *nombre = *nombre + " " + std::string(ns2_IdCExtrae.Identificacion->Ap_USCOREPaterno);
         else
         {
      
            m_MsgDesc = "WS_IDC: No se encontr� apellido paterno";
            Bitacora->escribe(BIT_ERROR, "WS_IDC(ExtraeNombre): No se encontr� Apellido Paterno");
            return ERR_NOREC_PATERNO;
         }
         if (ns2_IdCExtrae.Identificacion->Ap_USCOREMaterno != NULL &&
             ns2_IdCExtrae.Identificacion->Ap_USCOREMaterno[0] != 0 ) 
             *nombre = *nombre + " " + std::string(ns2_IdCExtrae.Identificacion->Ap_USCOREMaterno);
      //}
   }
   return 0;
}
 
int CSolDatosWS_IDC::getNombre(const char* RFCBsq, std::string *nombre)
{
   char rfcCons[14];
   rfcCons[0] = 0;
   std::string origError = "WS_IDC: getNombre (" + std::string(RFCBsq) + ").";
   
   if (!ValObjInic(origError))
      return NO_INICIALIZADO;
   if ( !m_RFCcargado )
   {
      m_MsgDesc = origError + "WS_IDC: No se indic� el RFC a consultar";
      Bitacora->escribePV(BIT_ERROR, "%s No se indico el RFC a consultar", origError.c_str());
      return RFC_NO_CARGADO;
   }
   if ( !CoincideRFCObj(RFCBsq) )
      return NO_COINCIDE_RFCBSQ;
   if (ns2_IdCExtrae.Identificacion == NULL)
   {
      m_MsgDesc = "WS_IDC: No se obtuvo datos de Identificaci�n del WS";
      return ERR_NOREC_IDENTIFIC;
   }
   return ExtraeNombre(nombre);
}

int CSolDatosWS_IDC::getNombre(std::string *nombre)
{
   std::string origError = "WS_IDC:getNombre(con RFC previo)";

   if (!ValObjInic(origError))
      return NO_INICIALIZADO;
   if ( !m_RFCcargado )
   {
      m_MsgDesc = "WS_IDC: No se indic� el RFC a consultar";
      return RFC_NO_CARGADO;
   }
   if (ns2_IdCExtrae.Identificacion == NULL)
   {
      m_MsgDesc = "WS_IDC: No se obtuvo datos de Identificaci�n del WS";
      return ERR_NOREC_IDENTIFIC;
   }
   return ExtraeNombre(nombre);
}

//##### Extrae la clave de la situaci�n de domicilio #####
int CSolDatosWS_IDC::getSitDomicilio(const char* RFCBsq)
{
   std::string origError = "WS_IDC: getSitDomicilio (" + std::string(RFCBsq) + ").";
   
   if (!ValObjInic(origError))
      return NO_INICIALIZADO;
   if ( !m_RFCcargado )
   {
      m_MsgDesc = origError + " No se indic� el RFC a consultar";
      return RFC_NO_CARGADO;
   }
   if ( !CoincideRFCObj(RFCBsq) )
      return NO_COINCIDE_RFCBSQ;
   if (ns2_IdCExtrae.Identificacion == NULL)
   {
      m_MsgDesc = origError + " No se obtuvieron datos del WS.";
      return ERR_NOREC_IDENTIFIC;
   }
   if ( ns2_IdCExtrae.Identificacion->c_USCORESit_USCOREDom == NULL ||
        ns2_IdCExtrae.Identificacion->c_USCORESit_USCOREDom[0] == 0 )
   {
      m_MsgDesc = origError + " No se recuper� informaci�n del domicilio.";
      return ERR_NOREC_DOMICILIO;
   }
   return atoi(ns2_IdCExtrae.Identificacion->c_USCORESit_USCOREDom);
}

std::string CSolDatosWS_IDC::getDescDom()
{
   std::string origError = "WS_IDC: getDesDom.";

   if (!ValObjInic(origError))
      return "";
   if ( !m_RFCcargado )
   {
      m_MsgDesc = origError + " No se indic� el RFC a consultar";
      return "";
   }
   if (ns2_IdCExtrae.Identificacion == NULL)
   {
      m_MsgDesc = origError + "No se tiene datos de Identificaci�n.";
      return "";
   }
   if (ns2_IdCExtrae.Identificacion->d_USCORESit_USCOREDom == NULL ||
       ns2_IdCExtrae.Identificacion->d_USCORESit_USCOREDom[0] == 0 )
   {
      m_MsgDesc = origError + "No se tiene la descripci�n de la Situaci�n de Domicilio.";
      return "";
   }
   return std::string(ns2_IdCExtrae.Identificacion->d_USCORESit_USCOREDom);
}

//##### Extrae el domicilio #####
int CSolDatosWS_IDC::getStrDomicilio(std::string *domicilio)
{
   std::string origError = "WS_IDC: getStrDomicilio.";

   *domicilio = "";
   if (!ValObjInic(origError))
      return NO_INICIALIZADO;
   if ( !m_RFCcargado )
   {
      m_MsgDesc = origError + " No se indic� el RFC a consultar";
      return RFC_NO_CARGADO;
   }
   if (ns2_IdCExtrae.Ubicacion == NULL)
      return ERR_NOREC_DOMICILIO;
  
   if (ns2_IdCExtrae.Ubicacion->Calle != NULL &&
       ns2_IdCExtrae.Ubicacion->Calle[0] != 0 ) 
      *domicilio = std::string(ns2_IdCExtrae.Ubicacion->Calle);
   if (ns2_IdCExtrae.Ubicacion->n_USCOREExterior != NULL &&
       ns2_IdCExtrae.Ubicacion->n_USCOREExterior[0] != 0 )
      *domicilio = *domicilio + " " + std::string(ns2_IdCExtrae.Ubicacion->n_USCOREExterior);
   if (ns2_IdCExtrae.Ubicacion->n_USCOREInterior != NULL &&
       ns2_IdCExtrae.Ubicacion->n_USCOREInterior[0] != 0 )
      *domicilio = *domicilio + " " + std::string(ns2_IdCExtrae.Ubicacion->n_USCOREInterior);
   if (ns2_IdCExtrae.Ubicacion->d_USCOREColonia != NULL &&
       ns2_IdCExtrae.Ubicacion->d_USCOREColonia[0] != 0 )
      *domicilio = *domicilio + " " + std::string(ns2_IdCExtrae.Ubicacion->d_USCOREColonia);
   if (ns2_IdCExtrae.Ubicacion->cp != NULL &&
       ns2_IdCExtrae.Ubicacion->cp[0] != 0 )
      *domicilio = *domicilio + " " + std::string(ns2_IdCExtrae.Ubicacion->cp);
   if (ns2_IdCExtrae.Ubicacion->d_USCOREEnt_USCOREFed != NULL &&
       ns2_IdCExtrae.Ubicacion->d_USCOREEnt_USCOREFed[0] != 0 )
      *domicilio = *domicilio + " " + std::string(ns2_IdCExtrae.Ubicacion->d_USCOREEnt_USCOREFed);
   if (ns2_IdCExtrae.Ubicacion->d_USCORELocalidad != NULL &&
       ns2_IdCExtrae.Ubicacion->d_USCORELocalidad[0] != 0 )
      *domicilio = *domicilio + " " + std::string(ns2_IdCExtrae.Ubicacion->d_USCORELocalidad);
   if (ns2_IdCExtrae.Ubicacion->d_USCOREMunicipio != NULL &&
       ns2_IdCExtrae.Ubicacion->d_USCOREMunicipio[0] != 0 )
      *domicilio = *domicilio + " " + std::string(ns2_IdCExtrae.Ubicacion->d_USCOREMunicipio);
   return 0;
}

/*
int CSolDatosWS_IDC::getSitFiscal(const char* RFCBsq)
{
   std::string origError = "WS_IDC:getSitFiscal("+ std::string(RFCBsq) +")";
   if (!ValObjInic(origError))
      return NO_INICIALIZADO;
   if ( !m_RFCcargado )
   {
      m_MsgDesc = origError + " No se indic� el RFC a consultar";
      return RFC_NO_CARGADO; 
   }
   if ( !CoincideRFCObj(RFCBsq) )
      return NO_COINCIDE_RFCBSQ;
   if (ns2_IdCExtrae.Identificacion == NULL)
   {
      m_MsgDesc = origError + " No se obtuvo datos de Identificaci�n del WS";
      return ERR_NOREC_IDENTIFIC;
   }
   if ( ns2_IdCExtrae.Identificacion->c_USCOREdet_USCORESit_USCORECont == NULL ||
        ns2_IdCExtrae.Identificacion->c_USCOREdet_USCORESit_USCORECont[0] == 0 )
   {
      m_MsgDesc = origError + " No se recuper� informaci�n de la clave de la situaci�n del Contribuyente.";
      return ERR_NOREC_CSITFIS;
   }
   return  atoi(ns2_IdCExtrae.Identificacion->c_USCOREdet_USCORESit_USCORECont);
}
*/

//SERR: Modificado para extraer la cve de Situaci�n Fiscal
bool CSolDatosWS_IDC::getSitFiscal(const char* RFCBsq, int &iSitFiscal)
{
   bool  bResp  = false;
   std::string origError = "WS_IDC:getSitFiscal("+ std::string(RFCBsq) +")";

   if (ValObjInic(origError))
   {
      if ( m_RFCcargado )
      {
         if ( CoincideRFCObj(RFCBsq) )
         {
            if (ns2_IdCExtrae.Identificacion != NULL)
            {
               if ( ns2_IdCExtrae.Identificacion->c_USCOREdet_USCORESit_USCORECont != NULL ||
                    ns2_IdCExtrae.Identificacion->c_USCOREdet_USCORESit_USCORECont[0] != 0 )
               {
                  bResp = true;
                  iSitFiscal =  atoi(ns2_IdCExtrae.Identificacion->c_USCOREdet_USCORESit_USCORECont);
               }
               else
               { m_MsgDesc = origError + " No se recuper� informaci�n de la clave de la situaci�n del Contribuyente."; 
                 iSitFiscal = ERR_NOREC_CSITFIS; 
               }
            }
            else
            { m_MsgDesc = origError + " No se obtuvo datos de Identificaci�n del WS";
              iSitFiscal = ERR_NOREC_IDENTIFIC;
            }
         }
         else iSitFiscal = NO_COINCIDE_RFCBSQ;
      }
      else
      { m_MsgDesc = origError + " No se indic� el RFC a consultar"; 
        iSitFiscal = RFC_NO_CARGADO;
      }
   }
   else iSitFiscal = NO_INICIALIZADO;

   return bResp;
}


bool CSolDatosWS_IDC::getDesSitFis(std::string *DesSitFisc)
{
   std::string origError = "WS_IDC: getDesSitFisc.";

   *DesSitFisc = "";
   if (!ValObjInic(origError))
      return false;             //GHM (090327)
   // return NO_INICIALIZADO;   //PENDIENTE: Falta revisar l�gica para el manejo del error
   if ( !m_RFCcargado )
   {
      m_MsgDesc = origError + " No se indic� el RFC a consultar";
      return false;             //GHM (090327)
   //   return RFC_NO_CARGADO;
   }
   if ( m_RFCObj == NULL )
      return false;             //GHM (090327)
   //   return RFC_NO_CARGADO;
   if ( ns2_IdCExtrae.Identificacion->d_USCORESit_USCORECont == NULL)
   {
      *DesSitFisc = "";
      m_MsgDesc = "No se recuper� la descripci�n de la situaci�n del contribuyente";
      return false;
   }
   *DesSitFisc = std::string(ns2_IdCExtrae.Identificacion->d_USCORESit_USCORECont);
   return true;
}

bool CSolDatosWS_IDC::getTipoContrib(std::string *TipoCont)
{
   int  ldato = 30;
   char dato[30];
   
   if ( !transfTpoContr( ns2_IdCExtrae.Identificacion->t_USCOREpersona[0], dato, ldato) )
      return false;
   dato[ldato] = 0;
   *TipoCont = std::string(dato);     
   return true;
}

bool CSolDatosWS_IDC::CalcStatusRFC()
{
   char cRFCVig[14];
   memset(cRFCVig,0,sizeof(cRFCVig));

   if ( (ns2_IdCExtrae.RFC_USCOREVigente == NULL) || (ns2_IdCExtrae.RFC_USCOREVigente[0] == 0) )
   {
      m_statusRFC=-1;
      m_MsgDesc = "Por favor verifique el RFC, no se tiene RFC Vigente registrado";
      return false;
   }
   sprintf( cRFCVig, "%s", ns2_IdCExtrae.RFC_USCOREVigente);
   if ( strcmp(cRFCVig, m_RFCObj) != 0 )
   {
      m_statusRFC = 2;
      m_MsgDesc = "Por favor, verifique el RFC ya que no es el �ltimo expedido";
      return false;
   }
   m_statusRFC=1;
   return true;
}

bool CSolDatosWS_IDC::getRFCOrig(char* RFCOrig)
{
   m_MsgDesc  = "";
   if ( (ns2_IdCExtrae.RFC_USCOREOriginal == NULL) || (ns2_IdCExtrae.RFC_USCOREOriginal[0] == 0) )
   {
      if ( (ns2_IdCExtrae.RFC_USCOREVigente == NULL) || (ns2_IdCExtrae.RFC_USCOREVigente[0] == 0) )
      {
          m_MsgDesc = "No se obtuvo el RFC Original";     
          return false;
      }
      sprintf( RFCOrig, "%s", ns2_IdCExtrae.RFC_USCOREVigente ); 
   }
   else
       sprintf( RFCOrig, "%s", ns2_IdCExtrae.RFC_USCOREOriginal );
   return true;
}

bool CSolDatosWS_IDC::getCURP(char* CURP )
{
   m_MsgDesc = "";
   if ( (ns2_IdCExtrae.Identificacion == NULL) || (ns2_IdCExtrae.Identificacion->CURP == NULL) )
   {
      m_MsgDesc = "No se obtuvo la CURP)";
      return false; 
   }
   sprintf( CURP, "%s", ns2_IdCExtrae.Identificacion->CURP);
   return true;
}

bool CSolDatosWS_IDC::getRegimen(int indice, std::string *cveRegimen, bool *Activo )
{
   m_MsgDesc = "";
   if ( (ns2_IdCExtrae.Regimenes == NULL) || (ns2_IdCExtrae.Regimenes[indice]->c_USCORERegimen == NULL) )
   {
      m_MsgDesc = "No se encontraron datos del Regimen";
      return false;
   }
   *cveRegimen = std::string(ns2_IdCExtrae.Regimenes[indice]->c_USCORERegimen);
   *Activo = valVigencia("REGIMEN", indice, ns2_IdCExtrae.Regimenes[indice]->c_USCORERegimen,
                         ns2_IdCExtrae.Regimenes[indice]->f_USCOREAlta_USCOREReg,
                         ns2_IdCExtrae.Regimenes[indice]->f_USCOREBaja_USCOREReg);
   return true;
}

bool CSolDatosWS_IDC::getObligacion(int indice, std::string *Obligacion, bool *Activo )
{
   m_MsgDesc = "";
   if ( (ns2_IdCExtrae.Obligaciones == NULL) || (ns2_IdCExtrae.Obligaciones[indice]->c_USCOREObligacion == NULL) )
   {
      m_MsgDesc = "No se encontraron datos de la Obligaci�n";
      return false;
   }
   *Obligacion = std::string(ns2_IdCExtrae.Obligaciones[indice]->c_USCOREObligacion);
   *Activo = valVigencia("OBLIGACION", indice, ns2_IdCExtrae.Obligaciones[indice]->c_USCOREObligacion,
                         ns2_IdCExtrae.Obligaciones[indice]->f_USCOREAlta_USCOREOblig, 
                         ns2_IdCExtrae.Obligaciones[indice]->f_USCOREBaja_USCOREOblig);

   return true;
}

bool CSolDatosWS_IDC::getRol(int indice, std::string *Rol, bool *Activo )
{
   m_MsgDesc = "";
   if ( (ns2_IdCExtrae.Roles == NULL) || (ns2_IdCExtrae.Roles[indice]->c_USCORERol == NULL) )
   {
      m_MsgDesc = "No se encontraron datos del Rol";
      return false;
   }
   *Rol = std::string(ns2_IdCExtrae.Roles[indice]->c_USCORERol);
   *Activo = valVigencia("ROL", indice, ns2_IdCExtrae.Roles[indice]->c_USCORERol,
                         ns2_IdCExtrae.Roles[indice]->f_USCOREAlta_USCORERol,
                         ns2_IdCExtrae.Roles[indice]->f_USCOREBaja_USCORERol);
   return true;
}

bool CSolDatosWS_IDC::getActiv(int indice, std::string *cveActiv, bool *Activo )
{
   m_MsgDesc = "";
   if ( (ns2_IdCExtrae.Actividades == NULL) || (ns2_IdCExtrae.Actividades[indice]->c_USCOREActividad == NULL) )
   {
      m_MsgDesc = "No se encontraron datos de la Actividad";
      return false;
   }
   *cveActiv = std::string(ns2_IdCExtrae.Actividades[indice]->c_USCOREActividad);
   *Activo = valVigencia("ACTIVIDAD", indice, ns2_IdCExtrae.Actividades[indice]->c_USCOREActividad,
                         ns2_IdCExtrae.Actividades[indice]->f_USCOREAlta_USCOREAct,
                         ns2_IdCExtrae.Actividades[indice]->f_USCOREBaja_USCOREAct);
   return true;
}

int CSolDatosWS_IDC::getNumRegs()
{
   if ( ns2_IdCExtrae.Regimenes == NULL )
   {
      m_MsgDesc = "CSolDatosWS_IDC: ns2_IdCExtrae o __sizeRegimenes son nulos";
      Bitacora->escribe(BIT_ERROR, m_MsgDesc.c_str());
      return -1;
   }
   else
      return ns2_IdCExtrae.__sizeRegimenes;
}

int CSolDatosWS_IDC::getNumObligs()
{
   if ( ns2_IdCExtrae.Obligaciones == NULL )
   {
      m_MsgDesc = "CSolDatosWS_IDC: ns2_IdCExtrae o __sizeObligaciones son nulos";
      Bitacora->escribe(BIT_ERROR, m_MsgDesc.c_str());
      return -1;
   }
   else
      return ns2_IdCExtrae.__sizeObligaciones;
}

int CSolDatosWS_IDC::getNumRoles()
{
   if ( ns2_IdCExtrae.Roles == NULL )
   {
      m_MsgDesc = "CSolDatosWS_IDC: ns2_IdCExtrae o __sizeRoles son nulos";
      Bitacora->escribe(BIT_ERROR, m_MsgDesc.c_str());
      return -1;
   }
   else
      return ns2_IdCExtrae.__sizeRoles;
}

int CSolDatosWS_IDC::getNumActivs()
{
   if ( ns2_IdCExtrae.Actividades == NULL )
   {
      m_MsgDesc = "CSolDatosWS_IDC: ns2_IdCExtrae o __sizeActividades son nulos";
      Bitacora->escribe(BIT_ERROR, m_MsgDesc.c_str());
      return -1;
   }
   else
      return ns2_IdCExtrae.__sizeActividades;
}

//*********************************************************
//  0  EXITO V�lido
// >0        Error que se present� 
int CSolDatosWS_IDC::valCadenaVig(const char fecha[], bool nullValido)
{
   CFecha objFechas;
   std::string auxFecha;
   int fTope = strcmp("2999-12-31", fecha);
   if ( fecha == NULL || fecha[0] == 0 || fTope == 0)
   {
      if (nullValido)
         return EXITO;
      else
         return ERR_NOREC_FECHASOL;
   }
   if ( strlen(fecha) != 10 )
      return ERR_LONGFECHA_WS;
   auxFecha = std::string(fecha) + " 00:00:00";
   int result = objFechas.VigValidaFtoBD( auxFecha );
      return result;
}

bool CSolDatosWS_IDC::valVigencia(const char seccion[], int indice, const char clave[], char* ptrVigInic, char* ptrVigFinal)
{
   int vigInic = -1, vigFinal = -1;
   char dato[10], numReg[5];
   
   sprintf(numReg, "%i", indice);
   if ( (vigInic = valCadenaVig(ptrVigInic)) != SGIFEC_ERR_FECHACADUC )
   {
      sprintf(dato, "%i", vigInic);
      m_MsgDesc = "   Error (" + std::string(dato) + ") al obtener la vigInicial de la secci�n " 
                  + std::string(seccion)+ " [" + std::string(numReg) + "] ->clave(" + std::string(clave) + ")";
      if ( ptrVigInic != NULL )
         m_MsgDesc = m_MsgDesc + " fecha(" + std::string(ptrVigInic) + ")";
      else
         m_MsgDesc = m_MsgDesc + " fecha(null)";
      Bitacora->escribe(BIT_ERROR, m_MsgDesc.c_str());
   }
   if ( (vigFinal = valCadenaVig(ptrVigFinal, true)) != EXITO )
   {
      sprintf(dato, "%i", vigFinal);
      m_MsgDesc = "   Error (" + std::string(dato) + ") al obtener la vigFinal de la secci�n " 
                  + std::string(seccion) + "[" + std::string(numReg)  + "]-> clave(" + std::string(clave) +")";
      if ( ptrVigInic != NULL )
         m_MsgDesc = m_MsgDesc + " fecha(" + std::string(ptrVigInic) + ")";
      else
         m_MsgDesc = m_MsgDesc + " fecha(null)";
      Bitacora->escribe(BIT_ERROR, m_MsgDesc.c_str());
   }
   return ( vigInic == SGIFEC_ERR_FECHACADUC && vigFinal == 0 );
}

