static const char* _CONSULTAIES_CPP_VERSION_ ATR_USED = "ConsultaIES @(#)"\
"DSIC08295AR_ 2007-12-14 ConsultaIES.cpp 1.1.1/1";

//#VERSION: 1.1.1
#include "ConsultaIES.h"
#include <Sgi_ProtegePwd.h>
#include <string>

int main()
{
   iniciaLibOpenSSL();
   CConsultaIES consulta;
   if(consulta.enviaMsg() && consulta.recibeMsg())
   {
       Bitacora->escribe(BIT_INFO, "Se ha completado exitosamente la consulta a la IES.");
   }
   terminaLibOpenSSL();
   return 0;
}

CConsultaIES::CConsultaIES() :
   semaforo(NULL)
{
   Bitacora = new CBitacora(RUTA_LOG);
   MsgBuffer = new uint8[TAM_BUFFER];
   mensajes = new MensajesIES();
   cargaConfiguracion();
   for ( unsigned int i=0; i< condebug.length(); i++)
      std::toupper( condebug[i] );
   if ( condebug.compare("SI") == 0 ) 
   {
      Bitacora->setNivel(BIT_DEBUG);
   }
}

CConsultaIES::~CConsultaIES()
{
   if(Bitacora)
   {
      delete Bitacora;
      Bitacora = NULL;
   }
   if(mensajes)
   {
      delete mensajes;
      mensajes = NULL;
   }
   if(semaforo)
   {
      delete semaforo;
      semaforo = NULL;
   }
   if(MsgBuffer)
   {
      free(MsgBuffer);
      MsgBuffer = NULL;
   }
}

bool CConsultaIES::reservaSemaforo()
{
   if ( !semaforo )
   {
     if(iniciaSemaforo())
     {
        int error = semaforo->bloquea();
        if( error )
           Bitacora->escribe(BIT_ERROR, error, "Error al intentar bloquear el semaforo.");
        else
           return true;
     }
   }
   return false;
}

bool CConsultaIES::liberaSemaforo()
{
   if( !semaforo )
   {
      Bitacora->escribe(BIT_ERROR, "No se puede liberar el semaforo (no hay semaforo)");
   }
   intE error = semaforo->desBloquea();
   if ( !error )
      return true;
   else
      Bitacora->escribe(BIT_ERROR, error, "Error al intentar debloquear el semaforo");
   return false;
}

bool CConsultaIES::iniciaSemaforo()
{
   if( !semaforo )
   {
      semaforo = new CSemaforo(FIFO, 1);
      if(!semaforo)
         Bitacora->escribe(BIT_ERROR, "No se pudo inicializar el semaforo para acceder al AR Mediador");
      else if (!semaforo->getError())
      {
         Bitacora->escribePV(BIT_DEBUG, "Semaforo creado 0X%X - %d", semaforo->getLlave(), semaforo->getId());
         return true;
      }
      else
         Bitacora->escribe(BIT_ERROR, semaforo->getError(), "No se pudo crear el semaforo."); 
   }
   return false;
}

bool CConsultaIES::preparaMsg()
{
   int pid = getpid();
   sprintf((char *)MsgBuffer, "%d ", pid);
   sprintf(Ruta_Fifo, "/tmp/fifoLect.%d", pid);
   if ( !(mkfifo(Ruta_Fifo, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)) )
   {
      Bitacora->escribePV(BIT_INFO, "Se ha creado el fifo %d.", pid );
   } 
  
   int l_password = 128;
   char *password = new char[l_password];
   memset(password, 0, sizeof(char)*l_password);
   if( preparaPasswd(&password, l_password) )
   {
      if( preparaMsgAR(password, l_password, ruta_cert.c_str(), ruta_pvk.c_str(), ruta_cies.c_str() ) )
      {
         int error; 
         if( (error = mensajes->setMensaje(OP_PIDECRTNVOFMT, (char *)no_serie.c_str(), 20)) == 0 )
         {
            free(password); password = NULL;
            int msglen = mensajes->getTamDatos();
            
            char *longitud = (char *)malloc(32);
            sprintf(longitud, "%d ", msglen);


            uint8 *ptr = (uint8 *)mempcpy(MsgBuffer + (strlen((char *)MsgBuffer)), longitud, strlen(longitud));
            memcpy(ptr, mensajes->buffer, msglen);
            free(longitud); longitud = NULL; ptr = NULL;
            
            return true;
         }
         else
            Bitacora->escribe(BIT_ERROR, error, "No se pudo establecer el mensaje"); 
      }
   }
   else
      Bitacora->escribe(BIT_ERROR, "No se pudo desencriptar la la contrase�a.");
   free(password); password = NULL; 
   return false;
}

bool CConsultaIES::
esperarArchivo(const int fd, const time_t tiempo)
{
   timeval temporizador = {tiempo, 0};
   fd_set descriptores;
   FD_ZERO(&descriptores);
   FD_SET(fd, &descriptores);
   int resultado = select(fd+1, &descriptores, NULL, NULL, &temporizador);
   if( resultado <= 0 )
   {
      return false;
   }
   return true;
}

bool CConsultaIES::enviaMsg()
{
   if ( ( access( FIFO, F_OK ) ) == -1 )
      Bitacora->escribe(BIT_ERROR, "No se tiene acceso al fifo.");
   else
   {
      int fifo = open( FIFO, O_WRONLY | O_NONBLOCK ); // mar feb 12 09:41:25 CST 2008 // FALTA revisar el uso de select despues de esto.....
      if ( fifo > 0 )
      {
         if( preparaMsg() )
         {
            if( reservaSemaforo() )
            {
               #ifdef DBG
                  int escrito = write(fifo, (char *)MsgBuffer, (strlen((char *)MsgBuffer) + mensajes->getTamDatos()));
                  Bitacora->escribePV(BIT_DEBUG, "Se escribieron %d bytes en el FIFO.", escrito);
                  std::string letbuff = dumpBuffer("Se envio el mensaje.", MsgBuffer, escrito);
                  Bitacora->escribe(BIT_DEBUG, letbuff.c_str());
               #else
                  int escrito = write(fifo, (char *)MsgBuffer, (strlen((char *)MsgBuffer) + mensajes->getTamDatos()));
                  Bitacora->escribePV(BIT_INFO, "Se escribieron %d bytes en el FIFO.", escrito);
               #endif
               close( fifo ); 
               liberaSemaforo();
               return true;
            }
         }
         else
            Bitacora->escribePV(BIT_ERROR, "El archivo tardo demasiado en estar listo.");
         close( fifo );
      }
      else 
         Bitacora->escribePV(BIT_ERROR, "No se pudo abrir la tuberia con el mediador. (errno: %d)", errno);
   }
   return false;
}

bool CConsultaIES::recibeMsg()
{
   int fifo_lect = open(Ruta_Fifo, O_RDONLY | O_NONBLOCK);
   if ( esperarArchivo(fifo_lect, timeout) )
   {
      if( fifo_lect > 0 )
      {
         int leido = read(fifo_lect, mensajes->buffer, sizeof(mensajes->buffer));
         close(fifo_lect);
         if ( !unlink(Ruta_Fifo) )
         {
            Bitacora->escribePV(BIT_INFO, "Se ha borrado el fifo %s", Ruta_Fifo);
         }
         #ifdef DBG
            Bitacora->escribePV(BIT_DEBUG, "Se leyeron %d bytes del FIFO.", leido);

         //GHM (080107): Se modifica por el cambio del tipo de funci�n y que se almaceno en Sgi_MsgPKI
         //dumpBuffer("Se recibio el mensaje.", mensajes->buffer, leido);
         //>> +
            std::string letbuff = dumpBuffer("Se recibio el mensaje.", mensajes->buffer, leido);
            Bitacora->escribe(BIT_DEBUG, letbuff.c_str());
         //<< +
         #endif
         if ( leido > 0 )
         {
            int spcb = 0;
            int posc = 0;
            while ( spcb < 2 )
            {
                if ( mensajes->buffer[posc] == ' ' )
                   spcb++;
                posc++;
            }
            char buffer[TAM_DATOS];
            memcpy(buffer, mensajes->buffer + posc ,sizeof(mensajes->buffer) - posc );
              
            mensajes->getDatos(buffer, leido -  posc );

            char firma[TAM_FIRMA];
            char estado[3];
            char f_cad[20];
            char f_regies[20];
            char f_gmen[20];
            char certificado[2048];
            char *estado_c;
            int  lfirma = TAM_FIRMA;
            int  lestado = 3;
            int  lf_cad = 20;
            int  lf_regies = 20;
            int  lf_gmen = 20;
            int  lcertificado = 2048;
            
            if ( mensajes->tipOperacion() == OP_CRTNOEXISTE || mensajes->tipOperacion() == OP_AUTNOCONN )
               Bitacora->escribePV(BIT_ERROR, "No existe el certificado solicidado (%s)", no_serie.c_str());
            else
            { 
               intE error = mensajes->getMensajeIES(OP_REGCRTNVOFMT, firma, &lfirma, estado, &lestado, f_cad, &lf_cad, 
                                       f_regies, &lf_regies, f_gmen, &lf_gmen, certificado, &lcertificado);
               if ( !error )
               {
                  switch(estado[0])
                  {
                     case '\000': estado_c = "A"; break;  
                     case '\001': estado_c = "R"; break;  
                     case '\002': estado_c = "L"; break;  
                     case '\003': estado_c = "C"; break;  
                  }
                  
                  Bitacora->escribePV(BIT_DEBUG, "Se recibio el estado: %s del cert. %d tama�o %d", estado_c, no_serie.c_str(), lcertificado);
                  time_t t_cad = atoi(f_cad);
                  struct tm *fechaCad    = gmtime(&t_cad);
                  Bitacora->escribePV(BIT_DEBUG, "Fecha de Caducidad: %d-%0.2d-%0.2d %d:%d:%d",
                                      fechaCad->tm_year+1900, fechaCad->tm_mon+1, fechaCad->tm_mday,
                                      fechaCad->tm_hour, fechaCad->tm_min, fechaCad->tm_sec);
                  time_t t_regies = atoi(f_regies);
                  struct tm *fechaRegIES = gmtime(&t_regies);
                  Bitacora->escribePV(BIT_DEBUG, "Fecha de Registro IES: %d-%0.2d-%0.2d %d:%d:%d",
                                      fechaRegIES->tm_year+1900, fechaRegIES->tm_mon+1, fechaRegIES->tm_mday,
                                      fechaRegIES->tm_hour, fechaRegIES->tm_min, fechaRegIES->tm_sec);
                  time_t t_gmen = atoi(f_gmen);
                  struct tm *fechaMens   = gmtime(&t_gmen);
                  Bitacora->escribePV(BIT_DEBUG, "Fecha de generacion del mensaje: %d-%0.2d-%0.2d %d:%d:%d",
                                      fechaMens->tm_year+1900, fechaMens->tm_mon+1, fechaMens->tm_mday,
                                      fechaMens->tm_hour, fechaMens->tm_min, fechaMens->tm_sec);
               } 
               else
               {
                  Bitacora->escribePV(BIT_ERROR, "Ocurrio un error al obtener los datos del mensaje (%d)", error);
               }
            }
            return true;
         }
         else
            Bitacora->escribe(BIT_ERROR, "No se pudo leer la respuesta");
      }
      else
         Bitacora->escribe(BIT_ERROR, errno, "No se pudo abrir el fifo de lectura.");
   }
   else
   {
      if( !close(fifo_lect) )
      {
         if( unlink(Ruta_Fifo) == -1 )
            Bitacora->escribePV(BIT_ERROR, "No se pudo borrar la tuberia de lectura %s (errno: %d)", Ruta_Fifo, errno);
      }
      else
         Bitacora->escribePV(BIT_ERROR, "No se pudo cerrar la tuberia de lectura (errno: %d)", errno);
      Bitacora->escribe(BIT_ERROR, "El mediador tardo demasiado en contestar.");
   }
   return false;
}

bool CConsultaIES::preparaPasswd(char **pass, int &l_pass)
{
   if( desencripta((uint8 *)s_passwd.c_str(), s_passwd.length(), (uint8 *)*pass, &l_pass) )
      return true;
   Bitacora->escribe(BIT_ERROR, "Ocurrio un error al intentar desencriptar la contrase�a de la llave privada.");
   return false;
}

bool CConsultaIES::preparaMsgAR(const char *password, const int l_password, const char *ruta_cert, const char *ruta_key, const char *cer_ies)
{
   int error = 0;
   error = mensajes->Inicia( IES, (char *)ruta_cert, (char *)ruta_key, (char *)password, l_password, (char *)cer_ies );
   if( !error )
      return true;
   else
      Bitacora->escribe(BIT_ERROR, error, "Ocurrio un error al intentar iniciar MensajesAR.");
   return false;
}

bool CConsultaIES::preparaMsgAR(char *password, const int l_password)
{
   int error = 0;
   if ( !mensajes )
   {
      mensajes = new MensajesIES();
   }
   char *p_ruta     = (char *)ruta_cert.c_str();
   char *p_pvk      = (char *)ruta_pvk.c_str();
   char *p_rutacies = (char *)ruta_cies.c_str();

   Bitacora->escribePV(BIT_INFO, "Cert : %s \nLlavePriv : %s \n certiIes : %s \n ",p_ruta,p_pvk,p_rutacies);

   error = mensajes->Inicia( IES, p_ruta, p_pvk, (char *)password, l_password, p_rutacies );
   if ( (error == 0) )
      return true;
   else
      Bitacora->escribe(BIT_ERROR, error, "Ocurrio un error al intentar iniciar MensajesAR."); 
   Bitacora->escribePV(BIT_INFO, "Cert : %s \nLlavePriv : %s \n certiIes : %s \n", p_ruta, p_pvk, p_rutacies);
   return false;
}

bool CConsultaIES::cargaConfiguracion()
{
   CConfigFile *configuracion = new CConfigFile(RUTA_CONF);
   if( configuracion )
   {
      int error = 0;
      if( (error = configuracion->cargaCfgVars()) != 0 )
      { 
         delete configuracion;
         configuracion = NULL;	 
         Bitacora->escribe(BIT_ERROR, "No se pudieron cargar las variables de configuracion.");
      }
      else
      {
         configuracion->getValorVar("[LLAVES_MENSAJES_SRV]", "CERT", &ruta_cert);
         configuracion->getValorVar("[LLAVES_MENSAJES_SRV]", "PRIVK", &ruta_pvk);
         configuracion->getValorVar("[LLAVES_MENSAJES_SRV]", "PWDPK", &s_passwd);
         configuracion->getValorVar("[LLAVES_MENSAJES_SRV]", "IES", &ruta_cies);
         configuracion->getValorVar("[CONFIG]", "DEBUG", &condebug);
         configuracion->getValorVar("[CONFIG]", "SERIE", &no_serie);
         configuracion->getValorVar("[CONFIG]", "TIMEOUT", &s_timeout);
         timeout = atoi(s_timeout.c_str());
         delete configuracion;
         configuracion = NULL;	 
         return true;
      }
   }
   return false;
}

