#ifndef _CONSULTAIES_H_
#define _CONSULTAIES_H_
static const char* _CONSULTAIES_H_VERSION_ ATR_USED = "ConsultaIES @(#)"\
"DSIC08295AR_ 2007-12-14 ConsultaIES.h 1.1.1/1";

//#VERSION: 1.1.1
#include <Sgi_ConfigFile.h>
#include <Sgi_Bitacora.h>
#include <Sgi_Semaforo.h>
#include <Sgi_MsgPKI.h>
#include <sys/select.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string>

#ifdef DBG
   #define RUTA_CONF "/usr/local/SAT/PKI/etc/ConsultaIES_dbg.cfg"
   #define RUTA_LOG  "/var/local/SAT/PKI/ConsultaIES_dbg.log"
   #define FIFO	   "/tmp/fifod.serv"
   #define FIFO_LEC  "/tmp/FifoLect"
#else
   #define RUTA_CONF "/usr/local/SAT/PKI/etc/ConsultaIES.cfg"
   #define RUTA_LOG  "/var/local/SAT/PKI/ConsultaIES.log"
   #define FIFO	   "/tmp/fifo.serv"
   #define FIFO_LEC  "/tmp/FifoLect"
#endif

#define TAM_BUFFER  256

CBitacora *Bitacora;

class CConsultaIES
{
   private :
   char           Ruta_Fifo[256];
   uint8          *MsgBuffer;
   time_t         timeout;
   CSemaforo      *semaforo;
   MensajesIES    *mensajes;
   std::string    ruta_cies;
   std::string    ruta_cert;
   std::string    ruta_pvk;
   std::string    s_passwd;
   std::string    s_timeout;
   std::string    no_serie;
   std::string    condebug;

/*   char firma[TAM_FIRMA];
   int  lfirma;
   char estado[3];
   int  lestado;
   char f_cad[20];
   int  lf_cad;
   char f_regies[20];
   int  lf_regies;
   char f_gmen[20];
   int  lf_gmen;
   char certificado[2048];
   int  lcertificado; */

   protected :
   bool reservaSemaforo();
   bool liberaSemaforo();
   bool iniciaSemaforo();
   bool cargaConfiguracion();
   bool esperarArchivo(const int descriptor, const time_t tiempo);
   public :
   CConsultaIES();
   ~CConsultaIES();
   bool preparaPasswd(char **, int &);
   bool preparaMsgAR(const char *password, const int l_password, const char *ruta_cert, const char *ruta_key, const char *cer_ies);
   bool preparaMsgAR(char *password, const int l_password);
   bool enviaMsg();
   bool recibeMsg();
   bool preparaMsg();
};

#endif
