static const char* _SHK2PKCS8_CPP_VERSION_ ATR_USED = "@(#) Shk2Pkcs8 ( L : DSIC10XXXAR_ : Shk2Pkcs8.cpp : 1.0.0 : 0 : 08/10/10)";

/*########################################################################################
  ###  PROYECTO:              Aplicaci�n                                               ###
  ###  MODULO:                Shk2Pkcs8: Programa principal que maneja las operaciones ###
  ###                                    para transformar las secciones de llave en    ###
  ###                                    memoria compartida a un archivo Pkcs8         ###
  ###  DESARROLLADORES:                                                                ###
  ###                         Gudelia Hern�ndez Molina     GHM                         ###
  ###                                                                                  ###
  ###  FECHA DE INICIO:       octubre 2010                                             ###
  ###                                                                                  ###
  ########################################################################################
*/

/*#################################################################################
   VERSION:
      V.1.00      (20101008 - 20101014) GHM: Versi�n original

   CAMBIOS:

#################################################################################*/
#include "Def_Shk2Pkcs8.h"
#include <CFunciones.h>

//#################################################################################
std::string archKeys, archOut;

//#################################################################################
void DespliegaUso(char *argv[])
{
   std::cout << "\nUso: " << std::endl << "\t" << argv[0] << " -i ARCH_LLAVES -o ARCH_SALIDA\n" << std::endl << std::endl;
   std::cout << "Donde ARCH_LLAVES: Ruta y nombre del archivo que contiene la ubicaci�n de las llaves que participan en el proceso" << std::endl;
   std::cout << "      ARCH_SALIDA: Ruta y nombre del archivo donde se almacenar� la llave privada en formato PKCS8" << std::endl << std::endl;
}

//#################################################################################
/*
   -i : Path del archivo de llaves (/ruta/nombre)
   -o : Path del archivo de salida (/ruta/nombrePKCS8)
*/
int ProcesaParametros( int argc, char *argv[] )
{
   int         opt    = 0, ierror = EXITO;
   size_t      l_dato = 0;
   char        *dato  = NULL;
   
   if ( argc == 1 )
   {
      DespliegaUso( argv );
      return ERR_SIN_PARAM;
   }
   while( (opt = getopt( argc, argv, "i:o:" )) != -1 )
   {
     l_dato = strlen( optarg );
     dato = (char *)malloc( sizeof(char) * l_dato + 1 );
     dato[l_dato] = 0x00;
     strncpy(dato, optarg, l_dato);
      switch( opt )
      {
         case 'i' :
            archKeys = std::string(dato);
            break;
         case 'o' :
            archOut = std::string(dato);
            break;
      }
      free(dato);
   }
#ifdef DBG
   std::cout << "Arch. Llaves: " << archKeys.c_str() << std::endl;
   std::cout << "Arch. Salida: " << archOut.c_str() << std::endl;
#endif
   if ( !archKeys.empty() )
   {
      if ( !archOut.empty() )
         ierror = EXITO;
      else
      {
         std::cout << "\nNo se proporcion� el archivo de Salida" << std::endl;
         ierror = ERR_FALTA_ARCHOUT;
      }
   }
   else
   {  
      std::cout << "\nNo se proporcion� el archivo de Llaves" << std::endl;
      ierror = ERR_FALTA_ARCHKEYS;
   }
   if ( ierror != EXITO )
      DespliegaUso( argv );
   return ierror;
}

//#################################################################################
int main (int argc, char* argv[])
{
   std::string descError = "";
   CFunciones funcGrales;
   iniciaLibOpenSSL();
   int resp = ProcesaParametros( argc, argv );
   if ( resp != EXITO )
      return resp;
   resp = funcGrales.procesaKeys(archKeys, archOut);
   if ( resp == EXITO )
       std::cout << "PROCESO TERMINADO EXITOSAMENTE" << std::endl << std::endl;
   terminaLibOpenSSL();
   return resp;
}
