static const char* _CARMALLAVE_CPP_VERSION_ ATR_USED = "@(#) Shk2Pkcs8 ( L : DSIC10XXXAR_ : CArmaLlave.cpp : 1.0.0 : 0 : 08/10/10)";

/*##############################################################################
  ###  DESARROLLADORES:                                                      ###
  ###                         Gudelia Hernández Molina     GHM               ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       octubre 2010                                   ###
  ###                                                                        ###
  ##############################################################################
*/
/*#################################################################################
   VERSION:
      V.1.00      (20101008 - 20101014) GHM: Versión original

   CAMBIOS:

#################################################################################*/

#define _CARMALLAVE_CPP_
#include "CArmaLlave.h"

//Constructor
CArmaLlave::CArmaLlave()
{
   objValida = new CValCadena();
   inumKeys  = 0;
   strError  = "";
}

//Destructor
CArmaLlave::~CArmaLlave()
{
   if (objValida)
   {
      delete objValida;
      objValida = NULL;
   }
}

//Proporciona el mensaje de error
std::string CArmaLlave::getError()
{
   return strError;
}

//#################################################################################
int CArmaLlave::setPrivKeyRSA(RSA_PRIVADA privadaKey, RSA **privKeyRSA)
{
   char cdato[30];
   memset(cdato, 0, 30);
   sprintf(cdato, "%i", privadaKey.bits);
   (*privKeyRSA)->n = BN_new(); 
   int idato = BN_dec2bn(&(*privKeyRSA)->n, cdato);
   if ( (*privKeyRSA)->n <= 0 )
      return ERR_NO_CARGA_BITS;
   (*privKeyRSA)->e = BN_bin2bn(privadaKey.exponenteP, sizeof(privadaKey.exponenteP), NULL);
   if ( (*privKeyRSA)->e <= 0 )
      return ERR_NO_EXP_PUB;
   (*privKeyRSA)->d = BN_bin2bn(privadaKey.exponentePv, sizeof(privadaKey.exponentePv), NULL);
   if ( (*privKeyRSA)->d <= 0 )
      return ERR_NO_EXP_PRIV;
   (*privKeyRSA)->p = BN_bin2bn(privadaKey.primo[0], sizeof(privadaKey.primo[0]), NULL);
   if ( (*privKeyRSA)->p <= 0 )
      return ERR_NO_PRIMO; 
   (*privKeyRSA)->q = BN_bin2bn(privadaKey.primo[1], sizeof(privadaKey.primo[1]), NULL);
   if ( (*privKeyRSA)->q <= 0 )
      return ERR_NO_PRIMO2;
   (*privKeyRSA)->dmp1 = BN_bin2bn(privadaKey.primoExp[0], sizeof(privadaKey.primoExp[0]), NULL);
   if ( (*privKeyRSA)->dmp1 <= 0 )
      return ERR_PRIME_EXP;
   (*privKeyRSA)->dmq1 = BN_bin2bn(privadaKey.primoExp[1], sizeof(privadaKey.primoExp[1]), NULL);
   if ( (*privKeyRSA)->dmq1 <= 0 )
      return ERR_PRIME_EXP2;
   (*privKeyRSA)->iqmp = BN_bin2bn( (const unsigned char *) &privadaKey.coeficiente, 
                                    sizeof(privadaKey.coeficiente), NULL);
   if ( (*privKeyRSA)->iqmp <= 0 )
      return ERR_NO_COEF;
   return EXITO;
}

//#################################################################################
int CArmaLlave::getTamSegmento(unsigned char *uSegmento, int iTam)
{
   int segmento = -1;

   if( uSegmento[0] == 0x30 )
   {
      if( uSegmento[1] == 0x82 )
         segmento = ((uint16(uSegmento[2]) << 8) | uint16(uSegmento[3]));
      else
         segmento = iTam -2;
   }
   return segmento;
}

int CArmaLlave::getTamSegAnt( unsigned char *uSegmento, uint8 *cars_lng, uint16 *lng_datos )
{
   if (uSegmento[0] == 0x30)
   {
      if ( (uSegmento[1] < 0x80) )
      {
         *cars_lng  = 1;
         *lng_datos = uSegmento[1];
      }
      else if (uSegmento[1] == 0x81)
      {
         *cars_lng  = 2;
         *lng_datos = uSegmento[2];
      }
      else if (uSegmento[1] == 0x82)
      {
         *cars_lng  = 3;
         *lng_datos = (uint16(uSegmento[2]) << 8) | uint16(uSegmento[3]);
      }else
      {
         strError = "La llave no corresponde al Formato Anterior. ";
         return ERR_FTO_ANTERIOR;
      }
   }else
   {
      strError = "La llave no tiene el Formato Anterior. ";
      return ERR_FTO_ANTERIOR;
   }
   return EXITO;
}

//#################################################################################
bool CArmaLlave::armaKeyAnt(std::string prarchKey, char *pwd, unsigned char **uSegmentos, int iTamSeg ,int iNumSeg)
{
   int          ierror, lpwd = 0;
   RSA_PRIVADA  privada;
   SGIRSA       objRSA;
   RSA         *rsaLlavePriv= NULL;
   
   CSecretoCompartido  *sc = new CSecretoCompartido(iNumSeg, iNumSeg);
   
   lpwd = strlen(pwd);
   if (sc->decodifica(uSegmentos, iTamSeg, (uint8*) &privada) )
   {
      rsaLlavePriv = RSA_new();
      ierror = setPrivKeyRSA(privada, &rsaLlavePriv);
      if( ierror != 0 )
         strError = "Ocurrio el error: " + objValida->int2string(ierror) + " al convertir la llave privada reconstruida.";
      else
      {
         FILE* fpp = fopen(prarchKey.c_str(), "wb");
         if (!fpp)
            ierror = ERR_PKCS7_NOARCH;
         else
            ierror = (int) objRSA.CodPvKey1(rsaLlavePriv, (uchar*) pwd, lpwd, fpp, AES_256_CBC_ALG, DER_FORMAT);
         RSA_free(rsaLlavePriv);
      }
      delete sc;
      sc = NULL;
   }
   else
      strError = "No se descifraron correctamente las llaves del formato anterior";
   return (ierror==EXITO);
}

//#################################################################################
bool CArmaLlave::armaKeyPKCS7(std::string prarchKey, char *pwd, unsigned char **uSegmentos, int iTamSeg ,int iNumSeg)
{
   int       ierror, lpwd = 0;
   SGIRSA    objRSA;
   EVP_PKEY *evpLlavePriv = NULL;
   int       iTamLlave  = (iTamSeg/2);

   CSecretoCompartido  *sc = new CSecretoCompartido(iNumSeg, iNumSeg);
   const unsigned char *cLlavePriv = new unsigned char[iTamSeg];

   lpwd = strlen(pwd);
   if (sc->decodifica(uSegmentos, iTamSeg, (uint8*) cLlavePriv) )
   {
      SGI_SHK* shk = SGI_SHK_new();
      shk  = d2i_SGI_SHK(&shk, &cLlavePriv, iTamLlave);
      if(shk)
      {
         BIO *bLlavePriv  = BIO_new(BIO_s_mem());
         BUF_MEM *memLlavePriv;
         if (bLlavePriv)
         {
            memLlavePriv = BUF_MEM_new();
            BUF_MEM_grow(memLlavePriv, shk->pkcs8->length);
            memcpy(memLlavePriv->data, shk->pkcs8->data, shk->pkcs8->length);
            BIO_set_mem_buf(bLlavePriv, memLlavePriv, BIO_CLOSE);
            evpLlavePriv = d2i_PKCS8PrivateKey_bio(bLlavePriv, NULL, NULL, shk->pwd->data);
            if (evpLlavePriv)
            {
               RSA *rsaLlavePriv  = EVP_PKEY_get1_RSA(evpLlavePriv);
               if( rsaLlavePriv)
               {
                  FILE* fpp = fopen(prarchKey.c_str(), "wb");
                  if (!fpp)
                     ierror = ERR_PKCS7_NOARCH;
                  else
                     ierror = (int) objRSA.CodPvKey1(rsaLlavePriv, (uchar*) pwd, lpwd, fpp,AES_256_CBC_ALG, DER_FORMAT);
                  BIO_free(bLlavePriv);
                  EVP_PKEY_free(evpLlavePriv);
                  RSA_free(rsaLlavePriv);
                  return true;
               }
               else
                  strError = "Error en la asignación de la memoria del RSA para el PKCS7";
            }
            else
               strError = "Error en la memoria asignada al EVP para el PKCS7";
         }
         else
            strError = "Error en la memoria asignada al BIO para el PKCS7";
      }
      else
      {
         strError = "No se realizo el proceso SHK correctamente para el PKCS7";
         ierror = ERR_get_error();
         char bError[120];
         char *  cError  = ERR_error_string(ERR_get_error(), bError);
         strError = "\n" + std::string(cError);
      }
      SGI_SHK_free(shk);
      delete sc;
      sc = NULL;
   }
   else
      strError = "No se descifraron correctamente las llaves PKCS7";
   return false;
}

//#################################################################################
int CArmaLlave::armaLlavePriv(std::string prarchKey, char *pwd, int tipoFtoKey,
                              unsigned char **uSegmentos, int iTamSeg, int iNumSeg)
{
   bool result = false; 
   if (tipoFtoKey == TIPO_FTO_PKCS7)
      result = armaKeyPKCS7(prarchKey, pwd, uSegmentos, iTamSeg, iNumSeg);
   else 
      result = armaKeyAnt(prarchKey, pwd, uSegmentos, iTamSeg, iNumSeg);
   return result;
}
