#ifndef _CFUNCIONES_H_
#define _CFUNCIONES_H_

static const char* _CFUNCIONES_H_VERSION_ ATR_USED = "@(#) Shk2Pkcs8 ( L : DSIC10XXXAR_ : CFunciones.h : 1.0.0 : 0 : 08/10/10)";

/*##############################################################################
  ###  DESARROLLADORES:                                                      ###
  ###                         Gudelia Hern�ndez Molina     GHM               ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       septiembre 2010                                ###
  ###                                                                        ###
  ##############################################################################
*/
/*#################################################################################
   VERSION:
      V.1.00      (20101008 - 20101014) GHM: Versi�n original

   CAMBIOS:

#################################################################################*/
#include <stdio.h>
#include <iostream>
#include <string>
#include <sys/stat.h>
#include <fstream>
#include <errno.h>

#include <Sgi_OpenSSL.h>

#include "Def_Shk2Pkcs8.h"
#include "CArmaLlave.h"
#include "CValCadena.h"

extern "C"{
#include <openssl/rc4.h>
#include <openssl/md5.h>
}

//#################################################################################
class CFunciones
{
   protected:
      int         inumKeys;
      int         tipoFormatoLlave; 
      CValCadena  *objValida;
      CArmaLlave  *objArmaKey;
      std::string strError; 
      std::string arrayKeys[MAX_NUM_KEYS];
       
      int valFile(std::string rutaArch, int *lbuffer);
      int getFileKeys(std::string rutaArch);
      int getKeyBuff(std::string rutaArch, unsigned char **bufKey, int *lbuffer);
      int getKeyBuffAnt(std::string rutaArch, unsigned char **bufKey, int *lbufKey);
      int solicitaPwd(bool valida, char *pwd );
      int getFtoAnt(std::string pathSegmento, char *pwd, unsigned char **bufKey, int *lbufKey, int *tipo);
      PKCS7* obtienePKCS7(std::string pathSegmento);
      int getFtoPKCS7(std::string pathSegmento, char *pwd, unsigned char **bufKey, int *lbufKey, int *tipo);
      int trataLlave(std::string pathLeido, int iReg, unsigned char **bufKey, int *lbufKey, int *tipo);
      
   public:
      CFunciones();
      ~CFunciones();
 
      std::string getError();
      int procesaKeys(std::string pathFileKeys, std::string pathSalida);
};
#endif //_CFUNCIONES_H_
