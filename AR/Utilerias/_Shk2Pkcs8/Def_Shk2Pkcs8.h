#ifndef _DEF_SHK2PKCS8_H_
#define _DEF_SHK2PKCS8_H_

static const char* _DEF_SHK2PKCS8_H_VERSION_ ATR_USED = "@(#) Shk2Pkcs8 ( L : DSIC10XXXAR_ : Def_Shk2Pkcs8.h : 1.0.0 : 0 : 08/10/10)";

/*##############################################################################
  ###  DESARROLLADORES:                                                      ###
  ###                         Gudelia Hern�ndez Molina     GHM               ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       septiembre 2010                                ###
  ###                                                                        ###
  ##############################################################################
*/
/*#################################################################################
   VERSION:
      V.1.00      (20101008 - 20101014) GHM: Versi�n original

   CAMBIOS:

#################################################################################*/

//tipoFormatoLlave
#define NO_EVALUADO                 -1
#define TIPO_FTO_ANTERIOR            1
#define TIPO_FTO_PKCS7               2

#define MAX_TAM_LINEA              255
#define MAX_TAM_PWD                255
#define MAX_TAM_KEY               1024*5
#define MAX_NUM_KEYS                20
#define MAX_NUM_INTENTOS             2

#define EXITO                        0
#define ERR_SIN_PARAM               -1 
#define ERR_FALTA_ARCHKEYS          -2
#define ERR_FALTA_ARCHOUT           -3
#define ERR_SPACE_MEM               -4
#define ERR_PATH_KEY                -5
#define ERR_KEYS_DIF_FTO            -6 

#define ERR_PWD_CAPTURA            -10
#define ERR_PWD_INCORRECTO         -11

#define ERR_FTO_ANTERIOR           -20

#define ERR_PKCS7_NOSPACEMEM       -30
#define ERR_PKCS7_NOGETPKCS7       -31
#define ERR_PKC7_NOCRYPT           -32
#define ERR_PKCS7_NODECDATOS       -33
#define ERR_PKCS7_NOARCH           -34

#define ERR_SC_NODECDATOS          -40

//#################################################################################
#endif //_DEF_SHK2PKCS8_H_
