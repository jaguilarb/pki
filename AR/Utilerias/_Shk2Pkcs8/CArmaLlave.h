#ifndef _CARMALLAVE_H_
#define _CARMALLAVE_H_

static const char* _CARMALLAVE_H_VERSION_ ATR_USED = "@(#) Shk2Pkcs8 ( L : DSIC10XXXAR_ : CArmaLlave.h : 1.0.0 : 0 : 08/10/10)";

/*##############################################################################
  ###  DESARROLLADORES:                                                      ###
  ###                         Gudelia Hern�ndez Molina     GHM               ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       septiembre 2010                                ###
  ###                                                                        ###
  ##############################################################################
*/
/*#################################################################################
   VERSION:
      V.1.00      (20101008 - 20101014) GHM: Versi�n original

   CAMBIOS:

#################################################################################*/

#include <string>

#include <Sgi_SecretoCompartido.h>
#include <Sgi_MemCompartida.h>
#include <Sgi_OpenSSL.h>
#include <openssl/asn1t.h>

#include "Def_Shk2Pkcs8.h"
#include "CValCadena.h"
//#################################################################################
typedef struct Sgi_Shk_st
{
   ASN1_OCTET_STRING  *pwd;
   ASN1_STRING        *pkcs8;
} SGI_SHK;

#ifdef _CARMALLAVE_CPP_
DECLARE_ASN1_FUNCTIONS(SGI_SHK);

ASN1_SEQUENCE(SGI_SHK) =
{
   ASN1_SIMPLE(SGI_SHK, pwd  , ASN1_OCTET_STRING),
   ASN1_SIMPLE(SGI_SHK, pkcs8, ASN1_SEQUENCE),
} ASN1_SEQUENCE_END(SGI_SHK)

IMPLEMENT_ASN1_FUNCTIONS(SGI_SHK);

#endif

//#################################################################################

class CArmaLlave
{
   protected:
      std::string strError;
      int         inumKeys;
      CValCadena *objValida;
       
      int  setPrivKeyRSA(RSA_PRIVADA privateK, RSA **privKey);
      bool armaKeyAnt(std::string prarchKey, char *pwd, unsigned char **uSegmentos, int iTamSeg ,int iNumSeg);
      bool armaKeyPKCS7(std::string prarchKey, char *pwd, unsigned char **uSegmentos, int iTamSeg ,int iNumSeg);
   public:
      CArmaLlave();
      ~CArmaLlave();
      
      std::string getError();
      int getTamSegAnt(unsigned char *uSegmento, uint8 *cars_lng, uint16 *lng_datos);
      int getTamSegmento(unsigned char *uSegmento, int iTam);
      int armaLlavePriv(std::string prarchKey, char *pwd, int tipoFtoKey,
                        unsigned char **uSegmentos, int iTamSeg ,int iNumSeg);
};
#endif //_CARMALLAVE_H
