static const char* _DISTRIBUYE_CPP_VERSION_ ATR_USED = "Distribuye @(#)"\
"DSIC07412AR_ 2007-12-14 Distribuye.cpp 1.1.0/0";

//#VERSION: 1.1.0
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <fcntl.h>
//>- ERGL mi� mar 21 11:20:39 CDT 2007
//#include <ArchivosConfg.h>
#include <dirent.h>
#include <Sgi_PKI.h>
//- #include <CConfigFile.h> //- GHM (070430)
#include <Sgi_ConfigFile.h>
#include <errno.h>
#include <Sgi_Bitacora.h>

#define PATH_CONF               "/usr/local/SAT/PKI/etc/"

#ifdef DBG
   #define ARCH_CONF            PATH_CONF "Distribuye_dbg.cfg"
   #define ARCH_LOG             "/var/local/SAT/PKI/Distribuye_dbg.log"
#else
   #define ARCH_CONF            PATH_CONF "Distribuye.cfg"
   #define ARCH_LOG             "/var/local/SAT/PKI/Distribuye.log"
#endif


//###############################################################################

   char cRutaRep[256];
   char cRutaFTP[256];
   char cRutaFTPARA[256];
   char cRutaBDARA[256];
   char cFTP[2];
   char cARA[2];
   char cRutaSellos[256];
   bool bIniciado = false;
   bool esRevocacion = false;

   int  iError;
   
   //>>> ERGL mi� mar 21 11:21:50 CDT 2007
   //>-ArchivosConfg conf;
   CConfigFile *conf;
   CBitacora   *log;


//#################################################################################

bool esVacio(char *cRutaRep , char *cNombre )
{
    struct stat estado;
    char   cRutaNombre[255];
    sprintf(cRutaNombre,"%s/%s",cRutaRep,cNombre); 
    
    int  iError   = stat( cRutaNombre ,&estado);
    if( !iError)
    {
       if (estado.st_size == 0)
       {
         log->escribePV(BIT_ERROR,"El archivo %s tiene tama�o cero. Favor de verificar." ,cRutaNombre);
         return true;
       }
    }
    return false;
}

//#################################################################################
void borrar(char *cArch)
{
   char aBorrar[256];
   sprintf(aBorrar ,"%s%s",cRutaRep,cArch);
   remove(aBorrar);
}
         

//#################################################################################

int escribeMem()
{
   int memoria;
   iError = -90;
   char *shmbuf;

   memoria = shmget(ftok("/tmp/Distribucion.txt",1),strlen("Distribucion"),IPC_CREAT | 0666);
   if(memoria <0)
   {
      printf("No se Cre�");
      return errno;
   } 

   shmbuf =(char*)shmat(memoria,0,0);
   if(shmbuf < 0)
   {
      printf("No se Escribio Memoria compartida");
      return errno;
   }

   memcpy(shmbuf, "Distribucion", strlen("Distribucion"));
   
   iError = shmdt(shmbuf);
   if(iError < 0)
   {
      printf("No se Escribio Memoria compartida");
      return errno;
   }

   return true;


}

//#################################################################################

bool  hayOtro()
{
   return true;
}

//##################################################################################

int  Inicia()
{
   iError = -90;

   //char *var[] = {"RUTA_REP","RUTA_FTP","RUTA_FTPARA","RUTA_BDARA","RUTA_SELLOS","FTP","ARA",};
  
   //>- ERGL Error = conf.VarConfg( 7,var,"/usr/local/SAT/PKI/etc/Distribuye.cfg",cRutaRep,cRutaFTP,
   //>-                                                      cRutaFTPARA,cRutaBDARA,cRutaSellos,cFTP,cARA );
   conf = new CConfigFile(ARCH_CONF); // MAML  "/usr/local/SAT/PKI/etc/Distribuye.cfg"
   std::string s_var_config;
   if(conf)
   {
      iError = conf->cargaCfgVars();
      if(iError)
         return iError;
      
      conf->getValorVar("[GENERAL]", "RUTA_REP", 	&s_var_config);
      strcpy(cRutaRep, s_var_config.c_str());
      conf->getValorVar("[GENERAL]", "RUTA_FTP", 	&s_var_config);
      strcpy(cRutaFTP, s_var_config.c_str());
      conf->getValorVar("[GENERAL]", "RUTA_FTPARA",	&s_var_config);
      strcpy(cRutaFTPARA, s_var_config.c_str());
      conf->getValorVar("[GENERAL]", "RUTA_BDARA", 	&s_var_config);
      strcpy(cRutaBDARA, s_var_config.c_str());
      conf->getValorVar("[GENERAL]", "RUTA_SELLOS", 	&s_var_config);
      strcpy(cRutaSellos, s_var_config.c_str());
      conf->getValorVar("[GENERAL]", "FTP", 		&s_var_config);
      strcpy(cFTP, s_var_config.c_str());
      conf->getValorVar("[GENERAL]", "ARA", 		&s_var_config);
      strcpy(cARA, s_var_config.c_str());
   }

   log = new CBitacora(ARCH_LOG);
   bIniciado = true;
   return iError;   
}


//###############################################################################

int MarcasFTP(char* cNumSerie, char *cRutaIni, int iNumARA)
{
   iError = 0;
   int   iDesc;
   char  cRuta[256];
   char  cRutaN[256];
   char  cSoloNum[22];

   sprintf(cRutaN,"%s%02d/",cRutaIni,iNumARA);
   memset(cRuta,0, sizeof(cRuta));
   memset(cSoloNum,0 ,sizeof(cSoloNum));
   //conf.CreaDir(cRutaN);
   pkiCreaDir(cRutaN);
   strncpy(cSoloNum,cNumSerie,20);
   //- sprintf(cRuta,"%s%s\0",cRutaN,cSoloNum); //- GHM (070410)
   sprintf(cRuta,"%s%s",cRutaN,cSoloNum);       //+ GHM (070410)
   iDesc = open(cRuta,O_CREAT,S_IRWXU);
   if(iDesc < 0)
      return errno;

   close(iDesc);
   return iError;

}

//###############################################################################


int MarcasBD(char* cNumSer,char* RutaIni, char *cRutaFin,int iNumARA)
{
   char  cBuffer[100];
   char  cArchivo[256];

   int   iDesc;
   iError = 90;
   int   iBuffer = sizeof(cBuffer);
  
   memset(cBuffer,0,sizeof(cBuffer));  

   sprintf(cArchivo,"%s%s",RutaIni,cNumSer);

   iDesc = open(cArchivo,O_RDONLY);
   if(iDesc < 0) 
      return errno;

   iError = read(iDesc,cBuffer,iBuffer);
   if(iError < 0)
      return errno;
   if(cBuffer[0] == 'R')
      esRevocacion = true;   	   
   
   close(iDesc);

   memset(cArchivo,0,sizeof(cArchivo));

   char  cRutaN[256];
   memset(cRutaN,0,sizeof(cRutaN));

   sprintf(cRutaN,"%s%02d/",cRutaFin,iNumARA);
   //conf.CreaDir(cRutaN);
   pkiCreaDir(cRutaN);

   sprintf(cArchivo,"%s%s",cRutaN,cNumSer);

   iDesc = open(cArchivo,O_WRONLY|O_CREAT,S_IRWXU);
   if(iDesc < 0)
      return errno;

   iError = write(iDesc,cBuffer,strlen(cBuffer));
   if(iError < 0)
      return errno;
   
   close(iDesc);
   return 0;

}


//###############################################################################

/*
int Marca()
{
   DIR   *direc;
   struct dirent *lista_nom;
   iError = -90;

   bool  estaBD     = false;
   bool  estaFTPARA = false;
   bool  estaFTP    = false;
   bool  estanARA   = false;
   char  aBorrar[256];
   //char  cSellos[100];

   if(!bIniciado)
      return errno;

   for ( int vez = 0 ; vez < 2 ;vez++ )
   {

    direc = opendir(cRutaRep);
    if(direc == NULL)
    {
      return -1;
    }

    while((lista_nom = readdir(direc))!= NULL )
    {

      if(vez == 0)	   
        iError = strcmp((lista_nom[0].d_name)+ 20,".A" );
      else
        iError = strcmp((lista_nom[0].d_name)+ 20,".R" );    

      if(iError)
         continue;


      for(int i = 1 ; i < (atoi(cARA) +1) ; i++ )
      { 
         esRevocacion = false;
         iError = MarcasBD(lista_nom[0].d_name,cRutaRep,cRutaBDARA,i);
         if(iError != 0)
            break; 
	 estaBD = true;
	 if (esRevocacion)
         {
           estanARA = true;		 
	   continue;	 
	 }
         iError = MarcasFTP(lista_nom[0].d_name,cRutaFTPARA,i);
         if(iError != 0)
            break;
	 estaFTPARA = true;
         if(estaBD && estaFTPARA)
            estanARA = true;		 
      }
     
      if(!esRevocacion) 
      {	      
         for(int i = 1 ; i < (atoi(cFTP) +1) ; i++ )
         {
            iError = MarcasFTP(lista_nom[0].d_name,cRutaFTP,i);
            if(iError != 0)
               break;
	    estaFTP = true;
         }
      }
      else estaFTP = true;

      if( estanARA && estaFTP)
      {
         memset(aBorrar ,0, sizeof(aBorrar));
	 sprintf(aBorrar ,"%s%s",cRutaRep,lista_nom[0].d_name);
	 unlink(aBorrar);
      } 
      estanARA = estaFTPARA = estaFTP = estaBD = false;
	      
	      
    } //WHILE
   
    closedir(direc);
    direc = NULL;
    lista_nom = NULL;
    //delete lista_nom; // MAML 070622: No es necesario por que no hay 'new'

   }// FOR
   
   //>+ ERGL mi� mar 21 13:50:35 CDT 2007
   return 0;
}
*/
//###########################################################################################################
bool Agente(char * cArch)
{
   char  cNumSerie [21];
   
   strncpy(cNumSerie,cArch,12);

   int iIgual =  memcmp(cNumSerie+ 6,"999999",6 );
   return (iIgual == 0 );
}


//###########################################################################################################

int ParalaARA(int nARA, char *cArch, bool esAlta , bool *estaBD, bool *estaFTPARA)
{
   
   for(int i = 1 ; i < nARA +1 ; i++ )
   {
      iError = MarcasBD(cArch,cRutaRep,cRutaBDARA,i);
      if(iError != 0)
      {
        estaBD = false; 
        return iError;
      }
      if(esAlta)
      {   
         iError = MarcasFTP(cArch,cRutaFTPARA,i);
         if(iError != 0)
         {
           estaFTPARA = false;
           return iError; 
         }
      }
   }

   *estaBD = true; *estaFTPARA = true;
   
   return iError;

}

//###########################################################################################################

int ParaFTP(int iFTP ,char *cArchiv,bool *estaFTP)
{
   //for(int i = 1 ; i < (atoi(cFTP) +1) ; i++ )  se cambio porque  la variable cFTP puede ser cambiada antes de entrar a esta funcion..
   for(int i = 1 ; i < (iFTP +1) ; i++ )
   {
      iError = MarcasFTP(cArchiv,cRutaFTP,i);
      if(iError != 0)
      {
         estaFTP = false;
         return iError; 
      }  
   }
   *estaFTP = true;
   return 0;

}

//###########################################################################################################

int Marca2()
{
   DIR   *direc;
   struct dirent *lista_nom;
   int iError;

   bool  esAlta     = false;
   bool  estaBD     = false;
   bool  estaFTPARA = false;
   bool  estaFTP    = false;
   bool  estanARA   = false;
   bool  esAgente   = false;
   //char  aBorrar[256];
   //char  cSellos[100];

   if(!bIniciado)
      return errno;

   for ( int vez = 0 ; vez < 2 ;vez++ )
   {
      esAlta = (vez == 0)? true: false;

      direc = opendir(cRutaRep);
      if(direc == NULL)
         return -1;

      while((lista_nom = readdir(direc))!= NULL )
      {
         if (esVacio(cRutaRep,lista_nom[0].d_name))
           continue; 
         if (vez == 0)
           iError = strcmp((lista_nom[0].d_name)+ 20,".A" );
         else
           //>>> HOA (080313): se agrega el manejo de archivos para marcar CD caducos (*.C) = *.R  
           //iError = strcmp((lista_nom[0].d_name) + 20,".R" );
           iError = strcmp((lista_nom[0].d_name) + 20, ".R") && 
                    strcmp((lista_nom[0].d_name) + 20, ".C");
           //<<< HOA (080313)

         if (iError)
            continue;

         esAgente = Agente(lista_nom[0].d_name);
         switch(esAgente)
         {
            case true: {
                         ParalaARA(atoi(cARA), lista_nom[0].d_name, esAlta, &estaBD, &estaFTPARA);
                         if( estaBD && estaFTPARA )
                            borrar(lista_nom[0].d_name);
                       }
                       break;
            case false:{
                         ParalaARA(atoi(cARA), lista_nom[0].d_name, esAlta ,&estaBD, &estaFTPARA);
                         if(vez == 0)
                         {   
                            ParaFTP(atoi(cFTP) ,lista_nom[0].d_name, &estaFTP);
                            if(estaBD && estaFTPARA)
                            {
                                if(estaFTP)
                                   borrar(lista_nom[0].d_name);   
                            }
                         }
                         else if(estaBD && estaFTPARA)
                            borrar(lista_nom[0].d_name);
                            
                       }
                       break;
         } // SWITCH
         estanARA = estaFTPARA = estaFTP = estaBD = false;

      } //WHILE

      closedir(direc);
      direc = NULL;
      lista_nom = NULL;
      //delete lista_nom; // MAML 070622: No es necesario por que no hay 'new'

   }// FOR

   //>+ ERGL mi� mar 21 13:51:31 CDT 2007
   return 0;
}


int main()
{
   int iError ;

   //iError = escribeMem();
   iError =  Inicia() ;
   if( !iError )
      Marca2();
   return iError;
}


