/*######################################################################################################################
  ###  PROYECTO:              PKI-SAT                                                                                ###
  ###  MODULO:                Contiene las definiciones de datos utilizados en los diferentes m�dulos                ###
  ###                                                                                                                ###
  ###  DESARROLLADORES:       Gudelia Hern�ndez Molina        GHM                                                    ###
  ###  FECHA DE INICIO:       Mi�rcoles 25, enero del 2006                                                           ###
  ###                                                                                                                ###
  ######################################################################################################################
         1         2         3         4         5         6         7         8         9        10        11        12
123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
*/
/*######################################################################################################################
   VERSION:
       V.1.00      (20060125 -         ) GHM: Primera Versi�n
   CAMBIOS:
######################################################################################################################*/

#ifndef _DEFDATOS_H_
#define _DEFDATOS_H_
static const char* _DEFDATOS_H_VERSION_ ATR_USED = "SrvAR @(#) DSIC09041AR_ 2007-12-13 DefDatos.h 1.1.1/3";

// ******************************  DEFINICIONES DE ERROR  *********************************

//ERRORES DE NEGOCIO
#define SDGPROCESANDO           1
#define SDGNOCORRESPONDE        2
#define CERTNOREN               3
#define CERTREVANT              4
#define LLAVEAPARTADA           5
#define CERTNOREVIES            6
#define CERTNOREGIES            7
#define TRAMNOEXITO             8
//#define CERTNOACTIVO            9
#define CERTNOREV              10
#define CERTNOGEN              11
#define FALTARFCRL             12
#define FALLACOMUNIC           13
#define ERRORFIRMA             14
#define NOEXISTERFC            15
#define NOLOCALIZADO           16
#define NOEXISTETRAM           17
#define NOEXISTECERT           18
#define NOEXISTERFCRL          19
#define NOEXISTEUSU            20
#define NOEXISTEAR             21
#define NOINTEGRO              22
#define REQDUPLICADO           23
#define REQGEN_RFCDIFSDG       24
#define SDGYAPROCESADO         25
#define REQ_CURP_INVALIDA      26
#define DESCONEXIONBD          27
#define ERRORPASSWORD          28
#define LLAVESCOMPR            29
#define USUYACONEC             30
#define CERTACTIVO             31
#define ERRORARAGC             32
#define ERRORARAC              33
#define CERTSINVIG             34
#define VERSIONANT             35
#define CERTNOFEA              36
#define AGCSINPRIV             37
#define COMPROMETIDACERT       38
#define DUPLICADOCERT          39
#define COMPROMETIDAREQ        40
#define DUPLICADOREQ           41

// Esta Etiqueta DEBE ESTAR aqui y EN LA TABLA DE "CAT_ERROR_OPER" ?
#define REQSELLOS_DATOSDIF_CERTENS  43   // Req. Sellos con Datos Diferentes del certificado q lo ensobreto
#define REQRENOV_DIFER_RFCRL   44        // Req. Renov con RFC_RL Diferente del que esta en la DB de la AR  
#define CERTNOVIGENTE          45        // El Certificado de FEA YA no esta Vigente
#define ACUSE_VERS_ANT         46        // Acuse de la versi�n anterior, datos no disponibles.
#define CERTFEANOACTIVO        47        // Certificado de FEA no activo
#define SITDOMFISNOPERM        48        // Situaci�n de domicilio fiscal no permitida
#define OBLIGACIONESFISINV     49        // Revisar las obligaciones fiscales

#define TIENECERTFEA_ACTIVO    80        // Tiene Certificado de Fea Activo
#define BIOMETRICOS_NOREGS     81        // Datos biometricos No registrados en LDAP
//>+ ERGL (070116)
#define FEC_VIG_FIN            82        // No se pudo recuperar fecha de revocacion o caducidad.

//ERRORES DE INTERNOS
#define ERR_BD                  91
#define ERR_MSGNOCORRECTO       92   //No se recibio el msg correcto en la secuencia
#define ERR_PROCESO             93   //Error de proceso 
#define ERR_NIVAGC              94
#define ERR_VALGENCERT          95
#define ERR_INIWS               96

#define ERR_NOT_DESENCRYPT_PWD   97
#define ERR_NO_APL_B64           98

// Errores en la Generacion de un certificado, en el entendido q despues se va dar un orden a esta secuencia
#define ERR_CR_PWDREVINV         126   // Contrase�a de revocaci�n vacia o no cumple con las especificaciones

//GHM (090319): Se anexa etiqueta para utilizar en el sp_conexion_v2
#define IPVER1_DIF_IPVER2        127   //La ip del cliente ver 1 (DARIO) es diferente la ip del cliente ver 2 (CRM)

// ******************************  DEFINICIONES PARA MENSAJES DE ERROR  *******************
#define MSGDESC_ICS             1
#define MSGDESC_CS              2
#define MSGDESC_VACIO           3
#define MSGDESC_C               4

// ******************************  DEFINICIONES GRALES TAMA�OS DE BUFFERS  *******************
#define TAM_CERT                4096
#define TAM_REQ                 4096

// ******************************  DEFINICIONES DE TIPO DE CERTIFICADOS  *******************
#define TIPCER_FEA               1
#define TIPCER_SD                2
#define TIPCER_AGC_SGI           3
#define TIPCER_AGC_ADM           4
#define TIPCER_AGC               5

// ******************************  DEFINICIONES COperacion  *******************
#define MAX_TAM_FIRMA_B64                 685
#define MAX_TAM_RFC                        14
#define MAX_TAM_CADORI                  32760L
#define MAX_TAM_PATH                      256
#define MAX_TAM_SEC_AR                     10
#define MAX_TAM_CVE_REV                   256


//+++ Define los Tipos de Acuse q estan definidos en la tabla CAT_TIPO_ACUSE
#define GENERACION_CERT_DIG      1
#define REVOCACION_OFICIO        2
#define REVOCACION_CLAVE         3
#define RENOVACION_CERT_DIG      4
#define SOLIC_CERT_SELLO_DIG     5
//+++

//+++ Define los Tipos de Certificado q estan definidos en la tabla CAT_TIP_CER
#define     FEA                  1
#define     SELLOS               2
#define     AGC_SEGURIDAD        3
#define     AGC_CENTRAL          4
#define     AGC_CERTIFICADOR     5
#define     AGC_WEB              6
//+++

#endif  //_DEFINEDATOS_H_
