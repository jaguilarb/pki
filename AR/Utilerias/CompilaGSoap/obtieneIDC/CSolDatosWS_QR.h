#ifndef _CSOLDATOSWS_QR_H_
#define _CSOLDATOSWS_QR_H_

#include <string.h>                          // Manejo de Strings
#include <sstream>                           // Manejo strstream para el manejo del error de gsoap
#include "stdsoap2.h"
#include "soapCertisatAcusesWSBindingProxy.h"

extern std::string m_URLWS_QR ;
extern CBitacora*   Bitacora;

// ******************************  DEFINICIONES DE LA CLASE  *******************************
class CSolDatosWS_QR
{
   protected:
  
   public:
      CSolDatosWS_QR();
      ~CSolDatosWS_QR();
      bool obtieneCodigoQR(std::string cadOriginal, std::string *codigoQR);
};

#endif //_CSOLDATOSWS_QR_H_
