#ifndef _OBTDATOSWS_IDC_H_ 
#define _OBTDATOSWS_IDC_H_
static const char* _OBTDATOSWS_IDC_H_VERSION_ ATR_USED = "ObtDatosWS_IDC @(#)"\
"DSIC09041AR_ 2009-03-20 ObtDatosWS_IDC.h 1.1.0/0";

//#VERSION: 1.1.0

#define LISTA_REGIMEN      1
#define LISTA_ROLE         2
#define LISTA_OBLIGACION   3
#define LISTA_ACTIVIDAD    4


bool procesaParametros(int argc, char* argv[], int *iTipoCons);
bool procesaPassword(const std::string& s_pwdEnc, std::string *s_pwdDes);
bool getVarConfWS(std::string *Usr, std::string *Pwd, std::string *conHTTPS, std::string *cert, int *iTpoConsulta);
void borrarPtr();

#endif // _OBTDATOSWS_IDC_H_

