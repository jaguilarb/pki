#include <Sgi_Bitacora.h>
#include "CSolDatosWS_QR.h"

//extern CBitacora*   Bitacora;
//######################################################################################################################

//##### Declaraci�n del Constructor #####
CSolDatosWS_QR::CSolDatosWS_QR() 
{
   std::cout << "Entra al constructor" ;
}

CSolDatosWS_QR::~CSolDatosWS_QR()
{
}

//##### Inicializaci�n del Web Service #####
bool CSolDatosWS_QR::obtieneCodigoQR(std::string cadOriginal, std::string *codigoQR)
{
   std::cout << "Cadena Recibida: " << cadOriginal.c_str() << std::endl;
   ns2__generaCodigoQR qr;
   ns2__generaCodigoQRResponse qrResponse;
   struct soap soap;
   bool exito = false; 

   Bitacora->escribe(BIT_INFO, "Inicia cliente web service QR "); 
   qr.arg0 = (char*) cadOriginal.c_str();
   qrResponse.return_ = "";
    Bitacora->escribe(BIT_INFO, qrResponse.return_);
    Bitacora->escribe(BIT_INFO, qr.arg0); 
   soap_init(&soap);
   soap.send_timeout = 2;
   soap.recv_timeout = 2;
   //Prototipo de metodo expuesto en web service para el regreso de la cadena
   std::cout << "URL de conexion a webservice de codigo QR: " << m_URLWS_QR.c_str() << "\n" << std::endl; 
   if(soap_call___ns2__generaCodigoQR(&soap, 
                                      m_URLWS_QR.c_str()  /*or NULL    endpoint address*/, 
                                      NULL  /*soapAction*/, 
                                      &qr,  /*Cadena original que se le va a mandar al webservice */
                                      &qrResponse /*String de QRB64 devuelto por el web service*/
                                      )== SOAP_OK)
   {
      exito = true;
      *codigoQR = std::string(qrResponse.return_);
   }
   else
   {
      soap_print_fault(&soap, stderr);
      // Bitacora->escribe(BIT_ERROR, "WS_QR(obtieneCodigoQR): No se obtuvo el c�digo QR.");
      std::cout << "***WS_QR(obtieneCodigoQR): No se obtuvo el c�digo QR.***\n";
   }
    soap_destroy(&soap); 
    soap_end(&soap); 
    soap_done(&soap); 
    std::cout << "++++**TERMINO OPERACIONES EXITOSAMENTE!**++++\n";
    //printf("****Termino exitosamente el cliente de CSW Acuses \n\n");
    //printf("QR coficado en B64: %s\n",qrResponse.return_);

  return exito;
}
