#include "stdio.h"

#include <string>
#include "stdsoap2.h"
#include "soapH.h"
#include "CertisatAcusesWSBinding.nsmap"

int main(int argc, char* argv[])
{
  struct soap soap;
  //Objeto que almacena la cadena original que sera enviada al web service
  ns2__generaCodigoQR qr;

  //Objeto que almacena el valor regresado por el web service
  ns2__generaCodigoQRResponse qrResponse;
   
     printf("Inicia cliente web service \n\n");

  
  qr.arg0 = argv[1];
  

  //qr.arg0 = "060300000305|COMPROBANTE DE INSCRIPCIÓN PARA LA FIRMA ELECTRÓNICA AVANZADA|2006-03-03 09:17:48|IEOR6505115B6|RUBEN IGLESIAS ORTIZ|00001000000000013787|MIGJAoGBAMbPRO4V3L7hy31efrgjwtNJPy1dAGKyZPrvv//3KfGzW41WsYHiMJlsKZrYYSrKL6Ie4jl1KssJ+kiSHzfKrpQqKUJrg6XsesFvuHPDnbpabf02qEZgJILQADXa5mJOmpsoYZkCLHLyB/TNXOFeBvM4D2FjaPjcR5H0vf14oVPNAgMBAAE=|IEOR6505115B6|RUBEN IGLESIAS ORTIZ|ADMINISTRACION DE SEGURIDAD DE LA INFORMACION|Firma Electrónica Avanzada||||00001000000000000003|2006-03-03 09:17:48";
  qrResponse.return_ = "";
     

    printf("Codigo B64: %s\n",&qrResponse.return_);
    printf("Cadena original: %s\n", qr.arg0);


  soap_init(&soap);
  


  //Prototipo de metodo expuesto en web service para el regreso de la cadena
  if(soap_call___ns2__generaCodigoQR(&soap, 
                              NULL  /*endpoint address*/, 
                              NULL  /*soapAction*/, 
                              &qr,  /*Cadena original que se le va a mandar al webservice */
                              &qrResponse /*String de QRB64 devuelto por el web service*/
                             )== SOAP_OK);

    
  else

    soap_print_fault(&soap, stderr); 

    soap_destroy(&soap); 
    soap_end(&soap); 
    soap_done(&soap); 

    printf("****Termino exitosamente el cliente de CSW Acuses \n\n");
    printf("QR coficado en B64: %s\n",qrResponse.return_);


  return 0;

}
