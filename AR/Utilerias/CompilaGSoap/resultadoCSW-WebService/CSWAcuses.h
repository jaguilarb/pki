// Reminder: Modify typemap.dat to customize the header file generated by wsdl2h
/* resultadoCSW-WebService/CSWAcuses.h
   Generated by wsdl2h 1.2.13 from CertisatAcusesWSService.wsdl and typemap.dat
   2014-07-11 11:15:53 GMT
   gSOAP XML Web services tools.
   Copyright (C) 2001-2009 Robert van Engelen, Genivia Inc. All Rights Reserved.
   Part of this software is released under one of the following licenses:
   GPL or Genivia's license for commercial use.
*/

/* NOTE:

 - Compile this file with soapcpp2 to complete the code generation process.
 - Use soapcpp2 option -I to specify paths for #import
   To build with STL, 'stlvector.h' is imported from 'import' dir in package.
 - Use wsdl2h options -c and -s to generate pure C code or C++ code without STL.
 - Use 'typemap.dat' to control namespace bindings and type mappings.
   It is strongly recommended to customize the names of the namespace prefixes
   generated by wsdl2h. To do so, modify the prefix bindings in the Namespaces
   section below and add the modified lines to 'typemap.dat' to rerun wsdl2h.
 - Use Doxygen (www.doxygen.org) to browse this file.
 - Use wsdl2h option -l to view the software license terms.

   DO NOT include this file directly into your project.
   Include only the soapcpp2-generated headers and source code files.
*/

//gsoapopt w

/******************************************************************************\
 *                                                                            *
 * http://ws.certisat.sat.gob.mx/                                             *
 *                                                                            *
\******************************************************************************/


/******************************************************************************\
 *                                                                            *
 * Import                                                                     *
 *                                                                            *
\******************************************************************************/


/******************************************************************************\
 *                                                                            *
 * Schema Namespaces                                                          *
 *                                                                            *
\******************************************************************************/


/* NOTE:

It is strongly recommended to customize the names of the namespace prefixes
generated by wsdl2h. To do so, modify the prefix bindings below and add the
modified lines to typemap.dat to rerun wsdl2h:

ns1 = "http://ws.certisat.sat.gob.mx/"

*/

//gsoap ns1   schema namespace:	http://ws.certisat.sat.gob.mx/
//gsoap ns1   schema form:	unqualified

/******************************************************************************\
 *                                                                            *
 * Schema Types                                                               *
 *                                                                            *
\******************************************************************************/



//  Forward declaration of class ns1__generaCodigoQR.
class ns1__generaCodigoQR;

//  Forward declaration of class ns1__generaCodigoQRResponse.
class ns1__generaCodigoQRResponse;

//  Forward declaration of class ns1__generaAcuse.
class ns1__generaAcuse;

//  Forward declaration of class ns1__generaAcuseResponse.
class ns1__generaAcuseResponse;

/// "http://ws.certisat.sat.gob.mx/":generaCodigoQR is a complexType.
class ns1__generaCodigoQR
{ public:
/// Element arg0 of type xs:string.
    char*                                arg0                           0;	///< Optional element.
/// A handle to the soap struct that manages this instance (automatically set)
    struct soap                         *soap                          ;
};

/// "http://ws.certisat.sat.gob.mx/":generaCodigoQRResponse is a complexType.
class ns1__generaCodigoQRResponse
{ public:
/// Element return of type xs:string.
    char*                                return_                        0;	///< Optional element.
/// A handle to the soap struct that manages this instance (automatically set)
    struct soap                         *soap                          ;
};

/// "http://ws.certisat.sat.gob.mx/":generaAcuse is a complexType.
class ns1__generaAcuse
{ public:
/// Element arg0 of type xs:string.
    char*                                arg0                           0;	///< Optional element.
/// A handle to the soap struct that manages this instance (automatically set)
    struct soap                         *soap                          ;
};

/// "http://ws.certisat.sat.gob.mx/":generaAcuseResponse is a complexType.
class ns1__generaAcuseResponse
{ public:
/// Element return of type xs:string.
    char*                                return_                        0;	///< Optional element.
/// A handle to the soap struct that manages this instance (automatically set)
    struct soap                         *soap                          ;
};

/// Element "http://ws.certisat.sat.gob.mx/":generaAcuse of type "http://ws.certisat.sat.gob.mx/":generaAcuse.
/// Note: use wsdl2h option -g to generate this global element declaration.

/// Element "http://ws.certisat.sat.gob.mx/":generaAcuseResponse of type "http://ws.certisat.sat.gob.mx/":generaAcuseResponse.
/// Note: use wsdl2h option -g to generate this global element declaration.

/// Element "http://ws.certisat.sat.gob.mx/":generaCodigoQR of type "http://ws.certisat.sat.gob.mx/":generaCodigoQR.
/// Note: use wsdl2h option -g to generate this global element declaration.

/// Element "http://ws.certisat.sat.gob.mx/":generaCodigoQRResponse of type "http://ws.certisat.sat.gob.mx/":generaCodigoQRResponse.
/// Note: use wsdl2h option -g to generate this global element declaration.

/******************************************************************************\
 *                                                                            *
 * Services                                                                   *
 *                                                                            *
\******************************************************************************/


//gsoap ns1  service name:	CertisatAcusesWSBinding 
//gsoap ns1  service type:	CertisatAcusesWS 
//gsoap ns1  service port:	http://10.0.1.104:8080/certisat-acuses-webservice/CertisatAcusesWS 
//gsoap ns1  service namespace:	http://ws.certisat.sat.gob.mx/ 
//gsoap ns1  service transport:	http://schemas.xmlsoap.org/soap/http 

/** @mainpage CertisatAcusesWSService Definitions

@section CertisatAcusesWSService_bindings Bindings
  - @ref CertisatAcusesWSBinding

*/

/**

@page CertisatAcusesWSBinding Binding "CertisatAcusesWSBinding"

@section CertisatAcusesWSBinding_operations Operations of Binding  "CertisatAcusesWSBinding"
  - @ref __ns1__generaAcuse
  - @ref __ns1__generaCodigoQR

@section CertisatAcusesWSBinding_ports Endpoints of Binding  "CertisatAcusesWSBinding"
  - http://10.0.1.104:8080/certisat-acuses-webservice/CertisatAcusesWS

Note: use wsdl2h option -N to change the service binding prefix name

*/

/******************************************************************************\
 *                                                                            *
 * CertisatAcusesWSBinding                                                    *
 *                                                                            *
\******************************************************************************/


/******************************************************************************\
 *                                                                            *
 * __ns1__generaAcuse                                                         *
 *                                                                            *
\******************************************************************************/


/// Operation "__ns1__generaAcuse" of service binding "CertisatAcusesWSBinding"

/**

Operation details:

  - SOAP document/literal style

C stub function (defined in soapClient.c[pp] generated by soapcpp2):
@code
  int soap_call___ns1__generaAcuse(
    struct soap *soap,
    NULL, // char *endpoint = NULL selects default endpoint for this operation
    NULL, // char *action = NULL selects default action for this operation
    // request parameters:
    ns1__generaAcuse*                   ns1__generaAcuse_,
    // response parameters:
    ns1__generaAcuseResponse*           ns1__generaAcuseResponse_
  );
@endcode

C server function (called from the service dispatcher defined in soapServer.c[pp]):
@code
  int __ns1__generaAcuse(
    struct soap *soap,
    // request parameters:
    ns1__generaAcuse*                   ns1__generaAcuse_,
    // response parameters:
    ns1__generaAcuseResponse*           ns1__generaAcuseResponse_
  );
@endcode

C++ proxy class (defined in soapCertisatAcusesWSBindingProxy.h):
  class CertisatAcusesWSBinding;

Note: use soapcpp2 option '-i' to generate improved proxy and service classes;

*/

//gsoap ns1  service method-style:	generaAcuse document
//gsoap ns1  service method-encoding:	generaAcuse literal
//gsoap ns1  service method-action:	generaAcuse ""
int __ns1__generaAcuse(
    ns1__generaAcuse*                   ns1__generaAcuse_,	///< Request parameter
    ns1__generaAcuseResponse*           ns1__generaAcuseResponse_	///< Response parameter
);

/******************************************************************************\
 *                                                                            *
 * __ns1__generaCodigoQR                                                      *
 *                                                                            *
\******************************************************************************/


/// Operation "__ns1__generaCodigoQR" of service binding "CertisatAcusesWSBinding"

/**

Operation details:

  - SOAP document/literal style

C stub function (defined in soapClient.c[pp] generated by soapcpp2):
@code
  int soap_call___ns1__generaCodigoQR(
    struct soap *soap,
    NULL, // char *endpoint = NULL selects default endpoint for this operation
    NULL, // char *action = NULL selects default action for this operation
    // request parameters:
    ns1__generaCodigoQR*                ns1__generaCodigoQR_,
    // response parameters:
    ns1__generaCodigoQRResponse*        ns1__generaCodigoQRResponse_
  );
@endcode

C server function (called from the service dispatcher defined in soapServer.c[pp]):
@code
  int __ns1__generaCodigoQR(
    struct soap *soap,
    // request parameters:
    ns1__generaCodigoQR*                ns1__generaCodigoQR_,
    // response parameters:
    ns1__generaCodigoQRResponse*        ns1__generaCodigoQRResponse_
  );
@endcode

C++ proxy class (defined in soapCertisatAcusesWSBindingProxy.h):
  class CertisatAcusesWSBinding;

Note: use soapcpp2 option '-i' to generate improved proxy and service classes;

*/

//gsoap ns1  service method-style:	generaCodigoQR document
//gsoap ns1  service method-encoding:	generaCodigoQR literal
//gsoap ns1  service method-action:	generaCodigoQR ""
int __ns1__generaCodigoQR(
    ns1__generaCodigoQR*                ns1__generaCodigoQR_,	///< Request parameter
    ns1__generaCodigoQRResponse*        ns1__generaCodigoQRResponse_	///< Response parameter
);

/* End of resultadoCSW-WebService/CSWAcuses.h */
