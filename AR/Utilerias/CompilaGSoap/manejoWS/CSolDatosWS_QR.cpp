#include <Sgi_Bitacora.h>
#include <Sgi_ConfigFile.h>
#include <Sgi_Fecha.h>
#include "CSolDatosWS_QR.h"

extern CBitacora*   Bitacora;

//######################################################################################################################

//##### Declaración del Constructor #####

CSolDatosWS_QR::CSolDatosWS_QR()
{
}

//##### Inicialización del Web Service #####
bool CSolDatosWS_QR::obtieneCodigoQR(char* cadOriginal,std::string *codigoQR)
{
   struct soap soap;
   bool exito = false; 
   //Objeto que almacena la cadena original que sera enviada al web service
   ns2__generaCodigoQR qr;
   //Objeto que almacena el valor regresado por el web service
   ns2__generaCodigoQRResponse qrResponse;
   
    Bitacora->escribe(BIT_INFO, "Inicia cliente web service QR "); 
    qr.arg0 = cadOrig;
    qrResponse.return_ = "";
    
    Bitacora->escribe(BIT_INFO, &qrResponse.return_);
    Bitacora->escribe(BIT_INFO,  qr.arg0); 
    //printf("Codigo B64: %s\n",&qrResponse.return_);
    //printf("Cadena original: %s\n", qr.arg0);

   soap_init(&soap);
   //soap.endpoint = (const char*) m_URLWS.c_str();
   soap->send_timeout = 2;
   soap->recv_timeout = 2;


   //Prototipo de metodo expuesto en web service para el regreso de la cadena
   if(soap_call___ns2__generaCodigoQR(&soap, 
                                       m_URLWS.c_str()  /*or NULL    endpoint address*/, 
                                      NULL  /*soapAction*/, 
                                      &qr,  /*Cadena original que se le va a mandar al webservice */
                                      &qrResponse /*String de QRB64 devuelto por el web service*/
                                      )== SOAP_OK);
      exito = true;
   else
   {
      soap_print_fault(&soap, stderr);
       Bitacora->escribe(BIT_ERROR, "WS_QR(obtieneCodigoQR): No se obtuvo el código QR.");
       Bitacora->escribe(BIT_ERROR, stderr); 
   }
    soap_destroy(&soap); 
    soap_end(&soap); 
    soap_done(&soap); 

    //printf("****Termino exitosamente el cliente de CSW Acuses \n\n");
    //printf("QR coficado en B64: %s\n",qrResponse.return_);

  return exito;
}
