static const char* _OBTDATOSWS_IDC_CPP_VERSION_ ATR_USED = "ObtDatosWS_IDC @(#)"\
"DSIC10284AR_ 2010-07-15 ObtDatosWS_IDC.cpp 1.1.4/2";

//  JSPC: Jose Salvador Parra Correa

//#VERSION: 1.1.0
//          1.1.1  JSPC: Se despliega la fecha de alta y la fecha de baja de Regimen, Roles, Obligaciones y Actividades
//                       Para facilitar la evaluaci�n de los criterios de validaci�n fiscal.



#ifdef WS_EXTERNO
   #include "IdCSOAP.nsmap"
#else
   #include "IdCInternoSOAP.nsmap"
#endif

#include <Sgi_ConfigFile.h>
#include <Sgi_Bitacora.h>
#include <Sgi_ProtegePwd.h>
#include "CSolDatosWS_IDC.h"
#include "ObtDatosWS_IDC.h"

CBitacora       *Bitacora      = NULL;
CSolDatosWS_IDC ObjWS_IDC;


std::string m_URLWS_IDC;

//#################################################################################
#define PATH_CONF            "/usr/local/SAT/PKI/etc/"
#define PATH_DBIT            "/var/local/SAT/PKI/"

#ifdef DBG
   #define ARCH_CONF            PATH_CONF "ObtDatosWS_IDC_dbg.cfg"
   #define PATH_BITA            PATH_DBIT "ObtDatosWS_IDC_dbg.log"
#else
   #define ARCH_CONF            PATH_CONF "ObtDatosWS_IDC.cfg"
   #define PATH_BITA            PATH_DBIT "ObtDatosWS_IDC.log"
#endif

// Creaci�n del objeto de Bitacora
CBitacora* Crea_CBitacora()
{
   return new CBitacora(PATH_BITA);
}

void convMayusc( char cadena[])
{
   int longitud = strlen(cadena);
   for ( int i = 0; i < longitud; i++)
      cadena[i] = toupper(cadena[i]);
}

// Valida los par�metros de entrada
bool procesaParametros(int argc, char* argv[], int *iTipoConsulta)
{
   if ( argc == 2 || argc == 3 )
   {
      convMayusc(argv[1]);
      if ( !(strlen(argv[1]) == 12 || strlen(argv[1]) == 13) )
      {
         std::cout << "La longitud del RFC es inv�lida (" << argv[1] << "), revise el dato." << std::endl;
         return false;
      }
      if ( argc == 2 )
         *iTipoConsulta = 1;
      else
      {
         *iTipoConsulta = atoi(argv[2]);
         if ( *iTipoConsulta != 1 && *iTipoConsulta != 2 )
            *iTipoConsulta = 1;
      }
      return true;
   }
   else
   {
      std::cout << std::endl << std::endl ;
      std::cout << "Uso: " << std::endl << "\t" << argv[0] << " RFC TIPOCONS\n" << std::endl << std::endl;
      std::cout << "Donde TIPOCONS: " << std::endl;
      std::cout << "                1 -> Despliega datos de las validaciones de FIEL" << std::endl;
      std::cout << "                2 -> Despliega datos de las validaciones de SDG " << std::endl;
      std::cout << "   Por default se toma 1." << std::endl;
      return false;
   }
}

//Desencripta el pwd
bool procesaPassword(const std::string& s_pwdEnc, std::string *s_pwdDes)
{
   bool b_resOperacion = true;
   int i_pwdSalida = s_pwdEnc.size();
   char *psz_pwdSalida = new char[i_pwdSalida];
   if (!desencripta((uint8*) s_pwdEnc.c_str(), i_pwdSalida, (uint8*) psz_pwdSalida, &i_pwdSalida))
      b_resOperacion = false;
   else
      s_pwdDes->assign(psz_pwdSalida, i_pwdSalida);
   delete[] psz_pwdSalida;
   return b_resOperacion;
}

bool getVarConfWS(std::string *Usr, std::string *Pwd, std::string *conHTTPS, std::string *cert)
{
   std::string NvlBitWS;
   std::string PwdWSEnc, TpoCons;
   int r1, r2, r3, r4, r5, iError;
   
   CConfigFile Configuracion(ARCH_CONF);

   //Lectura de ArchConf
   if ( (iError = Configuracion.cargaCfgVars()) )
   {
      std::cout << "Problemas al leer el archivo de configuraci�n (" << iError << "): " << ARCH_CONF << std::endl;
      return false;
   }

   //Establece el nivelde bit�cora
   r1  = Configuracion.getValorVar("[BITACORA]"    , "NIVELBIT" , &NvlBitWS);
   if (r1 != 0)
      NvlBitWS = "3";
   if (Bitacora)
      Bitacora->setNivel( (BIT_NIVEL) atoi(NvlBitWS.c_str()) );
   //Lee los datos del WEB SERVICE IDC
   r1 = Configuracion.getValorVar("[WEB_SERVICE_IDC]" , "USUARIO"    , Usr      );
   r2 = Configuracion.getValorVar("[WEB_SERVICE_IDC]" , "PASSWORD"   , &PwdWSEnc);
   r3 = Configuracion.getValorVar("[WEB_SERVICE_IDC]" , "URLWS"      , &m_URLWS_IDC );
   r4 = Configuracion.getValorVar("[WEB_SERVICE_IDC]" , "HTTPS"      , conHTTPS );
   //Determina si utiliza el certificado
   if ( conHTTPS->compare("S") == 0)
   {
      r5 = Configuracion.getValorVar("[WEB_SERVICE_IDC]", "CERT", cert);
      if ( r1 || r2 || r3 || r4 || r5 )
      {
         std::cout << "Problemas al leer la configuraci�n de WS_IDC: " << "usr(" << r1 << "), ";
         std::cout << "Pwd(" << r2 << "), URL(" << r3 << "), conHTTPS(" << r4 << ", Crt("  ;
         std::cout << r5 << ")" << std::endl;
         return false;
      }
   }
   else
   {
      if ( r1 || r2 || r3 || r4 )
      {
         std::cout << "Problemas al leer la configuraci�n de WS_IDC: " << "usr(" << r1 << "),";
         std::cout << "Pwd(" << r2 << "), URL(" << r3 << "), conHTTPS(" << r4 << ")" << std::endl; 
         return false;
      }
   } 
   if ( m_URLWS_IDC.length() == 0 )
   {
      std::cout << "No se recupero la URL correctamente" << std::endl;
      return false;
   }
   if ( !procesaPassword(PwdWSEnc, Pwd) )
   {
      std::cout << "Fall� la desencripci�n del pwd del WS" << std::endl;
      return false;
   }
   return true;
}

void borrarPtr()
{
   if (Bitacora)
   {
      delete Bitacora;
      Bitacora = NULL;
   }
}

std::string ObtLista( int iTpoLista, int iMaxNumElem )
{
   std::string sLista = "";
   std::string sDatoLeido, sVigIni, sVigFin;
   bool activo, seObtuvoDato;
   
   for (int indice=0; indice < iMaxNumElem; indice++)
   {
      seObtuvoDato = false;
      switch (iTpoLista)
      {
         case LISTA_REGIMEN:
            // JSPC (100714): Se modificaron los parametros
            // - seObtuvoDato = ObjWS_IDC.getRegimen(indice, &sDatoLeido, &activo); break;
            seObtuvoDato = ObjWS_IDC.getRegimen(indice, &sDatoLeido, &sVigIni, &sVigFin, &activo); break; // + V.1.1.1
         case LISTA_ROLE:
            // JSPC (100714): Se modificaron los parametros
            // - seObtuvoDato = ObjWS_IDC.getRol(indice, &sDatoLeido, &activo); break;
            seObtuvoDato = ObjWS_IDC.getRol(indice, &sDatoLeido, &sVigIni, &sVigFin, &activo); break; // + V.1.1.1
         case LISTA_OBLIGACION:
            // JSPC (100714): Se Modificaron los parametros
            // - seObtuvoDato = ObjWS_IDC.getObligacion(indice, &sDatoLeido, &activo); break;
            seObtuvoDato = ObjWS_IDC.getObligacion(indice, &sDatoLeido, &sVigIni, &sVigFin, &activo); break; // + V.1.1.1
         case LISTA_ACTIVIDAD:
            // JSPC (100714): Se modificaron los parametros
            // - seObtuvoDato = ObjWS_IDC.getActiv(indice, &sDatoLeido, &activo); break;
            seObtuvoDato = ObjWS_IDC.getActiv(indice, &sDatoLeido, &sVigIni, &sVigFin, &activo); break; // + V.1.1.1
      }
      if (seObtuvoDato)
         // JSPC (100714): Se modificaron datos de salida
         // - sLista += sDatoLeido + "," + (activo?"ACTIVO":"DESACTIVADO") + "|";
         sLista += sDatoLeido + "," + sVigIni + "," + sVigFin + "," + (activo?"ACTIVO":"DESACTIVADO") + "|"; // + V.1.1.1
   }
   if ( sLista.length() == 0)
      sLista = "Sin elementos";
   return sLista;
}

bool imprimeDatos( const char *RFCBusq, int iTipoCons, std::string *strCadError) 
{
   //Variables para extraer datos
   int         lcadena = 30, iClave = 0, iElementos = 0, inumError = 0;
   char        cadena[lcadena];
   bool        hayError = false;
   std::string sDato;
   
   //Despliegue de datos
   //RFC original char(13)
   memset(cadena, 0, lcadena); 
   if ( !ObjWS_IDC.getRFCOrig(cadena) )
   {
      std::cout << "RFC_ORIGINAL   = "  << std::endl;
      *strCadError += "No se obtuvo RFC original. ";
   }else
      std::cout << "RFC_ORIGINAL E   = "  << cadena << std::endl;
   //StatusRFC: Regresa -1, 2 y 1, este �ltimo es el que se considera correcto.
   iClave = ObjWS_IDC.getStatusRFC();
   std::cout << "STATUSRFC_CVE  = "  << iClave << std::endl;

   if ( !ObjWS_IDC.getTipoContrib(&sDato) )
   {
      std::cout << "TIPO_CONTRIB   = " << std::endl;
      *strCadError += "No se obtuvo el tipo de contribuyente. ";
   } else
      std::cout << "TIPO_CONTRIB   = " << sDato.c_str() << std::endl;
   //Nombre
   memset(cadena, 0, lcadena);
   if ( (inumError = ObjWS_IDC.getNombre(&sDato)) != 0 )
   {
      std::cout << "NOMBRE_CONTRIB = " << std::endl;
      sprintf(cadena, "%i", inumError);
      *strCadError += "Se present� el error (" + std::string(cadena) + "), al obtener el nombre. ";
   } else
      std::cout << "NOMBRE_CONTRIB = "<< sDato.c_str() << std::endl;
   //CURP
   memset(cadena, 0, lcadena);
   if( !ObjWS_IDC.getCURP(cadena) )
   {
      std::cout << "CURP           = " << std::endl;
      *strCadError += "No se obtuvo la CURP. ";
   }else
      std::cout << "CURP           = " << cadena << std::endl;
   //Situaci�n Fiscal
   if ( !ObjWS_IDC.getSitFiscal(RFCBusq, iClave) )
   {
      std::cout << "SITFIS_CLAVE   = " << std::endl;
      sprintf(cadena, "%i", iClave);
      *strCadError += "Se present� el error (" + std::string(cadena) + "), al obtener la clave de la Situaci�n Fiscal";
   }else
      std::cout << "SITFIS_CLAVE   = " << (iClave>0?iClave:-1) << std::endl;

   if ( !ObjWS_IDC.getDesSitFis(&sDato) )
   {
      std::cout << "SITFIS_DESC    = " << std::endl;
      *strCadError += "No se obtuvo la descripci�n de la Situaci�n Fiscal. ";
   }else
      std::cout << "SITFIS_DESC    = " << sDato.c_str() << std::endl;
   //Situaci�n Domicilio
   iClave = ObjWS_IDC.getSitDomicilio(RFCBusq);
   std::cout << "SITDOMIC_CVE   = " << (iClave>0?iClave:' ') << std::endl;
   sDato = ObjWS_IDC.getDescDom();
   std::cout << "SITDOMIC_DESC  = " << sDato.c_str() << std::endl;
   if ( iTipoCons == 2 )
   {
      //R�gimen
      iElementos = ObjWS_IDC.getNumRegs();
      //std::cout << "NUM_REGIMEN=     " << iElementos << std::endl;
      sDato = ObtLista( LISTA_REGIMEN, iElementos );
      std::cout << "REGIMENES      = " << sDato.c_str() << std::endl;
      //Role
      iElementos = ObjWS_IDC.getNumRoles();
      //std::cout << "NUM_ROL=         " << iElementos << std::endl;
      sDato = ObtLista( LISTA_ROLE, iElementos );
      std::cout << "ROLES          = " << sDato.c_str() << std::endl;
      //Obligacion
      iElementos = ObjWS_IDC.getNumObligs();
      //std::cout << "NUM_OBLIG=       " << iElementos << std::endl;
      sDato = ObtLista( LISTA_OBLIGACION, iElementos );
      std::cout << "OBLIGACIONES   = " << sDato.c_str() << std::endl;
      //Actividad
      iElementos = ObjWS_IDC.getNumActivs();
      //std::cout << "NUM_ACTIVIDAD=   " << iElementos << std::endl;
      sDato = ObtLista( LISTA_ACTIVIDAD, iElementos );
      std::cout << "ACTIVIDADES    = " << sDato.c_str() << std::endl;
   }
   return !hayError;
}

int main(int argc, char* argv[])
{
   std::string     usrWS, pwdWS, conHTTPSWS, certWS;
   int             iTipoCons=0, iError = 0;

   Bitacora       = Crea_CBitacora();
   
   if ( !(procesaParametros(argc, argv, &iTipoCons)) )
      return 1;
   //Carga de los datos del WS_IDC del ArchConf
   if ( !getVarConfWS(&usrWS, &pwdWS, &conHTTPSWS, &certWS) )
     return 3;
   //Genera el objeto para interactuar con el WS_IDC
   ObjWS_IDC.Inicia(argv[1], iTipoCons, usrWS, pwdWS, conHTTPSWS, certWS);
   //Inicializa el WS
   if ( !ObjWS_IDC.InicializaWS() )
   {
      std::cout << "Error al inicializar la interfaz del WS" << std::endl;
      iError = 5;
   }
   else
   {
      if ( (iError = ObjWS_IDC.SolInfoRFCWS(argv[1])) != EXITO )
      {
         std::cout << "Error (" << iError << ") al solicitar la informaci�n al WS" << std::endl;
         iError = 6;
      }
   }
   if (iError != 0)
   {
      borrarPtr();
      return iError;
   }
   //Variable para Errores
   std::string  cadErrores;
   cadErrores = "ERRORES: ";
   //Despliegue de datos
   //RFC Consultado
   std::cout << "RFC_CONTRIB    = "   << argv[1]   << std::endl;
   if ( !imprimeDatos(argv[1], iTipoCons, &cadErrores))
      std::cout << "Errores: " << cadErrores.c_str() ;
   borrarPtr();
   return iError;
};
