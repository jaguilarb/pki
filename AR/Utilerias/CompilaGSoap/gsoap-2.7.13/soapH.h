/* soapH.h
   Generated by gSOAP 2.7.13 from gsoap-2.7.13/IdCInterno.h
   Copyright(C) 2000-2009, Robert van Engelen, Genivia Inc. All Rights Reserved.
   This part of the software is released under one of the following licenses:
   GPL, the gSOAP public license, or Genivia's license for commercial use.
*/

#ifndef soapH_H
#define soapH_H
#include "soapStub.h"
#ifndef WITH_NOIDREF

#ifdef __cplusplus
extern "C" {
#endif
SOAP_FMAC3 void SOAP_FMAC4 soap_markelement(struct soap*, const void*, int);
SOAP_FMAC3 int SOAP_FMAC4 soap_putelement(struct soap*, const void*, const char*, int, int);
SOAP_FMAC3 void *SOAP_FMAC4 soap_getelement(struct soap*, int*);

#ifdef __cplusplus
}
#endif
SOAP_FMAC3 int SOAP_FMAC4 soap_putindependent(struct soap*);
SOAP_FMAC3 int SOAP_FMAC4 soap_getindependent(struct soap*);
#endif
SOAP_FMAC3 int SOAP_FMAC4 soap_ignore_element(struct soap*);

SOAP_FMAC3 void * SOAP_FMAC4 soap_instantiate(struct soap*, int, const char*, const char*, size_t*);
SOAP_FMAC3 int SOAP_FMAC4 soap_fdelete(struct soap_clist*);
SOAP_FMAC3 void* SOAP_FMAC4 soap_class_id_enter(struct soap*, const char*, void*, int, size_t, const char*, const char*);

#ifndef SOAP_TYPE_byte
#define SOAP_TYPE_byte (3)
#endif
SOAP_FMAC3 void SOAP_FMAC4 soap_default_byte(struct soap*, char *);

SOAP_FMAC3 int SOAP_FMAC4 soap_put_byte(struct soap*, const char *, const char*, const char*);
SOAP_FMAC3 int SOAP_FMAC4 soap_out_byte(struct soap*, const char*, int, const char *, const char*);
SOAP_FMAC3 char * SOAP_FMAC4 soap_get_byte(struct soap*, char *, const char*, const char*);
SOAP_FMAC3 char * SOAP_FMAC4 soap_in_byte(struct soap*, const char*, char *, const char*);

#ifndef SOAP_TYPE_int
#define SOAP_TYPE_int (1)
#endif
SOAP_FMAC3 void SOAP_FMAC4 soap_default_int(struct soap*, int *);

SOAP_FMAC3 int SOAP_FMAC4 soap_put_int(struct soap*, const int *, const char*, const char*);
SOAP_FMAC3 int SOAP_FMAC4 soap_out_int(struct soap*, const char*, int, const int *, const char*);
SOAP_FMAC3 int * SOAP_FMAC4 soap_get_int(struct soap*, int *, const char*, const char*);
SOAP_FMAC3 int * SOAP_FMAC4 soap_in_int(struct soap*, const char*, int *, const char*);

#ifndef SOAP_TYPE__ns2__IdCInterno
#define SOAP_TYPE__ns2__IdCInterno (17)
#endif

SOAP_FMAC3 int SOAP_FMAC4 soap_out__ns2__IdCInterno(struct soap*, const char*, int, const _ns2__IdCInterno *, const char*);
SOAP_FMAC3 _ns2__IdCInterno * SOAP_FMAC4 soap_get__ns2__IdCInterno(struct soap*, _ns2__IdCInterno *, const char*, const char*);
SOAP_FMAC3 _ns2__IdCInterno * SOAP_FMAC4 soap_in__ns2__IdCInterno(struct soap*, const char*, _ns2__IdCInterno *, const char*);

#define soap_new__ns2__IdCInterno(soap, n) soap_instantiate__ns2__IdCInterno(soap, n, NULL, NULL, NULL)


#define soap_delete__ns2__IdCInterno(soap, p) soap_delete(soap, p)

SOAP_FMAC3 _ns2__IdCInterno * SOAP_FMAC4 soap_instantiate__ns2__IdCInterno(struct soap*, int, const char*, const char*, size_t*);
SOAP_FMAC3 void SOAP_FMAC4 soap_copy__ns2__IdCInterno(struct soap*, int, int, void*, size_t, const void*, size_t);

#ifndef SOAP_TYPE__ns2__datosEntrada
#define SOAP_TYPE__ns2__datosEntrada (16)
#endif

SOAP_FMAC3 int SOAP_FMAC4 soap_out__ns2__datosEntrada(struct soap*, const char*, int, const _ns2__datosEntrada *, const char*);
SOAP_FMAC3 _ns2__datosEntrada * SOAP_FMAC4 soap_get__ns2__datosEntrada(struct soap*, _ns2__datosEntrada *, const char*, const char*);
SOAP_FMAC3 _ns2__datosEntrada * SOAP_FMAC4 soap_in__ns2__datosEntrada(struct soap*, const char*, _ns2__datosEntrada *, const char*);

#define soap_new__ns2__datosEntrada(soap, n) soap_instantiate__ns2__datosEntrada(soap, n, NULL, NULL, NULL)


#define soap_delete__ns2__datosEntrada(soap, p) soap_delete(soap, p)

SOAP_FMAC3 _ns2__datosEntrada * SOAP_FMAC4 soap_instantiate__ns2__datosEntrada(struct soap*, int, const char*, const char*, size_t*);
SOAP_FMAC3 void SOAP_FMAC4 soap_copy__ns2__datosEntrada(struct soap*, int, int, void*, size_t, const void*, size_t);

#ifndef SOAP_TYPE_ns2__sucursal
#define SOAP_TYPE_ns2__sucursal (15)
#endif

SOAP_FMAC3 int SOAP_FMAC4 soap_out_ns2__sucursal(struct soap*, const char*, int, const ns2__sucursal *, const char*);
SOAP_FMAC3 ns2__sucursal * SOAP_FMAC4 soap_get_ns2__sucursal(struct soap*, ns2__sucursal *, const char*, const char*);
SOAP_FMAC3 ns2__sucursal * SOAP_FMAC4 soap_in_ns2__sucursal(struct soap*, const char*, ns2__sucursal *, const char*);

#define soap_new_ns2__sucursal(soap, n) soap_instantiate_ns2__sucursal(soap, n, NULL, NULL, NULL)


#define soap_delete_ns2__sucursal(soap, p) soap_delete(soap, p)

SOAP_FMAC3 ns2__sucursal * SOAP_FMAC4 soap_instantiate_ns2__sucursal(struct soap*, int, const char*, const char*, size_t*);
SOAP_FMAC3 void SOAP_FMAC4 soap_copy_ns2__sucursal(struct soap*, int, int, void*, size_t, const void*, size_t);

#ifndef SOAP_TYPE_ns2__rep_USCORElegal
#define SOAP_TYPE_ns2__rep_USCORElegal (14)
#endif

SOAP_FMAC3 int SOAP_FMAC4 soap_out_ns2__rep_USCORElegal(struct soap*, const char*, int, const ns2__rep_USCORElegal *, const char*);
SOAP_FMAC3 ns2__rep_USCORElegal * SOAP_FMAC4 soap_get_ns2__rep_USCORElegal(struct soap*, ns2__rep_USCORElegal *, const char*, const char*);
SOAP_FMAC3 ns2__rep_USCORElegal * SOAP_FMAC4 soap_in_ns2__rep_USCORElegal(struct soap*, const char*, ns2__rep_USCORElegal *, const char*);

#define soap_new_ns2__rep_USCORElegal(soap, n) soap_instantiate_ns2__rep_USCORElegal(soap, n, NULL, NULL, NULL)


#define soap_delete_ns2__rep_USCORElegal(soap, p) soap_delete(soap, p)

SOAP_FMAC3 ns2__rep_USCORElegal * SOAP_FMAC4 soap_instantiate_ns2__rep_USCORElegal(struct soap*, int, const char*, const char*, size_t*);
SOAP_FMAC3 void SOAP_FMAC4 soap_copy_ns2__rep_USCORElegal(struct soap*, int, int, void*, size_t, const void*, size_t);

#ifndef SOAP_TYPE_ns2__actividad
#define SOAP_TYPE_ns2__actividad (13)
#endif

SOAP_FMAC3 int SOAP_FMAC4 soap_out_ns2__actividad(struct soap*, const char*, int, const ns2__actividad *, const char*);
SOAP_FMAC3 ns2__actividad * SOAP_FMAC4 soap_get_ns2__actividad(struct soap*, ns2__actividad *, const char*, const char*);
SOAP_FMAC3 ns2__actividad * SOAP_FMAC4 soap_in_ns2__actividad(struct soap*, const char*, ns2__actividad *, const char*);

#define soap_new_ns2__actividad(soap, n) soap_instantiate_ns2__actividad(soap, n, NULL, NULL, NULL)


#define soap_delete_ns2__actividad(soap, p) soap_delete(soap, p)

SOAP_FMAC3 ns2__actividad * SOAP_FMAC4 soap_instantiate_ns2__actividad(struct soap*, int, const char*, const char*, size_t*);
SOAP_FMAC3 void SOAP_FMAC4 soap_copy_ns2__actividad(struct soap*, int, int, void*, size_t, const void*, size_t);

#ifndef SOAP_TYPE_ns2__rol
#define SOAP_TYPE_ns2__rol (12)
#endif

SOAP_FMAC3 int SOAP_FMAC4 soap_out_ns2__rol(struct soap*, const char*, int, const ns2__rol *, const char*);
SOAP_FMAC3 ns2__rol * SOAP_FMAC4 soap_get_ns2__rol(struct soap*, ns2__rol *, const char*, const char*);
SOAP_FMAC3 ns2__rol * SOAP_FMAC4 soap_in_ns2__rol(struct soap*, const char*, ns2__rol *, const char*);

#define soap_new_ns2__rol(soap, n) soap_instantiate_ns2__rol(soap, n, NULL, NULL, NULL)


#define soap_delete_ns2__rol(soap, p) soap_delete(soap, p)

SOAP_FMAC3 ns2__rol * SOAP_FMAC4 soap_instantiate_ns2__rol(struct soap*, int, const char*, const char*, size_t*);
SOAP_FMAC3 void SOAP_FMAC4 soap_copy_ns2__rol(struct soap*, int, int, void*, size_t, const void*, size_t);

#ifndef SOAP_TYPE_ns2__regimen
#define SOAP_TYPE_ns2__regimen (11)
#endif

SOAP_FMAC3 int SOAP_FMAC4 soap_out_ns2__regimen(struct soap*, const char*, int, const ns2__regimen *, const char*);
SOAP_FMAC3 ns2__regimen * SOAP_FMAC4 soap_get_ns2__regimen(struct soap*, ns2__regimen *, const char*, const char*);
SOAP_FMAC3 ns2__regimen * SOAP_FMAC4 soap_in_ns2__regimen(struct soap*, const char*, ns2__regimen *, const char*);

#define soap_new_ns2__regimen(soap, n) soap_instantiate_ns2__regimen(soap, n, NULL, NULL, NULL)


#define soap_delete_ns2__regimen(soap, p) soap_delete(soap, p)

SOAP_FMAC3 ns2__regimen * SOAP_FMAC4 soap_instantiate_ns2__regimen(struct soap*, int, const char*, const char*, size_t*);
SOAP_FMAC3 void SOAP_FMAC4 soap_copy_ns2__regimen(struct soap*, int, int, void*, size_t, const void*, size_t);

#ifndef SOAP_TYPE_ns2__obligacion
#define SOAP_TYPE_ns2__obligacion (10)
#endif

SOAP_FMAC3 int SOAP_FMAC4 soap_out_ns2__obligacion(struct soap*, const char*, int, const ns2__obligacion *, const char*);
SOAP_FMAC3 ns2__obligacion * SOAP_FMAC4 soap_get_ns2__obligacion(struct soap*, ns2__obligacion *, const char*, const char*);
SOAP_FMAC3 ns2__obligacion * SOAP_FMAC4 soap_in_ns2__obligacion(struct soap*, const char*, ns2__obligacion *, const char*);

#define soap_new_ns2__obligacion(soap, n) soap_instantiate_ns2__obligacion(soap, n, NULL, NULL, NULL)


#define soap_delete_ns2__obligacion(soap, p) soap_delete(soap, p)

SOAP_FMAC3 ns2__obligacion * SOAP_FMAC4 soap_instantiate_ns2__obligacion(struct soap*, int, const char*, const char*, size_t*);
SOAP_FMAC3 void SOAP_FMAC4 soap_copy_ns2__obligacion(struct soap*, int, int, void*, size_t, const void*, size_t);

#ifndef SOAP_TYPE_ns2__ubicacion
#define SOAP_TYPE_ns2__ubicacion (9)
#endif

SOAP_FMAC3 int SOAP_FMAC4 soap_out_ns2__ubicacion(struct soap*, const char*, int, const ns2__ubicacion *, const char*);
SOAP_FMAC3 ns2__ubicacion * SOAP_FMAC4 soap_get_ns2__ubicacion(struct soap*, ns2__ubicacion *, const char*, const char*);
SOAP_FMAC3 ns2__ubicacion * SOAP_FMAC4 soap_in_ns2__ubicacion(struct soap*, const char*, ns2__ubicacion *, const char*);

#define soap_new_ns2__ubicacion(soap, n) soap_instantiate_ns2__ubicacion(soap, n, NULL, NULL, NULL)


#define soap_delete_ns2__ubicacion(soap, p) soap_delete(soap, p)

SOAP_FMAC3 ns2__ubicacion * SOAP_FMAC4 soap_instantiate_ns2__ubicacion(struct soap*, int, const char*, const char*, size_t*);
SOAP_FMAC3 void SOAP_FMAC4 soap_copy_ns2__ubicacion(struct soap*, int, int, void*, size_t, const void*, size_t);

#ifndef SOAP_TYPE_ns2__identificacion
#define SOAP_TYPE_ns2__identificacion (8)
#endif

SOAP_FMAC3 int SOAP_FMAC4 soap_out_ns2__identificacion(struct soap*, const char*, int, const ns2__identificacion *, const char*);
SOAP_FMAC3 ns2__identificacion * SOAP_FMAC4 soap_get_ns2__identificacion(struct soap*, ns2__identificacion *, const char*, const char*);
SOAP_FMAC3 ns2__identificacion * SOAP_FMAC4 soap_in_ns2__identificacion(struct soap*, const char*, ns2__identificacion *, const char*);

#define soap_new_ns2__identificacion(soap, n) soap_instantiate_ns2__identificacion(soap, n, NULL, NULL, NULL)


#define soap_delete_ns2__identificacion(soap, p) soap_delete(soap, p)

SOAP_FMAC3 ns2__identificacion * SOAP_FMAC4 soap_instantiate_ns2__identificacion(struct soap*, int, const char*, const char*, size_t*);
SOAP_FMAC3 void SOAP_FMAC4 soap_copy_ns2__identificacion(struct soap*, int, int, void*, size_t, const void*, size_t);

#ifndef SOAP_TYPE_ns2__mensajes
#define SOAP_TYPE_ns2__mensajes (7)
#endif

SOAP_FMAC3 int SOAP_FMAC4 soap_out_ns2__mensajes(struct soap*, const char*, int, const ns2__mensajes *, const char*);
SOAP_FMAC3 ns2__mensajes * SOAP_FMAC4 soap_get_ns2__mensajes(struct soap*, ns2__mensajes *, const char*, const char*);
SOAP_FMAC3 ns2__mensajes * SOAP_FMAC4 soap_in_ns2__mensajes(struct soap*, const char*, ns2__mensajes *, const char*);

#define soap_new_ns2__mensajes(soap, n) soap_instantiate_ns2__mensajes(soap, n, NULL, NULL, NULL)


#define soap_delete_ns2__mensajes(soap, p) soap_delete(soap, p)

SOAP_FMAC3 ns2__mensajes * SOAP_FMAC4 soap_instantiate_ns2__mensajes(struct soap*, int, const char*, const char*, size_t*);
SOAP_FMAC3 void SOAP_FMAC4 soap_copy_ns2__mensajes(struct soap*, int, int, void*, size_t, const void*, size_t);

#ifndef WITH_NOGLOBAL

#ifndef SOAP_TYPE_SOAP_ENV__Fault
#define SOAP_TYPE_SOAP_ENV__Fault (46)
#endif
SOAP_FMAC3 void SOAP_FMAC4 soap_default_SOAP_ENV__Fault(struct soap*, struct SOAP_ENV__Fault *);
SOAP_FMAC3 void SOAP_FMAC4 soap_serialize_SOAP_ENV__Fault(struct soap*, const struct SOAP_ENV__Fault *);

SOAP_FMAC3 int SOAP_FMAC4 soap_put_SOAP_ENV__Fault(struct soap*, const struct SOAP_ENV__Fault *, const char*, const char*);
SOAP_FMAC3 int SOAP_FMAC4 soap_out_SOAP_ENV__Fault(struct soap*, const char*, int, const struct SOAP_ENV__Fault *, const char*);
SOAP_FMAC3 struct SOAP_ENV__Fault * SOAP_FMAC4 soap_get_SOAP_ENV__Fault(struct soap*, struct SOAP_ENV__Fault *, const char*, const char*);
SOAP_FMAC3 struct SOAP_ENV__Fault * SOAP_FMAC4 soap_in_SOAP_ENV__Fault(struct soap*, const char*, struct SOAP_ENV__Fault *, const char*);

#define soap_new_SOAP_ENV__Fault(soap, n) soap_instantiate_SOAP_ENV__Fault(soap, n, NULL, NULL, NULL)


#define soap_delete_SOAP_ENV__Fault(soap, p) soap_delete(soap, p)

SOAP_FMAC3 struct SOAP_ENV__Fault * SOAP_FMAC4 soap_instantiate_SOAP_ENV__Fault(struct soap*, int, const char*, const char*, size_t*);
SOAP_FMAC3 void SOAP_FMAC4 soap_copy_SOAP_ENV__Fault(struct soap*, int, int, void*, size_t, const void*, size_t);

#endif

#ifndef WITH_NOGLOBAL

#ifndef SOAP_TYPE_SOAP_ENV__Reason
#define SOAP_TYPE_SOAP_ENV__Reason (45)
#endif
SOAP_FMAC3 void SOAP_FMAC4 soap_default_SOAP_ENV__Reason(struct soap*, struct SOAP_ENV__Reason *);
SOAP_FMAC3 void SOAP_FMAC4 soap_serialize_SOAP_ENV__Reason(struct soap*, const struct SOAP_ENV__Reason *);

SOAP_FMAC3 int SOAP_FMAC4 soap_put_SOAP_ENV__Reason(struct soap*, const struct SOAP_ENV__Reason *, const char*, const char*);
SOAP_FMAC3 int SOAP_FMAC4 soap_out_SOAP_ENV__Reason(struct soap*, const char*, int, const struct SOAP_ENV__Reason *, const char*);
SOAP_FMAC3 struct SOAP_ENV__Reason * SOAP_FMAC4 soap_get_SOAP_ENV__Reason(struct soap*, struct SOAP_ENV__Reason *, const char*, const char*);
SOAP_FMAC3 struct SOAP_ENV__Reason * SOAP_FMAC4 soap_in_SOAP_ENV__Reason(struct soap*, const char*, struct SOAP_ENV__Reason *, const char*);

#define soap_new_SOAP_ENV__Reason(soap, n) soap_instantiate_SOAP_ENV__Reason(soap, n, NULL, NULL, NULL)


#define soap_delete_SOAP_ENV__Reason(soap, p) soap_delete(soap, p)

SOAP_FMAC3 struct SOAP_ENV__Reason * SOAP_FMAC4 soap_instantiate_SOAP_ENV__Reason(struct soap*, int, const char*, const char*, size_t*);
SOAP_FMAC3 void SOAP_FMAC4 soap_copy_SOAP_ENV__Reason(struct soap*, int, int, void*, size_t, const void*, size_t);

#endif

#ifndef WITH_NOGLOBAL

#ifndef SOAP_TYPE_SOAP_ENV__Detail
#define SOAP_TYPE_SOAP_ENV__Detail (42)
#endif
SOAP_FMAC3 void SOAP_FMAC4 soap_default_SOAP_ENV__Detail(struct soap*, struct SOAP_ENV__Detail *);
SOAP_FMAC3 void SOAP_FMAC4 soap_serialize_SOAP_ENV__Detail(struct soap*, const struct SOAP_ENV__Detail *);

SOAP_FMAC3 int SOAP_FMAC4 soap_put_SOAP_ENV__Detail(struct soap*, const struct SOAP_ENV__Detail *, const char*, const char*);
SOAP_FMAC3 int SOAP_FMAC4 soap_out_SOAP_ENV__Detail(struct soap*, const char*, int, const struct SOAP_ENV__Detail *, const char*);
SOAP_FMAC3 struct SOAP_ENV__Detail * SOAP_FMAC4 soap_get_SOAP_ENV__Detail(struct soap*, struct SOAP_ENV__Detail *, const char*, const char*);
SOAP_FMAC3 struct SOAP_ENV__Detail * SOAP_FMAC4 soap_in_SOAP_ENV__Detail(struct soap*, const char*, struct SOAP_ENV__Detail *, const char*);

#define soap_new_SOAP_ENV__Detail(soap, n) soap_instantiate_SOAP_ENV__Detail(soap, n, NULL, NULL, NULL)


#define soap_delete_SOAP_ENV__Detail(soap, p) soap_delete(soap, p)

SOAP_FMAC3 struct SOAP_ENV__Detail * SOAP_FMAC4 soap_instantiate_SOAP_ENV__Detail(struct soap*, int, const char*, const char*, size_t*);
SOAP_FMAC3 void SOAP_FMAC4 soap_copy_SOAP_ENV__Detail(struct soap*, int, int, void*, size_t, const void*, size_t);

#endif

#ifndef WITH_NOGLOBAL

#ifndef SOAP_TYPE_SOAP_ENV__Code
#define SOAP_TYPE_SOAP_ENV__Code (40)
#endif
SOAP_FMAC3 void SOAP_FMAC4 soap_default_SOAP_ENV__Code(struct soap*, struct SOAP_ENV__Code *);
SOAP_FMAC3 void SOAP_FMAC4 soap_serialize_SOAP_ENV__Code(struct soap*, const struct SOAP_ENV__Code *);

SOAP_FMAC3 int SOAP_FMAC4 soap_put_SOAP_ENV__Code(struct soap*, const struct SOAP_ENV__Code *, const char*, const char*);
SOAP_FMAC3 int SOAP_FMAC4 soap_out_SOAP_ENV__Code(struct soap*, const char*, int, const struct SOAP_ENV__Code *, const char*);
SOAP_FMAC3 struct SOAP_ENV__Code * SOAP_FMAC4 soap_get_SOAP_ENV__Code(struct soap*, struct SOAP_ENV__Code *, const char*, const char*);
SOAP_FMAC3 struct SOAP_ENV__Code * SOAP_FMAC4 soap_in_SOAP_ENV__Code(struct soap*, const char*, struct SOAP_ENV__Code *, const char*);

#define soap_new_SOAP_ENV__Code(soap, n) soap_instantiate_SOAP_ENV__Code(soap, n, NULL, NULL, NULL)


#define soap_delete_SOAP_ENV__Code(soap, p) soap_delete(soap, p)

SOAP_FMAC3 struct SOAP_ENV__Code * SOAP_FMAC4 soap_instantiate_SOAP_ENV__Code(struct soap*, int, const char*, const char*, size_t*);
SOAP_FMAC3 void SOAP_FMAC4 soap_copy_SOAP_ENV__Code(struct soap*, int, int, void*, size_t, const void*, size_t);

#endif

#ifndef WITH_NOGLOBAL

#ifndef SOAP_TYPE_SOAP_ENV__Header
#define SOAP_TYPE_SOAP_ENV__Header (39)
#endif
SOAP_FMAC3 void SOAP_FMAC4 soap_default_SOAP_ENV__Header(struct soap*, struct SOAP_ENV__Header *);
SOAP_FMAC3 void SOAP_FMAC4 soap_serialize_SOAP_ENV__Header(struct soap*, const struct SOAP_ENV__Header *);

SOAP_FMAC3 int SOAP_FMAC4 soap_put_SOAP_ENV__Header(struct soap*, const struct SOAP_ENV__Header *, const char*, const char*);
SOAP_FMAC3 int SOAP_FMAC4 soap_out_SOAP_ENV__Header(struct soap*, const char*, int, const struct SOAP_ENV__Header *, const char*);
SOAP_FMAC3 struct SOAP_ENV__Header * SOAP_FMAC4 soap_get_SOAP_ENV__Header(struct soap*, struct SOAP_ENV__Header *, const char*, const char*);
SOAP_FMAC3 struct SOAP_ENV__Header * SOAP_FMAC4 soap_in_SOAP_ENV__Header(struct soap*, const char*, struct SOAP_ENV__Header *, const char*);

#define soap_new_SOAP_ENV__Header(soap, n) soap_instantiate_SOAP_ENV__Header(soap, n, NULL, NULL, NULL)


#define soap_delete_SOAP_ENV__Header(soap, p) soap_delete(soap, p)

SOAP_FMAC3 struct SOAP_ENV__Header * SOAP_FMAC4 soap_instantiate_SOAP_ENV__Header(struct soap*, int, const char*, const char*, size_t*);
SOAP_FMAC3 void SOAP_FMAC4 soap_copy_SOAP_ENV__Header(struct soap*, int, int, void*, size_t, const void*, size_t);

#endif

#ifndef SOAP_TYPE___ns1__getIdCInterno
#define SOAP_TYPE___ns1__getIdCInterno (38)
#endif
SOAP_FMAC3 void SOAP_FMAC4 soap_default___ns1__getIdCInterno(struct soap*, struct __ns1__getIdCInterno *);
SOAP_FMAC3 void SOAP_FMAC4 soap_serialize___ns1__getIdCInterno(struct soap*, const struct __ns1__getIdCInterno *);

SOAP_FMAC3 int SOAP_FMAC4 soap_put___ns1__getIdCInterno(struct soap*, const struct __ns1__getIdCInterno *, const char*, const char*);
SOAP_FMAC3 int SOAP_FMAC4 soap_out___ns1__getIdCInterno(struct soap*, const char*, int, const struct __ns1__getIdCInterno *, const char*);
SOAP_FMAC3 struct __ns1__getIdCInterno * SOAP_FMAC4 soap_get___ns1__getIdCInterno(struct soap*, struct __ns1__getIdCInterno *, const char*, const char*);
SOAP_FMAC3 struct __ns1__getIdCInterno * SOAP_FMAC4 soap_in___ns1__getIdCInterno(struct soap*, const char*, struct __ns1__getIdCInterno *, const char*);

#define soap_new___ns1__getIdCInterno(soap, n) soap_instantiate___ns1__getIdCInterno(soap, n, NULL, NULL, NULL)


#define soap_delete___ns1__getIdCInterno(soap, p) soap_delete(soap, p)

SOAP_FMAC3 struct __ns1__getIdCInterno * SOAP_FMAC4 soap_instantiate___ns1__getIdCInterno(struct soap*, int, const char*, const char*, size_t*);
SOAP_FMAC3 void SOAP_FMAC4 soap_copy___ns1__getIdCInterno(struct soap*, int, int, void*, size_t, const void*, size_t);

#ifndef WITH_NOGLOBAL

#ifndef SOAP_TYPE_PointerToSOAP_ENV__Reason
#define SOAP_TYPE_PointerToSOAP_ENV__Reason (48)
#endif
SOAP_FMAC3 void SOAP_FMAC4 soap_serialize_PointerToSOAP_ENV__Reason(struct soap*, struct SOAP_ENV__Reason *const*);
SOAP_FMAC3 int SOAP_FMAC4 soap_put_PointerToSOAP_ENV__Reason(struct soap*, struct SOAP_ENV__Reason *const*, const char*, const char*);
SOAP_FMAC3 int SOAP_FMAC4 soap_out_PointerToSOAP_ENV__Reason(struct soap*, const char *, int, struct SOAP_ENV__Reason *const*, const char *);
SOAP_FMAC3 struct SOAP_ENV__Reason ** SOAP_FMAC4 soap_get_PointerToSOAP_ENV__Reason(struct soap*, struct SOAP_ENV__Reason **, const char*, const char*);
SOAP_FMAC3 struct SOAP_ENV__Reason ** SOAP_FMAC4 soap_in_PointerToSOAP_ENV__Reason(struct soap*, const char*, struct SOAP_ENV__Reason **, const char*);

#endif

#ifndef WITH_NOGLOBAL

#ifndef SOAP_TYPE_PointerToSOAP_ENV__Detail
#define SOAP_TYPE_PointerToSOAP_ENV__Detail (47)
#endif
SOAP_FMAC3 void SOAP_FMAC4 soap_serialize_PointerToSOAP_ENV__Detail(struct soap*, struct SOAP_ENV__Detail *const*);
SOAP_FMAC3 int SOAP_FMAC4 soap_put_PointerToSOAP_ENV__Detail(struct soap*, struct SOAP_ENV__Detail *const*, const char*, const char*);
SOAP_FMAC3 int SOAP_FMAC4 soap_out_PointerToSOAP_ENV__Detail(struct soap*, const char *, int, struct SOAP_ENV__Detail *const*, const char *);
SOAP_FMAC3 struct SOAP_ENV__Detail ** SOAP_FMAC4 soap_get_PointerToSOAP_ENV__Detail(struct soap*, struct SOAP_ENV__Detail **, const char*, const char*);
SOAP_FMAC3 struct SOAP_ENV__Detail ** SOAP_FMAC4 soap_in_PointerToSOAP_ENV__Detail(struct soap*, const char*, struct SOAP_ENV__Detail **, const char*);

#endif

#ifndef WITH_NOGLOBAL

#ifndef SOAP_TYPE_PointerToSOAP_ENV__Code
#define SOAP_TYPE_PointerToSOAP_ENV__Code (41)
#endif
SOAP_FMAC3 void SOAP_FMAC4 soap_serialize_PointerToSOAP_ENV__Code(struct soap*, struct SOAP_ENV__Code *const*);
SOAP_FMAC3 int SOAP_FMAC4 soap_put_PointerToSOAP_ENV__Code(struct soap*, struct SOAP_ENV__Code *const*, const char*, const char*);
SOAP_FMAC3 int SOAP_FMAC4 soap_out_PointerToSOAP_ENV__Code(struct soap*, const char *, int, struct SOAP_ENV__Code *const*, const char *);
SOAP_FMAC3 struct SOAP_ENV__Code ** SOAP_FMAC4 soap_get_PointerToSOAP_ENV__Code(struct soap*, struct SOAP_ENV__Code **, const char*, const char*);
SOAP_FMAC3 struct SOAP_ENV__Code ** SOAP_FMAC4 soap_in_PointerToSOAP_ENV__Code(struct soap*, const char*, struct SOAP_ENV__Code **, const char*);

#endif

#ifndef SOAP_TYPE_PointerTo_ns2__IdCInterno
#define SOAP_TYPE_PointerTo_ns2__IdCInterno (36)
#endif
SOAP_FMAC3 void SOAP_FMAC4 soap_serialize_PointerTo_ns2__IdCInterno(struct soap*, _ns2__IdCInterno *const*);
SOAP_FMAC3 int SOAP_FMAC4 soap_put_PointerTo_ns2__IdCInterno(struct soap*, _ns2__IdCInterno *const*, const char*, const char*);
SOAP_FMAC3 int SOAP_FMAC4 soap_out_PointerTo_ns2__IdCInterno(struct soap*, const char *, int, _ns2__IdCInterno *const*, const char *);
SOAP_FMAC3 _ns2__IdCInterno ** SOAP_FMAC4 soap_get_PointerTo_ns2__IdCInterno(struct soap*, _ns2__IdCInterno **, const char*, const char*);
SOAP_FMAC3 _ns2__IdCInterno ** SOAP_FMAC4 soap_in_PointerTo_ns2__IdCInterno(struct soap*, const char*, _ns2__IdCInterno **, const char*);

#ifndef SOAP_TYPE_PointerTo_ns2__datosEntrada
#define SOAP_TYPE_PointerTo_ns2__datosEntrada (35)
#endif
SOAP_FMAC3 void SOAP_FMAC4 soap_serialize_PointerTo_ns2__datosEntrada(struct soap*, _ns2__datosEntrada *const*);
SOAP_FMAC3 int SOAP_FMAC4 soap_put_PointerTo_ns2__datosEntrada(struct soap*, _ns2__datosEntrada *const*, const char*, const char*);
SOAP_FMAC3 int SOAP_FMAC4 soap_out_PointerTo_ns2__datosEntrada(struct soap*, const char *, int, _ns2__datosEntrada *const*, const char *);
SOAP_FMAC3 _ns2__datosEntrada ** SOAP_FMAC4 soap_get_PointerTo_ns2__datosEntrada(struct soap*, _ns2__datosEntrada **, const char*, const char*);
SOAP_FMAC3 _ns2__datosEntrada ** SOAP_FMAC4 soap_in_PointerTo_ns2__datosEntrada(struct soap*, const char*, _ns2__datosEntrada **, const char*);

#ifndef SOAP_TYPE_PointerTons2__mensajes
#define SOAP_TYPE_PointerTons2__mensajes (34)
#endif
SOAP_FMAC3 void SOAP_FMAC4 soap_serialize_PointerTons2__mensajes(struct soap*, ns2__mensajes *const*);
SOAP_FMAC3 int SOAP_FMAC4 soap_put_PointerTons2__mensajes(struct soap*, ns2__mensajes *const*, const char*, const char*);
SOAP_FMAC3 int SOAP_FMAC4 soap_out_PointerTons2__mensajes(struct soap*, const char *, int, ns2__mensajes *const*, const char *);
SOAP_FMAC3 ns2__mensajes ** SOAP_FMAC4 soap_get_PointerTons2__mensajes(struct soap*, ns2__mensajes **, const char*, const char*);
SOAP_FMAC3 ns2__mensajes ** SOAP_FMAC4 soap_in_PointerTons2__mensajes(struct soap*, const char*, ns2__mensajes **, const char*);

#ifndef SOAP_TYPE_PointerToPointerTons2__sucursal
#define SOAP_TYPE_PointerToPointerTons2__sucursal (33)
#endif
SOAP_FMAC3 void SOAP_FMAC4 soap_serialize_PointerToPointerTons2__sucursal(struct soap*, ns2__sucursal **const*);
SOAP_FMAC3 int SOAP_FMAC4 soap_put_PointerToPointerTons2__sucursal(struct soap*, ns2__sucursal **const*, const char*, const char*);
SOAP_FMAC3 int SOAP_FMAC4 soap_out_PointerToPointerTons2__sucursal(struct soap*, const char *, int, ns2__sucursal **const*, const char *);
SOAP_FMAC3 ns2__sucursal *** SOAP_FMAC4 soap_get_PointerToPointerTons2__sucursal(struct soap*, ns2__sucursal ***, const char*, const char*);
SOAP_FMAC3 ns2__sucursal *** SOAP_FMAC4 soap_in_PointerToPointerTons2__sucursal(struct soap*, const char*, ns2__sucursal ***, const char*);

#ifndef SOAP_TYPE_PointerTons2__sucursal
#define SOAP_TYPE_PointerTons2__sucursal (32)
#endif
SOAP_FMAC3 void SOAP_FMAC4 soap_serialize_PointerTons2__sucursal(struct soap*, ns2__sucursal *const*);
SOAP_FMAC3 int SOAP_FMAC4 soap_put_PointerTons2__sucursal(struct soap*, ns2__sucursal *const*, const char*, const char*);
SOAP_FMAC3 int SOAP_FMAC4 soap_out_PointerTons2__sucursal(struct soap*, const char *, int, ns2__sucursal *const*, const char *);
SOAP_FMAC3 ns2__sucursal ** SOAP_FMAC4 soap_get_PointerTons2__sucursal(struct soap*, ns2__sucursal **, const char*, const char*);
SOAP_FMAC3 ns2__sucursal ** SOAP_FMAC4 soap_in_PointerTons2__sucursal(struct soap*, const char*, ns2__sucursal **, const char*);

#ifndef SOAP_TYPE_PointerToPointerTons2__rep_USCORElegal
#define SOAP_TYPE_PointerToPointerTons2__rep_USCORElegal (31)
#endif
SOAP_FMAC3 void SOAP_FMAC4 soap_serialize_PointerToPointerTons2__rep_USCORElegal(struct soap*, ns2__rep_USCORElegal **const*);
SOAP_FMAC3 int SOAP_FMAC4 soap_put_PointerToPointerTons2__rep_USCORElegal(struct soap*, ns2__rep_USCORElegal **const*, const char*, const char*);
SOAP_FMAC3 int SOAP_FMAC4 soap_out_PointerToPointerTons2__rep_USCORElegal(struct soap*, const char *, int, ns2__rep_USCORElegal **const*, const char *);
SOAP_FMAC3 ns2__rep_USCORElegal *** SOAP_FMAC4 soap_get_PointerToPointerTons2__rep_USCORElegal(struct soap*, ns2__rep_USCORElegal ***, const char*, const char*);
SOAP_FMAC3 ns2__rep_USCORElegal *** SOAP_FMAC4 soap_in_PointerToPointerTons2__rep_USCORElegal(struct soap*, const char*, ns2__rep_USCORElegal ***, const char*);

#ifndef SOAP_TYPE_PointerTons2__rep_USCORElegal
#define SOAP_TYPE_PointerTons2__rep_USCORElegal (30)
#endif
SOAP_FMAC3 void SOAP_FMAC4 soap_serialize_PointerTons2__rep_USCORElegal(struct soap*, ns2__rep_USCORElegal *const*);
SOAP_FMAC3 int SOAP_FMAC4 soap_put_PointerTons2__rep_USCORElegal(struct soap*, ns2__rep_USCORElegal *const*, const char*, const char*);
SOAP_FMAC3 int SOAP_FMAC4 soap_out_PointerTons2__rep_USCORElegal(struct soap*, const char *, int, ns2__rep_USCORElegal *const*, const char *);
SOAP_FMAC3 ns2__rep_USCORElegal ** SOAP_FMAC4 soap_get_PointerTons2__rep_USCORElegal(struct soap*, ns2__rep_USCORElegal **, const char*, const char*);
SOAP_FMAC3 ns2__rep_USCORElegal ** SOAP_FMAC4 soap_in_PointerTons2__rep_USCORElegal(struct soap*, const char*, ns2__rep_USCORElegal **, const char*);

#ifndef SOAP_TYPE_PointerToPointerTons2__actividad
#define SOAP_TYPE_PointerToPointerTons2__actividad (29)
#endif
SOAP_FMAC3 void SOAP_FMAC4 soap_serialize_PointerToPointerTons2__actividad(struct soap*, ns2__actividad **const*);
SOAP_FMAC3 int SOAP_FMAC4 soap_put_PointerToPointerTons2__actividad(struct soap*, ns2__actividad **const*, const char*, const char*);
SOAP_FMAC3 int SOAP_FMAC4 soap_out_PointerToPointerTons2__actividad(struct soap*, const char *, int, ns2__actividad **const*, const char *);
SOAP_FMAC3 ns2__actividad *** SOAP_FMAC4 soap_get_PointerToPointerTons2__actividad(struct soap*, ns2__actividad ***, const char*, const char*);
SOAP_FMAC3 ns2__actividad *** SOAP_FMAC4 soap_in_PointerToPointerTons2__actividad(struct soap*, const char*, ns2__actividad ***, const char*);

#ifndef SOAP_TYPE_PointerTons2__actividad
#define SOAP_TYPE_PointerTons2__actividad (28)
#endif
SOAP_FMAC3 void SOAP_FMAC4 soap_serialize_PointerTons2__actividad(struct soap*, ns2__actividad *const*);
SOAP_FMAC3 int SOAP_FMAC4 soap_put_PointerTons2__actividad(struct soap*, ns2__actividad *const*, const char*, const char*);
SOAP_FMAC3 int SOAP_FMAC4 soap_out_PointerTons2__actividad(struct soap*, const char *, int, ns2__actividad *const*, const char *);
SOAP_FMAC3 ns2__actividad ** SOAP_FMAC4 soap_get_PointerTons2__actividad(struct soap*, ns2__actividad **, const char*, const char*);
SOAP_FMAC3 ns2__actividad ** SOAP_FMAC4 soap_in_PointerTons2__actividad(struct soap*, const char*, ns2__actividad **, const char*);

#ifndef SOAP_TYPE_PointerToPointerTons2__rol
#define SOAP_TYPE_PointerToPointerTons2__rol (27)
#endif
SOAP_FMAC3 void SOAP_FMAC4 soap_serialize_PointerToPointerTons2__rol(struct soap*, ns2__rol **const*);
SOAP_FMAC3 int SOAP_FMAC4 soap_put_PointerToPointerTons2__rol(struct soap*, ns2__rol **const*, const char*, const char*);
SOAP_FMAC3 int SOAP_FMAC4 soap_out_PointerToPointerTons2__rol(struct soap*, const char *, int, ns2__rol **const*, const char *);
SOAP_FMAC3 ns2__rol *** SOAP_FMAC4 soap_get_PointerToPointerTons2__rol(struct soap*, ns2__rol ***, const char*, const char*);
SOAP_FMAC3 ns2__rol *** SOAP_FMAC4 soap_in_PointerToPointerTons2__rol(struct soap*, const char*, ns2__rol ***, const char*);

#ifndef SOAP_TYPE_PointerTons2__rol
#define SOAP_TYPE_PointerTons2__rol (26)
#endif
SOAP_FMAC3 void SOAP_FMAC4 soap_serialize_PointerTons2__rol(struct soap*, ns2__rol *const*);
SOAP_FMAC3 int SOAP_FMAC4 soap_put_PointerTons2__rol(struct soap*, ns2__rol *const*, const char*, const char*);
SOAP_FMAC3 int SOAP_FMAC4 soap_out_PointerTons2__rol(struct soap*, const char *, int, ns2__rol *const*, const char *);
SOAP_FMAC3 ns2__rol ** SOAP_FMAC4 soap_get_PointerTons2__rol(struct soap*, ns2__rol **, const char*, const char*);
SOAP_FMAC3 ns2__rol ** SOAP_FMAC4 soap_in_PointerTons2__rol(struct soap*, const char*, ns2__rol **, const char*);

#ifndef SOAP_TYPE_PointerToPointerTons2__regimen
#define SOAP_TYPE_PointerToPointerTons2__regimen (25)
#endif
SOAP_FMAC3 void SOAP_FMAC4 soap_serialize_PointerToPointerTons2__regimen(struct soap*, ns2__regimen **const*);
SOAP_FMAC3 int SOAP_FMAC4 soap_put_PointerToPointerTons2__regimen(struct soap*, ns2__regimen **const*, const char*, const char*);
SOAP_FMAC3 int SOAP_FMAC4 soap_out_PointerToPointerTons2__regimen(struct soap*, const char *, int, ns2__regimen **const*, const char *);
SOAP_FMAC3 ns2__regimen *** SOAP_FMAC4 soap_get_PointerToPointerTons2__regimen(struct soap*, ns2__regimen ***, const char*, const char*);
SOAP_FMAC3 ns2__regimen *** SOAP_FMAC4 soap_in_PointerToPointerTons2__regimen(struct soap*, const char*, ns2__regimen ***, const char*);

#ifndef SOAP_TYPE_PointerTons2__regimen
#define SOAP_TYPE_PointerTons2__regimen (24)
#endif
SOAP_FMAC3 void SOAP_FMAC4 soap_serialize_PointerTons2__regimen(struct soap*, ns2__regimen *const*);
SOAP_FMAC3 int SOAP_FMAC4 soap_put_PointerTons2__regimen(struct soap*, ns2__regimen *const*, const char*, const char*);
SOAP_FMAC3 int SOAP_FMAC4 soap_out_PointerTons2__regimen(struct soap*, const char *, int, ns2__regimen *const*, const char *);
SOAP_FMAC3 ns2__regimen ** SOAP_FMAC4 soap_get_PointerTons2__regimen(struct soap*, ns2__regimen **, const char*, const char*);
SOAP_FMAC3 ns2__regimen ** SOAP_FMAC4 soap_in_PointerTons2__regimen(struct soap*, const char*, ns2__regimen **, const char*);

#ifndef SOAP_TYPE_PointerToPointerTons2__obligacion
#define SOAP_TYPE_PointerToPointerTons2__obligacion (23)
#endif
SOAP_FMAC3 void SOAP_FMAC4 soap_serialize_PointerToPointerTons2__obligacion(struct soap*, ns2__obligacion **const*);
SOAP_FMAC3 int SOAP_FMAC4 soap_put_PointerToPointerTons2__obligacion(struct soap*, ns2__obligacion **const*, const char*, const char*);
SOAP_FMAC3 int SOAP_FMAC4 soap_out_PointerToPointerTons2__obligacion(struct soap*, const char *, int, ns2__obligacion **const*, const char *);
SOAP_FMAC3 ns2__obligacion *** SOAP_FMAC4 soap_get_PointerToPointerTons2__obligacion(struct soap*, ns2__obligacion ***, const char*, const char*);
SOAP_FMAC3 ns2__obligacion *** SOAP_FMAC4 soap_in_PointerToPointerTons2__obligacion(struct soap*, const char*, ns2__obligacion ***, const char*);

#ifndef SOAP_TYPE_PointerTons2__obligacion
#define SOAP_TYPE_PointerTons2__obligacion (22)
#endif
SOAP_FMAC3 void SOAP_FMAC4 soap_serialize_PointerTons2__obligacion(struct soap*, ns2__obligacion *const*);
SOAP_FMAC3 int SOAP_FMAC4 soap_put_PointerTons2__obligacion(struct soap*, ns2__obligacion *const*, const char*, const char*);
SOAP_FMAC3 int SOAP_FMAC4 soap_out_PointerTons2__obligacion(struct soap*, const char *, int, ns2__obligacion *const*, const char *);
SOAP_FMAC3 ns2__obligacion ** SOAP_FMAC4 soap_get_PointerTons2__obligacion(struct soap*, ns2__obligacion **, const char*, const char*);
SOAP_FMAC3 ns2__obligacion ** SOAP_FMAC4 soap_in_PointerTons2__obligacion(struct soap*, const char*, ns2__obligacion **, const char*);

#ifndef SOAP_TYPE_PointerTons2__ubicacion
#define SOAP_TYPE_PointerTons2__ubicacion (21)
#endif
SOAP_FMAC3 void SOAP_FMAC4 soap_serialize_PointerTons2__ubicacion(struct soap*, ns2__ubicacion *const*);
SOAP_FMAC3 int SOAP_FMAC4 soap_put_PointerTons2__ubicacion(struct soap*, ns2__ubicacion *const*, const char*, const char*);
SOAP_FMAC3 int SOAP_FMAC4 soap_out_PointerTons2__ubicacion(struct soap*, const char *, int, ns2__ubicacion *const*, const char *);
SOAP_FMAC3 ns2__ubicacion ** SOAP_FMAC4 soap_get_PointerTons2__ubicacion(struct soap*, ns2__ubicacion **, const char*, const char*);
SOAP_FMAC3 ns2__ubicacion ** SOAP_FMAC4 soap_in_PointerTons2__ubicacion(struct soap*, const char*, ns2__ubicacion **, const char*);

#ifndef SOAP_TYPE_PointerTons2__identificacion
#define SOAP_TYPE_PointerTons2__identificacion (20)
#endif
SOAP_FMAC3 void SOAP_FMAC4 soap_serialize_PointerTons2__identificacion(struct soap*, ns2__identificacion *const*);
SOAP_FMAC3 int SOAP_FMAC4 soap_put_PointerTons2__identificacion(struct soap*, ns2__identificacion *const*, const char*, const char*);
SOAP_FMAC3 int SOAP_FMAC4 soap_out_PointerTons2__identificacion(struct soap*, const char *, int, ns2__identificacion *const*, const char *);
SOAP_FMAC3 ns2__identificacion ** SOAP_FMAC4 soap_get_PointerTons2__identificacion(struct soap*, ns2__identificacion **, const char*, const char*);
SOAP_FMAC3 ns2__identificacion ** SOAP_FMAC4 soap_in_PointerTons2__identificacion(struct soap*, const char*, ns2__identificacion **, const char*);

#ifndef SOAP_TYPE_PointerTostring
#define SOAP_TYPE_PointerTostring (18)
#endif
SOAP_FMAC3 void SOAP_FMAC4 soap_serialize_PointerTostring(struct soap*, char **const*);
SOAP_FMAC3 int SOAP_FMAC4 soap_put_PointerTostring(struct soap*, char **const*, const char*, const char*);
SOAP_FMAC3 int SOAP_FMAC4 soap_out_PointerTostring(struct soap*, const char *, int, char **const*, const char *);
SOAP_FMAC3 char *** SOAP_FMAC4 soap_get_PointerTostring(struct soap*, char ***, const char*, const char*);
SOAP_FMAC3 char *** SOAP_FMAC4 soap_in_PointerTostring(struct soap*, const char*, char ***, const char*);

#ifndef SOAP_TYPE__QName
#define SOAP_TYPE__QName (5)
#endif

#define soap_default__QName(soap, a) soap_default_string(soap, a)


#define soap_serialize__QName(soap, a) soap_serialize_string(soap, a)

SOAP_FMAC3 int SOAP_FMAC4 soap_put__QName(struct soap*, char *const*, const char*, const char*);
SOAP_FMAC3 int SOAP_FMAC4 soap_out__QName(struct soap*, const char*, int, char*const*, const char*);
SOAP_FMAC3 char ** SOAP_FMAC4 soap_get__QName(struct soap*, char **, const char*, const char*);
SOAP_FMAC3 char * * SOAP_FMAC4 soap_in__QName(struct soap*, const char*, char **, const char*);

#ifndef SOAP_TYPE_string
#define SOAP_TYPE_string (4)
#endif
SOAP_FMAC3 void SOAP_FMAC4 soap_default_string(struct soap*, char **);
SOAP_FMAC3 void SOAP_FMAC4 soap_serialize_string(struct soap*, char *const*);
SOAP_FMAC3 int SOAP_FMAC4 soap_put_string(struct soap*, char *const*, const char*, const char*);
SOAP_FMAC3 int SOAP_FMAC4 soap_out_string(struct soap*, const char*, int, char*const*, const char*);
SOAP_FMAC3 char ** SOAP_FMAC4 soap_get_string(struct soap*, char **, const char*, const char*);
SOAP_FMAC3 char * * SOAP_FMAC4 soap_in_string(struct soap*, const char*, char **, const char*);

#endif

/* End of soapH.h */
