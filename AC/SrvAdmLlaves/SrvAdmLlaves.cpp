static const char* _SRVADMLLAVES_CPP_VERSION_ ATR_USED = "SrvAdmLlaves @(#)"\
"DSIC07412AC_ 2007-12-18 SrvAdmLlaves.cpp 1.1.0/0";

//#VERSION: 1.1.0
/*******************************************************************
*** Servidor para recibir la llave privada de la AC en un buffer ***
*** preparado como servicio de seguridad para subirlo a la mem   ***
*** compartida. AGZ Septiembre 2004                              ***
*******************************************************************/


//- #include "CServidor.h" //- GHM: Se cambio el nombre
//- #include "SrvSeguridad.h"  //GHM: H�ctor modific� el nombre de la aplicaci�n
#include "SrvAdmLlaves.h"

//#################################################################################
#define PATH_CONF            "/usr/local/SAT/PKI/etc/"
#define PATH_BITA            "/var/local/SAT/PKI/"

#ifdef DBG
   #define ARCH_CONF            PATH_CONF "SrvAdmLlaves_dbg.cfg"
   #define ARCH_BITA            PATH_BITA "SrvAdmLlaves_dbg.log"
#else
   #define ARCH_CONF            PATH_CONF "SrvAdmLlaves.cfg"
   #define ARCH_BITA            PATH_BITA "SrvAdmLlaves.log"
#endif

//################################################################################
//   Variables globales
//################################################################################

//-static char bLLave[30];
//-- static char* PATHS[] = {"ROOT","RUTAC","AC1","ACK","BLLAVE"};
static std::string  pSSL;

CBitacora* Crea_CBitacora()
{
   return new CBitacora(ARCH_BITA);
}

CSrvUnx* Crea_CSrvUnx()
{
   return new CServidor(ARCH_CONF);
}

bool libSrvUnx_Fin()
{
   //- iniciaLibOpenSSL(); //- GHM(070625)
   terminaLibOpenSSL();  //+ GHM(070625)
   return true;
}

bool libSrvUnx_Ini()
{
   //- terminaLibOpenSSL(); //+ GHM(070625)
   //>>+ GHM(070625): Tiempo para capturar el programa en debugger
   #if defined( DBG ) 
      sleep(30);
   #endif
   iniciaLibOpenSSL();     
   //<<+ GHM(070625)
   return true;
}

//################################################################################
#ifdef DBG
   #define ESPERA  450
#else
   #define ESPERA  300
#endif

bool CServidor::
SetParmsSktProc(CSSL_parms& parms)
{
   //- ArchivosConfg conFile;
   //>>+ GHM: Cambios para utilizar Sgi_ConfigFile
   int         r1, r2,     r3,   r4,    r5;
   std::string ac, acPath, cert, privK    ;	
   
   CConfigFile *Configuracion = new CConfigFile(ARCH_CONF);
   int error = Configuracion->cargaCfgVars();
   if (error)
      Bitacora->escribePV(BIT_ERROR, "Problemas al leer el archivo de configuraci�n (%d): %s", error, ARCH_CONF);
   
   r1 = Configuracion->getValorVar("[GENERAL]", "ROOT"   , &ac    );
   r2 = Configuracion->getValorVar("[GENERAL]", "RUTAC"  , &acPath);
   r3 = Configuracion->getValorVar("[GENERAL]", "AC1"    , &cert  );
   r4 = Configuracion->getValorVar("[GENERAL]", "ACK"    , &privK );
   r5 = Configuracion->getValorVar("[GENERAL]", "BLLAVE" , &pSSL);

   if (r1 || r2 || r3 || r4 || r5)
   {
      Bitacora->escribePV(BIT_ERROR, "Error al leer los par�metros de configuraci�n (%d,%d,%d,%d,%d)",
            r1, r2, r3, r4, r5);
      return false;
   }else
       Bitacora->escribePV(BIT_DEBUG, "Par�metros de configuraci�n (%s,%s,%s,%s)",
                                      ac.c_str(), acPath.c_str(), cert.c_str(), privK.c_str());
   struct timeval espera = {ESPERA, 0};

  /* err = conFile.VarConfg(5, PATHS, arcConf, root, rutaac, ac1, ack, llave);
   if(err != 0)
      Bitacora->escribePV(BIT_ERROR, "Error al extraer los datos de configuraci�n.(%i)", err);*/
   parms.SetServicio(ESSL_entero, &espera, ac.c_str(), acPath.c_str(), cert.c_str(), privK.c_str(),
                     verify_callback, password_cb);
   /*uint8 ps1[255];
   int lps1 = sizeof(ps1);
   memset(ps1, 0, sizeof(ps1));
   desencripta((uint8*)llave, strlen(llave), ps1, &lps1);
   strncpy(llave, (char*)ps1, lps1);
   llave[lps1] = '\0';
   memcpy(bLLave, llave, strlen(llave));*/
   //parms.SetServicio(EPlano);
   return true;
}

int verify_callback(int ok, X509_STORE_CTX *store)
{
   char data[256];
   if (!ok)
   {
      X509 *cert = X509_STORE_CTX_get_current_cert(store);
      int depth = X509_STORE_CTX_get_error_depth(store);
      int err = X509_STORE_CTX_get_error(store);
      Bitacora->escribePV(BIT_ERROR, "Error con el certificado %i", depth);
      X509_NAME_oneline(X509_get_issuer_name(cert),data,256);
      Bitacora->escribePV(BIT_ERROR, "\tissuer  = %s", data);
      X509_NAME_oneline(X509_get_subject_name(cert),data,256);
      Bitacora->escribePV(BIT_ERROR, "\tsubject = %s", data);
      Bitacora->escribePV(BIT_ERROR, "error = %i: %s", err, X509_verify_cert_error_string(err));
   }
   return (ok);
}

static int password_cb(char *buf, int size, int rwflag, void *password)
{
   int SSLsize = pSSL.size();
   if (size < SSLsize)
   {
      Bitacora->escribePV(BIT_ERROR, "Error al asignar el password de SSL, buffer menor del requerido(%d -> %d)",
                          size, pSSL.size());
      return 0;
   }
   /* password = bLLave;
   strncpy(buf, (char *)(password), size);
   buf[size - 1] = '\0'; //buf[strlen(bLLave)] = '\0';
   return(strlen(buf));
   */ 
   if (!desencripta((uint8*) pSSL.c_str(), pSSL.size(), (uint8*) buf, &size))
   {
      Bitacora->escribe(BIT_ERROR, "Error al desencriptar  el password del archivo de configuraci�n");
      return 0;
   }
   return size;
}

bool CServidor::Proceso()
{
   bool ok = true;
   if(!Central())
   {
      memset(texto, 0, sizeof(texto));
      sprintf(texto, "Ocurrio un error.");
      Bitacora->escribe(BIT_INFO, texto);
      ok = false;
   }
   return ok;
}

bool CServidor::Central()
{
   err = recibe(); 
   if(err != 0)  
   {
      memset(texto, 0, sizeof(texto));
      sprintf(texto, "Ocurrio el error: %i", sgiErrorBase(err));
      Bitacora->escribe(BIT_INFO, texto);
      return false;
   }
   if(!subeLlave())
   {
      memset(texto, 0, sizeof(texto));
      sprintf(texto, "Ocurrio el error: %i", sgiErrorBase(err));
      Bitacora->escribe(BIT_INFO, texto);
      return false;
   }
   err = envia((uint8*)"Se inicio el Servicio con �xito.", strlen("Se inicio el Servicio con �xito."));
   if(err != 0)
      return false;
   delete mem;
   delete srv;
   return true;
}

intE  CServidor::recibe()
{
   limpiaBuf();
   intE error = 0;
   error = SktProc->Recibe((uint8*)buffM, lbuffM);
   if(error != 0)
      return error;
   return error;
}

intE  CServidor::envia(uchar *buffer, int lbuffer)
{
   limpiaBuf();
   intE error = 0;
   if(lbuffer > lbuffM)
      return sgiError(ERR_NO_SPC, eAPL);
   lbuffM = lbuffer;
   memcpy(buffM, buffer, lbuffM);
   error = SktProc->Envia((uint8*)buffM, lbuffM);
   if(error != 0)
      return error;
   return error;
}

bool  CServidor::subeLlave()
{
   bool ok = true;
   CSHMemPKI *mem = new CSHMemPKI(1147483830, TamBufShMem);
   ServSegInfo *srv = new ServSegInfo;
   if(!mem->abre(true))
      return false;
   memset(srv, 0, sizeof(struct serv_info_st));
   memcpy(srv, buffM, sizeof(struct serv_info_st));
   ok = mem->registraServ(srv);  
   if(ok == false)
      Bitacora->escribe(BIT_INFO, "Ocurri� un error en la SHM."); 
   return ok;
}

CServidor::CServidor(char *archConf)
{
   arcConf = archConf;
}

CServidor::~CServidor()
{
   arcConf = NULL;
}

void CServidor::limpiaBuf()
{
   lbuffM = sizeof(buffM);
   memset(buffM, 0, sizeof(buffM));
}
