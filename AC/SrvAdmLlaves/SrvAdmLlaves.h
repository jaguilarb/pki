#ifndef _SRVADMLLAVES_H_
#define _SRVADMLLAVES_H_
static const char* _SRVADMLLAVES_H_VERSION_ ATR_USED = "SrvAdmLlaves @(#)"\
"DSIC07412AC_ 2007-12-18 SrvAdmLlaves.h 1.1.0/0";

//#VERSION: 1.1.0
#include <SgiTipos.h>     
#include <Sgi_SrvUnx.h>
//-#include <ArchivosConfg.h> //- GHM (070618)
#include <Sgi_ConfigFile.h>   //+ GHM (070618)
//-#include <PwdProtection.h> //- GHM (070427)
#include <Sgi_ProtegePwd.h>   //+ GHM (070427)
//-#include "ShMemPKI.h"           //- GHM (070427)
#include <Sgi_MemCompartida.h>     //+ GHM (070427)
//-#include "SecretoCompartido.h"  //- GHM (070427)
#include <Sgi_SecretoCompartido.h> //+ GHM (070427)

#define ERR_NO_SPC		-1

static int password_cb(char *buf, int size, int rwflag, void *password);
int verify_callback(int ok, X509_STORE_CTX *store);

class CServidor : public CSrvUnx
{
protected:
   char *arcConf;
   char texto[256];
   uchar buffM[1024 * 32];
   int lbuffM;
   intE err;
   bool  Proceso();
   bool  SetParmsSktProc(CSSL_parms& parms);
   char root[200];
   char rutaac[200];
   char ac1[200];
   char ack[200];
   char llave[30];
   ServSegInfo *srv;
   CSHMemPKI *mem; 
public:
   bool  Central();
   intE  inicia();
   intE  conecta();
   intE  recibe();
   intE  envia(uchar*, int);
   bool  subeLlave();
   void  limpiaBuf();
   CServidor(char*);
   virtual ~CServidor();
};
#endif
