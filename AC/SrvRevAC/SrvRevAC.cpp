/*##############################################################################
  ###  PROYECTO:              PKI-SAT                                        ###
  ###  MODULO:                SrvRevAC  Servidor de Revocación de la AC      ###
  ###                                                                        ###
  ###  DESARROLLADORES:                                                      ###
  ###                         Miguel Angel Mendoza López      MAML           ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       Lunes 30, agosto del 2010                      ###
  ###                                                                        ###
  ##############################################################################
         1         2         3         4         5         6         7         8
12345678901234567890123456789012345678901234567890123456789012345678901234567890
*/
/*##############################################################################
   VERSION:
      V.01.00 

#################################################################################*/

static const char* _SRVREVAC_CPP_VERSION_ ATR_USED = "@(#) SrvRevAC ( L : DSIC10391AC_ : SrvRevAC.cpp : 1.0.0 : 0 : 23/09/10)";

#include <string>
#include <sys/socket.h>

#include <SgiTipos.h>
#include <Sgi_SrvUnx.h> 
#include <Sgi_MsgPKI.h>
#include <Sgi_ConfigFile.h>
#include <COperacionRevAC.h>
#include <Sgi_ProtegePwd.h>

#include <SrvRevAC.h>
#include <CConexionRevAC.h>
#include <CRevocaSellosAC.h>

//#################################################################################
#define PATH_CONF            "/usr/local/SAT/PKI/etc/"
#define PATH_DBIT            "/var/local/SAT/PKI/"

#ifdef DBG
   #define ARCH_CONF            PATH_CONF "SrvRevAC_dbg.cfg"
   #define PATH_BITA            PATH_DBIT "SrvRevAC_dbg.log"
#else
   #define ARCH_CONF            PATH_CONF "SrvRevAC.cfg"
   #define PATH_BITA            PATH_DBIT "SrvRevAC.log"
#endif

//################################################################################
//   Variables globales
//################################################################################

CSrvRevAC* ptrSrvRevAC = NULL;
static string  pSSL;

//################################################################################
// Creación del objeto de Bitacora
//################################################################################
CBitacora* Crea_CBitacora()
{
   return new CBitacora(PATH_BITA);
}
//################################################################################
// Creación del objeto principal para el servicio de Rev la AC
//################################################################################
CSrvUnx* Crea_CSrvUnx()
{
   return ptrSrvRevAC = new CSrvRevAC;
}
//################################################################################
bool libSrvUnx_Ini()
{
   #if defined( DBG )
      sleep(2);
   #endif
   #if defined( DBG ) 
      Bitacora->setNivel(BIT_DEBUG);
   #endif
   iniciaLibOpenSSL();
   return true;
}
//################################################################################
bool libSrvUnx_Fin()
{
   terminaLibOpenSSL();
   return true;
}
//################################################################################
static int verificacionSSL(int ok, X509_STORE_CTX *store)
{
   char data[256];
   
   if (!ok)
   {
      X509 *cert = X509_STORE_CTX_get_current_cert(store);
      int depth  = X509_STORE_CTX_get_error_depth(store);
      int err    = X509_STORE_CTX_get_error(store);

      Bitacora->escribePV(BIT_ERROR, "Error con el certificado %i", depth);
      X509_NAME_oneline(X509_get_issuer_name(cert), data, sizeof(data));
      Bitacora->escribePV(BIT_ERROR, "\tissuer  = %s", data);
      X509_NAME_oneline(X509_get_subject_name(cert), data, sizeof(data));
      Bitacora->escribePV(BIT_ERROR, "\tsubject = %s", data);
      Bitacora->escribePV(BIT_ERROR, "error = %i: %s", err, X509_verify_cert_error_string(err));
   }
   return ok;
}

static int passwordSSL(char *buf, int size, int rwflag, void *password)
{
   int SSLsize = pSSL.size();
   
   rwflag   = rwflag;
   password = password;
   if (size < SSLsize)
   {
      Bitacora->escribePV(BIT_ERROR, "Error al asignar el password de SSL, buffer menor del requerido(%d -> %d)", 
                          size, pSSL.size());
      return 0;
   }
  
   if (!desencripta((uint8*) pSSL.c_str(), pSSL.size(), (uint8*) buf, &size))
   {
      Bitacora->escribe(BIT_ERROR, "Error al desencriptar  el password de SSL"); 
      return 0;
   }
   
   return size;
}
//################################################################################
//################################################################################
//################################################################################
                        
CSrvRevAC::CSrvRevAC()
   : Configuracion(ARCH_CONF),
     MsgCli(),
     BDAC(NULL),
     error(0)
{
}
//################################################################################
CSrvRevAC::~CSrvRevAC()
{
   if (BDAC)
      delete BDAC;
   BDAC = NULL;
}
//################################################################################

#define ESPERA  30 

bool CSrvRevAC::SetParmsSktProc(CSSL_parms& parms)
{
   string ac, acPath, cert, privK, nvlBitac, claves[] = { "CERT_SKT_AC", "RUTA_CERTS", "CERTCLI", "LLAVCLI", "PWDCLI" };
  
   if ( (error = Configuracion.getValoresConf("[CONEXION]", 5, claves, &ac, &acPath, &cert, &privK, &pSSL ) ) )
      Bitacora->escribePV(BIT_ERROR, "Error(%d): no existen las variables en el arch. de configuración([CONEXION], CERT_SKT_AC - PWDCLI).", error);
   else
   {
      if ( (error = Configuracion.getValorVar("[BITACORA]", "NIVELBIT" , &nvlBitac)) )
         Bitacora->escribePV(BIT_ERROR, "Error(%d) al leer el nivel de bitacora)", error);
      else
      {
         Bitacora->escribePV(BIT_DEBUG, "De archivo de configuración se obtuvo:ROOT (%s), RUTAAC (%s), AC1(%s), ACK(%s) y NIVELBIT(%s)",
                                  ac.c_str(), acPath.c_str(), cert.c_str(), privK.c_str(), nvlBitac.c_str() );
         BIT_NIVEL iNvlBitac = (BIT_NIVEL) atoi(nvlBitac.c_str());
         if (Bitacora)
            Bitacora->setNivel( iNvlBitac);
         struct timeval espera = {ESPERA, 0};
         parms.SetServicio(ESSL_entero, &espera, ac.c_str(), acPath.c_str(), cert.c_str(), privK.c_str(), 
                                 verificacionSSL, passwordSSL);
      }
   }
   return !error;
}
//********************************************************************************
bool CSrvRevAC::envOperInv()
{
   char errorn[128];

   sprintf(errorn, "Operación inválida (%d)",  MsgCli.tipOperacion());
   error = MsgCli.setMensaje(MSGERROR, "I", 1, "0", 1, errorn, strlen(errorn)); 
   if (!error)
      error = SktProc->Envia(MsgCli.buffer, MsgCli.tamMensaje()); 
   Bitacora->escribePV(BIT_ERROR, "errorn=%s, error=%d", errorn, error);
   Bitacora->escribe(BIT_INFO, "Se cerrará la aplicación."); 
   return !error;
}
//################################################################################
bool CSrvRevAC::Inicia()
{
   // Cargar archivo de configuración
   error = Configuracion.cargaCfgVars();
   if (error)
      Bitacora->escribePV(BIT_ERROR, "Problemas al leer el archivo de configuración (%d): %s", error, ARCH_CONF);
   else
      Bitacora->escribe(BIT_INFO, "Ok, se cargo el archivo de configuración correctamente." );
   return !error;
}
//################################################################################
bool CSrvRevAC::InicioDeSesion()
{
   CConexionRevAC* conexion = new CConexionRevAC(Configuracion, MsgCli, SktProc, *BDAC);
   if (conexion)
   {
      error = (conexion->Operacion() != true);
      delete conexion;
   }
   else
      error = ERR_FALTA_MEMORIA;
   return !error;
}

//################################################################################
bool CSrvRevAC::Operaciones()
{
   intE error;
   int ControlDesincronizacion = 0;
   for (;;)
   {
      error = MsgCli.Recibe(SktProc);
      if (!error)
      {
         COperacionRevAC *operacion = NULL;
         int oper = MsgCli.tipOperacion();
         switch (oper)
         {
            case SOLREVAUT        : operacion = new CRevocaSellosAC (Configuracion, MsgCli, SktProc, *BDAC); break;
            default : envOperInv(); ControlDesincronizacion = -1; break;
         }
         if (ControlDesincronizacion == -1)
            break;
         else if (!operacion)
            Bitacora->escribePV(BIT_ERROR, "Error al generar el objeto de operación %d (errno = %d)", oper, errno);
         else 
         {
            operacion->Operacion();
            delete operacion;
            if (oper == SOLDESCONEXION)
               return false;
         }
         
      }
      else
      {
         if (sgiErrorBase(error) != ERR_SKTTIMEOUT)
         {
            Bitacora->escribe(BIT_ERROR, error, "Error al recibir solicitud de operación");
            break;
         }
         struct timespec ts = { 0, 1000000};
         nanosleep(&ts, NULL);
      }
   }
   
   return !error;         
}
//################################################################################
bool CSrvRevAC::Proceso()
{
   bool ok = false;
   if ( conectaBD() ) 
   {
      ok = InicioDeSesion() && Operaciones(); 
      desconectaBD();
   }
   return ok;
}
//################################################################################
void CSrvRevAC::desconectaBD()
{
   BDAC->desconecta();
   delete BDAC;
   BDAC = NULL;
}
//################################################################################
bool CSrvRevAC::conectaBD()
{
   string s_srv, s_db, s_usr, s_paswd, s_role, sError, claves[] = {"EQ", "BD", "US", "PW", "ROLE"},claves_ora[] = {"usr", "pwd", "host", "port", "sid"};
   CBD* &bd = BDAC;

   if (bd)
      Bitacora->escribePV(BIT_ERROR, "Error(%d) al abrir BD: ya se encuentra abierta la conexion a la BD.", error = ERR_NOT_DESENCRYPT_PWD);
   else 
   {
   
		 #if ORACLE
		   error = Configuracion.getValoresConf("[BD_AC_ORA]", 5, claves_ora, &s_usr, &s_paswd, &s_srv, &s_db, &s_role);
		 #else
		   error = Configuracion.getValoresConf("[BD_AC]", 5, claves, &s_srv, &s_db, &s_usr, &s_paswd, &s_role);
		 #endif
	   
		 if (error)
		   Bitacora->escribePV(BIT_ERROR, "Error(%d) al obtener vlores de conexion.", error = ERR_NOT_DESENCRYPT_PWD);	   
		 else 
		   if (!procesaPassword(s_paswd, &s_paswd))
			 Bitacora->escribePV(BIT_ERROR, "Error(%d) al desencriptar el password de conexion a la BD.", error = ERR_NOT_DESENCRYPT_PWD);
		   else
		   {
			  bd = new CBD(Bitacora);
			  bool ok = bd != NULL;
			  
			  #if ORACLE
				ok = ok && bd->setConfig(s_usr.c_str(), s_paswd.c_str(), s_srv.c_str(), s_db.c_str(), s_role.c_str());
			  #else
				ok = ok && bd->setConfig(s_srv.c_str(), s_db.c_str(), s_usr.c_str(), s_paswd.c_str(), s_role.c_str());
			  #endif
			  
			  ok = ok && bd->conecta();
			  if (!ok)
				 Bitacora->escribePV(BIT_ERROR, "Error(%d) al conectarse a la BD.", error = ERR_NOT_DESENCRYPT_PWD);
		   }
	}
   return !error;
}
//##############################################################################
bool CSrvRevAC::procesaPassword(string s_pwdEnc, string *s_pwdDes)
{
   bool b_resOperacion = true;
   int i_pwdSalida = s_pwdEnc.size();
   char *psz_pwdSalida = new char[i_pwdSalida];
   if (!desencripta((uint8*) s_pwdEnc.c_str(), i_pwdSalida, (uint8*) psz_pwdSalida, &i_pwdSalida))
      b_resOperacion = false;
   else
      s_pwdDes->assign(psz_pwdSalida, i_pwdSalida);
   delete[] psz_pwdSalida;
   return b_resOperacion;
}

//################################################################################

