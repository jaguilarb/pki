#ifndef _COSLTORASRVREVAC_H_
#define _COSLTORASRVREVAC_H_

static const char* _COSLTORASRVREVAC_H_VERSION_ ATR_USED = "@(#) SrvAC ( L : DSIC10392AR_ : ConsltORASrvRevAC.h : 1.0.0 : 10/10/14)";

//#VERSION: 1.1.0
/*##############################################################################
  ###  PROYECTO:              PKI-SAT                                        ###
  ###  MODULO:                Servicio AC                                    ###
  ###                                                                        ###
  ###                                                                        ###
  ###  DESARROLLADORES:       Guadalupe Negrete Castro         GNC           ###
  ###                                                                        ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       Viernes 10, octubre del 2014                   ###
  ###                                                                        ###
  ##############################################################################
         1         2         3         4         5         6         7         8
12345678901234567890123456789012345678901234567890123456789012345678901234567890

################################################################################
  VERSION:
     V.1.0.0  (20141010- )    GNC: Version Original
                              Consutas para el Servicio de Autoridad Certificadora PKI-SAT
  

#################################################################################*/



//#VERSION: 1.1.0


#define QS_EDO_TIPO_RFC_CERT      "SELECT EDOCER, TIPCERCVE, RFC FROM SEG_PKI.SEGT_CERTIFICADO WHERE NOSERIE = '%s';"
#define QS_EDO_VF_SOLOP_CERT      "select EDOCER, VIGFIN, SOLOPERCVE from SEG_PKI.SEGT_CERTIFICADO where RFC = '%s' and NOSERIE = '%s';"
#define QU_CERTIFICADO            "update SEG_PKI.SEGT_CERTIFICADO set SOLOPERCVE = '%s', OPERSECAR = '%s', EDOCER = 'R', VIGFIN = '%s', FECREG = '%s' where RFC = '%s' and NOSERIE = '%s';"
#define QU_CERT_ROLLBACK          "update SEG_PKI.SEGT_CERTIFICADO set EDOCER = 'A', VIGFIN = '%s', SOLOPERCVE = '%s' where RFC = '%s' and NOSERIE = '%s';"



			 

#define SP_REG_SOLOPER				"SEG_PKI.segsf_regsoloper"
#define SP_REG_SOLOPERDET 			"SEG_PKI.segsp_regoperdet"






#endif
