#ifndef _CREVOCASELLOSAC_H_
   #define _CREVOCASELLOSAC_H_
static const char* _CREVOCASELLOSAC_H_VERSION_ ATR_USED = "@(#) SrvRevAC ( L : DSIC10391AC_ : CRevocaSellosAC.h : 1.0.0 : 0 : 23/09/10)";

//#VERSION: 1.0.0


#define ERR_SEP_CAD	 100
#define ERR_EDO_CER	 102
#define ERR_REG_OPER	 105
#define ERR_SET_MSG	 106
#define ERR_OBT_VFIN	 107
#define ERR_OBT_FREG  108
#define ERR_ACT_BD    109
#define ERR_ROLLBACK  110 
#define ERR_NUMCAMPOS 111 
#define ERR_VALID_REV 112

class CRevocaSellosAC : public COperacionRevAC
{
   private:
      static int m_MsgRev;
      char   m_numSerieRev[21];
      int    m_inumSerieRev, error;
      string m_vigFinTmp, m_numOperTmp, m_FecRev;      
          
   protected:
      bool preProceso();
      bool Proceso();
      const char* getNombreOperacion();
      int getTipOper();

      bool revocaCert();
      void iniciaVarsRev();
      bool rollBackRevocacion();

      bool validaRev();

   public:
      CRevocaSellosAC(CConfigFile& config, MensajesSAT& msgcli, CSSL_Skt* skt, CBD& BDAC);
      ~CRevocaSellosAC();
      
};

#endif //_CREVOCASELLOSAC_H_
