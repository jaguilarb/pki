#ifndef _COSLTSRVREVAC_H_
#define _COSLTSRVREVAC_H_

static const char* _COSLTSRVREVAC_H_VERSION_ ATR_USED = "@(#) SrvAC ( L : DSIC10392AR_ : ConsltSrvRevAC.h : 1.0.0 : 10/10/14)";

//#VERSION: 1.1.0
/*##############################################################################
  ###  PROYECTO:              PKI-SAT                                        ###
  ###  MODULO:                Servicio AC                                    ###
  ###                                                                        ###
  ###                                                                        ###
  ###  DESARROLLADORES:       Guadalupe Negrete Castro         GNC           ###
  ###                                                                        ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       Viernes 10, octubre del 2014                   ###
  ###                                                                        ###
  ##############################################################################
         1         2         3         4         5         6         7         8
12345678901234567890123456789012345678901234567890123456789012345678901234567890

################################################################################
  VERSION:
     V.1.0.0  (20141010- )    GNC: Version Original
                              Consutas para el Servicio de Autoridad Certificadora PKI-SAT
  

#################################################################################*/



//#VERSION: 1.1.0


#define QS_EDO_TIPO_RFC_CERT      "SELECT edo_cer, tipcer_cve, rfc FROM Certificado WHERE no_serie = '%s';"
#define QS_EDO_VF_SOLOP_CERT      "select edo_cer, vig_fin, soloper_cve from certificado where rfc = '%s' and no_serie = '%s';"
#define QU_CERTIFICADO            "update certificado set soloper_cve = '%s', oper_sec_ar = '%s', edo_cer = 'R', vig_fin = '%s', fec_reg = '%s' where rfc = '%s' and no_serie = '%s';"
#define QU_CERT_ROLLBACK          "update certificado set edo_cer = 'A', vig_fin = '%s', soloper_cve = '%s' where rfc = '%s' and no_serie = '%s';"





			 

#define SP_REG_SOLOPER				"sp_regSolOper"
#define SP_REG_SOLOPERDET 			"sp_regOperDet"













#endif















