/*##############################################################################
  ###  PROYECTO:              PKI-SAT                                        ###
  ###  MODULO:                Servicio Revocacion de la AC                   ###
  ###                                                                        ###
  ###                                                                        ###
  ###  DESARROLLADORES:       Miguel Angel Mendoza L�pez MAML                ###
  ###                                                                        ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       Lunes 30, agosto del 2010                      ###
  ###                                                                        ###
  ##############################################################################
         1         2         3         4         5         6         7         8
12345678901234567890123456789012345678901234567890123456789012345678901234567890

################################################################################
  VERSION:
     V.1.0.0  

#################################################################################*/
static const char* _COPERACIONREVAC_CPP_VERSION_ ATR_USED = "@(#) SrvRevAC ( L : DSIC10391AC_ : COperacionRevAC.cpp : 1.0.0 : 0 : 23/09/10)";

#include <Sgi_SrvUnx.h>
#include <Sgi_MsgPKI.h>
#include <Sgi_ConfigFile.h>
#include <Sgi_ProtegePwd.h>
#include <COperacionRevAC.h>
//##################################################################################

 //GNC Se agrega definici�n de consultas
#if ORACLE
  #include <CosltORASrvRevAC.h>
#else
  #include <CosltSrvRevAC.h>
#endif
//##################################################################################
COperacionRevAC::COperacionRevAC(CConfigFile& config, MensajesSAT& msgcli, CSSL_Skt* skt, CBD& BDAC) :
   m_Configuracion(config), m_MsgCli(msgcli), m_SktProc(skt), m_BDAC(BDAC)
{
   error = 0;
}
//##################################################################################
COperacionRevAC::~COperacionRevAC()
{  
}
//##################################################################################
bool COperacionRevAC::Operacion()
{
   bool ok = false;
   
   Bitacora->escribePV(BIT_INFO, "Inicio Operaci�n: %s", getNombreOperacion());
   if (preProceso())
      ok = Proceso();
   
   Bitacora->escribePV(BIT_INFO, "Fin Operaci�n: %s", getNombreOperacion());
   return ok;
}
//##############################################################################
int COperacionRevAC::envia()
{
   error = m_SktProc->Envia(m_MsgCli.buffer, m_MsgCli.tamMensaje());
   if (error)
      Bitacora->escribePV(BIT_ERROR, "Error(%d) al enviar al cliente de Revocacion-AR.", error);
   return error;
}

//##############################################################################
int COperacionRevAC::setMsgErr(const char *psz_desc, int i_error )
{
   // Hay 2 mensajes de error: ERRORREVAUT � CERTNOREVBD, los 2 tienen 3 parms 
   error = m_MsgCli.setMensaje(i_error, m_numTram, m_inumTram, m_NumSerieOp.c_str(), m_NumSerieOp.length(), psz_desc, strlen(psz_desc));
   if(error)
      Bitacora->escribePV(BIT_ERROR, "Error(%d) en setMensaje al crear mensaje de error en la Revocaci�n.", error);
   return error;
}

/*##################################################################################

    PROPOSITO: Envia mensaje de Error al Cliente Revocaci�n-AR, hace el registro del error en la
               base de datos y escribe en la bitacora.
    PREMISA  : Los elementos de BD y de Mensajes deben estar iniciados.
    EFECTO   :
    ENTRADAS : Entero de referencia a la aplicacion en donde sucedio el error, error
               local de la aplicacion, mensaje, medio 'P', 'O'.
    SALIDAS  : regresa false.
###################################################################################*/

bool COperacionRevAC::msgCliError(char c_tipErr, const char* psz_msg, int i_error )
{
   Bitacora->escribePV(BIT_ERROR, "%s, Error(%d), Num_Serie(%s), RFC(%s) ", psz_msg, i_error, m_NumSerieOp.c_str(), m_rfcOp );
   switch(c_tipErr)
   {
      case 'I': 
      default:
      {
         error = setMsgErr(psz_msg);
         break;
      }
      case 'N':
      {
         error = setMsgErr(psz_msg, CERTNOREVBD);
         break;
      }
   }
   if (!error)
   {
      error = envia();
      if (error)
         Bitacora->escribePV(BIT_ERROR, "Error(%d) al enviar mensaje de Error(%d) al Cliente Revocacion-AR.", error, i_error);
   }
   return !error;
}

//##################################################################################
bool COperacionRevAC::regOperacionBD()
{
   bool ok = false;
   int i_resRegOp = -1;
   Bitacora->escribePV(BIT_DEBUG,"Datos a registrar: '%s', %d, %d, '%s', '%s', '%s'", m_numTram, atoi(m_secOp),
                            m_MsgCli.tipOperacion(), m_rfcOp, m_NumSerieOp.c_str(), m_agcCve);
	
	#if ORACLE							
          ok = m_BDAC.ejecutaFuncion(SP_REG_SOLOPER, "'%s',%d,%d,'%s','%s','%s'", m_numTram, atoi(m_secOp), 
                            m_MsgCli.tipOperacion(), m_rfcOp, m_NumSerieOp.c_str(), m_agcCve); 
    #else
	      ok = m_BDAC.ejecutaSP(SP_REG_SOLOPER, "'%s',%d,%d,'%s','%s','%s'", m_numTram, atoi(m_secOp), 
                            m_MsgCli.tipOperacion(), m_rfcOp, m_NumSerieOp.c_str(), m_agcCve);
    #endif	
							
							
   if (ok && m_BDAC.getValores("i", &i_resRegOp))
   {
     if (i_resRegOp != 0)
        ok = false;
   }
   else 
   {
      Bitacora->escribePV(BIT_ERROR,"Datos no registrados: %s, %d, %d, %s, %s, %s", m_numTram, atoi(m_secOp),
                            m_MsgCli.tipOperacion(), m_rfcOp, m_NumSerieOp.c_str(), m_agcCve);
      ok = false;
   }
   return ok;
}

//################################################################################
bool COperacionRevAC::regOperacionBDDet(int sec, int op)
{
   #if ORACLE
       return m_BDAC.ejecutaSP(SP_REG_SOLOPERDET, "'%s',%d,%d,:i,:i", m_numTram, sec, op);
   #else
       return m_BDAC.ejecutaSP(SP_REG_SOLOPERDET, "'%s',%d,%d", m_numTram, sec, op);
   #endif
}
//################################################################################


