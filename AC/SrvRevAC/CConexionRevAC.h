#ifndef _CCONEXIONREVAC_H_
   #define _CCONEXIONREVAC_H_
static const char* _CCONEXIONREVAC_H_VERSION_ ATR_USED = "@(#) SrvRevAC ( L : DSIC10391AC_ : CConexionRevAC.h : 1.0.0 : 0 : 23/09/10)";

//#VERSION: 1.0.0

#define TERMINAR	1501

#define ERR_NO_ENVIA    1
#define ERR_NO_GENSESNO	2
#define ERR_MSG_ERROR	3
#define ERR_DESENCR     4

class CConexionRevAC : public COperacionRevAC
{
   private:
      int error;
      char m_NoSesion[39], m_firma[684];
      int  m_iNoSesion, m_ifirma;

      string m_CertAC, m_KeyAC, m_PassAC, m_ARCert;
      
      bool operacion;

      int  GenSesionNo(int bits, char *sesionNo, int *res);
      int  procesaMsg();
      int  getValoresMsg();
      int  ConectaARCli();

      bool preProceso(){ return true; };
      bool Proceso();
      const char* getNombreOperacion();
      int getTipOper();

   public:
      CConexionRevAC(CConfigFile& config, MensajesSAT& msgcli, CSSL_Skt* skt, CBD& BDAC);
      ~CConexionRevAC();
};

#endif // _CCONEXIONREVAC_H_
