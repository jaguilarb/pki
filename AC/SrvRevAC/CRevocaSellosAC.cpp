static const char* _CREVOCASELLOSAC_CPP_VERSION_ ATR_USED = "@(#) SrvRevAC ( L : DSIC10391AC_ : CRevocaSellosAC.cpp : 1.0.0 : 0 : 23/09/10)";

//#VERSION: 1.0.0

#include <Sgi_Fecha.h> 
#include <Sgi_SrvUnx.h>
#include <Sgi_MsgPKI.h>
#include <Sgi_ConfigFile.h>
#include <COperacionRevAC.h>
#include <CRevocaSellosAC.h>
//###################################################################
//GNC Se agrega definici�n de consultas

#if ORACLE
	#include <CosltORASrvRevAC.h>
#else
    #include <CosltSrvRevAC.h> 
#endif

//###################################################################
int CRevocaSellosAC::m_MsgRev = -1;

CRevocaSellosAC::CRevocaSellosAC(CConfigFile& config, MensajesSAT& msgcli, CSSL_Skt* skt, CBD& BDAC) :
COperacionRevAC(config, msgcli, skt, BDAC)
{
   error = 0;
}

CRevocaSellosAC::~CRevocaSellosAC()
{
   m_MsgRev = -1;
}

 
bool CRevocaSellosAC::preProceso()
{
   if (m_MsgRev == -1)
      m_MsgRev = m_MsgCli.tipOperacion();
 
   iniciaVarsRev();
   if (m_MsgRev == SOLREVAUT)
      error = sgiErrorBase(m_MsgCli.getMensaje(SOLREVAUT, m_firmaOp, &m_ifirmaOp, m_numTram, &m_inumTram,
                           m_secOp, &m_isecOp, m_rfcOp, &m_irfcOp, m_agcCve, &m_iagcCve, m_numSerieRev, &m_inumSerieRev));
   if(!error)
      m_NumSerieOp.assign(m_numSerieRev, m_inumSerieRev);
   else 
      msgCliError('I', "Error al procesar el mensaje SOLREVAUT", error );
   return !error;
}

bool CRevocaSellosAC::Proceso()
{
   if(!regOperacionBD())
      msgCliError('I', "Error al insertar la solicitud en la BD.", error = ERR_REG_OPER );
   else if ( m_MsgRev == SOLREVAUT )
   {
      if ( validaRev() ) // hay de 2: hacer funcion q traiga datos d la tabla cert o crear un store proced q lo verifica como en la AR 
      {
         if ( revocaCert() )
         {
            Bitacora->escribePV(BIT_INFO, "Certificado: %s revocado correctamente por Autoridad.", m_numSerieRev);
            Bitacora->escribePV(BIT_DEBUG, "N�mero de tr�mite: %s ; tama�o: %i", m_numTram, m_inumTram);
               
            //error = sgiErrorBase(m_MsgCli.setMensaje(REVAUT, m_numTram, m_inumTram, m_FecRev.c_str(), strlen(m_FecRev.c_str())) );
            error = sgiErrorBase(m_MsgCli.setMensaje(REVAUT, m_numTram, m_inumTram, m_numSerieRev, m_inumSerieRev ) );  
            if (!error)
            {
               if (!regOperacionBDDet(atoi(m_secOp), REVAUT))
                  Bitacora->escribePV(BIT_ERROR, "Error al registrar en la BD la operacion a detalle del Mensaje (%i).", REVAUT);
               error = envia();
               if(error)
                  rollBackRevocacion();
            }
            else 
            {
               msgCliError('I', "Error al preparar el mensaje.(REVAUT)", ERR_SET_MSG );
               Bitacora->escribePV(BIT_ERROR, "Error (%i) Al preparar el mensaje.(REVAUT)", error);
               rollBackRevocacion();
            }
         }
         else 
            Bitacora->escribePV(BIT_ERROR, "Error(%d) Al revocar el Certificado en la BD.", error);
      }
   }
   return !error;
}

bool CRevocaSellosAC::validaRev()
{
   char edoCert, rfc[15];
   int  tipCert, msg_err = 0;

   if ( m_BDAC.consultaReg(QS_EDO_TIPO_RFC_CERT, m_numSerieRev) &&
        m_BDAC.getValores ("cib", &edoCert, &tipCert, rfc ) )
   {
      if (edoCert == 'R' && tipCert == 2 && !strncmp(rfc, m_rfcOp, strlen(m_rfcOp))  )
         msg_err = CERTYAREV; // El certificado ya fue revocado antes
      else if (edoCert != 'A')
         msg_err = CERTNOACTIVO;
      else if (tipCert != 2)
         msg_err = CERTSINSELLOS;
      else if ( strncmp(rfc, m_rfcOp, strlen(m_rfcOp)) )
         msg_err = CERTNOCORRRFC;
   }
   else
   {
      Bitacora ->escribePV(BIT_ERROR, "CUtilBDAR(I): No se obtuvo(%d) correctamente los datos en la BD del no_serie=%s",
                                      error = ERR_VALID_REV, m_numSerieRev);
      msgCliError('I', "Error al obtener los datos de la BD para el no_serie dado.", error );
   }

   if (msg_err)
   {
      if (msg_err == CERTYAREV)
         Bitacora->escribePV(BIT_ERROR, "El Certificado ya fue revocado(%d) anteriormente: no_serie=%s y rfc=%s", 
                                         msg_err, m_numSerieRev, m_rfcOp); 
      else
         Bitacora->escribePV(BIT_ERROR, "El certificado no se puede revocar(%d), tiene datos invalidos: no_serie=%s y rfc=%s",
                                         msg_err, m_numSerieRev, m_rfcOp );

      error = sgiErrorBase(m_MsgCli.setMensaje(msg_err, m_numTram, m_inumTram, m_numSerieRev, m_inumSerieRev ) );
      if (!error)
      {
         if (!regOperacionBDDet(atoi(m_secOp), msg_err))
            Bitacora->escribePV(BIT_ERROR, "Error al registrar en la BD la operacion a detalle del Mensaje (%i).", msg_err);
         error = envia();
      }
      else
         Bitacora->escribePV(BIT_ERROR, "Error (%i) Al preparar el mensaje de error(%d)", error, msg_err); //ERR_SET_MSG

   }
   return !msg_err && !error;
}

const char* CRevocaSellosAC::getNombreOperacion()
{
   if (m_MsgRev == -1)
      m_MsgRev = m_MsgCli.tipOperacion();

   if (m_MsgRev == SOLREVAUT)
      return "Revocaci�n por Autoridad desde la AR.";
   else return "Operaci�n no reconocida.";
}

int CRevocaSellosAC::getTipOper()
{
   if (m_MsgRev == -1)
      m_MsgRev = m_MsgCli.tipOperacion();
   return m_MsgRev;
}

bool CRevocaSellosAC::revocaCert(  )
{
   string sFecRev, sFecReg;

   if ( m_BDAC.consultaReg(QS_EDO_VF_SOLOP_CERT,m_rfcOp,  m_numSerieRev) )
   {
      if ( m_BDAC.getValores("ss", &m_vigFinTmp, &m_numOperTmp) )
      {
         CFecha* FuncFecha = new CFecha();
         if (FuncFecha == NULL)
            Bitacora->escribePV(BIT_ERROR, "Error(%d) de falta de memoria.", error = ERR_FALTA_MEMORIA );
         else if ( (error = FuncFecha->GetCurrentUTC(&sFecRev)) != 0)
         {
            Bitacora->escribePV(BIT_ERROR, "Al evaluar la fecha UTC:%s, se encontr� el error: %s", sFecRev.c_str(),
                                FuncFecha->getTxtError(error).c_str());
            msgCliError('I', "Error al obtener la fecha de vigencia final para la Revocacion", error = ERR_OBT_VFIN );
         }
         else if ( (error = FuncFecha->TransFecha(sFecRev, &sFecReg, false ) ) != 0 )
         {
            Bitacora->escribePV(BIT_ERROR, "Al evaluar la fecha local:%s, se encontr� el error: %s", sFecReg.c_str(),
                                FuncFecha->getTxtError(error).c_str());
            msgCliError('I', "Error al obtener la fecha de registro para la Revocacion", error = ERR_OBT_FREG );
         }
         else if( m_BDAC.ejecutaOper(QU_CERTIFICADO, m_numTram, m_secOp, sFecRev.c_str(), sFecReg.c_str(), m_rfcOp, m_numSerieRev) )
            m_FecRev = sFecRev; 
         else
         {
            msgCliError('N', "Error al revocar el Certificado Digital en la BD.", error = ERR_ACT_BD); 
            // OjO si es msgCliError( 'N' es mensaje de error => se guarda en la BD el msg CERTNOREVBD
            if (!regOperacionBDDet(atoi(m_secOp), CERTNOREVBD ))
               Bitacora->escribePV(BIT_ERROR, "Error al registrar en la BD la operacion a detalle del Mensaje (%i).", CERTNOREVBD);
         } 
         delete FuncFecha;
         FuncFecha = NULL;
      }
      else 
         msgCliError('I', "Error al obtener el estado actual del Certificado Digital de la BD ", error = ERR_EDO_CER );
   }
   else 
      msgCliError('I', "Error al consultar el estado del Certificado Digital.", error = ERR_EDO_CER );
  
   return !error;
}

void CRevocaSellosAC::iniciaVarsRev()
{
   m_ifirmaOp   = sizeof(m_firmaOp);
   m_irfcOp     = sizeof(m_rfcOp);
   m_isecOp     = sizeof(m_secOp);
   m_iagcCve    = sizeof(m_agcCve);
   m_inumTram   = sizeof(m_numTram);

   m_firmaOp[0] = 0;
   m_rfcOp[0]   = 0;
   m_secOp[0]   = 0;
   m_agcCve[0]  = 0;
   m_numTram[0] = 0;

   m_FecRev[0]  = 0;
   m_inumSerieRev   = sizeof(m_numSerieRev);

   m_numSerieRev[0] = 0;
   m_NumSerieOp = m_numSerieRev;
}

bool CRevocaSellosAC::rollBackRevocacion()
{
   if ( !m_BDAC.ejecutaOper(QU_CERT_ROLLBACK, m_vigFinTmp.c_str(), m_numOperTmp.c_str(),m_rfcOp, m_numSerieRev) )
      Bitacora->escribePV(BIT_ERROR, "Error(%d) en rollback del estado del Certificado Digital.", error = ERR_ROLLBACK );
   return !error;
   
}

