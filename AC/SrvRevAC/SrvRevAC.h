/*##############################################################################
  ###  PROYECTO:              PKI-SAT                                        ###
  ###  MODULO:                SrvRevAC  Servidor de Revocacion de la AC      ###
  ###                                                                        ###
  ###  DESARROLLADORES:                                                      ###
  ###                         Miguel Angel Mendoza L�pez      MAML           ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       Lunes 30, agosto del 2010                      ###
  ###                                                                        ###
  ##############################################################################
         1         2         3         4         5         6         7         8
12345678901234567890123456789012345678901234567890123456789012345678901234567890
*/
/*##############################################################################
   VERSION:  
      V.01.00   

#################################################################################*/

#ifndef _SRVREVAC_H_
   #define _SRVREVAC_H_
static const char* _SRVREVAC_H_VERSION_ ATR_USED = "@(#) SrvRevAC ( L : DSIC10391AC_ : SrvRevAC.h : 1.0.0 : 0 : 23/09/10)";

//#############################################################################



//#############################################################################
class CSrvRevAC : public CSrvUnx
{
      CConfigFile   Configuracion;
      MensajesSAT   MsgCli;
      CBD          *BDAC;
      int           error;

   protected:
      bool InicioDeSesion();
      bool conectaBD();
      void desconectaBD();
      bool procesaPassword(string s_pwdEnc, string *s_pwdDes);

      virtual bool Inicia();
      virtual bool SetParmsSktProc(CSSL_parms& parms);
      virtual bool Proceso();
      virtual bool Operaciones();
      virtual bool envOperInv();

   public:
      CSrvRevAC();
      virtual ~CSrvRevAC();
};
//#############################################################################

extern CSrvRevAC* ptrSrvRevAC;

//#############################################################################

#endif //_SRVREVAC_H_

