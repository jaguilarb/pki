/*##############################################################################
  ###  PROYECTO:              PKI-SAT                                        ###
  ###  MODULO:                Servicio Revocación de la AC                   ###
  ###                                                                        ###
  ###                                                                        ###
  ###  DESARROLLADORES:       Miguel Angel Mendoza MAML                      ###
  ###                                                                        ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       Lunes 30, agosto del 2010                      ###
  ###                                                                        ###
  ##############################################################################
         1         2         3         4         5         6         7         8
12345678901234567890123456789012345678901234567890123456789012345678901234567890

################################################################################
  VERSION:
     V.1.0.0 
                              
#################################################################################*/
#ifndef _COPERACIONREVAC_H_
   #define _COPERACIONREVAC_H_
static const char* _COPERACIONREVAC_H_VERSION_ ATR_USED = "@(#) SrvRevAC ( L : DSIC10391AC_ : COperacionRevAC.h : 1.0.0 : 0 : 23/09/10)";


#include <unistd.h>

#include <iostream>
#include <vector>

#include <Sgi_BD.h>
#include <Sgi_Fecha.h>

//################################################################################


#define ERR_NOT_DESENCRYPT_PWD      	107


/*##############################################################################
   CLASE    : COperacionRevAC
   PROPOSITO: Esta clase se utiliza como clase base para todas las clases que
              integran el servicio de Revocación de la AC de la PKI del SAT.

#################################################################################*/
class COperacionRevAC
{
      int error; 
   protected:
      char  m_firmaOp[685], m_rfcOp[14], m_secOp[10], m_agcCve[9], m_numTram[13];
      int   m_ifirmaOp, m_irfcOp, m_isecOp, m_iagcCve, m_inumTram; 

      string        m_NumSerieOp;

      CConfigFile   &m_Configuracion;
      MensajesSAT   &m_MsgCli;
      CSSL_Skt      *m_SktProc;
      CBD           &m_BDAC;

      int  envia();
      int  setMsgErr(const char *psz_desc, int i_error = ERRORREVAUT);
      bool regOperacionBD();
      bool regOperacionBDDet(int sec, int op);
      bool msgCliError(char c_tipErr, const char* psz_msg, int i_error );

      virtual bool preProceso() = 0; 
      virtual bool Proceso() = 0;
      virtual const char* getNombreOperacion() = 0;
      virtual int getTipOper() = 0;
      
   public:
      COperacionRevAC(CConfigFile& config, MensajesSAT& msgcli, CSSL_Skt* skt, CBD& BDAC );
      virtual ~COperacionRevAC();      
      virtual bool Operacion();
                
};

#endif //_COPERACIONREVAC_H_
