static const char* _CCONEXIONREVAC_CPP_VERSION_ ATR_USED = "@(#) SrvRevAC ( L : DSIC10391AC_ : CConexionRevAC.cpp : 1.0.0 : 0 : 23/09/10)";

//#VERSION: 1.0.0

#include <Sgi_SrvUnx.h>
#include <Sgi_MsgPKI.h>
#include <Sgi_ConfigFile.h>
#include <Sgi_ProtegePwd.h>
#include <COperacionRevAC.h>
#include <CConexionRevAC.h>

//int CConexionRevAC::m_MsgTipo = 0;

CConexionRevAC::CConexionRevAC(CConfigFile& config, MensajesSAT& msgcli, CSSL_Skt* skt, CBD& BDAC) :
COperacionRevAC(config, msgcli, skt, BDAC)
{
   error = 0;
   operacion = false;
}

CConexionRevAC::~CConexionRevAC()
{
}

int CConexionRevAC::ConectaARCli()
{
   error = getValoresMsg();
   if(!error)
  {
      error = sgiErrorBase(m_MsgCli.Inicia(AR, (char*)m_CertAC.c_str(), (char*)m_KeyAC.c_str(), (char*)m_PassAC.c_str(), m_PassAC.length(),
                         (char*)m_ARCert.c_str()));
      if(!error)
      {
         for(;;)
         {
            error = procesaMsg();
            if (error == TERMINAR)
               break;
            else if (error == 0)
               continue;
            else
            {
               error = m_MsgCli.setMensaje(DESCONEXION);
               if(!error)
               {
                  error = m_SktProc->Envia(m_MsgCli.buffer, m_MsgCli.tamMensaje());
                  if(!error)
                     break;
                  else 
                  {
                     Bitacora->escribePV(BIT_ERROR, "Error (%i): Al enviar a la AR.", error);
                     error = ERR_NO_ENVIA;
                     break; 
                  }
               }
               else 
               {
                  Bitacora->escribePV(BIT_ERROR, "Error (%i): Al enviar a la AR.", error);
                  break; 
               }
            }
         }
      }
      else
         Bitacora->escribePV(BIT_ERROR,  "Error (%i): Al enviar al iniciar Mensajes.", error);

   }
   else
      Bitacora->escribePV(BIT_ERROR,  "Error (%i): Al obtener las variables de configuracion.", error);

   return 0; // Para el caso de inicia Conexion, error == TERMINAR y es OK.
}

int CConexionRevAC::getValoresMsg()
{
   char *pwdDes = NULL;
   int i_pwdDes = 0;
   string claves[] = { "CA_PATH", "CA_KEY_PATH", "CA_PASS", "AR_CER" };

   if ( (error = m_Configuracion.getValoresConf("[LLAVES_MENSAJES_SRV]", 4, claves, &m_CertAC, &m_KeyAC, &m_PassAC, &m_ARCert ) ) )
      Bitacora->escribe(BIT_ERROR, "Error: no existen las variables en el arch. de configuraci�n([LLAVES_MENSAJES_SRV], CA_PATH - AR_CER).");
   else
   {
      i_pwdDes = m_PassAC.length();
      pwdDes = new char[i_pwdDes];
      if(!pwdDes)
         error = ERR_FALTA_MEMORIA;
      else
      {
         if ( desencripta((uint8*)m_PassAC.c_str(), m_PassAC.length(), (uint8*)pwdDes, &i_pwdDes) )
            m_PassAC.assign(pwdDes, i_pwdDes);
         else
            Bitacora->escribePV(BIT_ERROR, "Error(%d) al desencriptar el password de la llave de mensajes del servidor de Rev.", error = ERR_DESENCR); 
         delete[] pwdDes;
      }
   }
   return error;
}

int CConexionRevAC::procesaMsg()
{
   error = m_MsgCli.Recibe(m_SktProc);
   if(!error)
   {
      switch(m_MsgCli.tipOperacion())
      {
         case SOLCONEXION:
         {
            error = GenSesionNo(128, m_NoSesion, &m_iNoSesion);
            if(error != 0)
            {
               Bitacora->escribePV(BIT_ERROR, "Error (%i): Al generar el numero de sesi�n.", error);
               error = ERR_NO_GENSESNO;
            }
            else
            {
               error = sgiErrorBase(m_MsgCli.setMensaje(PRUEBAID, m_NoSesion, m_iNoSesion));
               if(error)
                  Bitacora->escribePV(BIT_ERROR, "Error (%i): Al preparar el mensaje.(PRUEBAID)", error);
            }  
            break;
         }
         case CONFIDENT:
         {
            char psz_NoSesion[40];
            int  i_NoSesion = sizeof(psz_NoSesion);
            error = sgiErrorBase(m_MsgCli.getMensaje(CONFIDENT, m_firma, &m_ifirma, psz_NoSesion,
                                 &i_NoSesion));
            if(!error)
            {
               error = strncmp(m_NoSesion, psz_NoSesion, i_NoSesion);
               if(error == 0)
               {
                  error = sgiErrorBase(m_MsgCli.setMensaje(CONEXION));
                  if(error)
                     Bitacora->escribePV(BIT_ERROR, "Error (%i): Al preparar el mensaje.(CONEXION)", error);
               }
               else
                  Bitacora->escribePV(BIT_ERROR, "Error (%i): El n�mero recibido de la AR es distinto.", error);
            }
            else
               Bitacora->escribePV(BIT_ERROR, "Error (%i): Al obtener el mensaje.(CONFIDENT)", error);
            break;
         }
         case IDENAR:
         {
            char psz_numAR[5];
            int i_numAR = sizeof(psz_numAR);
            error = sgiErrorBase(m_MsgCli.getMensaje(IDENAR, m_firma, &m_ifirma, psz_numAR, &i_numAR));
            if(!error)
            {
               if(psz_numAR[0] == '1')
                  operacion = true;
               else 
                  operacion = false;
               error = m_MsgCli.setMensaje(CONEXIONUSU);
               if(error)
                  Bitacora->escribePV(BIT_ERROR, "Error (%i): Al preparar el mensaje.(CONEXIONUSU)", error);
            }
            else 
               Bitacora->escribePV(BIT_ERROR, "Error (%i): Al obtener el mensaje.(IDENAR)", error);
            break;
         }
         default:
            Bitacora->escribePV(BIT_ERROR, "Error (%i): Mensaje no esperado. (%d)", ERR_MSG_ERROR, 
                                error = m_MsgCli.tipOperacion());
      }
   }
   else
      Bitacora->escribePV(BIT_ERROR, "Error(%d) al recibir del socket de comunicacion.", error);
   if ( !error )
   {
      error = m_SktProc->Envia(m_MsgCli.buffer, m_MsgCli.tamMensaje());
      if(!error && m_MsgCli.tipOperacion() == CONEXIONUSU)
         error = TERMINAR;
   }
   return error;
}

int CConexionRevAC::GenSesionNo(int bits, char *sesionNo, int *res)
{
   SGIRSA rsa_alg;
   return sgiErrorBase(rsa_alg.GenAleatorio(bits, sesionNo, res));
}

bool CConexionRevAC::Proceso()
{
   error = ConectaARCli();
   if(!error)
      Bitacora->escribe(BIT_INFO, "Sesion Iniciada con la Revocaci�n de la AR.");
   else 
      Bitacora->escribePV(BIT_ERROR, "Error (%i). En la conexi�n con la Revocaci�n de la  AR.", error);

   return !error;
}

const char* CConexionRevAC::getNombreOperacion()
{
   return "Conexion con la Revocaci�n de la AR.";
}

int CConexionRevAC::getTipOper()
{
   return SOLCONEXION;
}
