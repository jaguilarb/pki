static const char* _CCONEXIONAC_CPP_VERSION_ ATR_USED = "SrvAC @(#)"\
"DSIC09041AC_ 2007-12-13 CConexionAC.cpp 1.1.0/1";

//#VERSION: 1.1.0

#include <DefsGeneracion.h>
#include <CConexionAC.h>

int CConexionAC::m_MsgTipo = 0;

CConexionAC::CConexionAC(CConfigFile& config, MensajesSAT& msgcli, CSSL_Skt* skt) :
COperacionAC(config, msgcli, skt)
{
   operacion = false;
}

CConexionAC::~CConexionAC()
{
}

int CConexionAC::ConectaARCli()
{
   int error = 0;
   error = getValoresMsg();
   if(!error)
   {
      error = sgiErrorBase(m_MsgCli.Inicia(AR, (char*)m_CertAC.c_str(), (char*)m_KeyAC.c_str(), (char*)m_PassAC.c_str(), m_PassAC.length(),
                         (char*)m_ARCert.c_str()));
      if(!error)
      {
         for(;;)
         {
            error = procesaMsg();
            if (error == TERMINAR)
               break;
            else if (error == 0)
               continue;
            else
            {
               error = m_MsgCli.setMensaje(DESCONEXION);
               if(!error)
               {
                  error = m_SktProc->Envia(m_MsgCli.buffer, m_MsgCli.tamMensaje());
                  if(!error)
                     break;
                  else 
                  {
                     Bitacora->escribePV(BIT_ERROR, "Error en CConexionAC en ConectaARCli: Error (%i): Al enviar a la AR.", error);
                     return ERR_NO_ENVIA;
                  }
               }
               else 
               {
                  Bitacora->escribePV(BIT_ERROR, "Error en CConexionAC en ConectaARCli: Error (%i): Al enviar a la AR.", error);
                  return error;
               }
            }
         }
      }
      else
      {
         Bitacora->escribePV(BIT_ERROR,  "Error en CConexionAC en ConectaARCli: Error (%i): Al enviar al iniciar Mensajes.", error);
         return error;
      }
   }
   else
   {
      Bitacora->escribePV(BIT_ERROR,  "Error en CConexionAC en ConectaARCli: Error (%i): Al obtener las variables de configuracion.", error);
      return error;
   }
   return 0;
}

int CConexionAC::getValoresMsg()
{
   char *pwdDes = NULL;
   int i_pwdDes = 0;
   int error = 0;
   error = m_Configuracion.getValorVar(TAG_OP, "CA_PATH", &m_CertAC);
   if(error)
      return error;
   error = m_Configuracion.getValorVar(TAG_OP, "CA_KEY_PATH", &m_KeyAC);
   if(error)
      return error;
   error = m_Configuracion.getValorVar(TAG_OP, "CA_PASS", &m_PassAC);
   if(error)
      return error;
   error = m_Configuracion.getValorVar(TAG_OP, "AR_CER", &m_ARCert);
   if(error)
      return error;
   i_pwdDes = m_PassAC.length();
   pwdDes = new char[i_pwdDes];
   if(!pwdDes)
      return ERR_ASG_MEM;
   bool ok = desencripta((uint8*)m_PassAC.c_str(), m_PassAC.length(), (uint8*)pwdDes, &i_pwdDes);
   if(!ok)
      delete[] pwdDes;
   else m_PassAC.assign(pwdDes, i_pwdDes);
   delete[] pwdDes;
   return error;
}

int CConexionAC::procesaMsg()
{
   //>- int i_buffMens = TAM_DATOS; //>- ERGL mi� sep 12 13:40:43 CDT 2007
   int error = 0;
   //>- error = m_SktProc->Recibe(m_MsgCli.buffer, i_buffMens); //>- ERGL mi� sep 12 13:14:11 CDT 2007
   error = m_MsgCli.Recibe(m_SktProc);
   if(!error)
   {
      switch(m_MsgCli.tipOperacion())
      {
         case SOLCONEXION:
         {
            error = GenSesionNo(128, m_NoSesion, &m_iNoSesion);
            if(error != 0)
            {
               Bitacora->escribePV(BIT_ERROR, "Error en CConexionAC en procesaMsg: Error (%i): Al generar el numero de sesi�n.", error);
               return ERR_NO_GENSESNO;
            }
            error = sgiErrorBase(m_MsgCli.setMensaje(PRUEBAID, m_NoSesion, m_iNoSesion));
            if(!error)
               break;           
            else
            {
               Bitacora->escribePV(BIT_ERROR, "Error en CConexionAC en procesaMsg: Error (%i): Al preparar el mensaje.(PRUEBAID)", error);
               return error;
            } 
         }
         case CONFIDENT:
         {
            char psz_NoSesion[40];
            int  i_NoSesion = sizeof(psz_NoSesion);
            error = sgiErrorBase(m_MsgCli.getMensaje(CONFIDENT, m_firma, &m_ifirma, psz_NoSesion,
                                 &i_NoSesion));
            if(!error)
            {
               int respuesta = strncmp(m_NoSesion, psz_NoSesion, i_NoSesion);
               if(respuesta == 0)
               {
                  error = sgiErrorBase(m_MsgCli.setMensaje(CONEXION));
                  if(!error)
                     break;
                  else
                  {
                     Bitacora->escribePV(BIT_ERROR, "Error en CConexionAC en procesaMsg: Error (%i): Al preparar el mensaje.(CONEXION)", error);
                     return error;
                  }
               }
               else
               {
                  Bitacora->escribePV(BIT_ERROR, "Error en CConexionAC en procesaMsg: Error (%i): El n�mero recibido de la AR es distinto.", error);
                  return error;
               }               
            }
            else
            {
               Bitacora->escribePV(BIT_ERROR, "Error en CConexionAC en procesaMsg: Error (%i): Al obtener el mensaje.(CONFIDENT)", error);
               return error;
            }
            break;
         }
         case IDENAR:
         {
            char psz_numAR[5];
            int i_numAR = sizeof(psz_numAR);
            error = sgiErrorBase(m_MsgCli.getMensaje(IDENAR, m_firma, &m_ifirma, psz_numAR, &i_numAR));
            if(!error)
            {
               if(psz_numAR[0] == '1')
                  operacion = true;
               else operacion = false;
               error = m_MsgCli.setMensaje(CONEXIONUSU);
               if(!error)
                 break;
               else 
               {
                  Bitacora->escribePV(BIT_ERROR, "Error en CConexionAC en procesaMsg: Error (%i): Al preparar el mensaje.(CONEXIONUSU)", error);
                  return error;
               }
            }
            else 
            {
               Bitacora->escribePV(BIT_ERROR, "Error en CConexionAC en procesaMsg: Error (%i): Al obtener el mensaje.(IDENAR)", error);
               return error;
            }
            break;
         }
         default:
         {
            Bitacora->escribePV(BIT_ERROR, "Error en CConexionAC en procesaMsg: Error (%i): Mensaje no esperado. (%d)", ERR_MSG_ERROR, 
                     m_MsgCli.tipOperacion());
            return error;
         }
      }
   }
   error = m_SktProc->Envia(m_MsgCli.buffer, m_MsgCli.tamMensaje());
   if(!error && m_MsgCli.tipOperacion() == CONEXIONUSU)
      return TERMINAR;
   else if (!error)
      return 0;
   else return error;
}

int CConexionAC::GenSesionNo(int bits, char *sesionNo, int *res)
{
   SGIRSA rsa_alg;
   return sgiErrorBase(rsa_alg.GenAleatorio(bits, sesionNo, res));
}

bool CConexionAC::preProceso()
{
   return true;
}

bool CConexionAC::Proceso()
{
   int error = 0;
   error = ConectaARCli();
   if(!error)
   {
      Bitacora->escribe(BIT_INFO, "CConexionAC.Proceso: Sesion Iniciada con la AR.");
      return true;
   } 
   else 
   {
      Bitacora->escribePV(BIT_ERROR, "Error en CConexionAC en Proceso: Error (%i). En la conexi�n con la AR.", error);
      return false;
   }
}

const char* CConexionAC::getNombreOperacion()
{
   return "Conexion con la AR.";
}

int CConexionAC::getTipOper()
{
   return SOLCONEXION;
}
