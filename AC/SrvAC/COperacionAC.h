/*##############################################################################
  ###  PROYECTO:              PKI-SAT                                        ###
  ###  MODULO:                Servicio AR                                    ###
  ###                                                                        ###
  ###                                                                        ###
  ###  DESARROLLADORES:       Amilcar Guerrero Zu�iga         AGZ            ###
  ###                                                                        ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       Miercoles 30, noviembre del 2005               ###
  ###                                                                        ###
  ##############################################################################
         1         2         3         4         5         6         7         8
12345678901234567890123456789012345678901234567890123456789012345678901234567890

################################################################################
  VERSION:
     V.1.0.0  (20051130- )    AGZ: Version Original
                              Servicio de Autoridad Registradora para la PKI-SAT

      V.3.0.3 SrvAC
              (20100826-20100903) GHM:Se realizan las modificaciones para que se asigne la vigencia de 48 meses a FIEL PF
                              y 27 meses a FIEL de PM
                              
#################################################################################*/
#ifndef _COPERACIONAC_H_
#define _COPERACIONAC_H_
static const char* _COPERACIONAC_H_VERSION_ ATR_USED = "SrvAC @(#)"\
"DSIC10352AC_ 2010-09-03 COperacionAC.h 1.1.0/2";

//#VERSION: 1.1.0

#include <unistd.h>

#include <iostream>
#include <vector>

#include <Sgi_BD.h>
#include <Sgi_Fecha.h>


//+ GNC  Se agrega definici�n de consultas
#if ORACLE
  //
#else
  #include <mitypes.h>
  #include <it.h>
#endif

//################################################################################

#define MAX_TAM_FIRMA_B64              684
#define MAX_TAM_RFC                     14
#define MAX_TAM_CADORI               32760
#define MAX_TAM_PATH                   256
#define MAX_TAM_SEC_AR                  10	

#define CIERRA_CLIENTE                -500

#define ERR_NO_ASG_MEM_OBJ_MEN         100
#define ERR_NO_ASG_MEM_OBJ_FIFO        101
#define ERR_NOT_CONN_BD_DARIO	         102
#define ERR_NOT_CONN_BD_AR             103
#define ERR_NOT_ROLE_BD_AR             104
#define ERR_NO_CREO_FIFO            	105
#define ERR_NO_ESCRIBE_FIFO            106
#define ERR_NOT_DESENCRYPT_PWD         107
#define ERR_ID_BD_NOT_EXIST            108
#define ERR_NO_SET_DATOS_MSG           109
#define ERR_NO_ENVIA_SKT               110
#define ERR_OPC_DEST_NO_RECON          111
#define ERR_NO_RECIBE_SKT              112
#define ERR_OPC_ORIGEN_NO_RECON        113
#define ERR_FIFO_MED_INI               114
#define ERR_ASG_MEM_VAR                115
#define ERR_CREA_ERROR                 116
#define ERR_BD_REG_OPER_SEG            117
#define ERR_NO_ENVIA_CSAT              118
#define ERR_NO_INICIA_VARS_MED         119
#define ERR_EDO_AGC                    120
#define ERR_EDO_CERT_AGC               121

/*##############################################################################
   CLASE    : COperacion
   PROPOSITO: Esta clase se utiliza como clase base para todas las clases que
              integran el servicio de AR de la PKI del SAT.

#################################################################################*/
class COperacionAC
{
   private:
      int getVarConBD(string* s_srv, string* s_db, string* s_usr, string* s_pswd, string* s_role);
   protected:
      //<<<AGZ: Variables para la generacion del numero de tramites.
      //<<<     cada tramite debe establecerlas si las necesita y liberarlas.
      char  m_firmaOp[685], m_rfcOp[14], m_secOp[10], m_agcCve[10], m_numTram[13];
      int   m_ifirmaOp, m_irfcOp, m_isecOp, m_iagcCve, m_inumTram; 
      //>>>AGZ: Fin

      CBD          *m_BDAC;
      string        m_NumSerieOp;

      CConfigFile   &m_Configuracion;
      MensajesSAT   &m_MsgCli;
      CSSL_Skt      *m_SktProc;

      intE escribeBitacora(int i_apl, int i_cdgErr, const char *psz_descripcion);
      int  iniciaMed(char *psz_cerDestino, char *psz_cerOrigen, char *psz_keyOrigen, char *psz_passOrigen);
      void terminaMed(void);
      bool conectaBD();      
      void desconectaBD();
      int  envia();
      int  recibe();
      int  getMsgErr(char& psz_tipError, char *psz_cdgErr, int i_cdgErr, char *psz_desc, int i_desc);
      int  setMsgErr(char psz_tipError, const char *psz_cdgErr, const char *psz_desc);
      bool regOperacionBD();
      bool regOperacionBDDet(int sec, int op);
      bool procesaPassword(string s_pwdEnc, string *s_pwdDes);
      void iniciaVarsTram(void);
      void terminaVarsTram(void);
      bool msgCliError(char c_tipErr, const char* psz_msg, int i_error, eIdLib i_apl);
      int  iniMed();

      virtual bool preProceso() = 0; 
      virtual bool Proceso() = 0;
      virtual const char* getNombreOperacion() = 0;
      virtual int getTipOper() = 0;
      
      //Para los errores en la Bitacora con nombre de la clase o sin nombre.
      const char* SeparaNombreClase(const char *QuitarClase, int NumeroQuitarClase);
      
   public:
      COperacionAC(CConfigFile& config, MensajesSAT& msgcli, CSSL_Skt* skt);
      virtual ~COperacionAC();      
      virtual bool Operacion();
                
      //- GHM (20100826): V3.0.3 bool    getVigencias(unsigned long vigencia[] ); //<<<--- MAML 081119
      bool    getVigencias(map<int, int> &mapVig); //+ GHM (20100826): V3.0.3
      int     getVarConfVig(const char* etiqueta, const char* variable, int* valor); 
};

#endif //_COPERACIONAC_H_
