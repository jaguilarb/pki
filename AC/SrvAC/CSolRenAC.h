#ifndef _CSOLRENAC_H_
#define _CSOLRENAC_H_
static const char* _CSOLRENAC_H_VERSION_ ATR_USED = "SrvAC @(#)"\
"DSIC09041AC_ 2007-12-13 CSolRenAC.h 1.1.0/1";

//#VERSION: 1.1.0


class CSolRenAC : public COperacionAC
{
   private:
      static int m_MsgRen;
      char  m_cadenaSol[512], m_tipoCer[2], m_SerialGen[21];
      uchar m_byBufferReq[1024*3], m_byBufferCert[1024*10];
      char  m_nomCont[1024*2];
      int   m_icadenaSol, m_itipoCer, m_ibuffReq, m_ibyBufferCert, m_ibyBufferReq;
      int   m_inomCont;
      string     m_pathCompleto, vigI, vigF, s_FechRev, s_NumSerRev;
      DATA_CERT  *data;

   protected:
      
      bool  preProceso();
      bool  Proceso();
      const char* getNombreOperacion();
      int   getTipOper();
      int   generaCertificado();
      int   procesaMsg();
      bool  procesaCadenaRFC();
      bool  tieneCertActivo();
      bool  generaSerial();
      int   registraCertificado();
      int   creaArchCert();
      void  armaFechaBD(char *vigIni, char *vigFin);
      int   revocaCertificado();
      void  iniciaVarsRen();
      void  procesaDatoRL(char *cadOrig, char *Cad1, char *Cad2);
      bool  regresaSerial();
      bool  obtieneCorreo(char* correo);

   public:

      CSolRenAC(CConfigFile& config, MensajesSAT& msgcli, CSSL_Skt* skt, DATA_CERT *dat );
      ~CSolRenAC();
};

#endif //_CSOLRENAC_H_
