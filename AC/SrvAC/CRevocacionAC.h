#ifndef _CREVOCACIONAC_H_
#define _CREVOCACIONAC_H_
static const char* _CREVOCACIONAC_H_VERSION_ ATR_USED = "SrvAC @(#)"\
"DSIC09041AC_ 2007-12-13 CRevocacionAC.h 1.1.0/1";

//#VERSION: 1.1.0


#define ERR_SEP_CAD	100
#define ERR_CVE_REV	101
#define ERR_EDO_CER	102
#define ERR_OBT_CVE_REV	103
#define ERR_OBT_RFC	104
#define ERR_REG_OPER	105
#define ERR_SET_MSG	106
#define ERR_OBT_VFIN	107
#define ERR_OBT_FREG    108

class CRevocacionAC : public COperacionAC
{
   private:
      static int m_MsgRev;
      char m_cadenaRFC[100], m_numSerieRev[21], m_cveRev[35];
      int  m_icadenaRFC, m_inumSerieRev, m_icveRev;
      string m_vigFinTmp, m_numOperTmp,m_FecRev;      
          
   protected:
      bool preProceso();
      bool Proceso();
      const char* getNombreOperacion();
      int getTipOper();
      void limpiaVars();

      bool procesaCadena();
      bool revocaCert();
      bool cmpCveRev();
      void iniciaVarsRev();
      bool rollBackRevocacion();
      int  GetCurrentUTC( string *s_FechaUTC);

   public:
      CRevocacionAC(CConfigFile& config, MensajesSAT& msgcli, CSSL_Skt* skt);
      ~CRevocacionAC();
      
};

#endif //_CREVOCACIONAC_H_
