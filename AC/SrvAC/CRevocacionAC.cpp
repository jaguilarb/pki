static const char* _CREVOCACIONAC_CPP_VERSION_ ATR_USED = "SrvAC @(#)"\
"DSIC09041AC_ 2007-12-13 CRevocacionAC.cpp 1.1.1/1";

//#VERSION: 1.1.1
//V.1.1.1.1 (080421) RORS: Se cambia este mensaje para mandar la fecha de revocacion  a la AR.
#include <Sgi_Fecha.h>     //Librer�a de Fechas
#include <DefsGeneracion.h>
#include <CRevocacionAC.h>

//##################################################################
//+ GNC  Se agrega definici�n de consultas
#if ORACLE
   #include <CosltORASrvAC.h> 
#else
  #include <CosltSrvAC.h> 
#endif
//##################################################################


int CRevocacionAC::m_MsgRev = -1;

CRevocacionAC::CRevocacionAC(CConfigFile& config, MensajesSAT& msgcli, CSSL_Skt* skt) :
COperacionAC(config, msgcli, skt)
{
}

CRevocacionAC::~CRevocacionAC()
{
   m_MsgRev = -1;
}

bool CRevocacionAC::preProceso()
{
   bool ok = false;
   int error = 0;
   Bitacora->escribePV(BIT_INFO, "    CRevocacionAC.preProceso: Inicio del Preproceso para %s.",getNombreOperacion());
   
   if (m_MsgRev == -1)
      m_MsgRev = m_MsgCli.tipOperacion();
 
   iniciaVarsRev();
   Bitacora->escribe(BIT_INFO, "CRevocacionAC.preProceso: Se iniciaron las variables para la solicitud.");
   
   switch(m_MsgRev)
   {
     case SOLREVFAR:
     {
        error = sgiErrorBase(m_MsgCli.getMensaje(SOLREVFAR, m_firmaOp, &m_ifirmaOp, m_numTram, &m_inumTram,
                   m_secOp, &m_isecOp, m_cadenaRFC, &m_icadenaRFC, m_numSerieRev, &m_inumSerieRev));
        break;

     }
     case SOLREVAR:
     {
        error = sgiErrorBase(m_MsgCli.getMensaje(SOLREVAR, m_firmaOp, &m_ifirmaOp, m_numTram, &m_inumTram, 
                   m_secOp, &m_isecOp, m_cadenaRFC, &m_icadenaRFC, m_numSerieRev, &m_inumSerieRev, m_cveRev, &m_icveRev));
        break;
     } 
     case SOLREVOFAR:
     {
        error = sgiErrorBase(m_MsgCli.getMensaje(SOLREVOFAR, m_firmaOp, &m_ifirmaOp, m_numTram, &m_inumTram,
                   m_secOp, &m_isecOp, m_cadenaRFC, &m_icadenaRFC, m_numSerieRev, &m_inumSerieRev));
        break;
     }
   }
   
   if(!error)
   {
      Bitacora->escribe(BIT_INFO, "CRevocacionAC.preProceso: Se obtuvo el mensaje para la revocaci�n");
	  
      if (procesaCadena())
      {
	     Bitacora->escribe(BIT_INFO, "CRevocacionAC.preProceso: Se obtuvieron los datos:'RFC y AGC'");
         m_NumSerieOp.assign(m_numSerieRev, m_inumSerieRev);
         ok = true;
      }
      else
      {
          Bitacora->escribePV(BIT_ERROR, "Error en CRevocacionAC en preProceso: Error (%i) Al separar la cadena.", ERR_SEP_CAD);
          msgCliError('I', "Error en CRevocacionAC en preProceso: Error al obtener la clave de agente y rfc.", ERR_SEP_CAD, eAPL);
          ok = false;
      }
   }
   else 
   {
      Bitacora->escribePV(BIT_ERROR, "Error en CRevocacionAC en preProceso: Error (%i) Al procesar el Mensaje (%i).", error, m_MsgCli.tipOperacion());
      msgCliError('I', "Error en CRevocacionAC en preProceso: Error al procesar el mensaje.", ERR_SEP_CAD, eAPL);
      ok = false;
   }   
   
   Bitacora->escribePV(BIT_INFO, "    CRevocacionAC.preProceso: Fin del Preproceso para %s.",getNombreOperacion());
   return ok;
}

bool CRevocacionAC::Proceso()
{
   bool ok = false;
   int error = 0;
   
   Bitacora->escribePV(BIT_INFO, "    CRevocacionAC.Proceso: Inicio del Proceso para %s.",getNombreOperacion()); 
   
   if(!regOperacionBD())
   {
      msgCliError('I', "Error en CRevocacionAC en Proceso: Error al insertar la solicitud en la BD.", ERR_REG_OPER, eAPL);
      return false;
   }
   switch(m_MsgRev)
   {
      case SOLREVFAR:
      case SOLREVAR:
      {
         if (m_MsgRev == SOLREVAR)
           cmpCveRev();
         
         if (revocaCert())
         {
            Bitacora->escribePV(BIT_INFO, "CRevocacionAC.Proceso: Certificado: %s revocado correctamente.", m_numSerieRev);
            Bitacora->escribePV(BIT_DEBUG, "CRevocacionAC.Proceso: N�mero de tr�mite: %s ; tama�o: %i", m_numTram, m_inumTram);
            
            //  RORS  lun abr 21 17:12:02 CDT 2008  Se cambia este mensaje para mandar la fecha de revocacion  a la AR.

            error = sgiErrorBase(m_MsgCli.setMensaje(CERTREVARAC, m_numTram, m_inumTram,m_FecRev.c_str(),strlen(m_FecRev.c_str())) );
             if (!error)
            {
               if (!regOperacionBDDet(atoi(m_secOp), CERTREVARAC))
                  Bitacora->escribePV(BIT_ERROR, "Error en CRevocacionAC en Proceso: Error al registrar la operacion a detalle del Mensaje (%i).", SOLREVAR);
			   else
               Bitacora->escribe(BIT_INFO, "CRevocacionAC.Proceso: Se registro el detalle de la operacion en la BD.");  
				  
               error = envia();
               if(!error)
                 { 
				    Bitacora->escribe(BIT_INFO, "CRevocacionAC.Proceso: Se envi� el mensaje a la AR (Mediador)");
				    ok = true;
				 }
               else
               {
                  Bitacora->escribePV(BIT_ERROR, "Error en CRevocacionAC en Proceso: Error (%i) Al enviar mensaje por el socket a la AR.", error);
                  rollBackRevocacion();
               }
            }
            else 
            {
               msgCliError('I', "Error en CRevocacionAC en Proceso: Error al preparar el mensaje.(CERTREVARAC)", ERR_SET_MSG, eAPL);
               Bitacora->escribePV(BIT_DEBUG, "CRevocacionAC.Proceso: N�mero de tr�mite: %s ; tama�o: %i l�nea: %i", m_numTram, m_inumTram, __LINE__);
               Bitacora->escribePV(BIT_ERROR, "Error en CRevocacionAC en Proceso: Error (%i) Al preparar el mensaje.(CERTREVARAC)", error);
               rollBackRevocacion();
            }
         }
         else 
            Bitacora->escribe(BIT_ERROR, "Error en CRevocacionAC en Proceso: Error Al revocar el Certificado en la BD.");
         break;
      }
      case SOLREVOFAR:
      {
         if (revocaCert())
         {

           //  RORS  lun abr 21 17:12:02 CDT 2008  Se cambia este mensaje para mandar la fecha de revocacion  a la AR.
            error = sgiErrorBase(m_MsgCli.setMensaje(CERTREVARAC, m_numTram, m_inumTram,m_FecRev.c_str(),strlen(m_FecRev.c_str())) );
            //error = sgiErrorBase(m_MsgCli.setMensaje(CERTREVAR, m_numTram, m_inumTram));
            if (!error)
            {
               Bitacora->escribePV(BIT_INFO, "CRevocacionAC.Proceso: Certificado: %s revocado correctamente.", m_numSerieRev);
               if (!regOperacionBDDet(atoi(m_secOp), CERTREVARAC))
                  Bitacora->escribePV(BIT_ERROR, "Error en CRevocacionAC en Proceso: Error al registrar la operacion a detalle del Mensaje (%i).", SOLREVAR);
				else
				  Bitacora->escribe(BIT_INFO, "CRevocacionAC.Proceso: Se registro el detalle de la operacion en la BD."); 
               error = envia();
               if(!error)
                 { 
				    Bitacora->escribe(BIT_INFO, "CRevocacionAC.Proceso: Se envi� el mensaje a la AR (Mediador)");
				    ok = true;
				 }
               else
               {
                  Bitacora->escribePV(BIT_ERROR, "Error en CRevocacionAC en Proceso: Error (%i) Al enviar mensaje por el socket a la AR.", error);
                  rollBackRevocacion();
               }
            }
            else 
            {
               Bitacora->escribePV(BIT_ERROR, "Error en CRevocacionAC en Proceso: Error (%i) Al preparar el mensaje.(CERTREVARAC)", error);
               rollBackRevocacion();
            }
         }
         else 
            Bitacora->escribe(BIT_ERROR, "Error en CRevocacionAC en Proceso: Error Al revocar el Certificado en la BD.");
         break;
      }
   }
   
   Bitacora->escribePV(BIT_INFO, "    CRevocacionAC.Proceso: Fin del Proceso para %s.",getNombreOperacion());
   return ok;
}

const char* CRevocacionAC::getNombreOperacion()
{
   if (m_MsgRev == -1)
      m_MsgRev = m_MsgCli.tipOperacion();

   if (m_MsgRev == SOLREVAR)
      return "Revocaci�n por clave de revocaci�n.";
   else if (m_MsgRev == SOLREVOFAR)
      return "Revocaci�n por oficio.";
   else return "Operaci�n no reconocida.";
}

int CRevocacionAC::getTipOper()
{
   if (m_MsgRev == -1)
      m_MsgRev = m_MsgCli.tipOperacion();
   return m_MsgRev;
}

bool CRevocacionAC::cmpCveRev()
{
   string s_Estado, s_CveRevTmp;
   bool ok = false;
   ok = m_BDAC->consultaReg(QS_CVEREV_CERT,m_rfcOp, m_numSerieRev);
   if (ok)
   {
      Bitacora->escribe(BIT_INFO, "CRevocacionAC.cmpCveRev: Se consulto la clave de revocaci�n.");
      ok = m_BDAC->getValores("s", &s_CveRevTmp);
      if (ok)
      {
	     Bitacora->escribe(BIT_INFO, "CRevocacionAC.cmpCveRev: Se obtuvo la clave de revocaci�n.");
         if (strncmp(s_CveRevTmp.c_str(), m_cveRev, m_icveRev) == 0)
            {
			   ok = true;
			   Bitacora->escribe(BIT_INFO, "CRevocacionAC.cmpCveRev: La clave de revocaci�n coincide con la almacenada en la BD.");
			}
         else 
          {
             msgCliError('N', "Error en CRevocacionAC en cmpCveRev: Error la clave de revocaci�n no coincide con la almacenada en la Base de Datos.", ERR_CVE_REV, 
             eAPL);
            ok = false;
         }
      }
      else 
      {
         msgCliError('I', "Error en CRevocacionAC en cmpCveRev: Error al obtener la clave de revocaci�n de la BD.", ERR_OBT_CVE_REV, eAPL);
         ok = false;
      }
   }
   else 
   {
      msgCliError('I', "Error en CRevocacionAC en cmpCveRev: Error al consultar la clave de revocaci�n de la BD.", ERR_OBT_CVE_REV, eAPL);
      ok = false;
   }
   return ok;
}

bool CRevocacionAC::revocaCert()
{
   bool   ok = true;
   string s_Estado, sFecRev, sFecReg;
   int    errorFecha = 0;
    
   Bitacora->escribePV(BIT_INFO, "CRevocacionAC.revocaCert: Inicio de Revocaci�n");
   
   Bitacora->escribePV(BIT_INFO, "CRevocacionAC.revocaCert: Certificado a revocar: %s con RFC: %s", m_numSerieRev, m_rfcOp);

   ok = m_BDAC->consultaReg(QS_EDO_VIG_SOLOPCERT,m_rfcOp,  m_numSerieRev);
   if (ok)
   {
      Bitacora->escribePV(BIT_INFO, "CRevocacionAC.revocaCert: Consulta EDO,VIGFIN,NUMOPER certificado");
	  
      ok = m_BDAC->getValores("sss", &s_Estado, &m_vigFinTmp, &m_numOperTmp);
      if (ok)
      {
	    Bitacora->escribePV(BIT_INFO, "CRevocacionAC.revocaCert: Se obtuvo EDO,VIGFIN,NUMOPER del certificado");
         //>>>+ 02.00.01 (070219) GHM: Objeto para utilizar las funciones de fecha
         CFecha* FuncFecha = new CFecha();
         
         if ( (errorFecha = FuncFecha->GetCurrentUTC(&sFecRev)) != 0)
         {
            Bitacora->escribePV(BIT_ERROR, "Error en CRevocacionAC en revocaCert: Error al evaluar la fecha UTC:%s, se encontr� el error: %s", sFecRev.c_str(),
                                FuncFecha->getTxtError(errorFecha).c_str());
            msgCliError('I', "Error en CRevocacionAC en revocaCert: Error al obtener la fecha de vigencia final para la Revocaci�n", ERR_OBT_VFIN, eAPL);
            ok = false;
         }

         // MAML 070726 : Se modifica el campo 'fec_reg' de la tabla certificado con la hora local de la fecha de Revocacion
         else if ( (errorFecha = FuncFecha->TransFecha(sFecRev, &sFecReg, false ) ) != 0 )
         {
            Bitacora->escribePV(BIT_ERROR, "Error en CRevocacionAC en revocaCert: Error al obtener la fecha de registro para la Revocacion", ERR_OBT_FREG, eAPL);
            msgCliError('I', "Error en CRevocacionAC en revocaCert: Error al obtener la fecha de registro para la Revocacion", ERR_OBT_FREG, eAPL);
            ok = false;
         }
         else if (s_Estado == "A")
         {

           ok = m_BDAC->ejecutaOper(QU_CERT_REVO, m_numTram, m_secOp, sFecRev.c_str(), sFecReg.c_str(), m_rfcOp, m_numSerieRev);   
          
           if(!ok)
               Bitacora->escribe(BIT_ERROR, "Error en CRevocacionAC en revocaCert: Error al revocar el Certificado Digital en la BD."); 
			else
			   Bitacora->escribePV(BIT_INFO, "CRevocacionAC.revocaCert: Se revoco el certificado");
         }
         else 
         {
		    Bitacora->escribe(BIT_ERROR, "Error en CRevocacionAC en revocaCert: Error el Certificado Digital fue revocado previamente o no se encuentra" 
		    "activo."); 
            msgCliError('N', "Error en CRevocacionAC en revocaCert: Error el Certificado Digital fue revocado previamente o no se "
                          "encuentra activo.", ERR_EDO_CER, eAPL);
            ok = false;
         }
      }
      else 
      {
	     Bitacora->escribe(BIT_ERROR, "Error en CRevocacionAC en revocaCert: Error al obtener el estado actual del Certificado Digital de la BD");             
         msgCliError('I', "Error en CRevocacionAC en revocaCert: Error al obtener el estado actual del Certificado Digital de la BD ", ERR_EDO_CER, eAPL);
         ok = false;
      }
   }
   else 
   {
      Bitacora->escribe(BIT_ERROR, "Error en CRevocacionAC en revocaCert: Error al consultar el estado del Certificado Digital."); 
      msgCliError('I', "Error en CRevocacionAC en revocaCert: Error al consultar el estado del Certificado Digital.", ERR_EDO_CER, eAPL);
      ok = false;
   }
   // RORS  lun abr 21 17:09:54 CDT 2008   Para enviar la misma fecha revocacion que se inserta en la bd AC  a la AR.
   m_FecRev = sFecRev;
   
   Bitacora->escribePV(BIT_INFO, "CRevocacionAC.revocaCert: Fin de Revocaci�n");
   return ok;
}

bool CRevocacionAC::procesaCadena()
{
   char *rfc = NULL, *agc = NULL;
   rfc = strtok(m_cadenaRFC, "|"); 
   if (rfc)
   {
      sprintf(m_rfcOp, "%s", rfc);
      agc = strtok(NULL, "|");
      if(agc)
         sprintf(m_agcCve, "%s", agc);      
      else 
	  {
	    return false;
		Bitacora->escribePV(BIT_ERROR, "Error en CRevocacionAC en procesaCadena: Error al obtener AGC");
	  }
   }
   else 
   {
     return false;
	 Bitacora->escribePV(BIT_ERROR, "Error en CRevocacionAC en procesaCadena: Error al obtener RFC");
   }
   
   
   return true;
}

void CRevocacionAC::iniciaVarsRev()
{
   m_ifirmaOp   = sizeof(m_firmaOp);
   m_irfcOp     = sizeof(m_rfcOp);
   m_isecOp     = sizeof(m_secOp);
   m_iagcCve    = sizeof(m_agcCve);
   m_inumTram   = sizeof(m_numTram);

   m_firmaOp[0] = 0;
   m_rfcOp[0]   = 0;
   m_secOp[0]   = 0;
   m_agcCve[0]  = 0;
   m_numTram[0] = 0;

   // RORS  lun abr 21 17:15:40 CDT 2008
   m_FecRev[0]  = 0;

   m_icadenaRFC     = sizeof(m_cadenaRFC);
   m_inumSerieRev   = sizeof(m_numSerieRev);
   m_icveRev        = sizeof(m_cveRev);

   m_cadenaRFC[0]   = 0;
   m_numSerieRev[0] = 0;
   m_cveRev[0]      = 0;
}

bool CRevocacionAC::rollBackRevocacion()
{
   bool ok = false;
   ok = m_BDAC->ejecutaOper(QU_CERT_ACT, m_vigFinTmp.c_str(), m_numOperTmp.c_str(), 
               m_rfcOp, m_numSerieRev);
   if(!ok)
      Bitacora->escribe(BIT_ERROR, "Error en CRevocacionAC en rollBackRevocacion: Error en rollback del estado del Certificado Digital.");
	else
	Bitacora->escribe(BIT_INFO, "CRevocacionAC.rollBackRevocacion: Rollback del estado del Certificado Digital Correcto.");
   return ok;
   
}

/* >>> -- 02.00.01 (070219) GHM: Se inhabilito la funci�n para utilizar las funciones de la librer�a
// Regresa la fecha current en formato UTC: SI LA LIBRERIA DE FECHAS FUERA GLOBAL ESTA FUNCION NO SE USARIA.
int CRevocacionAC::
GetCurrentUTC( string *s_FechaUTC)
{
   char       fecha [13], cFechaBD[20], anio[4+1];
   struct tm *sfecha;

   time_t tfecha = time(NULL);
   sfecha = gmtime( &tfecha );
   if (sfecha == NULL)
      return ERR_OBT_VFIN;
   sprintf(anio, "%d", (sfecha->tm_year +1900) );
   sprintf(fecha,"%s%02d%02d%02d%02d%02d", anio+2, sfecha->tm_mon + 1, sfecha->tm_mday,
                                      sfecha->tm_hour, sfecha->tm_min, sfecha->tm_sec);
   // Formatea como: ANO MES DIA HORA MIN SEG
   sprintf(cFechaBD,"20%2.2s-%2.2s-%2.2s %2.2s:%2.2s:%2.2s", fecha, fecha+2, fecha+4, fecha+6, fecha+8, fecha+10 );
   *s_FechaUTC = cFechaBD;
   return 0;
}
*/ //<<< -- 02.00.01
