#ifndef _COSLTSRV_H_
#define _COSLTSRV_H_

static const char* _COSLTSRV_H_VERSION_ ATR_USED = "@(#) SrvAC ( L : DSIC10392AR_ : ConsltSrvAC.h : 1.0.0 : 10/10/14)";

//#VERSION: 1.1.0
/*##############################################################################
  ###  PROYECTO:              PKI-SAT                                        ###
  ###  MODULO:                Servicio AC                                    ###
  ###                                                                        ###
  ###                                                                        ###
  ###  DESARROLLADORES:       Guadalupe Negrete Castro         GNC           ###
  ###                                                                        ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       Viernes 10, octubre del 2014                   ###
  ###                                                                        ###
  ##############################################################################
         1         2         3         4         5         6         7         8
12345678901234567890123456789012345678901234567890123456789012345678901234567890

################################################################################
  VERSION:
     V.1.0.0  (20141010- )    GNC: Version Original
                              Consutas para el Servicio de Autoridad Certificadora PKI-SAT
  

#################################################################################*/



//#VERSION: 1.1.0


#define QU_SOL_OPERACION         "update sol_operacion set no_serie = '%s' where soloper_cve = '%s';"
#define QI_CERTIFICADO           "insert into certificado values('%s','%s','%s','%s','A','%s','%s','%s','%s','%s','%s','%s','%s','%s',CURRENT);"
#define QS_CERT_1ACT_RFC         "select no_serie from certificado where rfc = '%s' AND tipcer_cve=1 AND edo_cer='A' order by no_serie desc;"
#define QU_CERT_REVO             "update certificado set soloper_cve = '%s', oper_sec_ar = '%s', edo_cer = 'R', vig_fin = '%s', fec_reg = '%s' where no_serie = '%s' and rfc = '%s';"
#define QS_VIGENCIA_CERT         "SELECT tipcer_cve, vigencia FROM cat_tip_cer;"
#define QS_EDOCERT               "select edo_cer from certificado where rfc = '%s' and tipcer_cve = 1 order by no_serie desc;"
#define QU_SOL_OPERACION_SECAR   "update sol_operacion set no_serie = '%s' where soloper_cve = '%s' and oper_sec_ar ='%s';"
#define QS_CVEREV_CERT           "select cve_rev from certificado where rfc = '%s' and no_serie = '%s';"	 
#define QS_EDO_VIG_SOLOPCERT     "select edo_cer,vig_fin,soloper_cve from certificado where rfc = '%s' and no_serie = '%s';"
#define QU_CERT_ACT			     "update certificado set edo_cer = 'A', vig_fin = '%s', soloper_cve = '%s' where rfc = '%s' and no_serie = '%s';"

			 
#define SP_GEN_NUMSERIE		        "sp_genNumSerie"
#define SP_REG_NUM					"sp_regNum"
#define SP_REG_SOLOPER				"sp_regSolOper"
#define SP_REG_SOLOPERDET 			"sp_regOperDet"
















#endif















