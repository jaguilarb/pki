static const char* _CSOLRENAC_CPP_VERSION_ ATR_USED = "SrvAC @(#)"\
"DSIC10352AC_ 2010-09-03 CSolRenAC.cpp 1.0.3/3";

//#VERSION: 1.1.0
/*##############################################################################
  ###  PROYECTO:              PKI-SAT                                        ###
  ###  MODULO:                Servicio AC                                    ###
  ###                                                                        ###
  ###                                                                        ###
  ###  DESARROLLADORES:       Amilcar Guerrero Zu�iga         AGZ            ###
  ###                                                                        ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       Lunes 5, diciembre del 2005                    ###
  ###                                                                        ###
  ##############################################################################
         1         2         3         4         5         6         7         8
12345678901234567890123456789012345678901234567890123456789012345678901234567890

################################################################################
  VERSION:
     V.1.0.0  (20051130- )    AGZ: Version Original
                              Servicio de Autoridad Certificadora para la PKI-SAT
     V.1.0.1  (20060518-20060518) AGZ: Cons:06001 Se corrige problem�tica con la
                                      generaci�n y p�rdida de n�meros de serie.
     V.1.0.2  (20081008-20081008) MAML: se elimina la funcion getLlavePrivada()
     V.1.0.3  (20100107-20100107) MAML: se agrega el Pais al Sujeto del Certificado

     V.3.0.3  SrvAC
              (20100826-20100903) GHM:Se realizan las modificaciones para que se asigne la vigencia de 48 meses a FIEL PF
                              y 27 meses a FIEL de PM

#################################################################################*/

//#include <sys/shm.h>
#include <Sgi_Fecha.h>
#include <DefsGeneracion.h>
#include <CSolRenAC.h>

//####################################################################################

//+ GNC  Se agrega definici�n de consultas
#if ORACLE
	#include <CosltORASrvAC.h> 
#else
    #include <CosltSrvAC.h> 
#endif

//####################################################################################
//#include <Sgi_MemCompartida.h> //+ GHM (070426)

int CSolRenAC::m_MsgRen = -1;

CSolRenAC::CSolRenAC(CConfigFile& config, MensajesSAT& msgcli, CSSL_Skt* skt, DATA_CERT *dat ):
           COperacionAC(config, msgcli, skt ), data(dat)
{
}

CSolRenAC::~CSolRenAC()
{
}

bool CSolRenAC::preProceso()
{
   int error = 0;
   bool ok = false;
   
   Bitacora->escribePV(BIT_INFO, "    CSolRenAC.preProceso: Inicio del Preproceso para %s.",getNombreOperacion()); 
   
   if (m_MsgRen == -1)
      m_MsgRen = m_MsgCli.tipOperacion();
	  
   iniciaVarsRen();
   Bitacora->escribe(BIT_INFO, "CSolRenAC.preProceso: Se iniciaron las variables para la solicitud.");
   
   error = sgiErrorBase(m_MsgCli.getMensaje(SOLRENAR, m_firmaOp, &m_ifirmaOp, m_numTram, &m_inumTram, m_secOp, &m_isecOp,
         m_cadenaSol, &m_icadenaSol, m_tipoCer, &m_itipoCer, m_byBufferReq, &m_ibyBufferReq));
   if(!error)
   {
      Bitacora->escribePV(BIT_INFO, "CSolRenAC.preProceso: Se obtuvieron los datos del Mensaje(%i)", SOLCERTAR);
      if (procesaCadenaRFC())
        {
		   Bitacora->escribe(BIT_INFO, "CSolRenAC.preProceso: Se obtuvieron los datos:'RFC, AGC y Nombre'");
    		ok = true;
		}
      else 
      {
	    Bitacora->escribe(BIT_ERROR, "Error en CSolRenAC en preProceso: Error al obtener el RFC, AGC y Nombre.");
        msgCliError('I', "Error en CSolRenAC en preProceso: Error al obtener el RFC, AGC y nombre.", ERR_SEP_CAD_RFC, eAPL); 
      }
   }
   else
   {
      Bitacora->escribePV(BIT_ERROR, "Error en CSolRenAC en preProceso: Error (%i) al obtener los datos del Mensaje (%i).", error, SOLCERTAR); 
      msgCliError('I', "Error en CSolRenAC en preProceso: Error al obtener los datos del Mensaje.", ERR_SEP_CAD_RFC, eAPL); 
   }
   
    Bitacora->escribePV(BIT_INFO, "    CSolRenAC.preProceso: Fin del Preproceso para %s.",getNombreOperacion());
   return ok;
}

bool CSolRenAC::Proceso()
{
   int error = 0;
   bool ok = false;

   Bitacora->escribePV(BIT_INFO, "    CSolRenAC.Proceso: Inicio del Proceso para %s.",getNombreOperacion()); 
   
   //<<<AGZ: Cons:06002 Se cambia la l�gica del registro de la operaci�n en la BD de la AC
   m_NumSerieOp = " ";
   if (!regOperacionBD())
      Bitacora->escribePV(BIT_ERROR, "Error en CSolRenAC en Proceso: Error al registrar la operacion %s.",getNombreOperacion()); 
   //>>>AGZ: Cons:06002 Fin.

   if(!generaSerial())
   {
      Bitacora->escribe(BIT_ERROR, "Error en CSolRenAC en Proceso: Error al generar el n�mero de serie.");
      msgCliError('I', "Error en CSolRenAC en Proceso: Error al generar el n�mero de serie del nuevo Certificado.", ERR_GEN_SER, eAPL);
      return false;
   } 
   else m_NumSerieOp = m_SerialGen;

   error = generaCertificado();
   if (!error) 
   {
      error = sgiErrorBase(m_MsgCli.setMensaje(CERTGENAR, m_rfcOp, m_irfcOp, m_byBufferCert, m_ibyBufferCert));
      if (!error)
      {
	     Bitacora->escribe(BIT_INFO, "CSolRenAC.Proceso: Se prepar� el mensaje con 'CERTGENAR' ");
		 
         if (!regOperacionBDDet(atoi(m_secOp), CERTGENAR))
            Bitacora->escribe(BIT_ERROR, "Error en CSolRenAC en Proceso: Error al registrar la operacion a detalle.");
		 else
            Bitacora->escribe(BIT_INFO, "CSolRenAC.Proceso: Se registro el detalle de la operacion en la BD.");
		 
         error = envia();
         if (!error)
         {
		    Bitacora->escribe(BIT_INFO, "CSolRenAC.Proceso: Se envi� el mensaje a la AR (Mediador)");
            //<<<AGZ: Cons:06002 Se agrega un ciclo para el timeout
            do
            {
               error = recibe();
              if ( error == ERR_SKTTIMEOUT)
                  Bitacora->escribePV(BIT_ERROR, "Error en CSolRenAC en Proceso: Error se recibi� Time Out del socket al esperar mensaje: (%i).", CERTREGIES);
            }while(error == ERR_SKTTIMEOUT);
            //>>>AGZ: Cons:06002 Fin
            if (!error)
            {
               error = procesaMsg();                
               //<<<AGZ: Cons:06001 Se agrega evaluacion para regresar el numero de serie.
               if (!error && m_MsgRen == SOLRENAR && m_MsgCli.tipOperacion() == MSGERROR)
               {
                  if(regresaSerial())
	                Bitacora->escribe(BIT_INFO, "CSolRenAC.Proceso: Se elimino el n�mero de serie de la BD.");
	              else
                    Bitacora->escribePV(BIT_ERROR,"Error en CSolRenAC en Proceso: Error al eliminar el n�mero de serie de la BD");
                  ok = true;
               }
               //>>>AGZ: Cons:06001 Fin. 
               else if (!error)
                  ok = true;
               else
                  msgCliError('I', "Error en CSolRenAC en Proceso: Error en procesaMsg", error, eAPL);
            }
            else
             	{ 
				Bitacora->escribePV(BIT_ERROR,"Error en CSolRenAC en Proceso: Error al recibir de la AR.");
                msgCliError('I', "Error en CSolRenAC en Proceso: Error al recibir de la AR.", error, eAPL);
				}
         }
         else
         {
            if(regresaSerial())
	         Bitacora->escribe(BIT_INFO, "CSolRenAC.Proceso: Se elimino el n�mero de serie de la BD.");
	        else
	         Bitacora->escribePV(BIT_ERROR,"Error en CSolRenAC en Proceso: Error al eliminar el n�mero de serie de la BD");
		
            msgCliError('I', "Error en CSolRenAC en Proceso: Error al enviar el mensaje.(CERTGENAR)", error, eAPL);
         }
      }
      else
      {
        if(regresaSerial())
	       Bitacora->escribe(BIT_INFO, "CSolRenAC.Proceso: Se elimino el n�mero de serie de la BD.");
	    else
	      Bitacora->escribePV(BIT_ERROR,"Error en CSolRenAC en Proceso: Error al eliminar el n�mero de serie de la BD");
		
         msgCliError('I', "Error en CSolRenAC en Proceso: Error al preparar el mensaje.(CERTGENAR)", error, eAPL);
      }
   }
   else
   {
      if(regresaSerial())
	     Bitacora->escribe(BIT_INFO, "CSolRenAC.Proceso: Se elimino el n�mero de serie de la BD.");
	  else
	    Bitacora->escribePV(BIT_ERROR,"Error en CSolRenAC en Proceso: Error al eliminar el n�mero de serie de la BD");
	  
      msgCliError('I', "Error en CSolRenAC en Proceso: Error al generar el Certificado Digital.", error, eAPL);
   }
   Bitacora->escribePV(BIT_INFO, "    CSolRenAC.Proceso: Fin del Proceso para %s.",getNombreOperacion());
   return ok;   
}

const char* CSolRenAC::getNombreOperacion()
{
   return "Generaci�n de Certificado Digital en la AC por Renovaci�n.";
}

int CSolRenAC::getTipOper()
{
   if(m_MsgRen == -1)
      m_MsgRen = m_MsgCli.tipOperacion();
   return m_MsgRen;
}

int CSolRenAC::generaCertificado()
{
   int error = 0;
   char correo[256];
   //uchar by_SerialTmp[20];
   uchar by_SerialTmp[21]; //+ GHM (20100826): Para considerar un cierre de cadena
   SGIX509 x509;
   //unsigned long vigencia = 0;
   char nomCN[65];
   map<int, int>::const_iterator itLista; //+ GHM (20100826): V3.0.3
   nomCN[0] = 0;
   strncpy(nomCN, m_nomCont, 64);
   SNames sujeto[5];
   sujeto[0].dato = nomCN;
   sujeto[0].dato[64] = '\0';
   sujeto[0].id = "commonName";
   sujeto[2].dato = nomCN;
   sujeto[2].dato[64] = '\0';
   sujeto[2].id = "organizationName";
   sujeto[1].dato = m_nomCont;
   sujeto[1].id = "name";
   //>- MAML Thu Jan 07 10:11:00 CDT 2010, por Estandares del Certificado de FIEL se agrega el Pais
   sujeto[3].dato = "MX";
   sujeto[3].id = "countryName";
   
   memcpy(by_SerialTmp, m_SerialGen, 20);
   by_SerialTmp[20] = 0; //+ GHM (20100826)
   //>+ GHM (20100826): V3.0.3
   int iVigencia = 0;
   int idVig = atoi(m_tipoCer);
   itLista =  data->vigencia.find(idVig);
   if (itLista == data->vigencia.end())
      return ERR_TAG_NOT_EXIST;
   iVigencia = itLista->second;

   if ( !obtieneCorreo(correo))
   {
      Bitacora->escribePV(BIT_ERROR, "Error en CSolRenAC en generaCertificado: Error al obtener el correo");
      return ERR_OBT_CORREO;
   }

   //<+ GHM (20100826)
   if (m_tipoCer[0] == '1' || m_tipoCer[0] > '2')
   //>+ GHM (20100826): V3.0.3
   {
      sujeto[4].dato = correo;;
      sujeto[4].id = "emailAddress";
 
      if (m_tipoCer[0] == '1' && strlen(m_rfcOp) == 12 )
      {
         idVig = ID_VIGFIELMORAL;
         itLista = data->vigencia.find(idVig);
         if (itLista == data->vigencia.end())
            return ERR_TAG_NOT_EXIST;
         iVigencia = itLista->second;
      }
      Bitacora->escribePV(BIT_DEBUG, "CSolRenAC.generaCertificado: Vigencia: %i", iVigencia);
   //<+ GHM (20100826)
      error = sgiErrorBase(x509.GenCERTAC((const uchar*)m_byBufferReq, m_ibyBufferReq, (char*)data->m_CertAC.c_str(),
                                          //- GHM (20100826) (uchar*)by_SerialTmp, 20, data->vigencia[atoi(m_tipoCer)], data->m_privKey, data->extFEA,
                                          (uchar*)by_SerialTmp, 20, iVigencia, data->m_privKey, data->extFEA, //+ GHM (20100826): V3.0.3
                                          data->n_extFEA, (uchar*)m_byBufferCert, &m_ibyBufferCert, sujeto, (m_tipoCer[0]=='1')?5:4, data->estCertDig));
   }//+ GHM (20100826): V3.0.3
   else if (m_tipoCer[0] == '2')
      error = sgiErrorBase(x509.GenCERTAC((const uchar*)m_byBufferReq, m_ibyBufferReq, (char*)data->m_CertAC.c_str(),
                                          //- GHM (20100826) (uchar*)by_SerialTmp, 20, data->vigencia[atoi(m_tipoCer)], data->m_privKey, data->extFEA,
                                          (uchar*)by_SerialTmp, 20, iVigencia, data->m_privKey, data->extFEA,
                                          data->n_extFEA, (uchar*)m_byBufferCert, &m_ibyBufferCert, sujeto, 2, data->estCertDig));
   if (!error)
   {
      //>+ ERGL 20080430 Se agreg� para contar con mas informaci�n para la operaci�n.
      Bitacora->escribePV(BIT_INFO, "CSolRenAC.generaCertificado: Se gener� correctamente el certificado solicitado.");
      return 0;
   }
   
   Bitacora->escribePV(BIT_ERROR, "Error en CSolRenAC en generaCertificado: Error al generar el certificado solicitado");
   return ERR_NO_GEN_CERT;
}

int CSolRenAC::procesaMsg()
{
   int error = 0;
   switch(m_MsgCli.tipOperacion())
   {
      case CERTREGIES:
      {
         error = creaArchCert();
         if (!error)
         {
            //<<<AGZ: Cons:06002 Se actualiza el n�mero de serie en sol_operaci�n hasta que se termina correcto
            if(!m_BDAC->ejecutaOper(QU_SOL_OPERACION, m_NumSerieOp.c_str(),m_numTram))
               Bitacora->escribe(BIT_ERROR, "Error en CSolRenAC en procesaMsg: Error al actualizar el n�mero de serie en sol_operacion.(SOLREN)");
			else
			   Bitacora->escribePV(BIT_INFO, "CSolRenAC.procesaMsg: Se actualizo el n�mero de serie del Certificado en la operaci�n.");
            //>>>AGZ: Cons:06002 Fin.
            if (!regOperacionBDDet(atoi(m_secOp), m_MsgCli.tipOperacion()))			   
			   Bitacora->escribe(BIT_ERROR, "Error en CSolRenAC en procesaMsg: Error al registrar la operacion a detalle.");
			 else
			   Bitacora->escribePV(BIT_INFO, "CSolRenAC.procesaMsg: Se registro la operaci�n a detalle"); 
			   
            error = revocaCertificado();
            if (!error)
            {
               error = registraCertificado();
               if(!error)
               {
                  error = sgiErrorBase(m_MsgCli.setMensaje(CERTRENAC, s_FechRev.c_str(), s_FechRev.length(), s_NumSerRev.c_str(),
                           s_NumSerRev.length()));
                  if(!error)
                  {
				    Bitacora->escribePV(BIT_INFO, "CSolRenAC.procesaMsg: Se prepar� el Mensaje con CERTRENAC ");
                     if (!regOperacionBDDet(atoi(m_secOp),  m_MsgCli.tipOperacion()))
                        Bitacora->escribe(BIT_ERROR, "Error en CSolRenAC en procesaMsg: Error al registrar la operacion a detalle.");
					  else
			            Bitacora->escribePV(BIT_INFO, "CSolRenAC.procesaMsg: Se registro la operaci�n a detalle");
					
                     error = envia();
                     if (!error)
                       {
					       Bitacora->escribePV(BIT_INFO, "CSolRenAC.procesaMsg: Se envi� mensaje a la AR");
     					   break;
					   }
                     else
                        Bitacora->escribePV(BIT_ERROR, "Error en CSolRenAC en procesaMsg: Error (%i) Al enviar a la AR.", ERR_NO_ENVIA);
                  }
                  else Bitacora->escribePV(BIT_ERROR, "Error en CSolRenAC en procesaMsg: Error (%i) Al preparar el Mensaje (%i).", error, CERTREGAC);
               }                     
               else Bitacora->escribePV(BIT_ERROR, "Error en CSolRenAC en procesaMsg: Error (%i) Al preparar el Mensaje (%i).", error, CERTREGAC);          
            }
            else Bitacora->escribePV(BIT_ERROR, "Error en CSolRenAC en procesaMsg: Error (%i) Al registrar el Certificado en la BD.", error);
         }
         else Bitacora->escribePV(BIT_ERROR, "Error en CSolRenAC en procesaMsg: Error (%i) Al crear el archivo en el repositorio.", error);
         break;
      }
      case MSGERROR:
      {
         char c_tipErr[2], psz_cdgErr[15], psz_descripcion[256];
         int  i_tipErr = sizeof(c_tipErr), i_cdgErr = sizeof(psz_cdgErr), i_descripcion = sizeof(psz_descripcion);
         error = sgiErrorBase(m_MsgCli.getMensaje(MSGERROR, m_firmaOp, &m_ifirmaOp, c_tipErr, &i_tipErr, psz_cdgErr, &i_cdgErr, 
                 psz_descripcion, &i_descripcion));
         if (!error)
         {
            if (!regOperacionBDDet(atoi(m_secOp), m_MsgCli.tipOperacion()))
               Bitacora->escribe(BIT_ERROR, "Error en CSolRenAC en procesaMsg: Error al registrar la operacion a detalle.");
			else
			   Bitacora->escribePV(BIT_INFO, "CSolRenAC.procesaMsg: Se registro la operaci�n a detalle");
            break;            
         }
         break;  
      }
   }
   return error;
}

int CSolRenAC::creaArchCert()
{
   int error = 0;
   ManArchivo fd;
   error = sgiErrorBase(pkiRutaCert(data->m_repositorio, m_SerialGen, m_pathCompleto, true));
   if (!error || error == EEXIST)
   {
      if(fd.CreaArchivo((uchar*)m_byBufferCert, m_ibyBufferCert, (char*)m_pathCompleto.c_str()))
        {
		   Bitacora->escribe(BIT_INFO, "CSolRenAC.creaArchCert: Se creo el archivo del Certificado Digital en el repositorio.");
		   return 0;
		 }
      else
      {
         escribeBitacora(eAPL, ERR_NO_CREA_ARCH_CERT, "Error en CSolRenAC en creaArchCert: Error al crear el archivo del Certificado Digital en el repositorio.");
         return ERR_NO_CREA_ARCH_CERT;
      }
   }
   else
   {
      escribeBitacora(eAPL, error, "Error en CSolRenAC en creaArchCert: Error al crear la ruta del archivo del Certificado Digital.");
      return error;
   }
   return error;
}

int CSolRenAC::registraCertificado()
{
   SGIX509 x509;
   int error = 0;
   char vigIni[13], vigFin[13], rfcRL[14], curpRL[19], email[50], Curp[19], cveRev[128], rfcTitular[14];

   vigIni[0] = 0; vigFin[0] = 0; rfcRL[0] = 0; curpRL[0] = 0; email[0] = 0; Curp[0] = 0; cveRev[0] = 0;

   SNames sujeto[3], atrib[1];
   int snids[3] = {503, 48, 105};
   int satrib[1] = {54};

   error = sgiErrorBase(x509.ProcREQ((const uchar*)m_byBufferReq, m_ibyBufferReq, sujeto, atrib, NULL, snids, 3, satrib, 1));
   if (!error)
   {
      error = sgiErrorBase(x509.ProcCERT((const uchar*)m_byBufferCert, m_ibyBufferCert, NULL, NULL, (uchar*)vigIni, (uchar*)vigFin));
      if (!error)
      {
	    Bitacora->escribePV(BIT_INFO, "CSolRenAC.registraCertificado: Se extrajeron datos del Certificado Digital en la BD.");

         if (sujeto[0].dato != NULL)
         {
            if (strlen(sujeto[0].dato) > 13)
               procesaDatoRL(sujeto[0].dato, rfcTitular, rfcRL);
            else sprintf(rfcTitular, "%s", sujeto[0].dato);
         }
         if (sujeto[2].dato != NULL)
         {
            if (strlen(sujeto[2].dato) > 18)
               procesaDatoRL(sujeto[2].dato, Curp, curpRL);
            else sprintf(Curp, "%s", sujeto[2].dato);
         }
 
         armaFechaBD(vigIni, vigFin);
         char pszNombreTmp[1024*2];
		 string cVacia = "";
         pszNombreTmp[0] = '\0';
         m_BDAC->prepCadDelim(1, (const char*)m_nomCont, pszNombreTmp); 

         Bitacora->escribePV(BIT_DEBUG, "CSolRenAC.registraCertificado: Nombre recibido: %s --- Nombre cambiado: %s", m_nomCont, pszNombreTmp);
		 Bitacora->escribePV(BIT_INFO, "CSolRenAC.registraCertificado: Se procesaron datos del certificado.");

		#if ORACLE
		     if (m_BDAC->ejecutaOper(QI_CERTIFICADO, m_SerialGen,m_tipoCer,m_rfcOp,Curp,pszNombreTmp,sujeto[1].dato,rfcRL,curpRL,
			 vigI.c_str(),vigF.c_str(),m_numTram,m_secOp,atrib[0].dato))	
			 {
				//>+ ERGL 20080430 Se agreg� para contar con mas informaci�n para la operaci�n.
				Bitacora->escribePV(BIT_INFO, "CSolRenAC.registraCertificado: Se registr� correctamente el Certificado Digital en la BD. (RFC: %s, SN: %s, TIPO: %s)", m_rfcOp, m_SerialGen, m_tipoCer );
				return 0;
             }
             else 
			  escribeBitacora(eAPL, ERR_REG_CERT_BD, "Error en CSolRenAC en registraCertificado: Error al registrar el Certificado Digital en la BD.");
		#else
		   
		   if (m_BDAC->ejecutaOper(QI_CERTIFICADO, m_SerialGen, m_numTram, m_secOp, m_tipoCer, m_rfcOp, Curp,
             pszNombreTmp, sujeto[1].dato, rfcRL, curpRL, vigI.c_str(), vigF.c_str(), atrib[0].dato))
            {
				//>+ ERGL 20080430 Se agreg� para contar con mas informaci�n para la operaci�n.
				Bitacora->escribePV(BIT_INFO, "CSolRenAC.registraCertificado: Se registr� correctamente el Certificado Digital en la BD. (RFC: %s, SN: %s, TIPO: %s)", m_rfcOp, m_SerialGen, m_tipoCer );
				return 0;
            }
            else 
			 escribeBitacora(eAPL, ERR_REG_CERT_BD, "Error en CSolRenAC en registraCertificado: Error al registrar el Certificado Digital en la BD.");
		#endif		 
      }
      else return sgiErrorBase(escribeBitacora(eAPL, ERR_PROC_CERT_BD, "Error en CSolRenAC en registraCertificado: Error al extraer datos del Certificado Digital en la BD."));
   }
   return 0;
}

bool CSolRenAC::procesaCadenaRFC()
{
   bool ok = false;
   char *rfc = NULL, *agc = NULL, *nom = NULL;
   rfc = strtok(m_cadenaSol, "|");
   if(rfc)
   {
      sprintf(m_rfcOp, "%s", rfc);
      nom = strtok(NULL, "|");
      if(nom)
      {
         sprintf(m_nomCont, "%s", nom);
         agc = strtok(NULL, "|");
         if(agc)
         {
            sprintf(m_agcCve, "%s", agc);         
            ok = true;
         }
		 else
		  Bitacora->escribePV(BIT_ERROR, "Error en CSolRenAC en procesaCadenaRFC: Error al obtener AGC");
         m_irfcOp = strlen(m_rfcOp); 
      }
	  else
	    Bitacora->escribePV(BIT_ERROR, "Error en CSolRenAC en procesaCadenaRFC: Error al obtener Nombre");
   }
   else
    Bitacora->escribePV(BIT_ERROR, "Error en CSolRenAC en procesaCadenaRFC: Error al obtener RFC");
   return ok;
}
  
bool CSolRenAC::generaSerial()
{
   bool ok = false;
   if(m_tipoCer[0] > '2')
     //GNC (27102014)
	   #if ORACLE
		  ok = m_BDAC->ejecutaFuncion(SP_GEN_NUMSERIE, ":b", "%d", 1);
	   #else
		  ok = m_BDAC->ejecutaSP(SP_GEN_NUMSERIE, "%i", 1);
		#endif	  
   else 
       #if ORACLE
		  ok = m_BDAC->ejecutaFuncion(SP_GEN_NUMSERIE, ":b", "%d", 0);
	   #else
		  ok = m_BDAC->ejecutaSP(SP_GEN_NUMSERIE, "%i", 0);
		#endif   
   if(ok)
   {
     Bitacora->escribePV(BIT_INFO, " CSolRenAC.generaSerial: Se genero el n�mero de serie: %s.",m_SerialGen); 
     ok = m_BDAC->getValores("b", &m_SerialGen);
    }
   return ok;
}

void CSolRenAC::armaFechaBD(char *vi, char *vf)
{
   char anio[5];
   struct tm *tiempo;
   time_t t = time(NULL);
   tiempo = gmtime(&t);
   sprintf(anio, "%i", tiempo->tm_year+1900);

   anio[2] = vi[0]; 
   anio[3] = vi[1];

   vigI = anio+(string)"-"+vi[2]+vi[3]+(string)"-"+vi[4]+vi[5]+(string)" "+vi[6]+vi[7]+(string)":"+vi[8]+vi[9]+(string)":"+vi[10]+vi[11];

   anio[2] = vf[0]; 
   anio[3] = vf[1];

   vigF = anio+(string)"-"+vi[2]+vi[3]+(string)"-"+vi[4]+vi[5]+(string)" "+vi[6]+vi[7]+(string)":"+vi[8]+vi[9]+(string)":"+vi[10]+vi[11]; 
}

int CSolRenAC::revocaCertificado()
{
   if (m_BDAC->consultaReg(QS_CERT_1ACT_RFC, m_rfcOp))
   {
      Bitacora->escribePV(BIT_INFO, "CSolRenAC.revocaCertificado: Se Consult� certificado");
      if (m_BDAC->getValores("s", &s_NumSerRev))
      {
        
		Bitacora->escribePV(BIT_INFO, "CSolRenAC.revocaCertificado: Se obtuvieron datos del certificado");
		
         CFecha *FuncFecha = new CFecha();
         std::string sFecReg;
         //int errFecha = 0; 
         if( ( /*errFecha = */ FuncFecha->GetCurrentUTC(&s_FechRev) ) == 0 )
         {
            // --->>> MAML 070727 : Se modifica el campo 'fec_reg' de la tabla certificado con la hora local de la fecha de RevocacionxRenovacion
            if ( FuncFecha->TransFecha(s_FechRev, &sFecReg, false ) != 0 )
               return ERR_NO_FREG; 		

            if (m_BDAC->ejecutaOper(QU_CERT_REVO, m_numTram, m_secOp, s_FechRev.c_str(), sFecReg.c_str(), s_NumSerRev.c_str(), m_rfcOp))
            {
               // MAML 070727: El next Query comentado de la DB de la AC no es necesario, porque el dato lo acabamos de actualizar
               //>+ ERGL 20080430: Se agreg� para tener mas informaci�n en la operaci�n. 
               Bitacora->escribePV(BIT_INFO, "CSolRenAC.revocaCertificado: Se revoc� el certificado %s del rfc: %s a causa de una renovaci�n.", 
               s_NumSerRev.c_str(), m_rfcOp );
               return 0;
 
            }
            else
            {
			    Bitacora->escribePV(BIT_ERROR, "Error en CSolRenAC en revocaCertificado: Error al revocar el certificado");
    			return ERR_REV_CERT;
			}
         }
         else 
		 {
		   Bitacora->escribePV(BIT_ERROR, "Error en CSolRenAC en revocaCertificado: Error al obtener la fecha de registro para la Revocacion");
           return ERR_NO_UTC;
		 }
         //<<<
      }
      else 
	  {
	    Bitacora->escribe(BIT_ERROR, "Error en CSolRenAC en revocaCertificado: Error al obtener datos Certificado Digital de la BD");             
        return ERR_GET_NO_SERIE;
	  }
   }
   else 
   {
      Bitacora->escribe(BIT_ERROR, "Error en CSolRenAC en revocaCertificado: Error al consultar el estado del Certificado Digital."); 
      return ERR_NO_TIENE_CERT_ACT;
   }
}

void CSolRenAC::iniciaVarsRen()
{
   iniciaVarsTram();
   m_cadenaSol[0]    = 0;
   m_tipoCer[0]      = 0;
   m_byBufferReq[0]  = 0;
   m_byBufferCert[0] = 0;
   m_nomCont[0]      = 0;

   m_icadenaSol      = sizeof(m_cadenaSol);
   m_itipoCer        = sizeof(m_tipoCer);
   m_ibuffReq        = sizeof(m_byBufferReq);
   m_ibyBufferCert   = sizeof(m_byBufferCert);
   m_ibyBufferReq    = sizeof(m_byBufferReq);
   m_inomCont        = sizeof(m_nomCont);
}

bool CSolRenAC::regresaSerial()
{
   bool ok = false;
   if(m_tipoCer[0] > '2')
      ok = m_BDAC->ejecutaSP(SP_REG_NUM, "%i", 1);
   else ok = m_BDAC->ejecutaSP(SP_REG_NUM, "%i", 0);
   return ok;
}

void CSolRenAC::procesaDatoRL(char *cadOrig, char *Cad1, char *Cad2)
{
   char cad1[30], cad2[30], *cad = NULL;
   int i_cad1 = 0, i_cad2 = 0, i = 0;

   cad = strtok(cadOrig, "/");
   if(cad)
   {
      strcpy(cad1, cad);
      cad = NULL;
      cad = strtok(NULL, "/");
      if (cad)
      {
         strcpy(cad2, cad);

         i_cad1 = strlen(cad1);
         for(i = 0; i < i_cad1; i++)
         {
            if (cad1[i] == ' ' && i == 0)
               cad1[i] = cad1[i + 1];
            else if (cad1[i] != ' ')
               cad1[i] = cad1[i];
            else if (cad1[i] == ' ' && i != 0)
               cad1[i] = 0;
            else
                continue;
         }
         if(cad1[0] == ' ')
            Cad1[0] = '\0';
         else
            strcpy(Cad1, cad1);

         i_cad2 = strlen(cad2);
         for(i = 0; i < i_cad2; i++)
         {
            if (cad2[i] == ' ' && i == 0)
               cad2[i] = cad2[i + 1];
            else if (cad2[i] != ' ' && i != 0)
               cad2[i] = cad2[i + 1];
            else if (cad2[i] == ' ' && i != 0)
               cad2[i] = 0;
            else
               continue;
         }
         if(cad2[0] == 0)
            Cad2[0] = '\0';
         else
            strcpy(Cad2, cad2);
      }
   }
}

bool   CSolRenAC::obtieneCorreo(char* correo)
{
   SNames sujeto[1], atrib[1];
   int snids[1] = {48};
   int satrib[1] = {54};
   SGIX509 x509;

   int error = sgiErrorBase(x509.ProcREQ((const uchar*)m_byBufferReq, m_ibyBufferReq, sujeto, atrib, NULL, snids, 1, satrib, 1));
   if (!error)
   {
      Bitacora->escribePV(BIT_INFO, "CSolRenAC.obtieneCorreo: ObtieneCorreo ProcREQ: %i", error);
      if (sujeto[0].dato != NULL)
      {
            strcpy(correo, sujeto[0].dato);
            Bitacora->escribePV(BIT_INFO, "CSolRenAC.obtieneCorreo: ObtieneCorreo correo est: %s", sujeto[0].dato);
      }
      else correo[0] = '\0';
      return true;
   }
   Bitacora->escribePV(BIT_INFO, "CSolRenAC.obtieneCorreo: ObtieneCorreo: %i", error);
   return false;


}


