static const char* _SRVAC_CPP_VERSION_ ATR_USED = "SrvAC @(#)"\
"DSIC10352AC_ 2010-09-03 SrvAC.cpp 1.1.0/2";

//#VERSION: 1.1.0
/*##############################################################################
  ###  PROYECTO:              PKI-SAT                                        ###
  ###  MODULO:                SrvAR    Servidor de la AR                     ###
  ###                                                                        ###
  ###  DESARROLLADORES:       H�ctor Ornelas Arciga           HOA            ###
  ###                         Amilcar Guerrero Zu�iga         AGZ            ###
  ###                         Gudelia Hern�ndez Molina        GHM            ###
  ###                         Miguel Angel Mendoza L�pez      MAML           ###
  ###                         Roc�o Alejandra Mart�nez Olayo  RAMO           ###
  ###                         Silvia Eur�dice Rocha Reyes     SERR           ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       Martes 6, diciembre del 2005                   ###
  ###                                                                        ###
  ##############################################################################
         1         2         3         4         5         6         7         8
12345678901234567890123456789012345678901234567890123456789012345678901234567890
*/
/*##############################################################################
   VERSION:
      V.02.00      (20051206 - 20051231) HOA: 2a versi�n: reconstrucci�n total
                                del servicio
                                M�dulo de control, recibe las solicitudes de las
                                aplicaciones clientes CertiSAT (ventanilla y Web)
                                
             .01   (20070208 - 200702  ) GHM: Modificaciones relacionadas con la
                                sustituci�n de las funciones de tiempo con las
                                funciones de la librer�a Sgi_Fecha
             .02   (20070313 -20070313)  GHM: Modificaciones en el uso de la bit�cora
                                con los resultados de la librer�a Sgi_Fecha
             .03   (20081008 - 20081008) MAML: se agrega la funcion getLlavePrivada()
                                y la variable m_privKey
             .04   (20081118)   GHM: Se establece el nivel de bit�cora a trav�s del archivo de configuraci�n

      V.3.0.3      (20100826- 20100903) GHM:Se realizan las modificaciones para que se asigne la vigencia de 48 meses a FIEL PF
                              y 27 meses a FIEL de PM
           .4      (20100903- 20100903) GHM: Se agrega funci�n para leer del archivo de configuraci�n los datos de la vigencia.
                                
#################################################################################*/

#define _SHK_CPP_
#include <string>
#include <sys/socket.h>
#include <sys/shm.h>

#include <DefsGeneracion.h>
#include <CConexionAC.h>
#include <CRevocacionAC.h>
#include <CSolCertAC.h>
#include <CSolRenAC.h>
#include <Sgi_MemCompartida.h>
#include <shk.h>

//#################################################################################
#define PATH_CONF            "/usr/local/SAT/PKI/etc/"
#define PATH_DBIT            "/var/local/SAT/PKI/"

#define ARCH_EXT             PATH_CONF "ExtensionesAC.cfg"
#define EXT_FIEL             "EXTENSIONES_FIEL" 
#define EXT_SELLO            "EXTENSIONES_SELLO"

#ifdef DBG
   #define ARCH_CONF            PATH_CONF "SrvAC_dbg.cfg"
   #define PATH_BITA            PATH_DBIT "SrvAC_dbg.log"
#else
   #define ARCH_CONF            PATH_CONF "SrvAC.cfg"
   #define PATH_BITA            PATH_DBIT "SrvAC.log"
#endif

//################################################################################
//   Variables globales
//################################################################################

CSrvAC* ptrSrvAC = NULL;
static string  pSSL;

//################################################################################
// Creaci�n del objeto de Bitacora
//################################################################################
CBitacora* Crea_CBitacora()
{
   return new CBitacora(PATH_BITA);
}
//################################################################################
// Creaci�n del objeto principal para el servicio de la AR
//################################################################################
CSrvUnx* Crea_CSrvUnx()
{
   return ptrSrvAC = new CSrvAC;
}
//################################################################################
bool libSrvUnx_Ini()
{
   #if defined( DBG )
      sleep(40);
   #endif
   #if defined( DBG ) 
      Bitacora->setNivel(BIT_DEBUG);
   #endif
   iniciaLibOpenSSL();
   return true;
}
//################################################################################
bool libSrvUnx_Fin()
{
   terminaLibOpenSSL();
   return true;
}
//################################################################################
static int verificacionSSL(int ok, X509_STORE_CTX *store)
{
   char data[256];
   
   if (!ok)
   {
      X509 *cert = X509_STORE_CTX_get_current_cert(store);
      int depth  = X509_STORE_CTX_get_error_depth(store);
      int err    = X509_STORE_CTX_get_error(store);

      Bitacora->escribePV(BIT_ERROR, "verificacionSSL(): Error con el certificado %i", depth);
      X509_NAME_oneline(X509_get_issuer_name(cert), data, sizeof(data));
      Bitacora->escribePV(BIT_ERROR, "verificacionSSL(): \tissuer  = %s", data);
      X509_NAME_oneline(X509_get_subject_name(cert), data, sizeof(data));
      Bitacora->escribePV(BIT_ERROR, "verificacionSSL(): \tsubject = %s", data);
      Bitacora->escribePV(BIT_ERROR, "verificacionSSL(): error = %i: %s", err, X509_verify_cert_error_string(err));
   }
   return ok;
}

static int passwordSSL(char *buf, int size, int rwflag, void *password)
{
   int SSLsize = pSSL.size();
   
   rwflag   = rwflag;
   password = password;
   if (size < SSLsize)
   {
      Bitacora->escribePV(BIT_ERROR, "Error en CSrvAC en passwordSSL: Error al asignar el password de SSL, buffer menor del requerido(%d -> %d)", 
                          size, pSSL.size());
      return 0;
   }
  
   if (!desencripta((uint8*) pSSL.c_str(), pSSL.size(), (uint8*) buf, &size))
   {
      Bitacora->escribe(BIT_ERROR, "Error en CSrvAC en passwordSSL: Error al desencriptar  el password de SSL"); 
      return 0;
   }
   
   return size;
}
//################################################################################
//################################################################################
//################################################################################
                        
CSrvAC::CSrvAC()
   : Configuracion(ARCH_CONF),
     MsgCli(),
     archCfgExt(NULL),
     datosCertAC(false)
{
}
//################################################################################
CSrvAC::~CSrvAC()
{
   if ( data.extSEL )
   {
     delete []data.extSEL;
     data.extSEL = NULL;
   }
   if ( data.extFEA )
   {
     delete []data.extFEA;
     data.extFEA = NULL;
   }
   cierraArchExtensiones();
   if ( data.m_privKey )
   {
      RSA_free( data.m_privKey  );
      data.m_privKey = NULL;
   }
}
//################################################################################

#define ESPERA  300 

bool CSrvAC::SetParmsSktProc(CSSL_parms& parms)
{
   int    r1, r2, r3, r4, r5;
   string ac, acPath, cert, privK;

   r1 = Configuracion.getValorVar("[SKT_SSL_AC]", "ROOT"    , &ac);
   r2 = Configuracion.getValorVar("[SKT_SSL_AC]", "RUTAC", &acPath);
   r3 = Configuracion.getValorVar("[SKT_SSL_AC]", "AC1"  , &cert);
   r4 = Configuracion.getValorVar("[SKT_SSL_AC]", "ACK" , &privK);
   r5 = Configuracion.getValorVar("[SKT_SSL_AC]", "BLLAVE" , &pSSL);
   //>>> V.02.00.04 GHM (081118): Se obtiene el nivel de bit�cora
   string nvlBitac;
   int r6 = Configuracion.getValorVar("[BITACORA]", "NIVELBIT" , &nvlBitac);
   //<<< V.02.00.04
   if (r1 || r2 || r3 || r4 || r5 /*+ V.02.00.04*/ || r6)
   {
      Bitacora->escribePV(BIT_ERROR, "Error en CSrvAC en SetParmsSktProc: Error al leer los par�metros de configuraci�n SSL_SRV (%d,%d,%d,%d,%d) y  BIT(%d)", 
            r1, r2, r3, r4, r5 /*+ V.02.00.04*/, r6);
      return false;
   }
   //>>> V.02.00.04 GHM (081118): Se establece la bit�cora
   Bitacora->escribePV(BIT_DEBUG, "CSrvAC.SetParmsSktProc: De archivo de configuraci�n se obtuvo:ROOT (%s), RUTAAC (%s), AC1(%s), ACK(%s) y NIVELBIT(%s)",
                                  ac.c_str(), acPath.c_str(), cert.c_str(), privK.c_str(), nvlBitac.c_str() );
   BIT_NIVEL iNvlBitac = (BIT_NIVEL) atoi(nvlBitac.c_str());
   if (Bitacora)
      Bitacora->setNivel( iNvlBitac);
   //<<< V.02.00.04
   struct timeval espera = {ESPERA, 0};
   parms.SetServicio(ESSL_entero, &espera, ac.c_str(), acPath.c_str(), cert.c_str(), privK.c_str(), 
                                 verificacionSSL, passwordSSL);
   return true;
}
//********************************************************************************
bool CSrvAC::envOperInv()
{
   char errorn[128];
   sprintf(errorn, "Error en CSrvAC en envOperInv: Operaci�n inv�lida (%d)",  MsgCli.tipOperacion());
   MsgCli.setMensaje(MSGERROR, "I", 1, "0", 1, errorn, strlen(errorn)); 
   SktProc->Envia(MsgCli.buffer, MsgCli.tamMensaje()); 
   Bitacora->escribe(BIT_ERROR, errorn);
   Bitacora->escribe(BIT_INFO, "CSrvAC.envOperInv: Se cerrar� la aplicaci�n."); 
   return true;
}
//################################################################################
bool CSrvAC::Inicia()
{
   string  s_estCert, s_datosCert, s_keyprivada, claves[] = { "CA_PATHX", "AC_ALMACEN", "EST_CERT_DIG", "DATOS_CERT_AC", "KEY_PRIVADA" };
   // Cargar archivo de configuraci�n
   int error = Configuracion.cargaCfgVars();
   if (error)
      Bitacora->escribePV(BIT_ERROR, "Error en CSrvAC en Inicia: Problemas al leer el archivo de configuraci�n (%d): %s", error, ARCH_CONF);
   
   // obtiene el nombre del archivo certif. de la AC
   else if ( (error = Configuracion.getValoresConf("[OP_SAT_AC]", 5, claves, &(data.m_CertAC), &(data.m_repositorio), &s_estCert, &s_datosCert, &s_keyprivada ) ) )       
      Bitacora->escribe(BIT_ERROR, "Error en CSrvAC en Inicia: Error no existen las variables en el arch. de configuraci�n([OP_SAT_AC], CA_PATHX, AC_ALMACEN, EST_CERT_DIG, DATOS_CERT_AC, KEY_PRIVADA).");
   else
   { 
      data.estCertDig = (atoi( (char *)s_estCert.c_str()) == 1);
      datosCertAC = (atoi( (char *)s_datosCert.c_str()) == 1);
      if ( (error = !getLlavePrivada( atoi( (char *)s_keyprivada.c_str()) == 1 )) )
         Bitacora->escribe(BIT_ERROR, "En CSrvAC.Inicia(): Error al obtener la llave privada de la memoria compartida." );
      else if ( (error = !VerificaLlaves()) ) 
         Bitacora->escribe(BIT_ERROR, "Error en CSrvAC.Inicia: Error fall� la verificaci�n de la llave privada contra la llave publica.");
      else if ( (error = !( abreArchExtensiones() && iniciaExtensiones(EXT_FIEL) && iniciaExtensiones(EXT_SELLO) && cierraArchExtensiones() ) ) )
         Bitacora->escribe(BIT_ERROR, "Error en CSrvAC.Inicia: Error no pudo obtener las extensiones de FIEL y/o de Sellos." );
      else
         Bitacora->escribe(BIT_INFO, "CSrvAC.Inicia: Se obtuvieron las extensiones de FIEL y Sellos correctamente." );
   }
   return !error;
}
//################################################################################
bool CSrvAC::InicioDeSesion()
{
   bool ok = false;
   CConexionAC* conexion = new CConexionAC(Configuracion, MsgCli, SktProc);
   if (conexion)
   {
      ok = conexion->Operacion();
      //>>> MAML 081119
      //GHM (20100826): V3.0.3 Se modific� la funci�n y el tipo de par�metro 
      if( ( !conexion->getVigencias( data.vigencia )))
         Bitacora->escribe(BIT_ERROR, "Error en CSrvAC en InicioDeSesion(): Error al obtener las vigencias de la tabla CAT_TIP_CER de la Base de datos." );
      //<<<
      delete conexion;
   }
   return ok;
}

//################################################################################
bool CSrvAC::Operaciones()
{
   intE error;
   //int lSktBufCli;
   //<<<AGZ: 050706 Control para cerrar la aplicaci�n en caso de desincronizaci�n.
   int ControlDesincronizacion = 0;
   //>>>AGZ: 050706 Fin
   for (;;)
   {
      error = MsgCli.Recibe(SktProc);
      if (!error)
      {
	    Bitacora->escribePV(BIT_INFO, "CSrvAC.Operaciones: Se recibi� el Mensaje del cliente");
		
         COperacionAC *operacion = NULL;
         int oper = MsgCli.tipOperacion();
		 Bitacora->escribePV(BIT_DEBUG, "CSrvAC.Operaciones: MsgCli.tipOperacion(): [%d]", oper);
         switch (oper)
         {
            case SOLCERTAR       :  operacion = new CSolCertAC       (Configuracion, MsgCli, SktProc, &data ); break;
            case SOLREVFAR       :
            case SOLREVAR        :
            case SOLREVOFAR      :  operacion = new CRevocacionAC    (Configuracion, MsgCli, SktProc);         break;
            case SOLRENAR        :  operacion = new CSolRenAC        (Configuracion, MsgCli, SktProc, &data ); break;
            default : envOperInv(); ControlDesincronizacion = -1; break;
         }
         if (ControlDesincronizacion == -1)
            break;
         else if (!operacion)
            Bitacora->escribePV(BIT_ERROR, "Error en CSrvAC en Operaciones: Error al generar el objeto de operaci�n %d (errno = %d)", oper, errno);
         else 
         {
            operacion->Operacion();
            delete operacion;
            if (oper == SOLDESCONEXION)
               return false;
         }
         
      }
      else
      {
         if (sgiErrorBase(error) != ERR_SKTTIMEOUT)
         {
            Bitacora->escribe(BIT_ERROR, error, "Error en CSrvAC en Operaciones: Error al recibir solicitud de operaci�n");
            break;
         }
         struct timespec ts = { 0, 1000000};
         nanosleep(&ts, NULL);
      }
   }
   
   return !error;         
}
//################################################################################
bool CSrvAC::Proceso()
{
   bool ok;
	   
   ok = InicioDeSesion() &&
        Operaciones();
   return ok;
}
//################################################################################
// switchea entre la llave privada antgerior y la nueva de la shared memory
bool CSrvAC::getLlavePrivada( bool es_keyprivada_nueva )
{
   bool ok;

   if ( es_keyprivada_nueva )
      ok = getLlavePrivada_NUEVA();
   else if ( !( ok = getLlavePrivada_ANTERIOR() ) )
      Bitacora->escribe(BIT_ERROR, "Error en CSrvAC en getLlavePrivada: Error en getLlavePrivada_ANTERIOR para obtener la llave privada ANTERIOR de la memoria compartida." );
   return ok;   
}

// Obtiene del arch. de config. la versi�n y luego la llave privada de la shared memory
bool CSrvAC::getLlavePrivada_NUEVA()
{
   string s_key, s_id_key, s_passwd, claves[] = { "KEY", "ID_KEY", "KEY_PASSW" };

   if ( ( Configuracion.getValoresConf("[KEY_SHARED]", 3, claves, &s_key, &s_id_key, &s_passwd ) ) != 0)
      Bitacora->escribe(BIT_ERROR, "Error en CSrvAC en getLlavePrivada_NUEVA: Error al leer los par�metros de configuraci�n del SrvAC de la secci�n [KEY_SHARED]." );
   else
   {
      // OjO: por si se llega a encriptar el password y asi se pone en el archivo de Config.
      uint8 password[128];
      int i_password = sizeof(password);

      if (!desencripta((uint8*)s_passwd.c_str(), s_passwd.length(), password, &i_password)  )
         Bitacora->escribe( BIT_ERROR, "Error en CSrvAC en getLlavePrivada_NUEVA: Error al desencriptar el password de acceso al PKCS7 de la shared-memory" );
      else
      {
         password[i_password] = 0;
         return ( getLlavePKCS7( (char *)s_key.c_str(), (char *)s_id_key.c_str(), (char *)password, i_password ) );
      }
   }
   return false;
}
//################################################################################
bool CSrvAC::getLlavePKCS7( char *key, char *id_key, char *passwd, int l_passwd  )
{
   bool ret = false;
   ServSegInfo info;
   CSHMemPKI *mem   = new CSHMemPKI(atoi(key), MAX_KEY_SIZE); // TamBufShMem);

   if (mem->abre(false))
   {
      int posServ  = mem->findServ(id_key);
      if (posServ >= 0 )
      {
         info  = mem->getServInfo(posServ);
         BIO *bLlavePriv  = BIO_new(BIO_s_mem());
         BUF_MEM *memLlavePriv = BUF_MEM_new();
         if ( bLlavePriv && memLlavePriv )
         {
            unsigned char *bufDes;
            int tamBufDes = sizeof(bufDes);
            int tamLlave  = ((uint16(info.privKey[2]) << 8) | uint16(info.privKey[3]));

            BUF_MEM_grow(memLlavePriv, tamLlave + 4 );
            memcpy(memLlavePriv->data, info.privKey, tamLlave + 4 );
            BIO_set_mem_buf(bLlavePriv, memLlavePriv, BIO_CLOSE);

            int rc = BIO_write(bLlavePriv, memLlavePriv->data, tamLlave + 4 );

            PKCS7 *p7 = NULL;
            d2i_PKCS7_bio(bLlavePriv, &p7);
            if ( rc && p7 )
            {
               X509_ALGOR *pbe = p7->d.encrypted->enc_data->algorithm;
               if ( PKCS12_pbe_crypt(pbe, passwd, l_passwd, (unsigned char*) p7->d.encrypted->enc_data->enc_data->data, p7->d.encrypted->enc_data->enc_data->length, &bufDes, &tamBufDes, 0) )
                  ret = getLlavePrivada_act( (const unsigned char*)bufDes, tamBufDes) ;
               else
                  Bitacora->escribe(BIT_ERROR, "Erro en CSrvAC en getLlavePKCS7: Fall� la funcion que obtiene el PKCS8 en PKCS12_pbe_crypt(pbe, passwd, l_passwd,...)" );
            }
            else
            {
               if ( !rc )
                  Bitacora->escribe(BIT_ERROR, "Erro en CSrvAC en getLlavePKCS7: Fall� la funcion que obtiene el PKCS7 en rc = BIO_write(bLlavePriv, memLlavePriv->data, tamLlave + 4 );" );
               else 
                  Bitacora->escribe(BIT_ERROR, "Erro en CSrvAC en getLlavePKCS7: Fall� la funcion que obtiene el PKCS7 en d2i_PKCS7_bio(bLlavePriv, &p7);" );
            } 
            BIO_free(bLlavePriv);
         }
         else
            Bitacora->escribe(BIT_ERROR, "Erro en CSrvAC en getLlavePKCS7: Error memoria insuficiente ( bLlavePriv == NULL || memLlavePriv == NULL)." );
      }
      else
         Bitacora->escribePV(BIT_ERROR, "Erro en CSrvAC en getLlavePKCS7: Error en posServ = %d, en la funcion mem->findServ(id_key)", posServ );
   }
   else
      Bitacora->escribe(BIT_ERROR, "Erro en CSrvAC en getLlavePKCS7: Error en la funcion (mem->abre(false) de CSHMemPKI *mem" );
   return ret;
}
//################################################################################
bool CSrvAC::getLlavePrivada_act( const unsigned char* cLlavePriv, int tamLlavePriv )
{
   bool ret = false;

   BIO *bLlavePriv  = BIO_new(BIO_s_mem());
   if ( bLlavePriv )
   {
      EVP_PKEY *evpLlavePriv =  EVP_PKEY_new();
      BUF_MEM *memLlavePriv = BUF_MEM_new();
      SGI_SHK* shk = SGI_SHK_new();

      if ( evpLlavePriv && memLlavePriv && shk && cLlavePriv )
      {
         shk  = d2i_SGI_SHK(&shk, &cLlavePriv, tamLlavePriv); 
         BUF_MEM_grow(memLlavePriv, shk->pkcs8->length );
         memcpy(memLlavePriv->data, shk->pkcs8->data, shk->pkcs8->length );
         BIO_set_mem_buf(bLlavePriv, memLlavePriv, BIO_CLOSE);
         evpLlavePriv = d2i_PKCS8PrivateKey_bio(bLlavePriv, &evpLlavePriv, NULL, shk->pwd->data );
         if ( evpLlavePriv )
         {
            data.m_privKey = EVP_PKEY_get1_RSA(evpLlavePriv);
            if ( data.m_privKey )
            {
               Bitacora->escribe(BIT_INFO, "CSrvAC.getLlavePrivada_act: Se subi� la llave privada RSA de la AC correctamente" ); 
               ret = true;
            }   
         }
         else
            Bitacora->escribe(BIT_ERROR, "Erro en CSrvAC en getLlavePrivada_act: Error fall� el cargado de la variable EVP_PKEY *evpLlavePriv = d2i_PKCS8PrivateKey_bio()." );
         EVP_PKEY_free(evpLlavePriv);
      }
      else
         Bitacora->escribe(BIT_ERROR, "Error en CSrvAC en getLlavePrivada_act: Error memoria insuficiente(evpLlavePriv, shk, memLlavePriv, cLlavePriv)." );

      Bitacora->escribePV(BIT_INFO, "CSrvAC.getLlavePrivada_act: string_err: %s, errno=%d, res = %s", ERR_error_string(ERR_get_error(), NULL ), errno, (ret)? "true":"false" );
      BIO_free(bLlavePriv);
   }
   else
      Bitacora->escribe(BIT_ERROR, "Error en CSrvAC en getLlavePrivada_act: Error memoria insuficiente, bLlavePriv == NULL." );
   return ret;
}
//################################################################################
bool CSrvAC::abreArchExtensiones()
{
   archCfgExt = new CArchCfgSC();
   if ( archCfgExt )
   {
      if ( archCfgExt->cargaArchivo(ARCH_EXT) )
         return true; 
      else
         Bitacora->escribePV(BIT_ERROR, "Error en CSrvAC en abreArchExtensiones: Error no pudo ejecutar archCfgExt->cargaArchivo(%s)", ARCH_EXT ); 
   }
   else
      Bitacora->escribe(BIT_ERROR, "Error en CSrvAC en abreArchExtensiones: Error memoria insuficiente, new CArchCfgSC()");
   return false;
}
//################################################################################
bool CSrvAC::cierraArchExtensiones()
{
   if ( archCfgExt != NULL)
   {
      delete archCfgExt;
      archCfgExt = NULL;  
   }
   return true;
}
//################################################################################
bool CSrvAC::iniciaExtensiones( const char *seccion )
{
   int     i = -2, iNumElem;
   string  nombre, valor;
   SNames  *ext = NULL;

   iNumElem = archCfgExt->length(seccion);
   if ( iNumElem )
   {
      if ( (ext = new SNames[iNumElem]) != NULL )
      {
         for (i = 0; i < iNumElem; i++)
            ext[i].Inicia();
  
         for (i = 0 ; i < iNumElem ; i++)
         {
            if ( archCfgExt->get(seccion, i, nombre, valor) )
            {  
               strcpy(ext[i].id,   nombre.c_str()); 
               strcpy(ext[i].dato, valor.c_str() );
               
               if((ext[i].idNid = OBJ_txt2nid(ext[i].id)) == NID_undef)
               {
                  Bitacora->escribePV(BIT_ERROR, "Error en CSrvAC en iniciaExtensiones: Error extension invalida: '%s' en el archivo %s", ext[i].id, ARCH_EXT );
                  break;
               }
               // Verifica si la extension se obtiene del cert. digital y pertenece a est� rango
               if ( datosCertAC && ( ext[i].idNid == NID_crl_distribution_points ||
                                  // ext[i].idNid == NID_info_access || // Por ahora est� atributo lo obtenemos del archivo ExtensionesAC.cfg
                                     ext[i].idNid == NID_authority_key_identifier   ) )
               {
                  if ( !GetExtCertAC(ext[i].idNid, ext[i].dato ) )
                     break;
               }
            }
            else
            {
               Bitacora->escribePV(BIT_ERROR, "Error en CSrvAC en iniciaExtensiones: Error fallo la funcion get(%s, %d, nombre, valor)", seccion, i );
               break;
            }
         }
         if ( !strcmp(EXT_FIEL, seccion))
         {
            data.extFEA   = ext;
            data.n_extFEA = iNumElem;  
         }
         else
         { 
            data.extSEL   = ext;
            data.n_extSEL = iNumElem;
         }
      }
      else
         Bitacora->escribePV(BIT_ERROR, "Error en CSrvAC en iniciaExtensiones:Error memoria insuficiente, new SNames[%d] para obtener: [ %s ]", iNumElem, seccion );
   }
   else
      Bitacora->escribePV(BIT_ERROR, "Error en CSrvAC en iniciaExtensiones: Error la seccion: [ %s ] es invalida  en el archivo:%s.", seccion, ARCH_EXT );

   return (i == iNumElem); // Si procesa bien todos los datos
}
//################################################################################
// Verifica la llave privada contra la llave publica del cert. de la AC
bool CSrvAC::VerificaLlaves()
{
   RSA        *rsaPbKey_cli = NULL;
   uchar      *contenido = NULL;
   int        lcontenido = MAX_TAMCERT;
   ManArchivo archivo;
   bool       ok = false;

   //>>> Obtenemos el RSA del certificado de la AC
   if ( ( rsaPbKey_cli = RSA_new() ) != NULL && ( contenido = new uchar[MAX_TAMCERT] ) != NULL )
   {
      if ( archivo.ProcesaArchivo(data.m_CertAC.c_str(), contenido ,&lcontenido) )
      {
         SGIX509    sgiX509;

         if ( !sgiX509.ObtienePbK(contenido, lcontenido, &rsaPbKey_cli, NULL, (char *)data.m_CertAC.c_str() ) )
         {
            // Ahora ya tenemos las 2 llaves y las comparamos
            SGIRSA ObjSgiRSA;
            if( !ObjSgiRSA.cmpKeys(rsaPbKey_cli, data.m_privKey) == 0)
               Bitacora->escribe(BIT_ERROR, "Error en CSrvAC en VerificaLlaves: Error la llave privada no corresponde con la llave publica del certificado.");
            else
               ok = true;
         }
         else
            Bitacora->escribePV(BIT_ERROR, "Error en CSrvAC en VerificaLlaves: Error al obtener la llave publica del certificado: %s", data.m_CertAC.c_str() );
      }
      else
         Bitacora->escribePV(BIT_ERROR, "Error en CSrvAC en VerificaLlaves: Error al leer el contenido del certificado: %s", data.m_CertAC.c_str() );
   }
   else
      Bitacora->escribe(BIT_ERROR, "Error en CSrvAC en VerificaLlaves: Error memoria insuficiente."); 
      
   if ( rsaPbKey_cli )
      RSA_free(rsaPbKey_cli);
   if ( contenido )
      delete[] contenido;
   //<<<
   if ( ok )
      Bitacora->escribe(BIT_INFO, "CSrvAC.VerificaLlaves: La llave privada corresponde con la llave publica del certificado");
   return ok;
}
//################################################################################
//>>>>>  RORS 20 octubre 2008
bool CSrvAC::GetExtCertAC(int idNid, char *dato)
{
   FILE  *fp;
   X509  *x509 = X509_new();
   unsigned char *dDato = NULL;
   bool  ok = false;

   fp = fopen( (const char*)data.m_CertAC.c_str(), "rb");
   if (!fp)
   {
      Bitacora->escribePV(BIT_ERROR, "Error en CSrvAC en VerificaLlaves: Error no se puede abrir el archivo certificado: %s", data.m_CertAC.c_str() );
      return ok;
   }
   void *aux = NULL;
   BIO *ext_bio  = BIO_new(BIO_s_mem());

   //aux = PEM_read_X509(fp,&x509,NULL,NULL);
   if(!aux)
      aux = d2i_X509_fp(fp,&x509);
   if(aux && ext_bio)
   {
      int iExt = X509_get_ext_by_NID(x509, idNid, -1);
      if ( iExt >= 0 )
      {
         X509_EXTENSION *ext;
         ext = X509_get_ext(x509,iExt);
         if ( ext )
         {
            if ( ( dDato = (uchar*)calloc(ext->value->length + 1, 1) ) != NULL )
            {
               X509V3_EXT_print(ext_bio, ext, 0, 0 );
               BIO_gets(ext_bio, (char *)dDato, ext->value->length);
               //memcpy(dDato, (const char*)ext->value->data, ext->value->length);
               dDato[ext->value->length] = 0;
               strncpy(dato, (const char*)dDato, ext->value->length );
               ok = true; 
            }
         }
      }
      BIO_free(ext_bio);
   }

   if( dDato )
   {
      free( dDato );
      dDato = NULL;
   }
   fclose ( fp ); //delete fp;
   fp = NULL;
   X509_free(x509);

   return ( ok );
}
//<<<<<
//################################################################################
//################################################################################
//################################################################################

#include <sys/shm.h>

// Obtiene la llave privada ANTERIOR de la memoria compartida
bool CSrvAC::getLlavePrivada_ANTERIOR()
{
   int error = 0;
   SGIRSA rsaalg;
   SGIPRIVADA *privada = new SGIPRIVADA;
   RegServPKI *srv;
   char *shmseg;
   int shmid;
   int reshm;
   data.m_privKey = RSA_new();
   if( data.m_privKey)
   {
      shmid = shmget(KEY_SHMEM, 0, 0);
      if(shmid <= 0)
         return  false;
      shmseg = (char*)shmat(shmid, 0, 0);
      srv = new RegServPKI;
      memcpy(srv, shmseg, sizeof(struct reg_serv_pki_st));
      memcpy(privada, srv->idserv->privKey, sizeof(SGIPRIVADA));
      error = sgiErrorBase(rsaalg.getPrivada(privada, data.m_privKey));
      if(error != 0)
         return false;
      reshm = shmdt(shmseg);
      if(reshm < 0)
         return false;
      delete privada;
      delete srv;
      return true;
   }
   return false;
}

