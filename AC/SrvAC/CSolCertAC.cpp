static const char* _CSOLCERTAC_CPP_VERSION_ ATR_USED = "SrvAC @(#)"\
"DSIC10352AC_ 2010-09-03 CSolCertAC.cpp 1.0.4/3";

//#VERSION: 1.1.1
/*##############################################################################
  ###  PROYECTO:              PKI-SAT                                        ###
  ###  MODULO:                Servicio AC                                    ###
  ###                                                                        ###
  ###                                                                        ###
  ###  DESARROLLADORES:       Amilcar Guerrero Zu�iga         AGZ            ###
  ###                                                                        ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       Lunes 5, diciembre del 2005                    ###
  ###                                                                        ###
  ##############################################################################
         1         2         3         4         5         6         7         8
12345678901234567890123456789012345678901234567890123456789012345678901234567890

################################################################################
  VERSION:
     V.1.0.0  (20051130- )    AGZ: Version Original
                              Servicio de Autoridad Certificadora para la PKI-SAT
     V.1.0.1  (20060518-20060518) AGZ: Cons:06001 Se corrige problem�tica con la 
                                      generaci�n y p�rdida de n�meros de serie.
     V.1.0.2  (20060605-20060605) AGZ: Cons:06002 1. Se agrega ciclo para el 
       socket se aumenta el timeout a 5 min. 
       2. Se cambia la l�gica del registro de la operaci�n insertandola con el 
          n�mero de serie vac�o y actualizandolo cuando este sea creado.    
     V.1.0.3  (20081008-20081008) MAML: se elimina la funcion getLlavePrivada()
     V.1.0.4  (20100107-20100107) MAML: se agrega el Pais al Sujeto del Certificado

     V.3.0.3  SrvAC
              (20100826-20100903) GHM:Se realizan las modificaciones para que se asigne la vigencia de 48 meses a FIEL PF
                              y 27 meses a FIEL de PM

#################################################################################*/

#include <DefsGeneracion.h>
//#include <Sgi_MemCompartida.h> //+ GHM (070425)
#include <CSolCertAC.h>

//#################################################################################
#if ORACLE
   #include <CosltORASrvAC.h> 
#else
   #include <CosltSrvAC.h> //+ GNC  Se agrega definici�n de consultas
#endif
//#################################################################################

int CSolCertAC::m_MsgSol = -1;

CSolCertAC::CSolCertAC(CConfigFile& config, MensajesSAT& msgcli, CSSL_Skt* skt, DATA_CERT *dat) :
COperacionAC(config, msgcli, skt ), data(dat)
{
}

CSolCertAC::~CSolCertAC()
{
}

bool CSolCertAC::preProceso()
{
   int error = 0;
   bool ok = false;
   Bitacora->escribePV(BIT_INFO, "    CSolCertAC.preProceso: Inicio del Preproceso para %s.",getNombreOperacion()); 
   
   if (m_MsgSol == -1)
      m_MsgSol = m_MsgCli.tipOperacion();
    
   iniciaVarsSol();
   Bitacora->escribe(BIT_INFO, "CSolCertAC.preProceso: Se iniciaron las variables para la solicitud.");
   
   error = sgiErrorBase(m_MsgCli.getMensaje(SOLCERTAR, m_firmaOp, &m_ifirmaOp, m_numTram, &m_inumTram, m_secOp, &m_isecOp,
         m_cadenaSol, &m_icadenaSol, m_tipoCer, &m_itipoCer, m_byBufferReq, &m_ibyBufferReq));
   if(!error)
   {
      Bitacora->escribePV(BIT_INFO, "CSolCertAC.preProceso: Se obtuvieron los datos del Mensaje(%i)", SOLCERTAR);
      if (procesaCadenaRFC())
      {  
	    Bitacora->escribe(BIT_INFO, "CSolCertAC.preProceso: Se obtuvieron los datos:'RFC, AGC y Nombre'");
         m_tipoCer[m_itipoCer] = 0;
         ok = true;
      }
      else  
	    { 
         Bitacora->escribePV(BIT_ERROR, "Error en CSolCertAC en preProceso: Error al obtener Datos:'RFC, AGC y Nombre'");
         msgCliError('I', "Error en CSolCertAC en preProceso: : Error al obtener el RFC, AGC y nombre.", ERR_SEP_CAD_RFC, eAPL); 
		}
   }
   else
   {
      Bitacora->escribePV(BIT_ERROR, "Error en CSolCertAC en preProceso: Error (%i) al obtener los datos del Mensaje (%i).", error, SOLCERTAR); 
      msgCliError('I', "Error en CSolCertAC en preProceso: Error al obtener los datos del Mensaje.", ERR_SEP_CAD_RFC, eAPL); 
   }
   
   Bitacora->escribePV(BIT_INFO, "    CSolCertAC.preProceso: Fin del Preproceso para %s.",getNombreOperacion());
   return ok;
}

bool CSolCertAC::Proceso()
{
   int error = 0;
   bool ok = false;
   Bitacora->escribePV(BIT_INFO, "    CSolCertAC.Proceso: Inicio del Proceso para %s.",getNombreOperacion()); 
   
   //<<<AGZ: Cons:06002 Cambio de l�gica para registro de las operaciones del servidor
   m_NumSerieOp = " ";
   if (!regOperacionBD())   
      Bitacora->escribePV(BIT_ERROR, "Error en CSolCertAC en Proceso: Error al registrar la operacion %s.",getNombreOperacion());   
   //>>>AGZ: Cons:06002 Fin

   if(!generaSerial())
   {
      Bitacora->escribe(BIT_ERROR, "Error en CSolCertAC en Proceso: Error al generar el n�mero de serie.");
      msgCliError('I', "Error en CSolCertAC en Proceso: Error al generar el n�mero de serie del nuevo Certificado.", ERR_GEN_SERIAL, eAPL);
      return false;
   }
   else m_NumSerieOp = m_SerialGen;
   
   error = generaCertificado();
   if (!error)
   {
      error = sgiErrorBase(m_MsgCli.setMensaje(CERTGENAR, m_rfcOp, strlen(m_rfcOp), m_byBufferCert, 
                 m_ibyBufferCert));
      if (!error)
      {
	    Bitacora->escribe(BIT_INFO, "CSolCertAC.Proceso: Se prepar� el mensaje con 'CERTGENAR' ");
		
         if (!regOperacionBDDet(atoi(m_secOp), CERTGENAR))
            Bitacora->escribe(BIT_ERROR, "Error en CSolCertAC en Proceso: Error al registrar la operacion a detalle.");
          else
         Bitacora->escribe(BIT_INFO, "CSolCertAC.Proceso: Se registro el detalle de la operacion en la BD.");
         error = envia();
         if (!error)
         {
		    Bitacora->escribe(BIT_INFO, "CSolCertAC.Proceso: Se envi� el mensaje a la AR (Mediador)");
			
            //<<<AGZ: Cons:06002 Se agrega un ciclo para el timeout
            do
            {
               error = recibe();
               if ( error == ERR_SKTTIMEOUT)
                  Bitacora->escribePV(BIT_ERROR, "Error en CSolCertAC en Proceso: Error se recibi� Time Out del socket al esperar mensaje CERTREGIES: (%i).", CERTREGIES);
            }while(error == ERR_SKTTIMEOUT);
            //>>>AGZ: Cons:06002 Fin 
            if (!error)
            {
			   Bitacora->escribe(BIT_INFO, "CSolCertAC.Proceso: Se recibi� el mensaje");
               error = procesaMsg();                 
               //<<<AGZ: Cons:06001 Se agrega evaluacion para regresar el numero de serie.
               if (!error && m_MsgSol == SOLCERTAR && m_MsgCli.tipOperacion() == MSGERROR)
               {
                  if(regresaSerial())
	               Bitacora->escribe(BIT_INFO, "CSolCertAC.Proceso: Se elimino el n�mero de serie de la BD.");
	              else
	               Bitacora->escribePV(BIT_ERROR,"Error en CSolCertAC en Proceso: Error al eliminar el n�mero de serie de la BD");
			  
                  Bitacora->escribe(BIT_ERROR, "Error en CSolCertAC en Proceso: Ocurri� un error al intentar registrar el certificado generado en la AR/IES.");
                  ok = true;
               }
               //>>>AGZ: Cons:06001 Fin.
               else if (!error)
                  ok = true;
               else
                  msgCliError('I', "Error en CSolCertAC en Proceso: Error en procesaMsg.", error, eAPL);
            }
            else
			   { Bitacora->escribePV(BIT_ERROR,"Error en CSolCertAC en Proceso: Error al recibir de la AR.", error, eAPL);
                msgCliError('I', "Error en CSolCertAC en Proceso: Error al recibir de la AR.", error, eAPL);
				}
         }
         else 
         {
            if(regresaSerial())
	          Bitacora->escribe(BIT_INFO, "CSolCertAC.Proceso: Se elimino el n�mero de serie de la BD.");
	        else
	          Bitacora->escribePV(BIT_ERROR,"Error en CSolCertAC en Proceso: Error al eliminar el n�mero de serie de la BD");
			
			Bitacora->escribePV(BIT_ERROR,"Error en CSolCertAC en Proceso: Error al enviar el mensaje.(CERTGENAR)", error, eAPL);
            msgCliError('I', "Error en CSolCertAC en Proceso: Error al enviar el mensaje.(CERTGENAR)", error, eAPL);
         }
      }
      else
      {
         if(regresaSerial())
	       Bitacora->escribe(BIT_INFO, "CSolCertAC.Proceso: Se elimino el n�mero de serie de la BD.");
	     else
	       Bitacora->escribePV(BIT_ERROR,"Error en CSolCertAC Proceso: Error al eliminar el n�mero de serie de la BD");
		 
		 Bitacora->escribePV(BIT_ERROR,"Error en CSolCertAC en Proceso: Error al preparar el mensaje.(CERTGENAR)", error, eAPL);
         msgCliError('I', "Error en CSolCertAC en Proceso: Error al preparar el mensaje.(CERTGENAR)", error, eAPL);
      }
   }
   else
   {
      
	  if(regresaSerial())
	   Bitacora->escribe(BIT_INFO, "CSolCertAC.Proceso: Se elimino el n�mero de serie de la BD.");
	  else
	    Bitacora->escribePV(BIT_ERROR,"Error en CSolCertAC en Proceso: Error al eliminar el n�mero de serie de la BD");
		  
      if (error == ERR_EDO_CER_ACT)
         {
		   Bitacora->escribePV(BIT_ERROR,"Error en CSolCertAC en Proceso: Error RFC cuenta con un Certificado Digital activo");
		   msgCliError('N', "Error en CSolCertAC en Proceso: RFC cuenta con un Certificado Digital activo.", error, eAPL);
		 }
		  else 
		  {
			Bitacora->escribePV(BIT_ERROR,"Error en CSolCertAC en Proceso: Error al generar el Certificado Digital");
			msgCliError('I', "Error en CSolCertAC en Proceso: Error al generar el Certificado Digital.", error, eAPL);
		  }
   }
   
  Bitacora->escribePV(BIT_INFO, "    CSolCertAC.Proceso: Fin del Proceso para %s.",getNombreOperacion());
   return ok;   
}

const char* CSolCertAC::getNombreOperacion()
{
   return "Generaci�n de Certificado Digital en la AC.";
}

int CSolCertAC::getTipOper()
{
   if(m_MsgSol == -1)
      m_MsgSol = m_MsgCli.tipOperacion();
   return m_MsgSol;
}

int CSolCertAC::generaCertificado()
{
   int error = 0;
   char correo[256];
   uchar by_SerialTmp[21]; //+ GHM (20100826): Para considerar un cierre de cadena
   SGIX509 x509;
   char nomCN[65];
   map<int, int>::const_iterator itLista; //+ GHM (20100826): V3.0.3
   nomCN[0] = 0;
   strncpy(nomCN, m_nomCont, 64);
   SNames sujeto[5];
   sujeto[0].dato = nomCN;
   sujeto[0].dato[64] = '\0';
   sujeto[0].id = "commonName";
   sujeto[2].dato = nomCN;
   sujeto[2].dato[64] = '\0';
   sujeto[2].id = "organizationName";
   sujeto[1].dato = m_nomCont;
   sujeto[1].id = "name";
   //>- MAML Thu Jan 07 10:11:00 CDT 2010, por Estandares del Certificado de FIEL se agrega el Pais
   sujeto[3].dato = "MX";
   sujeto[3].id = "countryName";   
   memcpy(by_SerialTmp, m_SerialGen, 20);
   by_SerialTmp[20] = 0; //+ GHM (20100826)   
   //>+ GHM (20100826): V3.0.3
   int iVigencia = 0;
   int idVig = atoi(m_tipoCer);
   itLista =  data->vigencia.find(idVig);
   if (itLista == data->vigencia.end())
      return ERR_TAG_NOT_EXIST;
   iVigencia = itLista->second;
   //<+ GHM (20100826)

   if ( !obtieneCorreo(correo))
   {
      Bitacora->escribePV(BIT_ERROR, "Error en CSolCertAC en generaCertificado: Error al obtener el correo");
      return ERR_OBT_CORREO;
   }
   if (!tieneCertActivo() && m_tipoCer[0] == '1')
   {
      Bitacora->escribePV(BIT_INFO, "CSolCertAC.generaCertificado: Certificado a generar: FIEL");
      sujeto[4].dato = correo;;
      sujeto[4].id = "emailAddress";
      Bitacora->escribePV(BIT_INFO, "CSolCertAC.generaCertificado: El correo ID:   %s", sujeto[4].id);
      Bitacora->escribePV(BIT_INFO, "CSolCertAC.generaCertificado: El correo DATO: %s", sujeto[4].dato);
      if ( strlen(m_rfcOp) == 12 )
      {
         idVig = ID_VIGFIELMORAL;
         itLista = data->vigencia.find(idVig);
         if (itLista == data->vigencia.end())
            return ERR_TAG_NOT_EXIST;
         iVigencia = itLista->second;
      }
      Bitacora->escribePV(BIT_DEBUG, "CSolCertAC.generaCertificado: Vigencia: %i", iVigencia);
      error = sgiErrorBase(x509.GenCERTAC((const uchar*)m_byBufferReq, m_ibyBufferReq, (char*)data->m_CertAC.c_str(),
                  //- GHM (20100826): V3.0.3 (uchar*)by_SerialTmp, 20, data->vigencia[atoi(m_tipoCer)], data->m_privKey, data->extFEA,
                  (uchar*)by_SerialTmp, 20, iVigencia, data->m_privKey, data->extFEA, //+ GHM (20100826): V3.0.3
                  data->n_extFEA, (uchar*)m_byBufferCert, &m_ibyBufferCert, sujeto, 5, data->estCertDig));
   }
   else if (m_tipoCer[0] == '2')
   {
      Bitacora->escribePV(BIT_INFO, "CSolCertAC.generaCertificado: Certificado a generar: Sellos");
      error = sgiErrorBase(x509.GenCERTAC((const uchar*)m_byBufferReq, m_ibyBufferReq, (char*)data->m_CertAC.c_str(),
                   //- GHM (20100826): V3.0.3 (uchar*)by_SerialTmp, 20, data->vigencia[atoi(m_tipoCer)], data->m_privKey, data->extSEL,
                   (uchar*)by_SerialTmp, 20, iVigencia, data->m_privKey, data->extSEL, //+ GHM (20100826): V3.0.3
                   data->n_extSEL, (uchar*)m_byBufferCert, &m_ibyBufferCert, sujeto, 3, data->estCertDig));
   }
   else if (m_tipoCer[0] > '2')
   {
      sujeto[3].dato = correo;;
      sujeto[3].id = "emailAddress";

      Bitacora->escribePV(BIT_INFO, "CSolCertAC.generaCertificado: El correo ID:   %s", sujeto[3].id);
      Bitacora->escribePV(BIT_INFO, "CSolCertAC.generaCertificado: El correo DATO: %s", sujeto[3].dato);

      error = sgiErrorBase(x509.GenCERTAC((const uchar*)m_byBufferReq, m_ibyBufferReq, (char*)data->m_CertAC.c_str(),
                  //- GHM (20100826): V3.0.3 (uchar*)by_SerialTmp, 20, data->vigencia[atoi(m_tipoCer)], data->m_privKey, data->extFEA,
                  (uchar*)by_SerialTmp, 20, iVigencia, data->m_privKey, data->extFEA, //+ GHM (20100826): V3.0.3
                  data->n_extFEA, (uchar*)m_byBufferCert, &m_ibyBufferCert, sujeto, 4, data->estCertDig));
   }
   else return ERR_EDO_CER_ACT;
   
   if (error != 0)
      {
	     Bitacora->escribePV(BIT_ERROR, "Error en CSolCertAC en generaCertificado: Error al generar el certificado"); 
	     return ERR_NO_GEN_CERT;
	  }
   
   //>+ ERGL 20080430 Se agrego para contar con mas informaci�n para la operaci�n. 
   Bitacora->escribePV(BIT_INFO, "CSolCertAC.generaCertificado: Se gener� correctamente el certificado solicitado.");
   
   return error;
}

bool CSolCertAC::tieneCertActivo()
{
   string s_EdoTmp;
   if (m_BDAC->consultaReg(QS_EDOCERT, m_rfcOp))
      if(m_BDAC->getValores("s", &s_EdoTmp) && s_EdoTmp == "A")
         {
		   Bitacora->escribePV(BIT_INFO, "CSolCertAC: Tiene un certificado Activo.");
		   return true;
		 }
		 
	Bitacora->escribePV(BIT_INFO, "CSolCertAC: No cuenta con certificado Activo.");
   return false;
}

int CSolCertAC::procesaMsg()
{
   int error = 0;
   switch(m_MsgCli.tipOperacion())
   {
      case CERTREGIES:
      {
         error = creaArchCert();
         if (!error)
         {
            //<<<AGZ: Cons:06002 Registro del n�mero de serie hasta que es exitoso.
            // 12 marzo 2008 <<  Se agrego oper_sec_ar en la condicion ya que en la generacion de sellos actualizaba todos los registros 
            //   con el mismo numero de serie cuando eran mas de dos.
            if(!m_BDAC->ejecutaOper(QU_SOL_OPERACION_SECAR, m_NumSerieOp.c_str(), m_numTram,m_secOp ))
               Bitacora->escribe(BIT_ERROR, "Error en CSolCertAC en procesaMsg: Error al actualizar el n�mero de serie del Certificado en la operaci�n.");
			else
			   Bitacora->escribePV(BIT_INFO, "CSolCertAC.procesaMsg: Se actualizo el n�mero de serie del Certificado en la operaci�n.");
            //>>>AGZ: Cons:06002 Fin
	    		
            if (!regOperacionBDDet(atoi(m_secOp), CERTREGIES))
               Bitacora->escribe(BIT_ERROR, "Error en CSolCertAC en procesaMsg: Error al registrar la operacion a detalle.");
			 else
			   Bitacora->escribePV(BIT_INFO, "CSolCertAC.procesaMsg: Se registro la operaci�n a detalle");
			   
            error = registraCertificado();
            if(!error)
            {
               error = sgiErrorBase(m_MsgCli.setMensaje(CERTREGAC, m_numTram, m_inumTram));
               if(!error)
               {
			      Bitacora->escribePV(BIT_INFO, "CSolCertAC.procesaMsg: Se prepar� el Mensaje con CERTREGAC ");
				  
                  if (!regOperacionBDDet(atoi(m_secOp), CERTREGIES))
                    Bitacora->escribe(BIT_ERROR, "Error en CSolCertAC en procesaMsg: Error al registrar la operacion a detalle.");
			      else
			        Bitacora->escribePV(BIT_INFO, "CSolCertAC.procesaMsg: Se registro la operaci�n a detalle");
		  	   
                  error = envia();
                  if (!error)
                  {
				     Bitacora->escribePV(BIT_INFO, "CSolCertAC.procesaMsg: Se envi� mensaje a la AR");
                     Bitacora->escribePV(BIT_INFO, "CSolCertAC.procesaMsg: Cenrtificado con N�mero Serial: %s ", m_SerialGen);
                     break; 
                  }
                  else
                     Bitacora->escribePV(BIT_ERROR, "Error en CSolCertAC en procesaMsg: Error (%i) Al enviar Mensaje a la AR.");
               }  
               else Bitacora->escribePV(BIT_ERROR, "Error en CSolCertAC en procesaMsg: Error (%i) Al preparar el Mensaje (%i).", error, CERTREGAC);          
            }
            else Bitacora->escribePV(BIT_ERROR, "Error en CSolCertAC en procesaMsg: Error (%i) Al registrar el Certificado en la BD.", error);
         }
         else Bitacora->escribePV(BIT_ERROR, "Error en CSolCertAC en procesaMsg: Error (%i) Al crear el archivo en el repositorio.", error);
         break;
      }
      case MSGERROR:
      {
         char c_tipErr[2], psz_cdgErr[15], psz_descripcion[256];
         int  i_tipErr = sizeof(c_tipErr), i_cdgErr = sizeof(psz_cdgErr), i_descripcion = sizeof(psz_descripcion);
         error = sgiErrorBase(m_MsgCli.getMensaje(MSGERROR, m_firmaOp, &m_ifirmaOp, c_tipErr, &i_tipErr, psz_cdgErr, &i_cdgErr, 
                 psz_descripcion, &i_descripcion));
         if (!error)
         {
            if (!regOperacionBDDet(atoi(m_secOp), m_MsgCli.tipOperacion()))
                  Bitacora->escribe(BIT_ERROR, "Error en CSolCertAC en procesaMsg: Error al registrar la operacion a detalle.");
		    else
			    Bitacora->escribePV(BIT_INFO, "CSolCertAC.procesaMsg: Se registro la operaci�n a detalle");
            break;            
         }
         break;  
      }
   }
   return error;
}

int CSolCertAC::creaArchCert()
{
   int error = 0;
   ManArchivo fd;
   error = sgiErrorBase(pkiRutaCert(data->m_repositorio, m_SerialGen, m_pathCompleto, true));
   if (!error || error == EEXIST)
   {
      if(fd.CreaArchivo((uchar*)m_byBufferCert, m_ibyBufferCert, (char*)m_pathCompleto.c_str()))
         {
		   Bitacora->escribe(BIT_INFO, "CSolCertAC.creaArchCert: Se creo el archivo del Certificado Digital en el repositorio.");
		   return 0;
		 }
      else
      {
         escribeBitacora(eAPL, ERR_NO_CREA_ARCH_CERT, "Error en CSolCertAC en creaArchCert: Error al crear el archivo del Certificado Digital en el repositorio.");
         return ERR_NO_CREA_ARCH_CERT;
      }
   }
   else
   {
      escribeBitacora(eAPL, error, "Error en CSolCertAC en creaArchCert: Error al crear la ruta del archivo del Certificado Digital.");
      return error;
   }
   return error;
}

int CSolCertAC::registraCertificado()
{
   SGIX509 x509;
   int     error = 0;
   char    vigIni[20], vigFin[20], rfcRL[14], curpRL[19], email[256], Curp[19], cveRev[128], rfcTitular[14];
   char    vigIni_[13], vigFin_[13];
   CFecha  fec;
   
   vigIni[0] = 0; vigFin[0] = 0; rfcRL[0] = 0; curpRL[0] = 0; email[0] = 0; Curp[0] = 0; cveRev[0] = 0;vigIni_[0]=0 ; vigFin_[0]=0;

   SNames sujeto[3], atrib[1];
   int snids[3] = {503, 48, 105};
   int satrib[1] = {54};

   error = sgiErrorBase(x509.ProcREQ((const uchar*)m_byBufferReq, m_ibyBufferReq, sujeto, atrib, NULL, snids, 3, satrib, 1));
   if (!error)
   {
      error = sgiErrorBase(x509.ProcCERT((const uchar*)m_byBufferCert, m_ibyBufferCert, NULL, NULL, (uchar*)vigIni_, (uchar*)vigFin_));
      if (!error)
      {
	    Bitacora->escribePV(BIT_INFO, "CSolCertAC.registraCertificado: Se extrajeron datos del Certificado Digital en la BD.");
         if (sujeto[0].dato != NULL)
         { 
            if (strlen(sujeto[0].dato) > 13)
               procesaDatoRL(sujeto[0].dato, rfcTitular, rfcRL);
            else sprintf(rfcTitular, "%s", sujeto[0].dato);
         }
         if (sujeto[2].dato != NULL)
         {
            if (strlen(sujeto[2].dato) > 18)
               procesaDatoRL(sujeto[2].dato, Curp, curpRL);
            else sprintf(Curp, "%s", sujeto[2].dato);
         }
      
	    fec.ConvCadFtoCertTOBD((string)vigIni_, vigIni);
         fec.ConvCadFtoCertTOBD((string)vigFin_, vigFin);
   
         if (sujeto[1].dato != NULL)
            strcpy(email, sujeto[1].dato);
         else email[0] = '\0';

         char pszNombreTmp[1024*2];	
		 string cVacia = "";
         pszNombreTmp[0] = '\0';
         m_BDAC->prepCadDelim(1, (const char*)m_nomCont, pszNombreTmp);
		 
		 Bitacora->escribePV(BIT_INFO, "CSolCertAC.registraCertificado: Se procesaron datos del certificado.");
		 
		 #if ORACLE    //Registra en nueva BD
		 
			if (m_BDAC->ejecutaOper(QI_CERTIFICADO, m_SerialGen,m_tipoCer,m_rfcOp,Curp,pszNombreTmp,sujeto[1].dato,rfcRL,curpRL,
			 vigI.c_str(),vigF.c_str(),m_numTram,m_secOp,atrib[0].dato)) 
			 {
				Bitacora->escribePV(BIT_INFO, "CSolCertAC.registraCertificado: Se registr� correctamente el Certificado en la BD. (RFC: %s, TIPO: %s).", m_rfcOp, m_tipoCer);
				return 0;
			 }
			 else 
			 {
				escribeBitacora(eAPL, ERR_REG_CERT_BD, "En CSolCertAC en registraCertificado: Error al registrar el Certificado Digital en la BD.");
				Bitacora->escribePV(BIT_INFO, "CSolCertAC.registraCertificado: Se continua con la operaci�n normalmente ya que el Certificado"
											  " se envio a la IES y su registro fue correcto. Revisar el Manejador"
											  " de la Base de Datos o el archivo SrvAC.log. El numero de serie es: %s"
											  " y debe registrarse manualmente en la Base de Datos.", m_NumSerieOp.c_str());
				return 0;
			 }
		 
		 #else       
		    
		    if (m_BDAC->ejecutaOper(QI_CERTIFICADO, m_SerialGen, m_numTram, m_secOp, m_tipoCer, m_rfcOp, Curp, 
             pszNombreTmp, email, rfcRL, curpRL, vigIni, vigFin, atrib[0].dato))
			 {
				//>+ ERGL 20080430 Se agreg� para contar con mas informaci�n para la operaci�n.
				Bitacora->escribePV(BIT_INFO, "CSolCertAC.registraCertificado: Se registr� correctamente el Certificado en la BD. (RFC: %s, TIPO: %s).", m_rfcOp, m_tipoCer);
				return 0;
			 }
			 else 
			 {
				escribeBitacora(eAPL, ERR_REG_CERT_BD, "En CSolCertAC en registraCertificado: Error al registrar el Certificado Digital en la BD.");
				Bitacora->escribePV(BIT_INFO, "CSolCertAC.registraCertificado: Se continua con la operaci�n normalmente ya que el Certificado"
											  " se envio a la IES y su registro fue correcto. Revisar el Manejador"
											  " de la Base de Datos o el archivo SrvAC.log. El numero de serie es: %s"
											  " y debe registrarse manualmente en la Base de Datos.", m_NumSerieOp.c_str());
				return 0;
			 }
		 #endif         
      }
      else
      {
          escribeBitacora(eAPL, ERR_PROC_CERT_BD, "Error en CSolCertAC en registraCertificado: Error al extraer datos del Certificado Digital en la BD.");
          return error;
      }
   }
   return 0;
}

bool CSolCertAC::procesaCadenaRFC()
{
   bool ok = false;
   char *rfc = NULL, *agc = NULL, *nom = NULL;
   rfc = strtok(m_cadenaSol, "|");
   if(rfc)
   {
      sprintf(m_rfcOp, "%s", rfc);
      nom = strtok(NULL, "|");
      if(nom)
      {
         sprintf(m_nomCont, "%s", nom);
         agc = strtok(NULL, "|");
         if(agc)
         {
            sprintf(m_agcCve, "%s", agc);         
            ok = true;
         }
		 else
            Bitacora->escribePV(BIT_ERROR, "Error en CSolCertAC en procesaCadenaRFC: Error al obtener AGC");
      }
	  else
       Bitacora->escribePV(BIT_ERROR, "Error en CSolCertAC en procesaCadenaRFC: Error al obtener Nombre");
   }
   else
     Bitacora->escribePV(BIT_ERROR, "Error en CSolCertAC en procesaCadenaRFC: Error al obtener RFC");
	 
   return ok;
}

bool CSolCertAC::generaSerial()
{
   bool ok = false;
   if(m_tipoCer[0] > '2')
   
     #if ORACLE
      ok = m_BDAC->ejecutaFuncion(SP_GEN_NUMSERIE, ":b", "%d", 1); 
	 #else
	   ok = m_BDAC->ejecutaSP(SP_GEN_NUMSERIE, "%i", 1); 
	 #endif	   
	   
   else 
     #if ORACLE
      ok = m_BDAC->ejecutaFuncion(SP_GEN_NUMSERIE, ":b", "%d", 0);
     #else
	  ok = m_BDAC->ejecutaSP(SP_GEN_NUMSERIE, "%i", 0); 
	 #endif
   
   if(ok)
     {
	   ok = m_BDAC->getValores("b", m_SerialGen);
	   Bitacora->escribePV(BIT_INFO, " CSolCertAC.generaSerial: Se genero el n�mero de serie: %s.",m_SerialGen); 
	 }
   return ok;
}

void CSolCertAC::armaFechaBD(char *vi, char *vf)
{
   char anio[5];
   struct tm *tiempo;
   time_t t = time(NULL);
   tiempo = gmtime(&t);
   sprintf(anio, "%i", tiempo->tm_year+1900);

   anio[2] = vi[0]; 
   anio[3] = vi[1];

   vigI = anio+(string)"-"+vi[2]+vi[3]+(string)"-"+vi[4]+vi[5]+(string)" "+vi[6]+vi[7]+(string)":"+vi[8]+vi[9]+(string)":"+vi[10]+vi[11];

   anio[2] = vf[0]; 
   anio[3] = vf[1];

   vigF = anio+(string)"-"+vi[2]+vi[3]+(string)"-"+vi[4]+vi[5]+(string)" "+vi[6]+vi[7]+(string)":"+vi[8]+vi[9]+(string)":"+vi[10]+vi[11]; 
}

void CSolCertAC::iniciaVarsSol()
{
   iniciaVarsTram();
   m_cadenaSol[0]    = 0;
   m_tipoCer[0]      = 0;
   m_byBufferReq[0]  = 0;
   m_byBufferCert[0] = 0;
   m_nomCont[0]      = 0;

   m_icadenaSol    = sizeof(m_cadenaSol);
   m_itipoCer      = sizeof(m_tipoCer);
   m_ibuffReq      = sizeof(m_byBufferReq);
   m_ibyBufferCert = sizeof(m_byBufferCert);
   m_ibyBufferReq  = sizeof(m_byBufferReq);
   m_inomCont      = sizeof(m_nomCont);
}

bool CSolCertAC::regresaSerial()
{
   bool ok = false;
   if(m_tipoCer[0] > '2')
      ok = m_BDAC->ejecutaSP(SP_REG_NUM, "%i", 1);
   else ok = m_BDAC->ejecutaSP(SP_REG_NUM, "%i", 0);
   return ok;
}

void CSolCertAC::procesaDatoRL(char *cadOrig, char *Cad1, char *Cad2)
{
   char cad1[30], cad2[30], *cad = NULL;
   int i_cad1 = 0, i_cad2 = 0, i = 0;

   cad = strtok(cadOrig, "/");
   if(cad)
   {
      strcpy(cad1, cad);
      cad = NULL;
      cad = strtok(NULL, "/");
      if (cad)
      {
         strcpy(cad2, cad);

         i_cad1 = strlen(cad1);
         for(i = 0; i < i_cad1; i++)
         {
            if (cad1[i] == ' ' && i == 0)
               cad1[i] = cad1[i + 1];
            else if (cad1[i] != ' ')
               cad1[i] = cad1[i];
            else if (cad1[i] == ' ' && i != 0)
               cad1[i] = 0;
            else
                continue;
         }
         if(cad1[0] == ' ')
            Cad1[0] = '\0';
         else
            strcpy(Cad1, cad1);
         
         i_cad2 = strlen(cad2);
         for(i = 0; i < i_cad2; i++)
         {
            if (cad2[i] == ' ' && i == 0)
               cad2[i] = cad2[i + 1];
            else if (cad2[i] != ' ' && i != 0)
               cad2[i] = cad2[i + 1];
            else if (cad2[i] == ' ' && i != 0)
               cad2[i] = 0;
            else
               continue;
         }
         if(cad2[0] == 0)
            Cad2[0] = '\0';
         else
            strcpy(Cad2, cad2);
      }
   }
}

bool   CSolCertAC::obtieneCorreo(char* correo)
{
   SNames sujeto[1], atrib[1];
   int snids[1] = {48};
   int satrib[1] = {54};
   SGIX509 x509;

   int error = sgiErrorBase(x509.ProcREQ((const uchar*)m_byBufferReq, m_ibyBufferReq, sujeto, atrib, NULL, snids, 1, satrib, 1));
   if (!error)
   {
      Bitacora->escribePV(BIT_INFO, "CSolCertAC.obtieneCorreo: ObtieneCorreo ProcREQ: %i", error);
      if (sujeto[0].dato != NULL)
      {
            strcpy(correo, sujeto[0].dato);
            Bitacora->escribePV(BIT_INFO, "CSolCertAC.obtieneCorreo: ObtieneCorreo correo est: %s", sujeto[0].dato);
      }
      else correo[0] = '\0';
	 Bitacora->escribePV(BIT_INFO, "CSolCertAC.obtieneCorreo: Se obtuvo el Correo: %i", error); 
      return true;
   }
   
   return false;


}

