#ifndef _CCONEXIONAC_H_
#define _CCONEXIONAC_H_
static const char* _CCONEXIONAC_H_VERSION_ ATR_USED = "SrvAC @(#)"\
"DSIC09041AC_ 2007-12-13 CConexionAC.h 1.1.0/1";

//#VERSION: 1.1.0

#define TERMINAR	1501

#define TAG_OP          "[OP_SAT_AC]"

//#define ERR_NO_ENVIA    1
#define ERR_ASG_MEM	   2
#define ERR_NO_GENSESNO	3
#define ERR_MSG_ERROR	4


class CConexionAC : public COperacionAC
{
   private:
      char m_numAR[5], m_nSerie[21], m_NoSesion[39], m_firma[684];
      int m_inumAR, m_ifirma, m_inSerie, m_iNoSesion;

      static int m_MsgTipo;

      string m_CertAC, m_KeyAC, m_PassAC, m_ARCert;
      
      bool operacion;

      int  iniciaVarsCon();
      void terminaVarsCon();
      int  GenSesionNo(int bits, char *sesionNo, int *res);
      int  procesaMsg();
      int  getValoresMsg();
      int  ConectaARCli();

      bool preProceso();
      bool Proceso();
      const char* getNombreOperacion();
      int getTipOper();

   public:
      CConexionAC(CConfigFile& config, MensajesSAT& msgcli, CSSL_Skt* skt);
      ~CConexionAC();
};

#endif
