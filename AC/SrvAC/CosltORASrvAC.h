#ifndef _COSLTSRV_H_
#define _COSLTSRV_H_

static const char* _COSLTSRV_H_VERSION_ ATR_USED = "@(#) SrvAC ( L : DSIC10392AR_ : ConsltSrvAC.h : 1.0.0 : 10/10/14)";

//#VERSION: 1.1.0
/*##############################################################################
  ###  PROYECTO:              PKI-SAT                                        ###
  ###  MODULO:                Servicio AC                                    ###
  ###                                                                        ###
  ###                                                                        ###
  ###  DESARROLLADORES:       Guadalupe Negrete Castro         GNC           ###
  ###                                                                        ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       Viernes 10, octubre del 2014                   ###
  ###                                                                        ###
  ##############################################################################
         1         2         3         4         5         6         7         8
12345678901234567890123456789012345678901234567890123456789012345678901234567890

################################################################################
  VERSION:
     V.1.0.0  (20141010- )    GNC: Version Original
                              Consutas para el Servicio de Autoridad Certificadora PKI-SAT
  

#################################################################################*/



//#VERSION: 1.1.0


#define QU_SOL_OPERACION        "update SEG_PKI.SEGT_SOL_OPERACION_AC set NOSERIE = '%s' where SOLOPERCVE = '%s';"
#define QI_CERTIFICADO          "insert into SEG_PKI.SEGT_CERTIFICADO values('%s','%s','A','%s','%s','%s','%s','%s','%s','%s','%s',SYSTIMESTAMP,'','','','','','%s','%s','%s',SYSTIMESTAMP);"
#define QS_CERT_1ACT_RFC        "select NOSERIE from SEG_PKI.SEGT_CERTIFICADO where RFC = '%s' AND TIPCERCVE=1 AND EDOCER='A' order by NOSERIE desc;"
#define QU_CERT_REVO            "update SEG_PKI.SEGT_CERTIFICADO set SOLOPERCVE = '%s', OPERSECAR = '%s', EDOCER = 'R', VIGFIN = '%s', FECREG = '%s' where NOSERIE = '%s' and RFC = '%s';"
#define QS_VIGENCIA_CERT        "SELECT TIPCERCVE, VIG FROM SEG_PKI.SEGC_TIP_CER;"
#define QS_EDOCERT              "select EDOCER from SEG_PKI.SEGT_CERTIFICADO where RFC = '%s' and TIPCERCVE = 1 order by NOSERIE desc;"
#define QU_SOL_OPERACION_SECAR  "update SEG_PKI.SEGT_SOL_OPERACION_AC set NOSERIE = '%s' where SOLOPERCVE = '%s' and OPERSECAR ='%s';"
#define QS_CVEREV_CERT          "select CVEREV from SEG_PKI.SEGT_CERTIFICADO where RFC = '%s' and NOSERIE = '%s';"	 
#define QS_EDO_VIG_SOLOPCERT    "select EDOCER,VIGFIN,SOLOPERCVE from SEG_PKI.SEGT_CERTIFICADO where RFC = '%s' and NOSERIE = '%s';"
#define QU_CERT_ACT			    "update SEG_PKI.SEGT_CERTIFICADO set EDOCER = 'A', VIGFIN = '%s', SOLOPERCVE = '%s' where RFC = '%s' and NOSERIE = '%s';"

			 
#define SP_GEN_NUMSERIE		        "SEG_PKI.segsf_gennumserie"
#define SP_REG_NUM					"SEG_PKI.segsp_regnum"
#define SP_REG_SOLOPER				"SEG_PKI.segsf_regsoloper"
#define SP_REG_SOLOPERDET 			"SEG_PKI.segsp_regoperdet"






#endif
