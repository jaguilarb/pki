static const char* _COPERACIONAC_CPP_VERSION_ ATR_USED = "SrvAC @(#)"\
"DSIC10352AC_ 2010-09-03 COperacionAC.cpp 1.1.0/2";

//#VERSION: 1.1.0
/*##############################################################################
  ###  PROYECTO:              PKI-SAT                                        ###
  ###  MODULO:                Servicio AR                                    ###
  ###                                                                        ###
  ###                                                                        ###
  ###  DESARROLLADORES:       Amilcar Guerrero Zu�iga         AGZ            ###
  ###                                                                        ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       Lunes 5, diciembre del 2005                    ###
  ###                                                                        ###
  ##############################################################################
         1         2         3         4         5         6         7         8
12345678901234567890123456789012345678901234567890123456789012345678901234567890

################################################################################
  VERSION:
     V.1.0.0  (20051130- )    AGZ: Version Original
                              Servicio de Autoridad Registradora para la PKI-SAT

     V.3.0.3  SrvAC
              (20100826-20100903) GHM:Se realizan las modificaciones para que se asigne la vigencia de 48 meses a FIEL PF
                              y 27 meses a FIEL de PM


#################################################################################*/

#include <DefsGeneracion.h>

//##################################################################################
//+ GNC  Se agrega definici�n de consultas
#if ORACLE
   #include <CosltORASrvAC.h> 
#else
   #include <CosltSrvAC.h> 
#endif

//##################################################################################
COperacionAC::COperacionAC(CConfigFile& config, MensajesSAT& msgcli, CSSL_Skt* skt) :
   m_Configuracion(config), m_MsgCli(msgcli), m_SktProc(skt)
{
   m_BDAC = NULL;
}
//##################################################################################
COperacionAC::~COperacionAC()
{
   if (m_BDAC)
      delete m_BDAC;
   m_BDAC = NULL;
}
//##################################################################################
bool COperacionAC::Operacion()
{
   bool ok = false;
   Bitacora->escribePV(BIT_INFO, "COperacionAC.Operacion: Inicio Operaci�n: %s", getNombreOperacion());
   if (conectaBD())
   {
      if (preProceso())
      {
         ok = Proceso();
      }
	  else
	    Bitacora->escribePV(BIT_ERROR, "Error en COperacionAC en Operacion: Error al ejecutar el Preproceso.");
		
      desconectaBD();
   }
   Bitacora->escribePV(BIT_INFO, "COperacionAC.Operacion: Fin Operaci�n: %s", getNombreOperacion());

   return ok;
}
/*##############################################################################
   PROPOSITO: Esta funcion establece la conexion con la BD.

   PREMISA:   Pasar la constante enum DARIO o AR_DB.
   EFECTO:    No tiene efecto sobre el ambiente.
   ENTRADAS:  id_db: Identificador de la base de datos a la que se va a conectar
   SALIDAS:   Regresa: 0 -> Exito o un codigo de error.
#################################################################################*/

bool COperacionAC::conectaBD()
{
   string s_srv, s_db, s_usr, s_paswd, s_role, sError;


   CBD* &bd = m_BDAC;
   
   if (bd)
   {
      sError = string("Error en COperacionAC en conectaBD: Error al abrir BD, ya se encuentra abierta la conexion a la BD.");
      escribeBitacora(eAPL, ERR_NOT_DESENCRYPT_PWD, sError.c_str());
      return false; 
   }
   
   if (getVarConBD(&s_srv, &s_db, &s_usr, &s_paswd, &s_role) != 0)
   {
      sError = string("Error en COperacionAC en conectaBD: Error al obtener valores de conexion a la BD.");
      escribeBitacora(eAPL, ERR_NOT_DESENCRYPT_PWD, sError.c_str());
      return false;
   }
   
   if (!procesaPassword(s_paswd, &s_paswd))
   {
      sError = string("Error en COperacionAC en conectaBD: Error al desencriptar el password de conexion a la BD.");
      escribeBitacora(eAPL, ERR_NOT_DESENCRYPT_PWD, sError.c_str());
      return false; 
   }
  
   bd = new CBD(Bitacora);   
   bool ok = bd != NULL;
   
 #if ORACLE 
 //usuario,pass,host,puerto,sid
   ok = ok && bd->setConfig(s_usr.c_str(), s_paswd.c_str(), s_srv.c_str(), s_db.c_str(), s_role.c_str());  
 #else   
   ok = ok && bd->setConfig(s_srv.c_str(), s_db.c_str(), s_usr.c_str(), s_paswd.c_str(), s_role.c_str());   
 #endif 
 
   ok = ok && bd->conecta();
   
   if (!ok)
   {
      sError = string("Error en COperacionAC en conectaBD: Error al conectarse a la BD.");
      escribeBitacora(eAPL, ERR_NOT_DESENCRYPT_PWD, sError.c_str());
      return false; 
   }
   
   return true;  
}
/*##############################################################################
   PROPOSITO: Esta funcion obtiene los valores de las variables para la 
              conexion con la BD, las variables que solicita son: NOMBRE
              USUARIO, PASSWROD, SERVIDOR, ROL. 
              
   PREMISA:   El apuntador global a la clase SrvAR debe tener las variables
              del archivo de configuracion en memoria.
   EFECTO:    No tiene efecto sobre el ambiente.
   ENTRADAS:  s_id: Identificador de la base de datos a la que se va a conectar
   SALIDAS:   No tiene valores de retorno.
#################################################################################*/

int COperacionAC::getVarConBD(string* s_srv, string* s_db, string* s_usr, string* s_pswd, string* s_role)
{
   int errVars = 0;

  #if ORACLE
       
	   errVars = m_Configuracion.getValorVar("[BD_SAT_BD_ORA]", "usr", s_usr);  //USUARIO
	   if(errVars != 0)
		  return errVars;
	   errVars = m_Configuracion.getValorVar("[BD_SAT_BD_ORA]", "pwd"  , s_pswd);  //PASSWORD
	   if(errVars != 0)
		  return errVars;
	   errVars = m_Configuracion.getValorVar("[BD_SAT_BD_ORA]", "host" , s_srv);  //HOST
	   if(errVars != 0)
		  return errVars;
	   errVars = m_Configuracion.getValorVar("[BD_SAT_BD_ORA]", "port", s_db);   //PUERTO
	   if(errVars != 0)
		  return errVars;
	   errVars = m_Configuracion.getValorVar("[BD_SAT_BD_ORA]", "sid" , s_role);   //SID
	   if(errVars != 0)
		  return errVars;  
  #else
	   errVars = m_Configuracion.getValorVar("[BD_SAT_BD]", "EQ", s_srv);
	   if(errVars != 0)
		  return errVars;
	   errVars = m_Configuracion.getValorVar("[BD_SAT_BD]", "BD"  , s_db);
	   if(errVars != 0)
		  return errVars;
	   errVars = m_Configuracion.getValorVar("[BD_SAT_BD]", "US" , s_usr);
	   if(errVars != 0)
		  return errVars;
	   errVars = m_Configuracion.getValorVar("[BD_SAT_BD]", "PW", s_pswd);
	   if(errVars != 0)
		  return errVars;
	   errVars = m_Configuracion.getValorVar("[BD_SAT_BD]", "ROLE" , s_role);
	   if(errVars != 0)
		  return errVars;
  #endif
	  
	  
   return errVars;
}

/*##############################################################################
   PROPOSITO: Esta funcion desencripta el password de la BD.

   PREMISA:   Pasar el password encriptado.
   EFECTO:    No tiene efecto sobre el ambiente.
   ENTRADAS:  s_pwdEnc: Password encriptado
   SALIDAS:   Regresa: true -> Exito o false error.
#################################################################################*/

bool COperacionAC::procesaPassword(string s_pwdEnc, string *s_pwdDes)
{
   bool b_resOperacion = true;
   int i_pwdSalida = s_pwdEnc.size();
   char *psz_pwdSalida = new char[i_pwdSalida];
   if (!desencripta((uint8*) s_pwdEnc.c_str(), i_pwdSalida, (uint8*) psz_pwdSalida, &i_pwdSalida))
      b_resOperacion = false;
   else
      s_pwdDes->assign(psz_pwdSalida, i_pwdSalida);
   delete[] psz_pwdSalida;
   return b_resOperacion;
}

/*##############################################################################
   PROPOSITO: Este metodo se utiliza para escribir a la bitacora

   PREMISA:   
   EFECTO:    
   ENTRADAS:  
   SALIDAS:   Regresa: 0 -> Exito o un codigo de error.
#################################################################################*/
intE COperacionAC::escribeBitacora(int i_apl, int i_cdgErr, const char *psz_descripcion)
{
   intE errorOp = 0;    
   errorOp = sgiError(i_apl, i_cdgErr);
   Bitacora->escribe(BIT_ERROR, errorOp, psz_descripcion);
   return errorOp;
}

/*##############################################################################
   PROPOSITO: Este metodo se encarga de cerrar la conexion con la BD.

   PREMISA:   
   EFECTO:    
   ENTRADAS:  
   SALIDAS:   Regresa: 0 -> Exito o un codigo de error.
#################################################################################*/
void COperacionAC::desconectaBD()
{
   m_BDAC->desconecta();
   delete m_BDAC;
   m_BDAC = NULL;
}
/*##############################################################################
   PROPOSITO: Esta funcion envia informacion por medio de un socket o fifo.

   PREMISA:   Pasar la constante enum 
   EFECTO:    No tiene efecto sobre el ambiente.
   ENTRADAS:  i_destino: Identificador del destino del envio.
   SALIDAS:   Regresa: 0 -> Exito o un codigo de error.
#################################################################################*/
int COperacionAC::envia()
{
   int error = 0;
   error = m_SktProc->Envia(m_MsgCli.buffer, m_MsgCli.tamMensaje());
   if (error)
      error = sgiErrorBase(escribeBitacora(eAPL, sgiErrorBase(error), "Error en COperacionAC en envia: Error al enviar a la AR.")); 
   return error;
}

/*##############################################################################
   PROPOSITO: Esta funcion recibe informacion de un socket o un fifo.

   PREMISA:   Pasar la constante AR_MEDIADOR o CERTISAT para que haga el envio.
   EFECTO:    No tiene efecto sobre el ambiente.
   ENTRADAS:  id_origen: Identificador del origen del que se recibe
   SALIDAS:   Regresa: 0 -> Exito o un codigo de error.
#################################################################################*/
int COperacionAC::recibe()
{
   int error = 0; 
   //>- int i_buffMen = TAM_DATOS; //>- ERGL mi� sep 12 13:41:38 CDT 2007
   //>- error = m_SktProc->Recibe(m_MsgCli.buffer, i_buffMen); //>- ERGL mi� sep 12 13:11:51 CDT 2007
   m_MsgCli.Recibe(m_SktProc);
   if(error != 0)
      error = sgiErrorBase(escribeBitacora(eAPL, sgiErrorBase(error), "Error en COperacionAC en recibe: Error al recibir del socket de comunicacion."));
   return error;
}

/*##############################################################################
   PROPOSITO: Esta funcion procesa un mensaje de error contenido en el 
              objeto de MensajesSAT.

   PREMISA:   Que ya se haya recibido el mensaje con el objeto m_mensajesMed.
   EFECTO:    No tiene efecto sobre el ambiente.
   ENTRADAS:  
   SALIDAS:   Regresa: 0 -> Exito o un codigo de error.
              Escribe en cada variable el contenido del mensaje desglosado.
#################################################################################*/

int COperacionAC::getMsgErr(char& psz_tipError, char *psz_cdgErr, int i_cdgErr, char *psz_desc, int i_desc)
{
   intE errOp = 0;
   char by_firma[684], by_tipError[30], by_cdgErr[30], by_desc[255];
   int  i_firma = sizeof(by_firma), i_tipError = sizeof(by_tipError);

   psz_cdgErr = psz_cdgErr;
   psz_desc   = psz_desc;
   errOp = m_MsgCli.getMensaje(MSGERROR, by_firma, &i_firma, &psz_tipError, &i_tipError,
                               by_cdgErr, &i_cdgErr, by_desc, &i_desc);
   if(errOp != 0)
      return sgiErrorBase(escribeBitacora(eAPL, sgiErrorBase(errOp), "Error en COperacionAC en getMsgErr: Error en getMensaje al procesar mensaje de error."));
   else return 0;
}

/*##############################################################################
   PROPOSITO: Crea el mensaje de error y lo coloca en el objeto de 
              MensajesSAT.

   PREMISA:   
   EFECTO:    No tiene efecto sobre el ambiente.
   ENTRADAS:  Todos los parametros son de entrada y representan el contenido
              de lo que se va a ainsertar al mensaje y el tamanio del contenido.
   SALIDAS:   Regresa: 0 -> Exito o un codigo de error.
#################################################################################*/

int COperacionAC::setMsgErr(char psz_tipError, const char *psz_cdgErr, const char *psz_desc)
{
   intE errOp = 0;
   errOp = m_MsgCli.setMensaje(MSGERROR, &psz_tipError, 1, psz_cdgErr, strlen(psz_cdgErr), 
                               psz_desc, strlen(psz_desc));
   if(errOp != 0)
      return sgiErrorBase(escribeBitacora(eAPL, sgiErrorBase(errOp), "Error en COperacionAC en setMsgErr: Error en setMensaje al crear mensaje de error."));
   return 0;
}

/*##################################################################################
 
    PROPOSITO: Generar una operaci�n.
    PREMISA  : Ya estar conectado a la BD.
    EFECTO   :
    ENTRADAS : cTip     :  Tipo de Opeaci�n a generar.
               iTipOper :  El N�mero de Mensaje ejemplo : SOLCERT.
               cAgcCve  :  Clave del Agente , RFC , Recortado.
    SALIDAS  : cOper    :  N�mero de operaci�n generado.
###################################################################################*/

void COperacionAC::iniciaVarsTram(void)
{
   m_ifirmaOp   = sizeof(m_firmaOp);
   m_irfcOp     = sizeof(m_rfcOp);
   m_isecOp     = sizeof(m_secOp);
   m_iagcCve    = sizeof(m_agcCve); 
   m_inumTram   = sizeof(m_numTram);
   
   m_firmaOp[0] = 0;
   m_rfcOp[0]   = 0;
   m_secOp[0]   = 0;
   m_agcCve[0]  = 0;
   m_numTram[0] = 0;
}

/*##################################################################################

    PROPOSITO: Termina las variables de tramite.
    PREMISA  :
    EFECTO   :
    ENTRADAS :
    SALIDAS  :
###################################################################################*/

void COperacionAC::terminaVarsTram(void)
{
   m_ifirmaOp     = 0;
   m_irfcOp     = 0;
   m_isecOp     = 0;
   m_iagcCve    = 0;
   m_inumTram   = 0;

   m_firmaOp[0] = 0;
   m_rfcOp[0]   = 0;
   m_secOp[0]   = 0;
   m_agcCve[0]  = 0;
   m_numTram[0] = 0;
}


/*##############################################################################
   PROPOSITO: Este metodo se utiliza para concatenar el nombre de la clase 
              que ejecuta el mensaje de error msgCliError().
              sólo si es requerido en la Bitacora, de no ser requerido
              envía el mensaje de error sin el nombre de la clase.
              
   PREMISA:   El formato de error en la bit�cora se espera recibir con el nombre 
              de la clase y el m�todo donde se ejecut�, seguido del caracter  ':'
              ejemplo: COperacion Metodo1: "Error "
   EFECTO:    
   ENTRADAS:  Si el entero es 1 coloca el nombre de la clase, si es diferente
              elimina el nombre de la clase.
   SALIDAS:   Regresa: un Char con o sin el nombre de la clase.
#################################################################################*/
const char* COperacionAC::SeparaNombreClase(const char *QuitarClase, int NumeroQuitarClase)
{
     
     char DosPuntos=':';
     char *apuntador=NULL;

     apuntador=strchr(QuitarClase,DosPuntos);
     if(apuntador==NULL)
        return(QuitarClase);
        
     apuntador++;
     
     //Coloca el nombre de la clase
     if(NumeroQuitarClase==1)
     {
       QuitarClase=QuitarClase;
       return(QuitarClase);
       
     //Elimina el nombre de la clase      
       }else{
             //apuntador++;
             QuitarClase=apuntador;
             return(QuitarClase);
             }
}



/*##################################################################################

    PROPOSITO: Envia mensaje de Error al CERTISAT, hace el registro del error en la
       base de datos y escribe en la bitacora.
    PREMISA  : Los elementos de BD y de Mensajes deben estar iniciados.
    EFECTO   :
    ENTRADAS : Entero de referencia a la aplicacion en donde sucedio el error, error
       local de la aplicacion, mensaje, medio 'P', 'O'.
    SALIDAS  : regresa false.
###################################################################################*/

bool COperacionAC::msgCliError(char c_tipErr, const char* psz_msg, int i_error, eIdLib i_apl)
{
   int error = 0;
   bool ok = false;
   char psz_error[20];
   string s_MsgBitacora = psz_msg;
   
   
   int NumeroClase=0;
   //Para sobreescribir el Nombre de la Clase 
   const char *psz_msgDos=psz_msg;
  
   i_apl = i_apl;
   sprintf(psz_error, "%d", i_error);
   s_MsgBitacora + " Error: (" + psz_error + ")";
   Bitacora->escribe(BIT_ERROR, i_error, s_MsgBitacora.c_str());
   switch(c_tipErr)
   {
      case 'I':
      default:
      {
         //const char msgCliTxt[] = "Error en la solicitud, favor de intentarlo de nuevo,\n" + psz_msg;
         const char msgCliTxt[] = "Error en la solicitud, favor de intentarlo de nuevo,\n";                                  ;
         error = setMsgErr(c_tipErr, psz_error, msgCliTxt);
         break;
      }
      case 'N':
      {
         //Eliminar el nombre de la clase
         NumeroClase=2;
         psz_msg=SeparaNombreClase(psz_msgDos,NumeroClase);      
      
         error = setMsgErr(c_tipErr, psz_error, psz_msg);
         break;
      }
   }
   if (!error)
   {
      error = envia();
      if (!error)
         ok = true;
      else escribeBitacora(eSGISSL, error, "En COperacionAC.msgCliError: Error al enviar mensaje de Error al CERTISAT.");
   }
   return ok;
}

/*##################################################################################

    PROPOSITO: Envia mensaje de Error al CERTISAT, hace el registro del error en la
       base de datos y escribe en la bitacora.
    PREMISA  : Los elementos de BD y de Mensajes deben estar iniciados.
    EFECTO   :
    ENTRADAS : Entero de referencia a la aplicacion en donde sucedio el error, error
       local de la aplicacion, mensaje, medio 'P', 'O'.
    SALIDAS  : regresa false.
###################################################################################*/

bool COperacionAC::regOperacionBD()
{
   //<<<AGZ: Cons:06002;06003 Se agrega la evaluaci�n al store procedure
   bool ok = false;
   int i_resRegOp = -1;
   Bitacora->escribePV(BIT_DEBUG,"COperacionAC.regOperacionBD: Datos a registrar: '%s', %d, %d, '%s', '%s', '%s'", m_numTram, atoi(m_secOp),
                            m_MsgCli.tipOperacion(), m_rfcOp, m_NumSerieOp.c_str(), m_agcCve); //+ 070710 (GHM)
							
	#if ORACLE							
       ok = m_BDAC->ejecutaFuncion(SP_REG_SOLOPER, "'%s',%d,%d,'%s','%s','%s'", m_numTram, atoi(m_secOp), 
                            m_MsgCli.tipOperacion(), m_rfcOp, m_NumSerieOp.c_str(), m_agcCve);   
		Bitacora->escribePV(BIT_INFO, "COperacionAC.regOperacionBD: Se ejecuto la funci�n: '%s' para el registro en la BD",SP_REG_SOLOPER);
	#else
	ok = m_BDAC->ejecutaSP(SP_REG_SOLOPER, "'%s',%d,%d,'%s','%s','%s'", m_numTram, atoi(m_secOp), 
                            m_MsgCli.tipOperacion(), m_rfcOp, m_NumSerieOp.c_str(), m_agcCve); 
		//Bitacora->escribePV(BIT_INFO, "COperacionAC: Se ejecuto el SP: '%s' para el registro en la BD", SP_REG_SOLOPER);
	#endif						
							
   if (ok && m_BDAC->getValores("i", &i_resRegOp))
   {
     if (i_resRegOp == 0)
        {
		  Bitacora->escribe(BIT_INFO, "COperacionAC.regOperacionBD: Se realiz� el registro en la BD"); 
		  return ok; 
		}
		else return false;
   }
   else 
   { //+ 070710 (GHM)
      Bitacora->escribePV(BIT_ERROR,"Error en COperacionAC en regOperacionBD: Error, Datos no registrados: %s, %d, %d, %s, %s, %s", m_numTram, atoi(m_secOp),
                            m_MsgCli.tipOperacion(), m_rfcOp, m_NumSerieOp.c_str(), m_agcCve); //+ GHM
      
      return false;
   }//+ 070710 (GHM)
   //>>>AGZ: Cons:06002;06003 Fin.
}
//################################################################################
bool COperacionAC::regOperacionBDDet(int sec, int op)
{
   #if ORACLE
     return m_BDAC->ejecutaSP(SP_REG_SOLOPERDET, "'%s',%d,%d,:i,:i", m_numTram, sec, op);
   #else 
     return m_BDAC->ejecutaSP(SP_REG_SOLOPERDET, "'%s',%d,%d", m_numTram, sec, op);
   #endif
   
}
//################################################################################
// Esta funcion se llama desde la clase CSrvAC y se apoya en el objeto CConexion para obtener s�lo una vez las vigencias de la DB
//GHM(20100826): V3.0.3: Se modifica para sustituir el arreglo donde se obtiene la vigencia por un objeto map
//- GHM (20100826): V3.0.3 bool COperacionAC::getVigencias( unsigned long vigencia[]) 
bool COperacionAC::getVigencias(map<int, int> &mapVig)
{
   bool ok = false;
   //-GHM V3.0.3 int  i = 0;
   int id = 0, vig = 0;

   if (conectaBD())
   {
      //- GHM V3.0.3 if (m_BDAC->consulta("SELECT vigencia FROM cat_tip_cer;"))
      if (m_BDAC->consulta(QS_VIGENCIA_CERT))
      {
         while(m_BDAC->sigReg() /*- GHM V3.0.3 && i++ < TOTAL_CAT_TIP_CER */ )
         {
            //- GHM V3.0.3 if (m_BDAC->getValores("i", &vigencia[i] ))
            //- GHM V3.0.3    ok = true;
            //>+ GHM (20100826): V3.0.3
            if (m_BDAC->getValores("ii", &id, &vig))
            {
               ok = true;
               mapVig[id] = vig;
               //>+ GHM (20100903): V3.0.4
               if (id == 1)
                  mapVig[ID_VIGFIELMORAL] = vig;
               //<+ GHM (20100903): V3.0.4
            }
            //<+ GHM (20100826): V3.0.3
            else
               //- GHM V3.0.3 Bitacora->escribePV(BIT_ERROR, "Error(%d) al obtener la vigencia del tipo de certificado:%d", ERR_NO_GET_VIG_VAL, i);
               Bitacora->escribePV(BIT_ERROR, "Error en COperacionAC en getVigencias: Error(%d) al obtener la vigencia del tipo de certificado:%d", ERR_NO_GET_VIG_VAL, id); //+ GHM V3.0.3
         }
      }
      else
         Bitacora->escribePV(BIT_ERROR, "Error en COperacionAC en getVigencias: Error(%d) al consultar la vigencia en la tabla CAT_TIP_CER", ERR_NO_GET_VIG);
      desconectaBD();
      //>+ GHM (20100903): V3.0.4 Se lee del archivo de configuraci�n
      int valLeidoArchConf = 0;
      int error = getVarConfVig("[VIGENCIA_FIEL]","FISICA",&valLeidoArchConf);
      if ( error == 0 && valLeidoArchConf > 0 )
      {
         mapVig.erase(1);
         mapVig[1] = valLeidoArchConf;
      }
      valLeidoArchConf = 0;
      error = getVarConfVig("[VIGENCIA_FIEL]","MORAL",&valLeidoArchConf);
      if ( error == 0 && valLeidoArchConf > 0 )
      {
         mapVig.erase(ID_VIGFIELMORAL);
         mapVig[ID_VIGFIELMORAL] = valLeidoArchConf;
      }
      //<+ GHM (20100903): V3.0.4
   }
   return ok;
} 
//################################################################################

//GHM(20100903): V3.0.4: Se agrega funci�n para obtener vigencia del archivo de configuraci�n y se sustituya en el objeto map
int COperacionAC::getVarConfVig(const char *etiqueta, const char *variable, int* valor)
{
   int errVars = 0;
   string sValor = "";
   *valor = 0;

   errVars = m_Configuracion.getValorVar(etiqueta, variable, &sValor);
   if(errVars == 0)
      *valor = atoi(sValor.c_str());
   return errVars;
}


