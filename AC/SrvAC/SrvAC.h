/*##############################################################################
  ###  PROYECTO:              PKI-SAT                                        ###
  ###  MODULO:                SrvAR    Servidor de la AR                     ###
  ###                                                                        ###
  ###  DESARROLLADORES:       H�ctor Ornelas Arciga           HOA            ###
  ###                         Amilcar Guerrero Zu�iga         AGZ            ###
  ###                         Gudelia Hern�ndez Molina        GHM            ###
  ###                         Miguel Angel Mendoza L�pez      MAML           ###
  ###                         Roc�o Alejandra Mart�nez Olayo  RAMO           ###
  ###                         Silvia Eur�dice Rocha Reyes     SERR           ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       Martes 6, diciembre del 2005                   ###
  ###                                                                        ###
  ##############################################################################
         1         2         3         4         5         6         7         8
12345678901234567890123456789012345678901234567890123456789012345678901234567890
*/
/*##############################################################################
   VERSION:  
      V.02.00      (20051206 - 20051231) HOA: 2a versi�n: reconstrucci�n total 
                                del servicio
                                M�dulo de control, recibe las solicitudes de las
                                aplicaciones clientes CertiSAT (ventanilla y Web)


             .01   (20070208 - 200702  ) GHM: Modificaciones relacionadas con la
                                sustituci�n de las funciones de tiempo con las
                                funciones de la librer�a Sgi_Fecha

      V.3.0.3      (20100826-20100903) GHM:Se realizan las modificaciones para que se asigne la vigencia de 48 meses a FIEL PF
                                y 27 meses a FIEL de PM

#################################################################################*/

#ifndef _SRVAC_H_
#define _SRVAC_H_
static const char* _SRVAC_H_VERSION_ ATR_USED = "SrvAC @(#)"\
"DSIC10352AC_ 2010-09-03 SrvAC.h 1.1.0/2";

//#VERSION: 1.1.0

//#############################################################################
#include <string>

#include <SgiTipos.h>
//#include <Sgi_SrvUnx.h>        //Lib para crear el Servicio
//- #include <MensajesSAT.h>   //- GHM (070423)
//#include <Sgi_MsgPKI.h>        //+ GHM (070423)
//- #include <CConfigFile.h>   //- GHM (070423)
//#include <Sgi_ConfigFile.h>    //+ GHM (070423)
#include <Sgi_PKI.h>
//-- #include <AdmSegPKI.h>    //- GHM (070425)
//-- #include <ShMemPKI.h>     //- GHM (070425)
#include <map>

typedef struct data_cert {
   bool          estCertDig; // 1 con estandares del certif. digital, 0 no
   int           n_extFEA;
   int           n_extSEL;
   //- GHM (20100826): V3.0.3 unsigned long vigencia[TOTAL_TIP_CER]; // Para no leer la tabla "cat_tip_cer" a c/rato, solo se hace al inicio
   map<int, int> vigencia; //+ GHM (20100826): V3.0.3
   std::string   m_CertAC;
   std::string   m_repositorio;
   RSA           *m_privKey;
   SNames        *extFEA;
   SNames        *extSEL;
   
   data_cert () 
   { //- GHM (20100826) estCertDig = false; n_extFEA = n_extSEL = 0; memset(&vigencia,0,sizeof(vigencia)); m_privKey=NULL; extFEA=NULL; extSEL=NULL; };
      estCertDig = false; n_extFEA = n_extSEL = 0; m_privKey = NULL; extFEA = NULL; extSEL = NULL; };
} DATA_CERT; 

//#############################################################################
class CSrvAC : public CSrvUnx
{
      CConfigFile   Configuracion;
      MensajesSAT   MsgCli;
      CArchCfgSC    *archCfgExt;
      bool          datosCertAC;  // 1 se obtienen los datos del Certif. de la AC, 0 se obtienen de memoria
      DATA_CERT     data;         // Datos para crear certificado de Fiel/Sellos

      bool getLlavePrivada( bool es_keyprivada_nueva );
      bool getLlavePrivada_NUEVA();
      bool getLlavePKCS7( char *key, char *id_key, char *passwd, int l_passwd );
      bool getLlavePrivada_act( const unsigned char* cLlavePriv, int tamLlavePriv );
      bool abreArchExtensiones();
      bool cierraArchExtensiones();
      bool iniciaExtensiones(const char * );
      bool VerificaLlaves();
      bool GetExtCertAC(int idNid, char *dato);
 
      bool getLlavePrivada_ANTERIOR();
      
   protected:
      bool InicioDeSesion();

      virtual bool Inicia();
      virtual bool SetParmsSktProc(CSSL_parms& parms);
      virtual bool Proceso();
      virtual bool Operaciones();
      virtual bool envOperInv();

   public:
      CSrvAC();
      virtual ~CSrvAC();
};
//#############################################################################

extern CSrvAC* ptrSrvAC;

//#############################################################################

#endif //_SRVAC_H_

