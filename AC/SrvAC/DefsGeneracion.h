#ifndef _DEFSGENERACION_H_
#define _DEFSGENERACION_H_
static const char* _DEFSGENERACION_H_VERSION_ ATR_USED = "SrvAC @(#)"\
"DSIC10352AC_ 2010-09-03 DefsGeneracion.h 1.1.0/1";

//#VERSION: 1.1.0
//            V.3.0.3  SrvAC
//                     (20100826-20100903) GHM:Se realizan las modificaciones para que se asigne la vigencia de 48 meses a FIEL PF
//                                         y 27 meses a FIEL de PM

#define ERR_SEP_CAD_RFC         200
#define ERR_NO_GEN_CERT         201
#define ERR_NO_GET_VIG          202
#define ERR_BD                  203
#define ERR_EDO_CER_ACT         204
#define ERR_CERT_AC             205
#define ERR_NO_CREA_ARCH_CERT   206
#define ERR_REG_CERT_BD         207
#define ERR_PROC_CERT_BD        208
#define ERR_NO_GET_VIG_VAL      209
#define ERR_GEN_SERIAL          210
#define ERR_NO_TIENE_CERT_ACT   211 
#define ERR_NO_FREG             212  // MAML 070727: No pudo obtener la fecha de registro en hora local en Rev*Ren
//#define ERR_EXEC_QUERY        213
#define ERR_REV_CERT            214
#define ERR_GET_NO_SERIE        215
#define ERR_GEN_SER             216
#define ERR_NO_GET_VIG_VAL_REN  217
#define ERR_NO_ENVIA            218
//>+ ERGL (070315)
#define ERR_NO_UTC	 	        219
#define ERR_OBT_CORREO          220

//- GHM (20100826): V3.0.3 #define  TOTAL_CAT_TIP_CER      5    // OjO : hasta hoy hasta 5 tipos de certificados ( MAML 081119 )
//- GHM (20100826): V3.0.3 #define  TOTAL_TIP_CER          TOTAL_CAT_TIP_CER + 1

#define  ID_VIGFIELMORAL        999  //+ GHM (20100903): V3.0.4 

#include <Sgi_ConfOpen.h>
//#include <SrvAC.h>
#include <Sgi_SrvUnx.h>        //Lib para crear el Servicio
#include <Sgi_MsgPKI.h>
#include <Sgi_ConfigFile.h>
#include <COperacionAC.h>
#include <SrvAC.h>
//- #include <PwdProtection.h> //- GHM (070423)
#include <Sgi_ProtegePwd.h> //+ GHM (070423)

#endif
