static const char* _GF256_CPP_VERSION_ ATR_USED = "MemoriaCompartida @(#)"\
"DSIC07412AC_ 2007-12-14 GF256.cpp 1.1.0/0";

//#VERSION: 1.1.0
#include <assert.h>

#include "GF256.h"
#include "GF256tables.h"

GF256::
GF256(int val) : polynomial(val % 256)
{
  if (val < 0)
    val += 256;
}

GF256 GF256::
operator+(const GF256& b) const
{
  GF256 result(*this);

  result.polynomial ^= b.polynomial;

  return result;
}

GF256 GF256::
operator-(const GF256& b) const
{
  GF256 result(*this);
  
  result.polynomial ^= b.polynomial;

  return result;
}

GF256 GF256::
operator*(const GF256& b) const
{
  // 0 * x = 0, x * 0 = 0
  if ((polynomial == 0) || (b.polynomial == 0))
    return GF256(0);

  int newPower = ((pow() + b.pow()) % 255);

  return GF256(GF2_8_powers_to_int[newPower]);
}

GF256 GF256::
operator/(const GF256& b) const
{
  // 0 / x = 0, x / 0 = error
  if (polynomial == 0)
    return GF256(0);
  
  assert(b.polynomial != 0);

  int newPower = ((pow() - b.pow()) % 255);

  if (newPower < 0) 
    newPower += 255;

  return GF256(GF2_8_powers_to_int[newPower]);
}

// A + A = 0, so A = -A.  We have to do this, because otherwise, -x will
// cast x to int, and it'll be something entirely different.
GF256 GF256::
operator-() const 
{
  return GF256(polynomial);
}

int GF256::
pow() const 
{
  assert(polynomial != 0);

  return GF2_8_int_to_powers[polynomial-1];
}
