#ifndef _LAGRANGEPOLYNOMIALINTERPOLATION_H_
#define _LAGRANGEPOLYNOMIALINTERPOLATION_H_
static const char* _LAGRANGEPOLYNOMIALINTERPOLATION_H_VERSION_ ATR_USED = "MemoriaCompartida @(#)"\
"DSIC07412AC_ 2007-12-14 LagrangePolynomialInterpolation.h 1.1.0/0";

//#VERSION: 1.1.0

//#include <algobase.h>
#include <vector>

#include "Point.h"
#include "Polynomial.h"

template <class NUMBER>
class LagrangePolynomialInterpolation {
public:

  LagrangePolynomialInterpolation() :
    points() {
  }

  virtual ~LagrangePolynomialInterpolation() {
  }

  void addPoint(const Point<NUMBER>& point) {
    points.push_back( point );
  }

  Polynomial<NUMBER> fit() const {
    Polynomial<NUMBER> result(0);

    for (unsigned j = 0; j < points.size(); j++) 
    {
      Polynomial<NUMBER> pj( Pj(j) );
      result = result + pj;
      // >>> HOA TMP
        std::cout << "Pj(" << j << ") = " << pj << '\n';
	std::cout << "resultado = " << result << std::endl;
      // <<< HOA TMP
   }

    return result;
  }

  NUMBER fit(const NUMBER& x) const {
    NUMBER result(0);

    for (unsigned j = 0; j < points.size(); j++) {
      result += Pjx(j, x);
    }

    return result;
  }

  // Lazily go through the points, making sure that we don't have repeats.
  bool uniquePoints() const {
    for (unsigned i = 0; i < points.size(); i++) {
      for (unsigned j = 0; j < points.size(); j++) {
	if (i == j)
	  continue;
	
	if (points[i].x() == points[j].x()) {
	  std::cout << "Error: Two keys share identical points." << std::endl;
	  return false;
	}
      } 
    } 

    return true;
  }

protected:

  std::vector< Point<NUMBER> > points;

private:

  Polynomial<NUMBER> Pj(unsigned j) const {
    Polynomial<NUMBER> result(0);
    Polynomial<NUMBER> product(1);

// >>> HOA TMP
	std::cout << "pj(" << j << ")" << std::endl;
        std::cout << "\tresult = " << result << std::endl;
        std::cout << "\tproduct = " << product << std::endl;
// <<< HOA TMP
 
    result.setCoefficient(0, points[j].y());
// >>> HOA TMP
        std::cout << "\tresult(a) = " << result << std::endl;
// <<< HOA TMP


    for (unsigned i = 0; i < points.size(); i++) {
      if (i == j)
	continue;

      product.setCoefficient(1, (NUMBER) 1 / (points[j].x() - points[i].x()));
      product.setCoefficient(0, points[i].x() /
			     (points[i].x() - points[j].x()));
      
      // cout << "Sub-product (" << i << ", " << j << ") " << product << endl;
      
      result = result * product;
// >>> HOA TMP
        std::cout << "\tproduct(b) = " << product << std::endl;
        std::cout << "\tresult(b) = " << result << std::endl;
// <<< HOA TMP
    }

    return result;
  }

  NUMBER Pjx(unsigned j, const NUMBER& x) const {
    NUMBER result(points[j].y());

    for (unsigned i = 0; i < points.size(); i++) {
      if (i == j)
	continue;

      result *= (x - points[i].x()) / (points[j].x() - points[i].x());
    }
    
    return result;
  }
};

#endif // _LAGRANGEPOLYNOMIALINTERPOLATION_H_








