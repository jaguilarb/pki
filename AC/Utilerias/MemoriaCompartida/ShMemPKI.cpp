static const char* _SHMEMPKI_CPP_VERSION_ ATR_USED = "MemoriaCompartida @(#)"\
"DSIC07412AC_ 2007-12-14 ShMemPKI.cpp 1.1.0/0";

//#VERSION: 1.1.0
/* ****************************************************
 * Rutinas para el manejo de servicios de seguridad.  *
 *                                                    *
 * Febrero 2004.                                      *
 * ****************************************************/

#include <memory.h>

#include <fstream>
using namespace std;
using std::fstream;

#include "ShMemPKI.h"

CSHMemPKI::
CSHMemPKI(key_t key, int32 tam) :
CSHMem(key, tam)
{
}

CSHMemPKI::~CSHMemPKI()
{
}

void CSHMemPKI::
_Inicializa()
{
   memset(Shmemb, 0, sizeof(struct reg_serv_pki_st));
}

uint8 CSHMemPKI::
getNumApls()
{
   return ((RegServPKI*) Shmemb)->numApls;
}

ServSegInfo& CSHMemPKI::
getServInfo(uint8 id)
{
   return((RegServPKI*) Shmemb)->idserv[id];
}

bool CSHMemPKI::
bajaServ(char *servicio)
{
   uint8 id = findServ(servicio);
   if (id == (uint8) -1)
      return false;
   ServSegInfo& pk = getServInfo(id);
   memset(&pk, 0, sizeof(struct serv_info_st));
   ((RegServPKI*)Shmemb)->numApls--;
   return true;
}

uint8 CSHMemPKI::
findServ(char *servicio)
{
   uint8 num = getNumApls();
   for (uint8 i = 0; i < num; i++)
   {
      if (!strncmp(getServInfo(i).clave, servicio, ID_SRV))
         return i;
   }
   return (uint8) -1;
}

bool CSHMemPKI::
registraServ(ServSegInfo* pk)
{
   if (findServ(pk->clave) == (uint8) -1)
   {
      RegServPKI* reg = (struct reg_serv_pki_st*) Shmemb;
      for (uint8 i = 0; i < (uint8) -1; i++)
      {
         if (reg->idserv[i].clave[0] == 0)
         {
            memcpy(&reg->idserv[i], pk, sizeof(struct serv_info_st));
            reg->numApls++;
            return true;
         }
      }
   }
   return false;
}

