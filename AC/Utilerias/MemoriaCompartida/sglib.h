//static const char* _SGLIB_H_VERSION_ ATR_USED = "MemoriaCompartida @(#)"\
"DSIC07412AC_ 2007-12-14 sglib.h 1.1.0/0";

//#VERSION: 1.1.0
#ifdef __cplusplus
extern "C" {
#endif

#include <limits.h>
#include <stdio.h>

#define MAX_RSA_MODULUS_BITS 2048
#define MAX_RSA_MODULUS_LEN ((MAX_RSA_MODULUS_BITS + 7) / 8)
#define MAX_RSA_PRIME_BITS ((MAX_RSA_MODULUS_BITS + 1) / 2)
#define MAX_RSA_PRIME_LEN ((MAX_RSA_PRIME_BITS + 7) / 8)

#define MAXSERIAL       23
#define MAXUTC          15
#define MAXCERTLEN      1024*6

#define MAX_DIGEST_LEN 20

#define DNITEMS    20
#define EAITEMS    25

/* CONSTANTES PARA DIGESTIONES */
#define DA_MD2  2
#define DA_SHS  3
#define DA_MD4  4
#define DA_MD5  5
#define DA_SHA1 6

/* CONSTANTES DE ERROR PARA RSA */
#define IDOK_RSA    0
#define IDERROR_RSA 1

/* CONSTANTES PARA LOS PARAMETROS IOCtxType */
#define IOCTX_MEM       1  /* Memoria Continua    */
#define IOCTX_MEMB      2  /* Memoria por Bloques */
#define IOCTX_DISK      3  /* Archivo             */

/* CONSTANTES PARA FORMATO DE CONTEXTO: Fmt*/
#define IO_BINARY           1
#define IO_INTERNET         2 /* B64 con saltos de linea */
#define IO_INTERNET1        3 /* B64 sin saltos de linea */

/* CONSTANTES DE ERROR */
#define IOCTX_OK            1000
#define IOCTX_UNKNOWN_CTX   1001
#define IOCTX_NO_MEMORY     1002
#define IOCTX_WRONG_TYPE    1003
#define IOCTX_WRONG_FORMAT  1004 /* FORMATO DE ARCHIVO ERRONEO */
#define IOCTX_WRONG_IOMODE  1005
#define IOCTX_OPENING_ERROR 1006
#define IOCTX_READ_ERROR    1007
#define IOCTX_CLOSE_ERROR   1008
#define IOCTX_WRITE_ERROR   1009
#define IOCTX_NOROOM        1010

/* CONSTANTES Y ESTRUCTURAS PARA CONVERSION B64 */
#define BinBufSize       90  /* Debe ser multiplo de 3 */
#define B64BufSize       (BinBufSize/3)*4
#define B64ExtraBufSize  (1+BinBufSize/76)*2

/* CONSTANTES PARA LOS PARAMETROS IOCTX_IOMode */
#define IOCTX_INPUT_MODE    2000
#define IOCTX_OUTPUT_MODE   2001
#define IOCTX_APPEND_MODE   2002

/* ERRORES QUE PUEDEN REGRESAR LAS FUNCIONES */
#define SET_OK                       0
#define SET_ALREADY_EXIST            1
#define SET_NOT_FOUND                2
#define SET_NO_ROOM                  3
#define SET_NO_MEMORY                4
#define SET_ITEM_NOT_FOUND           5
#define SET_EMPTY_FILE               6
#define SET_EMPTY_BLOCK              7
#define SET_FILE_ERROR               8

/* CONSTANTES PARA LOS NOMBRES DE LAS LISTAS */
#define  SET_signerinfos             0
#define  SET_certificates            1
#define  SET_recipientinfos          2
#define  SET_envelopekey             3
#define  SET_newsignercertificates   4
#define  SET_privatekeys             5
#define  SET_passwords               6
#define  SET_recipientcertificates   7
#define  SET_signatures              8
#define  SET_temporaluser1           9
#define  SET_temporaluser2          10
#define  SET_originaluser           11
#define  SET_userprivatekey         12
#define  SET_userpassword           13
#define  SET_usercertificate        14
#define  SET_userprivk              15
#define  SET_userpassw              16
#define  SET_usercerti              17
#define  SET_EF_signercertif        18
#define  SET_EF_signerprivkey       19
#define  SET_EF_signerpassw         20
#define  SET_EF_recipientcertif     21
#define  SET_EF_digest              22
#define  SET_EF_Sender              23
#define  SET_EF_Receiver            24
#define  SET_EF_PubKey              25
#define  SET_EF_CERTIFICATE         26
#define  SET_EF_HINFO               27
#define  SET_EF_TINFO               28
#define  SET_EF_TINFO2              29
#define  SET_EF_TINFO3              30
#define  SET_userset1               31
#define  SET_userset2               32
#define  SET_userset3               33
#define  SET_userset4               34
#define  SET_userset5               35
#define  SET_userset6               36
#define  SET_userset7               37
#define  SET_userset8               38
#define  SET_userset9               39
#define  SET_userset10              40
#define  SET_userset11              41
#define  SET_userset12              42
#define  SET_userset13              43
#define  SET_userset14              44
#define  SET_userset15              45
#define  SET_userset16              46
#define  SET_userset17              47
#define  SET_userset18              48
#define  SET_userset19              49
#define  SET_userset20              50

/* NUMERO MAXIMO DE SETS A CARGAR */
#define  SET_MAX_RECORDS             51

/* Constantes para algoritmos encripci�n/firmas */
#define RSA_ENCRIPTION           100
#define MD2_WITH_RSA_ENCRIPTION  101
#define MD4_WITH_RSA_ENCRIPTION  102
#define MD5_WITH_RSA_ENCRIPTION  103
#define MD2_WITH_DES_CBC         104
#define MD5_WITH_DES_CBC         105
#define DES_CBC                  106
#define RC4_FORMAT               107
#define PBE_SHA_40BIT_RC2_CBC    108
#define PBE_SHA_128BIT_RC2_CBC   109
#define SHA1_WITH_RSA_ENCRIPTION 110
#define DES_EDE3_CBC             111

#define PKCS_DATA                0
#define PKCS_SIGN                1
#define PKCS_ENVELOPE            2
#define PKCS_SIGN_AND_ENVELOPE   3
#define PKCS_DIGEST              4
#define PKCS_ENCRYPT             5	
#define PKCS_NONE                6

#define PKCS7_MAX_DEST_KEYS        10
#define PKCS7_NEW_FILE              0
#define PKCS7_APPEND_TO_LAST_FILE   1

#define PKCS_LOOKAUTENTICVAR   1
#define PKCS_NOAUTENTIC        2
#define PKCS_CANT_AUTENTICATE  3
#define PKCS_REVOCATED_CERT    4
#define PKCS_ERROR_ON_CRL      5

#define DATA                       200
#define SIGNED_DATA                201
#define ENVELOPED_DATA             202
#define SIGNED_AND_ENVELOPED_DATA  203
#define DIGESTED_DATA              204
#define ENCRYPTED_DATA             205

#define PKCS_ERROR_COULDNTOPEN_SOURCE        1000
#define PKCS_ERROR_COULDNTOPEN_DEST          1001
#define PKCS_ERROR_INVALID_FORMAT            1002
#define PKCS_COULDNT_PRIVATEDECRYPT          1003
#define PKCS_ERROR_NOMEMORY                  1004
#define PKCS_ERROR_COULDNT_DECODESIGNERCERT  1005
#define PKCS_COULDNT_PUBLICDECRYPT           1006
#define PKCS_ERROR_COULDNT_DECODEDESTCERT    1007
#define PKCS_ERROR_UNEXPECTED                1008
#define PKCS_ERROR_SOURCEEMPTY               1009
#define PKCS_COULDNT_PRIVATEENCRYPT          1010
#define PKCS_ERROR_CHECKCONSISTENCY          1011
#define PKCS_CONCATIDREG                     1012
#define PKCS_COULDNT_SIGN                    1013
#define PKCS_ERROR_COULDNTWRITE_FILE         1014
#define PKCS_ERROR_NOAUTENTIC_DOCUMENT       1015
#define PKCS_ERROR_NO_EXTERN_CONTEXT         1016
#define PKCS_ERROR_COULDNTOPEN_EXTERN_FILE   1017

#define PKCS_SUCCESS                         2000

#define FRIENDLY_NAME              300
#define LOCALKEY_ID                301
#define X509_CERTIFICATE           302
#define KEY_BAG                    303
#define CERT_BAG                   304
#define CRL_BAG                    305
#define SECRET_BAG                 306
#define PKCS8_KEY_BAG              307
#define KEY_USAGE                  308
#define BASIC_CONSTRAINTS          309
#define CERTIFICATE_POLICIES       310
#define CRL_DIST_POINTS            311
#define AUTHORITY_KEY_ID           312
#define CRL_NUMBER                 313
#define CRL_REASON                 314
#define MESSAGE_DIGEST             315
#define SIGNING_TIME               316
#define SMIME_CAPABILITIES         317
#define CONTENT_TYPE               318
#define UNKNOWN1                   319
#define UNKNOWN2                   320
#define UNKNOWN3                   321
#define UNKNOWN4                   322

#define RC4_OK                          0
#define RC4_ERROR_COULDNTOPEN_SOURCE 2000
#define RC4_ERROR_COULDNTOPEN_DEST   2001
#define RC4_ERROR_UNEXPECTED_EOF     2002
#define RC4_ERROR_COULDNTWRITE_DEST  2003
#define RC4_ERROR_NOMEMORY           2004
#define RC4_ERROR_UNEXPECTED         2005

#if UINT_MAX > 0xFFFFL
  #define USE_LONG_RC4
  typedef unsigned int RC4WORD ;
#else
  typedef unsigned char RC4WORD ;
#endif

#define SHA_DIGESTSIZE 20

/* RSAPOINTER defines a generic pointer type */
typedef unsigned char *RSAPOINTER;

/* UINT2 defines a two byte word */
typedef unsigned short int UINT2;

/* UINT4 defines a four byte word */
typedef unsigned long int UINT4;

/* BYTE defines a unsigned character */
typedef unsigned char BYTE;

typedef struct {
	unsigned char *Data ;
	unsigned int   Len ;
} ITEM ;

typedef struct {
  unsigned int bytesNeeded;                    /* seed bytes required   */
  unsigned char state[16];                     /* state of object       */
  unsigned int outputAvailable;                /* number byte available */
  unsigned char output[16];                    /* output bytes          */
} R_RANDOM_STRUCT;

typedef struct {
  unsigned short int bits;                     /* length in bits of modulus */
  unsigned char modulus[MAX_RSA_MODULUS_LEN];  /* modulus                   */
  unsigned char exponent[MAX_RSA_MODULUS_LEN]; /* public exponent           */
} R_RSA_PUBLIC_KEY ;

typedef struct {
  unsigned short int bits;                            /* length in bits of modulus */
  unsigned char modulus[MAX_RSA_MODULUS_LEN];         /* modulus                   */
  unsigned char publicExponent[MAX_RSA_MODULUS_LEN];  /* public exponent           */
  unsigned char exponent[MAX_RSA_MODULUS_LEN];        /* private exponent          */
  unsigned char prime[2][MAX_RSA_PRIME_LEN];          /* prime factors             */
  unsigned char primeExponent[2][MAX_RSA_PRIME_LEN];  /* exponents for CRT         */
  unsigned char coefficient[MAX_RSA_PRIME_LEN];       /* CRT coefficient           */
} R_RSA_PRIVATE_KEY;

typedef struct {
  unsigned char state[16];                  /* state                      */
  unsigned char checksum[16];               /* checksum                   */
  unsigned int count;                       /* number of bytes, modulo 16 */
  unsigned char buffer[16];                 /* input buffer               */
} SL_MD2_CTX;

typedef struct {
	UINT4 state[4];                          /* state (ABCD)          */
	UINT4 count[2];        /* number of bits, modulo 2^64 (lsb first) */
	unsigned char buffer[64];                /* input buffer          */
} SL_MD4_CTX;

typedef struct {
  UINT4 state[4];                           /* state (ABCD)         */
  UINT4 count[2];        /* number of bits, Modulo 2^64 (lsb first) */
  unsigned char buffer[64];                 /* input buffer         */
} SL_MD5_CTX;

typedef struct {
	UINT4 digest [5];                        /* Message digest   */
	UINT4 countLo, countHi;                  /* 64-bit bit count */
	UINT4 data [16];                         /* SHS data buffer  */
} SHS_CTX;

typedef struct {
    unsigned long state[5];                  /* The structure for storing SHA1 info */
    unsigned long count[2];
    unsigned char buffer[64];
} SHA1_CTX;

typedef struct {
  int digestAlgorithm;                         /* digest type */
  union {                                      /* digest sub-context */
		SL_MD2_CTX md2;
		SL_MD4_CTX md4;
		SL_MD5_CTX md5;
		SHS_CTX shs;
      SHA1_CTX sha1;
	} context;
} R_DIGEST_CTX;

typedef struct {
  unsigned int bits;                        /* length in bits of modulus       */
  int useFermat4;                           /* public exponent (1 = F4, 0 = 3) */
} R_RSA_PROTO_KEY;

typedef struct {
  UINT4 subkeys[32];                        /* subkeys                    */
  UINT4 iv[2];                              /* initializing vector        */
  UINT4 originalIV[2];                      /* for restarting the context */
  int encrypt;                              /* encrypt flag               */
} DES_CBC_CTX;

typedef struct {
  UINT4 subkeys[3][32];                     /* subkeys for three operations */
  UINT4 iv[2];                              /* initializing vector          */
  UINT4 originalIV[2];                      /* for restarting the context   */
  int encrypt;                              /* encrypt flag                 */
} DES3_CBC_CTX;

typedef struct {
  UINT4 subkeys[32];                        /* subkeys                    */
  UINT4 iv[2];                              /* initializing vector        */
  UINT4 inputWhitener[2];                   /* input whitener             */
  UINT4 outputWhitener[2];                  /* output whitener            */
  UINT4 originalIV[2];                      /* for restarting the context */
  int encrypt;                              /* encrypt flag               */
} DESX_CBC_CTX;


/*  ESTRUCTURA PARA CONTEXTO DE RC4 */
typedef struct {
  RC4WORD state[ 256 ];
  RC4WORD x, y;
} SL_RC4_CTX ;

/***********************/
/* NUMEROS ALEATORIOS  */
/***********************/
int R_RandomInit( R_RANDOM_STRUCT *random ); /* Estructura para nums aleatorios */

int R_RandomUpdate(R_RANDOM_STRUCT *random, /* random structure          */
                   unsigned char *block,    /* block of values to mix in */
                   unsigned int len ) ;     /* length of block           */

int R_GetRandomBytesNeeded(unsigned int *bytesNeeded, /* number of mix-in bytes needed */
                           R_RANDOM_STRUCT *random) ; /* random structure */

int R_GenerateBytes(unsigned char *block,       /* block            */
                    unsigned int len,           /* length of block  */
                    R_RANDOM_STRUCT *random) ;  /* random structure */

void R_RandomFinal( R_RANDOM_STRUCT *random );  /* random structure */


/*************************/
/* DIGESTION DE MENSAJES */
/*************************/

int R_DigestInit(R_DIGEST_CTX *context,		/* new context              */
		int digesttype);                 		/* message-digest algorithm */


int R_DigestUpdate(R_DIGEST_CTX *context,    /* context                  */
		unsigned char *partIn,          			/* next data part           */
		unsigned int partInLen);         		/* length of next data part */


int R_DigestFinal(R_DIGEST_CTX *context,     /* context                  */
		unsigned char *digest,          			/* message digest           */
		unsigned int *digestLen);        		/* length of message digest */


int R_DigestBlock(unsigned char *digest,     /* message digest           */
		unsigned int *digestLen,         		/* length of message digest */
		unsigned char *block,            		/* block                    */
		unsigned int blockLen,           		/* length of block          */
		int digestAlgorithm);             		/* message-digest algorithm */

void MD2Init(SL_MD2_CTX *context);              /* context */

void MD2Update(SL_MD2_CTX *context,             /* context               */
		unsigned char *input,           			/* input block           */
		unsigned int inputLen);          		/* length of input block */

void MD2Final(unsigned char digest[16],      /* message digest */
		SL_MD2_CTX *context);                     /* context        */

void MD4Init(SL_MD4_CTX *context);              /* context */

void MD4Update(SL_MD4_CTX *context,             /* context               */
		unsigned char *input,           			/* input block           */
		unsigned int inputLen);          		/* length of input block */

void MD4Final(unsigned char digest[16],      /* message digest */
		SL_MD4_CTX *context);               		/* context        */

void MD5Init(SL_MD5_CTX *context);              /* context               */

void MD5Update(SL_MD5_CTX *context,             /* context               */
	unsigned char *input,           				/* input block           */
	unsigned int inputLen);          			/* length of input block */

void MD5Final (unsigned char digest[16],     /* message digest */
		SL_MD5_CTX *context);               	   /* context        */

void SHA1Init(SHA1_CTX* context);

void SHA1Update(SHA1_CTX* context,
                unsigned char* data,
                unsigned int len);

void SHA1Final(unsigned char digest[SHA_DIGESTSIZE],
               SHA1_CTX* context);


/******************/
/* ALGORITMOS RSA */
/******************/

int R_GeneratePEMKeys (R_RSA_PUBLIC_KEY *publicKey, /* new RSA public key  */
		R_RSA_PRIVATE_KEY *privateKey,  					/* new RSA private key */
		R_RSA_PROTO_KEY *protoKey,      					/* RSA prototype key   */
		R_RANDOM_STRUCT *randomStruct);  				/* random structure    */


int RSAPrivateEncrypt(unsigned char *output,   /* output block           */
		unsigned int *outputLen,        			  /* length of output block */
		unsigned char *input,           			  /* input block            */
		unsigned int inputLen,          			  /* length of input block  */
		R_RSA_PRIVATE_KEY *privateKey);  		  /* RSA private key        */


int RSAPrivateDecrypt(unsigned char *output,   /* output block           */
		unsigned int *outputLen,        			  /* length of output block */
		unsigned char *input,           			  /* input block            */
		unsigned int inputLen,          			  /* length of input block  */
		R_RSA_PRIVATE_KEY *privateKey);  		  /* RSA private key        */


int RSAPublicEncrypt(unsigned char *output,    /* output block           */
		unsigned int *outputLen,        			  /* length of output block */
		unsigned char *input,           			  /* input block            */
		unsigned int inputLen,          			  /* length of input block  */
		R_RSA_PUBLIC_KEY *publicKey,    			  /* RSA public key         */
		R_RANDOM_STRUCT *randomStruct);  		  /* random structure       */


int RSAPublicDecrypt(unsigned char *output,    /* output block 			 */
		unsigned int *outputLen,        			  /* length of output block */
		unsigned char *input,           			  /* input block 			    */
		unsigned int inputLen,         			  /* length of input block  */
		R_RSA_PUBLIC_KEY *publicKey);    		  /* RSA public key 		    */


/******************/
/* ALGORITMOS DES */
/******************/

void DES_CBCInit(DES_CBC_CTX *context,    /* context */
		unsigned char *key,             		/* key 	  */
		unsigned char *iv,                  /* initializing vector      */
		int encrypt);			/* encrypt flag (1 = encrypt, 0 = decrypt) */

int DES_CBCUpdate(DES_CBC_CTX *context,  /* context 		*/
		unsigned char *output,          	  /* output block */
		unsigned char *input,           	  /* input block  */
		unsigned int len);                 /* length of input and output blocks */

void DES_CBCRestart(DES_CBC_CTX *context);   /* context */

void DES3_CBCInit(DES3_CBC_CTX *context,     /* context */
		unsigned char *key,             			/* key 	  */
		unsigned char *iv,              			/* initializing vector */
		int encrypt);                    		/* encrypt flag (1 = encrypt, 0 = decrypt) */

int DES3_CBCUpdate(DES3_CBC_CTX *context,  /* context      */
		unsigned char *output,          		 /* output block */
		unsigned char *input,           		 /* input block  */
		unsigned int len);               	 /* length of input and output blocks */

void DES3_CBCRestart(DES3_CBC_CTX *context); /* context */

void DESX_CBCInit(DESX_CBC_CTX *context, /* context 					 	*/
		unsigned char *key,             		/* DES key and whiteners 	*/
		unsigned char *iv,              		/* DES initializing vector */
		int encrypt);                    	/* encrypt flag (1 = encrypt, 0 = decrypt) */

int DESX_CBCUpdate(DESX_CBC_CTX *context,   /* context 		*/
		unsigned char *output,          		  /* output block */
		unsigned char *input,           		  /* input block  */
		unsigned int len);               	  /* length of input and output blocks */

void DESX_CBCRestart(DESX_CBC_CTX *context); /* context */

/***************************/
/* MANIPULACION DE MEMORIA */
/***************************/

void R_memset(RSAPOINTER output,    /* output block 	 */
              int value,                 /* value 			 */
              unsigned int len);         /* length of block */

void R_memcpy(RSAPOINTER output,    /* output block 	  */
              RSAPOINTER input,             /* input block 	  */
              unsigned int len);         /* length of blocks */

int R_memcmp(RSAPOINTER Block1,     /* first block 	  */
             RSAPOINTER Block2,            /* second block 	  */
             unsigned int len);         /* length of blocks */

/**********************/
/* FUNCIONES DE IOCTX */
/**********************/

typedef struct {                                      /* INICIALIZACION: */
   unsigned char Buf1[4];                             /*   {0,0,0,0}     */
   unsigned char Buf2[4];                             /*   {0,0,0,0}     */
   int UsedBytes;                                     /*   0             */
   int GroupsWritten;                                 /*   0             */
   int FoundPadding;                                  /*   4             */
   unsigned char Comment;                             /*   0             */
   unsigned char BreakLines;                          /*   0=No 1=Si     */
   unsigned char BufB64 [B64BufSize+B64ExtraBufSize]; /*   0,0,0,...     */
   unsigned char BufBin [BinBufSize];                 /*   0,0,0,...     */
   unsigned int P1,P2;                                /*   0,0           */
   unsigned int MaxGroups;                            /*   16 (para PEM) */
} B64Ctx;

/* ESTRUCTURA BASICA PARA CONTEXTO */
typedef struct {
    unsigned char IOCtxType ; /* TIPO DE CONTEXTO: MEMORIA CONTINUA, EN BLOQUES O DISCO */
    void * IOCtxInfo ;        /* APUNTADOR A LA INFORMACION DEL CONTEXTO                */
    int IOCtxFmt;             /* FORMATO: IO_BINARY , IO_INTERNET � IO_INTERNET1        */
    int IOCtxIOMode;
    B64Ctx B64;
    unsigned int MaxGroups;
} IOCTX;

/* CREACION Y DESTRUCCION DE CONTEXTOS */
int IOCTX_Create  ( IOCTX * IOctx,            /* CONTEXTO                    */
                    unsigned char IOCtxType,  /* TIPO DE CONTEXTO A CREAR    */
                    int Fmt);                 /* FORMATO: BINARIO O INTERNET */

int IOCTX_SetNewFormat (IOCTX * IOctx, int Format);

int IOCTX_Destroy ( IOCTX * IOctx );          /* CONTEXTO A DESTRUIR      */

/* DESLIGA EL BLOQUE DE MEMORIA DE UN CONTEXTO DE MEM. CONTINUA */
int IOCTX_UnlinkMem(IOCTX * IOctx);

/* ASIGNACION DEPENDIENDO DEL TIPO DE CONTEXTO */
int IOCTX_Set_Mem  (IOCTX * IOctx,             /* CONTEXTO                   */
                    unsigned long BufferSize); /* TAMA�O DEL BLOQUE CONTINUO */

int IOCTX_Set_MemB (IOCTX * IOctx,             /* CONTEXTO              */
                    unsigned int BufferSize);  /* TAMA�O DE LOS BLOQUES */

int IOCTX_Set_Disk (IOCTX * IOctx,             /* CONTEXTO           */
                    char * FileName);          /* NOMBRE DEL ARCHIVO */

/* ASIGNACION DE UN BLOQUE DE MEMORIA A UN CONTEXTO DE MEM. CONTINUA  */
int IOCTX_LinkMem( IOCTX * IOctx,                      /* CONTEXTO                */
                         unsigned char * Memory,       /* BLOQUE DE MEMORIA       */
                         unsigned long UsedBufferSize);/* TAMA�O USADO DEL BLOQUE */

/* APERTURA Y CIERRE DE CONTEXTOS */
int IOCTX_Open   (IOCTX * IOctx,               /* CONTEXTO                       */
                  unsigned int IOCTX_IOMode ); /* APERTURA PARA ENTRADA � SALIDA */

int IOCTX_Close ( IOCTX * IOctx );             /* CONTEXTO A CERRAR    */

int IOCTX_eoctx ( IOCTX * IOctx );             /* CONTEXTO A VERIFICAR */


/* LECTURA Y ESCRITURA DE CONTEXTOS */
int IOCTX_Read  ( IOCTX * IOctx,           /* CONTEXTO                          */
                  unsigned char * Block,   /* BLOQUE DONDE SE RECIBIRA LO LEIDO */
                  unsigned long BlockSize, /* TAMA�O DEL BLOQUE A LEER          */
                  unsigned long *Bytes );  /* BYTES LEIDOS                      */

int IOCTX_Write ( IOCTX * IOctx,           /* CONTEXTO                     */
                  unsigned char * Block,   /* BLOQUE A ESCRIBIR            */
                  unsigned long BlockSize, /* TAMA�O DEL BLOQUE A ESCRIBIR */
                  unsigned long *Bytes );  /* BYTES ESCRITOS               */

/*********************/
/* FUNCIONES DE SETS */
/*********************/

/* NODO PARA CADA ELEMENTO DE LOS SETS */
typedef struct {
	unsigned char * Block ;
	unsigned int BlockLen ;
	/*struct Node*/
   void * Next ;
} Node ;

/* DEFINICION DE UN SET */
typedef struct {
   Node BlockList[SET_MAX_RECORDS];
} SetRecord;

/* FUNCIONES PARA CREAR, INICIALIZAR Y DESTRUIR SETS */
int SET_Init_Sets ( SetRecord * SRecords);

int SET_Create    ( SetRecord * SRecords,
                    unsigned int Num_Set);
int SET_Destroy   ( SetRecord* SRecords,
                    unsigned int Num_Set);

int SET_Destroy_All ( SetRecord * SRecords );


/* FUNCIONES PARA MANIPULACION DE ELEMENTOS DE LOS SETS */

int SET_Add ( SetRecord * SRecords,
              unsigned int Num_Set,
              unsigned char * Cert,
              int CertLen );

int SET_Count_Items ( SetRecord * SRecords,
                      unsigned int Num_Set,
                      unsigned int * Count);

int SET_Get_Item ( SetRecord * SRecords,
                   unsigned int Num_Set,
                   unsigned int Count,
                   unsigned char ** Cert ,
                   unsigned int *CertLen);

int SET_Count_Sets ( SetRecord * SRecords,
                     unsigned int * Count);

int SET_Destroy_Item( SetRecord * SRecords,
                      unsigned int Num_Set,
                      unsigned int Count);

int SET_Copy_Item( SetRecord * SRecords,
                   unsigned int Num_Set1,
                   unsigned int Num_Set2,
                   unsigned int Count);

int SET_Switch( SetRecord * SRecords,
                unsigned int Num_Set1,
                unsigned int Num_Set2);

int SET_Delete( SetRecord * SRecords,
                unsigned int Num_Set);

int SET_Load( SetRecord * SRecords,
              unsigned int Num_Set,
              char * FileName);

int SET_Destroy_Extra( SetRecord * SRecords);


/******************************************/
/* FUNCIONES CRIPTOGRAFICAS DE ALTO NIVEL */
/******************************************/

int PKCS_CodeCertificationRequest( unsigned char *Buffer ,
		unsigned int *CodedBufferLen ,
		int AlgId , ITEM Names[] ,
		R_RSA_PUBLIC_KEY *PublicKey,
		R_RSA_PRIVATE_KEY *PrivateKey,
		ITEM ExtendedAttributes[] ) ;

int PKCS_DeCodeCertificationRequest( unsigned char *Buffer ,
		unsigned int CodedLen ,
		int *AlgId , ITEM Names[] ,
		R_RSA_PUBLIC_KEY *PublicKey ,
		int *Authentic, ITEM Attributes[]);

int PKCS_CodeCertificate( unsigned char *Buffer ,
                          unsigned int *CodedLen ,
                          ITEM *Version ,
                          ITEM *Serial ,
                          int SigAlgId ,
                          ITEM IssuerName[] ,
                          unsigned char *NotBefore ,
                          unsigned char *NotAfter ,
                          ITEM SubjectName[] ,
                          int AlgId ,
                          R_RSA_PUBLIC_KEY *SubjectPublicKey ,
                          R_RSA_PRIVATE_KEY *IssuerPrivateKey,
                          ITEM ExtV3Attribs[]) ;

int PKCS_CodePKCS6Certificate(
                     unsigned char *Buffer ,
                     unsigned int *CodedLen ,
                     ITEM *Version ,
                     ITEM *Serial ,
                     int SigAlgId ,
                     ITEM IssuerName[] ,
                     unsigned char *NotBefore ,
                     unsigned char *NotAfter ,
                     ITEM SubjectName[] ,
                     int AlgId ,
                     R_RSA_PUBLIC_KEY *SubjectPublicKey ,
                     R_RSA_PRIVATE_KEY *IssuerPrivateKey,
                     ITEM AdicData[] ) ;

int PKCS_DeCodeCertificate( unsigned char *Buffer ,
		unsigned int BufferLen ,
		ITEM *Version , ITEM *Serial ,
		int *AlgId , ITEM IssuerNames[] ,
		unsigned char *NotBefore ,
		unsigned char *NotAfter ,
		ITEM SubjectNames[] ,
		int *SigAlgId ,
		R_RSA_PUBLIC_KEY *SubjectPublicKey ,
		R_RSA_PUBLIC_KEY *IssuerPublicKey ,
		int *Authentic , ITEM Attributes[]);

int PKCS_EncryptPrivateKey( unsigned char *Buffer ,
		unsigned int *BufferLen ,
		int AlgId ,
		unsigned int Iterations ,
		unsigned char *Password ,
		unsigned int PasswordLen ,
		R_RSA_PRIVATE_KEY *PrivateKey );

int PKCS_DecryptPrivateKey( unsigned char *Buffer ,
		unsigned int BufferLen ,
		unsigned char *Password ,
		unsigned int PasswordLen ,
		R_RSA_PRIVATE_KEY *PrivateKey );

int PKCS_BuildFile(IOCTX *SrcFileName,  /* NOMBRE DEL ARCHIVO A FIRMAR                           */
                   IOCTX *DestFileName, /* NOMBRE DEL ARCHIVO DESTINO                            */
                   unsigned char * SignerCert, /* CERTIFICADO DEL FIRMANTE                       */
                   unsigned char * SignerPrivateKey, /* LLAVE PRIVADA (encriptada) DEL FIRMANTE  */
                   char * password,                  /* PASSWORD DE LA LLAVE PRIVADA (ASCII 0)   */
                   unsigned char * DestCert,         /* CERTIFICADO DEL DESTINATARIO             */
                   int Operation,                    /* OPERACION A REALIZAR                     */
                   SetRecord * SRecords);            /* SET                                      */

int PKCS_OpenFile(
        IOCTX *SrcFileName,                   /* NOMBRE DEL ARCHIVO PKCS A ABRIR        */
        IOCTX *DestFileName,                  /* NOMBRE DEL ARCHIVO A GENERAR           */
        unsigned char * AutCerCert,           /* CERTIFICADO DE LA UNIDAD CERTIFICADORA */
        unsigned char * DestinataryPrivateKey,/* LLAVE PRIV. DEL DESTINATARIO           */
        char * password,                      /* PASSWORD DE LLAVE PRIVADA              */
        int *DocAuthentic,                    /* RESULTADO DE LA AUTENTIFICACION        */
        SetRecord * SRecords);                /* SET                                    */

int PKCS_OpenMessage(
		     IOCTX *DestFileName ,         /* ARCHIVO DESTINO         */
		     IOCTX *SrcFileName ,          /* ARCHIVO FUENTE          */
		     int *DocAuthentic ,           /* ARCHIVO AUTENTICO O NO. */
		     unsigned char *SignerCert ,   /* CERIIF. DEL FIRMANTE    */
		     unsigned int *SignerCertLen,  /* LONG. DEL CERTIFICADO   */
		     SetRecord * SRecords,         /* SET                     */
		     unsigned char *PKCS7_USE_RC4 ) ;

int PKCS_OpenMessage_( IOCTX *DestFileName ,           /* ARCHIVO DESTINO         */
                       IOCTX *SrcFileName ,            /* ARCHIVO FUENTE          */
                       IOCTX *ExternContent ,          /* ARCHIVO EXTERNO         */
                       int *DocAuthentic ,             /* ARCHIVO AUTENTICO O NO. */
                       unsigned char *SignerCert ,     /* CERIIF. DEL FIRMANTE    */
                       unsigned int *SignerCertLen,    /* LONG. DEL CERTIFICADO   */
                       SetRecord * SRecords,           /* SET                     */
                       unsigned char *PKCS7_USE_RC4) ; 

/********************/
/* FUNCIONES DE RC4 */
/********************/
void RC4Init( SL_RC4_CTX *RC4Context , unsigned char *Key , unsigned int KeyLen ) ;

int RC4Update( SL_RC4_CTX *RC4Context , unsigned char *Output , unsigned char *Input  , unsigned int Len ) ;

int RC4XUpdate( SL_RC4_CTX *RC4Context , unsigned char *Buffer , unsigned int Len ) ;

int RC4UpdateToFile( char *TargetFile, char *SrcFile, SL_RC4_CTX *RC4Context, unsigned int HeaderSize ) ;

/**************************************************/
/* FUNCIONES EXTRA PARA MANEJO DE ITEMS DESDE VB3 */
/**************************************************/
void Get_Item_Stream (ITEM * Item, unsigned char * String);

int GetItem( unsigned char *Buffer , int Index , unsigned char *String );


#define HMAC_SUCCESS           0

int hmac(unsigned char *stream,
         int stream_len,
         unsigned char *key,
         unsigned int key_len,
         unsigned char *digest,
         unsigned int *DigestLen,
         int DigestAlgorithm);

int hmac_init(R_DIGEST_CTX *Context,
              unsigned char *key,
              unsigned int key_len,
              int digesttype);

int hmac_update(R_DIGEST_CTX *Context,
                unsigned char * block,
                int block_len);

int hmac_final(R_DIGEST_CTX *Context,
               unsigned char *key,
               unsigned int key_len,
               unsigned char *digest,
               unsigned int *digestLen);


#define UNSPECIFIED                0
#define KEY_COMPROMISE             1
#define CA_COMPROMISE              2
#define AFFILIATION_CHANGED        3
#define SUPERSEDED                 4
#define CESSATION_OF_OPERATION     5
#define CERTIFICATE_HOLD           6
#define REMOVE_FROM_CRL            8

#define CRL_ALG_INCONSISTENCY    101
#define CRL_UNEXPECTED_ERROR     102
#define CRL_ERROR_NOCONTEXT      103
#define CRL_ERROR_DEC_CERT       104
#define CRL_ERROR_NOMEMORY       105
#define CRL_INVALID_FORMAT       106
#define CRL_ERROR_WPARAMS        107
#define CRL_ERROR_ENCRYPT        108
#define CRL_ERROR_IOCTX          109
#define CRL_ERROR_NOUTC          110
#define CRL_ERROR_RFILE          111
#define CRL_NO_LIST              112
#define CRL_SUCCESS              100

int CrlInit(IOCTX *Target, char *TargetFile);

int CrlUpdate(IOCTX *Target,
              ITEM *Serial,
              unsigned char *RevDate,
              unsigned char *Reason);

int CrlFinal (IOCTX *List,
              ITEM *Cert,
              ITEM *CrlNum,
              unsigned char *thisUpdate,
              unsigned char *nextUpdate,
              int SigAlg,
              R_RSA_PRIVATE_KEY *PrivateKey,
              char * DestFile);

int Crl_Authenticate (char *SourceFile,
                      char *TargetFile,
                      ITEM CrlIssuerNames[],
                      ITEM IssuerIssuerNames[],
                      ITEM *AuthoritySerial,
                      ITEM *CrlNum,
                      R_RSA_PUBLIC_KEY *PublicKey,
                      int *Authentic) ;

int Crl_Open_List(IOCTX *Disk,
                  char *PathList);

int Crl_Close_List(IOCTX *Disk);                  
                                        
int Crl_Find_Entry (IOCTX *List,
                    ITEM *Serial,
                    unsigned char *Date,
                    unsigned char *reason );

int Crl_Get_Dates (IOCTX *List,
                   unsigned char *IssuingDate,
                   unsigned char *NextIssuing);

int Crl_Get_One_Entry ( IOCTX *List,
                        ITEM *Serial,
                        unsigned char *Date,
                        unsigned char *reason );

unsigned int Crl_Paste_Signature (char *PartialCrl ,
                                  ITEM *SignatureItem ,
                                  int SigAlgId ) ;

#define objectSigningCA     0x100
#define SMIMECA             0x200
#define SSLCA               0x400
#define objectSigning       0x1000
#define SMIMEClient         0x2000
#define SSLServer           0x4000
#define SSLClient           0x8000

#define decipherOnly        0x80
#define encipherOnly        0x100
#define crlSigning          0x200
#define certSigning         0x400
#define keyAgreement        0x800
#define dataEncipherment    0x1000
#define keyEncipherment     0x2000
#define nonRepudiation      0x4000
#define digitalSignature    0x8000

#define tlsweb_server_auth  0x01
#define tlsweb_client_auth  0x02
#define code_signing        0x04
#define secure_email        0x08
#define time_stamping       0x10
#define strong_encryption   0x20

#define cert_type           0x05
#define key_usage           0x0f
#define basic_constraints   0x10
#define ext_key_usage       0x11
#define cps                 0x12
#define Crl_Dist_Points     0x13
#define Subject_Alt_Name    0x14

/*----------------------------------------------------------------------------*/
/* convierte de b64 a bin */
int PEM_B64TOBIN ( unsigned char *fue,int fueS,unsigned char *des,int desS);
/*----------------------------------------------------------------------------*/
/* convierte de Bin to B64 */
int PEM_BINTOB64 (unsigned char *fue,int fueS,unsigned char *des,int desS);
/*----------------------------------------------------------------------------*/

void CleanArray(ITEM *, int );
void CleanVariable(ITEM Name, int size);

/* RC2 */

#define SL_RC2_KEY_SIZE		128
#define SL_RC2_KEY_SIZE_WORDS	( SL_RC2_KEY_SIZE / 2 )

#define RC2_BLOCKSIZE		8

typedef struct {
	unsigned int key[SL_RC2_KEY_SIZE_WORDS ];
	} SL_RC2_KEY;

typedef unsigned short	WORD;

void rc2keyInit( SL_RC2_KEY *rc2key, BYTE *userKey, int length, int reducedLength );
void rc2DecryptCBC( SL_RC2_KEY *rc2Key, BYTE *iv, BYTE *buffer, int noBytes );
void rc2EncryptCBC( SL_RC2_KEY *rc2Key, BYTE *iv, BYTE *buffer, int noBytes );
void rc2encrypt( SL_RC2_KEY *rc2key, BYTE *buffer );
void rc2decrypt( SL_RC2_KEY *rc2key, BYTE *buffer );

#ifdef __cplusplus
}
#endif
