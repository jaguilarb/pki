#ifndef _SGLIB_SD_H_
#define _SGLIB_SD_H_
static const char* _SGLIB_SD_H_VERSION_ ATR_USED = "MemoriaCompartida @(#)"\
"DSIC07412AC_ 2007-12-14 sglib_sd.h 1.1.0/0";

//#VERSION: 1.1.0
/******************************************************************************/
/* SGLIB.H      SeguriDATA Privada, S. A. de C. V  Copyright (c) 1997-2002    */
/*                                                                            */
/*  SeguriLib 2.5.1 header  (4096 bit key enhancement)                        */
/*                                                                            */
/******************************************************************************/

#ifdef __cplusplus
extern "C" {
#endif

#include <limits.h>
#include <stdio.h>

/* Data types used in DES and DIGEST algorithms */
typedef unsigned short int UINT2;  /* UINT2 defines a two byte word     */
typedef unsigned long int UINT4;   /* UINT4 defines a four byte word    */
typedef unsigned char BYTE;        /* BYTE defines a unsigned character */

/* Data type used in SETS, PKCS and CRL functions */
typedef struct {
	unsigned char *Data;  /* Block Pointer          */
	unsigned int  Len;    /* Block Len pointed to   */
} ITEM ;


/******************************************************************************/
/******************************* MESSAGE DIGEST *******************************/
/******************************************************************************/

#define MAX_DIGEST_LEN 20
#define SHA_DIGESTSIZE 20

#define DA_MD2  2
#define DA_SHS  3
#define DA_MD4  4
#define DA_MD5  5
#define DA_SHA1 6

typedef struct {
  unsigned char state[16];    /* state                      */
  unsigned char checksum[16]; /* checksum                   */
  unsigned int count;         /* number of bytes, modulo 16 */
  unsigned char buffer[16];   /* input buffer               */
} MD2_CTX;

typedef struct {
	UINT4 state[4];             /* state (ABCD)                            */
	UINT4 count[2];             /* number of bits, modulo 2^64 (lsb first) */
	unsigned char buffer[64];   /* input buffer                            */
} MD4_CTX;

typedef struct {
  UINT4 state[4];             /* state (ABCD)                            */
  UINT4 count[2];             /* number of bits, Modulo 2^64 (lsb first) */
  unsigned char buffer[64];   /* input buffer                            */
} MD5_CTX;

typedef struct {
	UINT4 digest [5];           /* Message digest   */
	UINT4 countLo, countHi;     /* 64-bit bit count */
	UINT4 data [16];            /* SHS data buffer  */
} SHS_CTX;

typedef struct {
    unsigned long state[5];   /* state (ABCDE)                           */
    unsigned long count[2];   /* number of bits, modulo 2^64 (lsb first) */
    unsigned char buffer[64]; /* input buffer                            */
} SHA1_CTX;

typedef struct {
  int digestAlgorithm;        /* digest type        */
  union {                     /* digest sub-context */
		MD2_CTX md2;
		MD4_CTX md4;
		MD5_CTX md5;
		SHS_CTX shs;
    SHA1_CTX sha1;
	} context;
} R_DIGEST_CTX;

/* MD2 Specific Context *******************************************************/
void MD2Init (
       MD2_CTX *context          /* MD2 Digest Context    */
     );

void MD2Update (
       MD2_CTX *context,         /* MD2 Digest Context    */
       unsigned char *input,     /* input block           */
       unsigned int inputLen     /* length of input block */
     );

void MD2Final (
       unsigned char digest[16], /* Message Digest        */
       MD2_CTX *context          /* MD2 Digest Context    */
     );

/* MD4 Specific Context *******************************************************/
void MD4Init (
       MD4_CTX *context          /* MD4 Digest Context    */
     );

void MD4Update (
       MD4_CTX *context,         /* MD4 Digest Context    */
       unsigned char *input,     /* input block           */
       unsigned int inputLen     /* length of input block */
     );

void MD4Final (
       unsigned char digest[16], /* Message Digest        */
       MD4_CTX *context          /* MD4 Digest Context    */
     );

/* MD5 Specific Context *******************************************************/
void MD5Init (
       MD5_CTX *context          /* MD5 Digest Context    */
     );

void MD5Update (
       MD5_CTX *context,         /* MD5 Digest Context    */
       unsigned char *input,     /* Input Block           */
       unsigned int inputLen     /* Length of input block */
     );

void MD5Final (
       unsigned char digest[16], /* Message Digest        */
       MD5_CTX *context          /* MD5 Digest Context    */
     );

/* SHA1 Specific Context ******************************************************/
void SHA1Init (
       SHA1_CTX* context         /* SHA1 Digest Context   */
     );

void SHA1Update (
       SHA1_CTX* context,        /* SHA1 Digest Context   */
       unsigned char *input,     /* Input block           */
       unsigned int inputlen     /* Length of input block */
     );

void SHA1Final (
       unsigned char digest[SHA_DIGESTSIZE],/* Msg Digest */
       SHA1_CTX* context         /* SHA1 Digest Context   */
     );

/* Multi-algorithm Context ****************************************************/
int R_DigestInit (
      R_DIGEST_CTX *context,   /* Digest Context           */
      int digesttype           /* Message-digest Algorithm */
    );

int R_DigestUpdate (
      R_DIGEST_CTX *context,   /* Digest Context           */
      unsigned char *partIn,   /* Next data part           */
      unsigned int partInLen   /* Length of next data part */
    );

int R_DigestFinal (
      R_DIGEST_CTX *context,   /* Digest Context           */
      unsigned char *digest,   /* Message digest           */
      unsigned int *digestLen  /* Length of message digest */
    );

int R_DigestBlock (
      unsigned char *digest,   /* Message digest           */
      unsigned int *digestLen, /* Length of message digest */
      unsigned char *block,    /* Block                    */
      unsigned int blockLen,   /* Length of block          */
      int digestAlgorithm      /* Message-digest algorithm */
    );


/******************************************************************************/
/******************************* RANDOM NUMBERS *******************************/
/******************************************************************************/

/* ERROR CODES FOR RANDOM AND RSA FUNCTIONS */
#define IDOK_RSA     0
#define IDERROR_RSA  1

typedef struct {
  unsigned int bytesNeeded;                    /* seed bytes required   */
  unsigned char state[16];                     /* state of object       */
  unsigned int outputAvailable;                /* number byte available */
  unsigned char output[16];                    /* output bytes          */
} R_RANDOM_STRUCT;

int R_RandomInit (
      R_RANDOM_STRUCT *random   /* Random number structure   */
    );

int R_RandomUpdate (
      R_RANDOM_STRUCT *random,  /* Random structure          */
      unsigned char *block,     /* Block of values to mix in */
      unsigned int len          /* Length of block           */
    );

int R_GetRandomBytesNeeded (
      unsigned int *bytesNeeded,/* Number of mix-in bytes needed */
      R_RANDOM_STRUCT *random   /* Random structure              */
    );

int R_GenerateBytes (
      unsigned char *block,     /* Block            */
      unsigned int len,         /* Length of block  */
      R_RANDOM_STRUCT *random   /* Random structure */
    );

void R_RandomFinal (
       R_RANDOM_STRUCT *random  /* Random structure */
     );


/******************************************************************************/
/*************************** RSA ALGORITHM (PKCS-1) ***************************/
/******************************************************************************/

#define MAX_RSA_MODULUS_BITS 4096
#define MAX_RSA_MODULUS_LEN ((MAX_RSA_MODULUS_BITS + 7) / 8)
#define MAX_RSA_PRIME_BITS ((MAX_RSA_MODULUS_BITS + 1) / 2)
#define MAX_RSA_PRIME_LEN ((MAX_RSA_PRIME_BITS + 7) / 8)
#define MAX_SIGNATURE_LEN MAX_RSA_MODULUS_LEN

typedef struct {
  unsigned short int bits;                     /* length in bits of modulus */
  unsigned char modulus[MAX_RSA_MODULUS_LEN];  /* modulus                   */
  unsigned char exponent[MAX_RSA_MODULUS_LEN]; /* public exponent           */
} R_RSA_PUBLIC_KEY ;

typedef struct {
  unsigned short int bits;                          /* len in bits of modulus */
  unsigned char modulus[MAX_RSA_MODULUS_LEN];       /* modulus                */
  unsigned char publicExponent[MAX_RSA_MODULUS_LEN];/* public exponent        */
  unsigned char exponent[MAX_RSA_MODULUS_LEN];      /* private exponent       */
  unsigned char prime[2][MAX_RSA_PRIME_LEN];        /* prime factors          */
  unsigned char primeExponent[2][MAX_RSA_PRIME_LEN];/* exponents for CRT      */
  unsigned char coefficient[MAX_RSA_PRIME_LEN];     /* CRT coefficient        */
} R_RSA_PRIVATE_KEY;

typedef struct {
  unsigned int bits;                          /* modulus length in bits       */
  int useFermat4;                             /* pub exponent (1 = F4, 0 = 3) */
} R_RSA_PROTO_KEY;

int R_GeneratePEMKeys (
      R_RSA_PUBLIC_KEY *publicKey,            /* New RSA Public Key     */
      R_RSA_PRIVATE_KEY *privateKey,  			  /* New RSA Private Key    */
      R_RSA_PROTO_KEY *protoKey,      			  /* RSA Prototype Key      */
      R_RANDOM_STRUCT *randomStruct  				  /* Random Structure       */
    );

int RSAPrivateEncrypt (
      unsigned char *output,                  /* Output Block           */
      unsigned int *outputLen,        			  /* Length of Output Block */
      unsigned char *input,           			  /* Input Block            */
      unsigned int inputLen,          			  /* Length of Input Block  */
      R_RSA_PRIVATE_KEY *privateKey  		      /* RSA private key        */
    );

int RSAPrivateDecrypt (
      unsigned char *output,                  /* Output Block           */
      unsigned int *outputLen,        			  /* Length of Output Block */
      unsigned char *input,           			  /* Input Block            */
      unsigned int inputLen,          			  /* Length of Input Block  */
      R_RSA_PRIVATE_KEY *privateKey  		      /* RSA private key        */
    );

int RSAPublicEncrypt (
      unsigned char *output,                  /* Output Block           */
      unsigned int *outputLen,        			  /* Length of Output Block */
      unsigned char *input,           			  /* Input Block            */
      unsigned int inputLen,          			  /* Length of Input Block  */
      R_RSA_PUBLIC_KEY *publicKey,    			  /* RSA Public Key         */
      R_RANDOM_STRUCT *randomStruct  		      /* Random Structure       */
    );

int RSAPublicDecrypt (
      unsigned char *output,                  /* Output Block           */
      unsigned int *outputLen,        			  /* Length of Output Block */
      unsigned char *input,           			  /* Input Block            */
      unsigned int inputLen,         			    /* Length of Input Block  */
      R_RSA_PUBLIC_KEY *publicKey             /* RSA Public Key         */
    );


/******************************************************************************/
/******************************* DES ALGORITHM  *******************************/
/******************************************************************************/

typedef struct {
  UINT4 subkeys[32];                        /* subkeys                     */
  UINT4 iv[2];                              /* initializing vector         */
  UINT4 originalIV[2];                      /* for restarting the context  */
  int encrypt;                              /* encrypt flag                */
} DES_CBC_CTX;

typedef struct {
  UINT4 subkeys[3][32];                     /* subkeys for three operations*/
  UINT4 iv[2];                              /* initializing vector         */
  UINT4 originalIV[2];                      /* for restarting the context  */
  int encrypt;                              /* encrypt flag                */
} DES3_CBC_CTX;

typedef struct {
  UINT4 subkeys[32];                        /* subkeys                     */
  UINT4 iv[2];                              /* initializing vector         */
  UINT4 inputWhitener[2];                   /* input whitener              */
  UINT4 outputWhitener[2];                  /* output whitener             */
  UINT4 originalIV[2];                      /* for restarting the context  */
  int encrypt;                              /* encrypt flag                */
} DESX_CBC_CTX;

/* DES-CBC ********************************************************************/
void DES_CBCInit (
       DES_CBC_CTX *context,  /* DES-CBC Context                         */
       unsigned char *key,    /* Key 	                                   */
       unsigned char *iv,     /* initializing Vector                     */
       int encrypt			      /* encrypt flag (1 = encrypt, 0 = decrypt) */
     );

int DES_CBCUpdate (
      DES_CBC_CTX *context,   /* DES-CBC Context                         */
      unsigned char *output,  /* Output Block                            */
      unsigned char *input,   /* Input Block                             */
      unsigned int len        /* Length of Input and Output Blocks       */
    );

void DES_CBCRestart (
       DES_CBC_CTX *context   /* DES-CBC Context                         */
     );

/* DES3-CBC *******************************************************************/
void DES3_CBCInit (
       DES3_CBC_CTX *context, /* DES3-CBC Context                        */
       unsigned char *key,    /* Key                                     */
       unsigned char *iv,     /* Initializing Vector                     */
       int encrypt            /* Encrypt flag (1 = encrypt, 0 = decrypt) */
     );

int DES3_CBCUpdate (
    DES3_CBC_CTX *context,    /* DES3-CBC Context                        */
		unsigned char *output,    /* Output Block                            */
		unsigned char *input,     /* Input Block                             */
		unsigned int len);        /* Length of Input and Output Blocks       */

void DES3_CBCRestart (
       DES3_CBC_CTX *context  /* DES3-CBC Context                        */
     );

/* DESX-CBC *******************************************************************/
void DESX_CBCInit (
       DESX_CBC_CTX *context, /* DESX-CBC Context                        */
       unsigned char *key,    /* DES Key and whiteners 	                 */
       unsigned char *iv,     /* DES Initializing Vector                 */
       int encrypt            /* Encrypt flag (1 = encrypt, 0 = decrypt) */
     );

int DESX_CBCUpdate (
      DESX_CBC_CTX *context,  /* DESX-CBC Context                        */
      unsigned char *output,  /* Output Block                            */
      unsigned char *input,   /* Input Block                             */
      unsigned int len        /* Length of Input and Output Blocks       */
    );

void DESX_CBCRestart (
       DESX_CBC_CTX *context  /* DESX-CBC Context                        */
     );


/******************************************************************************/
/***************************** MEMORY MANIPULATION ****************************/
/******************************************************************************/

/* RSAPOINTER defines a generic pointer type */
typedef unsigned char *RSAPOINTER;


void R_memset (
       RSAPOINTER output,  /* Output Block     */
       int value,          /* Value            */
       unsigned int len    /* Length of Block  */
     );

void R_memcpy (
       RSAPOINTER output,  /* Output Block     */
       RSAPOINTER input,   /* Input Block      */
       unsigned int len    /* Length of Blocks */
     );

int R_memcmp (
      RSAPOINTER Block1,   /* First Block      */
      RSAPOINTER Block2,   /* Second Block     */
      unsigned int len     /* Length of Blocks */
    );


/******************************************************************************/
/******************************* SETS FUNCTIONS  ******************************/
/******************************************************************************/

/* ERROR CODES FOR SET FUNCTIONS */
#define SET_OK                       0
#define SET_ALREADY_EXIST            1
#define SET_NOT_FOUND                2
#define SET_NO_ROOM                  3
#define SET_NO_MEMORY                4
#define SET_ITEM_NOT_FOUND           5
#define SET_EMPTY_FILE               6
#define SET_EMPTY_BLOCK              7
#define SET_FILE_ERROR               8

/* LIST NAME VALUES */
#define  SET_signerinfos             0
#define  SET_certificates            1
#define  SET_recipientinfos          2
#define  SET_envelopekey             3
#define  SET_newsignercertificates   4
#define  SET_privatekeys             5
#define  SET_passwords               6
#define  SET_recipientcertificates   7
#define  SET_signatures              8
#define  SET_temporaluser1           9
#define  SET_temporaluser2          10
#define  SET_originaluser           11
#define  SET_userprivatekey         12
#define  SET_userpassword           13
#define  SET_usercertificate        14
#define  SET_userprivk              15
#define  SET_userpassw              16
#define  SET_usercerti              17
#define  SET_EF_signercertif        18
#define  SET_EF_signerprivkey       19
#define  SET_EF_signerpassw         20
#define  SET_EF_recipientcertif     21
#define  SET_EF_digest              22
#define  SET_EF_Sender              23
#define  SET_EF_Receiver            24
#define  SET_EF_PubKey              25
#define  SET_EF_CERTIFICATE         26
#define  SET_EF_HINFO               27
#define  SET_EF_TINFO               28
#define  SET_EF_TINFO2              29
#define  SET_EF_TINFO3              30
#define  SET_userset1               31
#define  SET_userset2               32
#define  SET_userset3               33
#define  SET_userset4               34
#define  SET_userset5               35
#define  SET_userset6               36
#define  SET_userset7               37
#define  SET_userset8               38
#define  SET_userset9               39
#define  SET_userset10              40
#define  SET_userset11              41
#define  SET_userset12              42
#define  SET_userset13              43
#define  SET_userset14              44
#define  SET_userset15              45
#define  SET_userset16              46
#define  SET_userset17              47
#define  SET_userset18              48
#define  SET_userset19              49
#define  SET_userset20              50

/* MAX NUMBER OF SETS */
#define SET_MAX_RECORDS             51

typedef struct {
	unsigned char * Block ;
	unsigned int BlockLen ;
  void * Next ;
} Node ;

typedef struct {
   Node BlockList[SET_MAX_RECORDS];
} SetRecord;

int SET_Init_Sets (
      SetRecord *SRecords    /* SetRecords        */
    );

int SET_Destroy (
      SetRecord *SRecords,   /* SetRecords        */
      unsigned int Num_Set   /* List number       */
    );

int SET_Destroy_All (
      SetRecord *SRecords    /* SetRecords        */
    );

int SET_Add (
      SetRecord *SRecords,   /* SetRecords        */
      unsigned int Num_Set,  /* List number       */
      unsigned char *Block,  /* Block ptr         */
      int BlockLen           /* Block Length      */
    );

int SET_Count_Sets (
      SetRecord *SRecords,   /* SetRecords        */
      unsigned int *Count    /* Used SET Counted  */
    );

int SET_Count_Items (
      SetRecord *SRecords,   /* SetRecords        */
      unsigned int Num_Set,  /* List number       */
      unsigned int *Count    /* Number of Items   */
    );

int SET_Get_Item (
      SetRecord *SRecords,   /* SetRecords        */
      unsigned int Num_Set,  /* List number       */
      unsigned int Count,    /* Item Number       */
      unsigned char **Block, /* Block ptr         */
      unsigned int *BlockLen /* Block Length      */
     );

int SET_Destroy_Item (
      SetRecord *SRecords,   /* SetRecords        */
      unsigned int Num_Set,  /* List number       */
      unsigned int Count     /* Item Number       */
    );

int SET_Copy_Item (
      SetRecord *SRecords,   /* SetRecords        */
      unsigned int Num_Set1, /* List number 1     */
      unsigned int Num_Set2, /* List number 2     */
      unsigned int Count     /* Item Number       */
    );

int SET_Switch (
      SetRecord *SRecords,   /* SetRecords        */
      unsigned int Num_Set1, /* List number 1     */
      unsigned int Num_Set2  /* List number 2     */
    );

int SET_Delete (
      SetRecord *SRecords,   /* SetRecords        */
      unsigned int Num_Set   /* List number       */
    );

int SET_Load (
      SetRecord *SRecords,   /* SetRecords        */
      unsigned int Num_Set,  /* List number       */
      char * FileName        /* File to read from */
    );

int SET_Destroy_Extra (
      SetRecord *SRecords    /* SetRecords        */
    );


/******************************************************************************/
/******************************* IOCTX FUNCTIONS ******************************/
/******************************************************************************/

/* INTERNAL CONSTANTS FOR B64 STRUCTURE */
#define BinBufSize          90
#define B64BufSize          (BinBufSize/3)*4
#define B64ExtraBufSize     (1+BinBufSize/76)*2

/* IOCtxType PARAMETER VALUES */
#define IOCTX_NONE_TYPE 0  /* No Type          */
#define IOCTX_MEM       1  /* Continous memory */
#define IOCTX_MEMB      2  /* Blocks memory    */
#define IOCTX_DISK      3  /* File             */

/* Fmt PARAMETER VALUES */
#define IO_BINARY    1 /* Binary            */
#define IO_INTERNET  2 /* B64 W/breaklines  */
#define IO_INTERNET1 3 /* B64 WO/breaklines */

/* IOCTX_IOMode PARAMETER VALUES */
#define IOCTX_INPUT_MODE    2000
#define IOCTX_OUTPUT_MODE   2001
#define IOCTX_APPEND_MODE   2002

/* ERROR CODES */
#define IOCTX_OK            1000
#define IOCTX_UNKNOWN_CTX   1001
#define IOCTX_NO_MEMORY     1002
#define IOCTX_WRONG_TYPE    1003
#define IOCTX_WRONG_FORMAT  1004
#define IOCTX_WRONG_IOMODE  1005
#define IOCTX_OPENING_ERROR 1006
#define IOCTX_READ_ERROR    1007
#define IOCTX_CLOSE_ERROR   1008
#define IOCTX_WRITE_ERROR   1009
#define IOCTX_NOROOM        1010

/* B64Ctx, Internal use by IOCTX Functions only */
typedef struct {                                     /* INITIALIZATION: */
  unsigned char Buf1[4];                             /*   {0,0,0,0}     */
  unsigned char Buf2[4];                             /*   {0,0,0,0}     */
  int UsedBytes;                                     /*   0             */
  int GroupsWritten;                                 /*   0             */
  int FoundPadding;                                  /*   4             */
  unsigned char Comment;                             /*   0             */
  unsigned char BreakLines;                          /*   0=No 1=Yes    */
  unsigned char BufB64 [B64BufSize+B64ExtraBufSize]; /*   0,0,0,...     */
  unsigned char BufBin [BinBufSize];                 /*   0,0,0,...     */
  unsigned int P1,P2;                                /*   0,0           */
  unsigned int MaxGroups;                            /*   16 (PEM Only) */
} B64Ctx;

/* IOCTX Structure: Internal information used by IOCTX Functions Only */
typedef struct {
    unsigned char IOCtxType ;/* Ctx type: Continous Memory, Blocks or Disk */
    void * IOCtxInfo ;       /* Pointer to Context Info                    */
    int IOCtxFmt;            /* IO_BINARY, IO_INTERNET, IO_INTERNET1       */
    int IOCtxIOMode;         /* INPUT, OUTPUT OR APPEND MODE CONSTANTS     */
    B64Ctx B64;              /* B64 Internal Context for conversions       */
    unsigned int MaxGroups;  /*  */
} IOCTX;


int IOCTX_Create (
      IOCTX * IOctx,          /* IOctx                                      */
      unsigned char IOCtxType,/* Type: Continous Memory, Mem Blocks or Disk */
      int Fmt                 /* IO_BINARY, IO_INTERNET, IO_INTERNET1       */
    );

int IOCTX_SetNewFormat (
       IOCTX * IOctx,         /* IOctx                                      */
       int Format             /* New format to IOctx                        */
     );

int IOCTX_Destroy (
      IOCTX * IOctx           /* IOctx                      */
    );

int IOCTX_Set_Mem  (
      IOCTX * IOctx,           /* IOctx                     */
      unsigned long BufferSize /* Continuous block size     */
    );

int IOCTX_Set_MemB (
      IOCTX * IOctx,           /* IOctx                     */
      unsigned int BufferSize  /* Each block size           */
    );

int IOCTX_Set_Disk (
      IOCTX * IOctx,           /* IOctx                     */
      char * FileName          /* File Name                 */
    );

int IOCTX_LinkMem (
      IOCTX * IOctx,               /* IOctx                 */
      unsigned char * Memory,      /* Memory Block          */
      unsigned long UsedBufferSize /* Used Block size       */
    );

int IOCTX_UnlinkMem (
      IOCTX * IOctx              /* IOctx                   */
    );

int IOCTX_Open (
      IOCTX * IOctx,             /* IOctx                   */
      unsigned int IOCTX_IOMode  /* Open Mode               */
    );

int IOCTX_Read (
      IOCTX * IOctx,             /* IOctx                   */
      unsigned char * Block,     /* Memory Block to read to */
      unsigned long BlockSize,   /* Block Size to read      */
      unsigned long *Bytes       /* Amount of  bytes read   */
    );

int IOCTX_Write (
      IOCTX * IOctx,             /* IOctx                   */
      unsigned char * Block,     /* Memory Block to write   */
      unsigned long BlockSize,   /* Block Size              */
      unsigned long *Bytes       /* Amount of bytes written */
    );

int IOCTX_Close (
      IOCTX * IOctx              /* IOctx                   */
    );

int IOCTX_eoctx (
      IOCTX * IOctx              /* IOctx                   */
    );


/******************************************************************************/
/********************* HIGH LEVEL CRYPTOGRAPHIC FUNCTIONS *********************/
/******************************************************************************/

/* Useful definitions: Max serial Length, Max UTC length, Max Certificate Len */
#define MAXSERIAL      23
#define MAXUTC         15
#define MAXCERTLEN     1024*6

/* Values for validations */
#define PKCS_LOOKAUTENTICVAR         1
#define PKCS_NOAUTENTIC              2
#define PKCS_CANT_AUTENTICATE        3
#define PKCS_REVOCATED_CERT          4
#define PKCS_ERROR_ON_CRL            5

/* Values for PKCS7 Operations */
#define PKCS_SIGN                    1
#define PKCS_ENVELOPE                2
#define PKCS_SIGN_AND_ENVELOPE       3

/* PKCS7 Error codes */
#define PKCS_ERROR_COULDNTOPEN_SOURCE          1000
#define PKCS_ERROR_COULDNTOPEN_DEST            1001
#define PKCS_ERROR_INVALID_FORMAT              1002
#define PKCS_COULDNT_PRIVATEDECRYPT            1003
#define PKCS_ERROR_NOMEMORY                    1004
#define PKCS_ERROR_COULDNT_DECODESIGNERCERT    1005
#define PKCS_COULDNT_PUBLICDECRYPT             1006
#define PKCS_ERROR_COULDNT_DECODEDESTCERT      1007
#define PKCS_ERROR_UNEXPECTED                  1008
#define PKCS_ERROR_SOURCEEMPTY                 1009
#define PKCS_COULDNT_PRIVATEENCRYPT            1010
#define PKCS_ERROR_CHECKCONSISTENCY            1011
#define PKCS_CONCATIDREG                       1012
#define PKCS_COULDNT_SIGN                      1013
#define PKCS_ERROR_COULDNTWRITE_FILE           1014
#define PKCS_ERROR_NOAUTENTIC_DOCUMENT         1015
#define PKCS_ERROR_NO_EXTERN_CONTEXT           1016
#define PKCS_ERROR_COULDNTOPEN_EXTERN_CONTEXT  1017
#define PKCS_SUCCESS                           2000

/* Encryption/signature algorithm values */
#define RSA_ENCRIPTION             100

#define MD2_WITH_RSA_ENCRIPTION    101
#define MD4_WITH_RSA_ENCRIPTION    102
#define MD5_WITH_RSA_ENCRIPTION    103
#define SHA1_WITH_RSA_ENCRIPTION   110

#define MD2_WITH_DES_CBC           104
#define MD5_WITH_DES_CBC           105

#define DES_CBC                    106
#define RC4_FORMAT                 107
#define DES_EDE3_CBC               111

/* Number of ITEMS to allocate for NAMES and EXTENDED ATTRIBUTES (PKCS7) */

#define DNITEMS          20
#define EAITEMS          25

/* Names location within DNITEMS */

#define DN_O                  0x00  /* Organization Name            */
#define DN_OU                 0x01  /* Organization Unit Name       */
#define DN_CN                 0x02  /* Common Name                  */
#define DN_T                  0x03  /* Title                        */
#define DN_STREET             0x04  /* Street Address               */
#define DN_PostalCode         0x05  /* Postal Code                  */
#define DN_C                  0x06  /* Country                      */
#define DN_S                  0x07  /* State or Province            */
#define DN_L                  0x08  /* Locality Name                */
#define DN_UID                0x09  /* Unique Identifier            */
#define DN_SN                 0x0a  /* Serial Number                */
#define DN_E                  0x0b  /* Email Address                */
#define DN_BC                 0x0c  /* Business Category            */
#define DN_Phone              0x0d  /* Telephone Number             */
#define DN_Fax                0x0e  /* Facsimile Telephone Number   */

/* Extended attributes location within EAITEMS */

#define EA_E                  0x00  /* Email Address                */
#define EA_ChallengePass      0x01  /* Challenge Password           */

/* PKCS10 */
int PKCS_CodeCertificationRequest (
      unsigned char *Buffer,           /* Memory Block                     */
      unsigned int *CodedBufferLen,    /* Bytes coded into Memory Block    */
      int AlgId,                       /* Algorithm used for signature     */
      ITEM Names[DNITEMS],             /* Names to encode                  */
      R_RSA_PUBLIC_KEY *PublicKey,     /* Public Key to encode into PKCS10 */
      R_RSA_PRIVATE_KEY *PrivateKey,   /* Private Key to sign PKCS10       */
      ITEM ExtendedAttributes[EAITEMS] /* Extended Attributes to encode    */
    );

int PKCS_DeCodeCertificationRequest (
      unsigned char *Buffer,                   /* Memory Block to decode   */
      unsigned int CodedLen,                   /* Memory Block Size        */
      int *AlgId,                              /* Algorithm used to sign   */
      ITEM Names[DNITEMS],                     /* Decoded names            */
      R_RSA_PUBLIC_KEY *PublicKey,             /* Public Key Decoded       */
      int *Authentic, ITEM Attributes[EAITEMS] /* Extended Attribs decoded */
    );


/* X.509/PKCS6 */
int PKCS_CodeCertificate (
      unsigned char *Buffer,               /* Memory Block to write coding  */
      unsigned int *CodedLen,              /* Bytes coded into Memory Block */
      ITEM *Version,                       /* Certificate Version           */
      ITEM *Serial,                        /* Serial Number                 */
      int SigAlgId,                        /* Signature algorithm (DIG&RSA) */
      ITEM IssuerName[DNITEMS],            /* Issuer Names                  */
      unsigned char *NotBefore,            /* Start of Validity (UTC)       */
      unsigned char *NotAfter,             /* End of Validity (UTC)         */
      ITEM SubjectName[DNITEMS],           /* Subject Names                 */
      int AlgId,                           /* Key Alg (RSA_ENCRIPTION Only) */
      R_RSA_PUBLIC_KEY *SubjectPublicKey,  /* Subject Public Key            */
      R_RSA_PRIVATE_KEY *IssuerPrivateKey, /* Private Key to sign Cert      */
      ITEM ExtV3Attribs[EAITEMS]           /* Extended Attributes           */
    );

int PKCS_CodePKCS6Certificate (
      unsigned char *Buffer,               /* Memory Block to write coding  */
      unsigned int *CodedLen,              /* Bytes coded into Memory Block */
      ITEM *Version,                       /* Certificate Version           */
      ITEM *Serial,                        /* Serial Number                 */
      int SigAlgId,                        /* Signature algorithm (DIG&RSA) */
      ITEM IssuerName[DNITEMS],            /* Issuer Names                  */
      unsigned char *NotBefore,            /* Start of Validity (UTC)       */
      unsigned char *NotAfter,             /* End of Validity (UTC)         */
      ITEM SubjectName[DNITEMS],           /* Subject Names                 */
      int AlgId,                           /* Key Alg (RSA_ENCRIPTION Only) */
      R_RSA_PUBLIC_KEY *SubjectPublicKey,  /* Subject Public Key            */
      R_RSA_PRIVATE_KEY *IssuerPrivateKey, /* Private Key to sign Cert      */
      ITEM AdicData[EAITEMS],              /* Extended Attributes           */
      int SigExAlgId2
    );

int PKCS_DeCodeCertificate (
      unsigned char *Buffer,               /* Memory Block to decode        */
      unsigned int BufferLen,              /* Block Size to decode          */
      ITEM *Version,                       /* Certificate Version           */
      ITEM *Serial,                        /* Serial Number                 */
      int *AlgId,                          /* Key Alg                       */
      ITEM IssuerNames[DNITEMS],           /* Issuer Names                  */
      unsigned char *NotBefore,            /* Start of Validity (UTC)       */
      unsigned char *NotAfter,             /* End of Validity (UTC)         */
      ITEM SubjectNames[DNITEMS],          /* Subject Names                 */
      int *SigAlgId,                       /* Signature algorithm (DIG&RSA) */
      R_RSA_PUBLIC_KEY *SubjectPublicKey,  /* Subject Public Key            */
      R_RSA_PUBLIC_KEY *IssuerPublicKey,   /* Issuer Pub Key to verify sgtr */
      int *Authentic,                      /* Signature Validation Result   */
      ITEM Attributes[EAITEMS]             /* Extended Attributes           */
    );


/* PKCS5 */
int PKCS_EncryptPrivateKey (
      unsigned char *Buffer,        /* Memory Block to write coding           */
      unsigned int *BufferLen,      /* Bytes coded into Memory Block          */
      int AlgId,                    /* PKCS5 valid Algorithm: MD5_WITH_DES_CBC*/
      unsigned int Iterations,      /* Iterations                             */
      unsigned char *Password,      /* Password used to encrypt Private Key   */
      unsigned int PasswordLen,     /* Passwor Length                         */
      R_RSA_PRIVATE_KEY *PrivateKey /* Private Key                            */
    );

int PKCS_DecryptPrivateKey (
      unsigned char *Buffer,               /* Memory Block to decode        */
      unsigned int BufferLen,              /* Block Size to decode          */
      unsigned char *Password,             /* Password                      */
      unsigned int PasswordLen,            /* Passwor Length                */
      R_RSA_PRIVATE_KEY *PrivateKey        /* Recovered Private Key         */
    );


/* PKCS7 */
/* Simplified PKCS7 API: */
int PKCS_BuildFile (
      IOCTX *SrcCtx,                       /* Source Context                */
      IOCTX *TgtCtx,                       /* Target Context                */
      unsigned char *SignerCert,           /* Signer Certificate PTR        */
      unsigned char *SignerPrivateKey,     /* Encrypted Signer Private Key  */
      char * password,                     /* Piv Key Passwor (ASCIIZ)      */
      unsigned char *DestCert,             /* Addressee Certificate         */
      int Operation,                       /* Operation: Sign, Envelope, S&E*/
      SetRecord *SRecords                  /* Set                           */
    );

int PKCS_OpenFile (
      IOCTX *SrcCtx,                         /* Source Context                */
      IOCTX *TgtCtx,			     /* Target Context                */
      unsigned char *CACert,	             /* CA Certificate PTR            */
      unsigned char *DestinataryPrivateKey,  /* Addressee Private Key         */
      char *password,		             /* Private Key Password          */
      int *DocAuthentic,                     /* Signature Auth result         */
      SetRecord *SRecords                    /* Set                           */
    );

/* STD PKCS7 API */
int PKCS_OpenMessage (
      IOCTX *TgtCtx,                       /* Target Context                */
      IOCTX *SrcCtx,                       /* Source Context                */
      int *DocAuthentic,                   /* Validation Result             */
      unsigned char *SignerCert,           /* Signer Certificate Buffer     */
      unsigned int *SignerCertLen,         /* Certificate Length            */
      SetRecord * SRecords,                /* Set                           */
      unsigned char *AlgID                 /* PKCS7 Symm Algorithm value    */
    );

int PKCS_OpenMessage_ (
      IOCTX *TgtCtx,                       /* Target Context                */
      IOCTX *SrcCtx,                       /* Source Context                */
      IOCTX *ExternContent,                /* Extern Content Context        */
      int *DocAuthentic,                   /* Validation Result             */
      unsigned char *SignerCert,           /* Signer Certificate Buffer     */
      unsigned int *SignerCertLen,         /* Certificate Length            */
      SetRecord * SRecords,                /* Set                           */
      unsigned char *PKCS7_USE_RC4         /* PKCS7 Symm Algorithm value    */
    );

int PKCS_FileMessage(
      int Operation,                       /* Operation to execute          */
      IOCTX *TgtCtx,                       /* Target Context                */
      IOCTX *SrcCtx,                       /* Source Context                */
      SetRecord *SRecords,                 /* Set                           */
      unsigned char PKCS7_USE_RC4          /* PKCS7 Symm Algorithm value    */
    );

/******************************************************************************/
/******************************** RC4 FUNCTIONS *******************************/
/******************************************************************************/

/* RC4 ERROR CODES  */
#define RC4_OK                          0
#define RC4_ERROR_COULDNTOPEN_SOURCE 2000
#define RC4_ERROR_COULDNTOPEN_DEST   2001
#define RC4_ERROR_UNEXPECTED_EOF     2002
#define RC4_ERROR_COULDNTWRITE_DEST  2003
#define RC4_ERROR_NOMEMORY           2004
#define RC4_ERROR_UNEXPECTED         2005

#if UINT_MAX > 0xFFFFL
  #define USE_LONG_RC4
  typedef unsigned int RC4WORD ;
#else
  typedef unsigned char RC4WORD ;
#endif

typedef struct {
  RC4WORD state[ 256 ];        /* S-boxes               */
  RC4WORD x, y;                /* i, j values           */
} RC4_CTX ;

void RC4Init (
       RC4_CTX *RC4Context,    /* RC4 Context           */
       unsigned char *Key,     /* Encryption Key        */
       unsigned int KeyLen     /* Encryption Key Length */
     ) ;

int RC4Update (
      RC4_CTX *RC4Context,     /* RC4 Context           */
      unsigned char *Output,   /* Output Block          */
      unsigned char *Input,    /* Input Block           */
      unsigned int Len         /* Blocks Length         */
    ) ;

int RC4XUpdate (
      RC4_CTX *RC4Context,     /* RC4 Context           */
      unsigned char *Buffer,   /* Input/Output Block    */
      unsigned int Len         /* Block Length          */
    ) ;

/******************************************************************************/
/******************************** RC2 FUNCTIONS *******************************/
/******************************************************************************/

#define RC2_KEY_SIZE		128
#define RC2_KEY_SIZE_WORDS	( RC2_KEY_SIZE / 2 )

typedef struct {
	unsigned int key[RC2_KEY_SIZE_WORDS ];
	} RC2_KEY;


void rc2keyInit( RC2_KEY *rc2key, BYTE *userKey, int length, int reducedLength );

void rc2encrypt( RC2_KEY *rc2key, BYTE *buffer );

void rc2decrypt( RC2_KEY *rc2key, BYTE *buffer );

/******************************************************************************/
/**************** VISUAL BASIC HANDLING FUNCTION FOR ITEMS ********************/
/******************************************************************************/

void Get_Item_Stream (
       ITEM *Item,            /* ITEM to copy                       */
       unsigned char *String  /* Target Buffer (VB String)          */
     );

int GetItem (
      unsigned char *Buffer,  /* ITEM array                         */
      int Index,              /* ITEM within array to copy (0..n-1 )*/
      unsigned char *String   /* Target Buffer (VB String)          */
    );


/******************************************************************************/
/*************************** CRL FUNCTIONS (X.509) ****************************/
/******************************************************************************/

/* CRL REVOCATION CODES */
#define UNSPECIFIED                0
#define KEY_COMPROMISE             1
#define CA_COMPROMISE              2
#define AFFILIATION_CHANGED        3
#define SUPERSEDED                 4
#define CESSATION_OF_OPERATION     5
#define CERTIFICATE_HOLD           6
#define REMOVE_FROM_CRL            8

/* CRL ERROR CODES */
#define CRL_ALG_INCONSISTENCY    101
#define CRL_UNEXPECTED_ERROR     102
#define CRL_ERROR_NOCONTEXT      103
#define CRL_ERROR_DEC_CERT       104
#define CRL_ERROR_NOMEMORY       105
#define CRL_INVALID_FORMAT       106
#define CRL_ERROR_WPARAMS        107
#define CRL_ERROR_ENCRYPT        108
#define CRL_ERROR_IOCTX          109
#define CRL_ERROR_NOUTC          110
#define CRL_ERROR_RFILE          111
#define CRL_NO_LIST              112
#define CRL_SUCCESS              100

int CrlInit (
      IOCTX *Target,                   /* List IOCtx              */
      char *TargetFile                 /* List physical File Name */
    );

int CrlUpdate (
      IOCTX *Target,                   /* List IOCtx             */
      ITEM *Serial,                    /* Serial Number to Add   */
      unsigned char *RevDate,          /* Revocation Date        */
      unsigned char *Reason            /* Reason Code            */
    );

int CrlFinal (
      IOCTX *List,                     /* List IOCtx             */
      ITEM *Cert,                      /* Issuer certificate     */
      ITEM *CrlNum,                    /* CRL Serial Number      */
      unsigned char *thisUpdate,       /* Issuing UTC Date       */
      unsigned char *nextUpdate,       /* Next Issuing UTC Date  */
      int SigAlg,                      /* Signature Algorithm    */
      R_RSA_PRIVATE_KEY *PrivateKey,   /* Issuer Private Key     */
      char *TgtFile                    /* Final CRL File         */
    );

int Crl_Authenticate (
      IOCTX *Source,                   /* Source Crl IOCtx                */
      IOCTX *Target,                   /* Target Searching List IOCtx     */
      ITEM CrlIssuerNames[DNITEMS],    /* CRL Issuer Names                */
      ITEM IssuerIssuerNames[EAITEMS], /* Issuer Names from CRL Issuer    */
      ITEM *AuthoritySerial,           /* CRL Issuer Certificate Serial   */
      ITEM *CrlNum,                    /* CRL Number                      */
      R_RSA_PUBLIC_KEY *PublicKey,     /* Public Key to validate Sgtr     */
      int *Authentic                   /* Signature Validation Result     */
    ) ;

int Crl_Open_List (
      IOCTX *Disk,                     /* IOCtx List                               */
      char *PathList                   /* List File Name, if linked to a file name */
    );

int Crl_Close_List (
      IOCTX *Disk                   /* List IOCtx            */
    );

int Crl_Find_Entry (
      IOCTX *List,                  /* List IOCTX            */
      ITEM *Serial,                 /* Serial to Search      */
      unsigned char *Date,          /* Revocation Date       */
      unsigned char *reason         /* Revocation Reason     */
    );

int Crl_Get_Dates (
      IOCTX *List,                  /* List IOCTX            */
      unsigned char *IssuingDate,   /* CRL Issuing Date      */
      unsigned char *NextIssuing    /* Next CRL Issuing Date */
    );

int Crl_Get_One_Entry (
      IOCTX *List,                  /* Crl IOCTX             */
      ITEM *Serial,                 /* Certificate Serial    */
      unsigned char *Date,          /* Revocation Date       */
      unsigned char *reason         /* Revocation Reason     */
    );

/******************************************************************************/
/**************** SIMPLIFIED API FOR B64 <-> BIN CONVERTIONS  *****************/
/******************************************************************************/

int PEM_BINTOB64 (
      unsigned char *src,   /* Source Block      */
      int srcSize,          /* Source Block Size */
      unsigned char *tgt,   /* Target Block      */
      int tgtSize           /* Target Block Size */
    );

int PEM_B64TOBIN (
      unsigned char *src,   /* Source Block (B64 W/no end of string) */
      int srcSize,          /* Source Block Size                     */
      unsigned char *tgt,   /* Target Block                          */
      int tgtSize           /* Target Block Size                     */
    );


/******************************************************************************/
/******* ADITIONAL FUNCTIONS TO FREE ALLOCATED MEMORY POINTED BY ITEMS ********/
/******************************************************************************/

void CleanArray (
       ITEM *Name,   /* ITEM array to free */
       int size      /* Array size         */
     );

void CleanVariable (
       ITEM Name,    /* ITEM to free       */
       int size      /* Always 1           */
     );

#ifdef __cplusplus
}
#endif

#endif
