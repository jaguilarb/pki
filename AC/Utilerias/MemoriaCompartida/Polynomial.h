#ifndef _POLYNOMIAL_H_
#define _POLYNOMIAL_H_
static const char* _POLYNOMIAL_H_VERSION_ ATR_USED = "MemoriaCompartida @(#)"\
"DSIC07412AC_ 2007-12-14 Polynomial.h 1.1.0/0";

//#VERSION: 1.1.0

#include <assert.h>
#include <iostream>

template <class NUMBER>
class Polynomial;

template <class T>
std::ostream& operator<<(std::ostream& o, const Polynomial<T>& poly);


// A polynomial, with coefficients composed of the type passed as NUMBER.  
// This is intended to support bignum-style classes.  These classes must,
// however, support integer assign and compare operations.  This class is 
// not optimized for extremely large polynomials (degree overflowing an int,
// for example), or for sparse polynomials.

template <class NUMBER>
class Polynomial {
public:
  Polynomial<NUMBER>(const Polynomial& b);
  Polynomial<NUMBER>(int degree =0);

  ~Polynomial();

  Polynomial<NUMBER>& operator=(const Polynomial<NUMBER>& b);

  void setCoefficient(int coeff, const NUMBER& val);
  NUMBER getCoefficient(int coeff) const;

  int getDegree() const;

  // Evaluate f(x).
  NUMBER operator[](NUMBER x) const; 

  Polynomial<NUMBER> operator*(const Polynomial<NUMBER>& b) const;
  Polynomial<NUMBER> operator/(const Polynomial<NUMBER>& b) const;
  Polynomial<NUMBER> operator+(const Polynomial<NUMBER>& b) const;
  Polynomial<NUMBER> operator-(const Polynomial<NUMBER>& b) const;
  Polynomial<NUMBER> operator%(const Polynomial<NUMBER>& b) const;

protected:
  int degree;
  int allocated;
  NUMBER* coefficient;

  // Determine the actual degree of the polynomial
  void reduce();
  
//  template <class NUMBER> 
  friend std::ostream& operator<< <>(std::ostream& o, const Polynomial<NUMBER>& poly);
};

#include "Polynomial.inl"

#endif
