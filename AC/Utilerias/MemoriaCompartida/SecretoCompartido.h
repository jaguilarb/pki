static const char* _SECRETOCOMPARTIDO_H_VERSION_ ATR_USED = "MemoriaCompartida @(#)"\
"DSIC07412AC_ 2007-12-14 SecretoCompartido.h 1.1.0/0";

//#VERSION: 1.1.0
// SharedSecret.h: interface for the CSharedSecret class.
//
//////////////////////////////////////////////////////////////////////


#include <SgiTipos.h>
#include "sglib.h"
#include "Polynomial.h"
#include "GF256.h"
#include "Point.h"


class CSecretoCompartido
{
   public:
      uint8 NKeys;
      uint8 RKeys;

	   uint8** Keys;
	   uint16  LKeys;
      uint16  DespKeys;

      R_RANDOM_STRUCT* pstRandom;

      bool keyUsed(int* usedKeys, int index, int x) const;

      CSecretoCompartido(uint8 nkeys, uint8 rkeys, R_RANDOM_STRUCT*	random);
	   virtual ~CSecretoCompartido();

      bool IniLlaves(uint16 lng);

#ifndef _GSI_LINUX_
      void codifica(uint8* bloque, uint16 lngbloque);
      void codifica(uint8 ch);
      void createKeys(const Polynomial<GF256>& p);
#endif

      bool decodifica(uint8* privKeySS[], uint16 lprivKeySS, uint8* out);
      bool decodifica(uint8* out, Point<GF256>* inputs);
};

