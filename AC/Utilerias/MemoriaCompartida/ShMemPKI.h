static const char* _SHMEMPKI_H_VERSION_ ATR_USED = "MemoriaCompartida @(#)"\
"DSIC07412AC_ 2007-12-14 ShMemPKI.h 1.1.0/0";

//#VERSION: 1.1.0
/****************************************************
 * Rutinas para el control y administracion de los  *
 * servicios de seguridad.                          *
 * Febrero 2004.                                    *
 ****************************************************/
#include "ShMem.h"
#include "AdmSegPKI.h"

typedef struct serv_info_st //Informacion pertinente sobre el estado del servicio de seguridad (Llave privada)
{
   char clave[ID_SRV];
   char numSerie[NUM_SERIE];
   char vigIni[13];
   char vigFin[13];
   uint8 privKey[LNG_PRIVKEY];
}ServSegInfo;

typedef struct reg_serv_pki_st //Registro de servicios en la Memoria (shared memory)
{
   uint8 numApls;
   ServSegInfo idserv[256];
}RegServPKI;

class CSHMemPKI : public CSHMem
{
   protected:
      virtual void  _Inicializa();

   public:
      CSHMemPKI(key_t key, int32 tam);
      virtual ~CSHMemPKI();
      uint8           getNumApls();
      ServSegInfo&    getServInfo(uint8 id);
      bool            bajaServ(char* servicio);
      uint8           findServ(char* servicio);
      bool            registraServ(ServSegInfo* pk);
};
