static const char* _SHMEM_CPP_VERSION_ ATR_USED = "MemoriaCompartida @(#)"\
"DSIC07412AC_ 2007-12-14 ShMem.cpp 1.1.0/0";

//#VERSION: 1.1.0
#include <iostream>
#include <errno.h>
#include <error.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <SgiTipos.h>
#include "ShMem.h"

using namespace std;
using std::cout;
using std::cin;
using std::endl;

CSHMem::
CSHMem(key_t k, int32 t)
{
   Key = k;
   Tam = t;
   idx = fail;
   Shmemb = (uint8*)fail;
}

CSHMem::
~CSHMem()
{
   cierra();
}

bool CSHMem::cierra(bool eliminar /*false*/)
{
   error = 0;
   if(Shmemb != (uint8*)fail)
   {
      if(shmdt(Shmemb) != -1)
         Shmemb = (uint8*)fail;
      else
         error = errno;
   }
   if(eliminar && idx != fail)
   {
      if(shmctl(idx, IPC_RMID, NULL) != 1)
         idx = -1;
      else
         error = errno;
   }
   return error != 0;
}

bool CSHMem::abre(bool crear /*false*/)
{
    error = 0;
    bool nuevo = false;
    idx = shmget(Key, Tam, IPC_EXCL);
    if(idx == fail && crear)
    {
       idx = shmget(Key, Tam, IPC_CREAT | IPC_CREAT | 0640);
       nuevo = true;
    }
    if(idx != fail)
       Shmemb = (uint8*)shmat(idx, 0, 0);
    if(idx == fail || Shmemb == (uint8*)fail)
    {
#ifdef DBG
       if(idx == fail)
          cout << "Error en shmget. Al crear el segmento." << endl;
       if(Shmemb == (uint8*)fail)
          cout << "Error en shmmat. Al adjuntar el segmento." << endl;
#endif /*DBG*/
       error = errno;
       return false;
    }
    if(nuevo)
       _Inicializa();
    return true;
}

