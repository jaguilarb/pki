static const char* _SECRETOCOMPARTIDO_CPP_VERSION_ ATR_USED = "MemoriaCompartida @(#)"\
"DSIC07412AC_ 2007-12-14 SecretoCompartido.cpp 1.1.0/0";

//#VERSION: 1.1.0
// SecretoCompartido.cpp: implementation of the CSecretoCompartido class.
//
//////////////////////////////////////////////////////////////////////

#include <iostream>
#include <cstdio>
#include <cstdlib>

#include <memory.h>

#include <vector>
//#include <algobase.h>

#include <SgiTipos.h>
#include "SecretoCompartido.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
CSecretoCompartido::CSecretoCompartido(uint8 nkeys, uint8 rkeys, R_RANDOM_STRUCT* random)
{
	NKeys = nkeys;
	RKeys = rkeys;

	Keys = NULL;
	LKeys = 0;
	DespKeys = 0;

	pstRandom = random;
}


CSecretoCompartido::~CSecretoCompartido()
{
	if (Keys)
	{
		for (int i = 0; i < NKeys; i++)
		{
			if (Keys[i])
			{
				memset(Keys[i], 0, LKeys);
				delete[] Keys[i];
				Keys[i] = NULL;
			}
		}
		delete[] Keys;
		Keys = NULL;
	}

}


bool CSecretoCompartido::IniLlaves(uint16 lngbloque)
{
	bool ok = true;
   lngbloque *= 2;
	int  lngext = lngbloque < 128 ? 1 : lngbloque < 256 ? 2 : 3;

	LKeys = lngbloque + 1 + lngext; 

	Keys = new uint8*[NKeys];
	if (!Keys)
		return false;

	memset(Keys, 0, sizeof(uint8*) * NKeys);
	for (int i = 0; ok && i < NKeys; i++)
	{
		Keys[i] = new uint8[LKeys];
		ok = Keys[i] != NULL;
		if (ok)
		{
			Keys[i][0] = 0x30;
			switch (lngext)
			{
				case 1:
						Keys[i][1] = (uint8) lngbloque; break;
				case 2:
						Keys[i][1] = 0x81;
						Keys[i][2] = (uint8) lngbloque; break;
				case 3:
						Keys[i][1] = 0x82;
						Keys[i][2] = (uint8) (lngbloque >> 8);
						Keys[i][3] = (uint8) (lngbloque & 0x00FF);
			}
		}
	}
	DespKeys = lngext + 1;

	return ok;
}

#ifndef _GSI_LINUX_
void CSecretoCompartido::codifica(uint8* bloque, uint16 lngbloque)
{
	bool ok = IniLlaves(lngbloque);
	
	for (uint16 i = 0; i < lngbloque; i++)
		codifica(bloque[i]);
}


void CSecretoCompartido::codifica(uint8 ch)
{
  Polynomial<GF256> p(NKeys - 1);

  p.setCoefficient(0, GF256(ch));

  for (int i = 1; i <= NKeys - 1; i++) 
  {
    uint8 ch;
    R_GenerateBytes(&ch, 1, pstRandom);
    p.setCoefficient(i, GF256(ch));
  }

  createKeys(p);
}

void CSecretoCompartido::createKeys(const Polynomial<GF256>& p)
{
   int* usedKeys = new int[NKeys];

   for (int i = 0; i < NKeys; i++) 
   {
      uint8 ch;
      GF256 x, y;

      do 
      {
         R_GenerateBytes(&ch, 1, pstRandom);
         x = GF256(ch);

         if (x != 0)
	         y = p[x];
      } while ((x == 0) || keyUsed(usedKeys, i - 1, x));

      usedKeys[i] = x;

      Keys[i][DespKeys] = x;
      Keys[i][DespKeys + 1] = y;
   }

   DespKeys += 2;
   delete[] usedKeys;
}
#endif


bool CSecretoCompartido::keyUsed(int* usedKeys, int index, int x) const
{
   for (int i = 0; i <= index; i++) 
   {
      if (usedKeys[i] == x) 
      {
         return true;
      }
   }

   return false;
}

bool CSecretoCompartido::decodifica(uint8* privKeySS[], uint16 lprivKeySS, uint8* out)
{
  Point<GF256>* p = new Point<GF256>[RKeys];
  GF256 x, y;
  bool ok = true;

  for (uint16 i = 0; ok && i < lprivKeySS; i += 2)
  {
      for (int j = 0; j < RKeys; j++) 
      {
         x = GF256((int) privKeySS[j][i]);
         y = GF256((int) privKeySS[j][i+1]);
      
      	p[j].set(x, y);
      }
      if (!decodifica(out + (i / 2), p))
         ok = false;
   }
   delete[] p;
   return ok;
}


bool CSecretoCompartido::decodifica(uint8* out, Point<GF256>* inputs)
{
   LagrangePolynomialInterpolation<GF256> LPI;

   for (uint8 i = 0; i < RKeys; i++)
      LPI.addPoint(inputs[i]);

   if (!LPI.uniquePoints())
      return false;

   // This really should be LPI.fit(0), but for some reason, that doesn't
   // work (yet?).

   // >>> HOA TMP
	std::cout << "decodifica puntos: ";
	for (int hi = 0; hi < RKeys; hi++)
	   std::cout << inputs[hi] << " ";
	std::cout << std::endl;
   // <<< HOA TMP

   GF256 fit = LPI.fit()[0];

   *out = (uint8) ((int) fit);

   // >>> HOA TMP
	std::cout << "\tResultado = " << int(*out) << std::endl;
   // <<< HOA TMP


   return true;
}
