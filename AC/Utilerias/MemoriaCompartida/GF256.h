#ifndef _GF256_H_
#define _GF256_H_
static const char* _GF256_H_VERSION_ ATR_USED = "MemoriaCompartida @(#)"\
"DSIC07412AC_ 2007-12-14 GF256.h 1.1.0/0";

//#VERSION: 1.1.0

class GF256 {
public:
  GF256(int val=0);

  GF256& operator=(const GF256& b) {
    polynomial = b.polynomial;
    return *this;
  }

  GF256 operator+=(const GF256& b) {
    *this = *this + b;
    return *this;
  }

  GF256 operator-=(const GF256& b) {
    *this = *this - b;
    return *this;
  }

  GF256 operator*=(const GF256& b) {
    *this = *this * b;
    return *this;
  }

  GF256 operator/=(const GF256& b) {
    *this = *this / b;
    return *this;
  }

  GF256 operator+(const GF256& b) const;
  GF256 operator-(const GF256& b) const;
  GF256 operator*(const GF256& b) const;
  GF256 operator/(const GF256& b) const;

  GF256 operator-() const;

  int pow() const;

  operator int() const { 
    return polynomial;
  }

private:
  int polynomial;
};

#endif // _GF256_H_
