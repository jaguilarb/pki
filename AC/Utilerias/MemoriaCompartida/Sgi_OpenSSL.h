#ifndef _SGI_OPENSSL_H_
#define _SGI_OPENSSL_H_
static const char* _SGI_OPENSSL_H_VERSION_ ATR_USED = "@(#) SgiCripto (W : DSIC11364SC_ : Sgi_OpenSSL.h : 1.1.2: 5: 11/09/01)";

//#VERSION: 1.1.1
/*##############################################################################
  ###  PROYECTO:              SgiOpenSSL                                     ###
  ###  MODULO:                Funciones criptograficas para las distintas    ###
  ###                         aplicaciones que asi lo requieran              ###
  ###                                                                        ###
  ###  DESARROLLADORES:       Amilcar Guerrero Zu�iga         AGZ            ###
  ###                                                                        ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       Jueves 17, noviembre del 2005                  ###
  ###                                                                        ###
  ##############################################################################
         1         2         3         4         5         6         7         8
12345678901234567890123456789012345678901234567890123456789012345678901234567890

################################################################################
  VERSION:
     V.1.0.0  (20051116 - ) AGZ: Version Original
                            Esta biblioteca tiene como objetivo  proveer a
                            las aplicaciones que asi lo requieran funciones
                            de criptografia.
     V.1.1.1  (20090113 - ) MAML: Se corrige la funcion ASN1_ascii_to_utf8()
     V.1.1.2  (20110901 - ) MAML: incorporacion de _WIN64 para compilar a 64 bits
#################################################################################*/

/*------------------------------------------------------------------------------*/
#include <SgiTipos.h>
#if defined (_GSI_HPUX_) || defined(_GSI_LINUX_)
   #include <sys/types.h>
   #include <unistd.h>
   #define FILE_ERR            "/var/local/SAT/PKI/SgiOpenSSL.log"
#else
   //Para que no incluya _WINSOCKAPI_ y trabaje con WINSOCK2
   #define _WINSOCKAPI_
   
   #include <windows.h>

   #define FILE_ERR            "SgiOpenSSL.log"
    
#endif //_GSI_HPUX_

#if defined (_WIN64)
   #include <winsock2.h>  // MAML: Nov 2010
#endif // or WIN64

/*------------------------------------------------------------------------------*/
#include <fstream>
#include <sys/stat.h>     //Tama�o de los archivos y otras funciones
#include <string>
#include <time.h>
/*------------------------------------------------------------------------------*/
extern "C"{
#include <openssl/asn1.h>
#include <openssl/bio.h>
#include <openssl/crypto.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <openssl/pem.h>
#include <openssl/pkcs7.h>
#include <openssl/pkcs12.h>
#include <openssl/rsa.h>
#include <openssl/x509.h>
#include <openssl/x509v3.h>
#include <openssl/obj_mac.h>
#include <openssl/rand.h>
#include <openssl/objects.h>
}
//using namespace::std;
/*------------------------------------------------------------------------------*/
//Definiciones para manejo de tamaño de los algoritmos RSA y otros
#define TAM_MAX_MODULO_BITS 4096
#define MAX_MODULO_LEN ((TAM_MAX_MODULO_BITS + 7)/8)
#define MAX_PRIMO_BITS ((TAM_MAX_MODULO_BITS + 1)/2)
#define MAX_PRIMO_LEN ((MAX_PRIMO_BITS + 7)/8)
#define MAX_FIRMA_LEN MAX_MODULO_LEN
#define TAM_MAX_REQCERT (1024*4)
#define MAX_TAMCERT (8 * 1024)
#define ANIO (365 * SEG_DIA)
#define SEG_DIA (60*60*24)
#define MES (SEG_DIA * 30)
#define HORA (60 * 60)
#define TAM_MAX_FECHA 13     // MAML 070618: Tama�o maximo de la fecha para version 0.9.8e
/*------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------*/
#define DER_FORMAT                  1 
#define PEM_FORMAT                  2 
//Flags de algoritmos de encripcion soportados
enum{
//Algoritmos de digestion
	MD5_ALG=0,		/*Algoritmo MD5*/
	MD4_ALG=1,		/*Algoritmo MD4*/
	MD2_ALG=2,		/*Algoritmo MD2*/
	SHA_ALG=3,		/*Algoritmo SHA*/
	SHA1_ALG=4,		/*Algoritmo SHA1*/
	RIPE160_ALG=5,	/*Algoritmo RIPEMD160*/
	SHA224_ALG=6,   /*Algoritmo sha 224 bits*/
	SHA256_ALG=7,   /*Algoritmo sha 256 bits*/
	SHA384_ALG=8,   /*Algoritmo sha 385 bits*/
	SHA512_ALG=9,   /*Algoritmo sha 512 bits*/
	MDC2_ALG=10,    /*Algoritmo MDC2 128 bits*/
//Algoritmos de encripcion
	DES_CBC_ALG=100,		/*Algoritmo de cifrado DES-CCB (Cipher Block Chaining)*/
	DES_EDE_ALG=101,		/*Algoritmo de cifrado DES-EDE*/
	DES_EDE3_ALG=102,		/*Algoritmo de cifrado triple DES-EDE3*/
	DES_CFB_ALG=103,		/*Algoritmo DES en cipher feedback*/
	DES_EDE_CFB_ALG=104,	/*Algoritmo triple DES con dos llaves en cipher feedback*/
	DES_EDE3_CFB_ALG=105,	/*Algoritmo triple DES con tres llaves en cipher feed back*/
	DES_OFB_ALG=106,		/*Algoritmo DES en output feedback*/
	DES_EDE_OFB_ALG=107,	/*Algoritmo triple DES con dos llaves en output feedback*/
	DES_EDE3_OFB_ALG=108,	/*Algoritmo triple DES con tres llaves en output feedback*/
	DES_EDE_CBC_ALG=109,	/*Algoritmo triple DES con dos llaves en cipher block chaining*/
	DES_EDE3_CBC_ALG=110,	/*Algoritmo triple DES con tres llaves en cipher block chaining*/
	DESX_CBC_ALG=111,		/*Algoritmo DES en cipher block chaining (variacion que soporta ataque de fuerza bruta)*/
	RC2_CBC_ALG=112,		/*Algoritmo de cifrado RC2 en cipher block chaining*/
	RC2_CFB_ALG=113,		/*Algoritmo de cifrado RC2 en cipher feedback*/
	RC2_OFB_ALG=114,		/*Algoritmo de cifrado RC2 en output feedback*/
	RC4_ALG=115,			/*Algoritmo de cifrado RC4*/
	RC4_40_ALG=116,			/*Algoritmo de cifrado RC4*/
	IDEA_CBC_ALG=117,		/*Algoritmo de cifrado IDEA en cipher block chaining*/
	IDEA_CFB_ALG=118,		/*Algoritmo de cifrado IDEA en cipher feedback*/
	IDEA_OFB_ALG=119,		/*Algoritmo de cifrado IDEA en output feedback*/
	BF_CBC_ALG=120,			/*Algoritmo de cifrado Blow fish en cipher block chaining*/
	BF_CFB_ALG=121,			/*Algoritmo de cifrado Blow fish en cipher feedcack*/
	BF_OFB_ALG=122,			/*Algoritmo de cifrado Blow fish en output feedback*/
	CAST5_CBC_ALG=123,		/*Algoritmo de cifrado CAST5 en cipher block chaining*/
	CAST5_CFB_ALG=124,		/*Algoritmo de cifrado CAST5 en cipher feedback*/
	CAST5_OFB_ALG=125,		/*Algoritmo de cifrado CAST5 en output feedback*/
	RC5_CBC_ALG=126,		/*Algoritmo de cifrado RC5 en cipher block cahining*/
	RC5_CFB_ALG=127,		/*Algoritmo de cifrado RC5 en cipher feedback*/
	RC5_OFB_ALG=128,		/*Algoritmo de cifrado RC5 en output feedback*/
	AES_128_CBC_ALG=129,	/*Algoritmo de cifrado AES 128 en cipher block chaining*/
	AES_128_CFB_ALG=130,	/*Algoritmo de cifrado AES 128 en cipher feedback*/
	AES_128_OFB_ALG=131,	/*Algoritmo de cifrado AES 128 en output feedback*/
	AES_192_CBC_ALG=132,	/*Algoritmo de cifrado AES 192 en cipher block chaining*/
	AES_192_CFB_ALG=133,	/*Algoritmo de cifrado AES 192 en cipher feedback*/
	AES_192_OFB_ALG=134,	/*Algoritmo de cifrado AES 192 en output feedback*/
	AES_256_CBC_ALG=135,	/*Algoritmo de cifrado AES 256 en cipher block chaining*/
	AES_256_CFB_ALG=136,	/*Algoritmo de cifrado AES 256 en cipher feedback*/
	AES_256_OFB_ALG=137	/*Algoritmo de cifrado AES 256 en output feedback*/
};
/* ----------------------------------------------------------------------- */
/* ----------------------------------------------------------------------- */
/* ----------------------------------------------------------------------- */
//Flags de PKCS7 
#define PKCS7_FIRMA                 1
#define PKCS7_ENSOBRETA             2 
#define PKCS7_FIRMAENSOBRETA        3
/* ----------------------------------------------------------------------- */
#define ID_RFC			503	// RFC contenido en el certificado / requerimiento
#define ID_CN			13	// CommonName del contribuyente 
#define ID_ORG			17	// Nombre del contribuyente
#define ID_ORGU			18	// Nombre de la unidad del contribuyente
#define ID_EMAIL		48	// Email Address del contribuyente
#define ID_NAME			173	// Nombre completo del contribuyente (64+)
#define ID_CURP			105	// Curp del Contribuyente

#define ID_CHPWD        54	// Challenge Password del requerimiento

#define ID_REQ		1
#define ID_CER		2

#define ID_DER		3
#define ID_PEM		4
/* ----------------------------------------------------------------------- */
/* ----------------------------------------------------------------------- */
//Definiciones de errores de la interfaz Sgi_OpenSSL
#define ERR_ASG_MEM_OBJ						-40
#define ERR_ASG_VAL_OBJ						-41 
#define ERR_PKCS_NOCOINCIDELLAVE			-42
#define ERR_PKCS_NODECODCERT				-43
#define ERR_PKCS7_DEC_OBJ					-44 
#define ERR_PKCS_CREAARCH					-45
#define ERR_RSA_NODECODLLAVE				-46
#define ERR_RSA_LLAVEINSEGURA				-47
#define ERR_RSA_ENCRIPCION					-48 
#define ERR_RSA_DESENCRIPCION				-49
#define ERR_CARGA_ARCHIVO					-50
#define ERR_PKCS_CARGAARCH					-51
#define ERR_RSA_FIRMA						-52
#define ERR_RSA_VERIFICA					-53
#define ERR_NO_DATOS_BUFFER					-54
#define ERR_PKCS_VERIFICACION				-55
#define ERR_PKCS_DESENCRIPCION				-56
#define ERR_NO_EST_SALIDA					-57
#define ERR_NO_CARGA_BITS					-58
#define ERR_NO_MODULO						-59
#define ERR_NO_EXP_PUB						-60
#define ERR_NO_EXP_PRIV						-61
#define ERR_NO_PRIMO						-62
#define ERR_NO_PRIMO2						-63
#define ERR_PRIME_EXP						-64
#define ERR_PRIME_EXP2						-65
#define ERR_NO_COEF							-66  
#define ERR_PKCS7_SET_CIPHER				-67
#define ERR_PKCS7_NO_ADD_RECIPIENT			-68
#define ERR_PKCS7_NO_ADD_SIGNATURE			-69
#define ERR_INIC_P7DATA						-70  
#define ERR_NO_DATOS_SUJETO					-71
#define ERR_RSA_NO_MEM_OBJ					-72
#define ERR_PKCS7_TIPO						-73
#define ERR_NO_INI_DIG						-74
#define ERR_DIG_CTX_NO_INI					-75
#define ERR_NO_PROCESA_BLOQUE				-76
#define ERR_NO_DIG							-77
#define ERR_CIP_ALG_NO_CARGADO				-78
#define ERR_NO_INICIA_ENC_CTX				-79
#define ERR_NO_TAM_DATOS_ENTRADA			-80
#define ERR_NO_ENCRIPTA_SEGMENTO			-81
#define ERR_NO_ENCRIPTA						-82
#define ERR_CIP_CTX_NO_INI					-83
#define ERR_DESC_CTX_NO_INI					-84
#define ERR_NO_DESENCRIPTA_SEGMENTO			-85
#define ERR_NO_DESENCRIPTA					-86
#define ERR_VOID_PARAM						-87
#define ERR_DER_PKEY_DECOD					-88
#define ERR_PEM_PKEY_DECOD					-89
#define ERR_PKEY_FRMT_NO_RECONOCIDO			-90
#define ERR_VAR_SER_NO_ESPACIO				-91
#define ERR_GET_NUM_SERIE					-92
#define ERR_CERT_NO_INI						-93
#define ERR_REQ_NO_INI						-94
#define ERR_NO_ID							-95
#define ERR_COD_PUBKEY_RSA					-96
#define ERR_NOT_GET_PUBKEY					-97
#define ERR_DECOD_DER_CERT					-98
#define ERR_NO_ESCRIBE_BIO_CERT				-99
#define ERR_DECOD_PEM_CERT					-100
#define ERR_ASG_MEM_BIO						-101
#define ERR_FORMATO_NO_RECONOCIDO			-102
#define ERR_ASG_MEM_X509					-103
#define ERR_TAM_VAR_VIGINI					-104
#define ERR_TAM_VAR_VIGFIN					-105
#define ERR_NO_CREA_ENTRY					-106
#define ERR_CREA_ATRIBUTO					-107
#define ERR_DEC_PUBKEY_RSA  				-108
#define ERR_NO_ADD_ATTR_REQ					-109
#define ERR_REQ_SET_PUBKEY					-110
#define ERR_NO_ASIGNA_LLAVE					-111
#define ERR_COD_REQ_FP  					-112
#define ERR_NO_CREA_REQ_FP					-113
#define ERR_NO_FIRMA_REQ					-114
#define ERR_NO_SET_VERSION_REQ				-115
#define ERR_NO_SET_PVKEY					-116
#define ERR_NO_CREA_BIO_REQ					-117
#define ERR_NO_GET_PUBKEY					-118
#define ERR_NO_ID_OBJ						-119
#define ERR_NO_OBJ_INI						-120
#define ERR_COD_CERT_BUFF_DER		    	-121
#define ERR_COD_CERT_BUFF_PEM 	    		-122
#define ERR_COD_CERT_FP_DER	    			-123
#define ERR_COD_CERT_FP_PEM		    		-124
#define ERR_NO_ASG_ALG_DIG					-125
#define ERR_NO_CREA_ARCHIVO_CERT			-126
#define ERR_NO_ABRE_ARCHIVO_PEM				-127
#define ERR_MEM_INSUFICIENTE				-128
#define ERR_NO_FIRMA_CERT					-129
#define ERR_NO_ASIGNA_ALG_DIG				-130
#define ERR_PROC_PRIVATE_KEY				-131
#define ERR_NO_ASIGNA_VIG_FIN				-132
#define ERR_NO_ASIGNA_VIG_INI				-133	
#define ERR_NO_ASIGNA_SERIAL				-134
#define ERR_NO_SET_PUBLIC_KEY				-135
#define ERR_NO_SET_ISSUER					-136
#define ERR_NO_SET_SUBJECT					-137
#define ERR_NO_SET_VERSION					-138
#define ERR_REQ_INCORRECTO					-139
#define ERR_NO_ABRE_CA_CERT					-140
#define ERR_EXT_CTX_NO_INI					-141
#define ERR_TAMANO_ARCHIVO_LLAVE			-142
#define ERR_NO_GEN_KEY						-143
#define ERR_NO_COD_PVKEY					-144
#define ERR_ASG_PVKEY_COD					-145
#define ERR_NO_ASG_CIPHER					-146
#define ERR_NO_INI_KEY						-147
//>>> MELM 25/Enero/2006: Defines de apoyo al manejo de errores
#define ERR_LEER_ARCHIVO					-148
#define ERR_ASG_MEM_BIO_BUF					-149
#define ERR_NO_ABRE_ARCHIVO					-150
#define ERR_DECOD_DER_REQ					-151
#define ERR_DECOD_DER_SIG_PUBKEY            -152
#define ERR_ASG_MEM_BIO_FP					-153
#define ERR_CREAR_PKCS7                     -154
#define ERR_TIPO_OPER_PKCS7                 -155
#define ERR_TAMANO_ARCHIVO                  -156
#define ERR_ASG_OBJ_EVP                     -157
#define ERR_COD_DER_REQ                     -158
#define ERR_TAMANO_BUFFER                   -159
#define ERR_CONV_UTF8						-160
//<<<
/*##############################################################################
   CLASE    : SNames
   PROPOSITO: Esta clase tiene como objetivo proveer los metodos necesarios para
              el manejo de los datos contenidos en las distintas estructuras que 
              se manejan dentro de la PKI.

   METODOS:
#################################################################################*/

class SNames
{
public:
	bool Iniciado;
	char *dato;
	char *id;
	int  idNid;
	bool Inicia();
	bool Termina();
	SNames();
	~SNames();
};
/*------------------------------------------------------------------------------*/
typedef struct certificados_st
{
	char *path;
}cadCerts;
/*------------------------------------------------------------------------------*/
typedef struct sgi_publicRSA_st
{
   unsigned short int bits;
   uchar moduloP[MAX_MODULO_LEN];
   uchar exponenteP[MAX_MODULO_LEN];
}SGIPUBLICA;
/*------------------------------------------------------------------------------*/
typedef struct sgi_privateRSA_st
{
   unsigned short int bits;
   uchar moduloP[MAX_MODULO_LEN];
   uchar exponenteP[MAX_MODULO_LEN];
   uchar exponentePv[MAX_MODULO_LEN];
   uchar primo[2][MAX_PRIMO_LEN];
   uchar primoExp[2][MAX_PRIMO_LEN];
   uchar coeficiente[MAX_PRIMO_LEN];   
}SGIPRIVADA;
/*------------------------------------------------------------------------------*/
typedef struct datosPKCS7_st
{
   struct pkcs7_signer_info_st *si;  //Informacion de los firmantes.
   bool fcontenido;        //true si esta contendio en el PKCS7. 1:1
   struct pkcs7_recip_info_st  *ri;  //Informacion de los recipientes.
   bool rcontenido;        //true si esta contenido en el PKCS7. 1:1
}P7Datos;
/*------------------------------------------------------------------------------*/
bool iniciaP7Datos(int num, P7Datos **p7);
void terminaP7Datos(int num, P7Datos *p7);
void PKCS7_SIGNER_INFO_dup(PKCS7_SIGNER_INFO *si, PKCS7_SIGNER_INFO *Si);
void PKCS7_RECIP_INFO_dup(PKCS7_RECIP_INFO *ri, PKCS7_RECIP_INFO *Ri);
void PKCS7_ISSUER_AND_SERIAL_dup(PKCS7_ISSUER_AND_SERIAL *pis, PKCS7_ISSUER_AND_SERIAL *Pis);

// Carga definiciones: flag = 1U (crypto), 2U (ssl), 3U ambas
extern void iniciaLibOpenSSL(uint flag = 1U);
extern void terminaLibOpenSSL();

/*------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------*/
#define int_error(msg) Maneja_ErrorOPSSL(__FILE__, __LINE__, msg)
/*------------------------------------------------------------------------------*/
void seed_prng(uchar *buff, int numbytes); //Funcion para sembrar las semillas aleatorias
#if defined (_GSI_HPUX_) || defined(_GSI_LINUX_)
	int seed_prng_1();
#endif//_GSI_HPUX

int ASN1_ascii_to_utf8(const char *cad_ori, char *cad_des, bool utf8_ascii = false);
int ASN1_ascii_to_utf8(const std::string& s_cad_ori, std::string *s_cad_des, bool utf8_ascii=false);

/*------------------------------------------------------------------------------*/
/*##############################################################################
   CLASE    : errManager
   PROPOSITO: Esta clase tiene como objetivo proveer los metodos necesarios para
              el manejo de los errores que provengan de la biblioteca libcryto
              y de las funciones propias de la biblioteca libSgiOpenSSL.

   METODOS:
#################################################################################*/

class errManager
{
protected:
	unsigned long openError;
public:
	errManager();
	~errManager();
	unsigned long getError(); //Errores de la libreria libcrypto
	bool strError(char *str, int *lstr); //String del error de libcrypto || libeay32
	intE armaError_(int baseError, int pos);

    //<+ ERGL (070227)
    //static unsigned long strErrorCompleto(char *); // MAML: Marca error en Windows
    unsigned long strErrorCompleto(char *);
    //+>
    intE armaError (int baseError, int pos); 

};

/*##############################################################################
   CLASE    : ManejaEVP_RSA
   PROPOSITO: clase para manejar el uso de la funcion EVP_PKEY_assign_RSA() junto con el objeto RSA
#################################################################################*/
class ManejaEVP_RSA
{
   intE     error;
   RSA      *par;
   bool     EVP_RSA;

public:
   EVP_PKEY *pkey;

   ManejaEVP_RSA();
   ~ManejaEVP_RSA();
   intE IniciaRSADup( RSA *pllaves);
   intE AsignaLlavePrivada_EVP(RSA *rsa);
};

/*##############################################################################
   CLASE    : Getllave
   PROPOSITO: Obtiene llaves privadas o publicas RSA junto con la funcion EVP_PKEY_get1_RSA()
#################################################################################*/
class Getllave
{
   EVP_PKEY *p_evp;
   RSA      *llave;
   intE     Error;

   intE Inicia();

public:
   Getllave();
   ~Getllave(); 
   intE GetllavePub(RSA **sPbK, X509*cert, X509_REQ *req);
   intE GetllavePri(char Formato, FILE *archivo, char *psw, RSA **rsaPvK);
};

/*##############################################################################
   CLASE    : SGIX509
   PROPOSITO: Esta clase tiene como objetivo proveer los metodos necesarios para
              el manejo de las etructuras X509 y PKCS10 
              (certificados y requerimientos).

   METODOS:
#################################################################################*/

class SGIX509 : public errManager
{
private:
	X509     *m_certProc;
	X509_REQ *m_reqProc;

	void     Inicia_NAME( SNames *names, ASN1_STRING *cade, bool es_sn = true);
	void     procX509_NAME(X509_NAME *name, SNames *names, int i_names);
	void     procX509_ATTR(X509_REQ *req, SNames *attributes, int i_attributes);
	int      genX509_NAME(int i_idObj, SNames *names, int i_names, bool estCertDig = false );
	int      genPKCS10Atributos(SNames *atributos, int i_atributos);
	int      putPublicKey(RSA *privateKey, ManejaEVP_RSA *evp_rsa); // EVP_PKEY **privada);
	int      getPrivateKey(const uchar *by_Privada, int i_Privada, uchar *by_Password, int i_Password, EVP_PKEY *Privada);
	int      getX509Cert(const uchar *by_Cert, int i_Cert);
	int      getPKCS10Req(const uchar *by_Req, int i_Req);
	bool     getVigencia(ASN1_TIME *time, char *psz_vig, int *i_vig ); 
	const EVP_MD* asignaAlgoritmoDig(int i_idAlg);
	int      escribeCert(X509 *Cert, int i_tipoCod, FILE *fp = NULL, unsigned char *byCert = NULL, int *i_byCert = 0);
	int      setVigenciaNvoCert(X509 *certNvo, long aniosVal);
	int      setExtensionesNvoCert(X509 *certNvo, SNames *ext, int i_ext);
	//int getX509Ext();
//  int      IniciagetTipo (X509  *cert, X509_NAME  *sujeto, X509_EXTENSION  *extension, BIO  *d_bio );
	int      IniciagetTipo (X509 **cert, X509_NAME **sujeto, X509_EXTENSION **extension, BIO **d_bio );
    int      TerminagetTipo(X509 *cert, X509_NAME *sujeto, X509_EXTENSION *extension, BIO *d_bio, int Error, ASN1_OBJECT *objnid = NULL );

public:
//>>>AGZ: 
	int      verificaReq();
	int		inicia(int i_idObj, unsigned char *by_Datos, int i_Datos);
	int     inicia(int i_idObj);
	void    termina();
	int     getVigIni(char *psz_vigIni, int i_vigIni);
	int     getVigFin(char *psz_vigFin, int i_vigFin);
	int     getSerial(char *psz_serial, int *i_serial); //Para la versi�n actual de los Certificados que utilizan cadena.
	int     getSubjectData(int i_idObj, SNames *subject, int i_subject);
	int     getAtributosReq(SNames *atributos, int i_atributos);
	int     getPublicKey(int i_idObj, uchar *by_publica, int *i_publica);
//<<<AGZ:
	intE GenREQ(char *narchivokey, char *narchivoreq, RSA *pllaves, SNames sujeto[], int nsujeto, 
		         SNames atributos[], int nattr, char *pass, int lnpass, int tipollave, int tipocifkey, 
		         int algdig = 0, bool estCertDig = false );
	intE GenREQ(uchar *buffreq, int *lbuffreq, RSA *pllaves, 
				   SNames sujeto[], int nsujeto, SNames atributos[], int nattr, int algdig, bool estCertDig = false );
	intE ProcREQ(const uchar *blqReq, int lnreq, SNames *names = NULL, SNames *atributos = NULL,
		RSA **rsaPbk = NULL, int sid[] = NULL, int nsNid = 0, int atid[] = NULL, int natNid = 0);
	intE ProcREQ_(const uchar *blqReq, int lnreq, SNames *names = NULL, SNames *atributos = NULL,
		RSA **rsaPbk = NULL, int sid[] = NULL, int nsNid = 0, int atid[] = NULL, int natNid = 0);
	intE ProcCERT(const uchar *cert, int lncert, SNames *snames = NULL, SNames *isnames = NULL, uchar *vigini = NULL, 
		uchar *vigfin = NULL, uchar *serial = NULL, RSA **sPbK = NULL, int sNid[] = NULL, int nnids = 0);
	intE GenCERT(const uchar *buffreq, int tamreq, char *narchCAcert, uchar *serial, unsigned long vigencia,
		RSA *CAPVK, SNames extensions[], int next, char *NArchivo = NULL, int tipocert = 0,
		uchar *Cert = NULL, int *lncert = NULL);
   intE GenREQ_REQ(const uchar*  ReqBase, int iReqBase,uchar *buffNvoreq, int *lbuffNvoreq, SNames sujeto[], int nsujeto,
                   SNames atributos[], int nattr, int algdig, bool estCertDig);
	intE ActualizaDataSubject( bool estCertDig = false );
   bool elimina_NID_REQ(int NID);
   void elimina_campos_REQ();
	intE GenCERTAC(const uchar *buffreq, int tamreq, char *cacert, uchar *serial, int lserial, unsigned long vigencia,
		RSA *CAPVK, SNames extensions[], int next, uchar *Cert, int *lncert, SNames sdata[], int nsdata, bool estCertDig = false );

	intE ObtienePbK (const uchar *buffcert, int lncert, RSA **sPbK, uchar *nSerie, char *narchivocert);
    //intE getLlavePub(RSA **sPbK, X509* cert, X509_REQ *req);
	intE ObtienePbK_(const uchar *buffcert, int lncert, RSA **sPbK, uchar *nSerie, char *narchivocert = NULL);
	
	intE ObtieneUID(const uchar *cert, int lncert, uchar *rfc); 
	intE TransCER(const uchar *bufcert, int lncert, char *narchPEM);
	intE pem2der(char *pathpem, char *pathder);
	intE getSerial(const uchar *cert, int lcert, char *serial); //Solo implementada para DER
	intE der2pem(const uchar *bufcert, int lncert, uchar *narchPEM, int *lpcert);
	int  getTipo(const uchar *bufcert, int lbufcert, int *tipo);
    //>>> ERGL (070222)
    void GenAuthorityKeyIdentifierEXTEN(X509 *, X509_EXTENSION **);
    //<<<
	SGIX509();
	~SGIX509();
};
/*##############################################################################
   CLASE    : SGIRSA
   PROPOSITO: Esta clase tiene como objetivo proveer los metodos 
              necesarios para el manejo de las etructuras RSA que representan 
              las llaves publicas y privadas.

   METODOS:
#################################################################################*/

class SGIRSA : public errManager
{
private:
//Contextos para encripcion y digestion por bloques de datos.
	EVP_PKEY        *RSAPvKey;
	EVP_MD_CTX      dig_ctx;
	EVP_CIPHER_CTX  cip_ctx;
	EVP_CIPHER_CTX	descip_ctx;
	int             cip_ctrl;
	int             descip_ctrl;
	bool            digCtxIni;
	bool            cipCtxIni;
	bool            descipCtxIni;
	int             codificaLlave(int i_idTipo, int i_idCipher, unsigned char *by_Passwd, int i_Passwd, RSA *rsa, FILE *fpp);
public:
	int   inicia();
	void  termina();
	//Funciones para firmar y calcular digestiones por bloques.	
	const EVP_CIPHER* asignaAlgoritmo(int id_alg);
	const EVP_MD *asignaAlgDigestion(int i_idAlg, int *i_idHash);
	intE iniciaDigestion(int id_alg);
	intE procesoBlqDig(uchar *by_Bloque, int i_Bloque);
	intE terminaDigestion(uchar *by_Digestion, int *i_Digestion);
	intE iniciaEncripcion(int id_alg, uchar *key, uchar *iv = NULL);
	intE procesoEncripcion(uchar *by_Bloque, int i_Bloque, uchar *salida, int *lsalida);
	intE terminaEncripcion(uchar *salida, int *lsalida);
	intE iniciaDesencripcion(int id_alg, uchar *key, uchar *iv = NULL);
	intE procesoDesencripcion(uchar *by_Bloque, int i_Bloque, uchar *by_Salida, int *i_Salida);
	intE terminaDesencripcion(uchar *by_Salida, int *i_Salida);
	
	intE  setPrivada(RSA *privada, SGIPRIVADA *privateK);
	intE  setPublica(RSA *publica, SGIPUBLICA *publicK);
	intE  getPrivada(SGIPRIVADA *privateK, RSA *privada);
	intE  getPublica(SGIPUBLICA *publicK, RSA *publica);

	int    getLlave(unsigned char *by_PrivKey, int i_PrivKey, unsigned char *by_Password, RSA **Llave);
	intE   GeneraLlaves(int num, RSA **rsallaves);
	intE   CodPvKey(RSA *rsa, uchar *psw, int pswlen, uchar *NArchivo, int tipocif, int formato);
	intE   CodPvKey1(RSA *rsa, uchar *psw, int pswlen, FILE *fpp, int tipocif, int formato);
	intE   DecodPvKey_BIO(char *NArchivo, char *psw, RSA **rsaPvK);
	intE   DecodPvKey    (char *NArchivo, char *psw, RSA **rsaPvK);
   //intE   DecodPvKey(uchar *by_Key, int i_Key, uchar *pszPassword, int i_Password, RSA **rsaKey);
	intE   EncPb(uchar *msg, int msglen, RSA *pbK, uchar *bfenc, int *bytesenc);
	intE   EncPv(uchar *msg, int msglen, RSA *pvK, uchar *bfenc, int *bytesenc);
	intE   DesencPb(uchar *msgcif, int msgciflen, RSA *pbK, uchar *bfdenc, int *bytesdes, bool padding);
	intE   DesencPv(uchar *msgcif, int msgciflen, RSA *pvK, uchar *bfdenc, int *bytesdes, bool padding);
	intE   RSAFirma(uchar *mens, uint msglen, RSA *SigPvKey, uchar *bffirma, uint *lnbuffer, int algdig = 0);
	intE   VerificaFirma(const uchar *firma, int firma_len, RSA *pbK, int algdig = 0, uchar *digestion = NULL, int *ldig = NULL);
   intE   VerificaFirmaP(const uchar* firma, int firma_len, const char* CertPath, int algdig, uchar *cadOrig = NULL, int* lcadOrig = NULL);
   intE   DigestAlg(uchar *mens, int lnmens, int algId, uchar *mdigest, int *bytesdig);
	intE   RSATrans(char *pathkey, char *password, int pslen, char *pathout, int formato, int cifkey);
	intE   GenAleatorio(int bits, char *sesionNo, int *res);
	intE   cmpKeys(RSA *key1, RSA *key2);
	SGIRSA();
	~SGIRSA();
    int BN_num_bits(const BIGNUM *a);
};	

/*##############################################################################
   CLASE    : SGIPKCS7
   PROPOSITO: Esta clase tiene como objetivo proveer los metodos necesarios para
              el manejo de las etructuras bajo el estandar PKCS7.

   METODOS:
#################################################################################*/

class SGIPKCS7 : public errManager
{
private:
	int    inicia(unsigned char *by_pkcs7, int i_pkcs7);

	int    getPKCS7(unsigned char *by_pkcs7, int i_pkcs7);
	int    codificaPKCS7(int i_idTipo, unsigned char *by_pkcs7, int i_pkcs7);
	int    genFirmado();
	int    genEnsobretado();
	int    genFirmadoEnsobretado();

    intE IniciaPKCS7(X509  **certificado,  ManejaEVP_RSA  **evp_rsa,    RSA            **llave,
		    		 PKCS7 **pkcs7,        X509_STORE **store,          STACK_OF(X509) **stack );
    intE TerminaPKCS7(X509  *certificado,  ManejaEVP_RSA  *evp_rsa,     RSA            *llave,
		              PKCS7 *pkcs7,        X509_STORE *store,           STACK_OF(X509) *stack,    intE Error,
					  FILE  *file = NULL, BIO *out = NULL,          BIO  *pkcs_data = NULL );
    intE IniciaGenPKCS7( uchar **buffer,    int            lnbuffer, X509 **signer, 
					  X509     **recipient, STACK_OF(X509) **stack,  BIO  **in     );
    intE TerminaGenPKCS7( uchar            *buffer, X509  *signer,  X509     *recipient,
					  STACK_OF(X509)       *stack,  BIO   *in,      intE Error,
					  FILE *file = NULL, FILE *datos = NULL, ManejaEVP_RSA *evp_rsa = NULL, RSA *rsa = NULL, PKCS7 *pkcs7 = NULL, BIO *Bio_Pem = NULL );
    intE IniciaGetDatosPKCS7 (PKCS7 **p7, X509 **certs, STACK_OF(X509) **stack );
    intE TerminaGetDatosPKCS7(PKCS7 *p7, X509 *certs, STACK_OF(X509) *stack, intE Error, FILE  *fp );

public:
	SGIPKCS7();
	~SGIPKCS7();
   intE procesaP7(int tipo, char *path, char *out, STACK_OF(X509) *cersk, char *key = NULL,
		char *pass = NULL);
   intE ProcesaPKCS7(char *narchivocert, char *narchivokey, char *password, char *narchivoens,
		char *narchivoout, char *CApath);
	intE GenPKCS7(char *sigcert, char *sigPvkey, char *pswkey, char *patharchin, int tipo, cadCerts recip[] = NULL, 
		int ncerts = 0, uchar *salida = NULL, char *pathout = NULL, int algCif = 0);
	intE GenPKCS7X(char *sigcert, char *sigPvkey, char *pswkey, char *patharchin, int tipo, const uchar *by_Destinatario = NULL, 
		int i_Destinatario = 0, uchar *salida = NULL, char *pathout = NULL, int algCif = 0);
	intE obtenDatos(char *archens, P7Datos **p7info, int *ndatos, int *tipo, STACK_OF(X509) **certsk);
};

/*##############################################################################
   CLASE    : SGIPKCS12
   PROPOSITO: Esta clase tiene como objetivo proveer los metodos necesarios para
              el manejo de las etructuras bajo el estandar PKCS12.

   METODOS:
#################################################################################*/

class SGIPKCS12 : public errManager
{
public:
	SGIPKCS12();
	~SGIPKCS12();
    intE GenPKCS12(char *pathcert, char *pathpvkey, char *password, char *pathout, char *alias = NULL,
                   cadCerts *CAs = NULL, int ncerts = 0);
    intE ProcPKCS12(char *pathpkcs12, char *password, char *pathcert = NULL, char *pathkey = NULL);
};

/*##############################################################################
   CLASE    : ManArchivo
   PROPOSITO: Esta clase tiene como objetivo auxiliar en las funciones en las 
              que se requiera abrir archivos para extraer la informacion, asi
              como crear archivos.

   METODOS:
#################################################################################*/

class ManArchivo
{
	std::fstream archivoS;
public:
    ManArchivo();
    bool CreaArchivo(uchar *blqDatos, int lblqDatos, const char *nArchivo);
    bool ProcesaArchivo(const char *nArchivo, uchar *data, int *bytes);
    bool TamArchivo(const char *pathArch, int *lnArch);
};
/*------------------------------------------------------------------------------*/
void Maneja_ErrorOPSSL(char *file, int lineno, char *msg);
/*------------------------------------------------------------------------------*/



#endif
