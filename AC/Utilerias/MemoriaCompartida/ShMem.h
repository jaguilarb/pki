static const char* _SHMEM_H_VERSION_ ATR_USED = "MemoriaCompartida @(#)"\
"DSIC07412AC_ 2007-12-14 ShMem.h 1.1.0/0";

//#VERSION: 1.1.0
/**************************************************
 * Rutinas para el manejo de memoriaz compartida. *
 * Febrero 2004.                                  *
 * ************************************************/

#include <SgiTipos.h>

#define success    0
#define fail      -1
#define MAX_KEY_SIZE  (4 * 1024)

class CSHMem
{
   protected:
      key_t  Key; //Clave del segmento...se puede obtener con ftok();
      int32  Tam; //Tama�o del segmento depende del procesador PAGE_SIZE intel = 4KB...
      int    idx; //idx identificador del segmento...
      uint8  *Shmemb; //Segmento de la memoria que se va a adjuntar a las aplicaciones...
      intE   error;
      virtual void _Inicializa() {};
   public:
      CSHMem(key_t k, int32 t); //Constructor de la clase...
      virtual ~CSHMem();
      bool abre(bool crear = false);
      bool cierra(bool eliminar = false);
      int getError(){return error;}
};
