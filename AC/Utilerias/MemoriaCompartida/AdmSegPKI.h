#ifndef _ADMSEGPKI_H_
#define _ADMSEGPKI_H_
static const char* _ADMSEGPKI_H_VERSION_ ATR_USED = "MemoriaCompartida @(#)"\
"DSIC07412AC_ 2007-12-14 AdmSegPKI.h 1.1.0/0";

//#VERSION: 1.1.0

#include <SgiTipos.h>
#include <Sgi_OpenSSL.h>

#define TamBufShMem    (4 * 1024 * 1024)
#define KEY_SHMEM      1979052003
#define LNG_PRIVKEY    (7 * 1024)
#define ID_SRV         16
#define NUM_SERIE      21
#define RKEYS          ((uint8)3)

struct RSA_PRIVADA 
{
   unsigned short int bits;
   uchar moduloP[MAX_MODULO_LEN];
   uchar exponenteP[MAX_MODULO_LEN];
   uchar exponentePv[MAX_MODULO_LEN];
   uchar primo[2][MAX_PRIMO_LEN];
   uchar primoExp[2][MAX_PRIMO_LEN];
   uchar coeficiente[MAX_PRIMO_LEN];
};

#endif //_ADMSEGPKI_H
