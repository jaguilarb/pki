template <class NUMBER>
inline Polynomial<NUMBER>::
Polynomial(const Polynomial& b) : degree(b.degree), allocated(b.allocated)
{
  coefficient = new NUMBER[allocated + 1];
  
  for (int i = 0; i <= allocated; i++)
    coefficient[i] = b.coefficient[i];
}

template <class NUMBER>
inline Polynomial<NUMBER>::
Polynomial(int degree /* =0 */) : degree(0), allocated(degree)
{
  coefficient = new NUMBER[degree + 1];
  
  // for (int i = 0; i <= allocated; i++)
  //   coefficient[i] = NUMBER(0);
}

template <class NUMBER>
inline Polynomial<NUMBER>::
~Polynomial()
{
  delete[] coefficient;
}

template <class NUMBER>
Polynomial<NUMBER>& 
Polynomial<NUMBER>::
operator=(const Polynomial<NUMBER>& b) 
{
  delete[] coefficient;

  degree = b.degree;
  allocated = b.allocated;
  coefficient = new NUMBER[allocated + 1];
  
  for (int i = 0; i <= allocated; i++)
    coefficient[i] = b.coefficient[i];
  
  return *this;
}

template <class NUMBER>
inline void Polynomial<NUMBER>::
setCoefficient(int coeff, const NUMBER& val) 
{
  assert((coeff >= 0) && (coeff <= allocated));
    
  coefficient[coeff] = val;

  reduce();
}

template <class NUMBER>
inline NUMBER Polynomial<NUMBER>::
getCoefficient(int coeff) const
{
  return coefficient[coeff];
}

template <class NUMBER>
inline int Polynomial<NUMBER>::
getDegree() const 
{
  return degree;
}

template <class NUMBER>
inline NUMBER Polynomial<NUMBER>::
operator[](NUMBER x) const 
{
  NUMBER exponent = 1;
  NUMBER sum = 0;

  for (int i = 0; i <= degree; i++) {
    sum += exponent * coefficient[i];
    exponent *= x;
  }
    
  return sum;
}

template <class NUMBER>
inline Polynomial<NUMBER> Polynomial<NUMBER>::
operator*(const Polynomial<NUMBER>& b) const
{
  Polynomial<NUMBER> result(degree + b.degree);

  for (int i = 0; i <= degree; i++) {
    for (int j = 0; j <= b.degree; j++) {
      result.setCoefficient(i + j, result.coefficient[i + j] +
			    coefficient[i] * b.coefficient[j]);
    }
  }

  return result;
}

template <class NUMBER>
inline Polynomial<NUMBER> Polynomial<NUMBER>::
operator/(const Polynomial<NUMBER>& b) const 
{
  Polynomial<NUMBER> result(getDegree() - b.getDegree());
  Polynomial<NUMBER> remainder(*this);
  int i, j;

  for (i = getDegree() - b.getDegree(), j = b.getDegree(); i >= 0; i--, j--) {

    NUMBER div = (remainder.getCoefficient(i + b.getDegree()) /
		  b.getCoefficient(b.getDegree()));

    Polynomial<NUMBER> sub(i);
    sub.setCoefficient(i, div);

    sub = sub * b;

    remainder = remainder - sub;

    result.setCoefficient(i, div);
  }

  return Polynomial<NUMBER>(result);
}

template <class NUMBER>
inline Polynomial<NUMBER> Polynomial<NUMBER>::
operator+(const Polynomial<NUMBER>& b) const 
{
  Polynomial<NUMBER> result;
    
  if (this->degree > b.degree) {
    result = *this;
    
    for (int i = 0; i <= b.degree; i++)
      result.setCoefficient(i, coefficient[i] + b.coefficient[i]);
  } else {
    result = b;

    for (int i = 0; i <= degree; i++) {
      result.setCoefficient(i, coefficient[i] + b.coefficient[i]);
    }
  }

  return result;
}

template <class NUMBER>
inline Polynomial<NUMBER> Polynomial<NUMBER>::
operator-(const Polynomial<NUMBER>& b) const 
{
  Polynomial<NUMBER> result;
    
  if (this->degree > b.degree) {
    result = *this;
    
    for (int i = 0; i <= b.degree; i++)
      result.setCoefficient(i, coefficient[i] - b.coefficient[i]);
  } else {
    result = b;

    for (int i = 0; i <= degree; i++) {
      result.setCoefficient(i, coefficient[i] - b.coefficient[i]);
    }
  }

  result.reduce();

  return Polynomial<NUMBER>(result);
}

template <class NUMBER>
inline Polynomial<NUMBER> Polynomial<NUMBER>::
operator%(const Polynomial<NUMBER>& b) const 
{
  Polynomial result = *this - b * (*this / b);

  return Polynomial<NUMBER>(result);
}

template <class NUMBER>
inline void Polynomial<NUMBER>::
reduce()
{
  for (int i = allocated; i >= 0; i--) {
    if (getCoefficient(i) != 0) {
      degree = i;
      return;
    }
  }

  degree = 0;
}

template <class NUMBER>
std::ostream& operator<<(std::ostream& o, const Polynomial<NUMBER>& poly)
{
  int printed = 0;

  for (int i = poly.degree; i >= 0; i--) {
    if (printed)
      o << " + ";

    o << poly.coefficient[i] << " * " << "x ^ " << i;
    printed++;
  }

  return o;
}

