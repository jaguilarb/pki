#ifndef _POINT_H_
#define _POINT_H_
static const char* _POINT_H_VERSION_ ATR_USED = "MemoriaCompartida @(#)"\
"DSIC07412AC_ 2007-12-14 Point.h 1.1.0/0";

//#VERSION: 1.1.0

// UGLY, but if we don't include <vector>, we don't get <algobase.h>, 
// and we potentially collide if we define our own min and max.
//#include <algobase.h>
#include <iostream>
#include <vector>

#if 0
template <class T> 
T min(const T& a, const T& b)
{
  return ((a < b) ? a : b);
}

template <class T> 
T max(const T& a, const T& b)
{
  return ((a > b) ? a : b);
}
#endif

template <class T> class Point;

template <class T>
std::ostream& operator<<(std::ostream& o, const Point<T>& p);

template <class T>
std::istream& operator>>(std::istream& i, Point<T>& p);


template <class T>
class Point {
public:
  Point() : 
    px(0), py(0) {}

  Point(const T& x, const T& y) :
    px(x), py(y) {}

  ~Point() {}

  T x() const { return px; }
  T y() const { return py; }

  Point<T>& min(const Point<T>& b) {
    px = std::min(px, b.px);
    py = std::min(py, b.py);
    return *this;
  }

  Point<T>& max(const Point<T>& b) {
    px = std::max(px, b.px);
    py = std::max(py, b.py);
    return *this;
  }

  Point<T> operator+(const Point<T>& b) const {
    return Point<T>(px + b.px, py + b.py);
  }

  Point<T> operator-(const Point<T>& b) const {
    return Point<T>(px - b.px, py - b.py);
  }

  Point<T>& operator-=(const Point<T>& b) {
    px -= b.px;
    py -= b.py;
    return *this;
  }

  Point<T>& operator+=(const Point<T>& b) {
    px += b.px;
    py += b.py;
    return *this;
  }

  Point<T> operator*(const Point<T>& b) const {
    return Point<T>(px * b.px, py * b.py);
  }

  Point<T>& operator*=(const Point<T>& b) {
    px *= b.px;
    py *= b.py;
    return *this;
  }

  Point<T> operator*(const T& b) const {
    return Point<T>(px * b, py * b);
  }

  Point<T>& operator*=(const T& b) {
    px *= b;
    py *= b;
    return *this;
  }

  Point<T> operator/(const Point<T>& b) const {
    return Point<T>(px / b.px, py / b.py);
  }

  Point<T>& operator/=(const Point<T>& b) {
    px /= b.px;
    py /= b.py;
    return *this;
  }

  Point<T> operator/(const T& b) const {
    return Point<T>(px / b, py / b);
  }

  Point<T>& operator/=(T b) {
    px /= b;
    py /= b;
    return *this;
  }

  bool operator==(const Point<T>& b) const {
    return ((px == b.px) && (py == b.py));
  }

  Point<T>& operator=(const Point<T>& b) {
    px = b.px;
    py = b.py;
    return *this;
  }

  void set(const T& x, const T& y) {
    px = x;
    py = y;
  }

private:
  T px, py;

  friend std::ostream& operator<< <>(std::ostream& o, const Point<T>& p);
  friend std::istream& operator>> <>(std::istream& i, Point<T>& p);
};

template <class T>
std::ostream& operator<<(std::ostream& o, const Point<T>& p)
{
  o << '(' << p.px << ',' << p.py << ')';

  return o;
}

template <class T>
std::istream& operator>>(std::istream& i, Point<T>& p)
{
  int c;

  // Continue searching until we find a '(' or what appears to be a 
  // number.
  do { 
    c = i.get();

    // If we run into what looks like the start of a number, unget it and
    // stop seeking.
    if ((c >= 0) && (isdigit(c) || (c == '.') || (c == '-'))) {
      i.unget();
      break;
    }

  } while ((c >= 0) && (c != '('));

  i >> p.px;

  // Continue searching until we find a ',' or a 'x' or the start of a 
  // number.
  do { 
    c = i.get();

    // If we run into what looks like the start of a number, unget it and
    // stop seeking.
    if ((c >= 0) && (isdigit(c) || (c == '.') || (c == '-'))) {
      i.unget();
      break;
    }

  } while ((c >= 0) && (c != ',') && (c != 'x'));

  i >> p.py;

  // Continue searching until we find a non-blank character, which should
  // be a ')'.  If it's another character, put it back.
  do { 
    c = i.get();

    if ((c >= 0) && !isspace(c)) {
      if (c != ')')
        i.unget();
      break; 
    }
  } while (c >= 0);

  return i;
}


#endif // _POINT_H_
