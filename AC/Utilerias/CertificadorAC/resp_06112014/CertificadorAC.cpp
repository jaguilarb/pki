static const char* _CERTIFICADORAC_CPP_VERSION_ ATR_USED = "@(#) CertificadorAC ( L : DSIC10482AC_ : CertificadorAC.cpp : 1.1.0 : 1 : 08/12/10)";
#define _SHK_CPP_

//#VERSION: 1.1.0
// MAML 101130: se valida el numero de AC y el tipo de certificado
#include <Sgi_SrvUnx.h>
#include <Sgi_ConfigFile.h>
#include <Sgi_ConfOpen.h>
#include <Sgi_B64.h>
#include <SgiCripto.h>
#include <Sgi_PKI.h>
#include <Sgi_LlaveMC.h>
#include <ExcepcionLlaveMC.h>
#include <netinet/in.h>

#include <openssl/bn.h>

using namespace std;
using namespace SgiCripto;

#include <SrvCertAC.h>
#include <CertificadorAC.h>

extern CSrvCertAC* ptrSrv;

CertificadorAC::
CertificadorAC( string &s_cert_firma ) :
   llave( NULL ),
   cert_firma(s_cert_firma),
   error(0),
   extCert(NULL)
{
   tipo_cert = "";   
   vigencia = VIGENCIA;
   memset(num_serie, 0, sizeof(num_serie));
}

CertificadorAC::
~CertificadorAC()
{
   if( sock )
      sock = NULL;
   //if( llave )
   //   llave = NULL;

   if ( extCert )
     delete []extCert;
   extCert = NULL;

   if ( llave )
      RSA_free( llave  );
   llave = NULL;
}

bool CertificadorAC::+
genera(const uint16 *header)
{  
   int   l_cert = 1024*10;
   uchar b_cert[l_cert];
   int   l_pkcs7; 

   l_pkcs7 = (int)ntohs(*header);
   Bitacora->escribePV(BIT_DEBUG, "CertificadorAC.genera: Se espera leer un PKCS7 con tama�o %u bytes.", l_pkcs7);
   uchar *b_pkcs7 = new uchar[l_pkcs7+1];
   if (b_pkcs7)
   {
      memset(b_pkcs7, 0, l_pkcs7+1);
      if( recibeCadena( l_pkcs7, &b_pkcs7 ) )
      {  
         int l_bpkcs7 = l_pkcs7;
         uchar *buf_pkcs7 = new uchar[l_bpkcs7+1]; 
         //-- uchar *buf_pkcs7 = new uchar[l_bpkcs7]; 
         if (buf_pkcs7)
         {   
            memset(buf_pkcs7, 0, l_bpkcs7+1);
            //-- memset(buf_pkcs7, 0, l_bpkcs7);
            if( verificaFirmaPKCS7( b_pkcs7, (int)l_pkcs7, (unsigned char*)buf_pkcs7, &l_bpkcs7) && 
                cargaDatosCert( (unsigned char*)buf_pkcs7, &l_bpkcs7)  )
            {
               // Crea el certificado solicitado y lo envia de regreso a la AR en formato DER.
               SGIX509 x509;

               memset(b_cert, 0, l_cert);  
               error = x509.GenCERT(buf_pkcs7, l_bpkcs7, (char *)certAC.c_str(), (unsigned char*)num_serie, vigencia, llave, extCert, nExtCert,
                                   //-- NULL, ID_DER, (uchar*)b_cert, &l_cert );
                                   //NULL, ID_DER, (uchar*)(b_cert+sizeof(uint32)) , &l_cert ); 
                                     NULL, ID_DER, (uchar*)(b_cert+sizeof(uint16)) , &l_cert );

               {
                  BIO *bio = BIO_new( BIO_s_file() );
                  BIO_write_filename( bio, (char *)"/tmp/certificado_temporal.cer" );
                  BIO_write( bio, b_cert, l_cert );
                  BIO_free( bio );
               }
               

               //Bitacora->escribePV(BIT_DEBUG, "Err(%d) genera(), l_bpkcs7=%d, certAC=%s, num_serie=%s, vigencia=%d, nExtCert=%d, l_cert=%d",
               //                                error, l_bpkcs7, certAC.c_str(), num_serie, vigencia, nExtCert, l_cert );
               if( !error )
               {
                  uint16 tam = (uint16) htons(l_cert);
                  memcpy(b_cert, &tam, sizeof(uint16)); //--

                  //--error = sock->Envia( (uint8 *)&tam, 2 );
                  //--error = sock->Envia( (uint8 *)b_cert, l_cert );
                  error = sock->Envia( (uint8 *)b_cert, l_cert+sizeof(uint16) ); //--

                  if (error) 
                  {
                     Bitacora->escribePV( BIT_ERROR, "Error en CertificadorAC en genera: Error(%d), al hacer el Envio del certificado al cliente.", error ); 
                     error = SOCK_ENVIA;
                  }
                  else
                     Bitacora->escribePV( BIT_INFO, "CertificadorAC.genera: Certificado(%d) generado y enviado al cliente, tipo(%s), AC(%d).",
                                                     l_cert, tipo_cert.c_str(), num_ac );
               }
               else  
                  Bitacora->escribePV( BIT_ERROR, "Error en CertificadorAC en genera: Error(%d), en la generaci�n del certificado ", error );
            }
            memset(buf_pkcs7, 0, l_bpkcs7+1);
            delete[] buf_pkcs7;
            buf_pkcs7 = NULL;
         }
         else
            Bitacora->escribePV(BIT_ERROR, "Error en CertificadorAC en genera: Error(%d) fatal de memoria(uchar) buf_pkcs7.", error = ERR_FALTA_MEMORIA);
      }
      memset(b_pkcs7, 0, l_pkcs7+1);
      delete[] b_pkcs7; 
      b_pkcs7 = NULL;
   }
   else
      Bitacora->escribePV(BIT_ERROR, "Error en CertificadorAC en genera: Error(%d) fatal de memoria(uchar).", error = ERR_FALTA_MEMORIA);

   if ( error && error != SOCK_ENVIA) // si hay error => regresa -1
   {
      //uint16 tam = -1;
      uint16 tam = (uint16) htons( (int)-1 );
      memcpy(b_cert, &tam, sizeof(uint16));
      if (sock)
      {
         error = sock->Envia( (uint8 *)b_cert, sizeof(uint16) );
         if (error)
            Bitacora->escribePV( BIT_ERROR, "Error en CertificadorAC en genera: Error(%d), al hacer el Envio de Error al cliente.", error );

      }
      else 
         Bitacora->escribePV( BIT_ERROR, "Error en CertificadorAC en genera: Error(%d), barrido de memory en 'sock'", error = 99 );
   }
   return !error;
}

bool CertificadorAC::
recibeCadena(const int longitud, uchar **buff)
{
   int len = (int)longitud;
   int lec = 0;
   do
   {
      if( !(error = sock->Recibe( (uint8 *)*buff + lec, len )) )
      {
         lec += len;
         len = longitud - lec;
      }
      else
      {
         Bitacora->escribePV(BIT_ERROR, "Error en CertificadorAC en recibeCadena: Ocurrio un error(%d) durante la lectura del buffer del PKCS7 recibido.", error);
         break;
      }
   } while( lec < longitud );
   return !error;
}


bool CertificadorAC::
atender( CSSL_Skt *skt )
{
   sock = skt;
   if( sock )
   {
      int l_buff = sizeof(uint16);  
      uint16 buff[2];
      memset(buff, 0, sizeof(buff) );
      error = sock->Recibe( (uint8 *)buff, l_buff );
      if( !error )
         genera(buff);
   }
   return !error;
}


// estructura enviada sea un PKCS7: numero de Serie|TipoCert|AC|vigencia|iden=valor^iden=valor^iden=valor^|Requerimiento en PEM
bool CertificadorAC::cargaDatosCert( uchar *buf_pkcs7, int *l_bpkcs7)
{
   // cargamos en una estructura de Token, los 3 valores para generar el Cert
   CSeparaToken *Tkn = NULL;

   Tkn = new CSeparaToken( (char *)buf_pkcs7, '|' );

   if ( Tkn != NULL )
   {
      if ( Tkn->getCounter() == 6 )
      {
         strcpy(num_serie, Tkn->items[0] );
         tipo_cert = Tkn->items[1];
         num_ac = atoi(Tkn->items[2]);
         if ( levantaLlave() )
         { 
            vigencia = atoi( Tkn->items[3] );
            cargaExtensiones( Tkn->items[4] );
            error = SgiUtils::B64(false, (const unsigned char*)Tkn->items[5], strlen(Tkn->items[5]), buf_pkcs7, l_bpkcs7 );
            if (error)
               Bitacora->escribePV(BIT_ERROR, "Error en CertificadorAC en cargaDatosCert: "
               "Error(%d), al decodificar el requerimiento:%s", error, SgiUtils::msgError( (tError)error)); 
         }
      }
      else
         Bitacora->escribePV(BIT_ERROR, "Error en CertificadorAC en cargaDatosCert: Error(%d) numero invalido de campos(%d) en el buffer: %s",
                                        error = ERR_NUMINV_CAMPOS, Tkn->getCounter(), buf_pkcs7);
      delete Tkn;
      Tkn = NULL;
   }
   else
      Bitacora->escribePV(BIT_ERROR, "Error en CertificadorAC en cargaDatosCert: Error(%d) falta de memoria en CSeparaToken", error = ERR_FALTA_MEMORIA);

   return !error;
}

bool CertificadorAC::levantaLlave()
{
   if (ptrSrv == NULL)
      Bitacora->escribePV(BIT_ERROR, "Error en CertificadorAC en levantaLlave: Error(%d) se perdio el puntero a la clase CSrvCertAC* ptrSrv", 
      error = POINTER_INVAL );
   //else if (num_ac == LLAVE_ANTERIOR && (tipo_cert != CERT_OCSP && tipo_cert != CERT_CRL) )
   //   Bitacora->escribePV(BIT_ERROR, "Error(%d) el tipo(%d) de certificado no se puede generar para esta numero_AC(%d)",
   //                                   error = CERTINVAL_X_AC, tipo_cert, num_ac );
   else if ( cargaParmLlave() && cargaLlave() && verificaLlaves() ) 
   {
      ptrSrv->obt_dataCert(certAC.c_str(), true );
      if (!error)
         Bitacora->escribePV(BIT_INFO, "CertificadorAC.levantaLlave: ruta de certificacion valida para el certificado:%s", certAC.c_str());
   }
   return !error; 
}

//iden=valor^iden=valor^iden=valor^
bool CertificadorAC::cargaExtensiones(char *buf_ext )
{
   int           i = -2, len;
   CSeparaToken *Tkn = NULL;

   Tkn = new CSeparaToken(buf_ext, '^' );

   if ( Tkn != NULL )
   {
      if ( Tkn->getCounter() > 0 )
      {
         nExtCert = Tkn->getCounter();
         if ( (extCert = new SNames[nExtCert]) != NULL )
         {
            for (i = 0; i < nExtCert; i++)
              extCert[i].Inicia();

            for (i = 0 ; i < nExtCert ; i++)
            {
               char *pch = strchr(Tkn->items[i], '=');

               if ( pch )
               {
                  len = pch-Tkn->items[i];
                  strncpy(extCert[i].id,  Tkn->items[i], len);
                  strcpy(extCert[i].dato, Tkn->items[i]+ len+1 );

                  if( (extCert[i].idNid = OBJ_txt2nid(extCert[i].id)) == NID_undef)
                  {
                     Bitacora->escribePV(BIT_ERROR, "Error en CertificadorAC en cargaExtensiones: Error(%d): inv�lida extension: '%s' ",
                                                     error = NID_undef, extCert[i].id );
                     break;
                  }
                  // Duda: es NID_key_usage or NID_ext_key_usage
                  if ( num_ac == LLAVE_ANTERIOR && 
                       ( ( !strcmp(tipo_cert.c_str(), "ocsp") && extCert[i].idNid == NID_ext_key_usage && strcmp(extCert[i].dato, SN_OCSP_sign) ) ||
                         ( !strcmp(tipo_cert.c_str(), "crl")  && extCert[i].idNid == NID_key_usage     && strcmp(extCert[i].dato, "cRLSign")    )    )
                     )
                  {
                     Bitacora->escribePV(BIT_ERROR, "Error en CertificadorAC en cargaExtensiones: "
                     "Error(%d) certificado con extension(%d) invalida(%s) para este numero_AC(%d)",
                                                     error = EXTINVAL_X_AC, extCert[i].idNid, extCert[i].dato, num_ac );
                     break;
                  }
               }
               else
               {
                  Bitacora->escribePV(BIT_ERROR, "Error en CertificadorAC en cargaExtensiones: Err(%d): en cargaExtens() el token[%d]='%s' no incluye un '='",
                                                  error = FTOKEN_INVAL, i, Tkn->items[i] );
                  break;
               }
            }
         }
         else
            Bitacora->escribePV(BIT_ERROR, "Error en CertificadorAC en cargaExtensiones: Error(%d): memoria insuficiente, new SNames[%d] ",
                                            error = ERR_FALTA_MEMORIA, nExtCert  );
      }
      else
         Bitacora->escribePV(BIT_ERROR, "Error en CertificadorAC en cargaExtensiones: Error(%d) numero invalido de campos(%d) en el buffer: %s",
                                        error = ERR_NUMINV_CAMPOS, Tkn->getCounter(), buf_ext);
      delete Tkn;
      Tkn = NULL;
   }
   else
      Bitacora->escribePV(BIT_ERROR, "Error en CertificadorAC en cargaExtensiones: Error(%d) falta memoria en CSeparaToken", error = ERR_FALTA_MEMORIA);

   return !error;
}


// Verifica la Firma del PKCS7 y lo compara el num_serie del certificado del Firmante 
bool CertificadorAC::verificaFirmaPKCS7( const uchar *b_pkcs7, int l_pkcs7, uchar *buf_pkcs7, int *l_bpkcs7)
{
   tError  err = Error_OK;
   string  s_ruta_fir;

   PtrSgiIO entrada = new_SgiIO();
   if ( entrada )
   {
      err = SgiIO::inicia(entrada, IO_in, (unsigned char*)b_pkcs7, &l_pkcs7 ); 
      if (err == Error_OK)
      {
         PtrSgiSobre sobre = new_SgiSobre();
         if (sobre)
         {
            err = SgiSobre::inicia( sobre, entrada);
            if ( err == Error_OK )
            {
               tTipoPKCS7 tipo;
               string       seriesFirmantes[3], seriesDestinatarios[3];
               PtrSgiCert   certificados[3];
               int          ncertificados = 3, nFirmantes = 3, nDestinatarios = 3, i;

               err = SgiSobre::getDatosSobre( sobre, &tipo, certificados, &ncertificados, seriesFirmantes, &nFirmantes,
                                              seriesDestinatarios, &nDestinatarios );
               if (err == Error_OK)
               {
                  if (tipo != TPKCS7_Firmado)
                     Bitacora->escribePV(BIT_ERROR, "Error en CertificadorAC en verificaFirmaPKCS7: "
                     "Error(%d) el buffer enviado No es un PKCS7 firmado", error = ERR_BUFFERNOFIR );
                  else
                  {
                     for ( i = 0; i < 3 && i < (int)nFirmantes ; i++ )
                     {
                        Bitacora->escribePV(BIT_DEBUG, "CertificadorAC.verificaFirmaPKCS7: serieFirmante[%d]= %s", i, seriesFirmantes[i].c_str() );
                        if ( seriesFirmantes[i] == cert_firma )
                           break;
                     }
                     if ( seriesFirmantes[i] != cert_firma )
                       Bitacora->escribePV(BIT_ERROR, "Error en CertificadorAC en verificaFirmaPKCS7: "
                       "Error(%d) el buffer enviado No esta firmado por el cert. del Firmante(%s)",
                                                       error = ERR_NOFIRMACERT, cert_firma.c_str() );
                     if (!error)
                     {
                        PtrSgiIO salida  = new_SgiIO();
                        if ( salida )
                        {
                           err = SgiIO::inicia(salida, IO_out, buf_pkcs7, l_bpkcs7 );
                           if (err == Error_OK)
                              err = SgiSobre::procesa(sobre, NULL, NULL, salida );
                           delete_SgiIO(salida); // FALTA: a la hora de probar DEPURAR y verificar q est� delete no borre el buffer... 
                        }
                        else
                           err = Error_MemInsuficiente;
                     }
                  }
               }
            }
            delete_SgiSobre(sobre);
         }
         else
            err = Error_MemInsuficiente;
      }
      delete_SgiIO(entrada); // OjO
   }
   else
      err = Error_MemInsuficiente;

   if (err != Error_OK)
   {
      Bitacora->escribePV(BIT_ERROR, "Error en CertificadorAC en verificaFirmaPKCS7: Error(%d) SgiCripto: %s", err, SgiUtils::msgError(err) );
      error = (error)? error : (int)err;
   }
   return !error;
}

// Carga los parametros de la memoria compartida donde se guarda la llave privada
bool CertificadorAC::
cargaParmLlave()
{
   string key_shared, key_id, tip_cert, claves[] = { "KEY", "KEY_ID", "KEY_PW", "CA_PATHX", "TIP_CERT" };
   char   buffer[10];
   
   sprintf( buffer, "%d]", num_ac); // data de la shared memory donde se guarda la llave de la AC 
   key_shared = "[KEY_SHARED_" + string( buffer );
   if ( (error = ptrSrv->Configuracion->getValoresConf(key_shared.c_str(), 5, claves, &mem_nm, &key_id, &pwd_mem, &certAC, &tip_cert ) ) )
      Bitacora->escribePV(BIT_ERROR, "Error en CertificadorAC en cargaParmLlave: Error(%d): "
      "no existen las variables(%s, KEY - TIP_CERT) en el arch. de configuraci�n.",
                                      error, key_shared.c_str() );
   else if ( verificaTipoCert( tip_cert ) ) 
      mem_id = atoi( key_id.c_str() );
   return !error;
}

bool CertificadorAC::cargaLlave()
{
   if (num_ac == LLAVE_ANTERIOR)
      cargaLlaveAnterior();
   else if ( num_ac >= LLAVES_NUEVAS )
      cargaLlaveNueva();
   else
      Bitacora->escribePV(BIT_ERROR, "Error en CertificadorAC en cargaLlave: Error(%d): Numero de AC(%d) invalido", error = NUM_AC_INVALIDA, num_ac );

   #if defined( DBG )
   if (!error)
      guardaArchKey();
   #endif
   return !error; 
}

bool CertificadorAC::
cargaLlaveNueva()
{
   string no_serie;
   SgiLlaveMem *llavemc = new SgiLlaveMem( mem_id );
   if( llavemc )
   {
      try
      {
         llavemc->getLlave( mem_nm.c_str(), pwd_mem.c_str(), &llave, no_serie );
      }
      catch ( ExcepcionLlaveMC ex )
      {
         Bitacora->escribePV(BIT_ERROR, "Error en CertificadorAC en cargaLlaveNueva: Error(%d) al cargar del segmento(%d) la llave:%s, AC(%d) ",
                                         error = ERR_NO_ASIGNA_LLAVE, mem_id, ex.descripcion(), num_ac );
      }
      delete llavemc; // <<-- FALTA: depurar que la llave no se bata
      llavemc = NULL;
   }
   else
      Bitacora->escribePV(BIT_ERROR, "Error en CertificadorAC en cargaLlaveNueva: Error(%d) fatal de memoria(SgiLlaveMem).", error = ERR_FALTA_MEMORIA);
   return !error;
}

// Verifica la llave privada contra la llave publica del cert. de la AC
bool CertificadorAC::verificaLlaves()
{
   RSA        *rsaPbKey_cli = NULL;
   uchar      *contenido = NULL;
   int        lcontenido = MAX_TAMCERT;
   ManArchivo archivo;

   if ( ( rsaPbKey_cli = RSA_new() ) != NULL && ( contenido = new uchar[MAX_TAMCERT] ) != NULL )
   {
      if ( archivo.ProcesaArchivo(certAC.c_str(), contenido ,&lcontenido) )
      {
         SGIX509    sgiX509;

         error = sgiX509.ObtienePbK(contenido, lcontenido, &rsaPbKey_cli, NULL, (char *)certAC.c_str() );
         if ( !error )
         {
            SGIRSA ObjSgiRSA;
            if( !ObjSgiRSA.cmpKeys(rsaPbKey_cli, llave) == 0)
               Bitacora->escribePV(BIT_ERROR, "Error en CertificadorAC en verificaLlaves: "
               "Error(%d): la llave privada no corresponde con la llave publica del certificado, AC(%d).",
                                               error = ERR_PKCS_NOCOINCIDELLAVE, num_ac );
         }
         else
            Bitacora->escribePV(BIT_ERROR, "Error en CertificadorAC en verificaLlaves: "
            "Error(%d): al obtener la llave publica del certificado: %s", error, certAC.c_str() );
      }
      else
         Bitacora->escribePV(BIT_ERROR, "Error en CertificadorAC en verificaLlaves: "
         "Error(%d): al leer el contenido del certificado: %s", error = ERR_CARGA_ARCHIVO, certAC.c_str() );
   }
   else
      Bitacora->escribePV(BIT_ERROR, "Error en CertificadorAC en verificaLlaves: Error(%d): memoria insuficiente.", error = ERR_FALTA_MEMORIA);

   if ( rsaPbKey_cli )
      RSA_free(rsaPbKey_cli);
   if ( contenido )
      delete[] contenido;
   if ( !error )
      Bitacora->escribePV(BIT_INFO, "CertificadorAC.verificaLlaves: OK: la llave privada corresponde con la llave publica del certificado, AC(%d)", num_ac );
   return !error;
}


bool CertificadorAC::cargaLlaveAnterior()
{
   SGIRSA rsaalg;
   SGIPRIVADA *privada = new SGIPRIVADA;
   RegServPKI *srv;
   char *shmseg;
   int shmid, reshm;

   llave = RSA_new();
   if( llave )
   {
      shmid = shmget(mem_id, 0, 0); 
      if( shmid > 0 )
      {
         shmseg = (char*)shmat(shmid, 0, 0);
         srv = new RegServPKI;

         {
            #define MAX_RSA_MODULUS_BITS 2048
            #define MAX_M_S ((MAX_RSA_MODULUS_BITS + 7) / 8)
            #define MAX_RSA_PRIME_BITS ((MAX_RSA_MODULUS_BITS + 1) / 2)
            #define MAX_E_S ((MAX_RSA_PRIME_BITS + 7) / 8)


            unsigned char *r_buffer = (unsigned char *)malloc( 4194304 );
            unsigned char *m_buff = (unsigned char *)malloc(MAX_M_S);
            unsigned char *e_buff = (unsigned char *)malloc(3);
            unsigned char *d_buff = (unsigned char *)malloc(MAX_M_S);
            unsigned char *p1_buff = (unsigned char *)malloc(MAX_E_S);
            unsigned char *p2_buff = (unsigned char *)malloc(MAX_E_S);

            unsigned char *dmp1_buff = (unsigned char *)malloc(MAX_E_S);
            unsigned char *dmq1_buff = (unsigned char *)malloc(MAX_E_S);
            unsigned char *coef_buff = (unsigned char *)malloc(MAX_E_S);

            memcpy(r_buffer, shmseg, sizeof(struct reg_serv_pki_st));

            int n_offset = 0x42;  //+512
            //int e_offset = 0x242; //+512
            int d_offset = 0x442; //+512
            int p_offset = 0x642; //+256
            int q_offset = 0x742; //+256
            int dmp1_offset = 0x842; //+256
            int dmq1_offset = 0x942; //+256
            int coef_offset = 0xA42; //+256

            memcpy( m_buff,  r_buffer + n_offset, MAX_M_S );
            memcpy( d_buff,  r_buffer + d_offset, MAX_M_S );
            memcpy( p1_buff, r_buffer + p_offset, MAX_E_S );
            memcpy( p2_buff, r_buffer + q_offset, MAX_E_S );
            memcpy( dmp1_buff, r_buffer + dmp1_offset, MAX_E_S );
            memcpy( dmq1_buff, r_buffer + dmq1_offset, MAX_E_S );
            memcpy( coef_buff, r_buffer + coef_offset, MAX_E_S );

            unsigned char a = 0x01;
            unsigned char b = 0x00;
            unsigned char c = 0x01;
            memcpy( e_buff,   &a, 1);
            memcpy( e_buff+1, &b, 1);
            memcpy( e_buff+2, &c, 1);
 
            llave->n = BN_bin2bn( m_buff, MAX_M_S, NULL ); 
            llave->e = BN_bin2bn( e_buff, 3, NULL ); 
            llave->d = BN_bin2bn( d_buff, MAX_M_S, NULL ); 
            llave->p = BN_bin2bn( p1_buff, MAX_E_S, NULL );
            llave->q = BN_bin2bn( p2_buff, MAX_E_S, NULL );
            llave->dmp1 = BN_bin2bn( dmp1_buff, MAX_E_S, NULL );
            llave->dmq1 = BN_bin2bn( dmq1_buff, MAX_E_S, NULL );
            llave->iqmp = BN_bin2bn( coef_buff, MAX_E_S, NULL );
         }

//       error = sgiErrorBase( rsaalg.getPrivada( privada, llave ) );
   #if defined( DEBUG )  // DBG: MAML 20101208
         char *mod = BN_bn2hex( llave->n );
         char *pub = BN_bn2hex( llave->e );
         char *pri = BN_bn2hex( llave->d );
         char *p = BN_bn2hex( llave->p );
         char *q = BN_bn2hex( llave->q );
         char *p1 = BN_bn2hex( llave->dmp1 );
         char *q1 = BN_bn2hex( llave->dmq1 );
         char *i = BN_bn2hex( llave->iqmp );

         Bitacora->escribePV(BIT_INFO, "MOD:%s\n", mod); 
         Bitacora->escribePV(BIT_INFO, "PUB:%s\n", pub); 
         Bitacora->escribePV(BIT_INFO, "PRI:%s\n", pri); 
         Bitacora->escribePV(BIT_INFO, "P:  %s\n", p); 
         Bitacora->escribePV(BIT_INFO, "Q:  %s\n", q); 
         Bitacora->escribePV(BIT_INFO, "P1: %s\n", p1); 
         Bitacora->escribePV(BIT_INFO, "Q1: %s\n", q1); 
         Bitacora->escribePV(BIT_INFO, "IQ: %s\n", i); 
    #endif
         //memcpy(srv, shmseg, sizeof(struct reg_serv_pki_st));
         //memcpy(privada, srv->idserv->privKey, sizeof(SGIPRIVADA));

         if( error )
            Bitacora->escribePV(BIT_ERROR, "Error en CertificadorAC en cargaLlaveAnterior: Error(%d): No se pudo obtener la llave anterior.", error );
         else
         {
            reshm = shmdt(shmseg);
            if(reshm < 0)
               Bitacora->escribePV(BIT_ERROR, "Error en CertificadorAC en cargaLlaveAnterior: Error(%d): fallo shmdt() en ::cargaLlaveAnterior().",
                error = reshm ); //OjO: ver q hace la funci�n shmdt()
         }
         delete srv;
      }
      else
         Bitacora->escribePV(BIT_ERROR, "Error en CertificadorAC en cargaLlaveAnterior: "
         "Error(%d): identificador(%d), errno(%d) de memoria compartida invalida en ::cargaLlaveAnterior().",
                                         error = SHMID_INVALIDA, shmid, errno );
   }
   else
      Bitacora->escribePV(BIT_ERROR, "Error en CertificadorAC en cargaLlaveAnterior: Error(%d): memoria insuficiente 'RSA_new()'.", error = ERR_FALTA_MEMORIA); 

   delete privada;
   return !error;
}


//tip_cert=cert1^cert2^cert3^cert4  <<-- se valida que el tipo de certificado a generar sea algunos de los incluidos en tip_cert 
bool CertificadorAC::verificaTipoCert(string &tip_cert )
{
   int           i = -2, len;
   CSeparaToken *Tkn = NULL;
   string        t_cert; 
   char          tip_certificado[1024];

   strcpy(tip_certificado, tip_cert.c_str() );
   Tkn = new CSeparaToken( tip_certificado, '^' );

   if ( Tkn != NULL )
   {
      len = Tkn->getCounter();
      if ( len > 0 )
      {
         for (i = 0; i < len; i++)        
         {
            t_cert = Tkn->items[i];
            if ( t_cert == tipo_cert )
               break;
         }
         if ( t_cert != tipo_cert )
            Bitacora->escribePV(BIT_ERROR, "Error en CertificadorAC en verificaTipoCert: "
            "Error(%d) el tipo de certificado(%s) no se puede generar para esta numero_AC(%d)",
                                            error = CERTINVAL_X_AC, tipo_cert.c_str(), num_ac );
      }
      else
         Bitacora->escribePV(BIT_ERROR, "Error en CertificadorAC en verificaTipoCert: "
         "Error(%d) en el archivo de configuracion la etiqueta 'TIP_CERT' tiene un valor(%d:%s) invalido.",
                                        error = ERR_NUMINV_CAMPOS, len, tip_cert.c_str() );
      delete Tkn;
      Tkn = NULL;
   }
   else
      Bitacora->escribePV(BIT_ERROR, "Error en CertificadorAC en verificaTipoCert: Error(%d) falta memory en CSeparaToken", error = ERR_FALTA_MEMORIA);

   return !error;
}


// ***********************************************************************************************
void CertificadorAC::guardaArchKey()
{
   FILE* fpp = fopen("/tmp/pkSAT.key", "wb");
   if (fpp)
   {
      SGIRSA o_rsa;
      o_rsa.CodPvKey1(llave, (uchar*)"12345678a" , 9, fpp, DES_EDE3_CBC_ALG, DER_FORMAT);
      fclose(fpp);
   }
}


