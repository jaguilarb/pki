static const char* _SRVCERTAC_CPP_VERSION_ ATR_USED = "@(#) CertificadorAC ( L : DSIC10482AC_ : SrvCertAC.cpp : 1.1.0 : 1 : 30/11/10)";

//#VERSION: 1.1.0
// MAML 101130: se valida el numero de AC y el tipo de certificado

#include <iostream>
#include <sstream>

#include <Sgi_SrvUnx.h>
#include <Sgi_SSL.h>
#include <Sgi_ConfigFile.h>
#include <Sgi_Bitacora.h>
#include <Sgi_ProtegePwd.h>
#include <Sgi_OpenSSL.h>
#include <SgiCripto.h>

using namespace std;
using namespace SgiCripto;

#include <SrvCertAC.h>
#include <CertificadorAC.h>

CSrvCertAC* ptrSrv = NULL;
static string  pSSL;
       string  s_ruta_bit;

//*****************************************************************************************
static int passwordSSL(char *buf, int size, int rwflag, void *password)
{
   int SSLsize = pSSL.size();

   rwflag   = rwflag;
   password = password;
   if (size < SSLsize)
   {
      Bitacora->escribePV(BIT_ERROR, "Error al asignar el password de SSL, buffer menor del requerido(%d -> %d)",
                          size, pSSL.size());
      return 0;
   }

   if (!desencripta((uint8*) pSSL.c_str(), pSSL.size(), (uint8*) buf, &size))
   {
      Bitacora->escribe(BIT_ERROR, "Error al desencriptar  el password de SSL");
      return 0;
   }
   return size;
}

static int verificacionSSL(int ok, X509_STORE_CTX *store)
{
   char data[256];

   if (!ok)
   {
      X509 *cert = X509_STORE_CTX_get_current_cert(store);
      int depth  = X509_STORE_CTX_get_error_depth(store);
      int err    = X509_STORE_CTX_get_error(store);

      Bitacora->escribePV(BIT_ERROR, "Error con el certificado %i", depth);
      X509_NAME_oneline(X509_get_issuer_name(cert), data, sizeof(data));
      Bitacora->escribePV(BIT_ERROR, "\tissuer  = %s", data);
      X509_NAME_oneline(X509_get_subject_name(cert), data, sizeof(data));
      Bitacora->escribePV(BIT_ERROR, "\tsubject = %s", data);
      Bitacora->escribePV(BIT_ERROR, "error = %i: %s", err, X509_verify_cert_error_string(err));
   }
   return ok;
}

//*****************************************************************************************
CSrvCertAC::
CSrvCertAC() :
   error(0),
   s_arch_conf(RUTA_CFG)
{
   s_ruta_bit = RUTA_BIT;
   Configuracion =  NULL;
}

CSrvCertAC::
~CSrvCertAC()
{ 
   if (Configuracion)
      delete Configuracion;
   Configuracion = NULL;
}

bool CSrvCertAC::
Inicia()
{
   Configuracion = new CConfigFile( s_arch_conf.c_str() );
   if( !Configuracion )
      Bitacora->escribePV(BIT_ERROR, "Error(%d) fatal de memoria(CConfigFile).", error = ERR_FALTA_MEMORIA);
   else
   {
      error = Configuracion->cargaCfgVars();
      if( error )
         Bitacora->escribePV(BIT_ERROR, "Error(%d), No se pudieron cargar las variables de configuracion", error);
      else 
         cargaParms(); 
   }
   return !error; 
}

#define ESPERA  30

bool CSrvCertAC::
SetParmsSktProc( CSSL_parms &pms )
{
   string ac, acPath, cert, privK, nvlBitac, claves[] = { "CERT_SKT_AC", "RUTA_CERTS", "CERTCLI", "LLAVCLI", "PWDCLI" };

   if ( (error = Configuracion->getValoresConf("[CONEXION]", 5, claves, &ac, &acPath, &cert, &privK, &pSSL ) ) )
      Bitacora->escribePV(BIT_ERROR, "Error(%d): no existen las variables([CONEXION], CERT_SKT_AC - PWDCLI) en el arch. de configuración(%s).",
                                      error, s_arch_conf.c_str());
   else
   {
      if ( (error = Configuracion->getValorVar("[BITACORA]", "NIVELBIT" , &nvlBitac)) )
         Bitacora->escribePV(BIT_ERROR, "Error(%d) al leer el nivel de bitacora)", error);
      else
      {
         Bitacora->escribePV(BIT_DEBUG, "Del archivo de configuración(%s) se obtuvo:ROOT (%s), RUTAAC (%s), AC1(%s), ACK(%s) y NIVELBIT(%s)",
                             s_arch_conf.c_str(), ac.c_str(), acPath.c_str(), cert.c_str(), privK.c_str(), nvlBitac.c_str() );
         BIT_NIVEL iNvlBitac = (BIT_NIVEL) atoi(nvlBitac.c_str());
         if (Bitacora)
            Bitacora->setNivel( iNvlBitac);
         struct timeval espera = {ESPERA, 0};
         pms.SetServicio(ESSL_entero, &espera, ac.c_str(), acPath.c_str(), cert.c_str(), privK.c_str(), verificacionSSL, passwordSSL);
      }
   }
   return !error;
}

bool CSrvCertAC::
Proceso()
{
   bool conectado = true;
   // Generacion del Certificado de Seguridad p/SAT
   CertificadorAC *cert_seg = new CertificadorAC( cert_firma ); // , this // llave, cert_firma, certAC ); 
   if (cert_seg)
   {
      //do
         conectado = cert_seg->atender( SktProc );
      //while( conectado );
      delete cert_seg; // OjO
   }
   else
      Bitacora->escribePV(BIT_ERROR, "Error(%d) fatal de memoria(CertificadorAC).", error = ERR_FALTA_MEMORIA);
   return !error;
}

bool CSrvCertAC::
ProcesaParametros( int argc, char *argv[], char *env[] )
{
   bool ok = true;
   int opcion;
 
   env = env;
   opterr = 0; // para que getopt no mande el mensaje de error al stderr
   while (ok && (opcion = getopt(argc, argv, "c:b:")) != -1) 
   {
      switch (opcion)
      {
         case 'c':   // Archivo de configuración
                     s_arch_conf = optarg;
                     break;
         case 'b':   // Nombre de la bitacora
                     s_ruta_bit = optarg;
                     break;
         default :   // Opción no reconocida
                     ok = false;
      }
   }
   return ok;
}

bool CSrvCertAC::
cargaParms()
{
   string certif_firma, claves[] = { "CERT_FIRMA", "CAD_CERT" };

   if ( (error = Configuracion->getValoresConf("[CERT_AC]", 2, claves, &certif_firma, &cad_cert ) ) )
      Bitacora->escribePV(BIT_ERROR, "Error(%d): no existen las variables([CERT_AC], CERT_FIRMA - CAD_CERT) en el arch. de configuración(%s).",
                                      error, s_arch_conf.c_str());
   else
      obt_dataCert( certif_firma.c_str() );
   return !error;
}


CBitacora*
Crea_CBitacora()
{
   return new CBitacora( s_ruta_bit.c_str() );
}

CSrvUnx* 
Crea_CSrvUnx()
{
   return ptrSrv = new CSrvCertAC();
}

bool libSrvUnx_Ini() 
{
   #if defined( DBG )
      sleep(15);
   #endif
   iniciaLibOpenSSL();
   return true;
}

bool libSrvUnx_Fin() 
{
   terminaLibOpenSSL();
   return true;
}


//*****************************************************************************************
bool CSrvCertAC::obt_dataCert(const char *certif, bool verifCadCert )
{
   PtrSgiIO      io = NULL;
   tError        err = Error_OK;
   PtrSgiCert    cert = NULL;
   PtrSgiCadCert cadcert = NULL;
   ostringstream ss;
   int           lbuffer = 512; 
   char          buffer[lbuffer];

   try
   {
      io = new_SgiIO();
      err = SgiIO::inicia(io, IO_in, (char *)certif );
      cert = new_SgiCertificado();
      if (!err && cert)
         err = SgiCertificado::inicia(cert, TC_ASCII, io);
      if ( err )
      {
         ss << "Error(" << err << ") al iniciar el certificado: " << certif;
         throw ss.str();
      }
      if ( !verifCadCert )
      {
         err = SgiCertificado::getNumSerie(cert, &cert_firma);
         if (err != Error_OK)
         {
            ss << "Error(" << err << ") al extraer el número de serie del certificado: " << certif;
            throw ss.str();
         }
      }
      else // Obtiene una extension certif de AC y Verifica la cadena de Certificación
      {
         err = SgiCertificado::getExtension(cert, Ext_basic_constraints, buffer, &lbuffer);
         if (err)
         {
            ss << "Error(" << err << ") al extraer la extension 'basic_constraints' del certificado: " << certif;
            throw ss.str();
         }
         else if ( strcmp(buffer,"0") != 0 ) // Validar Atributo CA (==true) [ basic constraint ]: 
         {
            cadcert = new_SgiCadCert();
            if (cadcert)
            {  
               err = SgiCadCert::inicia( cadcert,  cad_cert.c_str() ); 
               if ( !err )
               { 
                  err = SgiCadCert::verifica(cadcert, CTipoFEA, cert);
                  if ( err )
                  {
                     ss << "Error(" << err << ") cadena de certificación erronea del certificado: " << certif;
                     throw ss.str();
                  }   
               } 
               else
               { 
                  ss << "Error(" << err << ") en inicia() de la cadena de certificación: " << cad_cert.c_str();
                  throw ss.str();
               }
            }
            else
            {
               err = Error_MemInsuficiente;
               ss << "Error(" << err << "): memoria insuficiente en new_SgiCadCert().";
               throw ss.str();
            }  
         }
         else
         {
            error = ERR_CERT_NO_AC;
            ss << "Error(" << err << "): La llave Privada y certificado no son de AC.";
            throw ss.str();
         }
      }
   }
   catch (string msg)
   {
      if (err != Error_OK)
      {
         Bitacora->escribePV(BIT_ERROR, "Error(%d) SgiCripto: %s", err, SgiUtils::msgError(err) );
         error = (int)err;
      }
      Bitacora->escribePV(BIT_ERROR, "Error(%d) en obt_dataCert(): %s", error, msg.c_str() );
   }

   if (cadcert)
      delete_SgiCadCert(cadcert);
   if (cert)
      delete_SgiCertificado(cert);
   if (io)
      delete_SgiIO(io);

   return !error;
}


