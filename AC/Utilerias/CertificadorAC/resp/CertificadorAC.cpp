static const char* _CERTIFICADORAC_CPP_VERSION_ ATR_USED = "@(#) CertificadorAC ( L : DSIC10423AC_ : CertificadorAC.cpp : 1.0.0 : 0 : 20/10/10)";

//#VERSION: 1.0.0
#include <Sgi_SrvUnx.h>
#include <Sgi_ConfigFile.h>
#include <Sgi_OpenSSL.h>
#include <Sgi_ConfOpen.h>
#include <Sgi_B64.h>
#include <SgiCripto.h>
#include <Sgi_PKI.h>

using namespace std;
using namespace SgiCripto;

#include <CertificadorAC.h>

CertificadorAC::
CertificadorAC( RSA *key, string &s_cert_firma, string &m_certAC ) :
   llave( key ),
   cert_firma(s_cert_firma),
   certAC(m_certAC), 
   error(0),
   extCert(NULL)
{
   vigencia = VIGENCIA;
   memset(num_serie, 0, sizeof(num_serie));
}

CertificadorAC::
~CertificadorAC()
{
   if( sock )
      sock = NULL;
   if( llave )
      llave = NULL;

   if ( extCert )
     delete []extCert;
   extCert = NULL;
}

#include <netinet/in.h>

bool CertificadorAC::
genera(const uint16 *header)
{  
   int   l_cert = 1024*10;
   uchar b_cert[l_cert];
   int   l_pkcs7; 

   l_pkcs7 = (int)ntohs(*header);
   Bitacora->escribePV(BIT_DEBUG, "CertificadorAC.genera: Se espera leer un PKCS7 con tama�o %u bytes.", l_pkcs7);
   uchar *b_pkcs7 = new uchar[l_pkcs7+1];
   if (b_pkcs7)
   {
      memset(b_pkcs7, 0, l_pkcs7+1);
      if( recibeCadena( l_pkcs7, &b_pkcs7 ) )
      {
         int l_bpkcs7 = l_pkcs7;
         uchar *buf_pkcs7 = new uchar[l_bpkcs7+1]; 
         if (buf_pkcs7)
         {   
            memset(buf_pkcs7, 0, l_bpkcs7+1);
            if( verificaFirmaPKCS7( b_pkcs7, (int)l_pkcs7, (unsigned char*)buf_pkcs7, &l_bpkcs7) && 
                cargaDatosCert( (unsigned char*)buf_pkcs7, &l_bpkcs7) )
            {
               // Crea el certificado solicitado y lo envia de regreso a la AR en formato DER.
               SGIX509 x509;

               memset(b_cert, 0, l_cert);  

               //Bitacora->escribePV(BIT_DEBUG, "Err(%d) genera(), l_bpkcs7=%d, certAC=%s, num_serie=%s, vigencia=%d, nExtCert=%d, l_cert=%d",
               //                                error, l_bpkcs7, certAC.c_str(), num_serie, vigencia, nExtCert, l_cert );
               error = x509.GenCERT(buf_pkcs7, l_bpkcs7, (char *)certAC.c_str(), (unsigned char*)num_serie, vigencia, llave, extCert, nExtCert,
                                    NULL, ID_DER, (uchar*)(b_cert+sizeof(uint32)) , &l_cert );

               //Bitacora->escribePV(BIT_DEBUG, "Err(%d) genera(), l_bpkcs7=%d, certAC=%s, num_serie=%s, vigencia=%d, nExtCert=%d, l_cert=%d",
               //                                error, l_bpkcs7, certAC.c_str(), num_serie, vigencia, nExtCert, l_cert );
               if( !error )
               {
                  uint16  tam = (uint16) htons(l_cert);
                  memcpy(b_cert, &tam, sizeof(uint16));

                  error = sock->Envia( (uint8 *)b_cert, l_cert+sizeof(uint16) );
                  if (error) 
                  {
                     Bitacora->escribePV( BIT_ERROR, "Error en CertificadorAC en genera: Error(%d), al hacer el Envio del certificado al cliente.", error ); 
                     error = SOCK_ENVIA;
                  }
                  else
                     Bitacora->escribePV( BIT_INFO, "CertificadorAC.genera: Certificado(%d) generado y enviado al cliente.", l_cert );
               }
               else  
                  Bitacora->escribePV( BIT_ERROR, "Error en CertificadorAC en genera: Error(%d), en la generaci�n del certificado ", error );
            }
            memset(buf_pkcs7, 0, l_bpkcs7+1);
            delete buf_pkcs7;
            buf_pkcs7 = NULL;
         }
         else
            Bitacora->escribePV(BIT_ERROR, "Error en CertificadorAC en genera: Error(%d) fatal de memoria(uchar) buf_pkcs7.", error = ERR_FALTA_MEMORIA);
      }
      memset(b_pkcs7, 0, l_pkcs7+1);
      delete b_pkcs7; 
      b_pkcs7 = NULL;
   }
   else
      Bitacora->escribePV(BIT_ERROR, "Error en CertificadorAC en genera: Error(%d) fatal de memoria(uchar).", error = ERR_FALTA_MEMORIA);

   if ( error && error != SOCK_ENVIA) // si hay error => regresa -1
   {
      uint16 tam = -1;
      memcpy(b_cert, &tam, sizeof(uint16));
      error = sock->Envia( (uint8 *)b_cert, sizeof(uint16) );
      if (error)
         Bitacora->escribePV( BIT_ERROR, "Error en CertificadorAC en genera: Error(%d), al hacer el Envio de Error al cliente.", error );
   }
   return !error;
}

bool CertificadorAC::
recibeCadena(const int longitud, uchar **buff)
{
   int len = (int)longitud;
   int lec = 0;
   do
   {
      if( !(error = sock->Recibe( (uint8 *)*buff + lec, len )) )
      {
         lec += len;
         len = longitud - lec;
      }
      else
      {
         Bitacora->escribePV(BIT_ERROR, "Error en CertificadorAC en recibeCadena: Ocurrio un error(%d) durante la lectura del buffer del PKCS7 recibido.", error);
         break;
      }
   } while( lec < longitud );
   return !error;
}


bool CertificadorAC::
atender( CSSL_Skt *skt )
{
   sock = skt;
   if( sock )
   {
      int l_buff = sizeof(uint16);  
      uint16 buff[2];
      memset(buff, 0, sizeof(buff) );
      error = sock->Recibe( (uint8 *)buff, l_buff );
      if( !error )
         genera(buff);
   }
   return !error;
}

// estructura enviada sea un PKCS7: numero de Serie|vigencia|iden=valor^iden=valor^iden=valor^|Requerimiento en PEM
bool CertificadorAC::cargaDatosCert( uchar *buf_pkcs7, int *l_bpkcs7)
{
   // cargamos en una estructura de Token, los 3 valores para generar el Cert
   CSeparaToken *Tkn = NULL;

   Tkn = new CSeparaToken( (char *)buf_pkcs7, '|' );

   if ( Tkn != NULL )
   {
      if ( Tkn->getCounter() == 4 )
      {
         strcpy(num_serie, Tkn->items[0] );
         vigencia = atoi( Tkn->items[1] );
         cargaExtensiones( Tkn->items[2] );
         error = SgiUtils::B64(false, (const unsigned char*)Tkn->items[3], strlen(Tkn->items[3]), buf_pkcs7, l_bpkcs7 );
         if (error)
            Bitacora->escribePV(BIT_ERROR, "Error en CertificadorAC en cargaDatosCert: Error(%d), al decodificar el requerimiento:%s", 
            error, SgiUtils::msgError( (tError)error)); 
      }
      else
         Bitacora->escribePV(BIT_ERROR, "Error en CertificadorAC en cargaDatosCert: Error(%d) numero invalido de campos(%d) en el buffer: %s",
                                        error = ERR_NUMINV_CAMPOS, Tkn->getCounter(), buf_pkcs7);
      //Bitacora->escribePV(BIT_DEBUG, "Err(%d) en cargaDatosCert(), Tkn->items[3]=%s", error, Tkn->items[3]  );
      delete Tkn;
      Tkn = NULL;
   }
   else
      Bitacora->escribePV(BIT_ERROR, "Error en CertificadorAC en cargaDatosCert: Error(%d) falta de memoria en CSeparaToken", error = ERR_FALTA_MEMORIA);

   return !error;
}

//iden=valor^iden=valor^iden=valor^
bool CertificadorAC::cargaExtensiones(char *buf_ext )
{
   int           i = -2, len;
   CSeparaToken *Tkn = NULL;

   Tkn = new CSeparaToken(buf_ext, '^' );

   if ( Tkn != NULL )
   {
      if ( Tkn->getCounter() > 0 )
      {
         nExtCert = Tkn->getCounter();
         if ( (extCert = new SNames[nExtCert]) != NULL )
         {
            for (i = 0; i < nExtCert; i++)
              extCert[i].Inicia();

            for (i = 0 ; i < nExtCert ; i++)
            {
               char *pch = strchr(Tkn->items[i], '=');

               if ( pch )
               {
                  len = pch-Tkn->items[i];
                  strncpy(extCert[i].id,  Tkn->items[i], len);
                  strcpy(extCert[i].dato, Tkn->items[i]+ len+1 );

                  if( (extCert[i].idNid = OBJ_txt2nid(extCert[i].id)) == NID_undef)
                  {
                     Bitacora->escribePV(BIT_ERROR, "Error en CertificadorAC en cargaExtensiones: Error(%d): inv�lida extension: '%s' ",
                                                     error = NID_undef, extCert[i].id );
                     break;
                  }
               }
               else
               {
                  Bitacora->escribePV(BIT_ERROR, "Error en CertificadorAC en cargaExtensiones: Err(%d): en cargaExtens() el token[%d]='%s' no incluye un '='",
                                                  error = FTOKEN_INVAL, i, Tkn->items[i] );
                  break;
               }
            }
         }
         else
            Bitacora->escribePV(BIT_ERROR, "Error en CertificadorAC en cargaExtensiones: Error(%d): memoria insuficiente, new SNames[%d] ",
                                            error = ERR_FALTA_MEMORIA, nExtCert  );
      }
      else
         Bitacora->escribePV(BIT_ERROR, "Error en CertificadorAC en cargaExtensiones: Error(%d) numero invalido de campos(%d) en el buffer: %s",
                                        error = ERR_NUMINV_CAMPOS, Tkn->getCounter(), buf_ext);
      delete Tkn;
      Tkn = NULL;
   }
   else
      Bitacora->escribePV(BIT_ERROR, "Error en CertificadorAC en cargaExtensiones: Error(%d) falta memoria en CSeparaToken", error = ERR_FALTA_MEMORIA);

   return !error;
}


// Verifica la Firma del PKCS7 y lo compara el num_serie del certificado del Firmante 
bool CertificadorAC::verificaFirmaPKCS7( const uchar *b_pkcs7, int l_pkcs7, uchar *buf_pkcs7, int *l_bpkcs7)
{
   tError  err = Error_OK;
   string  s_ruta_fir;

   PtrSgiIO entrada = new_SgiIO();
   if ( entrada )
   {
      err = SgiIO::inicia(entrada, IO_in, (unsigned char*)b_pkcs7, &l_pkcs7 ); 
      if (err == Error_OK)
      {
         PtrSgiSobre sobre = new_SgiSobre();
         if (sobre)
         {
            err = SgiSobre::inicia( sobre, entrada);
            if ( err == Error_OK )
            {
               tTipoPKCS7 tipo;
               string       seriesFirmantes[3], seriesDestinatarios[3];
               PtrSgiCert   certificados[3];
               int          ncertificados = 3, nFirmantes = 3, nDestinatarios = 3, i;

               err = SgiSobre::getDatosSobre( sobre, &tipo, certificados, &ncertificados, seriesFirmantes, &nFirmantes,
                                              seriesDestinatarios, &nDestinatarios );
               if (err == Error_OK)
               {
                  if (tipo != TPKCS7_Firmado)
                     Bitacora->escribePV(BIT_ERROR, "Error en CertificadorAC en verificaFirmaPKCS7: "
                     "Error(%d) el buffer enviado No es un PKCS7 firmado", error = ERR_BUFFERNOFIR );
                  else
                  {
                     for ( i = 0; i < 3 && i < (int)nFirmantes ; i++ )
                     {
                        Bitacora->escribePV(BIT_DEBUG, "CertificadorAC.verificaFirmaPKCS7: serieFirmante[%d]= %s", i, seriesFirmantes[i].c_str() );
                        if ( seriesFirmantes[i] == cert_firma )
                           break;
                     }
                     if ( seriesFirmantes[i] != cert_firma )
                       Bitacora->escribePV(BIT_ERROR, "Error en CertificadorAC en verificaFirmaPKCS7: "
                       "Error(%d) el buffer enviado No esta firmado por el cert. del Firmante(%s)",
                                                       error = ERR_NOFIRMACERT, cert_firma.c_str() );
                     if (!error)
                     {
                        PtrSgiIO salida  = new_SgiIO();
                        if ( salida )
                        {
                           err = SgiIO::inicia(salida, IO_out, buf_pkcs7, l_bpkcs7 );
                           if (err == Error_OK)
                              err = SgiSobre::procesa(sobre, NULL, NULL, salida );
                           //delete_SgiIO(salida); // FALTA: a la hora de probar DEPURAR y verificar q est� delete no borre el buffer... 
                        }
                        else
                           err = Error_MemInsuficiente;
                     }
                  }
               }
            }
            delete_SgiSobre(sobre);
         }
         else
            err = Error_MemInsuficiente;
      }
      //delete_SgiIO(entrada);
   }
   else
      err = Error_MemInsuficiente;

   if (err != Error_OK)
   {
      Bitacora->escribePV(BIT_ERROR, "Error en CertificadorAC en verificaFirmaPKCS7: Error(%d) SgiCripto: %s", err, SgiUtils::msgError(err) );
      error = (error)? error : (int)err;
   }
   return !error;
}


