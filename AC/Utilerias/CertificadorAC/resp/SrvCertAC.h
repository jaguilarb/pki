#ifndef _SRVCERTAC_H_
   #define _SRVCERTAC_H_
static const char* _SRVCERTAC_H_VERSION_ ATR_USED = "@(#) CertificadorAC ( L : DSIC10423AC_ : SrvCertAC.h : 1.0.0 : 0 : 20/10/10)";
//#VERSION: 1.0.0

#define PATH_CONF            "/usr/local/SAT/PKI/etc/"
#define PATH_DBIT            "/var/local/SAT/PKI/"

#ifdef DBG
   #define RUTA_CFG            PATH_CONF "CertificadorAC_dbg.cfg"
   #define RUTA_BIT            PATH_DBIT "CertificadorAC_dbg.log"
#else
   #define RUTA_CFG            PATH_CONF "CertificadorAC.cfg"
   #define RUTA_BIT            PATH_DBIT "CertificadorAC.log"
#endif

#define ERR_CERT_NO_AC  15

class CSrvCertAC : public CSrvUnx 
{
   protected :
      CConfigFile *Configuracion;
      RSA         *llave;
      int         error; 
      string      s_arch_conf;
      string      certAC; 
      string      cert_firma; 
      string      cad_cert;
     
      string      pwd_mem;
      string      mem_nm; 
      int         mem_id;

  
   private :
      bool Inicia();
      bool SetParmsSktProc(CSSL_parms &);
      bool Proceso();
      bool cargaParmLlave();
      bool cargaLlave();
      bool obt_dataCert(const char *certif, bool verifCadCert = false );
      bool verificaLlaves();

   public :
      CSrvCertAC();
      ~CSrvCertAC();
      bool ProcesaParametros(int argc, char* argv[], char* env[]); 
}; 

static int passwordSSL(char *buf, int size, int banderas, void *passwd);
static int verificacionSSL(int ok, X509_STORE_CTX *store);
string     s_ruta_bit;

#endif  //_SRVCERTAC_H_

