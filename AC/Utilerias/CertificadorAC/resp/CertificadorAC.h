#ifndef _CERTIFICADORAC_H_ 
   #define _CERTIFICADORAC_H_ 
static const char* _CERTIFICADORAC_H_VERSION_ ATR_USED = "@(#) CertificadorAC ( L : DSIC10423AC_ : CertificadorAC.h : 1.0.0 : 0 : 20/10/10)";

//#VERSION: 1.0.0

#define VIGENCIA  126230440

#define FALLO_GET         30
#define TIPCERT_INVAL     40
#define ERR_BUFFERNOFIR   50
#define ERR_NOFIRMACERT   60
#define ERR_NUMINV_CAMPOS 70
#define FTOKEN_INVAL      80
#define SOCK_ENVIA        90

class CertificadorAC
{
   private :
      RSA         *llave;
      string      &cert_firma;
      string      &certAC;
      int          error; 
      int          nExtCert;
      SNames      *extCert;
      CSSL_Skt    *sock;
      int          vigencia;
      char         num_serie[50]; 

      bool genera(const uint16 *header);
      bool recibeCadena(const int longitud, uchar **buff);
      bool cargaDatosCert( uchar *buf_pkcs7, int *l_bpkcs7);
      bool cargaExtensiones(char *);
      bool verificaFirmaPKCS7( const uchar *b_pkcs7, int l_pkcs7, uchar *b_req, int *l_breq);

   public :
      CertificadorAC(RSA *key, string &s_cert_firma, string &m_certAC );
      ~CertificadorAC();
      bool atender(CSSL_Skt *skt);
};

#endif // _CERTIFICADORAC_H_

