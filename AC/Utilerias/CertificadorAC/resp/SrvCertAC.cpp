static const char* _SRVCERTAC_CPP_VERSION_ ATR_USED = "@(#) CertificadorAC ( L : DSIC10423AC_ : SrvCertAC.cpp : 1.0.0 : 0 : 20/10/10)";

//#VERSION: 1.1.0
#define _SHK_CPP_

#include <iostream>
#include <sstream>

#include <Sgi_SrvUnx.h>
#include <Sgi_SSL.h>
#include <Sgi_ConfigFile.h>
#include <Sgi_Bitacora.h>
#include <Sgi_ProtegePwd.h>
#include <Sgi_LlaveMC.h>
#include <ExcepcionLlaveMC.h>
#include <SgiCripto.h>

using namespace std;
using namespace SgiCripto;

#include <SrvCertAC.h>
#include <CertificadorAC.h>

CSrvCertAC* ptrSrv = NULL;
static string pSSL;

CSrvCertAC::
CSrvCertAC() :
   Configuracion( NULL ),
   llave( NULL ),
   error(0),
   s_arch_conf(RUTA_CFG)
{
   s_ruta_bit = RUTA_BIT;
}

CSrvCertAC::
~CSrvCertAC()
{ 
   if (Configuracion)
      delete Configuracion;
   Configuracion = NULL;
}

bool CSrvCertAC::
Inicia()
{
   Configuracion = new CConfigFile( s_arch_conf.c_str() );
   if( !Configuracion )
      Bitacora->escribePV(BIT_ERROR, "Error en CSrvCertAC en Inicia: Error(%d) fatal de memoria(CConfigFile).", error = ERR_FALTA_MEMORIA);
   else
   {
      error = Configuracion->cargaCfgVars();
      if( error )
         Bitacora->escribePV(BIT_ERROR, "Error en CSrvCertAC en Inicia: Error(%d), No se pudieron cargar las variables de configuracion", error);
      else if( cargaParmLlave() && cargaLlave() && verificaLlaves() )
         obt_dataCert(certAC.c_str(), true ); 
   }
   return !error; 
}

#define ESPERA  30

bool CSrvCertAC::
SetParmsSktProc( CSSL_parms &pms )
{
   string ac, acPath, cert, privK, nvlBitac, claves[] = { "CERT_SKT_AC", "RUTA_CERTS", "CERTCLI", "LLAVCLI", "PWDCLI" };

   if ( (error = Configuracion->getValoresConf("[CONEXION]", 5, claves, &ac, &acPath, &cert, &privK, &pSSL ) ) )
      Bitacora->escribePV(BIT_ERROR, "Error en CSrvCertAC en SetParmsSktProc: Error(%d): "
      "no existen las variables([CONEXION], CERT_SKT_AC - PWDCLI) en el arch. de configuración(%s).",
                                      error, s_arch_conf.c_str());
   else
   {
      if ( (error = Configuracion->getValorVar("[BITACORA]", "NIVELBIT" , &nvlBitac)) )
         Bitacora->escribePV(BIT_ERROR, "Error en CSrvCertAC en SetParmsSktProc: Error(%d) al leer el nivel de bitacora)", error);
      else
      {
         Bitacora->escribePV(BIT_DEBUG, "CSrvCertAC.SetParmsSktProc: Del archivo de configuración(%s) se obtuvo:"
         "ROOT (%s), RUTAAC (%s), AC1(%s), ACK(%s) y NIVELBIT(%s)",
                             s_arch_conf.c_str(), ac.c_str(), acPath.c_str(), cert.c_str(), privK.c_str(), nvlBitac.c_str() );
         BIT_NIVEL iNvlBitac = (BIT_NIVEL) atoi(nvlBitac.c_str());
         if (Bitacora)
            Bitacora->setNivel( iNvlBitac);
         struct timeval espera = {ESPERA, 0};
         pms.SetServicio(ESSL_entero, &espera, ac.c_str(), acPath.c_str(), cert.c_str(), privK.c_str(), verificacionSSL, passwordSSL);
      }
   }
   return !error;
}

bool CSrvCertAC::
Proceso()
{
   bool conectado = true;
   // Generacion del Certificado de Seguridad p/SAT
   CertificadorAC *cert_seg = new CertificadorAC( llave, cert_firma, certAC ); 
   if (cert_seg)
   {
      //do
         conectado = cert_seg->atender( SktProc );
      //while( conectado );
      delete cert_seg; // OjO
   }
   else
      Bitacora->escribePV(BIT_ERROR, "Error en CSrvCertAC en Proceso: Error(%d) fatal de memoria(CertificadorAC).", error = ERR_FALTA_MEMORIA);
   return !error;
}

bool CSrvCertAC::
ProcesaParametros( int argc, char *argv[], char *env[] )
{
   bool ok = true;
   int opcion;
 
   env = env;
   opterr = 0; // para que getopt no mande el mensaje de error al stderr
   while (ok && (opcion = getopt(argc, argv, "c:b:")) != -1) 
   {
      switch (opcion)
      {
         case 'c':   // Archivo de configuración
                     s_arch_conf = optarg;
                     break;
         case 'b':   // Nombre de la bitacora
                     s_ruta_bit = optarg;
                     break;
         default :   // Opción no reconocida
                     ok = false;
      }
   }
   return ok;
}

bool CSrvCertAC::
cargaParmLlave()
{
   string key_id, cert_firma, claves[] = { "KEY", "KEY_ID", "KEY_PW", "CA_PATHX", "CERT_FIRMA", "CAD_CERT" };

   if ( (error = Configuracion->getValoresConf("[KEY_SHARED]", 6, claves, &mem_nm, &key_id, &pwd_mem, &certAC, &cert_firma, &cad_cert ) ) )
      Bitacora->escribePV(BIT_ERROR, "Error en CSrvCertAC en cargaParmLlave: Error(%d): "
      "no existen las variables([KEY_SHARED], KEY - CAD_CERT) en el arch. de configuración(%s).",
                                      error, s_arch_conf.c_str());
   else if ( obt_dataCert( cert_firma.c_str() ) )
        mem_id = atoi( key_id.c_str() );
   return !error;
}


bool CSrvCertAC::
cargaLlave()
{
   string no_serie;
   SgiLlaveMem *llavemc = new SgiLlaveMem( mem_id );
   if( llavemc )
   {
      try
      {
         llavemc->getLlave( mem_nm.c_str(), pwd_mem.c_str(), &llave, no_serie ); 
      }
      catch ( ExcepcionLlaveMC ex )
      {
         Bitacora->escribePV(BIT_ERROR, "Error en CSrvCertAC en cargaLlave: Error(%d) al cargar del segmento(%d) la llave:%s ",
                                         error = ERR_NO_ASIGNA_LLAVE, mem_id, ex.descripcion()); 
      }
      delete llavemc; // <<-- FALTA: depurar que la llave no se bata
      llavemc = NULL;  
   }
   else
      Bitacora->escribePV(BIT_ERROR, "Error en CSrvCertAC en cargaLlave: Error(%d) fatal de memoria(SgiLlaveMem).", error = ERR_FALTA_MEMORIA);
   return !error; 
}

CBitacora*
Crea_CBitacora()
{
   return new CBitacora( s_ruta_bit.c_str() );
}

CSrvUnx* 
Crea_CSrvUnx()
{
   return ptrSrv = new CSrvCertAC();
}

bool libSrvUnx_Ini() 
{
   #if defined( DBG )
      sleep(15);
   #endif
   iniciaLibOpenSSL();
   return true;
}

bool libSrvUnx_Fin() 
{
   terminaLibOpenSSL();
   return true;
}

static int passwordSSL(char *buf, int size, int rwflag, void *password)
{  
   int SSLsize = pSSL.size();

   rwflag   = rwflag;
   password = password;
   if (size < SSLsize)
   {
      Bitacora->escribePV(BIT_ERROR, "Error en CSrvCertAC en passwordSSL: Error al asignar el password de SSL, buffer menor del requerido(%d -> %d)",
                          size, pSSL.size());
      return 0;
   }

   if (!desencripta((uint8*) pSSL.c_str(), pSSL.size(), (uint8*) buf, &size))
   {
      Bitacora->escribe(BIT_ERROR, "Error en CSrvCertAC en passwordSSL: Error al desencriptar  el password de SSL");
      return 0;
   }
   return size;
}

static int verificacionSSL(int ok, X509_STORE_CTX *store)
{
   char data[256];
   
   if (!ok)
   {
      X509 *cert = X509_STORE_CTX_get_current_cert(store);
      int depth  = X509_STORE_CTX_get_error_depth(store);
      int err    = X509_STORE_CTX_get_error(store);

      Bitacora->escribePV(BIT_ERROR, "Error en CSrvCertAC en verificacionSSL: Error con el certificado %i", depth);
      X509_NAME_oneline(X509_get_issuer_name(cert), data, sizeof(data));
      Bitacora->escribePV(BIT_ERROR, "Error en CSrvCertAC en verificacionSSL: \tissuer  = %s", data);
      X509_NAME_oneline(X509_get_subject_name(cert), data, sizeof(data));
      Bitacora->escribePV(BIT_ERROR, "Error en CSrvCertAC en verificacionSSL: \tsubject = %s", data);
      Bitacora->escribePV(BIT_ERROR, "Error en CSrvCertAC en verificacionSSL: error = %i: %s", err, X509_verify_cert_error_string(err));
   }
   return ok;
}

//*****************************************************************************************
bool CSrvCertAC::obt_dataCert(const char *certif, bool verifCadCert )
{
   PtrSgiIO      io = NULL;
   tError        err = Error_OK;
   PtrSgiCert    cert = NULL;
   PtrSgiCadCert cadcert = NULL;
   ostringstream ss;
   int           lbuffer = 512;
   char          buffer[lbuffer];

   try
   {
      io = new_SgiIO();
      err = SgiIO::inicia(io, IO_in, (char *)certif );
      cert = new_SgiCertificado();
      if (!err && cert)
         err = SgiCertificado::inicia(cert, TC_ASCII, io);
      if ( err )
      {
         ss << "Error(" << err << ") al iniciar el certificado: " << certif;
         throw ss.str();
      }
      if ( !verifCadCert )
      {
         err = SgiCertificado::getNumSerie(cert, &cert_firma);
         if (err != Error_OK)
         {
            ss << "Error(" << err << ") al extraer el número de serie del certificado: " << certif;
            throw ss.str();
         }
      }
      else // Obtiene una extension certif de AC y Verifica la cadena de Certificación
      {
         err = SgiCertificado::getExtension(cert, Ext_basic_constraints, buffer, &lbuffer);
         if (err)
         {
            ss << "Error(" << err << ") al extraer la extension 'basic_constraints' del certificado: " << certif;
            throw ss.str();
         }
         else if ( strcmp(buffer,"0") != 0 ) // Validar Atributo CA (==true) [ basic constraint ]: 
         {
            cadcert = new_SgiCadCert();
            if (cadcert)
            {  
               err = SgiCadCert::inicia( cadcert, cad_cert.c_str() );
               if ( !err )
               { 
                  err = SgiCadCert::verifica(cadcert, CTipoFEA, cert);
                  if ( err )
                  {
                     ss << "Error(" << err << ") cadena de certificación erronea del certificado: " << certif;
                     throw ss.str();
                  }   
               } 
               else
               { 
                  ss << "Error(" << err << ") en inicia() de la cadena de certificación: " << cad_cert.c_str();
                  throw ss.str();
               }
            }
            else
            {
               err = Error_MemInsuficiente;
               ss << "Error(" << err << "): memoria insuficiente en new_SgiCadCert().";
               throw ss.str();
            }  
         }
         else
         {
            error = ERR_CERT_NO_AC;
            ss << "Error(" << err << "): La llave Privada y certificado no son de AC.";
            throw ss.str();
         }
      }
   }
   catch (string msg)
   {
      if (err != Error_OK)
      {
         Bitacora->escribePV(BIT_ERROR, "Error en CSrvCertAC en obt_dataCert: Error(%d) SgiCripto: %s", err, SgiUtils::msgError(err) );
         error = (int)err;
      }
      Bitacora->escribePV(BIT_ERROR, "Error en CSrvCertAC en obt_dataCert: Error(%d) en obt_dataCert(): %s", error, msg.c_str() );
   }

   if (cadcert)
      delete_SgiCadCert(cadcert);
   if (cert)
      delete_SgiCertificado(cert);
   //if (io)
   //   delete_SgiIO(io);

   return !error;
}

// Verifica la llave privada contra la llave publica del cert. de la AC
bool CSrvCertAC::verificaLlaves()
{
   RSA        *rsaPbKey_cli = NULL;
   uchar      *contenido = NULL;
   int        lcontenido = MAX_TAMCERT;
   ManArchivo archivo;

   if ( ( rsaPbKey_cli = RSA_new() ) != NULL && ( contenido = new uchar[MAX_TAMCERT] ) != NULL )
   {
      if ( archivo.ProcesaArchivo(certAC.c_str(), contenido ,&lcontenido) )
      {
         SGIX509    sgiX509;

         error = sgiX509.ObtienePbK(contenido, lcontenido, &rsaPbKey_cli, NULL, (char *)certAC.c_str() );
         if ( !error )
         {
            SGIRSA ObjSgiRSA;
            if( !ObjSgiRSA.cmpKeys(rsaPbKey_cli, llave) == 0)
               Bitacora->escribePV(BIT_ERROR, "Error en CSrvCertAC en verificaLlaves: Error(%d): "
               "la llave privada no corresponde con la llave publica del certificado.",
                                               error = ERR_PKCS_NOCOINCIDELLAVE);
         }
         else
            Bitacora->escribePV(BIT_ERROR, "Error en CSrvCertAC en verificaLlaves: Error(%d): al obtener la llave publica del certificado: %s", 
            error, certAC.c_str() );
      }
      else
         Bitacora->escribePV(BIT_ERROR, "Error en CSrvCertAC en verificaLlaves: Error(%d): al leer el contenido del certificado: %s",
          error = ERR_CARGA_ARCHIVO, certAC.c_str() );
   }
   else
      Bitacora->escribePV(BIT_ERROR, "Error en CSrvCertAC en verificaLlaves: Error(%d): memoria insuficiente.", error = ERR_FALTA_MEMORIA);

   if ( rsaPbKey_cli )
      RSA_free(rsaPbKey_cli);
   if ( contenido )
      delete[] contenido;
   if ( !error )
      Bitacora->escribe(BIT_INFO, "CSrvCertAC.verificaLlaves: OK: la llave privada corresponde con la llave publica del certificado");
   return !error;
}

