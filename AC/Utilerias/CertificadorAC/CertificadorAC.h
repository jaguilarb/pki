#ifndef _CERTIFICADORAC_H_ 
   #define _CERTIFICADORAC_H_ 
static const char* _CERTIFICADORAC_H_VERSION_ ATR_USED = "@(#) CertificadorAC ( L : DSIC10482AC_ : CertificadorAC.h : 1.1.0 : 1 : 30/11/10)";

//#VERSION: 1.1.0

#define VIGENCIA  126230440

#define LLAVE_ANTERIOR    0
#define LLAVES_NUEVAS     1


// Errores
#define FALLO_GET         30
#define TIPCERT_INVAL     40
#define ERR_BUFFERNOFIR   50
#define ERR_NOFIRMACERT   60
#define ERR_NUMINV_CAMPOS 70
#define FTOKEN_INVAL      80
#define SOCK_ENVIA        90
#define CERTINVAL_X_AC    100
#define EXTINVAL_X_AC     110
#define NUM_AC_INVALIDA   120
#define SHMID_INVALIDA    130
#define POINTER_INVAL     140


class CertificadorAC
{
   private :
      RSA         *llave;
      string      &cert_firma;
      string       certAC;
      int          error; 
      int          nExtCert;
      SNames      *extCert;
      CSSL_Skt    *sock;
      int          vigencia;
      char         num_serie[50]; 

      string       pwd_mem;
      string       mem_nm;
      int          mem_id;
      int          num_ac;
      string       tipo_cert;

      bool genera(const uint16 *header);
      bool recibeCadena(const int longitud, uchar **buff);
      bool cargaDatosCert( uchar *buf_pkcs7, int *l_bpkcs7);
      bool levantaLlave();
      bool cargaExtensiones(char *);
      bool verificaFirmaPKCS7( const uchar *b_pkcs7, int l_pkcs7, uchar *b_req, int *l_breq);

      bool cargaParmLlave();
      bool cargaLlave();
      bool cargaLlaveNueva();
      bool verificaLlaves();
      bool cargaLlaveAnterior();
      bool verificaTipoCert(string &tip_cert);
      void guardaArchKey();

   public :
      CertificadorAC(string &s_cert_firma);
      ~CertificadorAC();
      bool atender(CSSL_Skt *skt);
};

#endif // _CERTIFICADORAC_H_

