#ifndef _SRVCERTAC_H_
   #define _SRVCERTAC_H_
static const char* _SRVCERTAC_H_VERSION_ ATR_USED = "@(#) CertificadorAC ( L : DSIC10482AC_ : SrvCertAC.h : 1.1.0 : 1 : 30/11/10)";
//#VERSION: 1.1.0

#define PATH_CONF            "/usr/local/SAT/PKI/etc/"
#define PATH_DBIT            "/var/local/SAT/PKI/"

#ifdef DBG
   #define RUTA_CFG            PATH_CONF "CertificadorAC_dbg.cfg"
   #define RUTA_BIT            PATH_DBIT "CertificadorAC_dbg.log"
#else
   #define RUTA_CFG            PATH_CONF "CertificadorAC.cfg"
   #define RUTA_BIT            PATH_DBIT "CertificadorAC.log"
#endif

#define ERR_CERT_NO_AC  15

class CSrvCertAC : public CSrvUnx 
{
   protected :
      int         error; 
      string      s_arch_conf;
     
   private :
      bool Inicia();
      bool SetParmsSktProc(CSSL_parms &);
      bool Proceso();
      bool cargaParms();

   public :
      CConfigFile *Configuracion;
      string      cert_firma;
      string      cad_cert;

      CSrvCertAC();
      ~CSrvCertAC();
      bool ProcesaParametros(int argc, char* argv[], char* env[]); 
      bool obt_dataCert(const char *certif, bool verifCadCert = false );
}; 


#endif  //_SRVCERTAC_H_

