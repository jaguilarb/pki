/*##############################################################################
  ###  PROYECTO:              PKI-SAT                                        ###
  ###  MODULO:                Reportes de Sellos de la PKI                   ###
  ###                                                                        ###
  ###  DESARROLLADORES:       Miguel Angel Mendoza L�pez      MAML           ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       Martes 4, Mayo del 2010                        ###
  ###                                                                        ###
  ##############################################################################
*/


/*##############################################################################
   VERSION:
      V.1.0.1      (20080805 - 20080815) MAML: 1a versi�n: 
                   Cron /script: genera reportes de la BD y 
                                 los envia via socket a la AR => varios equipos FTP    
#################################################################################*/

#ifndef _REPORTE_SELLOS_H_
   #define _REPORTE_SELLOS_H_
static const char* _REPORTE_SELLOS_H_VERSION_ ATR_USED = "Reporte_Sellos @(#)"\
"DSIC10205AC_ 2010-05-04 Reporte_Sellos.h 1.0.1/0";



//#VERSION: 1.0.1

#define  EXITO               0

#define  ERR_ABRIR_DB        101
#define  ERR_DES_PWD_DB      102
#define  ERR_CONECTA_DB      103
#define  ERR_EJECUTA_QUERY   110 
#define  ERR_ACCESOREPPEND   115 
#define  ERR_MOVARCHIVOREP   116
#define  ERR_ARCHMISMARUTA   117
#define  ERR_PARAMETROS      120
#define  ERR_ARCHNOCERRO     125

#define  MAX_PATH            256
#define  MAX_TAM_QUERY       999    
#define  MAX_FTPS            10    // Soporta hasta 10 FTPs a enviar 

//#############################################################################
class CReporte_Sellos 
{
   CConfigFile *configuracion;
   CBD         *m_BD; 

   int         NivelBit;
   bool        CreaReporSellos;
   int         idArch;
   int         error;
   uint32      no_rows; // Numero de rows del Reporte en question  (long = 4 bytes) 
   int         max_campo_bd;
   short       num_FTPs, envioFTPs[MAX_FTPS];
   char        nom_reporte[MAX_PATH]; // tiene el nombre del archivo reporte sin el path
   std::string s_arch_conf;
   std::string s_separador;
   std::string s_ruta_local;

   std::string s_ruta_reporte;        // tiene el nombre del archivo reporte con el path 

   std::string s_path_dst;
   std::string s_host_dst;
   std::string s_user_dst;   
   std::string s_rutaPendientes;
   std::string s_rutaProcesados;

   void iniciaEnvioFTPs();
   bool creaArchReporte(const char *query);
   bool cargaConfiguracion();
   void creaNombre( const char *nombre );
   int  creaArch (std::string encab);
   int  escribeArch (const char *dato, int len_dato );
   void cierraArch();
   int  conectaBD();
   int  revisaReportesPendientes();
   int  obtieneNumFTPErr(const char *nombreArch);
   int  borraArchivos(const char *ruta, bool borraTodos = true);
   int  transmiteReporte(); 
   int  marcasFTP(int num_FTP_err );
   int  reenvia_RepSellos_FTP(int envioFTP, char *reporteAR );
   int  mueveRepSellos_a_Procesado();
 
   public:
      CBitacora   *Bitacora;   

      CReporte_Sellos();
      ~CReporte_Sellos();
      bool inicia();
      bool genera();
      bool procesaParametros(int args, char* const argv[]);
};

//#############################################################################

#endif //_REPORTE_SELLOS_H_

