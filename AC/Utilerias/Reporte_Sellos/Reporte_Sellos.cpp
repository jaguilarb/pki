static const char* _REPORTE_SELLOS_CPP_VERSION_ ATR_USED = "@(#) Reporte_Sellos ( L : DSIC11025AC_ : Reporte_Sellos.cpp : 1.0.3 : 1 : 07/01/11)";

/*##############################################################################
  ###  PROYECTO:              PKI-SAT                                        ###
  ###  MODULO:                Reportes de Sellos de la PKI                   ###
  ###                                                                        ###
  ###  DESARROLLADORES:       MAML                                           ###
  ###                                                                        ###
  ###  FECHA DE INICIO:       Martes 4, Mayo del 2010                        ###
  ###                                                                        ###
  ##############################################################################
*/


/*##############################################################################
   VERSION:
      V.1.00      (20100504 - 20100511) MAML: 1a versi�n:
                  Cron o script 
      V.1.00.01   Genera reportes de una BD y los envia via socket -> AR -> FTPs
      
      V.1.00.03   (20110107 - 20110107) GHM: Se hizo la modificaci�n para no desplegar en el
                  archivo el pipe de final de linea, se requiere en paralelo modificar la etiqueta 
                  [REPORTES] ENC_1= quitar el ultimo pipe
                   ENC_1=no_serie|fec_inicial_cert|fec_final_cert|RFC|edo_certificado | <-
#################################################################################*/


//#define VERSION "V.1.00.02"
#define VERSION "V.1.00.03" //<+ GHM(20110107) V1.0.3

#include <unistd.h>
#include <string>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <dirent.h>
#include <iostream>

#include <Sgi_Bitacora.h>
#include <Sgi_ConfigFile.h>
#include <Sgi_ProtegePwd.h>
#include <Sgi_BD.h>
#include <SockSeg.h>

#include <Reporte_Sellos.h>


//#################################################################################
#define PATH_CONF            "/usr/local/SAT/PKI/etc/"
#define PATH_DBIT            "/var/local/SAT/PKI/"

#ifdef DBG
   #define ARCH_CONF            PATH_CONF "Reportes_Sellos_dbg.cfg"
   #define PATH_BITA            PATH_DBIT "Reportes_Sellos_dbg.log"
#else
   #define ARCH_CONF            PATH_CONF "Reportes_Sellos.cfg"
   #define PATH_BITA            PATH_DBIT "Reportes_Sellos.log"
#endif

int main(int args, char * argv[])
{
   int res = -1;

   CReporte_Sellos *reportes = new CReporte_Sellos();
   if ( reportes == NULL )
   {
      printf("Error fatal de memoria(CReporte_Sellos).\n");
      return 1;
   }
   if (reportes->procesaParametros(args, argv) &&
       reportes->inicia() && 
       reportes->genera() )
   {
       reportes->Bitacora->escribe(BIT_INFO, "Terminando Proceso de reportes de sellos en la AC." );
       res = 0;
   }
   delete reportes;
   reportes = NULL;

   return res;
}


CReporte_Sellos::CReporte_Sellos() :
   configuracion(NULL),
   m_BD(NULL),
   NivelBit(-99),
   idArch(0),
   error(0),
   num_FTPs(0),
   Bitacora(NULL)
{
   memset(nom_reporte, 0, MAX_PATH);
   s_arch_conf = ARCH_CONF;
}

CReporte_Sellos::~CReporte_Sellos()
{
   if (m_BD)
   {
      delete m_BD;
      m_BD = NULL;
   }
   if (configuracion)
   {
      delete configuracion;
      configuracion = NULL;
   }
   if(Bitacora)
   {
      delete Bitacora;
      Bitacora = NULL;
   }
}

bool CReporte_Sellos::inicia()
{
   Bitacora = new CBitacora(PATH_BITA);

   if ( !Bitacora )
   {
      error = ERR_FALTA_MEMORIA;
      printf("Error fatal de memoria(CBitacora).\n");
   }
   else 
   {
      if (NivelBit >= 0)
         Bitacora->setNivel((BIT_NIVEL) NivelBit);

      Bitacora->escribe(BIT_INFO, "Iniciando proceso");

      cargaConfiguracion(); 
   }
   return !error;
}

// Ejecuta todos los querys q haya en el archivo de Configuraci�n
bool CReporte_Sellos::genera()
{
   std::string num_query[3]; 
   bool  hay_query;
   int   i = 0;
   std::string  s_query, s_nombre, s_encab;       // nombre del archivo reporte 
   char  buffer[10];

   //do  // <<-- Esta de mas por que solo se pidio un reporte de Sellos de est� tipo 
   //{
      sprintf( buffer, "%d", ++i);
      num_query[0] = "REP_" + string( buffer );
      num_query[1] = "NOM_" + string( buffer );
      num_query[2] = "ENC_" + string( buffer ); 
	  //GNC (28102014)
	  #if ORACLE
	    hay_query = (configuracion->getValoresConf("[REPORTES_ORA]", 3, num_query, &s_query, &s_nombre, &s_encab ) == 0);
	  #else
	    hay_query = (configuracion->getValoresConf("[REPORTES]", 3, num_query, &s_query, &s_nombre, &s_encab ) == 0);
	  #endif
        
      if ( hay_query )
      {
         Bitacora->escribe(BIT_DEBUG, s_query.c_str());  // PreparaQuery(s_query); 
         creaNombre( s_nombre.c_str() );
         if ( !CreaReporSellos ) // Siempre suponemos que hay Reportes pendientes 
            revisaReportesPendientes(); // Si no hay q Crear el Reporte de Sellos => revisa si hay pendientes por enviar
         else // Hay q crear el NUEVO Reporte de Sellos
         {
            if ( !(error = conectaBD()) ) // Solo abre la Base cuando crea el archivo de Reporte
               creaArch( s_encab );
            if ( !error )
               creaArchReporte( s_query.c_str() );
            if ( !error ) 
            {
               iniciaEnvioFTPs();
               transmiteReporte();
               mueveRepSellos_a_Procesado(); // transmistas bien o mal, manda el Reporte de Sellos a Procesado.
            }
         } 
      }
   //} while ( !error && hay_query );
   return ( !error );
}

// inicializa el numero de FTPs con los numeros de equipos q hay en el arch de Configuraci�n;  { 1, 2, 0, 0, ... } 
void CReporte_Sellos::iniciaEnvioFTPs()
{
   memset(envioFTPs , 0, sizeof(envioFTPs) );
   for (int i = 0; i < num_FTPs && i < MAX_FTPS; i++ )
      envioFTPs [i] = i + 1; 
}

// Hace un query a la DB y manda los datos a un archivo de texto
bool CReporte_Sellos::creaArchReporte(const char *query)
{
   std::string valor;

   Bitacora->escribePV(BIT_DEBUG, "Inicia reporte %s", s_ruta_reporte.c_str());

   no_rows = 0L;
   if( m_BD->consulta( query ) )
   { 
      while( m_BD->sigReg() ) // Procesa row * row
      {
         for (int i = 0; !error && i < m_BD->getNumColumnas() ; i++) // Procesa columna * columna 
         {
            if ( m_BD->getColumna (i, valor) )
            {
               //if ( i < m_BD->NumColumnas-1 ) // No es la ultima columna le pega el pipe 
               if ( i < m_BD->getNumColumnas()-1 ) //+> GHM(20110107): V1.0.3 Eliminar el ultimo pipe de la linea
                  valor += s_separador;
               error = escribeArch( valor.c_str(), valor.length() );   
            }
         }
         if ( !error ) 
         {
            error = escribeArch( "\n", 1 );  // acabo un renglon, escribe el salto de linea
            no_rows++; 
         } 
      }
      cierraArch();
   }
   else 
   {
      error = ERR_EJECUTA_QUERY; 
      Bitacora->escribePV(BIT_ERROR, "fallo la ejecuci�n del query: %s ", query );
   }

   Bitacora->escribe(BIT_DEBUG, "Fin reporte");

   return !error; 
}

bool CReporte_Sellos::cargaConfiguracion()
{
   Bitacora->escribe(BIT_DEBUG, "Inicia carga configuraci�n");

   configuracion = new CConfigFile( s_arch_conf.c_str(), MAX_TAM_QUERY ); // MALM: maximo de caracteres por linea va a leer del arch de config.
   if( configuracion )
   {
      if( (error = configuracion->cargaCfgVars()) != 0 )
         Bitacora->escribePV(BIT_ERROR, "No se pudieron cargar las variables de configuracion(%d) del archivo(%s).", error, s_arch_conf.c_str() );
      else
      {
         std::string max_campo, num_ftps,
                     config[] = { "SEPARADOR", "MAX_CAMPO_DB", "RUTA_LOCAL", "FTPS" },
                     envio[]  = { "HOST_DST",  "USER_DST",     "PATH_DST",   "RUTA_PENDIENTES", "RUTA_PROCESADOS" };

         error = ( configuracion->getValoresConf("[CONFIG]",   4, config, &s_separador, &max_campo,  &s_ruta_local, &num_ftps ) || 
                   configuracion->getValoresConf("[ENVIO_AR]", 5, envio,  &s_host_dst,  &s_user_dst, &s_path_dst, &s_rutaPendientes, &s_rutaProcesados ) );
         if (!error)
         {
            max_campo_bd = atoi( max_campo.c_str() );
            num_FTPs =     atoi( num_ftps.c_str() ); 
         }
         else
            Bitacora->escribePV(BIT_ERROR, "fallo la funci�n configuracion->getValoresConf(), error(%d) del archivo(%s).", error, s_arch_conf.c_str() );
      }
   }
   else
   {
      error = ERR_FALTA_MEMORIA;
      Bitacora->escribe(BIT_ERROR, "Error fatal de memoria(CConfigFile).");
   }
   
   Bitacora->escribe(BIT_DEBUG, "Fin carga configuraci�n");
   
   return !error;
}

// crea el nombre del archivo del reporte con su ruta 
void CReporte_Sellos::creaNombre(const char *nombre)
{
   struct tm today;
   char   *pPos, c_aux[5];  

   /*sprintf( nom_reporte, "%s.txt", nombre );
   if (!s_fecha_nom.empty() && (pPos = strstr(nom_reporte, "AAAAMMDD")) != NULL)
      strncpy(pPos, s_fecha_nom.c_str(), 8);
   else
   {*/
      // MAML 20100506: El reporte de Sellos se va a hacer a partir de la fecha ocurrente del equipo 
      time_t ltime = time(NULL);
      memcpy(&today, localtime( &ltime ), sizeof(today));

      sprintf( nom_reporte, "%s%s.txt", nombre, (today.tm_hour < 12)?"-AM":"-PM" );
      // Hay q buscar las cadenas AAAA, AA, MM, DD, HH, NN y cambiarlas
      if ((pPos = strstr(nom_reporte,"AAAA")) != NULL)
      {
         sprintf (c_aux, "%04d", today.tm_year + 1900);
         strncpy (pPos, c_aux, 4);
      }
      else if ((pPos = strstr(nom_reporte,"AA")) != NULL)
      {
         sprintf (c_aux, "%02d", today.tm_year - 100);
         strncpy (pPos, c_aux, 2);
      }
      if ((pPos = strstr(nom_reporte,"MM")) != NULL)
      {
         sprintf (c_aux, "%02d", today.tm_mon + 1);
         strncpy (pPos, c_aux, 2);
      }
      if ((pPos = strstr(nom_reporte,"DD")) != NULL)
      {
         sprintf (c_aux, "%02d", today.tm_mday );
         strncpy (pPos, c_aux, 2);
      }
   //}
   //<<< MAML 080904: OK: nom_reporte ya esta correctamente transformado. 
   s_ruta_reporte = s_rutaProcesados + "/" + nom_reporte; // Suponemos q el directorio donde se depositan los archivos SI existe. 

   // Ahora valida si se crea el arch. de Sellos 
   if (today.tm_hour == 0 || today.tm_hour == 12 || access(s_ruta_reporte.c_str(), F_OK) ) // R_OK // && no existe el archivo en PROCESADOS => crealo 
   {
      CreaReporSellos = true;
      s_ruta_reporte = s_ruta_local + "/" + nom_reporte; 
      borraArchivos( s_rutaPendientes.c_str() );  // <<-- FALTA: Borrate los todos los archivos que esten en el directorio de pendientes,
   }
   else
      CreaReporSellos = false;  // no es ni la hora inicial (las 0 y las 12) y ya existe el archivo  
}

// Crea el archivo de texto ( plano ) donde se van a guardar los datos de los Reportes y agrega el encabezado inicial
int CReporte_Sellos::creaArch(std::string encab)
{
   if (idArch)
   {
      Bitacora->escribePV(BIT_ERROR, "Id. Archivo no cerrado(%d), no se creara el archivo %s", idArch, s_ruta_reporte.c_str() );
      return ERR_ARCHNOCERRO;
   }
   idArch  = open(s_ruta_reporte.c_str(), O_WRONLY|O_CREAT, S_IRWXU);
   if(idArch < 0)
   {
      Bitacora->escribePV(BIT_ERROR, "No se pudo crear el archivo %s, errno(%d).", s_ruta_reporte.c_str(), errno );
      return errno;
   }
   else
   {
      encab += "\n"; // le agregamos al encabezado el fin de linea que no tiene
      error = escribeArch( encab.c_str(), encab.length() );
   }
   return !error; // EXITO;
}

// Escribe un dato del archivo
int CReporte_Sellos::escribeArch(const char *dato, int len_dato )
{
   if (write( idArch, dato, len_dato ) < 0)
   {
      cierraArch();
      Bitacora->escribePV(BIT_ERROR, "No se pudo escribir el dato %s en el archivo %s, errno(%d).", dato, s_ruta_reporte.c_str(), errno);
      return errno;
   }
   return EXITO;
}

void CReporte_Sellos::cierraArch()
{
   if (idArch)
   {
      close(idArch);
      idArch = 0;
      //memset(nom_reporte, 0, MAX_PATH);
   }
}


/*-------------------------------------------------------------------------------------------
   PROPOSITO: Esta funcion establece la conexion con la BD.

   EFECTO:    No tiene efecto sobre el ambiente.
   SALIDAS:   Regresa: 0 -> Exito o un codigo de error.
  -------------------------------------------------------------------------------------------*/
int CReporte_Sellos::conectaBD()
{
   Bitacora->escribe(BIT_DEBUG, "Inicia conectaBD");

   std::string s_srv, s_db, s_paswd, s_nombre, s_role, 
    claves[] = { "SERVIDOR", "BD", "PASSWORD", "USUARIO", "ROL" }, claves_ORA[] = { "usr", "pwd", "host", "port", "sid" };

	
   if (m_BD)
   {
      Bitacora->escribePV(BIT_ERROR, "Error(%d) al abrir BD: ya se encuentra abierta la conexi�n a la BD", ERR_ABRIR_DB );
      return ERR_ABRIR_DB;
   }
     //GNC (28102014)
   #if ORACLE
    error = configuracion->getValoresConf("[BD_ORA]", 5, claves_ORA, &s_nombre, &s_paswd, &s_srv, &s_db, &s_role );
   #else
    error = configuracion->getValoresConf("[BD]", 5, claves, &s_srv, &s_db, &s_paswd, &s_nombre, &s_role );
   #endif
   
   
   if ( error != 0 )
   {
      Bitacora->escribePV(BIT_ERROR, "Error(%d) al obtener valores de conexi�n de la BD en getValoresConf()", error );
      return error;
   }

   // OjO: por si se llega a encriptar el password y asi se pone en el archivo de Config.
   uint8 password[128];
   int i_password = sizeof(password);

   if (!desencripta((uint8*)s_paswd.c_str(), s_paswd.length(), password, &i_password)  )
   {
     Bitacora->escribePV(BIT_ERROR, "Error(%d) al desencriptar el password de conexi�n a la BD", ERR_DES_PWD_DB );
      return ERR_DES_PWD_DB;
   }
   password[i_password] = 0;
   //

   m_BD = new CBD(Bitacora);
   bool ok  = m_BD != NULL;  
   
   //GNC (28102014)
  #if ORACLE
    ok = ok && m_BD->setConfig(s_nombre.c_str(), (char *)password, s_srv.c_str(), s_db.c_str(), s_role.c_str());
  #else
   ok = ok && m_BD->setConfig(s_srv.c_str(), s_db.c_str(), s_nombre.c_str(), (char *)password, s_role.c_str());
  #endif  
      
   ok = ok && m_BD->conecta();
   if (!ok)
   {
     Bitacora->escribePV(BIT_ERROR, "Error(%d) al conectarse a la BD", ERR_CONECTA_DB );
      return ERR_CONECTA_DB;
   }

   Bitacora->escribe(BIT_DEBUG, "Fin conectaBD");

   return EXITO;
}

// #####################################################################################################################

// Obtiene los numeros de FTPs q no se transmitieron en el arreglo envioFTPs[]
// transmiteReporte y p/terminar Borra los archivos de marcasFTP q si haya podido transmitir (envioFTPs[i] > 10)
int CReporte_Sellos::revisaReportesPendientes()
{
   DIR    *dp;
   struct dirent *ep;
   int    nftp, i = 0;

   Bitacora->escribe(BIT_DEBUG, "Inicia revisa pendientes");

   // OjO: Para no hacerlo uno por uno y estar enviando a la AR, lo mejor es tener un arreglo con los int Envia_FTPs[10] = { 2, 0, 0, ... };
   //      Y al despues de este while ya mandamos llamar TransmiteArch

   memset(envioFTPs , 0, sizeof(envioFTPs) ); // Iniciamos el Arreglo de FTPs todo limpio
   dp = opendir(s_rutaPendientes.c_str());
   if ( dp != NULL) 
   {
      while((ep = readdir(dp))) // Obtiene los numeros de FTPs q no se transmitieron en el arreglo envioFTPs[]
      {
         if ((!strcmp(ep->d_name,".")) || (!strcmp(ep->d_name,"..")))
            continue;
         nftp = obtieneNumFTPErr(ep->d_name);
         if (nftp > 0)
            envioFTPs[i++] = nftp;
       //s_ruta_reporte = s_rutaPendientes + "/" + ep->d_name;
       //strcpy(nom_reporte, ep->d_name); // se supone que el nombre original del reporte ya esta en Procesados =>
                                          // Debe obtener el numero de FTP que fallo, num_FTP_err > 0
      }
      closedir(dp);

      if ( envioFTPs[0] > 0 ) // Por lo menos existe un archivo pendiente a reenviar
      {
         transmiteReporte();
         borraArchivos( s_rutaPendientes.c_str(), false ); // Borra los archivos de marcasFTP q si haya podido transmitir (envioFTPs[i] > 10)
      } 
   }
   else
   {
      Bitacora->escribePV(BIT_ERROR, "Al abrir el repositorio %s", s_rutaPendientes.c_str());
      return ERR_ACCESOREPPEND;
   }

   Bitacora->escribe(BIT_DEBUG, "Fin revisa pendientes");

   return EXITO;
}

// recibe el nombre del archivo que esta en pendientes y de acuerdo al formato dado al archivo obtiene el numero de FTP q le corresponde
int CReporte_Sellos::obtieneNumFTPErr(const char *nombreArch)
{
   //                                       01234567890123456789012
   // SUPONEMOS q el Formato del nombre es "Sellos20100429-#M_FTP_1.txt"
   int j = 0, c = 0;

   if ( strlen( nombreArch ) > 25)
      c = nombreArch[22];
   if ( (c > 0) && isdigit(c) )
   {
      char aux[3];
      sprintf(aux, "%c", nombreArch[22] ); 
      j = atoi( aux );
   }
   return j;
}

//############################################################################################################
// 080827: Borra todos los archivos de un directorio dado
int CReporte_Sellos::borraArchivos(const char *ruta, bool borraTodos )
{
   DIR    *direc;
   struct dirent *lista_nom;
   char   borra[2*MAX_PATH];
   int    nftp;

   direc = opendir( ruta );
   if(direc == NULL)
   {
      Bitacora->escribePV(BIT_ERROR, "Error al abrir el Directorio=%s, errno=%d", ruta, errno);
      return -1;
   }
   while((lista_nom = readdir(direc))!= NULL )
   {
      if ( !strcmp(lista_nom->d_name,".") || !strcmp(lista_nom->d_name,"..") )
        continue;
      sprintf(borra,"%s%s", ruta, lista_nom->d_name );
      if (  borraTodos  )
         unlink(borra);
      else if ( (nftp = obtieneNumFTPErr(lista_nom->d_name) ) > 0 ) // busca en el nombre d archivo el numero d FTP  que le corresponde 
      {
         // y ahora busca en (envioFTPs[i] > 10) si (envioFTPs[i]-10 == nftp)
         for (int i = 0; i < num_FTPs && i < MAX_FTPS && envioFTPs[i] > 0 ; i++ ) 
         {
            if ( envioFTPs[i] > 10 && ((envioFTPs[i]-10) == nftp) )  // est� es el numero de FTP q si pudo reenviarse
            {
               unlink(borra);
               break; // sale de este for no del while, DEPURAR
            }
         }
      }
   } // END del WHILE
   closedir(direc);
   direc = NULL;
   lista_nom = NULL;
   return 0; 
}

// Ahora Envia via socket de Seguridad: un archivo de datos (Reporte) 1ero a la AR y despues a los FTPs
int CReporte_Sellos::transmiteReporte()
{
   char         mens_err[1204], nombreReporteRemoto[MAX_PATH*2]; //, comando[1024];
   std::string  comando;

   sprintf(nombreReporteRemoto, "%s%s", s_path_dst.c_str(), nom_reporte); // OjO: s_path_dst debe terminar con el separador de la plataforma '\\' � '/'

   error = TransmiteArchivo((char*)s_host_dst.c_str(),
           (char*)s_user_dst.c_str(), (char*)s_ruta_reporte.c_str(), nombreReporteRemoto, mens_err);
   if(error != 0)
   {
      Bitacora->escribePV(BIT_ERROR, "Al enviar(%d) a la AR el archivo local(%s), error --> %s",
                                      error, s_ruta_reporte.c_str(), mens_err);
      // return ERR_ENVIO_AR; // mas bien si ni siquiera llega a la AR => q se salga de est� funci�n
   }
   else
   {
      Bitacora->escribePV(BIT_INFO, "Se transmiti� el reporte a la AR (%s)", nom_reporte ) ;
      for (int i = 0; i < num_FTPs && envioFTPs[i]; i++) 
      {
         reenvia_RepSellos_FTP( envioFTPs[i], nombreReporteRemoto );
         if ( error && CreaReporSellos )
            marcasFTP(envioFTPs[i]);   // Crea la marca del numero de FTP al q no se mando el archivo 
         else if ( !error && !CreaReporSellos) // ie reenvio correctamente el archivo de Sellod faltante <<-- DEPURARLO aqui 
         {
            Bitacora->escribePV(BIT_INFO, "Se reenv�o el reporte de sellos pendiente(%s) al FTP(%d)", nom_reporte, envioFTPs[i] );
            envioFTPs[i] += 10;        // En lugar de ser valor 2 va a ser 12 es decir > 10
         }
      }   
      // Comando Remoto del Socket para borrar el archivo q se transmitio a la AR (Linux) temporalmente 
      comando = "rm -f " + string( nombreReporteRemoto ); //"DEL /Q " +  nombreReporteRemoto;
      int res = EjecutaCmdoRem( (char*)s_host_dst.c_str(), (char*)s_user_dst.c_str(), (char*)comando.c_str(), mens_err  );
      if( res )
         //if( res != -41)
         Bitacora->escribePV(BIT_ERROR, "Al ejecutar en la AR el borrado del archivo(%s) con el socket(%d), error --> %s",
                                         comando.c_str(), res, mens_err);
   }
   return error;
}

// crea la marca pendiente del archivo reporte de Sellos con el numero_FTP q fallo y con su ruta
// => crea en: s_rutaPendientes  + "/" + std::string(nom_reporte) + "_FTP_i"; con tama�o cero <<-- Marcas del Mediador
int CReporte_Sellos::marcasFTP(int num_FTP_err )
{
   char         aux_nom_reporte[MAX_PATH];
   std::string  s_marcaFTP;   
   int          len = strlen(nom_reporte) - 4; // - ".txt"

   sprintf(aux_nom_reporte, "%*.*s_FTP_%d.txt", len, len, nom_reporte, num_FTP_err ); 
   s_marcaFTP = s_rutaPendientes + "/" + aux_nom_reporte ; // Suponemos q el directorio donde se depositan los archivos SI existe.
   int iDesc = open(s_marcaFTP.c_str(), O_CREAT, S_IRWXU);
   if(iDesc < 0)
   {
      Bitacora->escribePV(BIT_ERROR, "Al crear el archivo de marca(%s) para el FTP(%d), errno=%d", s_marcaFTP.c_str(), num_FTP_err, errno );
      return errno;
   }
   close(iDesc);
   return (iDesc < 0);
}

// Ejecutar los 2 comandos remotos para reenvio de la AR(Linux) al FTP(Windows) y luego renombrar: CSD.txt
// Obtiene los datos del FTP hacia donde se va a reenviar el reporte de Sellos y ejecuta los 2 comandos remotos
int CReporte_Sellos::reenvia_RepSellos_FTP(int envioFTP, char *reporteAR )
{
   std::string  s_ftp, s_ip_des, s_usu_rem, s_dir_des, comando, reporteFTP,
                claves[] = { "IP_DES", "USU_REM", "DIR_DES" };
   char  buffer[10], mens_err[1204], comandoFTP[1024];

   Bitacora->escribePV(BIT_DEBUG, "Inicia envio al FTP:%d", envioFTP);
   sprintf( buffer, "%d]", envioFTP);
   s_ftp = "[FTP_" + string( buffer );
   if ( (error = configuracion->getValoresConf( s_ftp.c_str(), 3, claves, &s_ip_des, &s_usu_rem, &s_dir_des )) != 0 )
      Bitacora->escribePV(BIT_ERROR, "Error(%d) al obtener valores de conexi�n del bloque %s en getValoresConf()", error, s_ftp.c_str() );
   else
   {
      // 1er. Comando Remoto: usamos el soky para reenviar el archivo de la AR(Linux) al FTP_i(Win)
      reporteFTP =  s_dir_des + std::string(nom_reporte); // s_dir_des: termina con el caracter '\'
      // soky <T> <host> <login remoto> <arch local> <arch remoto>
      comando = "soky T "+ s_ip_des + " " + s_usu_rem + " " + std::string(reporteAR) + " " + reporteFTP + " > " + s_path_dst + "RepSello.soky";
      error = EjecutaCmdoRem( (char*)s_host_dst.c_str(), (char*)s_user_dst.c_str(), (char*)comando.c_str(), mens_err  );
      if( error )
         Bitacora->escribePV(BIT_ERROR, "Al ejecutar en la AR el reenvio del reporte(%s) con el socket(%d), error --> %s",
                                         comando.c_str(), error, mens_err);
      else
      {
         // 2o. Comando Remoto: usamos el soky para ejecutar el renombrado del archivo reporteFTP en el FTP_i(Win)
         sprintf ( comandoFTP, "\"move /Y %s %sCSD.txt\"", reporteFTP.c_str(), s_dir_des.c_str() );  // usamos move de WIN 
         // soky <X> <host> <login remoto> <comando remoto>
         comando = "soky X "+ s_ip_des + " " + s_usu_rem + " " + std::string(comandoFTP) + " > " + s_path_dst + "RepSello.soky";
         error = EjecutaCmdoRem( (char*)s_host_dst.c_str(), (char*)s_user_dst.c_str(), (char*)comando.c_str(), mens_err  );
         if( error )
            Bitacora->escribePV(BIT_ERROR, "Al ejecutar en la AR el renombrado del reporte(%s) con el socket(%d), error --> %s",
                                            comando.c_str(), error, mens_err);
         else
            Bitacora->escribePV(BIT_INFO, "Se env�o el correctamente al FTP(%d) el archivo reporte(%s)", envioFTP, nom_reporte );
      }
   }
   Bitacora->escribePV(BIT_DEBUG, "Termina el envio al FTP:%d", envioFTP);
   return error;
}

// Recuerda el mover solo se hace una vez y es cuando CreaReporSellos == true, no depende de si el socket envio ok o no => esto deberia hacerse primero
int CReporte_Sellos::mueveRepSellos_a_Procesado()  
{
   std::string  archDest;

   archDest = s_rutaProcesados + "/" + std::string(nom_reporte);
   // Solo haz el rename/mueve si los nombres de archivo son diferentes, si son iguales no hagas nada.
   if ( !strcmp( s_ruta_reporte.c_str(), archDest.c_str() ) )
   {
      Bitacora->escribePV(BIT_DEBUG, "El archivo local(%s) no se ha movido, sigue en la misma ruta.", s_ruta_reporte.c_str() );
      return ERR_ARCHMISMARUTA;
   }
   if ( rename(s_ruta_reporte.c_str(), archDest.c_str()) < 0 )
   {
      Bitacora->escribePV(BIT_ERROR, "No se movio el archivo local (%s) al directorio (%s)", s_ruta_reporte.c_str(), s_rutaProcesados.c_str() );
      return ERR_MOVARCHIVOREP;
   }
   else
   {
      Bitacora->escribePV(BIT_DEBUG, "Se movi� el archivo (%s) al directorio (%s)", s_ruta_reporte.c_str(), s_rutaProcesados.c_str() );
      return EXITO;
   }
}

// Se puede correr como cron de diferentes maneras usando un archivo de configuracion con diferentes querys.
bool CReporte_Sellos::procesaParametros(int argc, char* const argv[])
{
   bool ok = true;
   int opcion;

   opterr = 0; // para que getopt no mande el mensaje de error al stderr
   while (ok && (opcion = getopt(argc, argv, "n:c:v")) != -1) // "c:i:f:n:pv"
   {
      switch (opcion)
      {
         case 'n':   // Nivel de bitacora
                     NivelBit = atoi(optarg);
                     if ( NivelBit <= 0 )
                        ok = false;
                     break;
         case 'c':   // Archivo de configuraci�n
                     s_arch_conf = optarg;
                     break;
         case 'v':   // Versi�n
                     std::cout << "Reportes " << VERSION << std::endl;
                     return false;
         default :   // Opci�n no reconocida
                     ok = false;
      }
   }
   if (!ok)
   {
      std::cout << "Uso: " << argv[0] << " [opciones]\n" << std::endl;
      std::cout << "\t-c arch_config    -- indica el archivo de configuraci�n para el proceso" << std::endl;
      std::cout << "\t-i fec_ini        -- fecha inicial del intervalo del reporte" << std::endl;
      std::cout << "\t                        YYYYMMDD" << std::endl;
      std::cout << "\t-f fec_fin        -- fecha final del intervalo del reporte" << std::endl;
      std::cout << "\t-n nivel_bitacora -- indica el nivel de mensajes en la bitacora" << std::endl;
      std::cout << "\t   2                 alertas" << std::endl;
      std::cout << "\t   3                 debug"   << std::endl;
      std::cout << "\t-p                -- s�lo transmite los archivos pendientes" << std::endl;
      std::cout << "\t-v                -- proporciona la versi�n de la aplicaci�n" << std::endl;
      std::cout << "\n" << std::endl;
   }

   return ok;
}

// #####################################################################################################################
