static const char* _OBTIENELLAVE_CPP_VERSION_ ATR_USED = "ObtieneLlave @(#)"\
"DSIC07412AC_ 2007-12-14 ObtieneLlave.cpp 1.1.0/0";

//#VERSION: 1.1.0
#include <SgiOpenSSL.h>
#include <AdmSegPKI.h>
#include <ShMemPKI.h>

bool leellave(RSA **rsa)
{
   SGIRSA rsaalg;
   SGIPRIVADA *privada = new SGIPRIVADA;
   RegServPKI *srv;
   RSA *llave1 = NULL;
   bool ok = true;
   uint8 *shmbuff;
   char *shmseg;
   int shmid;
   int reshm;
   llave1 = RSA_new();
   if(!llave1)
      return false;
   shmid = shmget(KEY_SHMEM, 0, 0);
   if(shmid <= 0)
      return  false;
   shmseg = (char*)shmat(shmid, 0, 0);
   srv = new RegServPKI;
   memcpy(srv, shmseg, sizeof(struct reg_serv_pki_st));
   memcpy(privada, srv->idserv->privKey, sizeof(SGIPRIVADA));
   intE error = rsaalg.getPrivada(privada, llave1);
   if(error != 0)
      ok = false;
   reshm = shmdt(shmseg);
   if(reshm < 0)
      ok = false;
   *rsa = RSAPrivateKey_dup(llave1);
   delete privada;
   delete srv;
   RSA_free(llave1);
   return ok;
}

int main(int argc, char *argv[])
{
   if ( argc < 2 )
   {
      cout << "Uso: ObtieneLlave RutaSalida" << endl;
      cout << "ObtieneLlave: Aplicacion." << endl;
      cout << "RutaSalida: Ruta para la llave privada." << endl;
      return -1;
   }  
   iniciaLibOpenSSL(); 

   int error = 0;
   char pass[256];
   SGIRSA sgirsa;
   RSA *rsa = NULL;

   rsa = RSA_new();
   if (!rsa)
   {
      cout << "Error al asignar memoria al objeto RSA." << endl;
      terminaLibOpenSSL();
      return -2;
   }

   sprintf(pass, "%s", getpass("Escriba la contrasenia para la llave: "));

   if(leellave(&rsa))
   {
      error = sgiErrorBase(sgirsa.CodPvKey(rsa, (uchar*)pass, strlen(pass), (uchar*)argv[1], (int)DES_CBC_ALG, 1));
      if( error )   
         cout << "Error " << "(" << error << ")" << " al encriptar la llave privada al archivo." << endl;
   }
   if(error == 0)
      cout << "Llave guardada correctamente." << endl;

   terminaLibOpenSSL();
   return error;
}
