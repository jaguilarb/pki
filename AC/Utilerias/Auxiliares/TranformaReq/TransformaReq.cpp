static const char* _TRANSFORMAREQ_CPP_VERSION_ ATR_USED = "TranformaReq @(#)"\
"DSIC07412AC_ 2007-12-14 TransformaReq.cpp 1.1.0/0";

//#VERSION: 1.1.0
#include <SgiOpenSSL.h>

int main(int argc, char *argv[])
{
   if ( argc != 3 )
   {
      cout << "Uso: TransformaReq Entrada Salida" << endl;
      return -1;
   }
   X509_REQ *req = NULL;
   FILE *fp = NULL;

   fp = fopen(argv[1], "rb");
   if ( fp )
   {
      req = X509_REQ_new();
      if ( req )
      {
         if ( PEM_read_X509_REQ(fp, &req, NULL, NULL ))
         {
            fclose(fp);
            fp = fopen(argv[2], "wb");
            if ( i2d_X509_REQ_fp(fp, req) )
            {
               X509_REQ_free(req);
               req = NULL;
               fclose(fp);
               return 0;
            }
         }         
      }
      else
      {
         fclose(fp);
         return -1;
      }
   }
   X509_REQ_free(req);
   req = NULL;
   fclose(fp);
   return -1;  
}
