static const char* _VIG27FIX_CPP_VERSION_ ATR_USED = "Vig27Fix @(#)"\
"DSIC12153AC_ 2012-04-11 Vig27Fix.cpp 1.0.0/1";

#include <Sgi_OpenSSL.h>
#include <SgiTipos.h>
#include <Sgi_B64.h>
#include <stdio.h>
#include <SgiTipos.h>
#include <sys/stat.h>
#include <Sgi_BD.h>
#include <Sgi_Bitacora.h>
#include <unistd.h>
#include <signal.h>
#include <Sgi_PKI.h>

#include <string>
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <ostream>
#include <istream>


//##################################################################################################################
// GNC Se agregan definiciones de consultas

#if ORACLE
	//----> CONSULTA ORACLE
	#define QD_CERTIFICADO       "delete SEG_PKI.SEGT_BIT_CERTIFICADO where NOSERIE= '%s';"
	#define QU_VF_CERTIFICADO    "update SEG_PKI.SEGT_CERTIFICADO set VIGFIN = '%s' where NOSERIE = '%s';"
#else
	#define QD_CERTIFICADO       "delete bit_certificado where no_serie= '%s';"
	#define QU_VF_CERTIFICADO    "update certificado set vig_fin = '%s' where no_serie = '%s';"
#endif

//##################################################################################################################



using std::string;
using std::ifstream;
using std::vector;
using std::stringstream;
using std::cout;
using std::endl;

int fileSize(const char* fPath)
{
   struct stat st;
   int err = stat(fPath, &st);
   return st.st_size;
}

CBD* hdBD;
CBitacora* Bitacora;
//ifstream* ifs;
//ofstream* ofs;
int filePos;

int conectaBD(string srv, string db, string us, string pwd, string role)
{
   CBD* &hd_db = hdBD;
   if (!hd_db)
   {
      hd_db = new CBD(Bitacora);
      bool ok = hd_db != NULL;
      ok = ok && hd_db->setConfig(srv.c_str(), db.c_str(), us.c_str(), pwd.c_str(), role.c_str());
      ok = ok && hd_db->conecta();
      if (!ok)
      {
         cout << "Error al conectarse a la Base de Datos" << endl;
         return -1;
      }
      return 0;  
   }
   else return -2; //La base de datos ya est� abierta
}

void desconectaBD()
{
   hdBD->desconecta();
   delete hdBD;
   hdBD = NULL;
}

/*void procesaLinea(string linea, char* no_serie, char* vig_fin, char* edo_cer)
{
   string elemento, l_tmp;
   l_tmp = linea;
   stringstream ss(l_tmp);
   vector<string> results;
   while (getline(ss, elemento, '|' ))
   {
      results.push_back(elemento);
   }
   strcpy(no_serie, results[0].c_str());
   strcpy(vig_fin, results[10].c_str());
   strcpy(edo_cer, results[2].c_str());
}*/

void procesaLinea(string linea, char* no_serie, char* vig_fin, char* edo_cer)
{
   CSeparaToken* Tkn = NULL;
   if ((Tkn = new CSeparaToken((char*)linea.c_str(), '|')) != NULL)
   {
      strcpy(no_serie, Tkn->items[0]);
      strcpy(vig_fin, Tkn->items[10]);
      strcpy(edo_cer, Tkn->items[2]);
   }

}

bool operacion(char* no_serie, char* vig_fin, char* edo_cer)
{
   bool ok = false;
   ok = hdBD->ejecutaOper(QD_CERTIFICADO, no_serie);
   if (ok)
   {
      ok = hdBD->ejecutaOper(QU_VF_CERTIFICADO, vig_fin, no_serie);
   }
   return ok;
}

void maneja_sig(int sig)
{
   desconectaBD();
   ofstream ofS("pos.cfg");
   char posStr[10];
   sprintf(posStr, "%i\n", filePos);
   ofS.write(posStr, strlen(posStr));
   ofS.close();
}

void armaPathProcesados(char* in, int lin, char* out, int* lout)
{
   char* complemento = "_procesados.txt";
   char salida[256];
   memset(salida, 0, sizeof(salida));
   strncpy(salida, in, lin-4);
   strcat(salida, complemento);
   *lout = lin-4+strlen(complemento);
   salida[*lout] = '\0';
   strncpy(out, salida, *lout);
   
}

int main(int argc, char*argv[])
{
   if (argc < 5)
   {
      cout << "Uso: Vig27Fix PathArchivo User Server BaseDatos" << endl;
      cout << "Vig27Fix: Aplicaci�n que se va a ejecutar." << endl;
      cout << "PathArchivo: Ruta y nombre del archivo que se va a procesar." << endl;
      cout << "User: Usuario de la Base de Datos con el que se va a ejecutar el proceso." << endl;
      cout << "Server: Servidor de Informix al que se va a conectar." << endl;
      cout << "BaseDatos: Nombre de la Base de Datos a la que se va a conectar." << endl;
      return -1;
   }
   //Manejo de la se�al
   void maneja_sig(int sig);
   if(signal(SIGINT,maneja_sig)==SIG_ERR)
   {
      perror("signal");
      exit(-1);
   }
   //Fin
   //Agregar el control de posici�n de registro procesado...
   string ln;
   ifstream ifS("pos.cfg", std::ios::in);
   if (ifS)
      getline(ifS, ln);
   else 
   {
      cout << "Error no existe el archivo de configuracion pos.cfg" << endl;
      return -1;
   }
   filePos = atoi(ln.c_str());
   ifS.close();
   //Fin
   string line; 
   vector<string> vs;
   ifstream ifs(argv[1], std::ios::in);
   if (!ifs)
   {
      std::cout << "Error al leer el archivo " << argv[1] << endl;
      return -2;
   }
   ifs.seekg(filePos, ios::cur);
   char pathSalida[256];
   int tamPath = sizeof(pathSalida);
   armaPathProcesados(argv[1], strlen(argv[1]), (char*)pathSalida, &tamPath);
   pathSalida[tamPath] = 0;
   ofstream ofs(pathSalida, std::ios::out);
   //Conectamos a la base de datos
   char pwd[256];
   char no_serie[21], vig_fin[50], edo_cer[2];
   sprintf((char*)pwd, getpass("Proporcione el password de usuario de Base de Datos: "));
   Bitacora = new CBitacora("Vig27Fix.log");
   if (conectaBD(argv[3], argv[4], argv[2], pwd, "") != 0)
      return -1;
   cout << "Conectado a la base de datos." << endl;
   cout << endl << "Iniciando proceso de actualizacion." << endl;
   while ((getline(ifs, line)))
   {
      procesaLinea(line, no_serie, vig_fin, edo_cer); 
      if (edo_cer[0] == 'A')
      {
         if (operacion(no_serie, vig_fin, edo_cer))
         {
            Bitacora->escribePV(BIT_INFO, "Procesado el Certificado: %s con vigencia final: %s", no_serie, vig_fin);
            line += "\n";
            ofs.write(line.c_str(), line.length());
            filePos = ifs.tellg();
         }
      }    
   }
   //Desconectamos de la bd
   desconectaBD();
   delete Bitacora;
   ifs.close();
   ofs.close();
   cout << "Terminado proceso de actualizacion." << endl;
}
