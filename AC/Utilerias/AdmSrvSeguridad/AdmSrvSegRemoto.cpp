static const char* _ADMSRVSEGREMOTO_CPP_VERSION_ ATR_USED = "AdmSrvSeguridad @(#)"\
"DSIC07412AC_ 2007-12-14 AdmSrvSegRemoto.cpp 1.1.0/0";

//#VERSION: 1.1.0
/*******************************************
 * Administrador de Servicios de Seguridad *
 * Febrero 2004.                           *
 * Reprogramacion de funciones Sep 2004    *
 *******************************************/
//>+ ERGL mar mar 20 12:36:28 CDT 2007
#include <string>
//- #include <CConfigFile.h>  //- GHM (070427)
#include <Sgi_ConfigFile.h>   //+ GHM (070427)

//- #include "PrincipalRemoto.h" //- GHM (070427)
#include "AdmSrvSegRemoto.h"     //+ GHM (070427)
//#include <ArchivosConfg.h>
//- #include <PwdProtection.h> //- GHM (070427)
#include <Sgi_ProtegePwd.h>    //+ GHM (070427)
#include <openssl/md5.h>
#include <openssl/rc4.h>

//#################################################################################
#define PATH_CONF            "/usr/local/SAT/PKI/etc/"

#ifdef DBG
   #define ARCH_CONF         PATH_CONF "AdmSrvSeguridad_dbg.cfg"
#else
   #define ARCH_CONF         PATH_CONF "AdmSrvSeguridad.cfg"
#endif

//################################################################################
//   Variables globales
//################################################################################
#define SIZEBLLAVE           60
static char bLLave[SIZEBLLAVE];
//>- ERGL static char* PATHS[] = {"IP","PTO","ACCER","ACPATH","AC1","ACK","BLLAVE"};

int verify_callback(int ok, X509_STORE_CTX *store)
{
   char data[256];
   if (!ok)
   {
      X509 *cert = X509_STORE_CTX_get_current_cert(store);
      int depth = X509_STORE_CTX_get_error_depth(store);
      int err = X509_STORE_CTX_get_error(store);
      fprintf(stderr,"Error con el certificado %i\n",depth);
      X509_NAME_oneline(X509_get_issuer_name(cert),data,256);
      fprintf(stderr,"issuer = %s\n",data);
      X509_NAME_oneline(X509_get_subject_name(cert),data,256);
      fprintf(stderr,"subject=%s\n",data);
      fprintf(stderr,"err%i:%s\n",err,X509_verify_cert_error_string(err));
   }
   return (ok);
}

static int password_cb(char *buf, int size, int rwflag, void *password)
{
   password = bLLave;
   strncpy(buf, (char *)(password), size);
   buf[size - 1] = '\0'; //buf[strlen(bLLave)] = '\0';
   rwflag = 0;
   return(strlen(buf));
}

double CalcTagLen(uchar *bytes, int tam)
{
   int i = 0;//, total = 0;  //, ttag = 0;
   double ttag = 0, total = 0;
   //char *cadena = (char *)calloc(tam, sizeof(char));
   char cadena[tam];
   char c[3];
   int am = 0;
   //>- cadena = new char[tam]; // ERGL  vie jul 27 17:52:18 CDT 2007
   //>- memset(cadena, 0, tam); // ERGL  vie jul 27 17:52:18 CDT 2007
   if(tam == 1)
      sprintf(cadena, "%x", bytes[0]);
   else sprintf(cadena, "%x%x", bytes[0], bytes[1]);
   int ta = strlen(cadena);
   for(i = 0; i < ta; i++)
   {
      if(cadena[i] == 'a')
      {
         ttag = 0;
    ttag = pow(16, (ta-(i+1)));
    ttag = (ttag*10);
    total += ttag;
      }
      else if(cadena[i] == 'b')
      {
         ttag = 0;
         ttag = pow(16, (ta-(i+1)));
         ttag = (ttag*11);
    total += ttag;
      }
      else if(cadena[i] == 'c')
      {
         ttag = 0;
         ttag = pow(16, (ta-(i+1)));
         ttag = (ttag*12);
         total += ttag;
      }
      else if(cadena[i] == 'd')
      {
         ttag = 0;
         ttag = pow(16, (ta-(i+1)));
         ttag = (ttag*13);
         total += ttag;
      }
      else if(cadena[i] == 'e')
      {
         ttag = 0;
         ttag = pow(16, (ta-(i+1)));
         ttag = (ttag*14);
         total += ttag;
      }
      else if(cadena[i] == 'f')
      {
         ttag = 0;
         ttag = pow(16, (ta-i));
         ttag = (ttag*15);
         total += ttag;
      }
      else 
      {
         memset(c, 0, 2);
         ttag = 0;
         c[0] = cadena[i];
         am = atoi(c);
         ttag = pow(16, (ta-(i+1)));
         ttag = (ttag*am);
         total += ttag;
      }
   }
   //delete []cadena;
   return total;
}

void leeTags(uchar *buff, int lbuff, ASNTag *tags, int *ntags)
{
   int i = 0;
   int desp = 0;
   int tagDesp = 0;
   uchar tagTam[3];
   int ntagS = 0;
   memset(tagTam, 0, sizeof(tagTam));
   for(i = 0; i < lbuff; i++)
   {
      if(buff[i] == 0x30)
         tags[tagDesp].compuesta = true;
      else 
         tags[tagDesp].compuesta = false;
      memcpy(tags[tagDesp].idTag, buff + i, 1);
      tags[tagDesp].numTags = 0;
      if(buff[i+1] == 0x82)
      {
         memcpy(tagTam, buff + (i+2), 2);
         if(int(CalcTagLen(tagTam, 2) + 4) == lbuff && i == 0)
         {
            tags[tagDesp].tam = int(CalcTagLen(tagTam, 2));
            tags[tagDesp].correcta = true;
            tags[tagDesp].posArray = i;
         }
         else
         {
            tags[tagDesp].tam = int(CalcTagLen(tagTam, 2));
            tags[tagDesp].correcta = true;
            tags[tagDesp].posArray = i;
            desp += tags[tagDesp].tam + 2;
            i = desp;
            continue;
         }
         desp += desp + 3;
         ntagS += 1;
      }
      else
      {
         memcpy(tagTam, buff + (i+1), 1);
         tags[tagDesp].tam = int(CalcTagLen(tagTam, 1));            
         if(buff[i+tags[tagDesp].tam+1+1] == 0x30)
            tags[tagDesp].correcta = true;
         else if(buff[i+tags[tagDesp].tam+1+1] == 0x01)
            tags[tagDesp].correcta = true;
         else if(buff[i+tags[tagDesp].tam+1+1] == 0x02)
            tags[tagDesp].correcta = true;
         else if(buff[i+tags[tagDesp].tam+1+1] == 0x03)
            tags[tagDesp].correcta = true;
         else if(buff[i+tags[tagDesp].tam+1+1] == 0x04)
            tags[tagDesp].correcta = true;
         else if(buff[i+tags[tagDesp].tam+1+1] == 0x05)
            tags[tagDesp].correcta = true;
         else if(buff[i+tags[tagDesp].tam+1+1] == 0x06)
            tags[tagDesp].correcta = true;
         else if(buff[i+tags[tagDesp].tam+1+1] == 0x13)
            tags[tagDesp].correcta = true;
         else if(buff[i+tags[tagDesp].tam+1+1] == 0x16)
            tags[tagDesp].correcta = true;
         else if(buff[i+tags[tagDesp].tam+1+1] == 0x17)
            tags[tagDesp].correcta = true;
         else 
            tags[tagDesp].correcta = false;
         tags[tagDesp].posArray = i;
         if(buff[i] == 0x30)
            desp += 2;
         else desp += tags[tagDesp].tam + 2;
         ntagS += 1;
      }
      i = desp;
      tagDesp++;
      continue;
   }
   *ntags = ntagS;
}

int compInc(ASNTag *t, int nt)
{
   int comp = 0; 
   for(int i = 0; i < nt; i++)
   {
      if(t[i].compuesta == true && t[i].correcta == false)
         comp ++;
      else continue;
   }
   return comp;
}

int calcTam(ASNTag *tags, int ntags)
{
   int total = 0;
   int i = 0;
   for(i = 0; i < ntags; i++)
   {
      if(tags[i].compuesta == false && tags[i].correcta == true)
         total += (tags[i].tam + 2);
      else continue;
   }
   return (total+2);
}

bool corrige(ASNTag *tags, int ntags)
{
   bool ok = false;
   int tam = 0;
   int tagC = 0;
   tam = calcTam(tags, ntags);      
   for(int i = 0; i < ntags; i++)
   {
      if(tags[i].compuesta == true && tags[i].correcta == true)
         continue;
      else if(tags[i].compuesta == true && tags[i].correcta == false && i == 1)
      {
         tagC = tam - tags[i].tam;
         if(tagC == 0)
            continue;
    else tags[i].tam = (tags[i].tam + tagC);
    ok = true;
      }
      else if(tags[i].compuesta == true && i != 0 && tags[i].correcta == false)
      {
         tags[i].tam = (tags[i].tam + tagC);
         ok = true;
      }
      else continue;
   }
   return ok;
}


/*intE CAdmSeg::obtieneDatos(char *archCfg, char *ip, char *pto, char *accer, char *pathac, char *ac1, char *ack)
{
   intE error = 0;
   // / *>- ERGL mar mar 20 12:39:12 CDT 2007
   ArchivosConfg cfg;
   char Ip[100], Pto[50], Accer[150], Pathac[150], Ac1[150], Ack[150], llave[150];
   memset(Ip, 0, sizeof(Ip)); 
   memset(Pto, 0, sizeof(Pto));
   memset(Accer, 0, sizeof(Accer));
   memset(Pathac, 0, sizeof(Pathac));
   memset(Ac1, 0, sizeof(Ac1));
   memset(Ack, 0, sizeof(Ack));
   memset(llave, 0, sizeof(llave));
   error = cfg.VarConfg(7, PATHS, archCfg, Ip, Pto, Accer, Pathac, Ac1, Ack, llave);
   //->* /
   //>+ ERGL mar mar 20 12:40:23 CDT 2007
   std::string s_ip, s_pto, s_accer, s_pathac, s_ac1, s_ack, s_llave;
   CConfigFile *cfg = new CConfigFile(ARCH_CONF);
   if(cfg)
   {
      error = cfg->cargaCfgVars();
      if(error)
         return error;
     
      cfg->getValorVar("[GENERAL]", "IP",    &s_ip);
      cfg->getValorVar("[GENERAL]", "PTO",    &s_pto);
      cfg->getValorVar("[GENERAL]", "ACCER",    &s_accer);
      cfg->getValorVar("[GENERAL]", "ACPATH",    &s_pathac);
      cfg->getValorVar("[GENERAL]", "AC1",    &s_ac1);
      cfg->getValorVar("[GENERAL]", "ACK",    &s_ack);
      cfg->getValorVar("[GENERAL]", "BLLAVE",    &s_llave);
   }
   //<<< 

   //>- ERGL mar mar 20 12:43:50 CDT 2007
   //if(error != 0)
   //   return error;
   uint8 ps1[255];
   int lps1 = sizeof(ps1);
   memset(ps1, 0, sizeof(ps1));
   //>- ERGL mar mar 20 12:49:46 CDT 2007
   //desencripta((uint8*)llave, strlen(llave), ps1, &lps1);
   desencripta((uint8*)s_llave.c_str(), strlen(s_llave.c_str()), ps1, &lps1);
   
   char llave[lps1+1];
   memset(llave, 0, lps1+1);
   strncpy(llave, (char*)ps1, lps1);
   llave[lps1] = '\0';

   strcpy(bLLave, llave);
   strcpy(ip, s_ip.c_str());
   strcpy(pto, s_pto.c_str());
   strcpy(accer, s_accer.c_str());
   strcpy(pathac, s_pathac.c_str());
   strcpy(ac1, s_ac1.c_str());
   strcpy(ack, s_ack.c_str());
   / *>- ERGL mar mar 20 12:49:46 CDT 2007
   memcpy(bLLave, llave, strlen(llave));
   memcpy(ip, Ip, strlen(Ip));
   memcpy(pto, Pto, strlen(Pto));
   memcpy(accer, Accer, strlen(Accer));
   memcpy(pathac, Pathac, strlen(Pathac));
   memcpy(ac1, Ac1, strlen(Ac1));
   memcpy(ack, Ack, strlen(Ack));
   * /
   return error;
}*/

void CAdmSeg::menuAdm()
{
   system("clear");
   std::cout << std::endl << std::endl << "ADMINISTRADOR DE SERVICIOS DE SEGURIDAD" << std::endl;
   std::cout << "   1.Alta de servicio." << std::endl;
   std::cout << "   2.Baja de servicio." << std::endl;
   std::cout << "   3.Lista de servicios activos." << std::endl;
   std::cout << "   4.Salir del Administador." << std::endl << std::endl;
   std::cout << "Elija una opcion: ";
}

void CAdmSeg::ini_sskey() //Asigna memoria a los segmentos de las llaves que van a ser procesados...
{
   for (int i = 0; i < RKEYS; i++)
      privKeySS[i] = new uint8[LNG_PRIVKEY];
}

void CAdmSeg::fin_sskey() //Elimina los segmentos de la llave obtenidos y libera la memoria...
{
   for (int i = 0; i < RKEYS; i++)
   {
      if (privKeySS[i])
      {
         memset(privKeySS[i], 0, LNG_PRIVKEY);
         delete privKeySS[i];
         privKeySS[i] = NULL;
      }
   }
}

bool CAdmSeg::BajaServ()
{
   system("clear");
   bool con = false;
   //char servicio[ID_SRV];
   std::cout << std::endl << std::endl << "ADMINISTRADOR DE SERVICIOS DE SEGURIDAD" << std::endl;
   std::cout << std::endl << "En desarrollo actualmente..." << std::endl;
   return con;
}

bool CAdmSeg::leeDatosFloppy(int id, char *arch)
{
   bool ok = true;
   ManArchivo archivo;
   uint16 ldata = 0;
   if(!archivo.ProcesaArchivo(arch, (uchar*)privKeySS[id], (int*)&ldata))
   {
      ok = false;
      return ok;
   }
   else
      lprivKeySS[id] = ldata;
   return ok;
}

bool CAdmSeg::leeDatos(int id, char *path, int &error)
{
   //>>> ERGL jue jul 26 13:59:30 CDT 2007
   //bool ok = true;
   FILE *fp;
   int tamDatos = 0;
   ManArchivo archivo;
   //if(!archivo.TamArchivo(path, &tamDatos))
   //   ok = false;
   //else 
   //{
   if( archivo.TamArchivo( path, &tamDatos ) )
   {
      fp = fopen(path, "r");
      error = errno;
      if( fp )  //>+ ERGL 04072006
      {
         fread((uint8*) privKeySS[id], 1, tamDatos, fp);
         fclose(fp); 
         lprivKeySS[id] = tamDatos;
         return true;
      }
   }
   //return ok;
   return false;
   //<<< ERGL jue jul 26 14:01:26 CDT 2007
}

bool CAdmSeg::leeDatos(int id, char *path)
{
   int error = 0;
   return leeDatos(id, path, error);
}

#ifdef _DBG
bool CAdmSeg::armaPK(RSA_PRIVADA *pk)
{
   SGIRSA rsa;
   bool ok = true;
   static uint8 id;
   char pathkey[100], archivo[100], pwd[100];
   size_t escrito;
   for(id = 0; ok && (id < RKEYS); id++)
   {
      getchar();
      system("clear");
      std::cout << "\n\nADMINISTRADOR DE SERVICIOS DE SEGURIDAD" << std::endl;
      std::cout << "\nCARGA DE LLAVE PRIVADA DEL SERVICIO" << std::endl << std::endl;
      std::cout << "\nEscriba la ruta de la llave:  " << id + 1 << std::endl;
      std::cin >> archivo;
      //sprintf(pathkey, "/usr/local/SAT/Proyectos/PKI/AC/Utilerias/MemoriaCompartida/%s", archivo);
      sprintf(pathkey, "%s", archivo);
      if(!leeDatos(id, pathkey))
         ok = false;
      if(ok)
      {
         ok = false;
         for (uint8 j = 0; !ok && j < 3; j++)
         {
            strcpy(pwd, getpass("\nProporcione la contrase�a de su llave: "));
            uint8 digestion[16];
            MD5_CTX md5ctx;
            RC4_KEY rc4key;
            
            MD5_Init(&md5ctx);
            MD5_Update(&md5ctx, pwd, strlen(pwd));
            MD5_Final(digestion, &md5ctx);
            
            RC4_set_key(&rc4key, 16, digestion);
            RC4(&rc4key, lprivKeySS[id], privKeySS[id], tmp_privKeySS);
            
            if(tmp_privKeySS == NULL)
               ok = true;
// / *           
/*#ifdef DBG
            FILE *fp = NULL;
            char ag[15];
            char path[150];
            std::cout << "Archivo de salida? ";
            std::cin >> ag;
            //sprintf(path, "usr/local/SAT/Proyectos/PKI/AC/Utilerias/MemoriaCompartida/%s", ag);
            sprintf(path, "%s", ag);
            fp = fopen(path, "w");
            if(fp == NULL)
               ok = false;
            else {escrito = fwrite(tmp_privKeySS, 1, lprivKeySS[id], fp);
               fclose(fp);
            }
#endif //  */             
            memcpy(privKeySS[id], tmp_privKeySS, lprivKeySS[id]);
            ok = true;
         }
      }
   }
   if (ok)
   {
      //CSecretoCompartido* ss = new CSecretoCompartido(RKEYS, RKEYS, (R_RANDOM_STRUCT *) NULL); //>- ERGL 04072006
      CSecretoCompartido* ss = new CSecretoCompartido(RKEYS, RKEYS);
      ss->decodifica((uint8**) &privKeySS, lprivKeySS[0], (uint8*)pk);
      if(pk->bits != 1024 && pk->bits != 2048 && pk->bits != 4096 )
         ok = false;
   }
   return ok;
}
#endif
#ifdef NO_DBG 
bool CAdmSeg::armaPK(RSA_PRIVADA *pk)
{
   bool ok = true;
   //>- ERGL int err;
   //>- ERGL char tecla, 
   char pathkey[100], archivo[100], pwd[100];
   memset(pathkey, 0, sizeof(pathkey));
   memset(archivo, 0, sizeof(archivo));
   memset(pwd, 0, sizeof(pwd));   
   for(uint8 i = 0; ok && i < RKEYS; i++)
   {
      bool cargafloppy = false;
      int opt = 0;
      int nagente = 0;
      system("clear");
      do{
         opt++;
         system("clear");
         std::cout << "\n\nADMINISTRADOR DE SERVICIOS DE SEGURIDAD" << std::endl;
         std::cout << "\nCARGA DE LLAVE PRIVADA DEL SERVICIO" << std::endl << std::endl;
         std::cout << "\nInserte el disco del agente presente " << i + 1 << std::endl;
         std::cout << " Oprima cualquier tecla cuando este listo...";
         getchar();
         if(mount("/dev/fd0", "/mnt/floppy", "msdos", MS_MGC_VAL | MS_RDONLY, NULL) == -1)
         {
            int error = errno;
            if (error)
            {
               std::cout << "\n\nError al tratar de montar el sistema de archivos correspondiente al floppy (" 
                         << error << ")\n";
               std::cout << "\t" << strerror(error) << "\n\n";
               std::cout <<"Intento de cargado: " << opt << " presione cualquier tecla para el siguiente intento...";
               getchar();
            }
         }
         else cargafloppy = true;
         if(opt == 3)
         {
            std::cout << "\nNo se puede leer el disco del agente intente con otro disco..." << std::endl;
            opt = 0;
            nagente++;
            getchar();
         }
         if(nagente == 5)
         {
            system("clear");
            std::cout << std::endl << "ADMINISTRADOR DE SERVICIOS DE SEGURIDAD" << std::endl;
            std::cout << "FALLO GENERAL. Ninguno de los discos de los agentes se logro cargar." << std::endl;
            std::cout << "Fin de la aplicacion..." << std::endl;
            sleep(3);
            return false;
         }
      }while(!cargafloppy);
      system("clear");
      std::cout << std::endl << "ADMINISTRADOR DE SERVICIOS DE SEGURIDAD" << std::endl;
      do{
         std::cout << "\nEscriba el nombre de la llave privada: ";
         std::cin.getline(archivo, sizeof(archivo));
         if(archivo[0] == '\0')
         std::cout << "\nNo se capturo ning�n nombre de archivo... ";
      }while(archivo[0] == '\0');
      sprintf(pathkey, "/mnt/floppy/%s", archivo);
      if(!leeDatos(i, pathkey))
      {
         std::cout << "\nError al leer datos del disco..." << std::endl;         
         ok = false;
      }         
      if(ok)
      {
         ok = false;
         for (uint8 j = 0; !ok && j < 3; j++)
         {
            strcpy(pwd, getpass("\nProporcione la contrase�a de su llave: "));
            uint8 dig[16];
            MD5_CTX md5ctx;
            RC4_KEY rc4key;
            MD5_Init(&md5ctx);
            MD5_Update(&md5ctx, pwd, strlen(pwd));
            MD5_Final(dig, &md5ctx);
            RC4_set_key(&rc4key, 16, dig);
            RC4(&rc4key, lprivKeySS[i], privKeySS[i], tmp_privKeySS);
            if (tmp_privKeySS[0] == 0x30)
            {
               uint8  cars_lng;
               uint16 lng_datos;

               if (tmp_privKeySS[1] < 0x80)
               {
                  cars_lng  = 1;
                  lng_datos = tmp_privKeySS[1];
               }
               else if (tmp_privKeySS[1] == 0x81)
               {
                  cars_lng  = 2;
                  lng_datos = tmp_privKeySS[2];
               }
               else if (tmp_privKeySS[1] == 0x82)
               {
                  cars_lng  = 3;
                  lng_datos = (uint16(tmp_privKeySS[2]) << 8) | uint16(tmp_privKeySS[3]);
               }
               else
                  continue;
               memcpy(privKeySS[i], tmp_privKeySS + cars_lng + 1, lprivKeySS[i] - cars_lng -1);
               ok = true;
            }
         }
      }
      umount("/mnt/floppy");
   }  
   if (ok)
   {
      RSA *privKey = NULL;
      EVP_PKEY *privateKey = NULL;
      BIO *bio = NULL;
      uint8 Salida[10 *1024];
      uchar clave[8];
      memset(Salida, 0, sizeof(Salida));
      memset(clave, 0, sizeof(clave));
      //CSecretoCompartido* ss = new CSecretoCompartido(RKEYS, RKEYS, (R_RANDOM_STRUCT *) NULL); //>- ERGL 04072006
      CSecretoCompartido* ss = new CSecretoCompartido(RKEYS, RKEYS);
      ss->decodifica((uint8**) &privKeySS, lprivKeySS[0], Salida);
      ASNTag tagS[10];
      int ntagS = 0;
      //>- ERGL int cont;
      //>- ERGL int tamC = 0, tamC2 = 0;
      leeTags(Salida+8, 1261, tagS, &ntagS);
      if(!corrige(tagS, ntagS))
         std::cout << "\nNo es posible que regrese error aqui, no mouse..." << std::endl;
      for(int c = 0; c < ntagS-1; c++)
      {
         if(tagS[c].correcta == false)
         {
            if(Salida[tagS[c].posArray+1+8] != tagS[c].tam)
               Salida[tagS[c].posArray+1+8] = tagS[c].tam;
            else continue;
         }
         else continue;
      }
      memcpy(clave, Salida, 8);
      uchar tagTam[3];
      int itamTag = 0;
      memset(tagTam, 0, sizeof(tagTam));
      if(Salida[8+1] == 0x82)
      {
         memcpy(tagTam, Salida + 8+2, 2);
         itamTag = (int)(CalcTagLen(tagTam, 2)+4);
      }
      bio = BIO_new(BIO_s_mem());
      BIO_write(bio, Salida+8, 4096);
      memset(Salida, 0, sizeof(Salida));
      privateKey = EVP_PKEY_new();
      if(privateKey == NULL)
         ok = false;
      memset(Salida, 0, sizeof(Salida));
      if(!d2i_PKCS8PrivateKey_bio(bio, &privateKey, NULL, clave))
      {
         ok = false;
         unsigned long opErr = ERR_get_error();
         ERR_load_crypto_strings();
         char strErr[256];
         memset(strErr, 0, sizeof(strErr));
         ERR_error_string(opErr, strErr);
         ERR_free_strings();
      }
      else
      {
         ok = false;
         privKey = EVP_PKEY_get1_RSA(privateKey);
         intE error = setprivada(privKey, pk);
         if(error != 0)
         {
            std::cout << "\nOcurrio el error: " << error << " al abrir la llave privada..." << std::endl;
            ok = false;
         }
         RSA_free(privKey);
         ok = true;
      }  
   }
   return ok;
}
#endif //DBG
//////////////////////////////////////////////////////////////////////////////////
//const char* CAdmSeg::dev[2][2] = { {"/dev/fd0" , "/media/floppy"},
//                                        {"/dev/sdb1", "/media/usb"   }}; //>>- ERGL mar jul 24 13:33:11 CDT 2007
bool CAdmSeg::montaDispositivo()
{
   int intentos = 0;
   for (intentos = 0; intentos < 3; intentos++)
   {
      char resp; 
      std::cout << "\nDispositivo donde se encuentra el segmento (Usb, Floppy, Disco duro):" << std::endl;
      std::cin >> resp;
      resp = toupper(resp); 
      //if (resp == 'U' || resp == 'F') //ERGL mar jul 24 16:39:23 CDT 2007
      //{
         disp = resp == 'U' ? 1 : (resp == 'F' ? 0 : 2);
         if ( disp == 1 || disp == 0 ) 
         {
            std::cout << "\nInserte el dispositivo del agente presente. Intento: " << intentos + 1 << std::endl;
            std::cout << "\tOprima cualquier tecla cuando este listo..." << std::endl;
            std::cin >> resp;
         }
         else
            return true;

         if ( (mount(dev[disp][0], dev[disp][1], "msdos", MS_RDONLY | MS_NOEXEC, NULL)) == -1 )
         {
            int error = errno;
            system("clear");
            std::cout << "\n\nError al tratar de montar el dispositivo errno(" << error << ")\n";
            std::cout << "\t" << strerror(error) << "\n\n";
            std::cout <<"Intento de cargado: " << intentos+1 << " presione cualquier tecla para el siguiente intento...";
            std::cin >> resp;
            continue;
         }
         else
            return true;
      //}
   }
   return false;
}
//////////////////////////////////////////////////////////////////////////////////
bool CAdmSeg::desmontarDispositivo()
{
   if( disp == 0 || disp == 1 )
   {
      if( (umount(dev[disp][0])) != -1 )
         return true;
      else
      {
         int error = errno;
         std::cout << "Ha ocurrido un error al desmontar la unidad " <<  dev[disp][0] << " : " << strerror(error) << std::endl;
      }
   }
   else if ( disp == 2 )
      return true;
   return false;   
}
//////////////////////////////////////////////////////////////////////////////////
bool CAdmSeg::cargaSegmento(uint8 ns)
{
   //int disp = 0, intentos = 0, i;
   system("clear");
   std::cout << "\n\nADMINISTRADOR DE SERVICIOS DE SEGURIDAD" << std::endl;
   std::cout << "\nCARGA DE LLAVE PRIVADA DEL SERVICIO\n" << std::endl;
   std::cout << "\nCarga segmento agente " << ns + 1 << std::endl;

   if( montaDispositivo() )
   {
      if( cargaArchivo(ns) )
      {
         if( desencriptaSegmento(ns) && desmontarDispositivo() )
               return true;
      }
      else
         desmontarDispositivo();
   }
   return false; 
}
//////////////////////////////////////////////////////////////////////////////////
bool CAdmSeg::armaPK(RSA_PRIVADA *pk)
{
   bool edo = true;
   for( uint8 i = 0; i < RKEYS; i++ )
   {
      if ( !cargaSegmento(i) )
      {
         edo = false;
         break;
      }
   }
   if( edo )
   {
      if ( armaSegmentos(pk) )
         return true;
   }
   return false;
}
//////////////////////////////////////////////////////////////////////////////////
//ERGL mar jul 24 16:42:50 CDT 2007
bool CAdmSeg::cargaArchivo(uint8 id)
{
   int intentos  = 3;
   int eError    = 0;
   char *archivo = new char[256];
   char *ruta_cm = new char[256];
   memset(ruta_cm, 0, 256);
   memset(archivo, 0, 256);
   for(int i=0; i < intentos; i++)
   {
      if ( i > 0 )
         system( "clear" );
      std::cout << "Escriba el nombre del archivo: "  << std::endl;
      std::cin >> archivo;
      if(archivo[0] != '\000')
      {
         if( disp == 1 || disp == 0 )
            sprintf( ruta_cm, "%s%s", dev[disp][1], archivo );
         else
            strncpy( ruta_cm, archivo, strlen(archivo) );
         if( leeDatos( id, ruta_cm, eError ) )
         {
            free( archivo ); archivo = NULL;
            free( ruta_cm ); ruta_cm = NULL;
            return true;
         }
         else
         {
            system("clear");
            std::cout << "Ocurrio un error al intentar leer el archivo." << "("<<eError<<")" << std::endl;
            std::cout << strerror(eError) << std::endl;
            char resp = 'T';
            while( resp != 'S' && resp != 'N' && (i != (intentos - 1)) )
            { 
               std::cout << "Desea reintentar (S/N)" << std::endl;
               std::cin >> resp;
               resp = toupper( resp );
            }
            if( resp == 'N' )
               break;
         }
      }
      else
      {
         std::cout << "�No se ha especificado un nombre de archivo!" << std::endl;
      }
      memset(ruta_cm, 0, 256);
   }
   free( archivo ); archivo = NULL;
   free( ruta_cm ); ruta_cm = NULL;
   return false;
}
//////////////////////////////////////////////////////////////////////////////////
bool CAdmSeg::desencriptaSegmento(uint8 id)
{
   int l_pass = 256;
   char *pass = (char *)malloc(l_pass);
   strcpy(pass, getpass("\nProporciene la contrase�a de su llave: "));
   uint8 digestion[16];
  
   MD5_CTX md5ctx;
   RC4_KEY rc4key; 
  
   MD5_Init(&md5ctx);
   MD5_Update(&md5ctx, pass, strlen(pass));
   MD5_Final(digestion, &md5ctx);

   RC4_set_key(&rc4key, 16, digestion);
   RC4(&rc4key, lprivKeySS[id], privKeySS[id], tmp_privKeySS); 
   
   if( tmp_privKeySS[0] == 0x30 )
   {
      uint8  cars_lng;
      uint16 lng_datos;
      uint8  val_pkss = (uint8)tmp_privKeySS[1];
      switch ( val_pkss )
      {
         case 128 :
            cars_lng = 1;
            lng_datos = tmp_privKeySS[1];
            break;
         case 129 :
            cars_lng = 2;
            lng_datos = tmp_privKeySS[1];
            break;
         case 130 :
            cars_lng = 3;
            lng_datos = (uint16)((tmp_privKeySS[1] << 8) | tmp_privKeySS[3]);
            break;
         default :
            return false;
      }
      memcpy(privKeySS[id], tmp_privKeySS + cars_lng + 1, lprivKeySS[id] - cars_lng - 1);

      /**/
      MD5_Init(&md5ctx);
      MD5_Update(&md5ctx, privKeySS[id], (lprivKeySS[id] - cars_lng - 1));
      MD5_Final(digestion, &md5ctx);
      for( int i = 0; i< 16; i++ )
         printf("%X", digestion[i]);
      printf("\n");
      /**/

 
      return true;
   }

   return false;
}
//////////////////////////////////////////////////////////////////////////////////
bool CAdmSeg::armaSegmentos(RSA_PRIVADA *pk)
{
   uint8 *salida = (uint8 *)calloc((24*1024), sizeof(uint8));
   uchar *clave = (uchar *)calloc(8, sizeof(uchar));
   uchar *tagTam = (uchar *)calloc(4, sizeof(tagTam));
   ASNTag *tagS = (ASNTag *)calloc(10, sizeof(ASNTag));
   //ASNTag tagS[10];
   int itamTag;
   int ntagS = 0;

   CSecretoCompartido *ss = new CSecretoCompartido(RKEYS, RKEYS);

   if( ss->decodifica((uint8**) &privKeySS, lprivKeySS[0], salida) )
   {
      leeTags(salida+8, 1261, tagS, &ntagS);
      corrige(tagS, ntagS);

      for( int c = 0; c < ntagS-1; c++ )
      {
         if( tagS[c].correcta == false )
         {
            if( salida[tagS[c].posArray+1+8] != tagS[c].tam )
               salida[tagS[c].posArray+1+8] = tagS[c].tam;
            else
            continue; 
         }
         else
            continue;
      }
      memcpy(clave, salida, 8);
   
      if( salida[8+1] == 0x82 )
      {
         memcpy( tagTam, salida+8, 2 );
         itamTag = (int)(CalcTagLen(tagTam, 2)+4);
      }
   
      BIO *bio = BIO_new(BIO_s_mem());
      if( bio )
      {
         BIO_write(bio, salida+8, 4096);
         //free(salida); salida = NULL;
         EVP_PKEY *llavePrivada = EVP_PKEY_new();
         if( llavePrivada )
         {
            if( !d2i_PKCS8PrivateKey_bio( bio, &llavePrivada, NULL, clave ) )
            {
               ERR_load_crypto_strings();
               char *error = new char[256];
               ERR_error_string(ERR_get_error(), error);
               std::cout << "Error al generar la Llave Privada: " << error << std::endl;
               ERR_free_strings();
               free(error); error = NULL;
            }
            else
            {
               RSA *privKey = EVP_PKEY_get1_RSA(llavePrivada);
               intE error = setprivada(privKey, pk);
               RSA_free(privKey); 
               if( error )
               {
                  std::cout << "\nOcurrio el error: " << error << " al abrir la llave privada..." << std::endl;
                  return false;
               }
               return true;
            }
         }
         EVP_PKEY_free(llavePrivada);
         BIO_free(bio);
      }
   }
   std::cout << "No se han podido decodificar las llaves participantes" << std::endl;
   return false;
}
//////////////////////////////////////////////////////////////////////////////////
#ifdef NO_DBG
bool CAdmSeg::armaPK(RSA_PRIVADA *pk)
{
   bool ok = true;
   char pathkey[100], archivo[100], pwd[100];

   memset(pathkey, 0, sizeof(pathkey));
   memset(archivo, 0, sizeof(archivo));
   memset(pwd    , 0, sizeof(pwd));   

   for(uint8 i = 0; ok && i < RKEYS; i++)
   {
      bool cargafloppy = false;
      int opt = 0;
      int nagente = 0;
      system("clear");
      do{
         opt++;
         system("clear");
         std::cout << "\n\nADMINISTRADOR DE SERVICIOS DE SEGURIDAD" << std::endl;
         std::cout << "\nCARGA DE LLAVE PRIVADA DEL SERVICIO" << std::endl << std::endl;
         std::cout << "\nInserte el disco del agente presente " << i + 1 << std::endl;
         std::cout << " Oprima cualquier tecla cuando este listo...";
         getchar();
         if(mount("/dev/fd0", "/mnt/floppy", "msdos", MS_MGC_VAL | MS_RDONLY, NULL) == -1)
         {
            int error = errno;
            if (error)
            {
               std::cout << "\n\nError al tratar de montar el sistema de archivos correspondiente al floppy (" 
                         << error << ")\n";
               std::cout << "\t" << strerror(error) << "\n\n";
               std::cout <<"Intento de cargado: " << opt << " presione cualquier tecla para el siguiente intento...";
               getchar();
            }
         }
         else cargafloppy = true;
         if(opt == 3)
         {
            std::cout << "\nNo se puede leer el disco del agente intente con otro disco..." << std::endl;
            opt = 0;
            nagente++;
            getchar();
         }
         if(nagente == 5)
         {
            system("clear");
            std::cout << std::endl << "ADMINISTRADOR DE SERVICIOS DE SEGURIDAD" << std::endl;
            std::cout << "FALLO GENERAL. Ninguno de los discos de los agentes se logro cargar." << std::endl;
            std::cout << "Fin de la aplicacion..." << std::endl;
            sleep(3);
            return false;
         }
      }while(!cargafloppy);
      system("clear");
      std::cout << std::endl << "ADMINISTRADOR DE SERVICIOS DE SEGURIDAD" << std::endl;
      do{
         std::cout << "\nEscriba el nombre de la llave privada: ";
         std::cin.getline(archivo, sizeof(archivo));
         if(archivo[0] == '\0')
         std::cout << "\nNo se capturo ning�n nombre de archivo... ";
      }while(archivo[0] == '\0');
      sprintf(pathkey, "/mnt/floppy/%s", archivo);
      if(!leeDatos(i, pathkey))
      {
         std::cout << "\nError al leer datos del disco..." << std::endl;         
         ok = false;
      }         
      if(ok)
      {
         ok = false;
         for (uint8 j = 0; !ok && j < 3; j++)
         {
            strcpy(pwd, getpass("\nProporcione la contrase�a de su llave: "));
            uint8 dig[16];
            MD5_CTX md5ctx;
            RC4_KEY rc4key;
            MD5_Init(&md5ctx);
            MD5_Update(&md5ctx, pwd, strlen(pwd));
            MD5_Final(dig, &md5ctx);
            RC4_set_key(&rc4key, 16, dig);
            RC4(&rc4key, lprivKeySS[i], privKeySS[i], tmp_privKeySS);
            if (tmp_privKeySS[0] == 0x30)
            {
               uint8  cars_lng;
               uint16 lng_datos;

               if (tmp_privKeySS[1] < 0x80)
               {
                  cars_lng  = 1;
                  lng_datos = tmp_privKeySS[1];
               }
               else if (tmp_privKeySS[1] == 0x81)
               {
                  cars_lng  = 2;
                  lng_datos = tmp_privKeySS[2];
               }
               else if (tmp_privKeySS[1] == 0x82)
               {
                  cars_lng  = 3;
                  lng_datos = (uint16(tmp_privKeySS[2]) << 8) | uint16(tmp_privKeySS[3]);
               }
               else
                  continue;
               memcpy(privKeySS[i], tmp_privKeySS + cars_lng + 1, lprivKeySS[i] - cars_lng -1);
               ok = true;
            }
         }
      }
      umount("/mnt/floppy");
   }  
   if (ok)
   {
      RSA *privKey = NULL;
      EVP_PKEY *privateKey = NULL;
      BIO *bio = NULL;
      uint8 Salida[10 *1024];
      uchar clave[8];
      memset(Salida, 0, sizeof(Salida));
      memset(clave, 0, sizeof(clave));
      //CSecretoCompartido* ss = new CSecretoCompartido(RKEYS, RKEYS, (R_RANDOM_STRUCT *) NULL); //>- ERGL 04072006
      CSecretoCompartido* ss = new CSecretoCompartido(RKEYS, RKEYS);
      ss->decodifica((uint8**) &privKeySS, lprivKeySS[0], Salida);
      ASNTag tagS[10];
      int ntagS = 0;
      //>- ERGL int cont;
      //>- ERGL int tamC = 0, tamC2 = 0;
      leeTags(Salida+8, 1261, tagS, &ntagS);
      if(!corrige(tagS, ntagS))
         std::cout << "\nNo es posible que regrese error aqui, no mouse..." << std::endl;
      for(int c = 0; c < ntagS-1; c++)
      {
         if(tagS[c].correcta == false)
         {
            if(Salida[tagS[c].posArray+1+8] != tagS[c].tam)
               Salida[tagS[c].posArray+1+8] = tagS[c].tam;
            else continue;
         }
         else continue;
      }
      memcpy(clave, Salida, 8);
      uchar tagTam[3];
      int itamTag = 0;
      memset(tagTam, 0, sizeof(tagTam));
      if(Salida[8+1] == 0x82)
      {
         memcpy(tagTam, Salida + 8+2, 2);
         itamTag = (int)(CalcTagLen(tagTam, 2)+4);
      }
      bio = BIO_new(BIO_s_mem());
      BIO_write(bio, Salida+8, 4096);
      memset(Salida, 0, sizeof(Salida));
      privateKey = EVP_PKEY_new();
      if(privateKey == NULL)
         ok = false;
      memset(Salida, 0, sizeof(Salida));
      if(!d2i_PKCS8PrivateKey_bio(bio, &privateKey, NULL, clave))
      {
         ok = false;
         unsigned long opErr = ERR_get_error();
         ERR_load_crypto_strings();
         char strErr[256];
         memset(strErr, 0, sizeof(strErr));
         ERR_error_string(opErr, strErr);
         ERR_free_strings();
      }
      else
      {
         ok = false;
         privKey = EVP_PKEY_get1_RSA(privateKey);
         intE error = setprivada(privKey, pk);
         if(error != 0)
         {
            std::cout << "\nOcurrio el error: " << error << " al abrir la llave privada..." << std::endl;
            ok = false;
         }
         RSA_free(privKey);
         ok = true;
      }  
   }
   return ok;
}
#endif //NO_DBG

void CAdmSeg::ListaServ()
{
   std::cout << "\nADMINISTRADOR DE SERVICIOS DE SEGURIDAD" << std::endl;
   std::cout << std::endl << std::endl << "En desarrollo actualmente." << std::endl;
   //std::cout << "\n\nLISTA DE SERVICIOS DE SEGURIDAD ACTIVOS" << std::endl << std::endl;
   
   /*for(uint8 i = 0; i < napl; i ++)
   {
      if (mem->getServInfo(i).clave[0])
         std::cout << mem->getServInfo(i).clave << std::endl;
   }*/
}

void CAdmSeg::termina()
{
   std::cout << std::endl << "Hasta luego..." << std::endl;
   exit(0);
}

bool CAdmSeg::getCertInfo(ServSegInfo* pk, char* certificado, struct rsa_st* pubKey)
{
   //static char ruta[128];
   X509 *x509;
   uchar *buffer;
   int lbuffer = 0;
   ManArchivo archivo;
   if(!archivo.TamArchivo(certificado, &lbuffer))
   {
      std::cout << "\nNo existe el archivo de certificado." << std::endl;
      return false;
   }   
   buffer = new uchar[lbuffer];
   if(!archivo.ProcesaArchivo(certificado, buffer, &lbuffer))
   {
      std::cout << "\nError: No se extrajeron los datos del certificado." << std::endl;
      return false;
   }
   x509 = X509_new();
   if(x509 == NULL)
   {
      std::cout << "\nError: No se aparto memoria para el certificado." << std::endl;
      return false;
   }
   if(!d2i_X509(&x509, (const uchar**)&buffer, lbuffer))
   {
      std::cout << "\n\nError: no se pudo decodificar el certificado\n";
      return false;
   }

   ASN1_INTEGER* serial = X509_get_serialNumber(x509);
   if (serial->length > NUM_SERIE)
   {
      std::cout << "\n\nError: numero de serie demasiado grande\n";
      return false;
   }
   memcpy(pk->numSerie, serial->data, serial->length);
   pk->numSerie[serial->length] = 0;

   ASN1_TIME *vigINI =  X509_get_notBefore(x509);
   ASN1_TIME *vigFIN =  X509_get_notAfter(x509);

   memcpy(&pk->vigIni, vigINI->data, vigINI->length);
   memcpy(&pk->vigFin, vigFIN->data, vigFIN->length);
   
   memcpy(pubKey, X509_get_pubkey(x509)->pkey.rsa, sizeof(struct rsa_st));

   X509_free(x509);
   return true;
}
intE CAdmSeg::setprivada(RSA *privada, RSA_PRIVADA *privateK)
{
   int res;
   privateK->bits = (size_t)BN_num_bits(privada->n);
   if(privateK->bits <= 0) return sgiError(0, 0, ERR_NO_CARGA_BITS);
   res = BN_bn2bin(privada->n, (uchar*)privateK->moduloP);
   if(res <= 0) return sgiError(0, 0, ERR_NO_MODULO);
   res = BN_bn2bin(privada->e, (uchar*)privateK->exponenteP);
   if(res <= 0) return sgiError(0, 0, ERR_NO_EXP_PUB);
   res = BN_bn2bin(privada->d, (uchar*)privateK->exponentePv);
   if(res <= 0) return sgiError(0, 0, ERR_NO_EXP_PRIV);
   res = BN_bn2bin(privada->p, (uchar*)privateK->primo[0]);
   if(res <= 0) return sgiError(0, 0, ERR_NO_PRIMO);
   res = BN_bn2bin(privada->q, (uchar*)privateK->primo[1]);
   if(res <= 0) return sgiError(0, 0, ERR_NO_PRIMO2);
   res = BN_bn2bin(privada->dmp1, (uchar*)privateK->primoExp[0]);
   if(res <= 0) return sgiError(0, 0, ERR_PRIME_EXP);
   res = BN_bn2bin(privada->dmq1, (uchar*)privateK->primoExp[1]);
   if(res <= 0) return sgiError(0, 0, ERR_PRIME_EXP2);
   res = BN_bn2bin(privada->iqmp, (uchar*)privateK->coeficiente);
   if(res <= 0) return sgiError(0, 0, ERR_NO_COEF);
   return 0;
}

bool CAdmSeg::CargaServ()
{
   bool ok = true;
   system("clear"); 
   char servicio[ID_SRV];
   char pathcert[280];
   std::cout << std::endl << "ADMINISTRADOR DE SERVICIOS DE SEGURIDAD" << std::endl;
   std::cout << "\nTeclee el servicio de desee iniciar: " << std::endl;
   std::cin >> (servicio);
   std::cout << "Ha elegido el servicio: " << servicio << std::endl;
   std::cout << "\nTeclee la ruta del certificado: " << std::endl;
   std::cin >> (pathcert);
   srv = new ServSegInfo;
   memset(srv, 0, sizeof(struct serv_info_st));
   memcpy(srv->clave, servicio, ID_SRV);
   if(getCertInfo(srv, pathcert, &publica))
   {
      int cv;
      std::cout << "\n1. Llave PKCS8." << std::endl;
      std::cout << "2. Llave de Secreto Compartido." << std::endl;
      std::cout << "\nEscriba la opci�n que desea: " << std::endl;
      std::cin >> cv;
      //getchar();
      if(cv == 2)
      {
         ini_sskey();
         if(ok=armaPK(&privada))
         {
            RSA *priv = NULL;
            priv = RSA_new();
            if(priv == NULL)
            {
               std::cout << "\nError irrecuperable, terminando..." << std::endl;
               exit(-1);
            }
            SGIRSA rsa;
            SGIPRIVADA *prv = new SGIPRIVADA;
            memcpy(prv, &privada, sizeof(RSA_PRIVADA));
            intE errorA = rsa.getPrivada(prv, priv);
            if(errorA != 0)
            {
               std::cout << "\nOcurri� el error: " << sgiErrorBase(errorA) << " al obtener la llave privada." << std::endl;
               ok = false;          
            }
            else if(BN_cmp(priv->n, publica.n) && BN_cmp(priv->e, publica.e) != 0)
            {
               std::cout << "\nLa llave no coincide con el certificado..." << std::endl;
               return ok = false;
            }
            else
            {
               memcpy(srv->privKey, &privada, sizeof(struct sgi_privateRSA_st));
               error = sktAC->Envia((uint8*)srv, sizeof(struct serv_info_st));
               if(error != 0)
                  ok = false;
               fin_sskey();
               delete prv;
               ok = true;
            }
         }
      }
      if(cv == 1)
      {   
         SGIRSA rsa;
         RSA *priv;
         priv = RSA_new();
         intE error;
         char pw[25], ruta[250];
         system("clear");
         std::cout << "\n\nADMINISTRADOR DE SERVICIOS DE SEGURIDAD" << std::endl;
         std::cout << "\nCARGA DE LLAVE PRIVADA DEL SERVICIO" << std::endl << std::endl;
         std::cout << "\nEscriba la ruta de la llave:  " << std::endl;
         std::cin  >> ruta;
         strcpy( pw, getpass("\n\nEscriba el password de su llave privada: ") );
         getchar();
         error = rsa.DecodPvKey(ruta, pw, &priv);
         if(error != 0)
         {
            std::cout << "\nOcurrio el error: " << error << " al abrir la llave privada..." << std::endl;
            ok = false;
         }
         if(BN_cmp(priv->n, publica.n) && BN_cmp(priv->e, publica.e) != 0)
         {
            std::cout << "\nLa llave no coincide con el certificado..." << std::endl;
            return ok = false;
         }
         error = setprivada(priv, &privada);
         if(error != 0)
         {
            std::cout << "\nOcurrio el error: " << error << " al abrir la llave privada..." << std::endl;
            ok = false;
         }
         else
            memcpy(srv->privKey, &privada, sizeof(RSA_PRIVADA));
         error = sktAC->Envia((uint8*)srv, sizeof(struct serv_info_st));
         if(error != 0)
            ok = false;
         int lMemServ = sizeof(MemServ);
         error = sktAC->Recibe((uint8*)MemServ, lMemServ);
         if(error != 0)
            ok = false;
         std::cout << MemServ << std::endl;
         RSA_free(priv);                      
      }
   }
   else 
   {
      std::cout << "\nOcurri� un error al levantar el servicio...";
      return ok = false;
   }
   delete srv;
   srv = NULL;
   system("clear");
   if(!ok)
      std::cout << "\nOcurri� un error al levantar el servicio...";
   else std::cout << "\nSe inicio el servicio con exito..." << std::endl;
   return ok;
}

bool CAdmSeg::otraOp()
{
   char opc;
   std::cout << std::endl << "Desea realizar otra operacion?(y,n)";
   std::cin >> opc;
   if(opc == 'y')
      return true;
   else return false;
}

/*intE CAdmSeg::conecta(char *archConfg)
{
   error = 0;
   char Ip[100], Pto[50], Accer[150], Pathac[150], Ac1[150], Ack[150];
   memset(Ip, 0, sizeof(Ip));
   memset(Pto, 0, sizeof(Pto));
   memset(Accer, 0, sizeof(Accer));
   memset(Pathac, 0, sizeof(Pathac));
   memset(Ac1, 0, sizeof(Ac1));
   memset(Ack, 0, sizeof(Ack));
   //char ar[] = "/usr/local/SAT/PKI/etc/Comunicacion.cfg";
   obtieneDatos(archConfg,Ip,Pto,Accer,Pathac,Ac1,Ack);
   int pto = atoi(Pto);
   paramAC.SetCliente(ESSL_entero,0,Ip,pto,Accer,Pathac,Ac1,Ack,verify_callback,password_cb);
   sslAC.Inicia();
   error = sslAC.Cliente(paramAC,sktAC);
   if(error != 0)
      return error;
   return error;
}
*/

intE CAdmSeg::conecta()
{
   int         r1  , r2   , r3     , r4      , r5   , r6   , r7  ;
   std::string s_ip, s_pto, s_accer, s_pathac, s_ac1, s_ack, pSSL;
   int  size = SIZEBLLAVE;

   CConfigFile *cfg = new CConfigFile(ARCH_CONF);
   
   r1 = cfg->cargaCfgVars();
   if(!cfg)
   {
      std::cout << "\nOcurrio el error: " << r1 << " al leer el archivo de configuraci�n" << std::endl;
      return error;
   }
   r1 = cfg->getValorVar("[GENERAL]", "IP",       &s_ip    );
   r2 = cfg->getValorVar("[GENERAL]", "PTO",      &s_pto   );
   r3 = cfg->getValorVar("[GENERAL]", "ACCER",    &s_accer );
   r4 = cfg->getValorVar("[GENERAL]", "ACPATH",   &s_pathac);
   r5 = cfg->getValorVar("[GENERAL]", "AC1",      &s_ac1   );
   r6 = cfg->getValorVar("[GENERAL]", "ACK",      &s_ack   );
   r7 = cfg->getValorVar("[GENERAL]", "BLLAVE",   &pSSL    );

   if (r1 || r2 || r3 || r4 || r5 || r6 || r7)
   {
      std::cout << "\nError al leer los par�metros de configuraci�n: "<< r1 << ", " << r2 << ", " << r3
                << ", " << r4 << ", " << r5 << ", " << r6 << ", " << r7 << std::endl;
      return -1;
   }
   else
   {
      #ifdef DBG
          std::cout << "\nPar�metros de configuraci�n: " << s_ip.c_str() << ", " << s_pto.c_str() << ", " <<
                 s_accer.c_str() << ", " << s_pathac.c_str() << ", " << s_ac1.c_str() << ", " <<
                 s_ack.c_str();
      #endif
   }
   int pto = atoi( s_pto.c_str() );
   if ((unsigned int)size < pSSL.size())
   {
      std::cout << "\nError al asignar el password de SSL, buffer menor del requerido: " <<
                   size << ", " << pSSL.size();
      return -2;
   }
   if (!desencripta((uint8*) pSSL.c_str(), pSSL.size(), (uint8*) bLLave, &size))
   {
      std::cout << "\nError al desencriptar el password del archivo de configuraci�n" ;
      return -3;
   }
   paramAC.SetCliente(ESSL_entero, 0, s_ip.c_str() , pto          , s_accer.c_str(), s_pathac.c_str(),
                                      s_ac1.c_str(), s_ack.c_str(), verify_callback, password_cb);
   sslAC.Inicia();
   error = sslAC.Cliente(paramAC,sktAC);
   return error;
}

CAdmSeg::CAdmSeg() :
   disp(-1)
{
   memset(MemServ, 0, sizeof(MemServ));
   memset(privKeySS, 0, sizeof(privKeySS));
   memset(&privada, 0, sizeof(RSA_PRIVADA));
   dev[0][0] = "/dev/fd0";
   dev[0][1] = "/media/floppy";
   dev[1][0] = "/dev/sdb1";
   dev[1][1] = "/media/usb";
}


CAdmSeg::~CAdmSeg()
{
}
/* --------------------------------------------------------------------------- */
/* --------------------------------------------------------------------------- */
int main(int argc /*, char *argv[]*/)
{
   if (argc > 1)
   {
      std::cout << "Uso: Ejecutable" /* ArchivoConfiguracion"*/ << std::endl;
      std::cout << "Ejecutable: Nombre del archivo ejecutable." << std::endl;
      //std::cout << "ArchivoConfiguracion: Ruta y nombre del archivo de configuraci�n de la aplicaci�n." << std::endl;
      return -1;
   }

   iniciaLibOpenSSL();
   intE err = 0;
   uint8 opc;
   bool con = true;
   //bool ok = true;
   CAdmSeg adm;
   // err = adm.conecta(argv[1]);
   err = adm.conecta();
   if(err != 0)
   {
      std::cout << "\nOcurrio el error: "<< err <<", en la conexi�n. " << std::endl;
      return -2;
   }

   do{
      adm.menuAdm();
      opc = std::cin.get();
      switch (opc)
      {
         case '1': {adm.CargaServ(); 
            con = adm.otraOp();
            break;}
         case '2': {adm.BajaServ(); 
            con = adm.otraOp();
            break;}
         case '3': {adm.ListaServ(); 
            con = adm.otraOp();
            break;}
         case '4':{
            adm.termina();
            con = false;
            break;}
      }
   }while(con);
   std::cout << std::endl << "Hasta luego..." << std::endl;
   sleep(2);
   terminaLibOpenSSL();
   return 0;
}
