static const char* _ADMSRVSEGURIDAD_CPP_VERSION_ ATR_USED = "AdmSrvSeguridad @(#)"\
"DSIC07412AC_ 2007-12-14 AdmSrvSeguridad.cpp 1.1.0/0";

//#VERSION: 1.1.0
/*******************************************
 * Administrador de Servicios de Seguridad *
 * Febrero 2004.                           *
 *******************************************/

#include <SgiTipos.h>
#include <unistd.h>
#include <memory.h>
#include <curses.h>
#include <errno.h>
#include <sys/mount.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <fstream>

using namespace std;
using std::fstream;

//#include <SgiOpenSSL.h> //-GHM(070424)
//- #include <sglib.h> //- GHM: Eliminar los datos de Seguridata
/* -GHM(070424) 
#include "ShMemPKI.h"
#include "SecretoCompartido.h"
#include "Principal.h"*/ //GHM(070424)

#include <Sgi_OpenSSL.h>
#include <Sgi_MemCompartida.h>
#include <Sgi_SecretoCompartido.h>
#include <AdmSrvSeguridad.h>

extern "C"{
#include <openssl/rc4.h>
#include <openssl/md5.h>
}

//#define DBG

void menuAdm()
{
   system("clear");
   cout << endl << endl << "ADMINISTRADOR DE SERVICIOS DE SEGURIDAD" << endl;
   cout << "	1.Alta de servicio." << endl;
   cout << "	2.Baja de servicio." << endl;
   cout << "	3.Lista de servicios activos." << endl;
   cout << "	4.Salir del Administador." << endl << endl;
   cout << "Elija una opcion: ";
}

void ini_sskey() //Asigna memoria a los segmentos de las llaves que van a ser procesados...
{
   for (int i = 0; i < RKEYS; i++)
      privKeySS[i] = new uint8[LNG_PRIVKEY];
}

void fin_sskey() //Elimina los segmentos de la llave obtenidos y libera la memoria...
{
   for (int i = 0; i < RKEYS; i++)
   {
      if (privKeySS[i])
      {
         memset(privKeySS[i], 0, LNG_PRIVKEY);
         delete privKeySS[i];
         privKeySS[i] = NULL;
      }
   }
}

bool BajaServ(CSHMemPKI *mem)
{
   system("clear");
   bool con = false;
   char servicio[ID_SRV];
   uint8 napl = mem->getNumApls();
   if(!napl)
   {
      cout << "\n No se encuentran servicios activos..." << endl;
      return con;
   }
   cout << endl << endl << "ADMINISTRADOR DE SERVICIOS DE SEGURIDAD" << endl;
   cout << "\n Servicio a dar de baja: ";
   cin >> servicio;
   servicio[ID_SRV-1] = 0;
   if(!mem->bajaServ(servicio))
   {
      cout << "Error: " << mem->getError() << " en la aplicacion." << endl;
      return con;
   }         
   else con = true;   
   return con;
}

bool leeDatosFloppy(int id, char *arch)
{
   bool ok = true;
   ManArchivo archivo;
   uint16 ldata = 0;
   if(!archivo.ProcesaArchivo(arch, (uchar*)privKeySS[id], (int*)&ldata))
   {
      ok = false;
      return ok;
   }
   else
      lprivKeySS[id] = ldata;
   return ok;
}

bool leeDatos(uint8 id, char *path)
{
   bool ok = true;
   FILE *fp;
   int tamDatos = 0;
   ManArchivo archivo;
   if(!archivo.TamArchivo(path, &tamDatos))
      ok = false;
   else {
   fp = fopen(path, "rb");
   fread((uint8*) privKeySS[id], 1, tamDatos, fp);
   fclose(fp); 
   lprivKeySS[id] = tamDatos;
   }
   return ok;
}

#ifdef DBG
bool armaPK(RSA_PRIVADA *pk)
{
   SGIRSA rsa;
   bool ok = true;
   //int err;
   static uint8 id;
   char pathkey[100], archivo[100], pwd[100];
   for(id = 0; ok && (id < RKEYS); id++)
   {
      getchar();
      system("clear");
      cout << "\n\nADMINISTRADOR DE SERVICIOS DE SEGURIDAD" << endl;
      cout << "\nCARGA DE LLAVE PRIVADA DEL SERVICIO" << endl << endl;
      cout << "\nEscriba la ruta de la llave:  " << id + 1 << endl;
      cin >> archivo;
      sprintf(pathkey, "/home/guza795k/MemoriaCompartida/%s", archivo);
      
      if(!leeDatos(id, pathkey))
         ok = false;
      
      if(ok)
      {
         ok = false;
         for (uint8 j = 0; !ok && j < 3; j++)
         {
            strcpy(pwd, getpass("\nProporcione la contraseņa de su llave: "));
            uint8 digestion[16];
            MD5_CTX md5ctx;
            RC4_KEY rc4key;

            MD5_Init(&md5ctx);
            MD5_Update(&md5ctx, pwd, strlen(pwd));
            MD5_Final(digestion, &md5ctx);
            
            RC4_set_key(&rc4key, 16, digestion);
            RC4(&rc4key, lprivKeySS[id], privKeySS[id], tmp_privKeySS);
            
            if(tmp_privKeySS == NULL)
               ok = true;
#ifdef DBG
            FILE *fp;
            char ag[15];
            char path[150];
            cout << "Archivo de salida? ";
            cin >> ag;
            sprintf(path, "/home/guza795k/MemoriaCompartida/%s", ag);
            fp = fopen(path, "wb");
            size_t escrito = fwrite(tmp_privKeySS, 1, lprivKeySS[id], fp);
            fclose(fp);
#endif //               
            memcpy(privKeySS[id], tmp_privKeySS, lprivKeySS[id]);
            ok = true;
         }
      }
   }
   if (ok)
   {
      #ifdef _GSI_WINDOWS_
      CSecretoCompartido* ss = new CSecretoCompartido(RKEYS, RKEYS, (R_RANDOM_STRUCT *) NULL);
      #else
      CSecretoCompartido* ss = new CSecretoCompartido(RKEYS, RKEYS);
      #endif
      ss->decodifica((uint8**) &privKeySS, lprivKeySS[0], (uint8*)pk);
      if(pk->bits != 1024 && pk->bits != 2048 && pk->bits != 4096 )
         ok = false;
   }
   return ok;
}

#else
bool armaPK(RSA_PRIVADA *pk)
{
   bool ok = true;
   int err;
   char tecla, pathkey[100], archivo[100], pwd[100];
   
   for(uint8 i = 0; ok && i < RKEYS; i++)
   {
      bool cargafloppy = false;
      int opt = 0;
      int nagente = 0;
      getchar();
      system("clear");
      do{
         opt++;
         system("clear");
         cout << "\n\nADMINISTRADOR DE SERVICIOS DE SEGURIDAD" << endl;
         cout << "\nCARGA DE LLAVE PRIVADA DEL SERVICIO" << endl << endl;
         cout << "\nInserte el disco del agente presente " << i + 1 << endl;
         cout << " Oprima cualquier tecla cuando este listo...";
         getchar();
         if(mount("/dev/fd0", "/mnt/floppy", "msdos", MS_MGC_VAL | MS_RDONLY, NULL) == -1)
         {
            int error = errno;
            if (error)
            {
               cout << "\n\nError al tratar de montar el sistema de archivos correspondiente al floppy (" << error << ")\n";
               cout << "\t" << strerror(error) << "\n\n";
               cout <<"Intento de cargado: " << opt << " presione cualquier tecla para el siguiente intento...";
               getchar();
            }
         }
         else cargafloppy = true;
         if(opt == 3)
         {
            cout << "No se puede leer el disco del agente intente con otro disco..." << endl;
            opt = 0;
            nagente++;
            getchar();
         }
         if(nagente == 5)
         {
            system("clear");
            cout << endl << "ADMINISTRADOR DE SERVICIOS DE SEGURIDAD" << endl;
            cout << "FALLO GENERAL. Ninguno de los discos de los agentes se logro cargar." << endl;
            cout << "Fin de la aplicacion..." << endl;
            return false;
         }
      }while(!cargafloppy);
      system("clear");
      cout << endl << "ADMINISTRADOR DE SERVICIOS DE SEGURIDAD" << endl;
      cout << "\nEscriba el nombre de la llave privada: ";
      cin.getline(archivo, sizeof(archivo));
      
      sprintf(pathkey, "/mnt/floppy/%s", archivo);
      if(!leeDatosFloppy(i, pathkey))
      {
         cout << "Error al leer datos del disco..." << endl;         
         ok = false;
      }         
      if(ok)
      {
         ok = false;
         for (uint8 j = 0; !ok && j < 3; j++)
         {
            strcpy(pwd, getpass("\nProporcione la contraseņa de su llave: "));
            uint8 dig[16];
            MD5_CTX md5ctx;
            RC4_KEY rc4key;
            MD5_Init(&md5ctx);
            MD5_Update(&md5ctx, pwd, strlen(pwd));
            MD5_Final(dig, &md5ctx);
            RC4_set_key(&rc4key, 16, dig);
            RC4(&rc4key, lprivKeySS[i], privKeySS[i], tmp_privKeySS);
            
            if (tmp_privKeySS[0] == 0x30)
            {
               uint8  cars_lng;
               uint16 lng_datos;

               if (tmp_privKeySS[1] < 0x80)
               {
                  cars_lng  = 1;
                  lng_datos = tmp_privKeySS[1];
               }
               else if (tmp_privKeySS[1] == 0x81)
               {
                  cars_lng  = 2;
                  lng_datos = tmp_privKeySS[2];
               }
               else if (tmp_privKeySS[1] == 0x82)
               {
                  cars_lng  = 3;
                  lng_datos = (uint16(tmp_privKeySS[2]) << 8) | uint16(tmp_privKeySS[3]);
               }
               else
                  continue;
               memcpy(privKeySS[i], tmp_privKeySS + cars_lng + 1, lprivKeySS[i] - cars_lng -1);
               ok = true;
            }
         }
      }
      umount("/mnt/floppy");
   }  
   if (ok)
   {
      #ifdef _GSI_WINDOWS_
      CSecretoCompartido* ss = new CSecretoCompartido(RKEYS, RKEYS, (R_RANDOM_STRUCT *) NULL);
      #else
      CSecretoCompartido* ss = new CSecretoCompartido(RKEYS, RKEYS);
      #endif
      ss->decodifica((uint8**) &privKeySS, lprivKeySS[0], (uint8*) pk); 
      if(pk->bits != 1024 && pk->bits != 2048 && pk->bits != 4096 )
         ok = false;
   }
   return ok;
}
#endif //DBG
void ListaServ(CSHMemPKI *mem)
{
   cout << "\nADMINISTRADOR DE SERVICIOS DE SEGURIDAD" << endl;
   uint16 napl = mem->getNumApls();
   if(napl <= 0)
   {
      cout << "\n\nNo hay servicios iniciados..." << endl;
      return;
   }
   cout << "\n\nLISTA DE SERVICIOS DE SEGURIDAD ACTIVOS" << endl << endl;
   for(uint8 i = 0; i < napl; i ++)
   {
      if (mem->getServInfo(i).clave[0])
         cout << mem->getServInfo(i).clave << endl;
   }
}

void termina()
{
   cout << endl << "Hasta luego..." << endl;
   exit(0);
   //return mem->cierra(true);
}

bool getCertInfo(ServSegInfo* pk, char* certificado, struct rsa_st* pubKey)
{
   //static char ruta[128];
   X509 *x509;
   const uchar *buffer;
   int lbuffer = 0;
   ManArchivo archivo;
   if(!archivo.TamArchivo(certificado, &lbuffer))
   {
      cout << "No existe el archivo de certificado." << endl;
      return false;
   }   
   buffer = new uchar[lbuffer];
   if(!archivo.ProcesaArchivo(certificado, (uchar*)buffer, &lbuffer))
   {
      cout << "Error: No se extrajeron los datos del certificado." << endl;
      return false;
   }
   x509 = X509_new();
   if(x509 == NULL)
   {
      cout << "Error: No se aparto memoria para el certificado." << endl;
      return false;
   }
   if(!d2i_X509(&x509, &buffer, lbuffer))
   {
      cout << "\n\nError: no se pudo decodificar el certificado\n";
      return false;
   }

   ASN1_INTEGER* serial = X509_get_serialNumber(x509);
   if (serial->length > NUM_SERIE)
   {
      cout << "\n\nError: numero de serie demasiado grande\n";
      return false;
   }
   memcpy(pk->numSerie, serial->data, serial->length);
   pk->numSerie[serial->length] = 0;

   ASN1_TIME *vigINI =  X509_get_notBefore(x509);
   ASN1_TIME *vigFIN =  X509_get_notAfter(x509);

   memcpy(&pk->vigIni, vigINI->data, vigINI->length);
   memcpy(&pk->vigFin, vigFIN->data, vigFIN->length);
   
   memcpy(pubKey, X509_get_pubkey(x509)->pkey.rsa, sizeof(struct rsa_st));

   X509_free(x509);
   return true;
}
intE  setprivada(RSA *privada, RSA_PRIVADA *privateK)
{
   int res;
   privateK->bits = (size_t)BN_num_bits(privada->n);
   if(privateK->bits <= 0) return ERR_NO_CARGA_BITS;
   res = BN_bn2bin(privada->n, (uchar*)privateK->moduloP);
   if(res <= 0) return ERR_NO_MODULO;
   res = BN_bn2bin(privada->e, (uchar*)privateK->exponenteP);
   if(res <= 0) return ERR_NO_EXP_PUB;
   res = BN_bn2bin(privada->d, (uchar*)privateK->exponentePv);
   if(res <= 0) return ERR_NO_EXP_PRIV;
   res = BN_bn2bin(privada->p, (uchar*)privateK->primo[0]);
   if(res <= 0) return ERR_NO_PRIMO;
   res = BN_bn2bin(privada->q, (uchar*)privateK->primo[1]);
   if(res <= 0) return ERR_NO_PRIMO2;
   res = BN_bn2bin(privada->dmp1, (uchar*)privateK->primoExp[0]);
   if(res <= 0) return ERR_PRIME_EXP;
   res = BN_bn2bin(privada->dmq1, (uchar*)privateK->primoExp[1]);
   if(res <= 0) return ERR_PRIME_EXP2;
   res = BN_bn2bin(privada->iqmp, (uchar*)privateK->coeficiente);
   if(res <= 0) return ERR_NO_COEF;
   return 0;
}

bool CargaServ(CSHMemPKI *mem)
{
   bool ok = true;
   system("clear"); 
   SGIRSA sgirsa;
   char servicio[ID_SRV];
   char pathcert[280];
   RSA_PRIVADA privada;
   struct rsa_st publica;
   cout << endl << "ADMINISTRADOR DE SERVICIOS DE SEGURIDAD" << endl;
   cout << "\nTeclee el servicio de desee iniciar: " << endl;
   cin >> (servicio);
   cout << "Ha elegido el servicio: " << servicio << endl;
   cout << "\nTeclee la ruta del certificado: " << endl;
   cin >> (pathcert);
   cout << "Ha elegido el certificado: " << pathcert << endl; 
   if(mem->findServ(servicio) != (uint8) -1)
   {
      cout << "\n El servicio ya existe..." << endl << endl;
      return ok = false;
   }
   ServSegInfo *srv = new ServSegInfo;
   memset(srv, 0, sizeof(struct serv_info_st));
   memcpy(srv->clave, servicio, ID_SRV);

   if(getCertInfo(srv, pathcert, &publica))
   {
      int cv;
      cout << "	\n\n	1. Llave PKCS8." << endl;
      cout << "	2. Llave de Secreto Compartido." << endl;
      cout << "Esciba la opcion que desea: ";
      cin >> cv;
      if(cv == 2)
      {
         ini_sskey();
         if(armaPK(&privada))
         {
            
           /* if(BN_cmp(priv->n, publica.n) && BN_cmp(priv->e, publica.e) != 0)
            {
               cout << "\nLa llave no coincide con el certificado..." << endl;
               return ok = false;
            }
            else*/
               memcpy(srv->privKey, &privada, sizeof(struct sgi_privateRSA_st));
               mem->registraServ(srv);
         }
         fin_sskey();
      }
      if(cv == 1)
      {   
          SGIRSA rsa;
          RSA *priv;
          priv = RSA_new();
          intE error;
          char pw[25], ruta[250];
          system("clear");
          cout << "\n\nADMINISTRADOR DE SERVICIOS DE SEGURIDAD" << endl;
          cout << "\nCARGA DE LLAVE PRIVADA DEL SERVICIO" << endl << endl;
          cout << "\nEscriba la ruta de la llave:  " << endl;
          cin >> ruta;
          strcpy(pw, getpass("\n\nEscriba el password de su llave privada: "));
          getchar();
          error = rsa.DecodPvKey(ruta, pw, &priv);
          if(error != 0)
          {
             cout << "Ocurrio el error: " << error << " al abrir la llave privada..." << endl;
             ok = false;
          }
          if(BN_cmp(priv->n, publica.n) && BN_cmp(priv->e, publica.e) != 0)
          {
             cout << "\nLa llave no coincide con el certificado..." << endl;
             return ok = false;
          }
          error = setprivada(priv, &privada);
          if(error != 0)
          {
             cout << "Ocurrio el error: " << error << " al abrir la llave privada..." << endl;
             ok = false;
          }
          else
             memcpy(srv->privKey, &privada, sizeof(RSA_PRIVADA));
             mem->registraServ(srv);
          RSA_free(priv);                      
      }
   }
   delete srv;
   srv = NULL;
   system("clear");
   cout << "\nSe inicio el servicio con exito..." << endl;
   return true;
}

bool otraOp()
{
   char opc;
   cout << endl << "Desea realizar otra operacion?(y,n)";
   cin >> opc;
   if(opc == 'y')
      return true;
   else return false;
}
/* --------------------------------------------------------------------------- */
/* --------------------------------------------------------------------------- */
int main()
{
   iniciaLibOpenSSL();
   CSHMemPKI *pki = new CSHMemPKI(KEY_SHMEM, TamBufShMem);
   if(!pki->abre(true))
      return -1000;
   uint8 opc;
   bool con = true;
   do{
      menuAdm();
      opc = cin.get();
      switch (opc)
      {
         case '1': {CargaServ(pki); 
            con = otraOp();
            break;}
         case '2': {BajaServ(pki); 
            con = otraOp();
            break;}
         case '3': {ListaServ(pki); 
            con = otraOp();
            break;}
         case '4':{
            termina();
            con = false;
            break;}
      }
   }while(con);
   cout << endl << "Hasta luego..." << endl;
   terminaLibOpenSSL();
   return 0;
}
