static const char* _ADMSRVSEGURIDAD_H_VERSION_ ATR_USED = "AdmSrvSeguridad @(#)"\
"DSIC07412AC_ 2007-12-14 AdmSrvSeguridad.h 1.1.0/0";

//#VERSION: 1.1.0
uint8* privKeySS [RKEYS] = { NULL, NULL, NULL }; 
uint16 lprivKeySS[RKEYS];

uint8  tmp_privKeySS[LNG_PRIVKEY];


void menuAdm();
void ini_sskey();
void fin_sskey();
bool BajaServ(CSHMemPKI *mem);
bool leeDatosFloppy(int id, char *arch);
bool armaPK(RSA_PRIVADA *pk);
void ListaServ(CSHMemPKI *mem);
void termina();
bool getCertInfo(ServSegInfo* pk, char* certificado, struct rsa_st* pubKey);
bool CargaServ(CSHMemPKI *mem);
bool otraOp();
bool leeDatos(int id, char *path);
intE obtienePVK(RSA_PRIVADA *privkey, RSA *rsapriv);
intE  setprivada(RSA *privada, RSA_PRIVADA *privateK);
