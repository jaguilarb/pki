#ifndef _ADMSRVSEGREMOTO_H_
#define _ADMSRVSEGREMOTO_H_
static const char* _ADMSRVSEGREMOTO_H_VERSION_ ATR_USED = "AdmSrvSeguridad @(#)"\
"DSIC07412AC_ 2007-12-14 AdmSrvSegRemoto.h 1.1.0/0";

//#VERSION: 1.1.0
#include <unistd.h>
#include <iostream>
#include <memory.h>
#include <curses.h>
#include <errno.h>
#include <sys/mount.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <Sgi_SrvUnx.h>
//- #include <SgiOpenSSL.h> //- GHM (070427)
#include <Sgi_OpenSSL.h>    //+ GHM (070427)
#include <math.h>

//- #include "ShMemPKI.h"          //- GHM (070427)
#include <Sgi_MemCompartida.h>     //+ GHM (070427)
//- #include "SecretoCompartido.h" //- GHM (070427)
#include <Sgi_SecretoCompartido.h> //+ GHM (070427)

#define TamBufShMem    (4 * 1024 * 1024)
#define KEY_SHMEM      1979052003
#define ID_SRV         16
#define NUM_SERIE      21
#define RKEYS          ((uint8)3)

#define OPALTA		0
#define OPBAJA		1
#define OPCONS		2

uint8* privKeySS [RKEYS] = { NULL, NULL, NULL }; /*[LNG_PRIVKEY];*/
uint16 lprivKeySS[RKEYS];

uint8  tmp_privKeySS[LNG_PRIVKEY];

static int password_cb(char *buf, int size, int rwflag, void *password);
int verify_callback(int ok, X509_STORE_CTX *store);

typedef struct st_asn_tag
{
   int posArray;
   uchar idTag[1];
   bool compuesta;
   int tam;
   int numTags;
   bool correcta;
}ASNTag;

double CalcTagLen(uchar *bytes, int tam);
void leeTags(uchar *buff, int lbuff, ASNTag *tags, int *ntags);
int compInc(ASNTag *t, int nt);
int calcTam(ASNTag *tags, int ntags);
bool corrige(ASNTag *tags, int ntags);

class CAdmSeg
{
protected:
///// Variables de Clase ///////
   int disp;
   intE error;
   CSSL_parms  paramAC;
   CSSL_Skt   *sktAC;
   Sgi_SSL     sslAC;
   ServSegInfo *srv;   
   SGIRSA sgirsa;
   uchar MemServ[1024*10];
   RSA_PRIVADA privada;
   struct rsa_st publica;
   char *dev[2][2];
   
   
///// Funciones de Clase //////
   void ini_sskey();
   void fin_sskey();
   bool leeDatosFloppy(int id, char *arch);
   bool armaPK(RSA_PRIVADA *pk);
   bool getCertInfo(ServSegInfo* pk, char* certificado, struct rsa_st* pubKey);
   bool leeDatos(int id, char *path);
   bool leeDatos(int id, char *path, int &error);
   intE obtienePVK(RSA_PRIVADA *privkey, RSA *rsapriv);
   intE setprivada(RSA *privada, RSA_PRIVADA *privateK);
   //intE obtieneDatos(char*, char*, char*, char*, char*, char*, char*);
   bool montaDispositivo(); //>>> ERGL
   bool cargaSegmento(uint8); //>>> ERGL mar jul 24 17:49:42 CDT 2007
   bool cargaArchivo(uint8); //>+ ERGL mar jul 24 16:07:39 CDT 2007
   bool desmontarDispositivo(); //>+ ERGL mar jul 24 17:28:14 CDT 2007
   bool desencriptaSegmento(uint8); //>+ ERGL mi� jul 25 09:56:15 CDT 2007
   bool armaSegmentos(RSA_PRIVADA *pk); //>+ ERGL mi� jul 25 17:39:22 CDT 2007
public:
   CAdmSeg();
   ~CAdmSeg();
   bool BajaServ();
   void ListaServ();
   void termina();
   bool CargaServ();
   bool otraOp();
   //intE conecta(char *archConfg);
   intE conecta(); 
   void menuAdm();
};
#endif
