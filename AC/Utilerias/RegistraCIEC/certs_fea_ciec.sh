static const char* _CERTS_FEA_CIEC_SH_VERSION_ ATR_USED = "RegistraCIEC @(#)"\
"DSIC07412AC_ 2007-12-14 certs_fea_ciec.sh 1.1.0/0";

//#VERSION: 1.1.0

#################################################################################################
# Variables para la conexi�n a la base de datos Oracle (Cambiar a los valores correspondientes)
HOST="10.70.100.92"
PORT="1521"
SID="TPSEGPKI"
USR="SYSTEM"
PWD="manager2014"
CONNECT="$USR/$PWD@(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=$HOST)(PORT=$PORT))(CONNECT_DATA=(SERVER=DEDICATED)(SID=$SID)))"

SQL_FILE="query.sql"
#################################################################################################
#Variable que indica la base de datos a utilizar
# I - Informix
# O - Oracle
BASE_DATOS="I"
#################################################################################################

echo "##################################################################"

export ruta=/opt/local/SAT/PKI/regCIEC
export nombre=`date +%Y%m%d%H%M%S`

export ruta_procesados=$ruta/procesados
export ruta_enviar=$ruta/enviar

export INFORMIXSERVER=pki_ac_tcp
#export INFORMIXSERVER=pki_sles10_des_tcp        -- Se extrae del /usr/local/IBM/informix/etc/sqlhost
export INFORMIXDIR=/usr/local/IBM/informix
export ONCONFIG=onconfig.pki_ac
#export ONCONFIG=onconfig.pki_sles10_des  -- Se localiza el archivo de config en /usr/local/IBM/informix/etc/
export CLIENT_LOCALES=es_es.819
export DB_LOCALES=es_es.819


echo "###################  $nombre  #############################"
echo "##################################################################"; echo

cd $ruta

# ##################################################################
echo "#####    Obtiene el �ltimo n�mero de serie enviado a la CIEC"
# ##################################################################
export no_serie_min=`cat $ruta/ult_no_serie.cfg`
echo "�ltimo n�mero de serie: $no_serie_min"

if [ -z $no_serie_min ]
then
   echo 'Error: archivo con �ltimo n�mero de serie da�ado'
   exit 1
fi

# ##################################################################
echo "#####    Genera archivo de datos"
# ##################################################################

if test "$BASE_DATOS" == "I"; then
   echo "#####    Base de datos a utilizar Informix"

   export query="set role rl_ac; unload to $ruta_enviar/$nombre.dat.txt select first 500 no_serie, trim(rfc), nombre, correo from certificado where no_serie > '$no_serie_min' and no_serie <= '00001000000099999999' and tipcer_cve = 1 order by no_serie"
   #Se debe modificar el limite superior del n�mero de serie 
   #export query="set role rl_ac; unload to $ruta_enviar/$nombre.dat.txt select first 500 no_serie, trim(rfc), nombre, correo from certificado where no_serie > '$no_serie_min' and no_serie <= '10001200000099999999' and tipcer_cve = 1 order by no_serie"
   echo "QUERY=$query"
   echo; echo; echo;

   if ! echo $query | dbaccess ac_sat - 
      then
         echo 'Error al extraer informaci�n de la base de datos'
         exit 1
   fi

elif test "$BASE_DATOS" == "O" ; then
   echo "#####    Base de datos a utilizar Oracle"

   export query="SELECT * FROM (SELECT NOSERIE || '|' || TRIM(RFC) || '|' || NOM || '|' || CORREO FROM SEG_PKI.SEGT_CERTIFICADO WHERE NOSERIE > '$no_serie_min' AND NOSERIE <= '00001000000099999999' AND TIPCERCVE = 1 ORDER BY NOSERIE) WHERE ROWNUM <= 500;"
   #Se debe modificar el limite superior del n�mero de serie 
   echo "QUERY=$query"
   echo; echo; echo;

   # Se crea el archivo del query
   echo "SET HEADING OFF;" > $SQL_FILE
   echo "SET FEEDBACK OFF;" >> $SQL_FILE
   echo "$query" >> $SQL_FILE
   echo "SET FEEDBACK ON;" >> $SQL_FILE
   echo "SET HEADING ON;" >> $SQL_FILE
   echo "EXIT;" >> $SQL_FILE

   if ! sqlplus -S $CONNECT @$SQL_FILE > "$ruta_enviar/$nombre.dat.txt"; then
      echo 'Error al extraer informaci�n de la base de datos'

      exit 1
   fi

   # Oracle devuelve el resultado con un salto de linea al inicio, por lo cual se quita
   echo "$(tail -n +2 $ruta_enviar/$nombre.dat.txt)" > "$ruta_enviar/$nombre.dat.txt"
else
   echo "#####    Base de datos a utilizar inv�lida"

   exit 1
fi


# ##################################################################
echo "#####    Revisa si existen datos a enviar"
# ##################################################################
export contador=`cat $ruta_enviar/$nombre.dat.txt | wc -l`
if (( $contador == 0 )) 
   then
      echo "No hay registros a notificar para la CIEC"
      rm $ruta_enviar/$nombre.dat.txt
      exit 0 
else
   echo "Se van a reportar $contador registros"
fi

# ##################################################################
echo "#####    Actualiza el archivo con el ultimo numero de serie"
# ##################################################################
tail -1 $ruta_enviar/$nombre.dat.txt | cut --delimiter=\| -f 1 > $ruta/ult_no_serie.cfg

# ##################################################################
echo "#####    Genera el archivo a enviar"
# ##################################################################
cut --delimiter=\| -f 2,3,4,5 $ruta_enviar/$nombre.dat.txt > $ruta_enviar/$nombre.env.txt.unx
unix2dos -n $ruta_enviar/$nombre.env.txt.unx $ruta_enviar/$nombre.env.txt
rm -f $ruta_enviar/$nombre.env.txt.unx

# ##################################################################
echo "#####    Envia el archivo: AC -> AR -> CIEC"; echo
# ##################################################################
if ! soky T ar_sat ciec_sat $ruta_enviar/$nombre.env.txt $ruta/tmp/$nombre.tmp
   then
      echo "ERROR: no se pudo enviar el archivo a la AR"
      exit 2
fi
echo

if ! soky X ar_sat ciec_sat "soky T ciec_sat usrsktfea $ruta/tmp/$nombre.tmp C:/Programs/SCADE_Servicios/AltaUsuarios/Input/$nombre.tmp > $ruta/tmp/$nombre.skt.ar"
   then
      echo
      if soky R ar_sat ciec_sat $ruta/tmp/$nombre.skt.ar $ruta/tmp/$nombre.skt.ac
         then
            echo; cat $ruta/tmp/$nombre.skt.ac
      fi
      echo; echo "ERROR: no se pudo enviar el archivo de la AR al equipo de la CIEC"; echo
      exit 3
fi
echo

if ! soky X ar_sat ciec_sat "soky X ciec_sat usrsktfea \"ren C:\\Programs\\SCADE_Servicios\\AltaUsuarios\\Input\\$nombre.tmp $nombre.txt\" > $ruta/tmp/$nombre.skt.ar"
   then
      echo
      if soky R ar_sat ciec_sat $ruta/tmp/$nombre.skt.ar $ruta/tmp/$nombre.skt.ac
         then
            echo; cat $ruta/tmp/$nombre.skt.ac
      fi
      echo; echo "ERROR: no se pudo renombrar el archivo de en el equipo de la CIEC"; echo
      exit 4
fi
echo

soky X ar_sat ciec_sat "rm -f $ruta/tmp/$nombre.*"
echo
rm -f $ruta/tmp/$nombre.*

# ##################################################################
echo "#####    Mueve los archivos a procesados"
# ##################################################################
mv $ruta_enviar/$nombre.* $ruta_procesados
echo
