static const char* _CERTS_FEA_CIEC_PROD_070622_SH_VERSION_ ATR_USED = "RegistraCIEC @(#)"\
"DSIC07412AC_ 2007-12-14 certs_fea_ciec_prod_070622.sh 1.1.0/0";

//#VERSION: 1.1.0
echo "##################################################################"

export ruta=/opt/local/SAT/PKI/regCIEC
export nombre=`date +%Y%m%d%H%M%S`

export ruta_procesados=$ruta/procesados
export ruta_enviar=$ruta/enviar

export INFORMIXSERVER=pki_ac_tcp
export INFORMIXDIR=/usr/local/IBM/informix
export ONCONFIG=onconfig.pki_ac
export CLIENT_LOCALES=es_es.819
export DB_LOCALES=es_es.819


echo "###################  $nombre  #############################"
echo "##################################################################"; echo

cd $ruta

# ##################################################################
echo "#####    Obtiene el �ltimo n�mero de serie enviado a la CIEC"
# ##################################################################
export no_serie_min=`cat $ruta/ult_no_serie.cfg`
echo "�ltimo n�mero de serie: $no_serie_min"

if [ -z $no_serie_min ]
then
   echo 'Error: archivo con �ltimo n�mero de serie da�ado'
   exit 1
fi

# ##################################################################
echo "#####    Genera archivo de datos"
# ##################################################################
export query="set role rl_ac; unload to $ruta_enviar/$nombre.dat.txt select first 500 no_serie, trim(rfc), nombre, correo from certificado where no_serie > '$no_serie_min' and no_serie <= '00001000000099999999' and tipcer_cve = 1 order by no_serie"
echo "QUERY=$query"
echo; echo; echo;

if ! echo $query | dbaccess ac_sat - 
   then
      echo 'Error al extraer informaci�n de la base de datos'
      exit 1
fi

# ##################################################################
echo "#####    Revisa si existen datos a enviar"
# ##################################################################
export contador=`cat $ruta_enviar/$nombre.dat.txt | wc -l`
if (( $contador == 0 )) 
   then
      echo "No hay registros a notificar para la CIEC"
      rm $ruta_enviar/$nombre.dat.txt
      exit 0 
else
   echo "Se van a reportar $contador registros"
fi

# ##################################################################
echo "#####    Actualiza el archivo con el ultimo numero de serie"
# ##################################################################
tail -1 $ruta_enviar/$nombre.dat.txt | cut --delimiter=\| -f 1 > $ruta/ult_no_serie.cfg

# ##################################################################
echo "#####    Genera el archivo a enviar"
# ##################################################################
cut --delimiter=\| -f 2,3,4,5 $ruta_enviar/$nombre.dat.txt > $ruta_enviar/$nombre.env.txt.unx
unix2dos -n $ruta_enviar/$nombre.env.txt.unx $ruta_enviar/$nombre.env.txt
rm -f $ruta_enviar/$nombre.env.txt.unx

# ##################################################################
echo "#####    Envia el archivo: AC -> AR -> CIEC"; echo
# ##################################################################
if ! soky T ar_sat ciec_sat $ruta_enviar/$nombre.env.txt $ruta/tmp/$nombre.tmp
   then
      echo "ERROR: no se pudo enviar el archivo a la AR"
      exit 2
fi
echo

if ! soky X ar_sat ciec_sat "soky T ciec_sat usrsktfea $ruta/tmp/$nombre.tmp D:/Programs/SCADE_Servicios/AltaUsuarios/Input/$nombre.tmp > $ruta/tmp/$nombre.skt.ar"
   then
      echo
      if soky R ar_sat ciec_sat $ruta/tmp/$nombre.skt.ar $ruta/tmp/$nombre.skt.ac
         then
            echo; cat $ruta/tmp/$nombre.skt.ac
      fi
      echo; echo "ERROR: no se pudo enviar el archivo de la AR al equipo de la CIEC"; echo
      exit 3
fi
echo

if ! soky X ar_sat ciec_sat "soky X ciec_sat usrsktfea \"ren D:\\Programs\\SCADE_Servicios\\AltaUsuarios\\Input\\$nombre.tmp $nombre.txt\" > $ruta/tmp/$nombre.skt.ar"
   then
      echo
      if soky R ar_sat ciec_sat $ruta/tmp/$nombre.skt.ar $ruta/tmp/$nombre.skt.ac
         then
            echo; cat $ruta/tmp/$nombre.skt.ac
      fi
      echo; echo "ERROR: no se pudo renombrar el archivo de en el equipo de la CIEC"; echo
      exit 4
fi
echo

soky X ar_sat ciec_sat "rm -f $ruta/tmp/$nombre.*"
echo
rm -f $ruta/tmp/$nombre.*

# ##################################################################
echo "#####    Mueve los archivos a procesados"
# ##################################################################
mv $ruta_enviar/$nombre.* $ruta_procesados
echo
