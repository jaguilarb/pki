#ifndef _SC_MEDIADOR_H_
#define _SC_MEDIADOR_H_
static const char* _SC_MEDIADOR_H_VERSION_ ATR_USED = "SC_Mediador @(#)"\
"DSIC09365SLD 2007-12-13 SC_Mediador.h 1.1.0/0";

//#VERSION: 1.1.0
#ifdef DBG
   #define PATH_CFG "/etc/SC_Mediador_dbg.cfg"
   #define PATH_LOG "/var/log/SC/SC_Mediador_dbg.log"
#else
   #define PATH_CFG "/etc/SC_Mediador.cfg"
   #define PATH_LOG "/var/log/SC/SC_Mediador.log"
#endif

#include "CProcParticipaciones.h"
#include <Sgi_ProtegePwd.h>
#include <Sgi_ConfigFile.h>
#include <Sgi_Demonio.h>
#include <Sgi_MsgPKI.h>
#include <sys/msg.h>
#include "comun.h"
#include <string>

#define MQ_TAMAX 4096 * 64;


class CSC_Mediador : public CDemonio
{

   private :
      int mq_id,man_cm, PARTICIPACIONES_MIN;
      CConfigFile *config;
      std::string passwd, cer, key, temporal, s_participaciones, shkid, shkey, shkpas, certAC;
      //char *ruta_cfg;
      virtual bool Inicia ();
      //virtual bool Termina();
      virtual bool setManejadorSignals();
      virtual bool CargaCfg();
      static void ManejadorSenales(int sig);
      bool iniciaCola();
      bool finalizaCola();
      char *desencriptaPwdKey();
      bool creaFifos();
      /////////
      bool generaContr();
      bool generaContr(char* cNumSerie, uchar* uContr, int* iContr ); 
      void generaNumAlea(uchar* uNumAlea, int* iNumAlea);
      void hashSHA384(uchar* cNumAlea, int iNumAlea , uchar* cHash384 , int* iHash384);
      fpos_t localiza(FILE *arch, char* cEtiqueta,char* cContenido ,int *iBusq,  bool esEtiqueta); 
      void trim(const char* cCadena,  char* cCadsinblancos);
      void ponerContEnArchCofig(FILE *arch, char* cNomArchivo, uchar*uContr, char* cVar);
      //bool ProcesaParametros(int argc, char* argv[], char* env[]);
       
   public :
      CSC_Mediador();
      bool Proceso();
      virtual ~CSC_Mediador();

};

#endif
