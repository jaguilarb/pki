static const char* _SC_MEDIADOR_CPP_VERSION_ ATR_USED = "SC_Mediador @(#)"\
"DSIC09365SLD 2007-12-13 SC_Mediador.cpp 1.1.0/0";

//#VERSION: 1.1.0
#include "SC_Mediador.h"
#include <signal.h>

CSC_Mediador::
CSC_Mediador() 
{
}

CSC_Mediador::
~CSC_Mediador()
{
}

bool CSC_Mediador::
Inicia()
{
   int error;
   //if( ruta_cfg )
   //   config = new CConfigFile(ruta_cfg);
   //else
      config = new CConfigFile(PATH_CFG);

   if( !config )
      Bitacora->escribe(BIT_ERROR, "No se ha podido iniciar la configuración del servicio.");
   else if( (error = config->cargaCfgVars()) != 0 )
      Bitacora->escribe(BIT_ERROR, "No se pudieron cargar las variables de configuración.");
   else
      return (CargaCfg() && iniciaCola());
   return false;
}

bool CSC_Mediador::
creaFifos()
{
   return true;
}

bool CSC_Mediador::
iniciaCola()
{
   man_cm = msgget(mq_id, IPC_CREAT | 0600 ); // | 0666 );
   if( man_cm == 0)
   {
      finalizaCola();
      man_cm = msgget(mq_id, IPC_CREAT | 0600 ); // | 0666 );
   }
   msqid_ds cola;
   if(man_cm > 0)
   {
      msgctl(man_cm, IPC_STAT, &cola);
      Bitacora->escribePV(BIT_INFO, "La cola de mensajes esta lista. (ID: %d).", man_cm);
      return true;
   }
   else 
   {
      Bitacora->escribePV(BIT_ERROR, "No se pudo crear la cola de mensajes %d. (%d)", mq_id, errno);
   }
   return false;
}


bool CSC_Mediador::
finalizaCola()
{
   msqid_ds cola;
   int iIte = 0;
 
   if(man_cm >= 0)
   {
      do
      {
         msgctl(man_cm, IPC_STAT, &cola);
         if (cola.msg_qnum == 0 || iIte == 1)
         {
            if( !msgctl(man_cm, IPC_RMID, &cola)  )
            {
               Bitacora->escribePV(BIT_INFO,"Se borro la cola de mensajes. (%d)", iIte);
               return true;
            }
            else Bitacora->escribePV(BIT_INFO,"No se borro la cola de mensajes.[%d]", errno);
         }
         else 
         {
            sleep(2000);
            iIte++;
            continue;
         }

      }while(iIte <=1);

   }else Bitacora->escribePV(BIT_INFO,"Error al leer los datos de la cola  de mensajes.[%d]", errno);

   Bitacora->escribePV(BIT_INFO,"man_cm  [ %d ]   ",man_cm);
   return false;


}

char *CSC_Mediador::
desencriptaPwdKey()
{
   /*int pwd_buf_l = 256;
   uint8 *pwd_buf = (uint8 *)calloc(256, sizeof(uint8));
   desencripta((const uint8 *)passwd.c_str(), passwd.length(), pwd_buf, &pwd_buf_l);
   return (char *)pwd_buf;*/
   return (char *)passwd.c_str();
}


void CSC_Mediador::
ManejadorSenales(int sig)
{
   switch ( sig )
   {
      case SIGINT :
         //finalizaCola();
         break;
      case SIGHUP :
         //finalizaCola();
         break;
   } 
}

bool CSC_Mediador::
setManejadorSignals()
{
   struct sigaction acciones;
   memset(&acciones, 0, sizeof(acciones));
   
   acciones.sa_handler = ManejadorSenales;
   acciones.sa_flags   = SA_SIGINFO;

   if( sigaction(SIGINT, &acciones, NULL) ||
       sigaction(SIGHUP, &acciones, NULL) )
   {
      Bitacora->escribePV(BIT_ERROR, "No se han podido establecer los manejadores de señales. (%d)", errno);
      return false;
   }
   return true;
}

bool CSC_Mediador::
CargaCfg()
{
   bool errores = false;
   if ( config->getValorVar("[SC_MEDIADOR]", "MQID", &temporal) )
   {
      Bitacora->escribe(BIT_ERROR, "No se pudo cargar la variable MQID del archivo de configuracion"); 
      errores = true;
   }
   else
      mq_id = atoi(temporal.c_str());   
   if ( config->getValorVar("[SC_MEDIADOR]", "CERTF", &cer) )
   {
      Bitacora->escribe(BIT_ERROR, "No se pudo cargar la variable CERTF del archivo de configuracion"); 
      errores = true;
   }
   if ( config->getValorVar("[SC_MEDIADOR]", "CRKEY", &key) )
   {
      Bitacora->escribe(BIT_ERROR, "No se pudo cargar la variable CRKEY del archivo de configuracion");
      errores = true;
   }
   if ( config->getValorVar("[SC_MEDIADOR]", "PASSW", &passwd) )
   {
      Bitacora->escribe(BIT_ERROR, "No se pudo cargar la variable PASSW del archivo de configuracion");
      errores = true;
   }
   if ( config->getValorVar("[SC_MEDIADOR]", "P_REQ", &s_participaciones) )
   {
      Bitacora->escribe(BIT_ERROR, "No se pudo cargar la variable P_REQ del archivo de configuración");
      errores = true;
   }
   if ( config->getValorVar("[SC_MEDIADOR]", "SHKID", &shkid) )
   {
      Bitacora->escribe(BIT_ERROR, "No se pudo cargar la variable SHKID del archivo de configuración");
      errores = true;
   }
   if ( config->getValorVar("[SC_MEDIADOR]", "SHKEY", &shkey) )
   {
      Bitacora->escribe(BIT_ERROR, "No se pudo cargar la variable SHKEY del archivo de configuración");
      errores = true;
   }
   if ( config->getValorVar("[SC_MEDIADOR]", "SHKPW", &shkpas) )
   {
      Bitacora->escribe(BIT_ERROR, "No se pudo cargar la variable SHKPW del archivo de configuración");
      if(generaContr())
      {
         delete config;
         config = NULL;
         Bitacora->escribe(BIT_INFO,"Se genero la nueva contraseña.");
         config = new CConfigFile(PATH_CFG);         
         if (!config->cargaCfgVars())
         {  
            if (config->getValorVar("[SC_MEDIADOR]", "SHKPW", &shkpas))
               errores = true;
            else 
               Bitacora->escribe(BIT_INFO,"Se leyo la nueva contraseña.");
         }
      }
      else errores = true;     
   }
   if ( config->getValorVar("[SC_MEDIADOR]", "CERTAC", &certAC) )
   {
      Bitacora->escribe(BIT_ERROR, "No se pudo cargar la variable CERTAC del archivo de configuración");
      errores = true;
   }

   else
   {
      PARTICIPACIONES_MIN = atoi(s_participaciones.c_str()); 
      Bitacora->escribePV(BIT_INFO, "Se ha configurado el mediador para construir con %d participaciones", PARTICIPACIONES_MIN);
   } 
   return errores ? false : true;
}

bool CSC_Mediador::
Proceso()
{
   CProcParticipaciones *participaciones = 
      new CProcParticipaciones(
         (char *)cer.c_str(), (char *)key.c_str(), desencriptaPwdKey(), (char*)certAC.c_str(), (char *)shkid.c_str(), 
         (char *)shkey.c_str(), (char *)shkpas.c_str(), man_cm, PARTICIPACIONES_MIN);
   if( participaciones )
   {
      while( participaciones->recibe() );
      finalizaCola();
   }
   return false;
}

CBitacora*
Crea_CBitacora()
{
   CBitacora *bitacora = new CBitacora(PATH_LOG);
  if( bitacora )
  {
     #ifdef DBG
     bitacora->setNivel(BIT_DEBUG);
     #endif
     bitacora->escribe(BIT_INFO, "Inicio de proceso");
     #ifdef DBG
        sleep(20);
     #endif
     return bitacora;
  }
  return NULL;
}

CDemonio*
Crea_CDemonio()
{
   return new CSC_Mediador();
}

const char *
getNombreApl()
{
   return "SC_Mediador";
}


// Agregar constrasenia RORS797e 10.08.09

void CSC_Mediador::
ponerContEnArchCofig(FILE *arch, char* cNomArchivo, uchar*uContr, char* cVar)
{

   char      cReng[1000];
   char cContenido[200];
   struct stat estado;
   int iVarLeida;
   int iResta;

   stat(cNomArchivo,&estado);
   memset(cReng,0,sizeof(cReng));
   memset(cContenido,0,sizeof(cContenido));

   fpos_t posicion =  localiza(arch, cVar, cContenido,&iVarLeida, false);

   if( (posicion.__pos >0) && (cContenido[0] == '$') )
   {
      fsetpos(arch,&posicion);
      iResta = estado.st_size - posicion.__pos;
      fseek(arch,iVarLeida ,SEEK_CUR);
      rewind(arch);
      if ( iResta==iVarLeida )
        fread(cReng,iVarLeida,1,arch);
      else
        fread(cReng,iResta-iVarLeida,1,arch);
      fsetpos(arch,&posicion);
      fprintf(arch,"%s=%s\n",cVar,uContr);
      fprintf(arch,"%s",cReng);
   }
   else
      printf("Contenido :  %s\n", cContenido);

}


bool  CSC_Mediador::
generaContr(char* cNumSerie, uchar* uContr, int* iContr )
{
   uchar uNumAlea[1024];
   uchar uContrSHA[1024];
   uchar uNumSerConcNumAle[256];
   int   iNumSerConcNumAle = sizeof(uNumSerConcNumAle);
   int   iContrSHA = sizeof(uContrSHA);
   int   iNumAlea  = 0;

   memset(uNumAlea, 0, sizeof(uNumAlea));
   memset(uContrSHA,0, sizeof(uContrSHA));
   memset(uNumSerConcNumAle,0,sizeof(uNumSerConcNumAle));

   generaNumAlea(uNumAlea,&iNumAlea);

   memcpy(uNumSerConcNumAle, cNumSerie, 20);
   memcpy(uNumSerConcNumAle +20,uNumAlea,iNumAlea );

   hashSHA384( uNumSerConcNumAle, iNumSerConcNumAle, uContrSHA ,&iContrSHA );

   for(int i =0 ; i<1024; i++)
     hashSHA384( uContrSHA, iContrSHA, uContrSHA ,&iContrSHA );

   encripta(uContrSHA,iContrSHA, uContr,iContr);

   return true;
}

void CSC_Mediador::
trim(const char* cCadena,  char* cCadsinblancos)
{
   int iSinB   = 0;
   int iCadena = strlen(cCadena);
   if ( cCadena )
   {
      for(int i=0 ; i < iCadena; i++)
      {
         if ( cCadena[i] != ' ' )
         {
            cCadsinblancos[iSinB] = cCadena[i];
            iSinB++;
         }
      }
   }
}

   
fpos_t CSC_Mediador::
localiza(FILE *arch, char* cEtiqueta,char* cContenido ,int *iBusq,  bool esEtiqueta)
{
   fpos_t pos;
   char   cReng[200];
   char   iReng     = sizeof(cReng);
   char*  cEnc      = NULL;
   int    iAleer    = 100;
   int    iEtiqueta = 0;
   int    iEOF;
   bool   bEncontrado = false;

   if (arch)
   {
      rewind(arch);
      while(true)
      {
         fpos_t pos;
         cEnc    = NULL;
         memset(cReng,0,sizeof(cReng));
         if ( fgets(cReng, iAleer +1, arch ) != NULL )
         {
            cEnc = strstr( cReng, cEtiqueta);
            if(cEnc)
            {
                iReng = strlen(cReng);
                *iBusq = iReng;
                char cRengsinblancos[iReng+1];
                memset(cRengsinblancos,0,sizeof(cRengsinblancos));
                trim(cReng, cRengsinblancos);

                if (cRengsinblancos[strlen(cRengsinblancos)-1] == '\n')
                {
                   char *cConVar = strstr( cRengsinblancos,"=");
                   if (cConVar )
                   {
                      if ( !strcmp(cConVar+1,(const char*)"\n") )
                         cContenido[0] = '$';
                      else
                         strcpy(cContenido,cConVar+1);
                   }
                   else
                      cContenido[0] = '$';

                   cRengsinblancos[strlen(cRengsinblancos)-1] = 0;
                   if (!esEtiqueta)
                   {
                      if ( cRengsinblancos[strlen(cRengsinblancos)-1] == '=')
                         cRengsinblancos[strlen(cRengsinblancos)-1] = 0;
                   }

                   if (!strcmp(cRengsinblancos,cEtiqueta))
                   {
                      if (esEtiqueta )
                      {
                         iEtiqueta++;
                         if(iEtiqueta == 2)
                         { 
                            bEncontrado = true;
                            return pos;
                         }
                      }else { bEncontrado = true; return pos;}
                   }
                }
             }
         }
         fgetpos(arch,&pos);
         iEOF = feof(arch);
         if ( iEOF != 0 )
            break;
      }
   }
   if ( !bEncontrado ) 
     pos =  localiza(arch,"[SC_MEDIADOR]" ,cContenido ,iBusq, true);
   return pos;

}

/*bool CSC_Mediador::
ProcesaParametros( int argc, char *argv[], char *env[] )
{
   int opt = 0;
   size_t l_path = 0;
   while( (opt = getopt( argc, argv, "c:" )) != -1 )
   {
      switch( opt )
      {
         case 'c' :
            l_path = strlen( optarg );
            ruta_cfg = (char *)malloc( sizeof(char) * l_path+1 );
            ruta_cfg[l_path] = 0x00;
            strncpy( ruta_cfg, optarg, l_path );
            break;
      }
   }
   return true;
}*/

void  CSC_Mediador::
hashSHA384(uchar* cNumAlea, int iNumAlea , uchar* cHash384 , int* iHash384)
{
    int iError = -1;

    SGIRSA sgirsa;
    iError = sgirsa.iniciaDigestion(SHA384_ALG);
    sgirsa.procesoBlqDig(cNumAlea, iNumAlea);
    sgirsa.terminaDigestion(cHash384, iHash384);

}


void CSC_Mediador::
generaNumAlea(uchar* uNumAlea, int *iNumAlea)
{
   uchar uNumAleaU[1];
   *iNumAlea = 236;
   int j = 0;
   memset(uNumAlea,0,sizeof(uNumAlea));

   do
   {
      if ( RAND_bytes(uNumAleaU, 1) )
      {
         if(uNumAleaU[0] != '\0')
         {
           uNumAlea[j] = uNumAleaU[0];
             j++;
         }
         else
             continue;
      }
   }while(j < *iNumAlea);

}
   
bool CSC_Mediador::
generaContr()
{
   uchar uContr[1024];
   int  iContr = 1024;
   bool bResp = false;
   FILE* arch = NULL;
   char cReng[1000];
   memset(cReng,0,sizeof(cReng));

   arch =  fopen(PATH_CFG,"r+b");
   if (arch)
   {
      memset(uContr,0,iContr);
      generaContr("66666600000000000000", uContr, &iContr );
      ponerContEnArchCofig(arch,PATH_CFG, uContr, "SHKPW");
      bResp = true;
      fclose(arch);
   }
   return bResp;
}

