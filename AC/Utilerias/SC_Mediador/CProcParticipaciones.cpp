static const char* _CPROCPARTICIPACIONES_CPP_VERSION_ ATR_USED = "SC_Mediador @(#)"\
"DSIC09365SLD 2007-12-13 CProcParticipaciones.cpp 1.1.0/0";

//#VERSION: 1.1.0
#include "CProcParticipaciones.h"
#ifdef DBG
#include <openssl/md5.h>
#endif //DBG

#define _SHK_CPP_
#include <shk.h>

CProcParticipaciones::
CProcParticipaciones(char *cer, char *key, char *pwd,char* certac, char* shkid, char* shkey, char *shmpass, int mqid, int p_min) :
   indice_participacion(0),
   man_cm(mqid),
   l_participacion(10000),
   PARTICIPACIONES_MIN(p_min),
   shKid(shkid),
   shKey(shkey),
   shmpw(shmpass),
   certAC(certac)
{
   hashes = new unsigned char*[PARTICIPACIONES_MIN];
   participaciones = new unsigned char*[PARTICIPACIONES_MIN];
   pids_participantes = new int[PARTICIPACIONES_MIN];
   descError[0] = (char *)calloc(2, sizeof(char));
   descError[1] = (char *)calloc(512, sizeof(char));
   for( int i =0; i< PARTICIPACIONES_MIN; i++ )
   { 
      participaciones[i] = (unsigned char *)calloc(l_participacion, sizeof(char));
      hashes[i] = (unsigned char*)calloc(16, sizeof(char));
   }
   //msgllave = new MensajesLLavPrivAC();
   //if( msgllave )
   //{
      OpenSSL_add_all_algorithms();
      int error = msgllave.Inicia(0, cer, key, pwd, strlen(pwd));
      if( error )
         Bitacora->escribePV(BIT_ERROR, "No se pudo iniciar la biblioteca de mensajes (%d)", error);
   //}
}

CProcParticipaciones::
~CProcParticipaciones()
{
   for( int i =0; i< PARTICIPACIONES_MIN ; i++ )
   {
      if (hashes[i])
      {
         free (hashes[i]);
         hashes[i] = NULL;
      }
      if (participaciones[i])
      {
         free (participaciones[i]);
         participaciones[i] = NULL;
      }
   }
   if (hashes)
   {
      delete []hashes;
      hashes = NULL;
   }
   if (participaciones)
   {
      delete []participaciones;
      participaciones = NULL;
   }
   if (pids_participantes)
   {
      delete []pids_participantes;
      pids_participantes = NULL;
   }
   for( int i =0; i< 2 && descError[i] ; i++ )
   {
      free(descError[i]);
      descError[i] = NULL;
   }
}

bool CProcParticipaciones::
extraerParticipacion()
{
   bool ret = false;
   int l_b64llave = msgllave.tamMensaje() - sizeof(Encabezado);
   char *b64llave = (char *)calloc(l_b64llave, sizeof(char));
   msgllave.getMensaje(LLAVEPLANA, b64llave, &l_b64llave);
   uint16 l_part = B64_TamCadDecodificada((uint16)l_b64llave); 
   if (!participaciones[indice_participacion] || l_part > l_participacion ) 
   {
      free(participaciones[indice_participacion]);
      participaciones[indice_participacion] = (unsigned char *)calloc(l_part, sizeof(char));
   }
   pids_participantes[indice_participacion] = mensaje.pid;
   if( B64_Decodifica(b64llave, l_b64llave, participaciones[indice_participacion], (uint16 *)&l_part) )
   {
      if( verificaHash(participaciones[indice_participacion], l_part) )
      { 
         Bitacora->escribe(BIT_INFO, "Se recibio un participacion.");
         l_participacion = l_part;
         #ifdef DBG
            //std::string tmp = dumpBuffer("mensaje", participaciones[indice_participacion], l_part);
            //Bitacora->escribe(BIT_DEBUG, tmp.c_str());
         #endif
         ret = respondeMsg(mensaje.pid, ACUSELLAVE);
      }
      else
         setError(ERR_PARTDUP, "La llave empleada ha sido cargada previamente.");
   }
   else
      setError(ERR_B64DECO, "No se pudo decodificar la participacion en B64.");

   if (b64llave)
   {
      free(b64llave);
      b64llave = NULL;
   }
   return ret;
}

bool CProcParticipaciones::
verificaHash(const unsigned char *llave, const int len)
{
   bool ret = true;
   MD5_CTX md5ctx;
   MD5_Init(&md5ctx);
   MD5_Update(&md5ctx, llave, len);
   unsigned char *suma = new unsigned char[len]; 
   MD5_Final(suma, &md5ctx);

   int hash_len = 16;
   uint16 destino = B64_TamCadCodificada(hash_len);
   char *b64hash = new char[destino];
   B64_Codifica(suma, hash_len, b64hash, &destino); 

   if( indice_participacion >= 1 )
   { 
      int i=0;
      for( i=0; i < indice_participacion; i++ )
      {
         if( ( strncmp((char *)hashes[i], b64hash, destino) == 0 ) )
         {
            Bitacora->escribe(BIT_ERROR, "Se recibio una participación que ya se proceso.");
            ret = false;
         }
      }
   } 
   Bitacora->escribe(BIT_DEBUG, "Se almacenara el hash para posteriores participaciones.");
   memcpy(hashes[indice_participacion], b64hash, destino);

   if (b64hash)
   {
      delete []b64hash;
      b64hash = NULL;
   }
   if (suma)
   {
      delete []suma;
      suma = NULL;
   }
   return ret;
}

bool CProcParticipaciones::
recibe()
{
   bool bArriba = true;

   memset(&mensaje, 0, sizeof(MSG_LLAVE_AC));
   if( msgrcv(man_cm, &mensaje, sizeof(MSG_LLAVE_AC), 2008L, 0) != -1 ) 
   {
      memcpy(msgllave.buffer, mensaje.mensaje, TAM_DATOS);
      switch ( msgllave.tipOperacion() )
      {
         case LLAVEPLANA :
            if( indice_participacion < PARTICIPACIONES_MIN )
            {
               if( extraerParticipacion() )
               {
                  if( ++indice_participacion == PARTICIPACIONES_MIN )
                  {
                     if (!construyeSC())
                     {  
                        indice_participacion = 0;
                        bArriba = false;
                     }
                  }
               }
               else
               {
                  Bitacora->escribe(BIT_ERROR, "Ocurrio un error al extraer la participacion del mensaje recibido.");
                  respondeMsg(mensaje.pid, ERRORLLAVE);
               }
            }
            else
            {
               Bitacora->escribe(BIT_INFO, "Ya se cuenta con suficientes participaciones.");
               setError(ERR_MAXRECV, "La participación no se tomara en cuenta.");
               respondeMsg(mensaje.pid, ERRORLLAVE);
            }
            break;
         case SOLEDOLLAVE :
            if(VerificaLlaveMem( atoi(shKey), shKid, NULL, NULL))
               memcpy(llaveArriba,"1",1);
            else
               memcpy(llaveArriba,"0",1);
            respondeMsg(mensaje.pid, EDOLLAVE); 
            break;
         default :
            Bitacora->escribePV(BIT_ERROR, "No se puede manejar el mensaje %d.", msgllave.tipOperacion());
            setError(ERR_MNJOMSG, "El mensaje no es valido, no se puede procesar.");
            respondeMsg(mensaje.pid, ERRORLLAVE);
      }
   }
   return bArriba;
}

bool CProcParticipaciones::
respondeMsg(long c_pid, int mensaje)
{
   int error = 0;
   switch( mensaje )
   {
      case ACUSELLAVE :
      case ACUSEARMADO :
         error = msgllave.setMensaje(mensaje); 
         break;
      case ERRORLLAVE :
         error = msgllave.setMensaje(mensaje, descError[0], strlen(descError[0]) ,descError[1],strlen(descError[1]));
         break;
      case EDOLLAVE :
         error = msgllave.setMensaje(mensaje, llaveArriba, 1);
         break;
      default :
         Bitacora->escribePV(BIT_ERROR, "No se puede crear una respuesta con el mensaje %d.", mensaje);
         return false;
   }
   if( !error )
   {
      return responde(c_pid);
   }
   Bitacora->escribePV(BIT_ERROR, "Ocurrio un error al preparar el mensaje de respuesta %d. (%d)", mensaje, error);
   return false;
}

bool CProcParticipaciones::
responde(long c_pid)
{
   MSG_LLAVE_AC respuesta;
   memset(&respuesta, 0, sizeof(MSG_LLAVE_AC));
   respuesta.tipo = c_pid;
   memcpy(respuesta.mensaje, msgllave.buffer, msgllave.tamMensaje());
   if( msgsnd( man_cm, &respuesta, sizeof(MSG_LLAVE_AC), 0 ) != -1 ) 
   {
      Bitacora->escribePV(BIT_INFO, "Se respondio %d para el pid %d", msgllave.tipOperacion(), c_pid);
      return true;
   }
   else
      Bitacora->escribePV(BIT_ERROR, "Ocurrio un error al colocar la respuesta en la cola. (ID: %d) (%d)", man_cm, errno);
   return false;
}
/*
bool CProcParticipaciones::
construyeSC()
{
   uint8 *privada = new uint8[l_participacion/2];

   CSecretoCompartido *sc = new CSecretoCompartido(PARTICIPACIONES_MIN, PARTICIPACIONES_MIN);
   if( !sc-decodifica((uint8 **)participaciones, l_participacion, privada) )
   {
      msg_cve = ERRORLLAVE;
      setError(ERR_INTERPL, "No se pudieron interpolar las participaciones.");
      Bitacora->escribe(BIT_ERROR, "Ocurrio un error al decodificar los puntos.");
      return false;
   }
   else
   {
      Bitacora->escribe(BIT_INFO, "Se interpolaron las participaciones correctamente.");
       
   } 
   return true;
}
*/
bool CProcParticipaciones::
construyeSC()
{
   bool resp = true;
   int msg_cve = ACUSEARMADO;
   uint8 *privada = new uint8[l_participacion/2];
   uchar uVigIni[15];
   uchar uVigFin[15];
   uchar uNumSer[21];
   unsigned char *uPKCS7 = NULL;
   int   iPKCS7 = sizeof(uPKCS7);

   CSecretoCompartido *sc = new CSecretoCompartido(PARTICIPACIONES_MIN, PARTICIPACIONES_MIN);
   if( !sc->decodifica((uint8 **)participaciones, l_participacion, privada) )
   {
      msg_cve = ERRORLLAVE;
      Bitacora->escribe(BIT_ERROR, "No se puedieron interpolar las participaciones.");
      setError(ERR_INTERPL, "No se pudieron interpolar las participaciones.");
      resp = false;
   } 
   else
   { 
      Bitacora->escribe(BIT_INFO, "Se integraron las participaciones de forma correcta");
      
      #ifdef DBG
      //std::string tmp_cad = dumpBuffer("Llave: ", privada, l_participacion/2);
      //Bitacora->escribe(BIT_DEBUG, tmp_cad.c_str());
      #endif
// LIMPIAR VARIABLES ANTES D ENTRAR EN LA FUNCION VERIFICA LLAVE

      char cError[200]; int iError = sizeof(cError);
      resp =  VerificaLlave( atoi(shKey), shKid, (unsigned char*)privada, l_participacion/2,uVigIni, uVigFin, uNumSer,cError, &iError );
      if(resp)
      { 
         if (generaPKCS7( (const char*)uNumSer, ( const unsigned char*)privada,  l_participacion/2, &uPKCS7, iPKCS7) )
         {   
             if ( subeLLave(shKey, shKid, strlen(shKid), (char*)uPKCS7, iPKCS7, uVigIni, uVigFin, uNumSer ) )
             {
                resp=0 ; 
                Bitacora->escribe(BIT_INFO, "Se subio la llave correctamente.");
             }
             else
             {
                msg_cve = ERRORLLAVE;
                setError( ERR_CARGSHK,"No se subio la llave.");
                Bitacora->escribePV(BIT_ERROR, "No se subio la llave [%d].",errno);
             }
         }
         else
         {
            msg_cve = ERRORLLAVE;
            setError( ERR_CARGSHK,"Error al armar el pkcs7.");
         }

      }
      else if(iError == ERR_NO_CORR_LLAVPRIV_CERT )
      {
         msg_cve = ERRORLLAVE;
         setError( ERR_CARGSHK, "No corresponen las llaves.");
      }
      else if (iError == ERR_AL_OBTENER_DAT_CERT )
      {
         msg_cve = ERRORLLAVE;
         setError( ERR_CARGSHK,"Error al obtener datos del certificado.");
      }
      else if (iError == ERR_AL_OBTENER_DAT_LLAVE_PRIV )
      {
         msg_cve = ERRORLLAVE;
         setError( ERR_CARGSHK, "Error al obtener datos de la llave privada.");
      }
      else 
      {  
         msg_cve = ERRORLLAVE;
         setError( ERR_CARGSHK,"La llave esta en la memoria.");
      }
      
   }
   for( int i = 0; i < indice_participacion; i++ )
      resp &= respondeMsg(pids_participantes[i], msg_cve);

   if (sc)
   {
      delete sc;
      sc = NULL;
   }
   if (privada)
   {
      delete []privada;
      privada = NULL;
   }
   return resp;
}

void CProcParticipaciones::
setError(int ERROR, char *desc)
{
   sprintf(descError[0], "%d", ERROR);
   strcpy(descError[1], desc);
   Bitacora->escribePV(BIT_ERROR, "%s",desc);
}

bool CProcParticipaciones::
generaPKCS7(const char *no_serie, const unsigned char *datos, int l_datos, unsigned char **destino, int &l_destino)
{
   bool ret = false;
   int l_pwd = 256;
   //unsigned char *t_pwd = new unsigned char[l_pwd];
   unsigned char *t_pwd = (unsigned char*)calloc (l_pwd,1);
   desencripta(( uint8*)shmpw, strlen(shmpw), (uint8*)t_pwd, &l_pwd); 
   
   X509_ALGOR *pbe = PKCS5_pbe2_set(EVP_aes_256_cbc(), 1001, (unsigned char *)no_serie, 20);
   
   unsigned char *tmp_buf;
   int l_tmp_buf = 0;

   if( PKCS12_pbe_crypt( pbe, (char *)t_pwd, (int)strlen((const char *)t_pwd), (unsigned char *)datos, l_datos, &tmp_buf, &l_tmp_buf, 1) )
   {
      PKCS7 *pkcs = PKCS7_new();
      if( pkcs )
      {
         PKCS7_set_type(pkcs, NID_pkcs7_encrypted);
         X509_ALGOR_free(pkcs->d.encrypted->enc_data->algorithm);
         pkcs->d.encrypted->enc_data->algorithm = pbe;
         pkcs->d.encrypted->enc_data->enc_data = ASN1_OCTET_STRING_new(); 
         ASN1_OCTET_STRING_set(pkcs->d.encrypted->enc_data->enc_data, tmp_buf, l_tmp_buf);
         l_destino = i2d_PKCS7(pkcs, destino);
         PKCS7_free(pkcs);
         if( l_destino > 0 )
         {
            Bitacora->escribe(BIT_INFO, "PKCS7 Generado.");
            ret = true;
         }
      }
      else Bitacora->escribe(BIT_ERROR, "Error al asignar memoria al PKCS.");
   }
   else 
      Bitacora->escribe(BIT_ERROR, "Error al encriptar el PKCS7.");

   if( t_pwd )
   {
      free(t_pwd);
      t_pwd = NULL;
   }
   return ret;
} 

bool CProcParticipaciones::  
subeLLave(char* key, char *clave, int iclave, char *PKCS8, int iPKCS8, uchar* uVigIni, uchar* uVigFin, uchar* uNumSer)
{
     bool ok = true;

     CSHMemPKI *mem   = new CSHMemPKI(atoi(key), TamBufShMem);
     ServSegInfo *srv = new ServSegInfo;

     if(!mem->abre(true))
     {
       Bitacora->escribePV(BIT_ERROR, "Error al abrir segmento. errno [ %d ]",errno);
       return false;
     }
     memset(srv, 0, sizeof(struct serv_info_st));
     memcpy(srv->privKey,  PKCS8,    iPKCS8);
     memcpy(srv->clave,    clave,    iclave);
     memcpy(srv->numSerie, uNumSer,  strlen((const char*)uNumSer));
     memcpy(srv->vigIni,   uVigIni,  strlen((const char*)uVigIni));
     memcpy(srv->vigFin,   uVigFin,  strlen((const char*)uVigFin));

     ok = mem->registraServ(srv);
     if(!ok)
     {
       Bitacora->escribePV(BIT_ERROR, "Error al Registrar el servicio. errno [ %d ]",errno);
       return false;
     }

     delete mem;
     delete srv;

     return true;
}

EVP_PKEY*  CProcParticipaciones::
ObtieneDatosCert(const char* cRutaCert, uchar* uVigIni, uchar* uVigFin, uchar*  uNumSer)
{
    uchar   uCert[5000];
    SGIX509 cert;
    FILE*            fp = NULL;
    bool             ok = false;
    X509*          x509 = X509_new();
    EVP_PKEY* evpPubKey = NULL;
    void*           aux = NULL;

    fp = fopen(cRutaCert, "rb");
    if (fp)
    {
       size_t leidos = fread(uCert, 1, sizeof(uCert), fp);
       rewind(fp);
       if (uCert[0] == 0x2d)
          aux = PEM_read_X509(fp, &x509, NULL, NULL);
       else if( uCert[0] == 0x30 )
          aux = d2i_X509_fp(fp, &x509);

       if (aux)
       {
          evpPubKey = EVP_PKEY_new();
          evpPubKey = X509_get_pubkey(x509);
          if(evpPubKey != NULL)
          {
             if ( !cert.ProcCERT(uCert, leidos ,NULL,NULL, uVigIni,uVigFin, uNumSer,NULL,NULL,0))
             {
                uVigIni[13] = 0 ;
                uVigFin[13] = 0 ;
                ok = true;

             }else Bitacora->escribe(BIT_ERROR, "Error al obtener los datos del certificado.");
          }else Bitacora->escribe(BIT_ERROR, "Error al obtener  evoPubKey del certificado.");
       }else Bitacora->escribe(BIT_ERROR, "Error al identificar si el certificado es DER o PEM.");
       X509_free(x509);
       fclose(fp);
       x509 = NULL;
       fp = NULL;
    }else Bitacora->escribePV(BIT_ERROR, "Error al abrir el certificado %s [%d].",cRutaCert, errno);

    return evpPubKey;
}


EVP_PKEY*  CProcParticipaciones::
ObtieneDatosLlavePriv( const unsigned char* cLlavePriv , int iTamLlavPriv)
{
   SGI_SHK*           shk = SGI_SHK_new();
   EVP_PKEY* evpLlavePriv = NULL;//EVP_PKEY_new();
   BIO*        bLlavePriv = NULL;

   shk  = d2i_SGI_SHK(&shk, &cLlavePriv, iTamLlavPriv);
   if(shk)
   {
      bLlavePriv  = BIO_new(BIO_s_mem());
      BUF_MEM *memLlavePriv = NULL;

      if (bLlavePriv)
      {
         memLlavePriv = BUF_MEM_new();
         BUF_MEM_grow(memLlavePriv, shk->pkcs8->length);
         memcpy(memLlavePriv->data, shk->pkcs8->data, shk->pkcs8->length);
         BIO_set_mem_buf(bLlavePriv, memLlavePriv, BIO_CLOSE);
         evpLlavePriv = d2i_PKCS8PrivateKey_bio(bLlavePriv, NULL, NULL, shk->pwd->data);
         if (!evpLlavePriv)
         {
            Bitacora->escribe(BIT_ERROR, "No se pudo desencriptar la llave privada.");
            evpLlavePriv = NULL;
         }
      }else Bitacora->escribe(BIT_ERROR, "Error no se asigno memoria al BIP bLlavePriv.");
   }else Bitacora->escribe(BIT_ERROR, "No se pudo Obtener la estructura SHK de la llave privada.");

   BIO_free(bLlavePriv);
   if(shk)
      SGI_SHK_free(shk);

   return  evpLlavePriv;
}

bool CProcParticipaciones::  
VerificaLlave(int iKey ,char* idLlave,uchar* uLlaveaSub, int iLlaveaSub, uchar* uVigIni, uchar* uVigFin, uchar* uNumSer, char* cError, int* iError)
{
   EVP_PKEY* evpPubKey   = NULL;
   EVP_PKEY* evpPrivKey  = NULL;

   uchar  uLlave[1024*3];
   int    iLlave = sizeof(uLlave);
   bool   ok     = false;

   bool lect  =  VerificaLlaveMem(iKey ,idLlave, uLlave, &iLlave);
   if (!lect)
   {
      evpPubKey = ObtieneDatosCert(certAC, uVigIni,  uVigFin, uNumSer);
      if (evpPubKey != NULL)
      { 
         evpPrivKey = ObtieneDatosLlavePriv((const unsigned char*) uLlaveaSub, iLlaveaSub);
         if( evpPrivKey != NULL)
         {
            int igual = EVP_PKEY_cmp(evpPubKey, evpPrivKey);
            if(igual)
            {
              ok = true;
              Bitacora->escribe(BIT_INFO, "La llave Publica y Privada corresponden.");
            }
            else 
            {
               sprintf(cError,"%s","No corresponde la llave privada y el certificado.");
               *iError = ERR_NO_CORR_LLAVPRIV_CERT;
               Bitacora->escribePV(BIT_INFO, "%s",cError);
            }
         }
         else 
            *iError = ERR_AL_OBTENER_DAT_LLAVE_PRIV;
      }
      else
         *iError = ERR_AL_OBTENER_DAT_CERT;
   }

   return ok;                                                     
}

bool CProcParticipaciones::
VerificaLlaveMem(int iKey ,char* idLlave, unsigned char* uLlave, int *iLlave)
{
   CSHMemPKI *mem   = new CSHMemPKI(iKey, TamBufShMem);
   ServSegInfo info;
   bool ok = false;

   if (mem != NULL)
   {
      if (mem->abre(false))
      {
         int posServ  = mem->findServ(idLlave);
         if (posServ >= 0 )
         {
           if (posServ != 255)
           {  
              info  = mem->getServInfo(posServ);
              if( (uLlave != NULL) && (iLlave != NULL))
              {  
                 *iLlave  =  ((uint16(info.privKey[2]) << 8) | uint16(info.privKey[3]));
                 memcpy( uLlave,  info.privKey , *iLlave + 4 );
              }
              ok = true;
           }else Bitacora->escribe(BIT_INFO, "No hay ninguna LLave cargada en este segmento de memoria.");
         }else Bitacora->escribe(BIT_INFO, "No hay ninguna LLave cargada en este segmento de memoria.");
      }else Bitacora->escribePV(BIT_INFO, "Al abrir la memoria compartida. %d",errno);
   }else Bitacora->escribe(BIT_ERROR, "No se asigno memoria a la variable mem."); 
   if(mem)
      delete mem;
   Bitacora->escribePV(BIT_INFO, "Llave cargada en memoria. %d",ok);
   return ok;
}
