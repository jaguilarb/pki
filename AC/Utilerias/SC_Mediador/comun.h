#ifndef _COMUN_H_
#define _COMUN_H_

static const char* _COMUN_H_VERSION_ ATR_USED = "SC_Mediador @(#)"\
"DSIC09365SLD 2007-12-13 comun.h 1.1.0/0";

//#VERSION: 1.1.0

//static int man_cm;
//static MensajesLLavPrivAC msgllave;

//#define M_SIZE (4096 - (sizeof(int)*2) - sizeof(long))
//#define HEADER_LENGTH 4

#define ERR_PARTDUP   1
#define ERR_INTERPL   2
#define ERR_B64DECO   3
#define ERR_MAXRECV   4
#define ERR_MNJOMSG   5
#define ERR_CARGSHK   6


/*typedef struct 
{
  long tipo;
  int pid;
  int p_len;
  unsigned char participacion[M_SIZE];
} MENSAJE_SC; */

typedef struct
{
  long tipo;
  int pid;
  uint8 mensaje[TAM_DATOS]; 
} MSG_LLAVE_AC;


#endif
