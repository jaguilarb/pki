#ifndef _CPROCPARTICIPACIONES_H_
#define _CPROCPARTICIPACIONES_H_
static const char* _CPROCPARTICIPACIONES_H_VERSION_ ATR_USED = "SC_Mediador @(#)"\
"DSIC09365SLD 2007-12-13 CProcParticipaciones.h 1.1.0/0";

//#VERSION: 1.1.0

#include <Sgi_SecretoCompartido.h>
#include <Sgi_MemCompartida.h>
#include <Sgi_ProtegePwd.h>
#include <Sgi_Demonio.h>
#include <Sgi_MsgPKI.h>
#include <sys/select.h>
#include <sys/stat.h>
#include <sys/msg.h>
#include <fcntl.h>
#include "comun.h"
#include <openssl/pkcs7.h>
#include <openssl/x509.h>
#include <openssl/rand.h>
#include <openssl/evp.h>
#include <openssl/md5.h>


#define ERR_NO_CORR_LLAVPRIV_CERT       -1
#define ERR_AL_OBTENER_DAT_CERT          -2
#define ERR_AL_OBTENER_DAT_LLAVE_PRIV    -3 

class CProcParticipaciones
{
   private :
      int indice_participacion, man_cm, l_participacion, PARTICIPACIONES_MIN;
      //uint8 *privada;
      int *pids_participantes;
      unsigned char **participaciones;
      unsigned char **hashes;
      char llaveArriba[1];
      char *descError[2];
      char *shKid;
      char *shKey;
      char *shmpw;
      char *certAC;
      MensajesLLavPrivAC msgllave;
      MSG_LLAVE_AC mensaje;
 
      bool responde(long c_pid);
      bool respondeMsg(long c_pid, int mensaje);
      bool extraerParticipacion();
      bool verificaHash(const unsigned char *llave, const int len);
      void setError(int, char *);
      bool construyeSC();
      bool subeLLave(char* key, char *clave, int iclave, char *PKCS8, int iPKCS8,uchar* uVigIni, uchar* uVigFin, uchar* uNumSer );
      bool generaPKCS7(const char *pwd, const unsigned char *datos, int l_datos, unsigned char **destino, int &l_destino);
      bool generaPassword(unsigned char **destino);
      EVP_PKEY* ObtieneDatosLlavePriv( const unsigned char* cLlavePriv, int iTamLlavPriv);
      EVP_PKEY* ObtieneDatosCert(const char* cRutaCert, uchar* uVigIni, uchar* uVigFin, uchar*  uNumSer);
      bool VerificaLlaveMem(int iKey ,char* idLlave, unsigned char* uLlave, int *iLlave);
      bool VerificaLlave(int iKey,char* idLlave,unsigned char* uLlavaSub,int iLlavaSub,uchar* uVigIni,uchar* uVigFin,uchar*uNumSer,char* cError,int*iError);


   public :
      CProcParticipaciones(char *cer, char *key, char *pwd, char* certac, char* shkid, char* shkey, char *shmpw, int mqid, int p_min); 
      ~CProcParticipaciones();
      bool recibe();
};

#endif
