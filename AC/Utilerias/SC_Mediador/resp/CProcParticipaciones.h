#ifndef _CPROCPARTICIPACIONES_H_
#define _CPROCPARTICIPACIONES_H_
static const char* _SGI_BD_071213_CPP_VERSION_ ATR_USED = "Sgi_BD @(#)"\
"DSIC09041AC_ 2009-01-27 Sgi_BD_071213.cpp 1.1.1/?";

static const char* _SGI_BD_071213_CPP_VERSION_ ATR_USED = "Sgi_BD @(#)"\
"DSIC09041AC_ 2009-01-27 Sgi_BD_071213.cpp 1.1.1/?";

//#VERSION: 1.1.0

#include <Sgi_SecretoCompartido.h>
#include <Sgi_MemCompartida.h>
#include <Sgi_ProtegePwd.h>
#include <Sgi_Demonio.h>
#include <Sgi_MsgPKI.h>
#include <sys/select.h>
#include <sys/stat.h>
#include <sys/msg.h>
#include <fcntl.h>
#include "comun.h"
#include <openssl/pkcs7.h>
#include <openssl/x509.h>
#include <openssl/rand.h>
#include <openssl/evp.h>

class CProcParticipaciones
{
   private :
      int indice_participacion, man_cm, l_participacion, PARTICIPACIONES_MIN;
      //uint8 *privada;
      int *pids_participantes;
      unsigned char **participaciones;
      char *descError[2];
      char *shKid;
      char *shKey;
      char *shmpw;
      char *certAC;
      MensajesLLavPrivAC msgllave;
      MSG_LLAVE_AC mensaje;
 
      bool responde(long c_pid);
      bool respondeMsg(long c_pid, int mensaje);
      bool extraerParticipacion();
      void setError(int, char *);
      bool construyeSC();
      bool subeLLave(char* key, char *clave, int iclave, char *PKCS8, int iPKCS8,uchar* uVigIni, uchar* uNumSer );
      bool generaPKCS7(const char *pwd, const unsigned char *datos, int l_datos, unsigned char **destino, int &l_destino);
      bool generaPassword(unsigned char **destino);
      bool ObtieneDatosLlavePriv( const unsigned char* cLlavePriv, int iTamLlavPriv, EVP_PKEY*& evpLlavePriv);
      bool VerDatosLlave(const unsigned char* cLlave, int iTamLlave, uchar *uVigIni, uchar *uNumSer);
      bool ObtieneDatosCert(const char* cRutaCert, EVP_PKEY*  evpPubKey, uchar* uVigIni, uchar*  uNumSer);


   public :
      CProcParticipaciones(char *cer, char *key, char *pwd, char* certac, char* shkid, char* shkey, char *shmpw, int mqid, int p_min); 
      ~CProcParticipaciones();
      bool recibe();
};

#endif
