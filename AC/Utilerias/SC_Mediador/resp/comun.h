#ifndef _COMUN_H_
#define _COMUN_H_
static const char* _SGI_BD_071213_CPP_VERSION_ ATR_USED = "Sgi_BD @(#)"\
"DSIC09041AC_ 2009-01-27 Sgi_BD_071213.cpp 1.1.1/?";

static const char* _SGI_BD_071213_CPP_VERSION_ ATR_USED = "Sgi_BD @(#)"\
"DSIC09041AC_ 2009-01-27 Sgi_BD_071213.cpp 1.1.1/?";

//#VERSION: 1.1.0

//static int man_cm;
//static MensajesLLavPrivAC msgllave;

//#define M_SIZE (4096 - (sizeof(int)*2) - sizeof(long))
//#define HEADER_LENGTH 4

#define ERR_PARTDUP   1
#define ERR_INTERPL   2
#define ERR_B64DECO   3
#define ERR_MAXRECV   4
#define ERR_MNJOMSG   5
#define ERR_CARGSHK   6


/*typedef struct 
{
  long tipo;
  int pid;
  int p_len;
  unsigned char participacion[M_SIZE];
} MENSAJE_SC; */

typedef struct
{
  long tipo;
  int pid;
  uint8 mensaje[TAM_DATOS]; 
} MSG_LLAVE_AC;


#endif
