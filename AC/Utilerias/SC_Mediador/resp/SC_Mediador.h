static const char* _SGI_BD_071213_CPP_VERSION_ ATR_USED = "Sgi_BD @(#)"\
"DSI_08341BAS 2007-12-13 Sgi_BD_071213.cpp 1.1.1/?";

static const char* _SGI_BD_071213_CPP_VERSION_ ATR_USED = "Sgi_BD @(#)"\
"DSI_08341BAS 2007-12-13 Sgi_BD_071213.cpp 1.1.1/?";

static const char* _SGI_BD_071213_CPP_VERSION_ ATR_USED = "Sgi_BD @(#)"\
"DSI_08341BAS 2007-12-13 Sgi_BD_071213.cpp 1.1.1/?";

static const char* _SGI_BD_071213_CPP_VERSION_ ATR_USED = "Sgi_BD @(#)"\
"DSI_08341BAS 2007-12-13 Sgi_BD_071213.cpp 1.1.1/?";

static const char* _SGI_BD_071213_CPP_VERSION_ ATR_USED = "Sgi_BD @(#)"\
"DSI_08341BAS 2007-12-13 Sgi_BD_071213.cpp 1.1.1/?";

//#VERSION: 1.1.0
#ifndef _CSC_MEDIADOR_
#define _CSC_MEDIADOR_

#ifdef DBG
   #define PATH_CFG "/usr/local/SAT/PKI/etc/SC_Mediador_dbg.cfg"
   #define PATH_LOG "/var/local/SAT/PKI/SC_Mediador_dbg.log"
#else
   #define PATH_CFG "/usr/local/SAT/PKI/etc/SC_Mediador.cfg"
   #define PATH_LOG "/var/local/SAT/PKI/SC_Mediador.log"
#endif

#include "CProcParticipaciones.h"
#include <Sgi_ProtegePwd.h>
#include <Sgi_ConfigFile.h>
#include <Sgi_Demonio.h>
#include <Sgi_MsgPKI.h>
#include <sys/msg.h>
#include "comun.h"
#include <string>

#define MQ_TAMAX 4096 * 64;

class CSC_Mediador : public CDemonio
{

   private :
      int mq_id, man_cm, PARTICIPACIONES_MIN;
      CConfigFile *config;
      std::string passwd, cer, key, temporal, s_participaciones, shkid, shkey, shkpas, certAC;
      virtual bool Inicia ();
      virtual bool Termina();
      virtual bool setManejadorSignals();
      virtual bool CargaCfg();
      static void ManejadorSenales(int sig);
      bool iniciaCola();
      char *desencriptaPwdKey();
      bool creaFifos();
   
   public :
      CSC_Mediador();
      bool Proceso();
      virtual ~CSC_Mediador();

};

#endif
