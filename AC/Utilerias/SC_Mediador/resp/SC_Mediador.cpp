static const char* _SGI_BD_071213_CPP_VERSION_ ATR_USED = "Sgi_BD @(#)"\
"DSIC09041AC_ 2009-01-27 Sgi_BD_071213.cpp 1.1.1/?";

static const char* _SGI_BD_071213_CPP_VERSION_ ATR_USED = "Sgi_BD @(#)"\
"DSIC09041AC_ 2009-01-27 Sgi_BD_071213.cpp 1.1.1/?";

//#VERSION: 1.1.0
#include "SC_Mediador.h"
#include <signal.h>

CSC_Mediador::
CSC_Mediador()
{
   config = new CConfigFile(PATH_CFG);
}

bool CSC_Mediador::
Inicia()
{
   int error;
   if( !config )
      Bitacora->escribe(BIT_ERROR, "No se ha podido iniciar la configuración del servicio.");
   else if( (error = config->cargaCfgVars()) != 0 )
      Bitacora->escribe(BIT_ERROR, "No se pudieron cargar las variables de configuración.");
   else
      return (CargaCfg() && iniciaCola());
   return false;
}

bool CSC_Mediador::
creaFifos()
{
   return true;
}

bool CSC_Mediador::
iniciaCola()
{
   man_cm = msgget(mq_id, IPC_CREAT | 0600);
   msqid_ds cola;
   if(man_cm >= 0)
   {
      msgctl(man_cm, IPC_STAT, &cola);
      /*cola.msg_qbytes = MQ_TAMAX;
      if( msgctl(man_cm, IPC_SET, &cola) != -1 )
      {
         Bitacora->escribePV(BIT_INFO, "La cola de mensajes esta lista. (ID: %d).", man_cm);
         return true;
      }
      Bitacora->escribePV(BIT_ERROR, "No se pudo inicializar la cola de mensajes (%d).", errno);
      */
      return true;
   }
   else 
      Bitacora->escribePV(BIT_ERROR, "No se pudo crear la cola de mensajes %d. (%d)", mq_id, errno);
   return false;
}

char *CSC_Mediador::
desencriptaPwdKey()
{
   int pwd_buf_l = 256;
   uint8 *pwd_buf = (uint8 *)calloc(256, sizeof(uint8));
   desencripta((const uint8 *)passwd.c_str(), passwd.length(), pwd_buf, &pwd_buf_l);
   return (char *)pwd_buf;
}

bool CSC_Mediador::
Termina()
{
   return true;
}

void CSC_Mediador::
ManejadorSenales(int sig)
{
   switch ( sig )
   {
      case SIGINT :
         break;
      case SIGHUP :
         break;
   } 
}

bool CSC_Mediador::
setManejadorSignals()
{
   struct sigaction acciones;
   memset(&acciones, 0, sizeof(acciones));
   
   acciones.sa_handler = ManejadorSenales;
   acciones.sa_flags   = SA_SIGINFO;

   if( sigaction(SIGINT, &acciones, NULL) ||
       sigaction(SIGHUP, &acciones, NULL) )
   {
      Bitacora->escribePV(BIT_ERROR, "No se han podido establecer los manejadores de señales. (%d)", errno);
      return false;
   }
   return true;
}

bool CSC_Mediador::
CargaCfg()
{
   bool errores = false;
   if ( config->getValorVar("[SC_MEDIADOR]", "MQID", &temporal) )
   {
      Bitacora->escribe(BIT_ERROR, "No se pudo cargar la variable MQID del archivo de configuracion"); 
      errores = true;
   }
   else
      mq_id = atoi(temporal.c_str());   
   if ( config->getValorVar("[SC_MEDIADOR]", "CERTF", &cer) )
   {
      Bitacora->escribe(BIT_ERROR, "No se pudo cargar la variable CERTF del archivo de configuracion"); 
      errores = true;
   }
   if ( config->getValorVar("[SC_MEDIADOR]", "CRKEY", &key) )
   {
      Bitacora->escribe(BIT_ERROR, "No se pudo cargar la variable LLAVE del archivo de configuracion");
      errores = true;
   }
   if ( config->getValorVar("[SC_MEDIADOR]", "PASSW", &passwd) )
   {
      Bitacora->escribe(BIT_ERROR, "No se pudo cargar la variable PASSW del archivo de configuracion");
      errores = true;
   }
   if ( config->getValorVar("[SC_MEDIADOR]", "P_REQ", &s_participaciones) )
   {
      Bitacora->escribe(BIT_ERROR, "No se pudo cargar la variable P_REQ del archivo de configuración");
      errores = true;
   }
   if ( config->getValorVar("[SC_MEDIADOR]", "SHKID", &shkid) )
   {
      Bitacora->escribe(BIT_ERROR, "No se pudo cargar la variable SHKID del archivo de configuración");
      errores = true;
   }
   if ( config->getValorVar("[SC_MEDIADOR]", "SHKEY", &shkey) )
   {
      Bitacora->escribe(BIT_ERROR, "No se pudo cargar la variable SHKEY del archivo de configuración");
      errores = true;
   }
   if ( config->getValorVar("[SC_MEDIADOR]", "SHKPW", &shkpas) )
   {
      Bitacora->escribe(BIT_ERROR, "No se pudo cargar la variable SHKPW del archivo de configuración");
      errores = true; 
   }
   if ( config->getValorVar("[SC_MEDIADOR]", "CERTAC", &certAC) )
   {
      Bitacora->escribe(BIT_ERROR, "No se pudo cargar la variable CERTAC del archivo de configuración");
      errores = true;
   }

   else
   {
      PARTICIPACIONES_MIN = atoi(s_participaciones.c_str()); 
      Bitacora->escribePV(BIT_INFO, "Se ha configurado el mediador para construir con %d participaciones", PARTICIPACIONES_MIN);
   } 
   return errores ? false : true;
}

CSC_Mediador::
~CSC_Mediador()
{
}

bool CSC_Mediador::
Proceso()
{
   CProcParticipaciones *participaciones = 
      new CProcParticipaciones(
         (char *)cer.c_str(), (char *)key.c_str(), desencriptaPwdKey(), (char*)certAC.c_str(), (char *)shkid.c_str(), 
         (char *)shkey.c_str(), (char *)shkpas.c_str(), man_cm, PARTICIPACIONES_MIN);
   if( participaciones )
   {
      while( participaciones->recibe() );
   }
   return false;
}

CBitacora*
Crea_CBitacora()
{
  CBitacora *bitacora = new CBitacora(PATH_LOG);
  if( bitacora )
  {
     #ifdef DBG
     bitacora->setNivel(BIT_DEBUG);
     #endif
     bitacora->escribe(BIT_INFO, "Inicio de proceso");
     #ifdef DBG
        sleep(20);
     #endif
     return bitacora;
  }
  return NULL;
}

CDemonio*
Crea_CDemonio()
{
   return new CSC_Mediador();
}

const char *
getNombreApl()
{
   return "SC_Mediador";
}
