static const char* _SGI_BD_071213_CPP_VERSION_ ATR_USED = "Sgi_BD @(#)"\
"DSIC09041AC_ 2009-01-27 Sgi_BD_071213.cpp 1.1.1/?";

static const char* _SGI_BD_071213_CPP_VERSION_ ATR_USED = "Sgi_BD @(#)"\
"DSIC09041AC_ 2009-01-27 Sgi_BD_071213.cpp 1.1.1/?";

//#VERSION: 1.1.0
#include "CProcParticipaciones.h"
#ifdef DBG
#include <openssl/md5.h>
#endif //DBG

#define _SHK_CPP_
#include <shk.h>

CProcParticipaciones::
CProcParticipaciones(char *cer, char *key, char *pwd,char* certac, char* shkid, char* shkey, char *shmpass, int mqid, int p_min) :
   indice_participacion(0),
   l_participacion(3000),
   man_cm(mqid),
   PARTICIPACIONES_MIN(p_min),
   shKid(shkid),
   shKey(shkey),
   shmpw(shmpass),
   certAC(certac)
{
   participaciones = new unsigned char*[PARTICIPACIONES_MIN];
   pids_participantes = new int[PARTICIPACIONES_MIN];
   descError[0] = (char *)calloc(2, sizeof(char));
   descError[1] = (char *)calloc(512, sizeof(char));
   for( int i =0; i< PARTICIPACIONES_MIN; i++) 
      participaciones[i] = (unsigned char *)calloc(l_participacion, sizeof(char));
   //msgllave = new MensajesLLavPrivAC();
   //if( msgllave )
   //{
      OpenSSL_add_all_algorithms();
      int error = msgllave.Inicia(0, cer, key, pwd, strlen(pwd));
      if( error )
         Bitacora->escribePV(BIT_ERROR, "No se pudo iniciar la biblioteca de mensajes (%d)", error);
   //}
}

CProcParticipaciones::
~CProcParticipaciones()
{
}

bool CProcParticipaciones::
extraerParticipacion()
{
   int l_b64llave = msgllave.tamMensaje() - sizeof(Encabezado);
   char *b64llave = (char *)calloc(l_b64llave, sizeof(char));
   msgllave.getMensaje(LLAVEPLANA, b64llave, &l_b64llave);
   uint16 l_part = B64_TamCadDecodificada((uint16)l_b64llave);  
   participaciones[indice_participacion] = (unsigned char *)calloc(l_part, sizeof(char));
   pids_participantes[indice_participacion] = mensaje.pid;
   if( B64_Decodifica(b64llave, l_b64llave, participaciones[indice_participacion], (uint16 *)&l_part) )
   { 
      Bitacora->escribe(BIT_INFO, "Se recibio un participacion.");
      l_participacion = l_part;
      #ifdef DBG
         std::string tmp = dumpBuffer("mensaje", participaciones[indice_participacion], l_part);
         Bitacora->escribe(BIT_DEBUG, tmp.c_str());
      #endif
      return respondeMsg(mensaje.pid, ACUSELLAVE);
   }
   setError(ERR_B64DECO, "No se pudo decodificar la participacion en B64.");
   return false;
}

bool CProcParticipaciones::
recibe()
{
   memset(&mensaje, 0, sizeof(MSG_LLAVE_AC));
   if( msgrcv(man_cm, &mensaje, sizeof(MSG_LLAVE_AC), 2008L, 0) != -1 ) 
   {
      memcpy(msgllave.buffer, mensaje.mensaje, TAM_DATOS);
      switch ( msgllave.tipOperacion() )
      {
         case LLAVEPLANA :
            if( indice_participacion < PARTICIPACIONES_MIN )
            {
               if( extraerParticipacion() )
               {
                  if( ++indice_participacion == PARTICIPACIONES_MIN )
                     construyeSC();
               }
               else
               {
                  Bitacora->escribe(BIT_ERROR, "Ocurrio un error al extraer la participacion del mensaje recibido.");
                  respondeMsg(mensaje.pid, ERRORLLAVE);
               }
            }
            else
            {
               Bitacora->escribe(BIT_INFO, "Ya se cuenta con suficientes participaciones.");
               setError(ERR_MAXRECV, "La participación no se tomara en cuenta.");
               respondeMsg(mensaje.pid, ERRORLLAVE);
            }
            break;
         case SOLEDOLLAVE :
            respondeMsg(mensaje.pid, EDOLLAVE); 
            break;
         default :
            Bitacora->escribePV(BIT_ERROR, "No se puede manejar el mensaje %d.", msgllave.tipOperacion());
            setError(ERR_MNJOMSG, "El mensaje no es valido, no se puede procesar.");
            respondeMsg(mensaje.pid, ERRORLLAVE);
      }
   }
   return true;
}

bool CProcParticipaciones::
respondeMsg(long c_pid, int mensaje)
{
   int error = 0;
   switch( mensaje )
   {
      case ACUSELLAVE :
      case ACUSEARMADO :
         error = msgllave.setMensaje(mensaje); 
         break;
      case ERRORLLAVE :
         error = msgllave.setMensaje(mensaje, descError[0], strlen(descError[0]) ,descError[1],strlen(descError[1]));
         break;
      case EDOLLAVE :
         error = msgllave.setMensaje(mensaje, "0", 1);
         break;
      default :
         Bitacora->escribePV(BIT_ERROR, "No se puede crear una respuesta con el mensaje %d.", mensaje);
         return false;
   }
   if( !error )
   {
      return responde(c_pid);
   }
   Bitacora->escribePV(BIT_ERROR, "Ocurrio un error al preparar el mensaje de respuesta %d. (%d)", mensaje, error);
   return false;
}

bool CProcParticipaciones::
responde(long c_pid)
{
   MSG_LLAVE_AC respuesta;
   memset(&respuesta, 0, sizeof(MSG_LLAVE_AC));
   respuesta.tipo = c_pid;
   memcpy(respuesta.mensaje, msgllave.buffer, msgllave.tamMensaje());
   if( msgsnd( man_cm, &respuesta, sizeof(MSG_LLAVE_AC), 0 ) != -1 ) 
   {
      Bitacora->escribePV(BIT_INFO, "Se respondio %d para el pid %d", msgllave.tipOperacion(), c_pid);
      return true;
   }
   else
      Bitacora->escribePV(BIT_ERROR, "Ocurrio un error al colocar la respuesta en la cola. (ID: %d) (%d)", man_cm, errno);
   return false;
}

bool CProcParticipaciones::
construyeSC()
{
   bool resp = true;
   int msg_cve = ACUSEARMADO;
   uint8 *privada = new uint8[l_participacion/2];
   uchar uVigIni[15];
   uchar uNumSer[21];
   unsigned char *uPKCS7 = NULL;
   int   iPKCS7 = sizeof(uPKCS7);

   CSecretoCompartido *sc = new CSecretoCompartido(PARTICIPACIONES_MIN, PARTICIPACIONES_MIN);
   if( !sc->decodifica((uint8 **)participaciones, l_participacion, privada) )
   {
      msg_cve = ERRORLLAVE;
      Bitacora->escribe(BIT_ERROR, "No se puedieron integrar las participaciones.");
      setError(ERR_INTERPL, "No se pudieron interpolar las participaciones.");
   } 
   else
   { 
      Bitacora->escribe(BIT_INFO, "Se integraron las participaciones de forma correcta");
      
      #ifdef DBG
      std::string tmp_cad = dumpBuffer("Llave: ", privada, l_participacion/2);
      Bitacora->escribe(BIT_DEBUG, tmp_cad.c_str());
      #endif

      resp =  VerDatosLlave((const unsigned char*)privada, l_participacion/2, uVigIni, uNumSer);
      if(resp)
      { 
         if (generaPKCS7( (const char*)uNumSer, ( const unsigned char*)privada,  l_participacion/2, &uPKCS7, iPKCS7))
         {   if (subeLLave(shKey, shKid, strlen(shKid), (char*)uPKCS7, iPKCS7, uVigIni, uNumSer ) )
             {
                resp=0 ; 
                Bitacora->escribe(BIT_INFO, "Se subio la llave correctamente.");
             }
         }
      }
      else
      {  
         msg_cve = ERRORLLAVE;
         setError( ERR_CARGSHK,"No se subió la llave.");
      }
      
   }
   for( int i = 0; i < indice_participacion; i++ )
      resp &= respondeMsg(pids_participantes[i], msg_cve);
   return resp;
}

void CProcParticipaciones::
setError(int ERROR, char *desc)
{
   sprintf(descError[0], "%d", ERROR);
   strcpy(descError[1], desc);
}

bool CProcParticipaciones::
generaPKCS7(const char *no_serie, const unsigned char *datos, int l_datos, unsigned char **destino, int &l_destino)
{
   int l_pwd = 256;
   unsigned char *t_pwd = new unsigned char[l_pwd];
   desencripta(( uint8*)shmpw, strlen(shmpw), (uint8*)t_pwd, &l_pwd); 
   
   X509_ALGOR *pbe = PKCS5_pbe2_set(EVP_aes_256_cbc(), 1001, (unsigned char *)no_serie, 20);
   
   unsigned char *tmp_buf;
   int l_tmp_buf = 0;

   if( PKCS12_pbe_crypt( pbe, (char *)t_pwd, (int)strlen((const char *)t_pwd), (unsigned char *)datos, l_datos, &tmp_buf, &l_tmp_buf, 1) )
   {
      PKCS7 *pkcs = PKCS7_new();
      if( pkcs )
      {
         PKCS7_set_type(pkcs, NID_pkcs7_encrypted);
         X509_ALGOR_free(pkcs->d.encrypted->enc_data->algorithm);
         pkcs->d.encrypted->enc_data->algorithm = pbe;
         pkcs->d.encrypted->enc_data->enc_data = ASN1_OCTET_STRING_new(); 
         ASN1_OCTET_STRING_set(pkcs->d.encrypted->enc_data->enc_data, tmp_buf, l_tmp_buf);
         l_destino = i2d_PKCS7(pkcs, destino);
         PKCS7_free(pkcs);
         if( l_destino > 0 )
            return true;
      }
   }
   return false;
} 

bool CProcParticipaciones::  
subeLLave(char* key, char *clave, int iclave, char *PKCS8, int iPKCS8, uchar* uVigIni, uchar* uNumSer)
{
     bool ok = true;

     CSHMemPKI *mem   = new CSHMemPKI(atoi(key), TamBufShMem);
     ServSegInfo *srv = new ServSegInfo;

     if(!mem->abre(true))
     {
       Bitacora->escribePV(BIT_ERROR, "Error al abrir segmento. errrno [ %d ]",errno);
       return false;
     }
     memset(srv, 0, sizeof(struct serv_info_st));
     memcpy(srv->privKey,  PKCS8,    iPKCS8);
     memcpy(srv->clave,    clave,    iclave);
     memcpy(srv->numSerie, uNumSer,  strlen((const char*)uNumSer));
     memcpy(srv->vigIni,   uVigIni,  strlen((const char*)uVigIni));

     ok = mem->registraServ(srv);
     if(!ok)
     {
       Bitacora->escribePV(BIT_ERROR, "Error al Registrar el servicio. errrno [ %d ]",errno);
       return false;
     }

     delete mem;
     delete srv;

     return true;
}

bool CProcParticipaciones::
ObtieneDatosCert(const char* cRutaCert, EVP_PKEY*  evpPubKey, uchar* uVigIni, uchar*  uNumSer)
{
    FILE*   fp = NULL;
    bool    ok = false;
    uchar   uCert[2048];
    SGIX509 cert;
    X509*          x509  = X509_new();
    void*            aux = NULL;

    fp    = fopen(cRutaCert,"r");
    if (fp)
    {
       size_t leidos = fread(uCert, 1, sizeof(uCert), fp);
       rewind(fp);
       if (uCert[0] == 0x2d)
          aux = PEM_read_X509(fp, &x509, NULL, NULL);
       else if( uCert[0] == 0x30 )
          aux = d2i_X509_fp(fp, &x509);

       if (aux)
       {
          evpPubKey = X509_get_pubkey(x509);
          if(evpPubKey != NULL)
          {
             if ( !cert.ProcCERT(uCert, leidos ,NULL,NULL, uVigIni, NULL,uNumSer,NULL,NULL,0))
             {
                uVigIni[13] = 0 ;
                ok = true;
             }
          }
       }
       X509_free(x509);
       fclose(fp);
    }

    return ok;
}

bool CProcParticipaciones::
ObtieneDatosLlavePriv( const unsigned char* cLlavePriv, int iTamLlavPriv, EVP_PKEY*& evpLlavePriv)
{
   SGI_SHK*       shk = SGI_SHK_new();
   BIO*    bLlavePriv = NULL;
   bool            ok = false;

   shk  = d2i_SGI_SHK(&shk, &cLlavePriv, iTamLlavPriv);
   if(shk)
   {
      bLlavePriv  = BIO_new(BIO_s_mem());
      BUF_MEM *memLlavePriv;

      if (bLlavePriv)
      {

         memLlavePriv = BUF_MEM_new();
         BUF_MEM_grow(memLlavePriv, shk->pkcs8->length);
         memcpy(memLlavePriv->data, shk->pkcs8->data, shk->pkcs8->length);
         BIO_set_mem_buf(bLlavePriv, memLlavePriv, BIO_CLOSE);
         evpLlavePriv = d2i_PKCS8PrivateKey_bio(bLlavePriv, &evpLlavePriv, NULL, shk->pwd->data);
         if (evpLlavePriv)
            ok =  true;
      }
   }
   BIO_free(bLlavePriv);
   if(shk)
      SGI_SHK_free(shk);
   return ok;
}

bool CProcParticipaciones::
VerDatosLlave(const unsigned char* cLlave, int iTamLlave, uchar *uVigIni, uchar *uNumSer)
{
    EVP_PKEY  evpPubKey;
    EVP_PKEY  *evpLlavePriv;
    bool      resp = false;    
 
    resp =  ObtieneDatosCert(certAC, &evpPubKey, uVigIni ,uNumSer);
    resp =  resp && ObtieneDatosLlavePriv(cLlave, iTamLlave, evpLlavePriv );

    if(resp)
      resp = EVP_PKEY_cmp(&evpPubKey, evpLlavePriv);

    return resp;

}
