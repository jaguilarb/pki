static const char* _GLOGFILE_CPP_VERSION_ ATR_USED = "ArchivoBitacora @(#)"\
"DSIC07412AC_ 2007-12-14 gLogFile.cpp 1.1.0/0";

//#VERSION: 1.1.0
#include <gLogFile.h>

/* ----------------------------------------------------- */
void GLogFile::
Gen(int cderr, int pos, char *file, int line)
{
   struct stat filei;
   if(stat(pathlog, &filei) != 0)
      fileP.open(pathlog, ios::out);
   else fileP.open(pathlog, ios::out | ios::app);
   if(fileP.fail())
      return;
   switch(pos)
   {
      case 0:
      {
         sprintf(mensaje, "Error # (%d) en funcion de Mensaje. Archivo: %s ; Linea: %d ", cderr, file, line);
         break;
      }
      case 1:
      {
         sprintf(mensaje, "Error # (%d) funcion de OpenSSL. Archivo: %s ; Linea: %d ", cderr, file, line);
         break;
      }
      case 2:
      {
         sprintf(mensaje, "Error # (%d) en funcion de la AC. Archivo: %s ; Linea: %d ", cderr, file, line);
         break;
      }
      case 3:
      {
         sprintf(mensaje, "Error # (%d) en Socket. Archivo: %s ; Linea: %d ", cderr, file, line);
         break;
      }
      case 4:
      {
         sprintf(mensaje, "Error # (%d) en BD. Archivo: %s ; Linea: %d ", cderr, file, line);
         break;
      }
   }
   tm = time(NULL);
   strcat(mensaje, ctime(&tm));
   fileP.write(mensaje, strlen(mensaje));
   fileP.close();
}
/* ----------------------------------------------------- */
void GLogFile::
seg(char *msg)
{
   struct stat file;
   if(stat(pathlog, &file) != 0)
      fileP.open(pathlog, ios::out | ios::ate | ios::app);
   else fileP.open(pathlog, ios::out | ios::app);
   tm = time(NULL);
   strcpy(mensaje, msg);
   strcat(mensaje, " ");
   strcat(mensaje, ctime(&tm));
   fileP.write(mensaje, strlen(mensaje));
   fileP.close();
}
/* ----------------------------------------------------- */
void GLogFile::
comentario(char *msg)
{
   struct stat file;
   if(stat(pathlog, &file) != 0)
      fileP.open(pathlog, ios::out | ios::ate | ios::app);
   else fileP.open(pathlog, ios::out | ios::app);
   strcpy(mensaje, msg);
   strcat(mensaje, "\n");
   fileP.write(mensaje, strlen(mensaje));
   fileP.close();
}
/* ----------------------------------------------------- */
void GLogFile::
armaMsg(char *s_Cadena, ...)
{
   int i = 0;
   va_list ap;
   va_start(ap, s_Cadena);
   vsprintf(mensaje, s_Cadena, ap);
   strcat(mensaje, "\n");
   struct stat file;
   if(stat(pathlog, &file) != 0)
      fileP.open(pathlog, ios::out | ios::ate | ios::app);
   else fileP.open(pathlog, ios::out | ios::app);
   fileP.write(mensaje, strlen(mensaje));
   fileP.close();   
}
/* ----------------------------------------------------- */
GLogFile::GLogFile(char *PathLog)
{
   strcpy(pathlog, PathLog);
}
/* ----------------------------------------------------- */
GLogFile::~GLogFile()
{
}
/* ----------------------------------------------------- */
/* ----------------------------------------------------- */
/* ----------------------------------------------------- */

