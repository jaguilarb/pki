#ifndef _GLOGFILE_H_
#define _GLOGFILE_H_
static const char* _GLOGFILE_H_VERSION_ ATR_USED = "ArchivoBitacora @(#)"\
"DSIC07412AC_ 2007-12-14 gLogFile.h 1.1.0/0";

//#VERSION: 1.1.0
#include <SgiOpenSSL.h>
#include <iostream.h>
#include <stdio.h>

/* ********************************************** */
class GLogFile
{
   protected:
      fstream fileP;
      time_t tm;
      char mensaje[1024 * 2];
      char pathlog[256];
   public:
      GLogFile(char *PathLog);
      ~GLogFile();
      void Gen(int cderr, int pos, char *file, int line);
      void seg(char *msg);
      void comentario(char *msg);
      void armaMsg(char *s_Cadena, ...); 
};
/* ********************************************** */
#endif
