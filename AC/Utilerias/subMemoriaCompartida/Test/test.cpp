static const char* _TEST_CPP_VERSION_ ATR_USED = "Test @(#)"\
"DSIC07412AC_ 2007-12-14 test.cpp 1.1.0/0";

//#VERSION: 1.1.0
#include <SgiOpenSSL.h>
#include <iostream.h>
#include <stdio.h>
#include <SgiTipos.h>
#include <MensajesSAT.h>
#include <Sgi_SrvUnx.h>
#include <ArchivosConfg.h>
#include <AdmSegPKI.h>
#include <ShMemPKI.h>
#include <PwdProtection.h>

/*struct RSA_PRIVADA
{
   unsigned short int bits;
   uchar moduloP[MAX_MODULO_LEN];
   uchar exponenteP[MAX_MODULO_LEN];
   uchar exponentePv[MAX_MODULO_LEN];
   uchar primo[2][MAX_PRIMO_LEN];
   uchar primoExp[2][MAX_PRIMO_LEN];
   uchar coeficiente[MAX_PRIMO_LEN];
};*/

uint8 keyTest[1024*4];
SGIPRIVADA *privada;
RSA_PRIVADA *priv;
RSA *rsa;

int verifica()
{
   SGIRSA Rsa;
   rsa = RSA_new();
   privada = new SGIPRIVADA;
   priv = new RSA_PRIVADA;
   intE error = 0;
   error = Rsa.DecodPvKey("/usr/local/SAT/PKI/Certificados/AC_SAT2048.cve", "ac_sat2048", &rsa);
   if(!error)
   {
      error = Rsa.setPrivada(rsa, (SGIPRIVADA*)priv);
      if(!error)
      {
         memcpy(keyTest, priv, sizeof(SGIPRIVADA));
      }
      else 
      {
         cout << "No se proces� la llave privada correctamente." << endl;
         return -1;
      }
      memset(privada, 0, sizeof(privada));
      memcpy(privada, keyTest, sizeof(SGIPRIVADA));
      RSA_free(rsa);
      rsa = NULL;
      rsa = RSA_new();
      error = Rsa.getPrivada(privada, rsa);
      if(!error)
      {
         cout << "Llave privada normal." << endl;
         exit(0);
      }
      else cout << "No se recuper� correcta." << endl;
   }
   else exit(-1);
   RSA_free(rsa);
   delete privada;
   return 0;   
}
bool test()
{
        SGIRSA rsaalg;
        SGIPRIVADA *privada = new SGIPRIVADA;
        RegServPKI *srv;
        rsa = RSA_new();
        bool ok = true;
        uint8 *shmbuff;
        char *shmseg;
        int shmid;
        int reshm;
        shmid = shmget(KEY_SHMEM, 0, 0);
        if(shmid <= 0)
                return  false;
        shmseg = (char*)shmat(shmid, 0, 0);
        srv = new RegServPKI;
        memcpy(srv, shmseg, sizeof(struct reg_serv_pki_st));
        memcpy(privada, srv->idserv->privKey, sizeof(SGIPRIVADA));
        intE error = rsaalg.getPrivada(privada, rsa);
        if(error != 0)
                ok = false;
        reshm = shmdt(shmseg);
        if(reshm < 0)
                ok = false;
        delete privada;
        delete srv;
        return ok;
}

int main(void)
{
   iniciaLibOpenSSL();
//   int error = verifica();
   bool ok = test();
   terminaLibOpenSSL();
   return 0;

}

