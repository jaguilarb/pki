static const char* _CSERVIDOR_H_VERSION_ ATR_USED = "Servidor @(#)"\
"DSIC07412AC_ 2007-12-14 CServidor.h 1.1.0/0";

//#VERSION: 1.1.0
#include <SgiTipos.h>
#include <Sgi_SrvUnx.h>
#include <ArchivosConfg.h>
#include <PwdProtection.h>
#include "ShMemPKI.h"
#include "SecretoCompartido.h"

#define ERR_NO_SPC		-1
#define PATHCONF		"/usr/local/SAT/PKI/etc/ADMSEG.cfg"

static int password_cb(char *buf, int size, int rwflag, void *password);
int verify_callback(int ok, X509_STORE_CTX *store);

class CServidor : public CSrvUnx
{
protected:
   char *arcConf;
   char texto[256];
   uchar buffM[1024 * 32];
   int lbuffM;
   intE err;
   bool  Proceso();
   bool  SetParmsSktProc(CSSL_parms& parms);
   char root[200];
   char rutaac[200];
   char ac1[200];
   char ack[200];
   char llave[30];
   ServSegInfo *srv;
   CSHMemPKI *mem; 
public:
   bool  Central();
   intE  inicia();
   intE  conecta();
   intE  recibe();
   intE  envia(uchar*, int);
   bool  subeLlave();
   void  limpiaBuf();
   CServidor(char*);
   virtual ~CServidor();
};
