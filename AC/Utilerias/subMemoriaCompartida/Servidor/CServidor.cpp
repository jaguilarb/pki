static const char* _CSERVIDOR_CPP_VERSION_ ATR_USED = "Servidor @(#)"\
"DSIC07412AC_ 2007-12-14 CServidor.cpp 1.1.0/0";

//#VERSION: 1.1.0
/*******************************************************************
*** Servidor para recibir la llave privada de la AC en un buffer ***
*** preparado como servicio de seguridad para subirlo a la mem   ***
*** compartida. AGZ Septiembre 2004                              ***
*******************************************************************/

#include "CServidor.h"

static char bLLave[30];
static char* PATHS[] = {"ROOT","RUTAC","AC1","ACK","BLLAVE"};

CBitacora* Crea_CBitacora()
{
   return new CBitacora("/var/local/SAT/PKI/ADMSEG.log");
}

CSrvUnx* Crea_CSrvUnx()
{
   return new CServidor(PATHCONF);
}

bool libSrvUnx_Fin()
{
   iniciaLibOpenSSL();
   return true;
}

bool libSrvUnx_Ini()
{
   terminaLibOpenSSL();
   return true;
}

bool CServidor::
SetParmsSktProc(CSSL_parms& parms)
{
   ArchivosConfg conFile;
   err = conFile.VarConfg(5, PATHS, arcConf, root, rutaac, ac1, ack, llave);
   if(err != 0)
      Bitacora->escribePV(BIT_ERROR, "Error al extraer los datos de configuraci�n.(%i)", err);
   parms.SetServicio(ESSL_entero, 0, root, rutaac, ac1, ack, verify_callback, password_cb);
   uint8 ps1[255];
   int lps1 = sizeof(ps1);
   memset(ps1, 0, sizeof(ps1));
   desencripta((uint8*)llave, strlen(llave), ps1, &lps1);
   strncpy(llave, (char*)ps1, lps1);
   llave[lps1] = '\0';
   memcpy(bLLave, llave, strlen(llave));
   //parms.SetServicio(EPlano);
   return true;
}

int verify_callback(int ok, X509_STORE_CTX *store)
{
   char data[256];
   if (!ok)
   {
      X509 *cert = X509_STORE_CTX_get_current_cert(store);
      int depth = X509_STORE_CTX_get_error_depth(store);
      int err = X509_STORE_CTX_get_error(store);
      fprintf(stderr,"Error con el certificado %i\n",depth);
      X509_NAME_oneline(X509_get_issuer_name(cert),data,256);
      fprintf(stderr,"issuer = %s\n",data);
      X509_NAME_oneline(X509_get_subject_name(cert),data,256);
      fprintf(stderr,"subject=%s\n",data);
      fprintf(stderr,"err%i:%s\n",err,X509_verify_cert_error_string(err));
   }
   return (ok);
}

static int password_cb(char *buf, int size, int rwflag, void *password)
{
   password = bLLave;
   strncpy(buf, (char *)(password), size);
   buf[size - 1] = '\0'; //buf[strlen(bLLave)] = '\0';
   return(strlen(buf));
}

bool CServidor::Proceso()
{
   bool ok = true;
   if(!Central())
   {
      memset(texto, 0, sizeof(texto));
      sprintf(texto, "Ocurrio un error.");
      Bitacora->escribe(BIT_INFO, texto);
      ok = false;
   }
   return ok;
}

bool CServidor::Central()
{
   err = recibe(); 
   if(err != 0)  
   {
      memset(texto, 0, sizeof(texto));
      sprintf(texto, "Ocurrio el error: %i", sgiErrorBase(err));
      Bitacora->escribe(BIT_INFO, texto);
      return false;
   }
   if(!subeLlave())
   {
      memset(texto, 0, sizeof(texto));
      sprintf(texto, "Ocurrio el error: %i", sgiErrorBase(err));
      Bitacora->escribe(BIT_INFO, texto);
      return false;
   }
   err = envia((uint8*)"Se inicio el Servicio con �xito.", strlen("Se inicio el Servicio con �xito."));
   if(err != 0)
      return false;
   delete mem;
   delete srv;
   return true;
}

intE  CServidor::recibe()
{
   limpiaBuf();
   intE error = 0;
   error = SktProc->Recibe((uint8*)buffM, lbuffM);
   if(error != 0)
      return error;
   return error;
}

intE  CServidor::envia(uchar *buffer, int lbuffer)
{
   limpiaBuf();
   intE error = 0;
   if(lbuffer > lbuffM)
      return sgiError(ERR_NO_SPC, eAPL);
   lbuffM = lbuffer;
   memcpy(buffM, buffer, lbuffM);
   error = SktProc->Envia((uint8*)buffM, lbuffM);
   if(error != 0)
      return error;
   return error;
}

bool  CServidor::subeLlave()
{
   bool ok = true;
   CSHMemPKI *mem = new CSHMemPKI(KEY_SHMEM, TamBufShMem);
   ServSegInfo *srv = new ServSegInfo;
   if(!mem->abre(true))
      return false;
   memset(srv, 0, sizeof(struct serv_info_st));
   memcpy(srv, buffM, sizeof(struct serv_info_st));
   ok = mem->registraServ(srv);  
   if(ok == false)
      Bitacora->escribe(BIT_INFO, "Ocurri� un error en la SHM."); 
   return ok;
}

CServidor::CServidor(char *archConf)
{
   arcConf = archConf;
}

CServidor::~CServidor()
{
   arcConf = NULL;
}

void CServidor::limpiaBuf()
{
   lbuffM = sizeof(buffM);
   memset(buffM, 0, sizeof(buffM));
}
