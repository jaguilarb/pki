#ifndef _SRVSECRETOCOMPARTIDO_H_
#define _SRVSECRETOCOMPARTIDO_H_
static const char* _SRVSECRETOCOMPARTIDO_H_VERSION_ ATR_USED = "SrvSecretoCompartido @(#)"\
"DSI_08341BAS 2007-12-13 SrvSecretoCompartido.h 1.1.1/?";


//#VERSION: 1.1.0

#ifdef DBG
   #define PATH_CFG "/usr/local/SAT/PKI/etc/SrvSecretoCompartido_dbg.cfg"
   #define PATH_BIT "/var/local/SAT/PKI/SrvSecretoCompartido_dbg.log"
#else
   #define PATH_CFG "/usr/local/SAT/PKI/etc/SrvSecretoCompartido.cfg"
   #define PATH_BIT "/var/local/SAT/PKI/SrvSecretoCompartido.log"
#endif


#include <malloc.h>
#include <string.h>
#include <sys/stat.h>
#include <Sgi_SrvUnx.h>
#include <Sgi_SSL.h>
#include <Sgi_ConfigFile.h>
#include "CDesencripta.h"
#include "CSecretoCompartido.h"

using std::string;

class SrvSecretoCompartido : public CSrvUnx
{
   protected :
      Participante *criptografia;
      CSecretoCompartido *secreto;
      CSSL_Skt *socket;
      CConfigFile *config;
      string rutaLlave;

      bool initSocket();
      bool finSocket();

   public :
      SrvSecretoCompartido();
      virtual ~SrvSecretoCompartido();
      virtual bool SetParmsSktProc(CSSL_parms &parms);
      virtual bool Inicia();
      virtual bool Proceso();

};

#endif
