static const char* _SRVSECRETOCOMPARTIDO_CPP_VERSION_ ATR_USED = "SrvSecretoCompartido @(#)"\
"DSI_08341BAS 2007-12-13 SrvSecretoCompartido.cpp 1.1.1/?";

//#VERSION: 1.1.0
#include "SrvSecretoCompartido.h"

SrvSecretoCompartido::SrvSecretoCompartido()
{
}

SrvSecretoCompartido::~SrvSecretoCompartido()
{
}

bool SrvSecretoCompartido::
Inicia()
{
   int error;
   config = new CConfigFile(PATH_CFG);
   if( !config )
      Bitacora->escribe(BIT_ERROR, "No se ha podido iniciar la configuración del servicio.");
   else if( (error = config->cargaCfgVars()) != 0 )
      Bitacora->escribe(BIT_ERROR, error, "No se pudieron cargar las variables de configuración.");
   else
      return true;
   return false;
}

bool SrvSecretoCompartido::
SetParmsSktProc(CSSL_parms &parms)
{
   int tm_espera = 60;
   #ifdef DBG
      tm_espera = 120;
   #endif
   timeval timeWait = {tm_espera, 0};
   parms.SetServicio(EPlano, &timeWait);
   return true;
}

bool SrvSecretoCompartido::
Proceso()
{
   secreto = new CSecretoCompartido(SktProc, Bitacora);
   secreto->cargaConfiguracion(config);
   if ( secreto->iniComunicacion() )
   {
      while ( secreto->esperaMensaje() );
      return true;
   }
   return false;
}

bool 
libSrvUnx_Ini() {
  return true;
}

bool 
libSrvUnx_Fin() {
  return true;
}

CBitacora* 
Crea_CBitacora() {
  #ifdef DBG_NO_SRV
  DBG_NS_Puerto = 99166;
  #endif
  CBitacora *bitacora = new CBitacora(PATH_BIT);
  if( bitacora )
  {
     #ifdef DBG
     bitacora->setNivel(BIT_DEBUG);
     #endif
     bitacora->escribe(BIT_INFO, "Inicio de proceso");
     #ifdef DBG
        sleep(20);
     #endif
     return bitacora;
  }
  return NULL;
}

CSrvUnx* 
Crea_CSrvUnx() {
  return new SrvSecretoCompartido();
}
