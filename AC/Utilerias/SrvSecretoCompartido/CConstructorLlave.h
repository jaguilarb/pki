#ifndef _CCONSTRUCTORLLAVE_H_
#define _CCONSTRUCTORLLAVE_H_
static const char* _CCONSTRUCTORLLAVE_H_VERSION_ ATR_USED = "SrvSecretoCompartido @(#)"\
"DSIC09365SLD 2007-12-13 CConstructorLlave.h 1.1.1/0";

//#VERSION: 1.1.1

#include <Sgi_SrvUnx.h>
#include <Sgi_Bitacora.h>
#include <Sgi_MsgPKI.h>
#include <sys/msg.h>
#include <stdio.h>
#include <string>

#define FIFO_ID 123
#define TMSIZE 4096
#define M_SIZE (TMSIZE - (sizeof(int)*2) - sizeof(long))
//#define MQ_ID 101

typedef struct
{
  long tipo;
  int pid;
  uint8 mensaje[TAM_DATOS]; 
} MSG_LLAVE_AC;
 

class CConstructorLlave
{
   private :
      int man_cm;
      MensajesLLavPrivAC *mensajes;
      MSG_LLAVE_AC mensaje;
      MSG_LLAVE_AC respuesta;
      bool llaveActiva();
      bool recibeMediador();
      bool prepararCola();
      bool procesarMensaje();
   protected :
      int MQ_ID;
   public :
      CConstructorLlave(MensajesLLavPrivAC *mensajes);
      ~CConstructorLlave();
      void setMQID(int);
      int  getMQID();
      bool enviaParticipacion();
      int solicitaEstadoLlave();
};

#endif
