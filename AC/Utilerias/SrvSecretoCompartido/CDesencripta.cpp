static const char* _CDESENCRIPTA_CPP_VERSION_ ATR_USED = "SrvSecretoCompartido @(#)"\
"DSIC09365SLD 2007-12-13 CDesencripta.cpp 1.1.1/0";

//#VERSION: 1.1.1
#include "CDesencripta.h"

/* 
   Los pasos para que esto funcione son los siguiente.
   - Se instancia un objeto de la clase Participante.
   - Se abre un archivo o se establece un buffer de bytes como datos para desencriptar.
   - se establece una contraseņa para desencriptar.
   - se llama a un metodo segun se sepa que es el correspondiente al algoritmo que se utilizo.
   - finalmente se hace algo con el buffer ya desencriptado.
*/

Participante::
Participante()
{
   plano = NULL;
   cifrado = NULL;
   passwd = (char *)malloc(512);
   memset(passwd, 0, 512);
   md5pwd = (unsigned char *)malloc(MD5LEN);
   memset(md5pwd, 0, MD5LEN);
   ERR_load_crypto_strings();
   error_string = NULL;
   llave_privada = NULL;
   l_cifrado = 0;
   l_plano = 0;
   l_privada = 0;
}

// Esta funcion Nadie la llama...
void Participante::
abrirArchivo(char *ruta)
{
   FILE *fp;
   ManArchivo archivo;
   if( archivo.TamArchivo( ruta, &l_cifrado ) )
   {
      if(cifrado)
      {
         free(cifrado);
         cifrado = NULL;
      }
      cifrado = (unsigned char *)malloc(l_cifrado);
      memset(cifrado, 0, l_cifrado);      
 
      fp = fopen(ruta, "r");
      if( fp )
      {
         fread((uint8 *)cifrado, 1, l_cifrado, fp);
         fclose(fp);
      }
   }
}

void Participante::
setPasswd(const char *pwd)
{
   if (passwd == NULL)
      passwd = (char *)calloc(strlen(pwd)+1, sizeof(char));
   else
      memset(passwd, 0, strlen(pwd)+1); 
   memcpy(passwd, pwd, strlen(pwd));
   if (md5pwd == NULL)
      md5pwd = (unsigned char *)calloc(MD5LEN, sizeof(char));
   else
      memset(md5pwd, 0, MD5LEN);
   getMd5sum((unsigned char *)passwd, strlen(pwd), md5pwd);
}

void Participante::
setMd5Pwd(const char *pwd)
{
   if(md5pwd == NULL)
      md5pwd = (unsigned char *)calloc(MD5LEN, sizeof(char));
   else
      memset(md5pwd, 0, MD5LEN);
   memcpy(md5pwd, pwd, MD5LEN);
}

char *Participante::
getMd5Pwd()
{
   if (md5pwd)
      return (char *)md5pwd;
   return NULL;
}

void Participante::
setData(unsigned char *datos, int longitud)
{
   cifrado = datos;
   l_cifrado = longitud;
}

void Participante::
getData(unsigned char **datos, int &longitud)
{
   if ( cifrado )
   {
      *datos = cifrado;
      longitud = l_cifrado;
   }
}

void Participante::
getDatosPlanos(unsigned char **datos, int &longitud)
{
   if ( *datos == NULL ) // ( !*datos ) // NOTA: esta variable se libera donde se usa la funcion
      *datos = (unsigned char *)calloc(l_plano, sizeof(char));
   if ( plano )
   {
      memcpy(*datos, plano, l_plano);
      longitud = l_plano;
   }
}

void Participante::
getMd5sum(const unsigned char *cad, int loncad, unsigned char *suma)
{
   MD5_CTX md5ctx;
   MD5_Init(&md5ctx);
   MD5_Update(&md5ctx, cad, loncad);
   MD5_Final(suma, &md5ctx);
} 

bool Participante::
desencriptaRC4()
{
   initBufPlano(l_cifrado);
   if ( plano )
   {
      RC4_KEY rc4key;
      RC4_set_key(&rc4key, MD5LEN, (uint8 *)this->getMd5Pwd());
      RC4(&rc4key, l_cifrado, cifrado, plano);
      if( strlen((char *)plano) > 0 ) 
         return true;
   }
   return false;
}

bool Participante::
desencriptaAES()
{
   if( cifrado )
   {
      initBufPlano(l_cifrado);
      if( plano )
      {
         int offset = 0;
         AES_KEY llave;
         AES_set_decrypt_key((const unsigned char *)getMd5Pwd(), 128, &llave);
         while( offset < l_cifrado )
         {
            AES_decrypt(cifrado+offset, plano+offset, &llave);
            offset+=AES_BLOCK_SIZE;
         }
         l_plano = (l_cifrado - (int)(plano[l_cifrado-1]));
         return true;
      }
   } 
   return false;
}

void Participante::
escribirPlano(const char *ruta)
{
   FILE *fp = NULL;
   fp = fopen(ruta, "w");
   if ( fp && plano )
   {
      fwrite(plano, 1, l_plano, fp);
      fclose(fp);
   }  
}

bool Participante::
desencriptaRSA(const char *r_llave, char *pwd)
{
   if( leerLlave(r_llave) )
   {
      Bitacora->escribePV(BIT_INFO, "Siguiente paso desencriptaRSA " );
      if( desencriptaRSA(llave_privada, l_privada, pwd) )
         return true;
   }
   else Bitacora->escribePV(BIT_INFO, "Error  %d\n",errno );
   return false; 
}


bool Participante::
desencriptaRSA(const char *c_llave, const int l_llave, char *pwd)
{
   Bitacora->escribePV(BIT_INFO, "Tam Cifrado %d\nTam Llave %d\n",l_cifrado,l_llave);
   initBufPlano(l_cifrado);
   //OpenSSL_add_all_algorithms();
   BIO *llave_bio = BIO_new_mem_buf((void *)c_llave, l_llave);
   EVP_PKEY *llave_evp = NULL;
   unsigned long error = 0;

   llave_evp = d2i_PKCS8PrivateKey_bio(llave_bio, NULL, NULL, pwd);
   if( llave_evp )
   {
      Bitacora->escribe(BIT_INFO, "Dentro llave_evp" );
      RSA *llave = EVP_PKEY_get1_RSA(llave_evp);
      if ( llave )
      {
         //Iterar hasta terminar con el buffer.
         l_plano = RSA_private_decrypt(128, cifrado, plano, llave, 1);
         Bitacora->escribe(BIT_INFO, "RSA_private" );
      }else Bitacora->escribe(BIT_INFO, "No se obtuvo la llave privada RSA");
   } 
   sacaErrorSSL(&error); 
   if( error != 0 )
      Bitacora->escribePV(BIT_ERROR, "error_string=%s\n", error_string );
   return error != 0 ? false : true; 
}

char *Participante::
sacaErrorSSL(unsigned long *error)
{
   unsigned long error_l = ERR_peek_error();
   if( error_l > 0 )
   {
      if ( error_string == NULL )
         error_string = (char *)malloc(512);
      memset(error_string, 0, 512); 
      ERR_error_string(error_l, error_string);
      *error = error_l;
      return error_string;
   }
   return NULL;
}

unsigned char *Participante::
initBufPlano(size_t longitud)
{
   if(plano)
   {
      free(plano);
      plano = NULL;
   }
   plano = (unsigned char *)calloc(longitud, sizeof(char));
   l_plano = longitud;
   return plano; 
}

bool Participante::
leerLlave(const char *ruta)
{
   Bitacora->escribePV(BIT_INFO, "LeeLlave %s ",ruta );
   int error = 0;
   FILE *archivo = fopen(ruta, "rb");
   struct stat s_arch;
   if( archivo )
   {
      stat(ruta, &s_arch); 
      l_privada = s_arch.st_size;
      if(llave_privada)
      {
         free(llave_privada);
         llave_privada = NULL;
      }
      llave_privada = (char *)calloc(l_privada, sizeof(char)); 
      fread(llave_privada, sizeof(char), l_privada, archivo);
      Bitacora->escribePV(BIT_INFO, "Dentro errno %d ",errno );
      error = errno;
      if( !error )
         return true;
   }
   else Bitacora->escribePV(BIT_INFO, "errno %d ",errno );
   return false;
}

void Participante::
reset()
{
   if(passwd)
   {
      free(passwd);
      passwd = NULL;
   }
   if(llave_privada)
   {
      free(llave_privada);
      llave_privada = NULL;
   }
   if(md5pwd)
   {
      free(md5pwd);
      md5pwd = NULL;
   }
   /*if(cifrado) // cifrado solo se usa como pointer pero no se le asigna memoria
   {
      free(cifrado);
      cifrado = NULL;
   }*/
   if(plano)
   {
      free(plano);
      plano = NULL;
   }
   if (error_string)
   {
      free(error_string);
      error_string = NULL;
   }
   l_cifrado = 0;
   l_plano = 0;
   l_privada = 0;
}

Participante::
~Participante()
{
   reset();
}

