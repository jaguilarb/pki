#ifndef _CDESENCRIPTA_H_
#define _CDESENCRIPTA_H_
static const char* _CDESENCRIPTA_H_VERSION_ ATR_USED = "SrvSecretoCompartido @(#)"\
"DSIC09365SLD 2007-12-13 CDesencripta.h 1.1.1/0";

//#VERSION: 1.1.1

#include <Sgi_SrvUnx.h>
#include <Sgi_Bitacora.h>
#include <Sgi_OpenSSL.h>
#include <openssl/bio.h>
#include <openssl/md5.h>
#include <openssl/rc4.h>
#include <openssl/aes.h>
#include <openssl/rsa.h>
#include <openssl/err.h>
#include <openssl/evp.h>
#include <SgiTipos.h>
#include <malloc.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>

#define MD5LEN 17

class Participante
{
   private :
      char *passwd;
      char *llave_privada, *error_string;
      unsigned char *md5pwd;
      unsigned char *cifrado;
      unsigned char *plano;
      int l_cifrado, l_plano, l_privada;
      char *sacaErrorSSL(unsigned long *);

   public :
      Participante();
      ~Participante();
      void abrirArchivo(char *ruta);
      void setPasswd(const char *pwd);
      void setMd5Pwd(const char *pwd);
      char *getMd5Pwd();
      void setData(unsigned char *datos, int longitud);
      void getData(unsigned char **datos, int &longitud);
      void getDatosPlanos(unsigned char **datos, int &longitud);
      void getMd5sum(const unsigned char *cad, int loncad, unsigned char *suma);
      void escribirPlano(const char *ruta);
      bool desencriptaRC4();
      bool desencriptaAES();
      bool desencriptaRSA(const char *, const int l_llave, char *);
      bool desencriptaRSA(const char *, char *);
      unsigned char *initBufPlano(size_t longitud);
      bool leerLlave(const char *ruta);
      void reset();
};

#endif 
