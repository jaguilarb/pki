#ifndef _CSECRETOCOMPARTIDO_H_
#define _CSECRETOCOMPARTIDO_H_
static const char* _CSECRETOCOMPARTIDO_H_VERSION_ ATR_USED = "SrvSecretoCompartido @(#)"\
"DSIC09365SLD 2007-12-13 CSecretoCompartido.h 1.1.1/0";


//#VERSION: 1.1.1

#include "CConstructorLlave.h"
#include <Sgi_ProtegePwd.h>
#include <Sgi_MsgPKI.h>
#include <Sgi_Bitacora.h>
#include <Sgi_ConfigFile.h>
#include <SgiTipos.h>
#include <Sgi_B64.h>
#include <string>


class CSecretoCompartido
{
   private :
      int error_mensajes, L_P_PASS, i_mqid;
      uint8 *p_pass;
      CSSL_Skt *socket;
      CConfigFile *config;
      CBitacora *Bitacora;
      Participante *cripto;
      CConstructorLlave *constructor;
      std::string s_cer, s_key, s_passwd, s_rsakey, s_mqid;
      MensajesLLavPrivAC *mensajes; 
      bool procesaParticipacion(unsigned char *participante, 
                                int l_participante, unsigned char *rsapass, int l_rsapass);
      bool procesaMensaje();
       
   public  :
      CSecretoCompartido(CSSL_Skt *skt, CBitacora *bit);
      ~CSecretoCompartido();
      bool iniComunicacion();
      void finComunicacion(); 
      bool esperaMensaje();
      void cargaConfiguracion(CConfigFile *config);
      void guardaMQID();
};

#endif //_CSECRETOCOMPARTIDO_H_
