static const char* _SRVSECRETOCOMPARTIDO_CPP_VERSION_ ATR_USED = "SrvSecretoCompartido @(#)"\
"DSIC09365SLD 2007-12-13 SrvSecretoCompartido.cpp 1.1.1/0";

//#VERSION: 1.1.1
#include "SrvSecretoCompartido.h"

SrvSecretoCompartido::SrvSecretoCompartido()
{
   ruta_bit = NULL;
   ruta_cfg = NULL;
   config = NULL;
}

SrvSecretoCompartido::~SrvSecretoCompartido()
{
   if (config)
   {
      delete config;
      config = NULL;
   }
   if( ruta_bit )
   {
      free( ruta_bit );
      ruta_bit = NULL;
   }
   if( ruta_cfg )
   {
      free( ruta_cfg );
      ruta_cfg = NULL;
   }
}

bool SrvSecretoCompartido::
Inicia()
{
   int error;
   
   if( ruta_cfg )
      config = new CConfigFile( ruta_cfg );
   else
      config = new CConfigFile(PATH_CFG);

   if( !config )
      Bitacora->escribe(BIT_ERROR, "No se ha podido iniciar la configuración del servicio.");
   else if( (error = config->cargaCfgVars()) != 0 )
      Bitacora->escribe(BIT_ERROR, error, "No se pudieron cargar las variables de configuración.");
   else
      return true;
   return false;
}

bool SrvSecretoCompartido::
SetParmsSktProc(CSSL_parms &parms)
{
   int tm_espera = 60;
   #ifdef DBG
      tm_espera = 120;
   #endif
   timeval timeWait = {tm_espera, 0};
   parms.SetServicio(EPlano, &timeWait);
   return true;
}

bool SrvSecretoCompartido::
Proceso()
{
   bool ret = false;
   OpenSSL_add_all_algorithms();

   CSecretoCompartido *secreto = NULL; 
   secreto = new CSecretoCompartido(SktProc, Bitacora);
   if (secreto)
   {
      secreto->cargaConfiguracion(config);
      if ( secreto->iniComunicacion() )
      {
         while ( secreto->esperaMensaje() );
         ret = true;
      }
      delete secreto;
      secreto = NULL;
   } 
   return ret;
}

bool SrvSecretoCompartido::
ProcesaParametros( int argc, char *argv[], char *env[] )
{
   int opt = 0;
   size_t l_path = 0;
   while( (opt = getopt( argc, argv, "c:b:" )) != -1 )
   {
      switch( opt )
      {
         case 'c' :
            l_path = strlen( optarg );
            ruta_cfg = (char *)malloc( sizeof(char) * l_path+1 );
            ruta_cfg[l_path] = 0x00;
            strncpy( ruta_cfg, optarg, l_path );
            break;
         case 'b' :
            l_path = strlen( optarg );
            ruta_bit = (char *)malloc( sizeof(char) * l_path+1 );
            ruta_bit[l_path] = 0x00;
            strncpy( ruta_bit, optarg, l_path );
            break;
      }
   }
   return true;
}

bool 
libSrvUnx_Ini() 
{
  #ifdef DBG
     sleep(20);
  #endif

  #ifdef DBG_NO_SRV
  DBG_NS_Puerto = 99166;
  #endif

  #ifdef DBG
  Bitacora->setNivel(BIT_DEBUG);
  #endif
  Bitacora->escribe(BIT_INFO, "Inicio de proceso");
  return true;
}

bool 
libSrvUnx_Fin() 
{
  return true;
}

CBitacora* 
Crea_CBitacora() 
{
   if( ruta_bit )
      return new CBitacora(ruta_bit);
   else
      return new CBitacora(PATH_BIT);
}

CSrvUnx* 
Crea_CSrvUnx() {
  return new SrvSecretoCompartido();
}
