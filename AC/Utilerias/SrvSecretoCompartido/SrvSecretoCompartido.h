#ifndef _SRVSECRETOCOMPARTIDO_H_
#define _SRVSECRETOCOMPARTIDO_H_
static const char* _SRVSECRETOCOMPARTIDO_H_VERSION_ ATR_USED = "SrvSecretoCompartido @(#)"\
"DSIC09365SLD 2007-12-13 SrvSecretoCompartido.h 1.1.1/0";


//#VERSION: 1.1.1

#ifdef DBG
   #define PATH_CFG "/etc/SrvSecretoCompartido_dbg.cfg"
   #define PATH_BIT "/var/log/SrvSecretoCompartido_dbg.log"
#else
   #define PATH_CFG "/etc/SrvSecretoCompartido.cfg"
   //#define PATH_CFG "/usr/local/SAT/PKI/etc/SrvSecretoCompartidoS.cfg"
   #define PATH_BIT "/var/log/SrvSecretoCompartido.log"
   //#define PATH_BIT "/var/local/SAT/PKI/SrvSecretoCompartido.log"
#endif


#include <malloc.h>
#include <string.h>
#include <sys/stat.h>
#include <Sgi_SrvUnx.h>
#include <Sgi_SSL.h>
#include <Sgi_ConfigFile.h>
#include "CDesencripta.h"
#include "CSecretoCompartido.h"

char *ruta_bit;
char *ruta_cfg;
using std::string;

class SrvSecretoCompartido : public CSrvUnx
{
   protected :
      CConfigFile *config;
      string rutaLlave;

      bool initSocket();
      bool finSocket();

   public :
      SrvSecretoCompartido();
      bool ProcesaParametros(int argc, char* argv[], char* env[]);
      virtual ~SrvSecretoCompartido();
      virtual bool SetParmsSktProc(CSSL_parms &parms);
      virtual bool Inicia();
      virtual bool Proceso();

};

#endif
