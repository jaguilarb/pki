static const char* _CSECRETOCOMPARTIDO_CPP_VERSION_ ATR_USED = "SrvSecretoCompartido @(#)"\
"DSIC09365SLD 2007-12-13 CSecretoCompartido.cpp 1.1.1/0";

//#VERSION: 1.1.1
#include "CDesencripta.h"
#include "CSecretoCompartido.h"

CSecretoCompartido::
CSecretoCompartido(CSSL_Skt *skt, CBitacora *bitacora) :
   L_P_PASS(128 * 4),
   p_pass((uint8 *)malloc(L_P_PASS)),
   socket(skt),
   config(NULL), 
   cripto(NULL)
{
   Bitacora = bitacora;
   mensajes = new MensajesLLavPrivAC(); 
   memset(p_pass, 0, (L_P_PASS));
   constructor = new CConstructorLlave(mensajes);
}

void CSecretoCompartido::
cargaConfiguracion(CConfigFile *config)
{
   if(config->getValorVar("[SECRETO_COMPARTIDO]", "CERT", &s_cer))
      Bitacora->escribe(BIT_ERROR, "Lectura de CERTF");
   if(config->getValorVar("[SECRETO_COMPARTIDO]", "LLAVE", &s_key))
      Bitacora->escribe(BIT_ERROR, "Lectura de LLAVE");
   if(config->getValorVar("[SECRETO_COMPARTIDO]", "PASSW", &s_passwd))
      Bitacora->escribe(BIT_ERROR, "Lectura de PASSW");
   //if(config->getValorVar("[SECRETO_COMPARTIDO]", "CRKEY", &s_rsakey))
   //   Bitacora->escribe(BIT_ERROR, "Lectura de Llave privada PKCS8");
   if(config->getValorVar("[SECRETO_COMPARTIDO]", "MQID", &s_mqid))
      Bitacora->escribe(BIT_ERROR, "Lectura del identificador de la cola.");
   i_mqid = atoi(s_mqid.c_str());
   guardaMQID();

   char *pass = (char *)s_passwd.c_str();
   memcpy(p_pass, pass, s_passwd.size()); 
   //desencripta((const uint8 *)s_passwd.c_str(), s_passwd.size(), p_pass, &L_P_PASS);
}


void CSecretoCompartido::
guardaMQID() // Parche: el valor de MQID de la clase CConstructorLlave trae un valor diferente del inicial
{
   if(constructor)
      constructor->setMQID(i_mqid);
   //Bitacora->escribePV(BIT_DEBUG, "i_mqid=%d, MQID=%d", i_mqid, constructor->getMQID() );   
}

CSecretoCompartido::
~CSecretoCompartido()
{
   if (constructor)
   {
      delete constructor;
      constructor = NULL;
   }
   if (mensajes)
   {
      delete mensajes;
      mensajes = NULL;
   }
   if (p_pass)
   {
      free( p_pass );
      p_pass = NULL;
   }
}

bool CSecretoCompartido::
iniComunicacion()
{
   error_mensajes = mensajes->Inicia(0, 
                    (char *)s_cer.c_str(), (char *)s_key.c_str(), 
                   (char *)p_pass, L_P_PASS);
   if ( error_mensajes )
   {
      Bitacora->escribePV(BIT_ERROR, "Ocurrio un error al iniciar la biblioteca de mensajes %d\n", error_mensajes); 
      return false;
   }
   return true;
}

void CSecretoCompartido::
finComunicacion()
{
} 

bool CSecretoCompartido::
esperaMensaje()
{
   if( socket->LecPendiente() )
   {
      guardaMQID(); // Parche: el valor de MQID de la clase CConstructorLlave trae un valor diferente del inicial 
      mensajes->Recibe(socket);
      switch ( mensajes->tipOperacion() )
      {
         case SOLCONEXIONLLAVEPRIV : 
         {
            Bitacora->escribePV(BIT_INFO, "Se recibio una solicitud de participacion.");
            error_mensajes = mensajes->setMensaje(CONEXIONLLAVEPRIV, "1", 1);
            if ( !error_mensajes )
            {
               Bitacora->escribePV(BIT_DEBUG, "Se escribira el mensaje %d en el Socket", mensajes->tipOperacion());
               mensajes->Envia(socket);
            }
            else
            {
               Bitacora->escribePV(BIT_ERROR, "Ocurrio un error al intentar preparar el mensaje Msg: %d Err: %d.", 
                                   CONEXIONLLAVEPRIV, error_mensajes);
            }
            break;
         }
         case ACUSECON :
         {
            Bitacora->escribePV(BIT_DEBUG, "La conexion con el cliente se ha completado.");
            break;
         }
         case ENVIARLLAVE :
         {
            Bitacora->escribePV(BIT_DEBUG, "Se recibio una llave participante.");
            if( procesaMensaje() )
            {
               if( !constructor->enviaParticipacion() ) 
               {
                  Bitacora->escribePV(BIT_DEBUG, "Se escribira el mensaje %d en el Socket", mensajes->tipOperacion());
                  mensajes->Envia(socket);
               }
               //error_mensajes = mensajes->setMensaje(ACUSELLAVE);
            }
            else
            {
               char *msg_error = "Ocurrio un error al procesar la participación.";
               mensajes->setMensaje(ERRORLLAVE, "0", 1, msg_error, strlen(msg_error)); 
            }
            if ( !error_mensajes )
            {
               Bitacora->escribePV(BIT_DEBUG, "Se escribira el mensaje %d en el Socket", mensajes->tipOperacion());
               mensajes->Envia(socket);
            }
            else
            {
               Bitacora->escribePV(BIT_ERROR, "Ocurrio un error al intentar preparar el mensaje Msg: %d Err: %d.", 
                                   CONEXIONLLAVEPRIV, error_mensajes);
            }
            break;
         }
         case SOLEDOLLAVE :
         {
            if( constructor->solicitaEstadoLlave() == -1 )
            {
               Bitacora->escribePV(BIT_ERROR, "Se saldra porque no se pudo obtener el estado de la llave.");
               return false;
            }
            Bitacora->escribePV(BIT_DEBUG, "Se escribira el mensaje %d en el Socket", mensajes->tipOperacion());
            mensajes->Envia(socket);
            break;
         }
         case DESCONEXIONLLAVEPRIV :
         {
            Bitacora->escribePV(BIT_INFO, "Se recibio la solicitud de desconexion desde el cliente.");
            return false;
         }
         default :
         {
            Bitacora->escribePV(BIT_INFO, "Se saldra de la espera con un mensaje inesperado de tipo (%d)", mensajes->tipOperacion());
            return false;
         }
      }
   }
   return true;
}

bool CSecretoCompartido::
procesaParticipacion(unsigned char *participante, int participante_len, unsigned char *rsapass, int l_rsapass)
{
   bool ret = false;

   Bitacora->escribePV(BIT_INFO, "Participante %d RSAPass %d RSAPass %d",participante_len,rsapass,l_rsapass);
   cripto = new Participante();
   if ( cripto )
   {
      cripto->setData(rsapass, l_rsapass);
      //if( cripto->desencriptaRSA(s_rsakey.c_str(), (char *)p_pass) )
      if( cripto->desencriptaRSA(s_key.c_str(), (char *)p_pass) )
      {
         memset(rsapass, 0, l_rsapass);
         cripto->getDatosPlanos(&rsapass, l_rsapass);
         cripto->setData(participante, participante_len);
         cripto->setMd5Pwd((const char*)rsapass);
         cripto->desencriptaAES();
         
         unsigned char *participacionbuff = NULL;
         int l_participacion = 0;
         int l_longitud = -1;
         cripto->getDatosPlanos(&participacionbuff, l_participacion); // le hace alloc
         #ifdef DBG
            std::string tmp = dumpBuffer("Mensaje: ", participacionbuff, l_participacion);
            Bitacora->escribe(BIT_INFO, tmp.c_str());
         #endif
         if( participacionbuff[0] == 0x30 )
         {
            if( (int)participacionbuff[1] == 0x82 )
            {
               l_participacion = (((uint16(participacionbuff[2]) << 8) | uint16(participacionbuff[3])));
               l_longitud = 3;
            }
            else
            {
               l_participacion = ((int)participacionbuff[1]);
               l_longitud = 1;
            }
         }
         if( l_longitud > 0 )
         {
            Bitacora->escribePV(BIT_DEBUG, "Se recibieron %d byte en la participacion.", l_participacion);
            uint16 l_b64Part = B64_TamCadCodificada((uint16)l_participacion);
            char *b64llave = (char *)calloc(l_b64Part, sizeof(char));
            B64_Codifica((uint8 *)(participacionbuff + l_longitud + 1), l_participacion, b64llave, &l_b64Part);

            unsigned char *suma_llave = (unsigned char *)calloc(17, sizeof(char));
            cripto->getMd5sum((participacionbuff + l_longitud + 1), l_participacion, suma_llave);
            char *suma_plana =  (char *)calloc(34, sizeof(char));
            char *apuntador = suma_plana;
            for(int i=0; i< 16; i++)
            {
               sprintf(apuntador, "%X", suma_llave[i]);
               apuntador+=2; 
            }
            #ifdef DBG
            Bitacora->escribePV(BIT_DEBUG, "Se recibio la participacion con suma %s", suma_plana );
            #endif   
            mensajes->setMensaje(LLAVEPLANA, b64llave, (int)l_b64Part);
            ret = true;
            if (suma_plana)
            {
               free(suma_plana);
               suma_plana = NULL;
            }
            if (suma_llave)
            {
               free(suma_llave);
               suma_llave = NULL;
            }
            if (b64llave)
            {
               free(b64llave);
               b64llave = NULL;
            }
         }
         if (participacionbuff)
         {
            free(participacionbuff);
            participacionbuff = NULL;  
         }
      }
      else
      {
         Bitacora->escribePV(BIT_ERROR, "Se desencripto incorrectamente la participación");
      }
      delete cripto;
      cripto = NULL;
   }
   return ret;
}

bool CSecretoCompartido::
procesaMensaje()
{
   bool ret = false;
   int l_b64pass = (2048), l_b64llave = (1024 * 10), l_pass, l_llave;

   unsigned char *b64pass =  (unsigned char *)calloc(l_b64pass, sizeof(char));
   unsigned char *b64llave = (unsigned char *)calloc(l_b64llave, sizeof(char));
   error_mensajes = mensajes->getMensaje(ENVIARLLAVE, b64pass, &l_b64pass, b64llave, &l_b64llave);
   if( error_mensajes )
      Bitacora->escribePV(BIT_ERROR, "Ocurrio un error al intentar procesar el mensaje con la llave %d.", error_mensajes);
   else
   {
      Bitacora->escribePV(BIT_INFO, "TamPassword  %d\n TamLlave %d\n", l_b64pass,l_b64llave);
      l_pass = B64_TamCadDecodificada((uint16)l_b64pass);
      l_llave = B64_TamCadDecodificada((uint16)l_b64llave);
      unsigned char *pass = (unsigned char *)calloc(l_pass, sizeof(char));  
      unsigned char *llave = (unsigned char *)calloc(l_llave, sizeof(char));  

      B64_Decodifica((char *)b64pass, l_b64pass, (uint8 *)pass, (uint16 *)&l_pass);
      B64_Decodifica((char *)b64llave, l_b64llave, (uint8 *)llave, (uint16 *)&l_llave);
   
      ret = procesaParticipacion(llave, l_llave, pass, l_pass);
      if ( llave )
      {
         free(llave);
         llave = NULL;
      }
      if ( pass )
      {
         free(pass);
         pass = NULL;
      }
      if ( b64llave )
      {
         free(b64llave);
         b64llave = NULL;
      }
      if ( b64pass )
      {
         free(b64pass);
         b64pass = NULL;
      }
   }
   return ret;
}
