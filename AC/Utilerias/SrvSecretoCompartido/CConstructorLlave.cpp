static const char* _CCONSTRUCTORLLAVE_CPP_VERSION_ ATR_USED = "SrvSecretoCompartido @(#)"\
"DSIC09365SLD 2007-12-13 CConstructorLlave.cpp 1.1.1/0";


//#VERSION: 1.1.1
#include "CConstructorLlave.h"

CConstructorLlave::
CConstructorLlave(MensajesLLavPrivAC *msgs) :
   man_cm(0),
   MQ_ID(101)
{
   mensajes = msgs;
}

CConstructorLlave::
~CConstructorLlave()
{ }

bool CConstructorLlave::
enviaParticipacion()
{
   if(prepararCola())
   {
      memset(&mensaje, 0, sizeof(MSG_LLAVE_AC));
      mensaje.tipo = 2008L;
      mensaje.pid = getpid();
      memcpy(mensaje.mensaje, mensajes->buffer, mensajes->tamMensaje()); 
      if( msgsnd( man_cm, &mensaje, sizeof(MSG_LLAVE_AC), 0 ) == -1 )
         Bitacora->escribePV(BIT_ERROR, "No se pudo enviar la participacion al mediador (ID: %d) (%d).", MQ_ID, errno);
      else
         return recibeMediador();
   }
   return false;
}

void CConstructorLlave::
setMQID(int id)
{
   MQ_ID = id;
   Bitacora->escribePV(BIT_INFO, "El id para la cola de mensajes es %d", MQ_ID);
}

int CConstructorLlave::
getMQID()
{
   Bitacora->escribePV(BIT_INFO, "El id para la cola de mensajes tiene el valor %d", MQ_ID);
   return (MQ_ID);
}

int CConstructorLlave::
solicitaEstadoLlave()
{
   if(prepararCola())
   {
      if(mensajes->tipOperacion() != SOLEDOLLAVE)
         mensajes->setMensaje(SOLEDOLLAVE);
      memset(&mensaje, 0, sizeof(MSG_LLAVE_AC));
      mensaje.tipo = 2008L;
      mensaje.pid = getpid();
      memcpy(mensaje.mensaje, mensajes->buffer, mensajes->tamMensaje());
      if( msgsnd( man_cm, &mensaje, sizeof(MSG_LLAVE_AC), 0 ) == -1 )
         Bitacora->escribePV(BIT_ERROR, "No se pudo solicitar el estado de la llave al mediador (ID: %d) (%d).", MQ_ID, errno);
      else
         return recibeMediador() ? 1 : 0;
   } 
   return -1;
}

bool CConstructorLlave::
prepararCola()
{
   if( !man_cm )
   {
      //Bitacora->escribePV(BIT_DEBUG, "MQ_ID=%d, man_cm=%d, ANTES de 'man_cm = msgget(MQ_ID, 0);'", MQ_ID, man_cm);
      man_cm = msgget(MQ_ID, 0);
      if ( man_cm >= 0 )
         Bitacora->escribe(BIT_DEBUG, "Se logro conectar con la cola de mensajes.");
      else
      {
         Bitacora->escribePV(BIT_ERROR, "No esta disponible la cola de mensajes %d", MQ_ID);
         return false;
      }
   }
   return true;
}

bool CConstructorLlave::
recibeMediador()
{
   Bitacora->escribe(BIT_DEBUG, "El servicio espera por una respuesta en la cola.");
   if( man_cm >= 0 )
   {
      memset(&respuesta, 0, sizeof(MSG_LLAVE_AC));
      if( msgrcv(man_cm, &respuesta, sizeof(MSG_LLAVE_AC), (long)getpid(), 0) != -1 )
      {
         memcpy(mensajes->buffer, respuesta.mensaje, TAM_DATOS);
         Bitacora->escribePV(BIT_INFO, "El mediador a respondido %d.", mensajes->tipOperacion() );
         return procesarMensaje();
      }
      else
         Bitacora->escribePV(BIT_ERROR, "No se pudo obtener una respuesta desde la cola de mensajes (%d)", errno); 
   }
   return false;   
}

bool CConstructorLlave::
llaveActiva()
{
   int l_estado = 1;
   char estado;
   mensajes->getMensaje(EDOLLAVE, &estado, &l_estado);
   l_estado = atoi(&estado);
   return l_estado == 1 ? true : false;
}

bool CConstructorLlave::
procesarMensaje()
{
   switch ( mensajes->tipOperacion() )
   {
      case ERRORLLAVE :
         Bitacora->escribePV(BIT_ERROR, "La participación fue rechazada por el mediador.");
         break;
      case ACUSELLAVE :
         Bitacora->escribePV(BIT_INFO, "La participación fue recibida por el mediador para ser procesada.");
         return recibeMediador();
      case ACUSEARMADO :
         Bitacora->escribePV(BIT_INFO, "La llave fue armada exitosamente en el servidor.");
         break;
      case EDOLLAVE :
         Bitacora->escribePV(BIT_INFO, "Se recibio el estado de la llave desde el mediador.");
         return llaveActiva(); 
      default :
         Bitacora->escribePV(BIT_ERROR, "No se puede manejar el mensaje.");
   }
   return false;
}
